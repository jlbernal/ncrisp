/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "mcefl.hh"

#include <math.h>
#include <stdio.h>

#include <iostream>
#include <iomanip>

#include <time.h>

#include "util.hh"


using namespace std;

MCEFL::MCEFL(int A, int Z, int Ex, long *idum)
{
    //    my_nucleus =new nucleus(A, Z, Ex);
    my_nucleus =new nucleus();

    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;
    this->idum =idum;

    n_fission =0;
    n_proton =0;
    n_neutron =0;
    n_alpha =0;

    cumul_fission =0;
    cumul_proton  =0;
    cumul_neutron =0;
    cumul_alpha   =0;
    cumul_fissility =0;
    
    final_a =0;
    final_z =0;
}

MCEFL::~MCEFL(){
    delete my_nucleus;
}


bool MCEFL::statisticalB(){

    int final_a_ =0;
    int final_z_ =0; //final configuration
    int neutron_ =0;
    int proton_  =0;
    int alpha_   =0;
    double energy_ =0.0;
    double W =0.0; //fissility

    bool result =false;

    //    n_fission =0;
    //    n_proton =0;
    //    n_neutron =0;
    //    n_alpha =0;


    if ( Ex > 0. ){
        result =Generate( A, Z, Ex);

        FinalConfiguration(A, Z, final_a_, final_z_);
        energy_ = final_E();
        neutron_ = NeutronMultiplicity();
        proton_  = ProtonMultiplicity();
        alpha_    = AlphaMultiplicity();
        W = Fissility();

        cumul_neutron +=n_neutron;
        cumul_proton +=n_proton;
        cumul_alpha +=n_alpha;
        cumul_fission +=n_fission;
        cumul_fissility +=_fissility;

        _energy += energy_;
    }
    
    return result;
}

bool MCEFL::dynamicalSB(){

    int _final_a_ =0;
    int _final_z_ =0; //final configuration
    int neutron_ =0;
    int proton_  =0;
    int alpha_   =0;
    double energy_ =0.0;

    n_proton =0;
    n_neutron =0;
    n_alpha =0;

    cumul_proton  =0;
    cumul_neutron =0;
    cumul_alpha   =0;

    if ( Ex > 0. ){
        Generate1( A, Z, Ex);
        FinalConfiguration(A, Z, _final_a_, _final_z_);
        energy_ = final_E();
        neutron_ = NeutronMultiplicity();
        proton_  = ProtonMultiplicity();
        alpha_    = AlphaMultiplicity();

        cumul_neutron +=n_neutron;
        cumul_proton +=n_proton;
        cumul_alpha +=n_alpha;

        _energy += energy_;
        
        final_a =_final_a_;
        final_z =_final_z_;
    }
}


void MCEFL::init(int A, int Z, double En){
    this->An = my_nucleus->CalculateAn(A, Z, En);
    this->Bn = my_nucleus->CalculateBn(A, Z);
    this->Af = my_nucleus->CalculateAf(A, Z, En);
    this->Bf = my_nucleus->CalculateBf(A, Z, En);
    this->Ap = my_nucleus->CalculateAp(A, Z, En);
    this->Bp = my_nucleus->CalculateBp(A, Z, En);
    this->Ba = my_nucleus->CalculateBa(A, Z, En);
    this->Vp = my_nucleus->CalculateVp(A, Z, En);
    this->Aa = my_nucleus->CalculateAa(A, Z, En);
    this->Va = my_nucleus->CalculateVa(A, Z, En);
}


double MCEFL::Gamma_p(int A, int Z, double E){
    double Ep = E - this->Bp - this->Vp;
    double En = E - this->Bn;
    if ( (Ep > 0.) && (En > 0.) ) {
        double Kp = Ep / En;
        double x = 2. * (sqrt(this->Ap) * sqrt(Ep) - sqrt(this->An) * sqrt(En));
        return Kp * exp(x);
    }
    return 0.;
}


double MCEFL::Gamma_a(int A, int Z, double E){
    double Ea = E - this->Ba - this->Va;
    double En = E - this->Bn;
    if ( Ea > 0. && En > 0. ) {
        double Ka = 2.0 * Ea / En;
        double x = 2. * (sqrt(this->Aa) * sqrt(Ea) - sqrt(this->An) * sqrt(En));
        return Ka * exp(x);
    }
    return 0.;
}

double MCEFL::Gamma_f(int A, int, double E){
    double Ef = E - this->Bf;
    double En = E - this->Bn;

    if (Ef > 0. && En > 0. ) {
        //		double rf = 1.;
        if( ( this->Af * Ef ) <= 0 ){
            std::cout << "\n" << "Af = " << Af << ", Ef = " << Ef << ", An = " << An << "\n";
            throw std::exception();
        }
        double Kf = 14.39  * this->An * (2. * sqrt( this->Af * Ef) - 1.) / (4.  * this->Af * pow( (double)A, 2./3.) * En );
        //double Kf = 15. * (2. * sqrt( rf * mcef_p.An * Ef) - 1.) / (4. * rf * pow( (double)A, 2./3.) * En );
        double x  =  2. * (sqrt(this->Af) * sqrt(Ef) - sqrt(this->An) * sqrt(En));
        return Kf * exp(x);
    }
    return 0.;
}



bool MCEFL::Generate(int A, int Z, double En){

    _energy =En;
    int _A  =A;
    int _Z  =Z;

    double W = 0.;
    double NF= 1.;
    n_fission = 0;
    n_proton = n_neutron = n_alpha = 0;

    double eps = 2. ;

    while ( _energy > 0. && _A > 0 && _Z > 0){

        init(_A, _Z, _energy);

        double Gf =0.0;
        double Gp =0.0;
        double Ga =0.0;
        double Gn =0.0;
        double G_sum =0.0;
        double p_fission =0.0;

        Gf = this->Gamma_f(_A, _Z, _energy);
        Gp = this->Gamma_p(_A, _Z, _energy);
        Ga = this->Gamma_a(_A, _Z, _energy);
        Gn = 1.0;


        if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
        if(Bp != Bp)	Gp = 0.0;
        if(Bn != Bn)	Gn = 0.0;
        if(Bf != Bf)	Gf = 0.0;
        if(Gn != Gn)	Gn = 0.0;
        if(Ga != Ga)	Ga = 0.0;
        if(Gp != Gp)	Gp = 0.0;
        if(Gf != Gf)	Gf = 0.0;

        if( (_A-_Z)<2 || _Z<2 )
            Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
        if( (_A-_Z)<1 )
            Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
        if( _Z<1 )
            Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


        G_sum = Gn + Gf + Gp + Ga;
        if(G_sum == 0.0)
            break;
        p_fission = Gf / ( G_sum );

        W += NF * p_fission;
        NF = NF * (1. - p_fission);


        double x = ran2(idum);

        if ( x <= Gp/G_sum ) { // proton out
            if( (this->Bp + eps) > _energy ){
                eps = _energy - this->Bp;
                _energy = 0.0;
            } else {
                _energy -= (this->Bp + eps);
            }
            _A--;
            _Z--;
            n_proton++;
        }
        else{
            if ( x <= (Gp + Ga)/G_sum ) { // alpha out
                if( (this->Ba + eps) > _energy ){
                    eps = _energy - this->Ba;
                    _energy = 0.0;
                } else {
                    _energy -= (this->Ba + eps);
                }
                _A -= 4;
                _Z -= 2;
                n_alpha++;
            }
            else {
                if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
                    n_fission++;
                    _fissility = W;
                    //return _fissility;
                    return true;
                }else {
                    if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                        if( (this->Bn + eps) > _energy ){
                            eps = _energy - this->Bn;
                            _energy = 0.0;
                        } else {
                            _energy -= (this->Bn + eps);
                        }
                        _A--;
                        n_neutron++;
                    }
                }
            }
        }
        _fissility = W;
        //return _fissility;
        return true;
    }
    //else{
    //return false;
    //}
}


void MCEFL::Generate1(int A, int Z, double En){

    double _energy1 =En;
    int _A  =A;
    int _Z  =Z;

    n_proton = n_neutron = n_alpha = 0;

    double eps = 2. ;

    init(_A, _Z, _energy1);

    double Gp =0.0;
    double Ga =0.0;
    double Gn =0.0;
    double G_sum =0.0;

    Gp = this->Gamma_p(_A, _Z, _energy);
    Ga = this->Gamma_a(_A, _Z, _energy);
    Gn = 1.0;

    if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
    if(Bp != Bp)	Gp = 0.0;
    if(Bn != Bn)	Gn = 0.0;
    if(Gn != Gn)	Gn = 0.0;
    if(Ga != Ga)	Ga = 0.0;
    if(Gp != Gp)	Gp = 0.0;

    if( (_A-_Z)<2 || _Z<2 )
        Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
    if( (_A-_Z)<1 )
        Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
    if( _Z<1 )
        Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


    G_sum = Gn + Gp + Ga;
    //if(G_sum == 0.0)
    //break;

    double x = ran2(idum);

    if ( x <= Gp/G_sum ) { // proton out
        if( (this->Bp + eps) > _energy ){
            eps = _energy - this->Bp;
            _energy = 0.0;
        } else {
            _energy -= (this->Bp + eps);
        }
        _A--;
        _Z--;
        n_proton++;
    }
    else{
        if ( x <= (Gp + Ga)/G_sum ) { // alpha out
            if( (this->Ba + eps) > _energy ){
                eps = _energy - this->Ba;
                _energy = 0.0;
            } else {
                _energy -= (this->Ba + eps);
            }
            _A -= 4;
            _Z -= 2;
            n_alpha++;
        }
        else {
            if ( x > ( Gp + Ga )/G_sum ) { // nêutron out
                if( (this->Bn + eps) > _energy ){
                    eps = _energy - this->Bn;
                    _energy = 0.0;
                } else {
                    _energy -= (this->Bn + eps);
                }
                _A--;
                n_neutron++;
            }
        }
    }
    //}
}



void MCEFL::FinalConfiguration(const int A, const int Z, int &A_final, int &Z_final){
    if ( n_fission == 0 ) {
        Z_final = Z - ProtonMultiplicity() - AlphaMultiplicity()*2;
        A_final = A - (ProtonMultiplicity() + NeutronMultiplicity() + AlphaMultiplicity()*4);
    }
}


