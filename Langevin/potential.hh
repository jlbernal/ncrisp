/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef POTENTIAL_H
#define POTENTIAL_H
/**
 * author:: Ubaldo Bannos Rodriguez && Drow
 * status:: not finished
 * date (last update): 05/04/2014
 **/

#include <math.h>
#include <stdio.h>
#include <iostream>

#include "derivFunction.hh"
#include "function.hh"


struct NuclearPotentialStruc{
public:
  
    double l_density(int l,double S){
      return (2*l+1)*exp(S);
    } 

    double entropy(double E, int A, int Z, /*double L,*/ double q/*, double c, double h*/){
        double diffE =E - potentialMS(A, Z, q);
        if( diffE > 0.)
            return 2.0*sqrt(a(q, A)*(E - potentialMS(A, Z, q/*, c, h*/)));
        else
            return 0.;
        //return sqrt(E-potentialMS(A, Z, L, q, c, h));
    }
 
      double entropy(double E, int A, int Z, double q , double L){
	double potMS  = E - potentialMS(A, Z, q, L);
        if( potMS > 0.)
            return 2.0*sqrt(a(q, A)*potMS);
        else
            return 0.;
        //return sqrt(E-potentialMS(A, Z, L, q, c, h));
    }	
    
    double deriv_entropy(double E, int A, int Z, double q);
     double deriv_entropy(double E, int A, int Z, double q, double L);


    /**
     * @brief potentialMS
     * @return Potential Myers and Swiatecki
     */
    //double potentialMS(double , struct params);
    double potentialMS(int A, int Z, double q ); // sobrecargado 
 double potentialMS(int A, int Z, double q, double L ); // sobrecargado 

    //    density level
    inline double a(double q, double A){
        return 0.073*A + 0.095*pow(A, 2.0/3.0)*Bs(q);
        //return A/12.0;
    }
    
    inline double temperature(double E, int A, int Z, double q){
        return entropy(E, A ,Z, q)/2./a(q, A);
        //return A/10;
    }
    
        inline double temperature(double E, int A, int Z, double q,double L){
        return entropy(E, A ,Z, q, L)/2./a(q, A);
        //return A/10;
        }
    
    //	force
    double force(int A, int Z, double q);
        double force(int A, int Z, double q, double L); 

    double HelmholtzFEnergy(double Ex, int A, int Z, double q);
     double HelmholtzFEnergy(double Ex, int A, int Z, double q, double L);
    
    inline double Bf(int A, int Z){
        double symCN= (1.-2.*Z/A)*(1.-2.*Z/A);
        double XCN=c3*Z*Z/(A*(1.-k*symCN)*2.*a2);
        return Bf_Essp(XCN)*pow(A, 2./3.)*a2*(1.-k*symCN);
    }
    


private:

    static const double a2 = 17.9439;// MeV
    static const double c3 = 0.7053; // MeV
    static const double k  = 1.7826;
    static const double cr = 34.50;  // Mev


    /*Method*/

    double Bs(double q);
    double X(double qsd);  //fissilityParam
    double Bf_Essp(double X);  //fission_barrier
    double Bc(double q);
    double Br(double q);
    double Calc_h(double c);

       double Jtrans(double c, double h);
       double Jparall(double c, double h);

        double Bsh(double c, double h);
        double q_func(double c, double h);
    

};

struct paramFunctionPot{
    int A;
    int Z;
    double E;
};

struct paramFunctionPot1{
    int A;
    int Z;
    double E;
    double L; 	
};


#endif // POTENTIAL_H
