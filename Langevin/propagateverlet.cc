/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "propagateverlet.hh"

int verlet_mod(double t, double x[], double out[], void *param, int dim,
               double (*function)(double x[], void *params)){

    /**
     * @brief VERLET ALGORITHM FOR LANGEVIN DYNAMICS ---> Thesis
     */

    double mass = ((struct paramFunction *) param)->mass;
    //    double k = ((struct paramFunction *) param)->k;
    double beta = ((struct paramFunction *) param)->beta;
    double randValue = ((struct paramFunction *) param)->randValue;
    double kT = ((struct paramFunction *) param)->kT;

    double *tmp_x =new double[dim];

    /**
         * @brief VERLET ALGORITHM  --> Jensen "Introduction to Computational....(eq. 14.9)"
         */
    //~ out[0] =x[0] + t*x[1] + t*t*0.5*(function(x, param))/mass;
    //~
    //~ tmp_x[0] =out[0];
    //~ tmp_x[1] =x[1];
    //~ out[1] =x[1] + t*0.5*( function(tmp_x, param) + function(x, param))/mass;


    out[0] =x[0] + t*x[1]/mass;

    tmp_x[0] =out[0];
    tmp_x[1] =x[1];
    out[1] =x[1] + t*function(tmp_x, param) + sqrt(beta*mass*kT*t)*randValue;

    //    out[0] =x[0] + t*x[1]/mass + t*t*0.5*(function(x, param))/mass;
    //    tmp_x[0] =out[0];
    //    tmp_x[1] =x[1] +t;//out[1] +t;
    //    out[1] =x[1] + t*(function(x, param)) + 0.5*t*t*(-k*x[1]/mass);

    //    out[0] =x[0] + t*x[1]/mass;
    //    out[1] =x[1] + t*(function(x, param));// + sqrt(beta)*sqrt(t)*randValue;

    delete[] tmp_x;

}
