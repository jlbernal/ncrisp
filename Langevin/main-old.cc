/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <iomanip>


#include "dynamic.h"
//#include "mcef.h"
#include "constant.h"

#include "potential.h"

//#include "binding_energy.h"
//#include "nucleus.h"

using namespace std;


int main (void)
{
    long nRun =100;


    //             double Ex =30.;
    //             int A =248;
    //             int Z =98;

    //            double Ex =50.;
    //            int A =40;
    //            int Z =20;

    //    double Ex =45.;
    //    int A =13;
    //    int Z =6;

    //    int Z =56;
    //    int A =126;
    //    double Ex =131.7; //MeV

    //              double Ex =30.;
    //              int A =58;
    //              int Z =28;

    //            double Ex =30.;
    //            int A =74;
    //            int Z =32;

    //    double Ex =30.;
    //    int A =50;
    //    int Z =24;

    //    double Ex =30.;
    //    int A =64;
    //    int Z =30;

    //        double Ex =30.;
    //        int A =58;
    //        int Z =28;

    //    double Ex =300.;
    //    int A =74;
    //    int Z =32;

    //        double Ex =207.;
    //        int A =158;
    //        int Z =68;

    //      double Ex =450.;
    //      int A =231;
    //      int Z =91;

    /* *************** */

    /*
    nucleus nucleus1;

    A =208;
    Z =82;
    //Z =A/(1.98+0.0015*pow(A,2./3.));

    //for( A =1; A<200; A++){
    //Z =A/(1.98+0.0015*pow(A,2./3.));
    cout  << setw(12) << setprecision(6)  << A;
    cout  << setw(12) << setprecision(6)  << Z;
    cout  << setw(12) << setprecision(6)  << CalculateMass(A,Z);
    cout  << setw(12) << setprecision(6)  << mass(A,Z);
//	cout << endl;
    cout  << setw(12) << setprecision(6)  << binding(A,Z);
    cout  << setw(12) << setprecision(6)  << binding1(A,Z);

    cout  << setw(12) << setprecision(6)  << nucleus1.CalculateBn(A,Z);
    cout  << setw(12) << setprecision(6)  << nucleus1.CalculateBp(A,Z, (double)A);
    cout << endl;
    //}
*/
    /* *************** */

    //    double Ex =100.;
    //    int A =205;
    //    int Z =85;

    //    int Z =90;
    //    int A =224;
    //    double Ex =90.; //MeV

    //    int Z =82;
    //    int A =200;
    //    double Ex =49.83362579; //MeV

    int Z =82;
    int A =200;
    double Ex =49.83362579; //MeV
    //    double Ex =100; //MeV

    /**
     * @brief mode:
     *              (0)             --> (default) Langevin && statisticall
     *              (< 0)(nagative) --> only Langevin                   <---- no finished
     *              (> 0)(positive) --> statisticall
     */

    int mode =0;

    NuclearPotentialStruc potentialStruct;
    double R0=(r0*pow(A, 1./3.));
    //double qsci =0.3*R0/2.;
    double qsci =1.2;
    double qneck =0.6;
    //double td =100.0*hbar_sz;
    double td =100.0;

    //    cout << R0 << endl;
    //    cout << 1.2*R0 << endl;
    //        cout << 0.3*R0 << endl;

    //    double temp =5.;
    //    double _Ex_temp  =temp*temp*potentialStruct.a( 0.375, A);
    //    Ex =_Ex_temp;


    cout << "-------------------------" << endl;
    cout << "|" << A << "\t \t \t|" << endl;
    cout << "|" << setw(3) << nuc_sym[2*Z-2] << nuc_sym[2*Z-1] << " *Ex:= " << Ex << " MeV" << "\t|" << endl;
    cout << "|" << Z ;
    cout << "\t \t \t|" << endl;
    cout << "-------------------------" << endl;
    //cout << 0.3*(1.16*pow(A, 1./3.));

    dynamic *objectDynamic =new dynamic(A, Z, Ex, nRun, qsci, qneck, td, mode);
    objectDynamic->run();

    int neutron =objectDynamic->Get_neutron();
    int proton =objectDynamic->Get_proton();
    int alpha  =objectDynamic->Get_alpha();
    int fission  =objectDynamic->Get_fission();
    double fissility =objectDynamic->Get_fissility();
    int residues =objectDynamic->Get_residues();


    cout  << setw(12) << setprecision(6)  << (double)neutron/nRun;
    cout  << setw(12) << setprecision(6)  << (double)proton/nRun;
    cout  << setw(12) << setprecision(6)  << (double)alpha/nRun;
    cout  << setw(12) << setprecision(6)  << (double)fission/nRun;
    cout  << setw(14) << setprecision(6)  << (double)residues/nRun;
    cout  << setw(14) << setprecision(6)  << (double)fissility/nRun;
    cout << endl;

    cout  << setw(12) << setprecision(6)  << (double)neutron/fission;
    cout  << setw(12) << setprecision(6)  << (double)proton/fission;
    cout  << setw(12) << setprecision(6)  << (double)alpha/fission;
    cout  << setw(12) << setprecision(6)  << (double)fission/fission;
    cout  << setw(14) << setprecision(6)  << (double)residues/fission;
    cout  << setw(14) << setprecision(6)  << (double)fissility/fission;
    cout << endl;

    //    cout  << setw(12) << setprecision(10)  << neutron;
    //    cout  << setw(12) << setprecision(10)  << proton;
    //    cout  << setw(12) << setprecision(10)  << alpha;
    //    cout  << setw(12) << setprecision(10)  << fission;
    //    cout  << setw(12) << setprecision(10)  <<  fissility;
    //    cout << endl;

    //    cout  << setw(6) << setprecision(8)  << (double)neutron/objectDynamic->Get_Nfiss();
    //    cout  << setw(6) << setprecision(8)  << (double)proton/objectDynamic->Get_Nfiss();
    //    cout  << setw(6) << setprecision(8)  << (double)alpha/objectDynamic->Get_Nfiss();
    //    cout  << setw(6) << setprecision(8)<< (double)fissility/objectDynamic->Get_Nfiss();
    //    cout << endl;

    //    cout  << setw(8) << setprecision(10)  << objectDynamic->Get_Nres();
    //    cout  << setw(8) << setprecision(10)  << objectDynamic->Get_Nfiss();
    //    cout  << setw(8) << setprecision(10)  << objectDynamic->Get_Ndin();
    //    cout  << setw(8) << setprecision(10)  << objectDynamic->Get_Nstat();
    //    cout << endl;

    delete objectDynamic;


    /* printing potential*/

    ofstream potentialFile;
    potentialFile.open("potentialFile.dat",ios::out | ios::binary);
    potentialFile.setf(ios::right | ios::showpoint | ios::fixed);
    for(double q=0; q<qsci; q+=0.001){
        potentialFile << setw(0) << setprecision(5)  << q;
        potentialFile << setw(16) << setprecision(5) << potentialStruct.potentialMS(A, Z, q);
        potentialFile << setw(17) << setprecision(5) << potentialStruct.deriv_entropy(Ex, A, Z, q);
        //potentialFile << setw(17) << setprecision(5) << potentialStruct.temperature(Ex, A, Z, q)*potentialStruct.deriv_entropy(Ex, A, Z, q);
        potentialFile << endl;
    }

    //    /**
    //     * testing potential
    //     **/
    //    long nRun =5000;
    //    double Ex =1.0;
    //    int A =200;
    //    int Z =82;


    //    //NuclearPotentialStruc potentialStruct;
    //    for(double q=0; q<1.34; q+=0.001){
    //        //        potentialStruct.potentialMS(A, Z, q);
    //        //cout << q*2.*(1.16*pow(A, 1./3.));
    //        cout << q;
    //        cout << " " << potentialStruct.potentialMS(A, Z, q);
    //        cout << " " << potentialStruct.force(A, Z, q);
    //        cout << " " << potentialStruct.entropy(Ex, A, Z, q);
    //        cout << " " << potentialStruct.deriv_entropy(Ex, A, Z, q);
    //        cout << " " << potentialStruct.temperature(Ex, A, Z, q);
    //        //cout << " " << -potentialStruct.entropy(Ex, A, Z, q)*sqrt((Ex - potentialStruct.potentialMS(A,Z,q))/potentialStruct.a(q, A));
    //        cout << " " << potentialStruct.HelmholtzFEnergy(Ex, A, Z, q) ;
    //        cout << " " <<  potentialStruct.a(q, A);
    //        cout << endl;
    //        //        cout << q << " " << -potentialStruct.entropy(Ex, A, Z, q) << endl;
    //        //cout << q << " " << potentialStruct.a(q, A) << endl;

    //        //        potentialFile << setw(0) << setprecision(5) << q;
    //        //        potentialFile << setw(8) << setprecision(5) << potentialStruct.potentialMS(A, Z, q);
    //        //        //        potentialFile << q;
    //        //        //        potentialFile << " " << potentialStruct.potentialMS(A, Z, q);
    //        //        potentialFile << endl;
    //    }


    //~ /**
    //~ * Barrera de fision
    //~ **/
    //    NuclearPotentialStruc potentialStruct;
    //    int Zi =1;
    //    for(int Ai=2; Ai<350;Ai++){
    //        Zi=0.5*(double)Ai*(1.-0.4*(double)Ai/((double)Ai+200.));
    //        cout << Ai;
    //        cout << " " << potentialStruct.Bf(Ai, Zi);
    //        cout << endl;
    //    }

    //~ /**
    //~ * level density
    //~ **/
    //~ for(int Ai=2; Ai<350;Ai++){
    //~ cout << Ai;
    //~ cout << " " << potentialStruct.a(0.375, Ai)/Ai;
    //~ cout << endl;
    //~ }

    return 1;
}



