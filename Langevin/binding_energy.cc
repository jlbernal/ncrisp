/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "binding_energy.hh"

double mass(int A, int Z/*, void *params*/){

    /**
     *@ return [mass] = MeV/(c*c)
     **/

    return Z*_mp + (A-Z)*_mn - binding(A, Z)/(_c*_c) ;
}

double binding(int A, int Z){

    /**
     *@ return [mass] = MeV
     **/

    int N =0;
    N=A-Z;
    //	double av =15.5;	//MeV
    //	double as =16.8;	//MeV
    //	double ac =0.72;	//MeV
    //	double asym =23.0;	//MeV
    //	double ap=34.0;	//MeV

    double av =15.75;	//MeV
    double as =17.8;	//MeV
    double ac =0.710;	//MeV
    double asym =23.7;	//MeV
    double ap=34.0;	//MeV

    //	double av =15.56;
    //	double as =17.23;
    //	double ac =0.7;
    //	double asym =23.29;
    //	double ap=12.0;

    double delta =0.;
    double B =0.;

    B =av*A - as*pow(A,2./3.) -ac*Z*(Z-1)*pow(A,-1./3.) - asym*pow((A-2*Z),2)/A;

    if( Z%2 ==0 && N%2 ==0 )
        delta =ap*pow(A,-3./4.);
    //		delta =ap*pow(A,-1./2.);
    if( Z%2 !=0 && N%2 !=0 )
        delta =-ap*pow(A,-3./4.);
    //		delta =-ap*pow(A,-1./2.);
    if( A%2 !=0)
        delta =0.0;

    return B + delta;
}


double binding1(int A, int Z){
    int N =0;
    N=A-Z;

    double W0    =15.645;	//MeV
    double gamma =19.655;	//MeV
    double beta  =30.586;	//MeV
    double etta  =53.767;	//MeV
    double delta =10.609;	//MeV
    double r0_    =1.2025;	//fm


    double piaring =0.;
    double B =0.;

    B =W0*A - gamma*pow(A,2./3.)
            - 0.86*pow(r0_,-1)*Z*Z*pow(A,-1./3.)*(1.0 - 0.76361*pow(Z,-1./3.) - 2.543*pow(r0_,-2.)*pow(A,-2./3.))
            - (etta*pow(A,-4./3.)- beta*pow(A,-1.))*(N-Z)*(N-Z)
            + 7.*exp(-6.*fabs(N-Z)*pow(A,-1)) + 14.33*10E-6*pow(Z,2.39);


    if( Z%2 ==0 && N%2 ==0 )
        piaring =delta*pow(A,-1./2.);
    if( Z%2 !=0 && N%2 !=0 )
        piaring =-delta*pow(A,-1./2.);
    if( A%2 !=0 && N%2 == 0)
        piaring =0.0;

    return B + piaring;
}

//double CalculateMass_Chung(int A, int Z){
double CalculateMass(int A, int Z){
    const double pn =   1.008665;
    const double pz =   1.007825;
    const double a1 =  16.710;
    const double a2 =  18.500;
    const double a3 = 100.00;
    const double a4 =   0.750;
    const double f  = 931.478;
    double delta = 0.;
    int	z2 = 2 * (int)(ceil( (double)Z / 2.)),
        n2 = 2 * (int)(ceil( (double)(Z - A) / 2.));
    if( z2 == Z ) {
        if( n2 == (Z - A) ) {
            delta= -36. * pow( (double)A, -3./4. );
        }
    }
    else {
        if ( n2  != Z - A )
            delta = 36. * pow(A, -3.0/4.0);
    }
    double m = 0.;
    m = pn * (A - Z)*f;
    m = m + pz * Z * f;
    m = m + 0.511 * Z;
    m = m - a1 * A * f/1000.;
    m = m + a2 * pow( (double)A, 2.0/3.0) * f/1000.;
    m = m + a3 * (f/1000.) * pow( (double)A/2. - (double)Z, 2) / (double)A;
    m = m + a4 * pow( (double)Z, 2.) / pow( (double)A, 1./3.);
    m = m + delta * f/1000.;
    return m;
}

