/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "dynamic.hh"
#include <fstream>
#include <TMath.h>

using namespace std;
//const double hbar_sz=0.658217;


dynamic::dynamic(param_rootFiles param){
    Bn =0.;
    Bf =0.;
    Nres =0;
    Ndin =0;
    nStat =0;
    Nfiss =0;
    iteration=0;
    finish_run =false;
    //tmax =100.*_hbar_sz; VER ESTO BIEN COMO ES
    tmax =100.;
    idum = -(long)time(NULL);

    q_eq =0.375; //posicion de equilibrio
    //        step =0.005;  //paso de tiempo
    //step =0.0005*hbar_sz;  //paso de tiempo

    step =0.1;  //paso de tiempo  --> 1fm/c

    DeltaStep =1000.*step;

    rootFile =new work_rootFiles();
    rootFile->create_rootFile(param);
}

dynamic::dynamic(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode)
{
    this->mode =mode;

    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;

    this->qsci =qsci;
    this->teq  =teq;
    this->qneck =qneck;

    Bn =0.;
    Bf =0.;

    Nres =0;
    Ndin =0;

    nStat =0;
    Nfiss =0;

    finish_run =false;

  //  tmax =1000.*_hbar_sz;
    tmax =100.;

    idum = -(long)time(NULL);

    /* esto no es necesario.. pero me aseguro que la semilla aun sea aun mas semi-aleatoria...*/
    int numb1 =1, numb =100*ran2(&idum);
    for(int i=0; i<numb ; i++)
        numb1 =ran2(&idum);

    /* langevin*/

    q_eq =0.375; //posicion de equilibrio
    //        step =0.005;  //paso de tiempo
   // step =0.0005*_hbar_sz;  //paso de tiempo
    //step =3E-3;  //paso de tiempo  --> 1fm/c
    step =1;  //paso de tiempo  --> 1fm/c // cambio a dos fm

    DeltaStep =1000.*step;
}

dynamic::~dynamic(){

    rootFile->end_rootFile();
    delete rootFile;
}

void dynamic::run(){

    n_proton =0;
    n_neutron =0;
    n_alpha =0;
    n_fission =0;
    n_fissility =0.;
    n_res =0;

    finish_run =false;

    if( Ex > 0.0){

        if ( mode < 0){
            std::cout << "dynamical Langevin ..." << std::endl;
            Langevin();
        }
        if( mode == 0 ){
            std::cout << "dynamical Langevin && statisticall emission ..." << std::endl;
            Langevin_Statisticall();
        }
        if( mode > 0 ){
            std::cout << "statisticall emission ..." << std::endl;
            Statisticall_1(this->A, this->Z, this->Ex);
        }
    }
}

void dynamic::run(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode){

    this->mode =mode;
    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;
    this->qsci =qsci;
    this->teq  =teq;
    this->qneck =qneck;

    this->n_proton =0;
    this->n_neutron =0;
    this->n_alpha =0;
    this->n_fission =0;
    this->n_gamma=0;
    this->n_fissility =0.;
    this->n_res =0;
    this->finish_run =false;
    
    paramFunction.mass =CalculateMass(A, Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
    paramFunction.randValue =0.;
    paramFunction.kT = sqrt(Ex/(potStruct.a(qsci, A)));
    double beta_coef = this-> beta_l;
   

    if( Ex > 0.0){                                                                      //ELIMINAR ESTO

        std::cout << "running... " << nRun << " trajectories" << std::endl;

        if ( mode < 0){
            std::cout << "dynamical Langevin ..." << std::endl;
            Langevin();
        }
        if( mode == 0 ){
            std::cout << "dynamical Langevin && statisticall emission ..." << std::endl;
//          Langevin_Statisticall_Copy();	    
	    Langevin_Statisticall();
        }
        if( mode > 0 ){
            std::cout << "statisticall emission ..." << std::endl;
            Statisticall_1(this->A, this->Z, this->Ex);
        }
    }
}

void dynamic::Calculate_qgs_qsd (int AA, int ZZ, double energy, double ll, double &qgs, double &qsd, double &Sgs, double &Ssd){
               
               Sgs=-1000;Ssd=1000;
	       double qq,nn=0;
	       double qsci=1.2;
	       double d_q=0.001;
	    
	       for(qq=0;qq<=qsci;qq=qq+d_q){
		 double SS = potStruct.entropy(energy, AA, ZZ, qq , ll);
	         if (SS>Sgs){
		   Sgs=SS;
		   qgs=qq;
		}		
	       }
	       if (qgs<=d_q || qgs>qsci-d_q) {qgs=0.375;nn=1;}
	       for(qq=qgs;qq<=qsci;qq=qq+d_q){
		 double SS=potStruct.entropy(energy, AA, ZZ, qq , ll);
		 if(SS < Ssd){
		   Ssd=SS;
		   qsd=qq;
		}
	       }
	       if(qsd==qgs){
		 if(qgs==0.375 && nn==1) qsd=0.6;
		 else qsd=1.17;
	       } 
	       
	      
	      //cout<<"qssss "<<" "<<qgs<<" "<<qsd<<endl;
}

void dynamic::Langevin_Statisticall() {
    
    int iA=0,iZ=0;
    double iE,il,it,iq,ipq,ih,S,der_S,temp,_mass,d_q=0.001,Ssd=1000,Sgs=-1000,qsd=0.3,qgs=0.3,min_barr;
    double beta_coef = this-> beta_l;
    bool fission_emission;

    elements_rootFiles elements;

    
    for(int i=0;i<nRun; i++){
     iteration++;
     
    this->n_proton =0;
    this->n_neutron =0;
    this->n_alpha =0;
    this->n_fission =0;
    this->n_gamma=0;
    this->n_fissility =0.;
    this->n_res =0;     
    
//        iA=this->A;
//        iZ=this->Z;
//        iE=this->Ex;
//        il=this->L;
       iA=178;
       iZ=74;
       
       int ap,zp;
       ap=19;zp=9;
       //iE=150*181/iA+CalculateMass(19,9)+CalculateMass(181,73)-CalculateMass(iA,iZ);
       double Elab=85+(iteration/200)*20;
       double Ecm=Elab*(iA-ap)/iA;
       iE=Ecm+CalculateMass(ap,zp)+CalculateMass(iA-ap,iZ-zp)-CalculateMass(iA,iZ);
       double Etot=iE;
       cout<<"Etot "<<iE<<endl;
       //iE=105.9;
       //il=40; 
       
       cout<<"Semilla_"<<gRandom->GetSeed()<<"_"<<endl;
       il=l_dist(iA,iZ,ap,zp,Ecm);
   //    il=40;
    //   iE=40+(iteration/100)*10;
       
//        for(iq=0.05;iq<=qsci;iq=iq+0.01){
// 	 printf("%lf\t%lf\t%lf\t%lf\n",iq,potStruct.potentialMS(iA,iZ,iq,il),potStruct.entropy(iE,iA,iZ,iq,il),potStruct.temperature(iE,iA,iZ,iq,il));
//       }
//       break;
       
       
       this->step=0.05;
       tmax=100;
       it=0;
       finish_run=0;
       fission_emission=false;
       Calculate_qgs_qsd(iA, iZ, iE, il, qgs, qsd,Sgs,Ssd);
       iq=qgs;
       this->beta_l=beta_SPS(iq,0.6,1.2);
       
       while(finish_run==0){
	 
	 Calculate_qgs_qsd(iA, iZ, iE, il, qgs, qsd,Sgs,Ssd);
	 calculate_nucleus(iA,iZ,iE);
	 
	 if(Bn<Bf) min_barr=Bn;
	 else min_barr=Bf;
	 
	 if(iE-potStruct.potentialMS(iA,iZ,iq,il)<0){
	   finish_run=4; iteration--; break; 
	 }
	 if(iE-potStruct.potentialMS(iA,iZ,qgs,il)<min_barr && iq<qsd){
	   finish_run=2; break;
	 }
	 
	else if(iq>qsci){
	   finish_run=1; break;
	}
	
	else if(iq<qsd && it>tmax){    //FALTA CONDICION DE ENTROPIA
	   finish_run=3; break;
	}
	
	else{
	  fission_emission=false;
	  Emission(iA, iZ, iE, it, il, iq, qsd, qgs, fission_emission);
	  dynamic_step(iA,iZ,iq,it,iE,il);
	}

      }
      
      
        fission_emission=true;
	iq=qgs;
	while(finish_run==3 && iE-potStruct.potentialMS(iA,iZ,iq,il)>=min_barr && iE-potStruct.potentialMS(iA,iZ,iq,il)>=0){
	  //cout<<"Esta aqui "<< iE-potStruct.potentialMS(iA,iZ,iq,il) <<" "<< min_barr <<endl;
	  Calculate_qgs_qsd(iA, iZ, iE, il, qgs, qsd,Sgs,Ssd);
	  calculate_nucleus(iA,iZ,iE);
	  Emission(iA, iZ, iE, it, il, iq, qsd, qgs, fission_emission);
	  if(Bn<Bf) min_barr=Bn;
	  else min_barr=Bf;
	}
      
        if(finish_run==3) finish_run=2;

          elements.A =iA;
          elements.Z =iZ;
          elements.energy=iE;
          elements.neutrons =n_neutron;
          elements.protons =n_proton;
          elements.alphas=n_alpha;
          elements.fission=n_fission;
          elements.q=iq;
          elements.qes=qsci;
          elements.l=il;
	  elements.iteration=iteration;
	  elements.E0=Etot;
	
	if(finish_run==2){
	  n_res++;
          rootFile->write_res_l_tree_rootFile(elements);
	}
	else if(finish_run==1){	
	  n_fission++;
          rootFile->write_fiss_l_tree_rootFile(elements);
	}
      
      
       
       cout<<"Neutrons: "<<n_neutron<<endl;
       cout<<"Protons: "<<n_proton<<endl;
       cout<<"Alpha: "<<n_alpha<<endl;
       cout<<"Gamma: "<<n_gamma<<endl;
       cout<<"Fission: "<<n_fission<<endl;
       cout<<"Res: "<<n_res<<endl;
       cout<<"nrun "<<i+1<<endl;
       cout<<"finish_run "<<finish_run<<endl<<endl;
    } //for nrun 
    
}//funcion

void dynamic::calculate_nucleus(int _A,int _Z, double Ex_fix){
  my_nucleus =new nucleus(_A, _Z, Ex_fix);
  this->Bn = my_nucleus->CalculateBn(_A, _Z);
  this->Bf = my_nucleus->CalculateBf(_A, _Z, Ex_fix);
  this->An = my_nucleus->CalculateAn(_A, _Z, Ex_fix);
  this->Af = my_nucleus->CalculateAf(_A, _Z, Ex_fix);
  this->Ap = my_nucleus->CalculateAp(_A, _Z, Ex_fix);
  this->Bp = my_nucleus->CalculateBp(_A, _Z, Ex_fix);
  this->Ba = my_nucleus->CalculateBa(_A, _Z, Ex_fix);
  this->Vp = my_nucleus->CalculateVp(_A, _Z, Ex_fix);
  this->Aa = my_nucleus->CalculateAa(_A, _Z, Ex_fix);
  this->Va = my_nucleus->CalculateVa(_A, _Z, Ex_fix);
  delete my_nucleus;
}

void dynamic::dynamic_step(int _A, int _Z, double &_q, double &_t, double Ex_fix, double _l){
     //cout<<_t<<"\t"<<_q<<endl;
     this->beta_l=beta_SPS(_q,0.6,1.2);
     double lamda=0; //cambiar tambien en fission emision
     bool finish_step=false;
     int tt=0;
     double qqq,ttt;

     double S = potStruct.entropy(Ex_fix, _A, _Z, _q , _l);
     double der_S=potStruct.deriv_entropy(Ex_fix, _A, _Z, _q , _l);
     double temp=potStruct.temperature(Ex_fix, _A, _Z, _q , _l);
     double _mass=0.01044*pow(_A,1.66666)*1.2249*1.2249;
     double _coeff=temp/this->beta_l/_mass;
     double tb = 0;
     tb=potStruct.temperature(Ex_fix, _A, _Z, _q+0.0001, _l)/beta_SPS(_q,0.6,1.2);
     tb=tb-potStruct.temperature(Ex_fix, _A, _Z, _q-0.0001, _l)/beta_SPS(_q,0.6,1.2);
     tb=tb/0.0002/_mass*lamda;
     
     while(finish_step==false){     
     
       qqq=_q+_coeff*der_S*step+sqrt(_coeff*step)*gRandom->Gaus(0.0,2.0);      //  +tb*step;
       if(Ex_fix-potStruct.potentialMS(_A,_Z,qqq,_l)>0){
        finish_step=true;
        _q=qqq;
        _t=_t+step;
       }
     //cout<<S<<Ex_fix-potStruct.potentialMS(_A,_Z,_q,_l)<<endl;    
     }
     this->beta_l=beta_SPS(_q,0.6,1.2); //cout<<"beta "<<beta_l<<endl; 
}

double dynamic::beta_SPS(double _q, double _q_neck, double _qsc){
      double beta0 =10;
    double betaSC =30.;
     if(_q <= _q_neck){
         return beta0;
    }else{
        return beta0 + (betaSC - beta0)/(_qsc - _q_neck)*(_q - _q_neck);
     }
}

double dynamic::l_dist(int _A, int _Z, double aap, double zzp, double _E){
  double l=0, lc, vc, ap=19,at, zp=9,zt, dl,lmin=0,lmax=100,dist,x,y;
  //_E=135;
  ap=aap;
  zp=zzp;
  at=_A-ap;
  zt=_Z-zp;
  vc=5./3.*0.7053*zp*zt/(pow(ap,1./3.)+pow(at,1./3.)+1.6);
  cout<<"E: "<<_E<<" vc: "<<vc<<endl; 
   if((_E-vc)<0)
     lc=sqrt(ap*at/_A)*(pow(ap,1./3.)+pow(at,1./3.))*(0.33);
   else if((_E-vc)<120)
    lc=sqrt(ap*at/_A)*(pow(ap,1./3.)+pow(at,1./3.))*(0.33+0.205*sqrt(_E-vc));    
   else
    lc=sqrt(ap*at/_A)*(pow(ap,1./3.)+pow(at,1./3.))*2.5757;    
   if(_E>vc+10)
     dl=pow(ap*at,3./2.)*pow(10,-5)*(1.5+0.02*(_E-vc-10));
   else
     dl=pow(ap*at,3./2.)*pow(10,-5)*(1.5-0.04*(_E-vc-10));
   
   //cout<<_E<<" "<<vc<<" "<<lc<<" "<<dl<<endl;
   x=gRandom->Uniform(100); printf("%.10lf\n",x/100);
   y=gRandom->Uniform(2*lmax+1);
   dist=(2*x+1)/(1+exp((x-lc)/dl));
   l=x;
   while(y>dist){
     
     x=gRandom->Uniform(100);
     y=gRandom->Uniform(2*lmax+1);
     dist=(2*x+1)/(1+exp((x-lc)/dl));
     l=x;
  }
  cout<<"l_dist: "<<l<<" lc: "<<lc<<" dl: "<<dl<<endl; 
  return l;  
}

void dynamic::Emission(int &A, int &Z, double &Ex, double time, double &L, double q, double qsd, double qgs, bool &fission_emission){
    
    double _energy =Ex;
    double _A =A;
    double _Z =Z;
    double _time =time;
    double eps;
    double hbar=0.658217;
    double max[5];
 

    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    int _n_fission =0;
    int _n_gamma=0;
    
    double _qsci = this->qsci;

    elements_rootFiles elements;
    Evap_part_l p_ejected_t;
    p_ejected_t.set_momentum(0.);
    	
    if( _energy-potStruct.potentialMS(_A,_Z,q,L) > 0. && _A > 0 && _Z > 0 ){

        calculate_nucleus(_A,_Z,_energy);
		
        double Gp =0.0;
        double Ga =0.0;
        double Gn =0.0;
	double Gg=0.0;
	double Gf =0.0;
	double Gd=0;
        double G_sum =0.0;
	double Ekv[5]={0,0,0,0,0};
	double Etemp;
	
 	
        Gn=Gamma_descend(_A, _Z, _energy, L, q, 0, Ekv[0]);
	Gp=Gamma_descend(_A, _Z, _energy, L, q, 1, Ekv[1]);
        Ga=Gamma_descend(_A, _Z, _energy, L, q, 2, Ekv[2]);
	Gg=Gamma_descend(_A, _Z, _energy, L, q, 4, Ekv[4]);
	Gf=Gamma_fission(_A, _Z, _energy, L, qsd, qgs, 1.2);


        if(_energy- potStruct.potentialMS(_A,_Z,q,L)- Ba < 0.) Ga =0.;            
        if(_energy- potStruct.potentialMS(_A,_Z,q,L)- Bp < 0.) Gp =0.;           
        if(_energy- potStruct.potentialMS(_A,_Z,q,L)- Bn < 0.) Gn =0.;           
        if(_energy- potStruct.potentialMS(_A,_Z,q,L)- Bf < 0.) Gf =0.;
	if(_energy-potStruct.potentialMS(_A,_Z,q,L)-Va<0) Ga=0;
	
	if(fission_emission==false) Gf =0.;
            
        if( (_A-_Z)<2 || _Z<2 )
            Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
        if( (_A-_Z)<1 )
            Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
        if( _Z<1 )
            Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton
        if(fission_emission==true) cout<<"EMISSION "<<" "<< q <<" "<<Gn<<" "<<Gp<<" "<<Ga<<" "<<Gg<<" "<<Gf<<" "<<fission_emission<<endl;
        G_sum = Gn + Gp + Ga + Gg + Gf;
        
	if(Gn==0.0 && Gg==0.0) {finish_run=2;return;}
	if(G_sum == 0.0) {finish_run=2;return;}
			
        double x = gRandom->Uniform();
	
	if( fission_emission==true || x<G_sum/hbar*step  ){
	  
	  double x = gRandom->Uniform(G_sum); //cout<<"x: "<<x<<endl;
	  
	 if(x<Gn){
	 //  cout<<"Emitio neutron x "<<x<<" Gt "<<G_sum<<endl;
	   
//	   eps =energy_emission(_A, _Z,_energy,L,q, 0, Gn ); 
	   eps=Ekv[0]; //cout<<"en "<<eps<<endl;
	   Etemp=_energy-(this->Bn + eps);
	   if (Etemp-potStruct.potentialMS(_A,_Z,q,L)<0) return;
	   _energy = Etemp;
           _A--;
           _n_neutron++;
	   if(L-1.>0.) L--;
	   p_ejected_t.q_gs=qgs;
	   p_ejected_t.q_sd=qsd;
	   p_ejected_t.q=q;
	   p_ejected_t.p_energy = eps;
      	   p_ejected_t.e_time = _time;
	   p_ejected_t.dir_PZ=0;
	   p_ejected_t.iteration=iteration;
   	   rootFile->write_ntree_t_rootFile(p_ejected_t);
	   
	 }
	 else if(x<(Gn+Gg)){
	 //  cout<<"Emitio gamma x "<<x<<" Gt "<<G_sum<<endl;
	 //  cout<<"EMISSION "<<" "<< q <<" "<<Gn<<" "<<Gp<<" "<<Ga<<" "<<Gg<<" "<<Gf<<endl;
//	   eps =energy_emission(_A, _Z,_energy,L,q, 4, Gg );
	   eps=Ekv[4]; //cout<<"eg "<<eps<<endl;
	   Etemp=_energy-eps;
	   if (Etemp-potStruct.potentialMS(_A,_Z,q,L)<0) return;
	   _energy = Etemp;	   
	   _n_gamma++;
	   if(L-1.>0.) L--;
	   p_ejected_t.q_gs=qgs;
	   p_ejected_t.q_sd=qsd;
	   p_ejected_t.q=q;
           p_ejected_t.p_energy = eps;
      	   p_ejected_t.e_time = _time;
	   p_ejected_t.dir_PZ=0;
	   p_ejected_t.iteration=iteration;
   	   rootFile->write_gtree_t_rootFile(p_ejected_t);
	   
	 }
	 else if(x<(Gn+Gg+Ga)){
	 //  cout<<"Emitio alfa x "<<x<<" Gt "<<G_sum<<endl;
	 //  cout<<"EMISSION "<<" "<< q <<" "<<Gn<<" "<<Gp<<" "<<Ga<<" "<<Gg<<" "<<Gf<<endl;
//	   eps =energy_emission(_A, _Z,_energy,L,q, 2, Ga );
	   eps=Ekv[2]; //cout<<"ea "<<eps<<endl;
	   Etemp=_energy-(this->Ba + eps);
	   if (Etemp-potStruct.potentialMS(_A,_Z,q,L)<0) return;
	   _energy = Etemp;	   
	   _A -= 4;
	   _Z -= 2;
	   _n_alpha++;
	   if(L-2.>0.)  L = L-2;
	   p_ejected_t.q_gs=qgs;
	   p_ejected_t.q_sd=qsd;
	   p_ejected_t.q=q;
	   p_ejected_t.p_energy = eps;
           p_ejected_t.e_time = _time;
	   p_ejected_t.dir_PZ=0;
	   p_ejected_t.iteration=iteration;
   	   rootFile->write_atree_t_rootFile(p_ejected_t);
	   
	 }	 
	 else if(x<(Gn+Gg+Ga+Gp)){
	  // cout<<"Emitio proton x "<<x<<" Gt "<<G_sum<<endl;
	 //  cout<<"EMISSION "<<" "<< q <<" "<<Gn<<" "<<Gp<<" "<<Ga<<" "<<Gg<<" "<<Gf<<endl;
//	   eps =energy_emission(_A, _Z,_energy,L , q, 1, Gp);
	   eps=Ekv[1]; //cout<<"eg "<<eps<<endl;
	   Etemp=_energy-(this->Bp + eps);
	   if (Etemp-potStruct.potentialMS(_A,_Z,q,L)<0) return;
	   _energy = Etemp;	   
	   _A--;
           _Z--;
	   _n_proton++; 
	   if(L-1.>0.)  L--;  
	   p_ejected_t.q_gs=qgs;
	   p_ejected_t.q_sd=qsd;
	   p_ejected_t.q=q;	      
           p_ejected_t.p_energy = eps;
           p_ejected_t.e_time = _time;
	   p_ejected_t.iteration=iteration;
           rootFile->write_ptree_t_rootFile(p_ejected_t); 
	   
	 }
	 else if(x<(Gn+Gg+Ga+Gp+Gf)){
 	   finish_run=1;
	 }
	 else{//Para deuterio
// 	   eps =energy_emission(_A, _Z,_energy,L , q, 1, Gp);
// 	   _energy -= (this->Bp + eps);
// 	   _A=_A-2;
//            _Z--;
// 	   _n_proton++; 
// 	   if(L-1.>0.)  L--;  
// 	   p_ejected_t.q_gs=qgs;
// 	   p_ejected_t.q_sd=qsd;
// 	   p_ejected_t.q=q;	      
//            p_ejected_t.p_energy = eps + this->Vp;
//            p_ejected_t.e_time = _time;
//            rootFile->write_dtree_t_rootFile(p_ejected_t);
	 }

      }
    }
    //            /* actualizar los "observables" */
    n_proton +=_n_proton;
    n_neutron +=_n_neutron;
    n_alpha +=_n_alpha;
    n_fission += _n_fission;
    n_gamma+=_n_gamma;
    
    Ex =_energy;
    A  =_A ;
    Z  =_Z ;
    //cout<<" SALIO EMISSION "<<Ex<<endl;
}


double dynamic::beta_OBD(double q){
  double beta;
  if(q>0.38){
    beta=15.0/pow(q,0.43)+1.0-10.5*pow(q,0.9)+q*q;
  }
  else{
    beta=32-32.21*q;
  }
  
  return beta;
}

double dynamic::energy_emission(int A, int Z, double E,double l,double q, int particle, double G ){
  
  
  double xx=gRandom->Uniform(G);
  
  if (particle<=3){ //el tres es para deuterio
 
  double av,zv,lv,B,V,mv,MAZ,sv,potencial;
  potencial=potStruct.potentialMS(A,Z,q,l);
  //MAZ=CalculateMass(A,Z);
  if (particle == 0){   //neutron
    av=1; lv=1; B=this->Bn; V=0; zv=0; mv=939.566; sv=0.5;
  }
  if (particle == 1){   //proton
    av=1; lv=1; B=this->Bp; V=this->Vp;zv=1; mv=938.272; sv=0.5;
  }
  else if (particle == 2){    //alpha
    av=4; lv=2; B=this->Ba; V=this->Va;zv=2; mv=3727.4; sv=0.0;
  }
  MAZ=CalculateMass(A-av,Z-zv);
  double Ea=E-B-potencial;
  double de=0.05,ev,I=0,cc=0;
  double m_red = mv*MAZ/(MAZ+mv)/90000;   /****  MeV*zs^2/fm^2  ******/
  double h_bar=0.658;
  if(l>=lv) l=l-lv;
  xx=xx/de/((2.0*sv+1.0)*m_red/pow(TMath::Pi()*h_bar,2.0)/potStruct.l_density(l+lv,potStruct.entropy(E, A, Z, q, l+lv)));
  //else return 0;
  
  if(Ea>0){
    for(ev=V;ev<=Ea;ev=ev+de){
      I=I+cc;
      cc=potStruct.l_density(l,potStruct.entropy(Ea-ev+potencial, A-av, Z-zv, q, l))*ev*sigma_inv(A,av,ev,particle,V)/2.0;
      I=I+cc;
      if(I>=xx) return ev;
    }
    //I=I-potStruct.l_density(l,potStruct.entropy(B, A-av, Z-zv, q, l))*Ea*sigma_inv(A,av,Ea,particle,V)/2.0;
    //I=I*de;                                  /**** MeV^2*fm^2 ******/
    //double m_red = mv*MAZ/(MAZ+mv)/90000;   /****  MeV*zs^2/fm^2  ******/
    //double h_bar=0.658;                      /**** MeV*zs  ********/
    //I=I*(2.0*lv+1.0)*m_red/pow(TMath::Pi()*h_bar,2.0)/potStruct.l_density(l+lv,potStruct.entropy(E, A, Z, q, l+lv));
    return 0;
  }
  else return 0;    
      
  }
  else if(particle == 4){ //gamma
  double lv=1,Egp,Egt,Ep=80,Et=80,gammag=5,factor,f;
  double de=0.1,ev,I=0,Constante;
  Egp=Ep/pow(A,1.0/3.0);
  Egt=Et/pow(A,1.0/3.0);
  Constante=4.0/3.0/TMath::Pi()/137.0*1.75/940.0*(A-Z)*Z/double(A)*de*3.0/potStruct.l_density(l,potStruct.entropy(E, A, Z, q, l+1));
  xx=xx/Constante;
  if(l>=lv) l=l-lv;
  double cc=0;
  double Ea=E-potStruct.potentialMS(A,Z,q,l);
  
  for(ev=0;ev<=Ea;ev=ev+de){
    factor=pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egp*Egp)*(ev*ev-Egp*Egp))/3.0;
    factor=factor+2.0*pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egt*Egt)*(ev*ev-Egt*Egt))/3.0;
    f=factor;
    I=I+cc;
    cc=f*potStruct.l_density(l,potStruct.entropy(E-ev, A, Z, q, l))/2.0;
    I=I+cc;
    if(I>=xx) return ev;
  }
    //ev=E;
    //factor=pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egp*Egp)*(ev*ev-Egp*Egp))/3.0;
    //factor=factor+2.0*pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egt*Egt)*(ev*ev-Egt*Egt))/3.0;
    //f=factor;
    //I=I-f*potStruct.l_density(l,potStruct.entropy(E-ev, A, Z, q, l))/2;
  
  //I=I*de*Constante;
  //I=I*Constante;
  return 0;
  
  }
  else{//fission
    
  }
}

double dynamic::sigma_inv (int A, int av, double ev, int neutron, double V){
  int delta=0;
  if (neutron==0) delta=1;
  double Rv=1.21*(pow(A-av,1./3.)+pow(av,1./3.))+3.4*delta/sqrt(ev);
  if(ev>V) return 3.1415*Rv*Rv*(1-V/ev);
  else return 0;
} 

double dynamic::sigmainv_descend(int A, int Z, double ev, int particle){
  
  double inverse;
  
  if (particle < 4){
     double av, lv, B, V, zv, mv,sv,kv, Rv, delta=0;
     double MAZ=CalculateMass(A,Z);
     if (particle == 0){   //neutron
     av=1; lv=1; B=this->Bn; V=0; zv=0; mv=939.566; sv=0.5;delta=1;
     }
     if (particle == 1){   //proton
     av=1; lv=1; B=this->Bp; V=this->Vp;zv=1; mv=938.272; sv=0.5;
     }
     else if (particle == 2){    //alpha
     av=4; lv=2; B=this->Ba; V=this->Va;zv=2; mv=3727.4; sv=0.0;
     } 
     
     Rv=1.21*(pow(A-av,1./3.)+pow(av,1./3.))+3.4*delta/sqrt(ev);
     double mred=mv*MAZ/(mv+MAZ)/90000;
     if(ev>V){
       inverse=(2.0*sv+1.0)*mred*Rv*Rv*(1.0-V/ev)*ev;
     }
     else inverse=0;
     return inverse;     
  }
  
  else if(particle==4){
    double gamconst=3.*(8./3.)/137.0*(1.+0.75)/940.0;
    double lv=1,Egp,Egt,Ep=80,Et=80,gammag=5,factor;
    Egp=Ep/pow(A,1.0/3.0);
    Egt=Et/pow(A,1.0/3.0);
    factor=pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egp*Egp)*(ev*ev-Egp*Egp))/3.0;
    factor=factor+2.0*pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egt*Egt)*(ev*ev-Egt*Egt))/3.0;
    inverse=gamconst*factor*(A-Z)*Z/double(A);
    return inverse;
  }

  return 0; 
}

double dynamic::Gamma_descend(int A, int Z, double E, double l, double q, int particle, double &kinetic){
  double I=0,c,ev,energy,de=0.2,entropy,potencial,Lf,maximo,S0;
  potencial=potStruct.potentialMS(A,Z,q,l);
  energy=E-potencial;
  kinetic=0;
   if (particle < 4){
     double av, lv, B, V, zv, mv,sv,kv, Rv, delta=0;
    
     if (particle == 0){   //neutron
     av=1; lv=1; B=this->Bn; V=0; zv=0; mv=939.566; sv=0.5;delta=1;
     }
     if (particle == 1){   //proton
     av=1; lv=1; B=this->Bp; V=this->Vp;zv=1; mv=938.272; sv=0.5;
     }
     else if (particle == 2){    //alpha
     av=4; lv=2; B=this->Ba; V=this->Va;zv=2; mv=3727.4; sv=0.0;
     } 
     energy=energy-B;
     if(energy<0) return 0;
     if(l>lv) Lf=l-lv;
     else Lf=l;
     c=0;I=0;maximo=0;
     double final=energy;
     if(energy>30) final=30;
     for(ev=V;ev<=final;ev=ev+de){
       c=c+I/2.0;
       entropy=potStruct.entropy(energy+potencial-ev,A-av,Z-zv,q,Lf);
       I=sigmainv_descend(A,Z,ev,particle)*potStruct.l_density(Lf,entropy);
       if(I>maximo) maximo=I;
       c=c+I/2.0;//cout<<"c "<<c;
       //cout<<" sigmainv "<<sigmainv_descend(A,Z,ev,particle)<<" densidad "<<potStruct.l_density(Lf,entropy)<<endl;
     }//cout<<"c "<<c<<endl;
     c=c*de/TMath::Pi()/pow(0.658,2.0);   
     maximo=maximo/TMath::Pi()/pow(0.658,2.0);
     S0=potStruct.entropy(E,A,Z,q,l);
     c=c/potStruct.l_density(l,S0);
     maximo=maximo/potStruct.l_density(l,S0);
     
     double x,y,f;
     do{
     x = gRandom->Uniform(final);
     entropy=potStruct.entropy(energy+potencial-x,A-av,Z-zv,q,Lf);
     f=sigmainv_descend(A,Z,x,particle)*potStruct.l_density(Lf,entropy);
     f=f/TMath::Pi()/pow(0.658,2.0)/potStruct.l_density(l,S0);
     y = gRandom->Uniform(maximo);
     }while(y>f);
     
     kinetic=x;//cout<<" kinetic "<<x<<" energy "<<energy<<endl;
     
       
    
     
     return c;
  }
  
  else if(particle==4){
     c=0;I=0;maximo=0;
     if(l>1) Lf=l-1;
     else Lf=l;
     
     if(energy <0 ) return 0;
     double final=energy;
     if(energy>30) final=30;     
     for(ev=0;ev<=energy;ev=ev+de){
       c=c+I/2.0;
       entropy=potStruct.entropy(E-ev,A,Z,q,Lf);
       I=sigmainv_descend(A,Z,ev,4)*potStruct.l_density(Lf,entropy);
       if(I>maximo) maximo=I;
       c=c+I/2.0;  
     }
     c=c*de;
     S0=potStruct.entropy(E,A,Z,q,l);
     c=c/potStruct.l_density(l,S0);
     maximo=maximo/potStruct.l_density(l,S0);
     
     double x,y,f;
     do{
     x = gRandom->Uniform(final);
     entropy=potStruct.entropy(E-x,A,Z,q,Lf);
     f=sigmainv_descend(A,Z,x,4)*potStruct.l_density(Lf,entropy);
     f=f/potStruct.l_density(l,S0);
     y = gRandom->Uniform(maximo);
     }while(y>f);
     
     kinetic=x;//	cout<<" kinetic "<<x<<" energy "<<energy<<endl;     
     
     return c;     
  }

}

double dynamic::Gamma_particle(int A, int Z, double E, double l, double q, int particle){
  double av,zv,lv,B,V,mv,MAZ,sv,potencial;
  potencial=potStruct.potentialMS(A,Z,q,l);
  //MAZ=CalculateMass(A,Z);
  if (particle == 0){   //neutron
    av=1; lv=1; B=this->Bn; V=0; zv=0; mv=939.566; sv=0.5;
  }
  if (particle == 1){   //proton
    av=1; lv=1; B=this->Bp; V=this->Vp;zv=1; mv=938.272; sv=0.5;
  }
  else if (particle == 2){    //alpha
    av=4; lv=2; B=this->Ba; V=this->Va;zv=2; mv=3727.4; sv=0.0;
  }
  MAZ=CalculateMass(A-av,Z-zv);
  double Ea=E-B-potencial;
  double de=0.05,ev,I=0,cc=0;
  if(l>=lv) l=l-lv;
  //else return 0;
  if(Ea>0){
    for(ev=V;ev<=Ea;ev=ev+de){
      I=I+cc;
      cc=potStruct.l_density(l,potStruct.entropy(Ea-ev+potencial, A-av, Z-zv, q, l))*ev*sigma_inv(A,av,ev,particle,V)/2.0;
      I=I+cc;
    }
    //I=I-potStruct.l_density(l,potStruct.entropy(B, A-av, Z-zv, q, l))*Ea*sigma_inv(A,av,Ea,particle,V)/2.0;
    I=I*de;                                  /**** MeV^2*fm^2 ******/
    double m_red = mv*MAZ/(MAZ+mv)/90000;   /****  MeV*zs^2/fm^2  ******/
    double h_bar=0.658;                      /**** MeV*zs  ********/
    I=I*(2.0*sv+1.0)*m_red/pow(TMath::Pi()*h_bar,2.0)/potStruct.l_density(l+lv,potStruct.entropy(E, A, Z, q, l+lv));
    return I;
  }
  else return 0;
}

double dynamic::Gamma_gamma(int A, int Z, double E, double l, double q){
  double lv=1,Egp,Egt,Ep=80,Et=80,gammag=5,factor,f;
  double de=0.1,ev,I=0,Constante;
  Egp=Ep/pow(A,1.0/3.0);
  Egt=Et/pow(A,1.0/3.0);
  Constante=4.0/3.0/TMath::Pi()/137.0*1.75/940.0*(A-Z)*Z/double(A)*de*3.0/potStruct.l_density(l,potStruct.entropy(E, A, Z, q, l+1));

  if(l>=lv) l=l-lv;
  double cc=0;
  double Ea=E-potStruct.potentialMS(A,Z,q,l);
  
  for(ev=0;ev<=Ea;ev=ev+de){
    factor=pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egp*Egp)*(ev*ev-Egp*Egp))/3.0;
    factor=factor+2.0*pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egt*Egt)*(ev*ev-Egt*Egt))/3.0;
    f=factor;
    I=I+cc;
    cc=f*potStruct.l_density(l,potStruct.entropy(E-ev, A, Z, q, l))/2.0;
    I=I+cc;
  }
    //ev=E;
    //factor=pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egp*Egp)*(ev*ev-Egp*Egp))/3.0;
    //factor=factor+2.0*pow(ev,4.0)*gammag/(pow(ev*gammag,2.0)+(ev*ev-Egt*Egt)*(ev*ev-Egt*Egt))/3.0;
    //f=factor;
    //I=I-f*potStruct.l_density(l,potStruct.entropy(E-ev, A, Z, q, l))/2;
  
  //I=I*de*Constante;
  I=I*Constante;
  return I;
}

double dynamic::Gamma_fission(int A, int Z, double E, double l, double qsd, double qgs, double qsc){
  
  double Sqqsd, Sqqgs,Tgs,Tsd,masa,lamda=0;
  double dq=0.0001,x,i,di=0.01,I=0,h=0.658;  /***** h=Mev*zs  ******/
  double expon;
  //masa=CalculateMass(A,Z); //cout<<"mass "<<masa<<endl;
  masa=0.01044*pow(A,1.66666)*1.2249*1.2249;
  Sqqsd=(potStruct.deriv_entropy(E,A,Z,qsd+dq,l)-potStruct.deriv_entropy(E,A,Z,qsd-dq,l))/dq/2.0;
  Sqqgs=(potStruct.deriv_entropy(E,A,Z,qgs+dq,l)-potStruct.deriv_entropy(E,A,Z,qgs-dq,l))/dq/2.0;
  if(Sqqgs<0) Sqqgs=-Sqqgs;
  if(Sqqsd<0) return 0;
  
  Tgs=potStruct.temperature(E,A,Z,qgs,l);
  Tsd=potStruct.temperature(E,A,Z,qsd,l);
  x=(qsc-qsd)*sqrt(Sqqsd/2.0);  //cout<<" x "<<x<<" ";
  if(x>3) x=3;
  for(i=0.00;i<=x;i=i+di){
    I=I+exp(-i*i);
  }
  
  I=I-0.5-exp(-x*x)/2.0;
  I=I*2.0/sqrt(TMath::Pi())*di;    //I is the error function
  I=1.0/(1.0+I);
  expon=exp(potStruct.entropy(E, A, Z, qsd, l)-potStruct.entropy(E, A, Z, qgs, l));
  if(expon>2) expon=2;
  I=I*h*sqrt(Sqqgs*Sqqsd)*expon;
  I=I*pow(Tgs/beta_SPS(qgs,0.6,1.2),1-lamda)*pow(Tsd/beta_SPS(qsd,0.6,1.2),lamda)/masa/TMath::Pi();
 
  return I;
}


double dynamic::Gamma_p(int A, int Z, double E){
    double Ep = E - this->Bp - this->Vp;
    double En = E - this->Bn;
    if ( (Ep > 0.) && (En > 0.) ) {
        double Kp = Ep / En;
        double x = 2. * (sqrt(this->Ap) * sqrt(Ep) - sqrt(this->An) * sqrt(En));
        return Kp * exp(x);
    }
    else
        return 0.;
}

double dynamic::Gamma_a(int A, int Z, double E, double l, double q){
  double Ea=E-this->Ba;
  double de=0.01,ev,I=0;
  double av=4, lv=2;
  if(l>=lv) l=l-lv;
  if(Ea>0){
    for(ev=0.0;ev<=Ea;ev=ev+de){
      I=I+potStruct.l_density(l,potStruct.entropy(E-ev, A, Z, q, l))*ev*sigma_inv(A,av,ev,0,this->Va);
    }
    I=I-potStruct.l_density(l,potStruct.entropy(this->Ba, A, Z, q, l))*Ea*sigma_inv(A,av,Ea,0,this->Va)/2.0;
    I=I*de;                                  /**** MeV^2*fm^2 ******/
    double m_red = av*A/(A+av)*931.48/90000;   /****  MeV*zs^2/fm^2  ******/
    double h_bar=0.658;                      /**** MeV*zs  ********/
    I=I*(2.0*lv+1.0)*m_red/pow(TMath::Pi()*h_bar,2.0);
    return I;
  }
  else return 0;
}


 double dynamic::Gamma_a(int A, int Z, double E){
     double Ea = E - this->Ba - this->Va;
     double En = E - this->Bn;
     if ( Ea > 0. && En > 0. ) {
         double Ka = 2.0 * Ea / En;
         double x = 2. * (sqrt(this->Aa) * sqrt(Ea) - sqrt(this->An) * sqrt(En));
         return Ka * exp(x);
     }
     else
         return 0.;
 }

double dynamic::Gamma_f(int A, int, double E){
    double Ef = E - this->Bf;
    double En = E - this->Bn;

    if (Ef > 0. && En > 0. ) {
        //		double rf = 1.;
        if( ( this->Af * Ef ) <= 0 ){
            std::cout << "\n" << "Af = " << Af << ", Ef = " << Ef << ", An = " << An << "\n";
            throw std::exception();
        }
        double Kf = 14.39  * this->An * (2. * sqrt( this->Af * Ef) - 1.) / (4.  * this->Af * pow( (double)A, 2./3.) * En );
        //double Kf = 15. * (2. * sqrt( rf * mcef_p.An * Ef) - 1.) / (4. * rf * pow( (double)A, 2./3.) * En );
        double x  =  2. * (sqrt(this->Af) * sqrt(Ef) - sqrt(this->An) * sqrt(En));
        return Kf * exp(x);
    }
    else
        return 0.;
}



double dynamic::boltzmanFunction(double q, double pq, void *param){
    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double fact =(pq*pq/2./mass)/kT;
    return exp(-fact);
}

void dynamic::initial_Condition(double *q, double *pq, void *param){
    /**
     *
     *boltzman distributions of velocities...
     *
     **/

    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double _Z = ((struct paramFunctionStruct *) param)->Z;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double rnd_x =q_eq;

    double eps =1E-8;

    double px_min =-sqrt(-2.*mass*kT*log(eps));
    double px_max =sqrt(-2.*mass*kT*log(eps));

    // std::cout << " mass:= " << mass;
    // std::cout << " kT:= " << kT;
    // std::cout << " px_max:= " << px_max;
    // std::cout << " px_min:= " << px_min;
    //std::cout << std::endl;

    //std::cout << "idum:= " << idum << endl;

    double rnd_px =0.;
    double rnd_y =0.;
    double energy =0.0;

    do{
        rnd_px =px_min + (double)( px_max - px_min )*gRandom->Uniform();
        rnd_y =(double)gRandom->Uniform();

        //std::cout << " rnd_px:= " << rnd_px;
        //std::cout << " rnd_y:= "  << rnd_y;
        //std::cout << std::endl;

    }while(rnd_y >= fabs(boltzmanFunction(rnd_x, rnd_px, param)));

    *q =rnd_x;
    *pq =rnd_px;
}


//double dynamic::beta_OBD(double _q){
//    double beta =0.0;
//    if(_q<=0.38)
//        beta =32.-32.21*_q;

//    else
//        beta =15./pow(_q, 0.43) + 1. - 10.5*pow(_q, 0.9) + _q*_q;


//    if(beta < 0.)
//        beta =0.;
//    return beta;
//}



void dynamic::Langevin(){
    int _A =0;
    int _Z =0;
    double _Ex =0.;

    _A =this->A;
    _Z =this->Z;
    _Ex =this->Ex;

    double _t =0.;
    double _q =0.;
    double _pq =0.;
    double _h =0.;

    n_fission =0;

    bool print_run =false;

    if( Ex > 0){

        for(int i=0; i<nRun; i++){
            //        std::cout << "i:=" << i << " ..." << " of " << nRun << std::endl;

            finish_run =false;
            print_run =false;

            _A =this->A;
            _Z =this->Z;
            _Ex =this->Ex;

            _t =0.;
            _q =0.;
            _pq =0.;
            _h =0.;

            /*preparadndo Langevine*/
            _q =q_eq;   //comienza en el equilibrio
            double temp_energy =0.;
            do{
                initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
                temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
            }while( temp_energy >= _Ex );

            paramFunction.A =_A;
            paramFunction.Z =_Z;
            paramFunction.Exit =Ex;
            paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
            paramFunction.randValue =0.;
            paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
            double beta_coef =beta_OBD(_q);

            //init_file << setw(15) << setprecision(8) << i;
            //init_file << setw(15) << setprecision(8) << _q;
            //init_file << setw(15) << setprecision(8) << _pq;
            //init_file << setw(15) << setprecision(8) << temp_energy;
            //init_file << endl;


            while( !finish_run ){
                /**
             *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
             *puede ser fuera del cilco.. y por tanto en el constructor
             **/

                my_nucleus =new nucleus(_A, _Z, _Ex);
                this->Bn = my_nucleus->CalculateBn(_A, _Z);
                this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);

                this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
                this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
                this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
                this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
                this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
                this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
                this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
                this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);

                delete my_nucleus;

                double minBnBf =0.;

                if(Bn > Bf)
                    minBnBf =Bf;
                else
                    minBnBf =Bn;

                if( _Ex > minBnBf ){

                    /* ...aqui va la dinamica Langevin... */

                    /** Abe article */
                    _h =paramFunction.kT*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) - beta_coef*_pq;
                    _q  +=_pq/paramFunction.mass*step;
                    _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
                    /** End Abe article */

                    //                    /** Gontchar article */
                    //                    _h =paramFunction.kT/beta_coef/paramFunction.mass*step;
                    //                    _q += _h*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) + sqrt(_h)*paramFunction.randValue;
                    //                    _pq =sqrt(paramFunction.kT*paramFunction.mass);
                    //                    /** End Gontchar article*/

                    print_run =true;

                    temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
                    //                    if(temp_energy > _Ex || temp_energy < 0. ){ //not possible
                    if(temp_energy > Ex || temp_energy < 0.){ //not possible
                        finish_run =true;
                        print_run =false;
                        //cout << "temp_energy > _Ex...temp_energy:= " << temp_energy << endl;
                        i--;
                        break;
                    }

                    /** actualizar */
                    _Ex =Ex-temp_energy;
                    paramFunction.randValue=gaus(&idum);
                    paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
                    paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
                    beta_coef =beta_OBD(_q);

                    _t +=step;

                    if( _q > qsci ){ //fission
                        //Nfiss++;
                        n_fission++;
                        finish_run =true;
                    }
                    if(_q < 0.0)
                        cout << "q negative..." << endl;

                    if(_t > tmax )  //not fission
                        finish_run =true;

                    //                    traj_file << setw(15) << setprecision(8) << _t;
                    //                    traj_file << setw(15) << setprecision(8) << _q;
                    //                    traj_file << setw(15) << setprecision(8) << _pq;
                    //                    traj_file << setw(15) << setprecision(8) << _pq*_pq/2./paramFunction.mass;
                    //                    traj_file << setw(15) << setprecision(8) << potStruct.potentialMS(_A, _Z, _q);
                    //                    traj_file << setw(15) << setprecision(8) << potStruct.deriv_entropy(_Ex, _A, _Z, _q);
                    //                    traj_file << setw(15) << setprecision(8) << temp_energy + _Ex;
                    //                    traj_file << setw(15) << setprecision(8) << _Ex;
                    //                    traj_file << setw(15) << setprecision(8) << _h;
                    //                    traj_file << setw(15) << setprecision(8) << sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step);
                    //                    traj_file << setw(15) << setprecision(8) << beta_coef;
                    //                    traj_file << setw(15) << setprecision(8) << paramFunction.mass;
                    //                    traj_file << setw(15) << setprecision(8) << paramFunction.kT;
                    //                    traj_file << endl;

                }
                else{
                    n_res++;
                    finish_run =true;
                }
            }

            /* actualizar los "observables" */
            /* Publicar los resultados de la corrida n_run[i] */

            //out_file << setw(15) << setprecision(8) << i;
            if(print_run){
                out_file << setw(15) << setprecision(8) << _t;
                out_file << setw(15) << setprecision(8) << _q;
                out_file << setw(15) << setprecision(8) << _pq;
                out_file << setw(15) << setprecision(8) << _A;
                out_file << setw(15) << setprecision(8) << _Z;
                out_file << setw(15) << setprecision(8) << _Ex;
                out_file << setw(15) << setprecision(8) << (double)_A/2.;
                out_file << setw(15) << setprecision(8) << i;
                out_file << endl;
            }
        }
    }
}


void dynamic::Statisticall_1(int A, int Z, double Ex){

    int _A =0;
    int _Z =0;

    double _energy =0.;
    double _fissility=0.;
    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    double _n_fission =0.;


    double W = 0.;
    double NF= 1.;

    double eps = 2. ;
    double _energyMax =0.;

    bool boolFission =false;

    bool neutronChannel =false;
    bool protonChannel =false;
    bool alphaChannel =false;

    elements_rootFiles elements;

    //    n_proton =0;
    //    n_neutron =0;
    //    n_alpha =0;
    //    n_fission =0;
    //    n_fissility =0.;

    for(long i=0; i<nRun; i++){

        _energy =Ex;
        _A =A;
        _Z =Z;
        eps = 2.;
        _energyMax =0.;

        W = 0.;
        NF= 1.;
        _n_fission = 0.0;
        _n_proton = _n_neutron = _n_alpha = 0;
        boolFission =false;


        //        double old_energy =_energy;

        //        my_nucleus =new nucleus(_A, _Z, _energy);
        //        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        //        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        //        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        //        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        //        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        //        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        //        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        //        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        //        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        //        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

        //        delete my_nucleus;

        //        double minEnergy =this->Bn;//MIN(this->Bn, this->Bf);

        //while ( _energy > minEnergy && _A > 0 && _Z > 0 && boolFission==false){
        do{

            my_nucleus =new nucleus(_A, _Z, _energy);
            this->Bn = my_nucleus->CalculateBn(_A, _Z);
            this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

            this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
            this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
            this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
            this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
            this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
            this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
            this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
            this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

            delete my_nucleus;

            //            double minEnergy =MIN(this->Bn, this->Bf);
            //            if(minEnergy > _energy){
            // minEnergy =this->Bn;

            /* neutron */
            if(this->Bn < 0.){
                neutronChannel =false;
                //                cout << "err.." << endl;
            }
            else
                neutronChannel =true;

            /* proton */
            if(this->Bp < 0.)
                protonChannel =false;
            else
                protonChannel =true;

            /* alpha */
            if(this->Ba < 0.)
                alphaChannel =false;
            else
                alphaChannel =true;

            double Gf =0.0;
            double Gp =0.0;
            double Ga =0.0;
            double Gn =0.0;
            double G_sum =0.0;
            double p_fission =0.0;

            Gf = this->Gamma_f(_A, _Z, _energy);
            Gp = this->Gamma_p(_A, _Z, _energy);
            Ga = this->Gamma_a(_A, _Z, _energy);
            Gn = 1.0;

            eps = 2.;

            //            if( Ba < 0. || Ba > _energy)
            //                Ga =0.;
            //            if( Bp < 0. || Bp > _energy )
            //                Gp =0.;
            //            if( Bn < 0. || Bn > _energy )
            //                Gn =0.;

            if( Ba < 0.)
                Ga =0.;
            if( Bp < 0.)
                Gp =0.;
            if( Bn < 0.)
                Gn =0.;

            //                        cout  << setw(12) << setprecision(8)  << neutronChannel;
            //                        cout  << setw(12) << setprecision(8)  << protonChannel;
            //                        cout  << setw(12) << setprecision(8)  << alphaChannel;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << this->Bn;
            //                        cout  << setw(12) << setprecision(8)  << this->Bp;
            //                        cout  << setw(12) << setprecision(8)  << this->Ba;
            //                        cout  << setw(12) << setprecision(8)  << this->Bf;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << Gn;
            //                        cout  << setw(12) << setprecision(8)  << Gp;
            //                        cout  << setw(12) << setprecision(8)  << Ga;
            //                        cout  << setw(14) << setprecision(8)  << Gf;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << _A;
            //                        cout  << setw(12) << setprecision(8)  << _Z;
            //                        cout  << setw(12) << setprecision(8)  << _energy;
            //                        cout << endl;


            if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
            if(Bp != Bp)	Gp = 0.0;
            if(Bn != Bn)	Gn = 0.0;
            if(Bf != Bf)	Gf = 0.0;
            if(Gn != Gn)	Gn = 0.0;
            if(Ga != Ga)	Ga = 0.0;
            if(Gp != Gp)	Gp = 0.0;
            if(Gf != Gf)	Gf = 0.0;

            if( (_A-_Z)<2 || _Z<2 )
                Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
            if( (_A-_Z)<1 )
                Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
            if( _Z<1 )
                Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


            G_sum = Gn + Gf + Gp + Ga;
            if(G_sum == 0.0)
                break;
            p_fission = Gf / ( G_sum );

            W += NF * p_fission;
            NF = NF * (1. - p_fission);

            double x = ran2(&idum);

            if ( x <= Gp/G_sum && protonChannel ==true ) { // proton out
                /* testing */
                _energyMax=_energy-this->Bp-this->Vp;
                eps =kinetic_energy(_energyMax, this->Ap, _A, false);
                _energy -= (this->Bp + eps + this->Vp);
                /* end testing */

                //                if( (this->Bp + eps) > _energy ){
                //                    eps = _energy - this->Bp;
                //                    _energy = 0.0;
                //                } else {
                //                    _energy -= (this->Bp + eps);
                //                }
                _A--;
                _Z--;
                _n_proton++;
                //                cout << "proton out:= " << this->Bp + eps << endl;
                //                _energy -= (this->Bp + eps + this->Vp);
                //                                                energy_file << setw(12) << setprecision(10)  << eps;
                //                                                energy_file << endl;
                //                p_energy_file << setw(0) << setprecision(5)  << eps + this->Vp;
                //                p_energy_file << endl;

                rootFile->pro_E->Fill( eps + this->Vp);
            }
            else{
                if ( x <= (Gp + Ga)/G_sum && alphaChannel ==true) { // alpha out
                    /* testing */
                    _energyMax=_energy-this->Ba-this->Va;
                    eps =kinetic_energy(_energyMax, this->Ba, _A, false);
                    _energy -= (this->Ba + eps + this->Va);
                    /* end testing */
                    //                    if( (this->Ba + eps) > _energy ){
                    //                        eps = _energy - this->Ba;
                    //                        _energy = 0.0;
                    //                    } else {
                    //                        _energy -= (this->Ba + eps);
                    //                    }
                    _A -= 4;
                    _Z -= 2;
                    _n_alpha++;
                    //                    cout << "alpha out:= " << this->Ba + eps << endl;
                    //                    a_energy_file << setw(0) << setprecision(5)  << eps + this->Va;
                    //                    a_energy_file << endl;

                    rootFile->alp_E->Fill(eps + this->Va);
                }
                else {
                    if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
                        _n_fission++;
                        _fissility = W;
                        //return _fissility;
                        //return true;
                        //                        return;
                        boolFission =true;
                        //                        cout << "fission out" << endl;
                    }else {
                        if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                            /* testing */
                            bool neutron =true;
                            _energyMax=_energy-this->Bn;
                            if(_energyMax < this->An){
                                break;
                            }
                            eps =kinetic_energy(_energyMax, this->An, _A, neutron);
                            _energy -= (this->Bn + eps);
                            /* end testing */

                            //                            if( (this->Bn + eps) > _energy ){
                            //                                eps = _energy - this->Bn;
                            //                                _energy = 0.0;
                            //                            } else {
                            //                                _energy -= (this->Bn + eps);
                            //                            }

                            _A--;
                            _n_neutron++;
                            //                            cout << "neutron out:= " << this->Bn + eps << endl;
                            //cout << "neutron out:= " << this->Bn<< endl;
                            //                            n_energy_file << setw(0) << setprecision(5)  << eps;
                            //                            n_energy_file << endl;
                            rootFile->neu_E->Fill(eps);
                        }
                    }
                }
            }
            _fissility = W;

            //            cout  << setw(12) << setprecision(10)  << _n_neutron;
            //            cout  << setw(12) << setprecision(10)  << _n_proton;
            //            cout  << setw(12) << setprecision(10)  << _n_alpha;
            //            cout  << setw(12) << setprecision(10)  << _n_fission;
            //            cout  << setw(12) << setprecision(10)  << _A;
            //            cout  << setw(12) << setprecision(10)  << _Z;
            //            cout << endl;

            //cout << "diff:= " << old_energy-_energy << endl;
        }while ( _energy > 0. && _A > 0 && _Z > 0 && boolFission==false);
        //        cout  << setw(12) << setprecision(8)  << "end" << endl;

        //            /* actualizar los "observables" */
        n_proton +=_n_proton;
        n_neutron +=_n_neutron;
        n_alpha +=_n_alpha;
        n_fission +=_n_fission;
        n_fissility +=_fissility;

        /**
         *opcion temporal
         **/
        elements.A =_A;
        elements.Z =_Z;
        elements.energy=_energy;
        elements.fissility=_fissility;
        elements.neutrons =_n_neutron;
        elements.protons =_n_proton;
        elements.alphas=_n_alpha;
        elements.fission=_n_fission;

        rootFile->write_rootFile(elements);

        //        if(observable.CompareTo("fission") == 0){
        //            if( n_fission > 0 ){
        //                fiss.fiss_A = _A;
        //                fiss.fiss_Z = _Z;
        //                fiss.fiss_E = _energy;
        //                fiss.fissility = _fissility;
        //                fiss.fiss_neutron = _n_neutron;
        //                fiss.fiss_proton = _n_proton;
        //                fiss.fiss_alpha = _n_alpha;
        //                Hist->Fill();

        //                //                cout  << setw(-10) << setprecision(0)  << "fiss.???";
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_neutron;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_proton;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_alpha;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fissility;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_A;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_Z;
        //                //                cout << endl;
        //            }
        //        }
        //        if(observable.CompareTo("spallation") == 0){
        //            if( n_fission == 0 ){
        //                spall.spall_A = _A;
        //                spall.spall_Z = _Z;
        //                spall.spall_neutron = _n_neutron;
        //                spall.spall_proton = _n_proton;
        //                spall.spall_alpha = _n_alpha;
        //                //frequencies[_Z][_A] += 1;
        //                Hist->Fill();
        //            }
        //        }

        //Hist->Print();
        // Hist->Scan("fiss_Z:fiss_A");


        //        cout  << setw(-10) << setprecision(0)  << "->";
        //        cout  << setw(12) << setprecision(10)  << _n_neutron;
        //        cout  << setw(12) << setprecision(10)  << _n_proton;
        //        cout  << setw(12) << setprecision(10)  << _n_alpha;
        //        cout  << setw(12) << setprecision(10)  << _n_fission;
        //        cout  << setw(12) << setprecision(10)  << _A;
        //        cout  << setw(12) << setprecision(10)  << _Z;
        //        cout  << setw(12) << setprecision(10)  << A-(_n_proton-_n_neutron-4*_n_alpha);
        //        cout  << setw(12) << setprecision(10)  << Z-(_n_proton-2*_n_alpha);
        //        cout << endl;

        //        cout  << setw(-10) << setprecision(0)  << "##";
        //        cout  << setw(12) << setprecision(10)  << n_neutron;
        //        cout  << setw(12) << setprecision(10)  << n_proton;
        //        cout  << setw(12) << setprecision(10)  << n_alpha;
        //        cout  << setw(12) << setprecision(10)  << n_fission;
        //        cout << endl;
    }
    //    cout  << setw(0) << setprecision(0)  << "#";
    //    cout  << setw(12) << setprecision(10)  << n_neutron;
    //    cout  << setw(12) << setprecision(10)  << n_proton;
    //    cout  << setw(12) << setprecision(10)  << n_alpha;
    //    cout  << setw(12) << setprecision(10)  << n_fission;
    //    cout << endl;

}


/**
 * @ref dostrovsky, paper .... Cesar thesis
 * @return
 */

double dynamic::probability_energy_fun(double Ex, double _x_max, double Ex_max, double aj){
    double exponent =0.;
    exponent =aj*_x_max-sqrt(aj*(Ex_max - Ex));
    return Ex/_x_max*exp(exponent);
}

double dynamic::kinetic_energy(double Ex_max, double aj, int A, bool neutron){

    double _eps =0.;
    double _x_max =0.;
    double GXmax =0;
    double beta = (2.12*pow(A, -2./3.) - 0.05)/(0.76 + 2.2*pow(A, -1./3.));
    double rEx =0.;
    double rProb =0.;
    double prob =0.;

    _x_max =(sqrt(aj*Ex_max+0.25)-0.5)/aj;
    GXmax =2.*_x_max;

    if(Ex_max < aj && neutron == true){
        //if(Ex_max < beta){
        //        return Ex_max;
        return 0.;
    }

    do{
        if(neutron == true){
            do{
                rEx =ran2(&idum)*Ex_max;
            }while( (rEx - beta) < 0.);
        }

        rEx =ran2(&idum)*Ex_max;
        rProb =ran2(&idum);
        if( rEx < GXmax )
            prob =probability_energy_fun(rEx, _x_max, Ex_max, aj);
        else
            prob =(rEx/_x_max)*exp(1. - rEx/_x_max);
    }while(rProb > prob);

    _eps =rEx;
    return _eps;
}


void dynamic::dynamicalEmission(int &A, int &Z, double &En){

    //    std::cout << "dynamical emission ..." << std::endl;

    double _energy =En;
    int _A  =A;
    int _Z  =Z;

    int proton =0;
    int neutron =0;
    int alpha = 0;

    double eps = 2.;

    double Gp =0.0;
    double Ga =0.0;
    double Gn =0.0;
    double G_sum =0.0;

    Gp = this->Gamma_p(_A, _Z, _energy);
    Ga = this->Gamma_a(_A, _Z, _energy);
    Gn = 1.0;

    if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
    if(Bp != Bp)	Gp = 0.0;
    if(Bn != Bn)	Gn = 0.0;
    if(Gn != Gn)	Gn = 0.0;
    if(Ga != Ga)	Ga = 0.0;
    if(Gp != Gp)	Gp = 0.0;

    if( (_A-_Z)<2 || _Z<2 ) // se o nucleo não tem neutons e protons suficientes
        Ga = 0.0;
    if( (_A-_Z)<1 )         // se não tem neutrons suficiente ((A-Z) < 1),
        Gn = 0.0;
    if( _Z<1 )              // se não tem protons suficiente (Z < 1)
        Gp = 0.0;

    G_sum = Gn + Gp + Ga;
    if(G_sum == 0.0){
        return ;
        //        break;
    }

    double x = ran2(&idum);

    if ( x <= Gp/G_sum ) { // proton out
        if( (this->Bp + eps) > _energy ){
            eps = _energy - this->Bp;
            _energy = 0.0;
        } else {
            _energy -= (this->Bp + eps);
        }
        _A--;
        _Z--;
        proton++;
        //        Ndin++;
    }
    else{
        if ( x <= (Gp + Ga)/G_sum ) { // alpha out
            if( (this->Ba + eps) > _energy ){
                eps = _energy - this->Ba;
                _energy = 0.0;
            } else {
                _energy -= (this->Ba + eps);
            }
            _A -= 4;
            _Z -= 2;
            alpha++;
            //            Ndin++;
        }
        else {
            if ( x > ( Gp + Ga )/G_sum ) { // nêutron out
                if( (this->Bn + eps) > _energy ){
                    eps = _energy - this->Bn;
                    _energy = 0.0;
                    //                            cout  << setw(8) << setprecision(10)  << 0 << endl;
                } else {
                    _energy -= (this->Bn + eps);
                }
                _A--;
                neutron++;
                //                Ndin++;
                //                        cout  << setw(8) << setprecision(10)  << this->Bn  << endl;
            }
        }
    }

    A =_A;
    Z =_Z;

    n_proton +=proton;
    n_neutron +=neutron;
    n_alpha +=alpha;
}


bool dynamic::Statisticall_dynamic(int &A, int &Z, double &Ex, double &time, double &L){
    int _A =0;
    int _Z =0;
    double _time =time;

    double _qsci = this->qsci;

    double _energy =0.;
    double _fissility=0.;
    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    double _n_fission =0.;
    int  _n_res  =0;

    double W = 0.;
    double NF= 1.;

    double eps = 2. ;
    double _energyMax =0.;

    bool boolEndStat =false;

    bool neutronChannel =false;
    bool protonChannel =false;
    bool alphaChannel =false;
    bool final_print = true;
    cout << " statistical " << endl; 	
    double t_decay =0.;
    	
	elements_rootFiles elements;
	Evap_part p_ejected;
	Evap_part_l p_ejected_t;
    	p_ejected.set_momentum(0.);
    	p_ejected_t.set_momentum(0.);

  
    _energy =Ex;
    _A =A;
    _Z =Z;
    eps = 2.;
    _energyMax =0.;
    _time =time;

    W = 0.;
    NF= 1.;
    _n_fission = 0.0;
    _n_proton = _n_neutron = _n_alpha = 0;
    boolEndStat =false;
    bool fisionstop = true;
    int retorno = 0;
    _n_res =0;
    int extrares=0;		

    double old_energy =_energy;

    while ( _energy > 1. && _A > 1 && _Z > 1 && boolEndStat==false && fisionstop == false ){
       // retorno = 0;
        my_nucleus =new nucleus(_A, _Z, _energy);
        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);
	delete my_nucleus;

        double minBnBf =0.;
        if(Bn > Bf)
            minBnBf =Bf;
        else
            minBnBf =Bn;

        if( _energy < minBnBf ){
            boolEndStat =true;
            _n_res++;
	    cout << "residuo" << endl;
            return 1;
        }else{

            /* neutron */
            if(this->Bn < 0.){
                neutronChannel =false;
                                cout << "err.." << endl;
            }
            else
                neutronChannel =true;

            /* proton */
            if(this->Bp < 0.)
                protonChannel =false;
            else
                protonChannel =true;

            /* alpha */
            if(this->Ba < 0.)
                alphaChannel =false;
            else
                alphaChannel =true;

            double Gf =0.0;
            double Gp =0.0;
            double Ga =0.0;
            double Gn =0.0;
            double G_sum =0.0;
            double p_fission =0.0;

            Gf = this->Gamma_f(_A, _Z, _energy);
            Gp = this->Gamma_p(_A, _Z, _energy);
            Ga = this->Gamma_a(_A, _Z, _energy);
            Gn = 1.0;

            eps = 2.;


            //            if( Ba < 0. || Ba > _energy)
            //                Ga =0.;
            //            if( Bp < 0. || Bp > _energy )
            //                Gp =0.;
            //            if( Bn < 0. || Bn > _energy )
            //                Gn =0.;

            if( Ba < 0.)
                Ga =0.;
            if( Bp < 0.)
                Gp =0.;
            if( Bn < 0.)
                Gn =0.;

            if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
            if(Bp != Bp)	Gp = 0.0;
            if(Bn != Bn)	Gn = 0.0;
            if(Bf != Bf)	Gf = 0.0;
            if(Gn != Gn)	Gn = 0.0;
            if(Ga != Ga)	Ga = 0.0;
            if(Gp != Gp)	Gp = 0.0;
            if(Gf != Gf)	Gf = 0.0;

            if( (_A-_Z)<2 || _Z<2 )
                Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
            if( (_A-_Z)<1 )
                Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
            if( _Z<1 )
                Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton

          //  cout << Gn << "   Gn  " << Gp << "   Gp  " << Gf << "   Gf  " << Ga << "   Ga  " << endl;
            G_sum = Gn + Gf + Gp + Ga;
            if(G_sum == 0.0)
                break;
            p_fission = Gf / ( G_sum );

            t_decay =1./(G_sum);

            W += NF * p_fission;
            NF = NF * (1. - p_fission);

            double x = ran2(&idum);
          //  cout << "ramdom: " << x << "  Gsum:" <<  G_sum << "  Gp/G_sum" << Gp/G_sum << " (Gp + Ga)/G_sum:" << (Gp + Ga)/G_sum << endl;
	  //  cout << " Gp + Ga + Gf: " << ( Gp + Ga + Gf)/(G_sum ) << endl;	
            if ( x <= Gp/G_sum && protonChannel ==true ) { // proton out
                /* testing */
		cout << "proton out" << endl;
                _energyMax=_energy-this->Bp-this->Vp;
                eps =kinetic_energy(_energyMax, this->Ap, _A, false);
                _energy -= (this->Bp + eps + this->Vp);
                /* end testing */

                //                if( (this->Bp + eps) > _energy ){
                //                    eps = _energy - this->Bp;
                //                    _energy = 0.0;
                //                } else {
                //                    _energy -= (this->Bp + eps);
                //                }
                _A--;
                _Z--;
                _n_proton++;
                //                cout << "proton out:= " << this->Bp + eps << endl;
                //                _energy -= (this->Bp + eps + this->Vp);
                //                                                energy_file << setw(12) << setprecision(10)  << eps;
                //                                                energy_file << endl;
                //                p_energy_file << setw(0) << setprecision(5)  << _time;
                //                p_energy_file << setw(16) << setprecision(5)  << eps + this->Vp;
                //                p_energy_file << endl;
		
		p_ejected_t.p_energy = eps + this->Vp;
                p_ejected_t.e_time = _time;
                rootFile->write_ptree_t_rootFile(p_ejected_t);   
		
		p_ejected.p_energy = eps + this->Vp; 			
		rootFile->write_ptree_rootFile(p_ejected);    
                rootFile->pro_E->Fill( eps + this->Vp);
		 if(L-1.>0.)
		L--;
            }
            else{
                if ( x <= (Gp + Ga)/G_sum && alphaChannel ==true) { // alpha out
                    /* testing */
                   cout << "alfa out" << endl;
                    _energyMax=_energy-this->Ba-this->Va;
                    eps =kinetic_energy(_energyMax, this->Ba, _A, false);
                    _energy -= (this->Ba + eps + this->Va);
                    /* end testing */
                    //                    if( (this->Ba + eps) > _energy ){
                    //                        eps = _energy - this->Ba;
                    //                        _energy = 0.0;
                    //                    } else {
                    //                        _energy -= (this->Ba + eps);
                    //                    }
                    _A -= 4;
                    _Z -= 2;
                    _n_alpha++;
                    //                    cout << "alpha out:= " << this->Ba + eps << endl;
                    //                    a_energy_file << setw(0) << setprecision(5)  << _time;
                    //                    a_energy_file << setw(16) << setprecision(5)  << eps + this->Va;
                    //                    a_energy_file << endl;
                    rootFile->alp_E->Fill(eps + this->Va);
			 if(L-2.>0.)
		L = L -2.;
                }
                else {
                    if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
			cout << "fission out" << endl;
			cout << "fission out" << endl;
			cout << "fission out" << endl;	                        
			
			++retorno;	
			elements.A =_A;
                	elements.Z =_Z;
                	elements.energy=_energy;
                	
                	elements.neutrons =n_neutron;
                	elements.protons =n_proton;
                	elements.alphas=n_alpha;
                	elements.fission=n_fission;
                	rootFile->write_fiss_tree_rootFile(elements);
			elements.q=0.;
			elements.qes=_qsci;
			elements.l=0;	
			rootFile->write_fiss_l_tree_rootFile(elements);
                        break;
                        fisionstop = false;
                       final_print =false;
                        
			
                       
                    }else {
                        if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                            /* testing */
                            bool neutron =true;
			 //   cout << "neutron out" << endl;
                            _energyMax=_energy-this->Bn;
                            if(_energyMax < this->An){
                                break;
                            }
                            eps =kinetic_energy(_energyMax, this->An, _A, neutron);
                            _energy -= (this->Bn + eps);
                            /* end testing */

                            //                            if( (this->Bn + eps) > _energy ){
                            //                                eps = _energy - this->Bn;
                            //                                _energy = 0.0;
                            //                            } else {
                            //                                _energy -= (this->Bn + eps);
                            //                            }

                            _A--;
                            _n_neutron++;
                            //                            cout << "neutron out:= " << this->Bn + eps << endl;
                            //cout << "neutron out:= " << this->Bn<< endl;
                            //                            n_energy_file << setw(0) << setprecision(5)  << time;
                            //                            n_energy_file << setw(16) << setprecision(5)  << eps;
                            //                            n_energy_file << endl;
			
			p_ejected_t.p_energy = eps;
	                p_ejected_t.e_time = _time;
        	        rootFile->write_ntree_t_rootFile(p_ejected_t); 
			
			
			p_ejected.p_energy = eps;                        
			rootFile->write_ntree_rootFile(p_ejected);    
			rootFile->neu_E->Fill(eps);
			 if(L-1.>0.)
				L--;
				cout << " new L " << L << endl;
                        }
                    }
                }
            }
            _fissility = W;
            _time +=t_decay/10000;
	
        }
    cout << _energy << endl;
    }

    //            /* actualizar los "observables" */
    n_proton +=_n_proton;
    n_neutron +=_n_neutron;
    n_alpha +=_n_alpha;
    n_fission +=_n_fission;
    n_fissility +=_fissility;

    Ex =_energy;
    A  =_A ;
    Z  =_Z ;
    time =_time;
    cout << "   Ex  = " << Ex << endl;
    cout << " retorno " << retorno << endl;
 if(retorno < 1)
	return 1; 
  else
   	return 0;
        
}
// void dynamic::Langevin_Statisticall_Copy(){
//    
//     int _A  =0;
//     int _Z  =0;
//     double _Ex =0.;
//     double Ex_copy =0.;
//     double l;
// 
//     Ex_copy =Ex;
//     Int_t counter_neg=0;
//     double _t   =0.;
//     double _q   =0.;
//     double _pq  =0.;
//     double _h   =0.;
// 
//     bool _resNres =false;
//     bool print_run =false;
//     bool print_final = true;	
// 
//     n_fission   =0;
//     n_neutron   =0;
//     n_proton    =0;
//     n_alpha     =0;
//     n_fissility =0.;
//     n_res =0;
// 
//      elements_rootFiles elements;
//     // Evap_part p_ejected;	
// 
//     if( Ex_copy > 0.){
//         
//         for(int i=0; i<nRun; i++){
// 		cout << " _________________________________ " << i << endl;
// 		
// 
// 
//             _A =this->A;
//             _Z =this->Z;
//             _Ex =this->Ex;
//             Ex_copy =this->Ex;
//             l = this->L;
//            //  cout << "L "  << l << endl; 
// 
//             _t =0.;
//             _q =0.;
//             _pq =0.;
//             _h =0.;
// 
//             finish_run =false;
//             print_run =false;
// 
//             /*preparadndo Langevine*/
//             _q =q_eq;   //comienza en el equilibrio
//             double temp_energy =0.;
// 	    
// 	    
// 	    
//             do {
// 	      /*
// 	       pensar en relacionar esos mkomentos con los del ucleo al final de la cascada
// 	       */
//                 initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
//      
// 		temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q, l);
// 		
// //		cout << "energy " << temp_energy << endl;
//  		
//             }  while( temp_energy >= _Ex ); //verifico que sea fisicamente posible...
// 
// 	    
// //	    cout << "salio esta jugada" << endl;
//             paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
//             paramFunction.randValue =0.;
//             paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
//             double beta_coef = this-> beta_l; // beta_OBD(_q);
// 	   //  cout << "beta_coef:= " << beta_coef << endl;
//             //double beta_coef =beta_TAprox(paramFunction.kT);
//             //double beta_coef =beta_SPS(_q, qneck, qsci);
// 
// 	    int counter =0;
// 	    int mcounter2 =0;
// 	    
//             while( !finish_run ){
//                 /**
//              *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
//              *puede ser fuera del cilco.. y por tanto en el constructor
//              **/
// 
//                 _Ex =Ex_copy;
// 
//                 my_nucleus =new nucleus(_A, _Z, _Ex);
//                 this->Bn = my_nucleus->CalculateBn(_A, _Z);
//                 this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);
//                 this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
//                 this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
//                 this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
//                 this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
//                 this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
//                 this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
//                 this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
//                 this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);
//                 delete my_nucleus;
// 
//                 double minBnBf =0.;
//                 if(Bn > Bf)
//                     minBnBf =Bf;
//                 else
//                     minBnBf =Bn;
// 
//                 if( Ex_copy-temp_energy > minBnBf ){ // cambia la comparacion de _Ex a la energia que tiene ahi 
// 
// //                      cout << "min Bn Bf " << minBnBf << "   " << _Ex << endl;
// //                      cout << "copy ex " <<  Ex_copy << endl;
// // 		     cout << "energy temp " <<  temp_energy << endl;		     
// //		     cout << "counter 2" << mcounter2 << endl;
// 		    temp_energy=0;
//                     _h =paramFunction.kT*potStruct.deriv_entropy(Ex_copy, _A,  _Z, _q, l) - beta_coef*_pq;
//                     _q  +=_pq/paramFunction.mass*step;
//                     _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
//                     /** End Abe article */
// 
//                     
// 		    print_final = true;
//                     temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q, l);
// 
//       
//                     if(temp_energy > Ex_copy || temp_energy < 0.){ //not possible
//                         finish_run =true;
//                         print_run =false;
//                         i--;
//                         break;
//                         counter++; 
//                     }
// 		    
// 		    if(counter==10){
// 			finish_run =true;
//                         print_run =false;
//                         i--;
// 			break;
// 		    }
// 		    
// 		    if(mcounter2>100000){
// 			cout << "estoy aqui" << endl;
//                         print_final =true;
// 			mcounter2=0;
// 			finish_run=true;
// 		    }
//                     
//                     /** actualizar */
//                     _Ex =Ex_copy-temp_energy;
// //		    cout << "_Ex " << _Ex << endl;
// 
// 		    
// 		    paramFunction.randValue=gaus(&idum);
//                     paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
//                     paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
//                     //beta_coef =beta_OBD(_q);
//                     //beta_coef =beta_TAprox(paramFunction.kT);
//                     //beta_coef =beta_SPS(_q, qneck, qsci);
//                     //                    cout << "beta_coef:= " << beta_coef << endl;
// 	//	   cout << _t << " " << _q << endl;		
//         //            _t +=step;
// 	//	   cout << _t << endl;	
//  	//	    cout << " Ex " << _Ex << "      step   " << _t << endl;
//                     /* End Abe article */
// 
// 		    mcounter2=mcounter2+1;
//                     if( _q > qsci ){        //fission
//                         
//                         n_fission++;
//                         finish_run =true;
//                         print_run =true;
//                         
//                         elements.A =_A;
//                 	elements.Z =_Z;
//                 	elements.energy=_Ex;
//                 	elements.neutrons =n_neutron;
//                 	elements.protons =n_proton;
//                 	elements.alphas=n_alpha;
//                 	elements.fission=n_fission;
//                 	rootFile->write_fiss_tree_rootFile(elements);
// 			elements.q=_q;
// 			elements.qes=qsci;
// 			elements.l=0;
// 			rootFile->write_fiss_l_tree_rootFile(elements);
// 			break;
//                         print_final = false;
// 			
//                         
//                         
//                     }else{                 //dynamical emission branch test
// 
//                         double Gp = this->Gamma_p(_A, _Z, _Ex);
//                         double Ga = this->Gamma_a(_A, _Z, _Ex);
//                         double Gn =1.;
//                         double shi = gRandom->Uniform();
//                         if( Ba < 0.)
//                             Ga =0.;
//                         if( Bp < 0.)
//                             Gp =0.;
//                         if( Bn < 0.)
//                             Gn =0.;
//                         double tau =1./(Gp + Ga + Gn)*hbar_sz;
//                         //double tau =1./Gn;
// 			print_final = false;
// 
//                         //                        cout << "...here..." << endl;
//                         //                        if(i%DeltaStep){
//                         if( shi < step/tau){ 		//MC dynamical emission if TRUE
//                             double _old_energy =_Ex;
//                             double _en =0.;
//                             dynamicalEmission1(_A, _Z, _Ex, _t, l);
// 			    
//                             _en =_old_energy - _Ex;
//                             Ex_copy -=_en;
//                             //cout << "here... shi:= " << shi << " _en:= " << _en << endl;
//                         }
//                         //                        }
// 
//                         //                        if(_q < 0.0)            //solo para informacion ::: por ahora...
//                         //                            cout << "q negative..." << endl;
// 
//                         if(_t > tmax ){	 //not fission:: statistical branch
// 
//                             //                            double _old_energy =_Ex;
//                             //                            double _en =0.;
// 			    cout << "Statisticall_dynamic" << endl; 
//                             print_final = Statisticall_dynamic(_A, _Z, _Ex, _t, l);
//                            
//                             print_run =true;
// 			    
// 			      if(print_final){
// 			elements.A =_A;
//                 	elements.Z =_Z;
//                 	elements.energy=_Ex;
//                 	//elements.fissility=fissility;
//                 	elements.neutrons =n_neutron;
//                 	elements.protons =n_proton;
//                 	elements.alphas=n_alpha;
//                 	elements.fission=n_fission;
// 		rootFile->write_res_tree_rootFile(elements);
// 			
// 		elements.q=_q;
// 		elements.qes=qsci;
// 		elements.l=0;
// 		rootFile->write_res_l_tree_rootFile(elements);			
// 		finish_run =true;
// 		} 
// 			    
// 			    
// 			    
// 			                            
//                         }
//                     }
// 
//                    
//                 }
//                 else{
//                                        
//                    n_res++;
//                     finish_run =true;
// 		elements.A =_A;
// 		elements.Z =_Z;
// 		elements.energy=_Ex;
//                 elements.neutrons =n_neutron;
//                 elements.protons =n_proton;
//                 elements.alphas=n_alpha;
//                 elements.fission=n_fission;
//                 rootFile->write_res_tree_rootFile(elements);
// 
// 		elements.q=_q;
// 			elements.qes=qsci;
// 			elements.l=0;
// 			rootFile->write_res_l_tree_rootFile(elements);	
// 
// 		 
//                 }
//             }
// 
// 	  	
// 
// 
//             /* Publicar los resultados de la corrida n_run[i] */
//             if(print_run){
//                 //                out_file << setw(15) << setprecision(8) << _t;
//                 //                out_file << setw(15) << setprecision(8) << _q;
//                 //                out_file << setw(15) << setprecision(8) << _pq;
//                 //                out_file << setw(15) << setprecision(8) << _A;
//                 //                out_file << setw(15) << setprecision(8) << _Z;
//                 //                out_file << setw(15) << setprecision(8) << _Ex;
//                 //                out_file << setw(15) << setprecision(8) << (double)_A/2.;
//                 //                out_file << setw(15) << setprecision(8) << i;
//                 //                out_file << endl;
//                 /**
//                  *opcion temporal
//                  **/
//                 
// 		
//             }
//         
//               
//         
//         }
//        
//        
//        
//        
//         
//     }
//     else{
//       counter_neg++;
//     }
//    cout << "Conteos negativos en el run " << counter_neg << endl; 
// }
// 
// 



/*dynamical branch emmision*/
