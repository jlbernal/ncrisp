/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef MCEF_H
#define MCEF_H


#include "nucleus.hh"

struct MCEFL
{
public:
    MCEFL(int A, int Z, int Ex, long *idum);
    ~MCEFL();

    bool statisticalB();
    bool dynamicalSB();

    //        int Get_neutron(){ return n_neutron;}
    //        int Get_proton(){ return n_proton;}
    //        int Get_alpha(){ return n_alpha;}
    //        int Get_fission(){ return n_fission;}
    //        int Get_fissility(){ return _fissility;}

    //        int Get_energy(){ return _energy;}

    int Get_neutron(){ return cumul_neutron;}
    int Get_proton(){ return cumul_proton;}
    int Get_alpha(){ return cumul_alpha;}
    int Get_fission(){ return cumul_fission;}
    double Get_fissility(){ return cumul_fissility;}
    double Get_energy(){ return _energy;}
    
    int Get_Afinal(){ return final_a;}
    int Get_Zfinal(){ return final_z;}
    

    //    double Get_mean_neutron(){ return (double)cumul_neutron/nRun;}
    //    double Get_mean_proton(){ return (double)cumul_proton/nRun;}
    //    double Get_mean_alpha(){ return (double)cumul_alpha/nRun;}
    //    double Get_mean_fission(){ return (double)cumul_fission/nRun;}
    //    double Get_mean_fissility(){ return (double)cumul_fissility/nRun;}

private:

    int A;
    int Z;
    int Ex;
    long nRun;

    nucleus *my_nucleus;

    double An, Bn;
    double Af, Bf;
    double Ap, Bp, Vp;
    double Aa, Ba, Va;

    double _energy;

    int n_fission;
    int n_proton;
    int n_neutron;
    int n_alpha;

    double _fissility;

    int cumul_fission;
    int cumul_proton;
    int cumul_neutron;
    int cumul_alpha;
    double cumul_fissility;
    
    int final_a;
    int final_z; //final configuration

    long *idum;	// "seed" for the random number generators

    /**/

    void init(int A, int Z, double En);

    bool Generate(int A, int Z, double En/*, bool WeissDist*/);
    void Generate1(int A, int Z, double En/*, bool WeissDist*/);
    void FinalConfiguration( const int A, const int Z, int &A_final, int &Z_final);

    double Gamma_p(int A, int Z, double E);
    double Gamma_a(int A, int Z, double E);
    double Gamma_f(int A, int Z, double E);

    inline double final_E() { return _energy; }
    inline double Fissility() { return _fissility; }
    inline int NeutronMultiplicity() { return n_neutron; }
    inline int ProtonMultiplicity() { return n_proton; }
    inline int AlphaMultiplicity() { return n_alpha; }
};

#endif // MCEF_H
