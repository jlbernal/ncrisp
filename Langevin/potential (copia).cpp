/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "potential.h"

/**
 * author:: Ubaldo Bannos Rodriguez
 * status:: not finished
 * date (last update): 05/04/2014
 **/


using namespace std;

double NuclearPotentialStruc::potentialMS(int A, int Z, double q){

    //    return a2*(1.0 - /*k*pow(N-Z,A)*/ k*pow(A-2*Z,pow(A, -1))*pow(A-2*Z,pow(A, -1)))*pow(A, 2.0*pow(3.0 ,-1))*(Bs(q)-1.0)
    //            + c3*Z*Z*pow(A, 2.0*pow(3.0 ,-1))*(Bc(q) - 1.0)
    //            + cr*L*L*pow(A, -5.0*pow(3.0 ,-1))*Br(q, c, h);

    //    printf ("Bs:= %.5e Bc:= %.5e Br:= %.5e \n",Bs(q), Bc(q), Br(q, c, h));

    //    std::cout << q << " " << Bc(q) << std::endl;

    int N =A-Z;

    return  a2*(1.0 - k*(N-Z)/A*(N-Z)/A)*pow(A, 2.0/3.0)*(Bs(q)-1.0) + c3*(Z*Z/pow(A, 1.0/3.0))*(Bc(q) - 1.0)
            /*+ cr*L*L*pow(A,-5.0/3.0)*Br(q, c, h)*/;

    //    return a2*(1.0 - k*pow((A-2*Z)/A,2))*pow(A, 2.0/3.0)*(Bs(q)-1.0) + c3*Z*Z/pow(A, 1.0/3.0)*(Bc(q) - 1.0);
    //    //            + cr*L*L*pow(A,-5.0/3.0)*Br(q, c, h);  // quito la parte angular
}


double NuclearPotentialStruc::Bs(double q){
    double criterium =0.452;
    if( q < criterium )
        //        return 1.0 + 0.4*(64./9.)*pow(q-0.375,2);
        //        return 1.0 + 2.844*(q - 0.375)*(q - 0.375);
        return 1.0 + 0.4*(64./9.)*(q-0.375)*(q-0.375);
    else
        return 0.983 + 0.439*(q-0.375);
}


//fissilityParam
double NuclearPotentialStruc::X(double qsd){
    //std::cout << qsd << " " << 0.05*log(0.875*(pow((qsd - 0.375),-1)-1.0)) + 0.74 << std::endl;
    return 0.05*log(0.875*(pow((qsd - 0.375),-1)-1.0)) + 0.74;
}


double NuclearPotentialStruc::Bf_Essp(double X){
    double criterium =0.6;
    double Y =1.0-X;

    if( X < criterium ){
        //        std::cout << X << " " << 0.2599 - 0.2151*X - 0.1643*X*X - 0.0673*X*X*X << std::endl;
        return 0.2599 - 0.2151*X - 0.1643*X*X - 0.0673*X*X*X;
    }
    else{
        //        std::cout << X << " " << 0.7259*Y*Y*Y - 0.3302*Y*Y*Y*Y + 0.6387*pow(Y,5) + 7.8727*pow(Y,6) - 12.0061*pow(Y,7) << std::endl;
        return 0.7259*Y*Y*Y - 0.3302*Y*Y*Y*Y + 0.6387*pow(Y,5) + 7.8727*pow(Y,6) - 12.0061*pow(Y,7);
    }
}


double NuclearPotentialStruc::Bc(double q){
    double criterium =0.452;
    if( q < criterium ){
        return 1.0 - 0.2*(64./9.)*(q - 0.375)*(q - 0.375);
    }
    else{
        return 1.0 + (1.0 - Bs(q) + Bf_Essp(X(1.2)))/(2.0*X(1.2));

        //        return 1.0 + (1.0 - Bs(q) + Bf_Essp(X(q)))/(2.0*X(q));
        //        return 1.0 + (1.0 - Bs(q) + Bf_Essp(0.83)/(2.0*0.83));
    }
}


//double NuclearPotentialStruc::Br(double q, double c, double h){
//    double criterium =0.375;

//    double Jpar =Jparall(c, h);
//    double Jtra =Jtrans(c, h);

//    if( Jtra < Jpar){
//        if( q > criterium){
//            return pow(Jpar,-1);
//        }
//    }
//    else{
//        return pow(Jtra,-1);
//    }
//}

//double NuclearPotentialStruc::Jtrans(double c, double h){
//    return c*c*(1.0 + pow(c,-3) + 4.0*pow(35.0,-1)*Bsh(c,h)*(2.0*pow(c,3) + 4.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3) - 1.0 ))*0.5;
//}

//double NuclearPotentialStruc::Jparall(double c, double h){
//    return c*c*(pow(c,-3) + 4.0*pow(35.0,-1)*Bsh(c,h)*( 4.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3) - 1.0 ));
//}

//double NuclearPotentialStruc::Bsh(double c, double h){
//    return 2.0*h + (c - 1.0)*0.5;
//}

//double NuclearPotentialStruc::q_func(double c, double h){
//    return 3.0*pow(8.0,-1)*c*(1.0 + 2.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3));
//}

double NuclearPotentialStruc::HelmholtzFEnergy(double Ex, int A, int Z, double q){
    /* variante */
    double potential =potentialMS(A, Z, q);
    return potential - a(q, A)*((Ex - potential)/a(q,A));
}
