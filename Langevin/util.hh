/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef UTIL_H
#define UTIL_H

#include <math.h>

//extern double ran0(long *);
//extern double ran1(long *);
//extern double ran2(long *);
//extern double ran3(long *);
//extern double gaus(long *); /*creates a Gaussian distribution with <u1>=0,<u1**2>=1.;*/

double ran0(long *);
double ran1(long *);
double ran2(long *);
double ran3(long *);

double gaus(long *); /*creates a Gaussian distribution with <u1>=0,<u1**2>=1.;*/

#endif // UTIL_H

