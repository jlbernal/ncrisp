/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef WORK_ROOTFILES_H
#define WORK_ROOTFILES_H

#include "TString.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TVectorD.h"

#include "TObject.h"

#include "TH1.h"


#include "DataHelper.hh"

/**
 * public:
 *            TString fname_out;
              TString observable;
 * 
 */
struct param_rootFiles
{
public:
    TString fname_out;
    TString observable;
};

/**
 * public:
    int A,Z,neutrons,protons,alphas,fission;
    double fissility,energy,q,qes,l;
 * 
 */
struct elements_rootFiles
{
public:
    int A;
    int Z;
    int neutrons;
    int protons;
    int alphas;
    int fission;
    double fissility;
    double energy;
    double q;
    double qes;
    double l;
    int iteration;
    double E0;
    
};

struct work_rootFiles
{

private:
    Fission fiss;
    Spallation spall;

    Fission_l fiss_l;
    Spallation_l spall_l;
	
    Evap_part evapora;
    Evap_part_l evaporation;

    TFile *file;
    TTree *Hist;
    TTree *ntree;
    TTree *ptree;
    TTree *fiss_tree;
    TTree *res_tree;	
    TTree *fiss_l_tree;
    TTree *res_l_tree;	
    param_rootFiles param;
    TTree *ntree_t;
    TTree *ptree_t;
    TTree *gtree_t; 
    TTree *atree_t;
    TTree *dtree_t;


public:
    work_rootFiles();
    ~work_rootFiles();

    /*  */
    void create_rootFile(param_rootFiles names);
    void write_rootFile(elements_rootFiles elements);
    void end_rootFile();
    /* */

    void write_En_rootFile(elements_rootFiles elements);
    void write_ntree_rootFile( Evap_part n_evap);
    void write_ptree_rootFile( Evap_part n_evap);
    void write_fiss_tree_rootFile( elements_rootFiles elements);
    void write_res_tree_rootFile( elements_rootFiles elements);


    void write_fiss_l_tree_rootFile( elements_rootFiles elements);
    void write_res_l_tree_rootFile( elements_rootFiles elements); 	
    
    void write_ntree_t_rootFile( Evap_part_l n_evap);
    void write_ptree_t_rootFile( Evap_part_l n_evap);
    void write_gtree_t_rootFile( Evap_part_l n_evap);
    void write_atree_t_rootFile( Evap_part_l n_evap);
    void write_dtree_t_rootFile( Evap_part_l n_evap);
    	
    TH1D *pro_E;
    TH1D *neu_E;
    TH1D *alp_E;


};

#endif // WORK_ROOTFILES_H
