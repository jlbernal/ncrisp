/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "dampcoeff.hh"

#include "math.h"

double beta_OBD(double _q){
    double beta =0.0;
    if(_q<=0.38)
        beta =32.-32.21*_q;
    else
        beta =15./pow(_q, 0.43) + 1. - 10.5*pow(_q, 0.9) + _q*_q;
    if(beta < 0.)
        beta =0.;
    return beta;
    //return 7.;
    //return 3.5;
}

double beta_OBD1(double _q){
    double beta =0.0;
    if(_q<=0.38)
        beta =32.-32.21*_q;
    else
        beta =15./pow(_q, 0.43) + 1. - 10.5*pow(_q, 0.9) + _q*_q;
    if(beta < 0.)
        beta =0.;
    return beta;
    //return beta*0.25;
    //return 3.5/hbar_sz;
}

double beta_sps(double _q, double _q_neck, double _qsc){
    double beta0 =2.;
    double betaSC =30.;
    if(_q <= _q_neck){
        return beta0;
    }else{
        return beta0 + (betaSC - beta0)/(_qsc - _q_neck)*(_q - _q_neck);
    }
}

double beta_T(double _T, double _T0, double _T1){
    double beta =0.0;
    double beta0T =0.0;
    double k0 =0.;

    if( _T < _T0 ){
        beta =beta0T;
    }
    if ( _T >= _T1 ) {
        beta =beta0T*(1. + k0*(_T1-_T0));
    }
    else{
        beta =beta0T*(1. + k0*(_T-_T0));
    }
    return beta;
}

double beta_TAprox(double _T){
    double beta =1.;
    beta =0.6*_T*_T/(1. + _T*_T/40.);
    if (beta < 0.)
        beta =0.;
    return beta;
}
