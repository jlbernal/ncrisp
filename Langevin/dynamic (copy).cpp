/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "dynamic.h"

using namespace std;

dynamic::dynamic(param_rootFiles param){
    Bn =0.;
    Bf =0.;

    Nres =0;
    Ndin =0;

    nStat =0;
    Nfiss =0;

    finish_run =false;

    tmax =100.*_hbar_sz;

    idum = -(long)time(NULL);

    /* langevin*/
    q_eq =0.375; //posicion de equilibrio
    //        step =0.005;  //paso de tiempo
    //step =0.0005*hbar_sz;  //paso de tiempo
    //step =3E-3;  //paso de tiempo  --> 1fm/c
    step =3E-3;  //paso de tiempo  --> 1fm/c

    DeltaStep =1000.*step;

    //    traj_file.open("trajFile.dat", ios::app | ios::out | ios::binary);
    //    out_file.open("outFile.dat", ios::app | ios::out | ios::binary);
    //    init_file.open("initFile.dat", ios::app | ios::out | ios::binary);
    //    sc_energy_file.open("energyFile.dat", ios::app | ios::out | ios::binary);

    traj_file.open("trajFile.dat",      ios::out | ios::binary);
    out_file.open("outFile.dat",        ios::out | ios::binary);
    init_file.open("initFile.dat",      ios::out | ios::binary);
    sc_energy_file.open("energyFile.dat",  ios::out | ios::binary);

    n_energy_file.open("n_energyFile.dat", ios::out | ios::binary);
    p_energy_file.open("p_energyFile.dat", ios::out | ios::binary);
    a_energy_file.open("a_energyFile.dat", ios::out | ios::binary);

    n_energy_file.setf(ios::right | ios::showpoint | ios::fixed);
    p_energy_file.setf(ios::right | ios::showpoint | ios::fixed);
    a_energy_file.setf(ios::right | ios::showpoint | ios::fixed);

    n_time_file.open("n_timeFile.dat", ios::out | ios::binary);
    p_time_file.open("p_timeFile.dat", ios::out | ios::binary);
    a_time_file.open("a_timeFile.dat", ios::out | ios::binary);

    rootFile =new work_rootFiles();
    rootFile->create_rootFile(param);
}

dynamic::dynamic(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode)
{
    this->mode =mode;

    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;

    this->qsci =qsci;
    this->teq  =teq;
    this->qneck =qneck;

    //    this->Hist =Hist;
    //    this->observable =observable;

    Bn =0.;
    Bf =0.;

    Nres =0;
    Ndin =0;

    nStat =0;
    Nfiss =0;

    finish_run =false;

    tmax =100.*_hbar_sz;

    idum = -(long)time(NULL);

    /* esto no es necesario.. pero me aseguro que la semilla aun sea aun mas semi-aleatoria...*/
    int numb1 =1, numb =100*ran2(&idum);
    for(int i=0; i<numb ; i++)
        numb1 =ran2(&idum);

    /* langevin*/

    q_eq =0.375; //posicion de equilibrio
    //        step =0.005;  //paso de tiempo
    //step =0.0005*hbar_sz;  //paso de tiempo
    //step =3E-3;  //paso de tiempo  --> 1fm/c
    step =3E-3;  //paso de tiempo  --> 1fm/c

    DeltaStep =1000.*step;
}

dynamic::~dynamic(){


    /* ver si estas cosas pueden ir en el destuctor */
    traj_file.close();
    out_file.close();
    init_file.close();
    sc_energy_file.close();

    n_energy_file.close();
    p_energy_file.close();
    a_energy_file.close();

    n_energy_file.close();
    p_energy_file.close();
    a_energy_file.close();

    n_time_file.close();
    p_time_file.close();
    a_time_file.close();

    rootFile->end_rootFile();
    delete rootFile;
}

void dynamic::run(){

    n_proton =0;
    n_neutron =0;
    n_alpha =0;
    n_fission =0;
    n_fissility =0.;
    n_res =0;

    finish_run =false;


    //    traj_file.open("trajFile.dat", ios::app | ios::out | ios::binary);
    //    out_file.open("outFile.dat", ios::app | ios::out | ios::binary);
    //    init_file.open("initFile.dat", ios::app | ios::out | ios::binary);
    //    sc_energy_file.open("energyFile.dat", ios::app | ios::out | ios::binary);

    traj_file.open("trajFile.dat",      ios::out | ios::binary);
    out_file.open("outFile.dat",        ios::out | ios::binary);
    init_file.open("initFile.dat",      ios::out | ios::binary);
    sc_energy_file.open("energyFile.dat",  ios::out | ios::binary);

    n_energy_file.open("n_energyFile.dat", ios::out | ios::binary);
    p_energy_file.open("p_energyFile.dat", ios::out | ios::binary);
    a_energy_file.open("a_energyFile.dat", ios::out | ios::binary);

    n_energy_file.setf(ios::right | ios::showpoint | ios::fixed);
    p_energy_file.setf(ios::right | ios::showpoint | ios::fixed);
    a_energy_file.setf(ios::right | ios::showpoint | ios::fixed);

    n_time_file.open("n_timeFile.dat", ios::out | ios::binary);
    p_time_file.open("p_timeFile.dat", ios::out | ios::binary);
    a_time_file.open("a_timeFile.dat", ios::out | ios::binary);

    if( Ex > 0.0){

        if ( mode < 0){
            std::cout << "dynamical Langevin ..." << std::endl;
            Langevin();
        }
        if( mode == 0 ){
            std::cout << "dynamical Langevin && statisticall emission ..." << std::endl;
            Langevin_Statisticall();
        }
        if( mode > 0 ){
            std::cout << "statisticall emission ..." << std::endl;
            Statisticall_1(this->A, this->Z, this->Ex);
        }
    }
}

void dynamic::run(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode){

    this->mode =mode;

    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;

    this->qsci =qsci;
    this->teq  =teq;
    this->qneck =qneck;

    /**
     *
*/

    n_proton =0;
    n_neutron =0;
    n_alpha =0;
    n_fission =0;
    n_fissility =0.;
    n_res =0;

    finish_run =false;

    //    rootFile->pro_E = new TH1D("prot_E","Kinetic Energy Distribution - proton",40,0,40);
    //    rootFile->neu_E = new TH1D("neut_E","Kinetic Energy Distribution - neutron",40,0,40);
    //    rootFile->alp_E = new TH1D("alph_E","Kinetic Energy Distribution - alpha",40,0,40);

    if( Ex > 0.0){

        std::cout << "running... " << nRun << " trajectories" << std::endl;

        if ( mode < 0){
            std::cout << "dynamical Langevin ..." << std::endl;
            Langevin();
        }
        if( mode == 0 ){
            std::cout << "dynamical Langevin && statisticall emission ..." << std::endl;
            Langevin_Statisticall();
        }
        if( mode > 0 ){
            std::cout << "statisticall emission ..." << std::endl;
            Statisticall_1(this->A, this->Z, this->Ex);
        }
    }
}


/*dynamical branch emmision*/

void dynamic::dynamicalEmission(int &A, int &Z, double &En){

    //    std::cout << "dynamical emission ..." << std::endl;

    double _energy =En;
    int _A  =A;
    int _Z  =Z;

    int proton =0;
    int neutron =0;
    int alpha = 0;

    double eps = 2.;

    double Gp =0.0;
    double Ga =0.0;
    double Gn =0.0;
    double G_sum =0.0;

    Gp = this->Gamma_p(_A, _Z, _energy);
    Ga = this->Gamma_a(_A, _Z, _energy);
    Gn = 1.0;

    if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
    if(Bp != Bp)	Gp = 0.0;
    if(Bn != Bn)	Gn = 0.0;
    if(Gn != Gn)	Gn = 0.0;
    if(Ga != Ga)	Ga = 0.0;
    if(Gp != Gp)	Gp = 0.0;

    if( (_A-_Z)<2 || _Z<2 ) // se o nucleo não tem neutons e protons suficientes
        Ga = 0.0;
    if( (_A-_Z)<1 )         // se não tem neutrons suficiente ((A-Z) < 1),
        Gn = 0.0;
    if( _Z<1 )              // se não tem protons suficiente (Z < 1)
        Gp = 0.0;

    G_sum = Gn + Gp + Ga;
    if(G_sum == 0.0){
        return ;
        //        break;
    }

    double x = ran2(&idum);

    if ( x <= Gp/G_sum ) { // proton out
        if( (this->Bp + eps) > _energy ){
            eps = _energy - this->Bp;
            _energy = 0.0;
        } else {
            _energy -= (this->Bp + eps);
        }
        _A--;
        _Z--;
        proton++;
        //        Ndin++;
    }
    else{
        if ( x <= (Gp + Ga)/G_sum ) { // alpha out
            if( (this->Ba + eps) > _energy ){
                eps = _energy - this->Ba;
                _energy = 0.0;
            } else {
                _energy -= (this->Ba + eps);
            }
            _A -= 4;
            _Z -= 2;
            alpha++;
            //            Ndin++;
        }
        else {
            if ( x > ( Gp + Ga )/G_sum ) { // nêutron out
                if( (this->Bn + eps) > _energy ){
                    eps = _energy - this->Bn;
                    _energy = 0.0;
                    //                            cout  << setw(8) << setprecision(10)  << 0 << endl;
                } else {
                    _energy -= (this->Bn + eps);
                }
                _A--;
                neutron++;
                //                Ndin++;
                //                        cout  << setw(8) << setprecision(10)  << this->Bn  << endl;
            }
        }
    }

    A =_A;
    Z =_Z;

    n_proton +=proton;
    n_neutron +=neutron;
    n_alpha +=alpha;
}

void dynamic::dynamicalEmission1(int &A, int &Z, double &Ex, double time){
    int _A =0;
    int _Z =0;
    double _energy =0.;
    double _time =time;

    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;

    double eps = 2. ;
    double _energyMax =0.;

    bool neutronChannel =false;
    bool protonChannel =false;
    bool alphaChannel =false;

    //    n_proton =0;
    //    n_neutron =0;
    //    n_alpha =0;

    _energy =Ex;
    _A =A;
    _Z =Z;


    _n_proton = _n_neutron = _n_alpha = 0;

    if( _energy > 0. && _A > 0 && _Z > 0 ){

        my_nucleus =new nucleus(_A, _Z, _energy);
        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

        delete my_nucleus;

        /* neutron */
        if(this->Bn < 0.)
            neutronChannel =false;
        else
            neutronChannel =true;

        /* proton */
        if(this->Bp < 0.)
            protonChannel =false;
        else
            protonChannel =true;

        /* alpha */
        if(this->Ba < 0.)
            alphaChannel =false;
        else
            alphaChannel =true;

        double Gp =0.0;
        double Ga =0.0;
        double Gn =0.0;
        double G_sum =0.0;

        Gp = this->Gamma_p(_A, _Z, _energy);
        Ga = this->Gamma_a(_A, _Z, _energy);
        Gn = 1.0;

        eps = 2.;

        if( Ba < 0.)
            Ga =0.;
        if( Bp < 0.)
            Gp =0.;
        if( Bn < 0.)
            Gn =0.;


        if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
        if(Bp != Bp)	Gp = 0.0;
        if(Bn != Bn)	Gn = 0.0;
        if(Gn != Gn)	Gn = 0.0;
        if(Ga != Ga)	Ga = 0.0;
        if(Gp != Gp)	Gp = 0.0;

        if( (_A-_Z)<2 || _Z<2 )
            Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
        if( (_A-_Z)<1 )
            Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
        if( _Z<1 )
            Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


        G_sum = Gn + Gp + Ga;
        if(G_sum == 0.0)
            return;

        double x = ran2(&idum);

        if ( x <= Gp/G_sum && protonChannel ==true ) { // proton out
            /* testing */
            _energyMax=_energy-this->Bp-this->Vp;
            eps =kinetic_energy(_energyMax, this->Ap, _A, false);
            _energy -= (this->Bp + eps + this->Vp);
            /* end testing */
            //            if( (this->Bp + eps) > _energy ){
            //                eps = _energy - this->Bp;
            //                _energy = 0.0;
            //            } else {
            //                _energy -= (this->Bp + eps);
            //            }
            _A--;
            _Z--;
            _n_proton++;
            p_energy_file << setw(0) << setprecision(5)  << _time;
            p_energy_file << setw(16) << setprecision(5)  << eps + this->Vp;
            p_energy_file << endl;
            //            p_time_file   << setw(12) << setprecision(10)  << time;
            //            p_time_file   << endl;
        }
        else{
            if ( x <= (Gp + Ga)/G_sum && alphaChannel ==true) { // alpha out
                /* testing */
                _energyMax=_energy-this->Ba-this->Va;
                eps =kinetic_energy(_energyMax, this->Ba, _A, false);
                _energy -= (this->Ba + eps + this->Va);
                /* end testing */
                //                if( (this->Ba + eps) > _energy ){
                //                    eps = _energy - this->Ba;
                //                    _energy = 0.0;
                //                } else {
                //                    _energy -= (this->Ba + eps);
                //                }
                _A -= 4;
                _Z -= 2;
                _n_alpha++;
                a_energy_file << setw(0) << setprecision(5)  << _time;
                a_energy_file << setw(16) << setprecision(5)  << eps + this->Va;
                a_energy_file << endl;
            }
            else {

                if ( x > ( Gp + Ga )/G_sum ) { // nêutron out
                    /* testing */
                    bool neutron =true;
                    _energyMax=_energy-this->Bn;
                    //                    if(_energyMax < this->An){
                    //                        break;
                    //                    }
                    if(_energyMax > this->An){
                        eps =kinetic_energy(_energyMax, this->An, _A, neutron);
                        _energy -= (this->Bn + eps);
                        /* end testing */
                        //                    if( (this->Bn + eps) > _energy ){
                        //                        eps = _energy - this->Bn;
                        //                        _energy = 0.0;
                        //                    } else {
                        //                        _energy -= (this->Bn + eps);
                        //                    }
                        _A--;
                        _n_neutron++;
                        n_energy_file << setw(0) << setprecision(5)  << _time;
                        n_energy_file << setw(16) << setprecision(5)  << eps;
                        n_energy_file << endl;
                    }
                }
            }
        }
    }

    //            /* actualizar los "observables" */
    n_proton +=_n_proton;
    n_neutron +=_n_neutron;
    n_alpha +=_n_alpha;

    Ex =_energy;
    A  =_A ;
    Z  =_Z ;
}

double dynamic::Gamma_p(int A, int Z, double E){
    double Ep = E - this->Bp - this->Vp;
    double En = E - this->Bn;
    if ( (Ep > 0.) && (En > 0.) ) {
        double Kp = Ep / En;
        double x = 2. * (sqrt(this->Ap) * sqrt(Ep) - sqrt(this->An) * sqrt(En));
        return Kp * exp(x);
    }
    else
        return 0.;
}


double dynamic::Gamma_a(int A, int Z, double E){
    double Ea = E - this->Ba - this->Va;
    double En = E - this->Bn;
    if ( Ea > 0. && En > 0. ) {
        double Ka = 2.0 * Ea / En;
        double x = 2. * (sqrt(this->Aa) * sqrt(Ea) - sqrt(this->An) * sqrt(En));
        return Ka * exp(x);
    }
    else
        return 0.;
}

double dynamic::Gamma_f(int A, int, double E){
    double Ef = E - this->Bf;
    double En = E - this->Bn;

    if (Ef > 0. && En > 0. ) {
        //		double rf = 1.;
        if( ( this->Af * Ef ) <= 0 ){
            std::cout << "\n" << "Af = " << Af << ", Ef = " << Ef << ", An = " << An << "\n";
            throw std::exception();
        }
        double Kf = 14.39  * this->An * (2. * sqrt( this->Af * Ef) - 1.) / (4.  * this->Af * pow( (double)A, 2./3.) * En );
        //double Kf = 15. * (2. * sqrt( rf * mcef_p.An * Ef) - 1.) / (4. * rf * pow( (double)A, 2./3.) * En );
        double x  =  2. * (sqrt(this->Af) * sqrt(Ef) - sqrt(this->An) * sqrt(En));
        return Kf * exp(x);
    }
    else
        return 0.;
}


double dynamic::boltzmanFunction(double q, double pq, void *param){
    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double fact =(pq*pq/2./mass)/kT;
    return exp(-fact);
}

void dynamic::initial_Condition(double *q, double *pq, void *param){
    /**
     *
     *boltzman distributions of velocities...
     *
     **/

    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double _Z = ((struct paramFunctionStruct *) param)->Z;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double rnd_x =q_eq;

    double eps =1E-8;

    double px_min =-sqrt(-2.*mass*kT*log(eps));
    double px_max =sqrt(-2.*mass*kT*log(eps));

    //std::cout << " mass:= " << idum;
    //std::cout << " px_max:= " << px_max;
    //std::cout << " px_min:= " << px_min;
    //std::cout << std::endl;

    //std::cout << "idum:= " << idum << endl;

    double rnd_px =0.;
    double rnd_y =0.;
    double energy =0.0;

    do{
        rnd_px =px_min + (double)( px_max - px_min )*ran2(&idum);
        rnd_y =(double)ran2(&idum);

        //std::cout << " rnd_px:= " << rnd_px;
        //std::cout << " rnd_y:= "  << rnd_y;
        //std::cout << std::endl;

    }while(rnd_y >= fabs(boltzmanFunction(rnd_x, rnd_px, param)));

    *q =rnd_x;
    *pq =rnd_px;
}


//double dynamic::beta_OBD(double _q){
//    double beta =0.0;
//    if(_q<=0.38)
//        beta =32.-32.21*_q;

//    else
//        beta =15./pow(_q, 0.43) + 1. - 10.5*pow(_q, 0.9) + _q*_q;


//    if(beta < 0.)
//        beta =0.;
//    return beta;
//}


void dynamic::Langevin_Statisticall(){
    int _A  =0;
    int _Z  =0;
    double _Ex =0.;
    double Ex_copy =0.;

    Ex_copy =Ex;

    double _t   =0.;
    double _q   =0.;
    double _pq  =0.;
    double _h   =0.;

    bool _resNres =false;
    bool print_run =false;

    n_fission   =0;
    n_neutron   =0;
    n_proton    =0;
    n_alpha     =0;
    n_fissility =0.;
    n_res =0;


    if( Ex_copy > 0.){

        for(int i=0; i<nRun; i++){

            _A =this->A;
            _Z =this->Z;
            _Ex =this->Ex;
            Ex_copy =this->Ex;

            _t =0.;
            _q =0.;
            _pq =0.;
            _h =0.;

            finish_run =false;
            print_run =false;

            /*preparadndo Langevine*/
            _q =q_eq;   //comienza en el equilibrio
            double temp_energy =0.;
            do{
                initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
                temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
            }while( temp_energy >= _Ex );   //verifico que sea fisicamente posible...

            paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
            paramFunction.randValue =0.;
            paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
            double beta_coef =beta_OBD(_q);
            //double beta_coef =beta_TAprox(paramFunction.kT);
            //double beta_coef =beta_sps(_q, qneck, qsci);

            while( !finish_run ){
                /**
             *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
             *puede ser fuera del cilco.. y por tanto en el constructor
             **/

                _Ex =Ex_copy;

                my_nucleus =new nucleus(_A, _Z, _Ex);
                this->Bn = my_nucleus->CalculateBn(_A, _Z);
                this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);

                this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
                this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
                this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
                this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
                this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
                this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
                this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
                this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);

                delete my_nucleus;

                double minBnBf =0.;
                if(Bn > Bf)
                    minBnBf =Bf;
                else
                    minBnBf =Bn;

                if( _Ex > minBnBf ){

                    /* ...aqui va la dinamica Langevin... */
                    /** Abe article */
                    //                    _h =paramFunction.kT*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) - beta_coef*_pq;
                    _h =paramFunction.kT*potStruct.deriv_entropy(Ex_copy, _A,  _Z, _q) - beta_coef*_pq;
                    _q  +=_pq/paramFunction.mass*step;
                    _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
                    /** End Abe article */

                    //                    /** Gontchar article */
                    //                    _h =paramFunction.kT/beta_coef/paramFunction.mass*step;
                    //                    _q += _h*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) + sqrt(_h)*paramFunction.randValue;
                    //                    _pq =sqrt(paramFunction.kT*paramFunction.mass);
                    //                    /** End Gontchar article*/

                    /* ...aqui va la dinamica Langevin... */
                    /* Abe article */

                    print_run =true;

                    temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);

                    //if( temp_energy > _Ex || temp_energy < 0. || fabs(temp_energy)>fabs(Ex_copy) ){ //not possible
                    //if( temp_energy > Ex_copy){
                    //if( temp_energy < 0.){
                    if(temp_energy > Ex_copy || temp_energy < 0.){ //not possible
                        finish_run =true;
                        print_run =false;
                        i--;
                        break;
                    }

                    /** actualizar */
                    _Ex =Ex_copy-temp_energy;

                    paramFunction.randValue=gaus(&idum);
                    paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
                    paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
                    beta_coef =beta_OBD(_q);
                    //beta_coef =beta_TAprox(paramFunction.kT);
                    //beta_coef =beta_sps(_q, qneck, qsci);
                    //                    cout << "beta_coef:= " << beta_coef << endl;

                    _t +=step;
                    /* End Abe article */

                    if( _q > qsci ){        //fission
                        //Nfiss++;
                        n_fission++;
                        finish_run =true;
                        print_run =true;
                    }else{                 //dynamical emission branch

                        double Gp = this->Gamma_p(_A, _Z, _Ex);
                        double Ga = this->Gamma_a(_A, _Z, _Ex);
                        double Gn =1.;
                        double shi = ran2(&idum);
                        if( Ba < 0.)
                            Ga =0.;
                        if( Bp < 0.)
                            Gp =0.;
                        if( Bn < 0.)
                            Gn =0.;
                        //double tau =1./(Gp + Ga + Gn)*hbar_sz;
                        double tau =1./Gn;


                        //                        cout << "...here..." << endl;
                        //                        if(i%DeltaStep){
                        if( shi < step/tau){ 		//MC dynamical emission
                            double _old_energy =_Ex;
                            double _en =0.;
                            dynamicalEmission1(_A, _Z, _Ex, _t);

                            _en =_old_energy - _Ex;
                            Ex_copy -=_en;
                            //cout << "here... shi:= " << shi << " _en:= " << _en << endl;
                        }
                        //                        }

                        //                        if(_q < 0.0)            //solo para informacion ::: por ahora...
                        //                            cout << "q negative..." << endl;

                        if(_t > tmax ){	 //not fission:: statistical branch

                            //                            double _old_energy =_Ex;
                            //                            double _en =0.;

                            Statisticall_dynamic(_A, _Z, _Ex, _t);
                            finish_run =true;
                            print_run =true;

                            //                            _en =_old_energy - _Ex;
                            //                            Ex_copy -=_en;

                            //cout << "after statistical emission..." ;
                            //                            cout << " _A:= " << _A ;
                            //                            cout << " _Z:= " << _Z ;
                            //                            cout << " _Ex:= " << _Ex ;
                            //                            cout << endl;
                        }
                    }

                    traj_file << setw(15) << setprecision(8) << _t;
                    traj_file << setw(15) << setprecision(8) << _q;
                    traj_file << setw(15) << setprecision(8) << _pq;
                    traj_file << setw(15) << setprecision(8) << _pq*_pq/2./paramFunction.mass;
                    traj_file << setw(15) << setprecision(8) << potStruct.potentialMS(_A, _Z, _q);
                    traj_file << setw(15) << setprecision(8) << potStruct.deriv_entropy(_Ex ,_A, _Z, _q)*paramFunction.kT/(paramFunction.mass*beta_coef)*step;
                    traj_file << setw(15) << setprecision(8) << temp_energy + _Ex;
                    traj_file << setw(15) << setprecision(8) << _h;
                    traj_file << setw(15) << setprecision(8) << sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step);
                    traj_file << setw(15) << setprecision(8) << beta_coef;
                    traj_file << setw(15) << setprecision(8) << paramFunction.mass;
                    traj_file << setw(15) << setprecision(8) << paramFunction.kT;
                    traj_file << setw(15) << setprecision(8) << i;
                    traj_file << endl;
                }
                else{
                    //                    cout << "n_res++ sinStatistical i:= " << i << endl;
                    n_res++;
                    finish_run =true;
                }
            }

            /* Publicar los resultados de la corrida n_run[i] */
            if(print_run){
                out_file << setw(15) << setprecision(8) << _t;
                out_file << setw(15) << setprecision(8) << _q;
                out_file << setw(15) << setprecision(8) << _pq;
                out_file << setw(15) << setprecision(8) << _A;
                out_file << setw(15) << setprecision(8) << _Z;
                out_file << setw(15) << setprecision(8) << _Ex;
                out_file << setw(15) << setprecision(8) << (double)_A/2.;
                out_file << setw(15) << setprecision(8) << i;
                out_file << endl;
            }
        }
    }
}

void dynamic::Langevin(){
    int _A =0;
    int _Z =0;
    double _Ex =0.;

    _A =this->A;
    _Z =this->Z;
    _Ex =this->Ex;

    double _t =0.;
    double _q =0.;
    double _pq =0.;
    double _h =0.;

    n_fission =0;

    bool print_run =false;

    if( Ex > 0){

        for(int i=0; i<nRun; i++){
            //        std::cout << "i:=" << i << " ..." << " of " << nRun << std::endl;

            finish_run =false;
            print_run =false;

            _A =this->A;
            _Z =this->Z;
            _Ex =this->Ex;

            _t =0.;
            _q =0.;
            _pq =0.;
            _h =0.;

            /*preparadndo Langevine*/
            _q =q_eq;   //comienza en el equilibrio
            double temp_energy =0.;
            do{
                initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
                temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
            }while( temp_energy >= _Ex );

            paramFunction.A =_A;
            paramFunction.Z =_Z;
            paramFunction.Exit =Ex;
            paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
            paramFunction.randValue =0.;
            paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
            double beta_coef =beta_OBD(_q);

            //init_file << setw(15) << setprecision(8) << i;
            //init_file << setw(15) << setprecision(8) << _q;
            //init_file << setw(15) << setprecision(8) << _pq;
            //init_file << setw(15) << setprecision(8) << temp_energy;
            //init_file << endl;


            while( !finish_run ){
                /**
             *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
             *puede ser fuera del cilco.. y por tanto en el constructor
             **/

                my_nucleus =new nucleus(_A, _Z, _Ex);
                this->Bn = my_nucleus->CalculateBn(_A, _Z);
                this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);

                this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
                this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
                this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
                this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
                this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
                this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
                this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
                this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);

                delete my_nucleus;

                double minBnBf =0.;

                if(Bn > Bf)
                    minBnBf =Bf;
                else
                    minBnBf =Bn;

                if( _Ex > minBnBf ){

                    /* ...aqui va la dinamica Langevin... */

                    /** Abe article */
                    _h =paramFunction.kT*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) - beta_coef*_pq;
                    _q  +=_pq/paramFunction.mass*step;
                    _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
                    /** End Abe article */

                    //                    /** Gontchar article */
                    //                    _h =paramFunction.kT/beta_coef/paramFunction.mass*step;
                    //                    _q += _h*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) + sqrt(_h)*paramFunction.randValue;
                    //                    _pq =sqrt(paramFunction.kT*paramFunction.mass);
                    //                    /** End Gontchar article*/

                    print_run =true;

                    temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
                    //                    if(temp_energy > _Ex || temp_energy < 0. ){ //not possible
                    if(temp_energy > Ex || temp_energy < 0.){ //not possible
                        finish_run =true;
                        print_run =false;
                        //cout << "temp_energy > _Ex...temp_energy:= " << temp_energy << endl;
                        i--;
                        break;
                    }

                    /** actualizar */
                    _Ex =Ex-temp_energy;
                    paramFunction.randValue=gaus(&idum);
                    paramFunction.mass =CalculateMass(_A, _Z)*_MeVcc2MW;  //<-- revizar unidades de la masas...
                    paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
                    beta_coef =beta_OBD(_q);

                    _t +=step;

                    if( _q > qsci ){ //fission
                        //Nfiss++;
                        n_fission++;
                        finish_run =true;
                    }
                    if(_q < 0.0)
                        cout << "q negative..." << endl;

                    if(_t > tmax )  //not fission
                        finish_run =true;

                    //                    traj_file << setw(15) << setprecision(8) << _t;
                    //                    traj_file << setw(15) << setprecision(8) << _q;
                    //                    traj_file << setw(15) << setprecision(8) << _pq;
                    //                    traj_file << setw(15) << setprecision(8) << _pq*_pq/2./paramFunction.mass;
                    //                    traj_file << setw(15) << setprecision(8) << potStruct.potentialMS(_A, _Z, _q);
                    //                    traj_file << setw(15) << setprecision(8) << potStruct.deriv_entropy(_Ex, _A, _Z, _q);
                    //                    traj_file << setw(15) << setprecision(8) << temp_energy + _Ex;
                    //                    traj_file << setw(15) << setprecision(8) << _Ex;
                    //                    traj_file << setw(15) << setprecision(8) << _h;
                    //                    traj_file << setw(15) << setprecision(8) << sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step);
                    //                    traj_file << setw(15) << setprecision(8) << beta_coef;
                    //                    traj_file << setw(15) << setprecision(8) << paramFunction.mass;
                    //                    traj_file << setw(15) << setprecision(8) << paramFunction.kT;
                    //                    traj_file << endl;

                }
                else{
                    n_res++;
                    finish_run =true;
                }
            }

            /* actualizar los "observables" */
            /* Publicar los resultados de la corrida n_run[i] */

            //out_file << setw(15) << setprecision(8) << i;
            if(print_run){
                out_file << setw(15) << setprecision(8) << _t;
                out_file << setw(15) << setprecision(8) << _q;
                out_file << setw(15) << setprecision(8) << _pq;
                out_file << setw(15) << setprecision(8) << _A;
                out_file << setw(15) << setprecision(8) << _Z;
                out_file << setw(15) << setprecision(8) << _Ex;
                out_file << setw(15) << setprecision(8) << (double)_A/2.;
                out_file << setw(15) << setprecision(8) << i;
                out_file << endl;
            }
        }
    }
}

bool dynamic::Statisticall_dynamic(int &A, int &Z, double &Ex, double &time){
    int _A =0;
    int _Z =0;
    double _time =time;

    double _energy =0.;
    double _fissility=0.;
    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    double _n_fission =0.;
    int  _n_res  =0;

    double W = 0.;
    double NF= 1.;

    double eps = 2. ;
    double _energyMax =0.;

    bool boolEndStat =false;

    bool neutronChannel =false;
    bool protonChannel =false;
    bool alphaChannel =false;

    double t_decay =0.;

    _energy =Ex;
    _A =A;
    _Z =Z;
    eps = 2.;
    _energyMax =0.;
    _time =time;

    W = 0.;
    NF= 1.;
    _n_fission = 0.0;
    _n_proton = _n_neutron = _n_alpha = 0;
    boolEndStat =false;

    _n_res =0;


    double old_energy =_energy;

    do{

        my_nucleus =new nucleus(_A, _Z, _energy);
        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

        delete my_nucleus;

        double minBnBf =0.;
        if(Bn > Bf)
            minBnBf =Bf;
        else
            minBnBf =Bn;

        if( _energy < minBnBf ){
            boolEndStat =true;
            _n_res++;
        }else{

            /* neutron */
            if(this->Bn < 0.){
                neutronChannel =false;
                //                cout << "err.." << endl;
            }
            else
                neutronChannel =true;

            /* proton */
            if(this->Bp < 0.)
                protonChannel =false;
            else
                protonChannel =true;

            /* alpha */
            if(this->Ba < 0.)
                alphaChannel =false;
            else
                alphaChannel =true;

            double Gf =0.0;
            double Gp =0.0;
            double Ga =0.0;
            double Gn =0.0;
            double G_sum =0.0;
            double p_fission =0.0;

            Gf = this->Gamma_f(_A, _Z, _energy);
            Gp = this->Gamma_p(_A, _Z, _energy);
            Ga = this->Gamma_a(_A, _Z, _energy);
            Gn = 1.0;

            eps = 2.;


            //            if( Ba < 0. || Ba > _energy)
            //                Ga =0.;
            //            if( Bp < 0. || Bp > _energy )
            //                Gp =0.;
            //            if( Bn < 0. || Bn > _energy )
            //                Gn =0.;

            if( Ba < 0.)
                Ga =0.;
            if( Bp < 0.)
                Gp =0.;
            if( Bn < 0.)
                Gn =0.;

            if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
            if(Bp != Bp)	Gp = 0.0;
            if(Bn != Bn)	Gn = 0.0;
            if(Bf != Bf)	Gf = 0.0;
            if(Gn != Gn)	Gn = 0.0;
            if(Ga != Ga)	Ga = 0.0;
            if(Gp != Gp)	Gp = 0.0;
            if(Gf != Gf)	Gf = 0.0;

            if( (_A-_Z)<2 || _Z<2 )
                Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
            if( (_A-_Z)<1 )
                Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
            if( _Z<1 )
                Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


            G_sum = Gn + Gf + Gp + Ga;
            if(G_sum == 0.0)
                break;
            p_fission = Gf / ( G_sum );

            t_decay =1./(G_sum);

            W += NF * p_fission;
            NF = NF * (1. - p_fission);

            double x = ran2(&idum);

            if ( x <= Gp/G_sum && protonChannel ==true ) { // proton out
                /* testing */
                _energyMax=_energy-this->Bp-this->Vp;
                eps =kinetic_energy(_energyMax, this->Ap, _A, false);
                _energy -= (this->Bp + eps + this->Vp);
                /* end testing */

                //                if( (this->Bp + eps) > _energy ){
                //                    eps = _energy - this->Bp;
                //                    _energy = 0.0;
                //                } else {
                //                    _energy -= (this->Bp + eps);
                //                }
                _A--;
                _Z--;
                _n_proton++;
                //                cout << "proton out:= " << this->Bp + eps << endl;
                //                _energy -= (this->Bp + eps + this->Vp);
                //                                                energy_file << setw(12) << setprecision(10)  << eps;
                //                                                energy_file << endl;
                p_energy_file << setw(0) << setprecision(5)  << _time;
                p_energy_file << setw(16) << setprecision(5)  << eps + this->Vp;
                p_energy_file << endl;
            }
            else{
                if ( x <= (Gp + Ga)/G_sum && alphaChannel ==true) { // alpha out
                    /* testing */
                    _energyMax=_energy-this->Ba-this->Va;
                    eps =kinetic_energy(_energyMax, this->Ba, _A, false);
                    _energy -= (this->Ba + eps + this->Va);
                    /* end testing */
                    //                    if( (this->Ba + eps) > _energy ){
                    //                        eps = _energy - this->Ba;
                    //                        _energy = 0.0;
                    //                    } else {
                    //                        _energy -= (this->Ba + eps);
                    //                    }
                    _A -= 4;
                    _Z -= 2;
                    _n_alpha++;
                    //                    cout << "alpha out:= " << this->Ba + eps << endl;
                    a_energy_file << setw(0) << setprecision(5)  << _time;
                    a_energy_file << setw(16) << setprecision(5)  << eps + this->Va;
                    a_energy_file << endl;
                }
                else {
                    if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
                        _n_fission++;
                        _fissility = W;
                        //return _fissility;
                        //return true;
                        //                        return;
                        boolEndStat =true;
                        //                        cout << "fission out" << endl;
                    }else {
                        if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                            /* testing */
                            bool neutron =true;
                            _energyMax=_energy-this->Bn;
                            if(_energyMax < this->An){
                                break;
                            }
                            eps =kinetic_energy(_energyMax, this->An, _A, neutron);
                            _energy -= (this->Bn + eps);
                            /* end testing */

                            //                            if( (this->Bn + eps) > _energy ){
                            //                                eps = _energy - this->Bn;
                            //                                _energy = 0.0;
                            //                            } else {
                            //                                _energy -= (this->Bn + eps);
                            //                            }

                            _A--;
                            _n_neutron++;
                            //                            cout << "neutron out:= " << this->Bn + eps << endl;
                            //cout << "neutron out:= " << this->Bn<< endl;
                            n_energy_file << setw(0) << setprecision(5)  << time;
                            n_energy_file << setw(16) << setprecision(5)  << eps;
                            n_energy_file << endl;
                        }
                    }
                }
            }
            _fissility = W;
            _time +=t_decay/10000;
        }
    }while ( _energy > 0. && _A > 0 && _Z > 0 && boolEndStat==false);

    //            /* actualizar los "observables" */
    n_proton +=_n_proton;
    n_neutron +=_n_neutron;
    n_alpha +=_n_alpha;
    n_fission +=_n_fission;
    n_fissility +=_fissility;

    Ex =_energy;
    A  =_A ;
    Z  =_Z ;
    time =_time;
}

bool dynamic::Statisticall_dynamic_old(int &A, int &Z, double &Ex){
    int _A =0;
    int _Z =0;

    double _energy =0.;
    double _fissility=0.;
    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    double _n_fission =0.;
    int  _n_res  =0;

    double W = 0.;
    double NF= 1.;

    double eps = 2. ;

    bool boolEndStat =false;

    _energy =Ex;
    _A =A;
    _Z =Z;


    W = 0.;
    NF= 1.;
    _n_fission = 0.0;
    _n_proton = _n_neutron = _n_alpha = 0;
    _n_res =0;
    boolEndStat =false;


    while ( _energy > 0. && _A > 0 && _Z > 0 && boolEndStat==false){

        my_nucleus =new nucleus(_A, _Z, _energy);
        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

        delete my_nucleus;

        double minBnBf =0.;
        if(Bn > Bf)
            minBnBf =Bf;
        else
            minBnBf =Bn;

        if( _energy < minBnBf ){
            //boolFission =false;
            boolEndStat =true;
            _n_res++;
        }else{

            double Gf =0.0;
            double Gp =0.0;
            double Ga =0.0;
            double Gn =0.0;
            double G_sum =0.0;
            double p_fission =0.0;

            Gf = this->Gamma_f(_A, _Z, _energy);
            Gp = this->Gamma_p(_A, _Z, _energy);
            Ga = this->Gamma_a(_A, _Z, _energy);
            Gn = 1.0;


            if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
            if(Bp != Bp)	Gp = 0.0;
            if(Bn != Bn)	Gn = 0.0;
            if(Bf != Bf)	Gf = 0.0;
            if(Gn != Gn)	Gn = 0.0;
            if(Ga != Ga)	Ga = 0.0;
            if(Gp != Gp)	Gp = 0.0;
            if(Gf != Gf)	Gf = 0.0;

            if( (_A-_Z)<2 || _Z<2 )
                Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
            if( (_A-_Z)<1 )
                Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
            if( _Z<1 )
                Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


            G_sum = Gn + Gf + Gp + Ga;
            if(G_sum == 0.0)
                break;
            p_fission = Gf / ( G_sum );

            W += NF * p_fission;
            NF = NF * (1. - p_fission);

            double x = ran2(&idum);

            double old_energy =_energy;

            if ( x <= Gp/G_sum ) { // proton out
                if( (this->Bp + eps) > _energy ){
                    eps = _energy - this->Bp;
                    _energy = 0.0;
                } else {
                    _energy -= (this->Bp + eps);
                }
                _A--;
                _Z--;
                _n_proton++;
            }
            else{
                if ( x <= (Gp + Ga)/G_sum ) { // alpha out
                    if( (this->Ba + eps) > _energy ){
                        eps = _energy - this->Ba;
                        _energy = 0.0;
                    } else {
                        _energy -= (this->Ba + eps);
                    }
                    _A -= 4;
                    _Z -= 2;
                    _n_alpha++;
                }
                else {
                    if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
                        _n_fission++;
                        _fissility = W;
                        //return _fissility;
                        //return true;
                        //                        return;
                        boolEndStat =true;
                    }else {
                        if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                            if( (this->Bn + eps) > _energy ){
                                eps = _energy - this->Bn;
                                _energy = 0.0;
                            } else {
                                _energy -= (this->Bn + eps);
                            }
                            _A--;
                            _n_neutron++;
                        }
                    }
                }
            }
            _fissility = W;
            //            cout << "diff:= " << old_energy-_energy << endl;
        }
    }
    //            /* actualizar los "observables" */
    n_proton +=_n_proton;
    n_neutron +=_n_neutron;
    n_alpha +=_n_alpha;
    n_fission +=_n_fission;
    n_fissility +=_fissility;
    n_res +=_n_res;

    Ex =_energy;
    A  =_A ;
    Z  =_Z ;
}

void dynamic::Statisticall_1(int A, int Z, double Ex){

    int _A =0;
    int _Z =0;

    double _energy =0.;
    double _fissility=0.;
    int _n_proton  =0;
    int _n_neutron =0;
    int  _n_alpha  =0;
    double _n_fission =0.;

    double W = 0.;
    double NF= 1.;

    double eps = 2. ;
    double _energyMax =0.;

    bool boolFission =false;

    bool neutronChannel =false;
    bool protonChannel =false;
    bool alphaChannel =false;

    elements_rootFiles elements;

    //    n_proton =0;
    //    n_neutron =0;
    //    n_alpha =0;
    //    n_fission =0;
    //    n_fissility =0.;

    for(long i=0; i<nRun; i++){

        _energy =Ex;
        _A =A;
        _Z =Z;
        eps = 2.;
        _energyMax =0.;

        W = 0.;
        NF= 1.;
        _n_fission = 0.0;
        _n_proton = _n_neutron = _n_alpha = 0;
        boolFission =false;


        //        double old_energy =_energy;

        //        my_nucleus =new nucleus(_A, _Z, _energy);
        //        this->Bn = my_nucleus->CalculateBn(_A, _Z);
        //        this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

        //        this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
        //        this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
        //        this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
        //        this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
        //        this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
        //        this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
        //        this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
        //        this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

        //        delete my_nucleus;

        //        double minEnergy =this->Bn;//MIN(this->Bn, this->Bf);

        //while ( _energy > minEnergy && _A > 0 && _Z > 0 && boolFission==false){
        do{

            my_nucleus =new nucleus(_A, _Z, _energy);
            this->Bn = my_nucleus->CalculateBn(_A, _Z);
            this->Bf = my_nucleus->CalculateBf(_A, _Z, _energy);

            this->An = my_nucleus->CalculateAn(_A, _Z, _energy);
            this->Af = my_nucleus->CalculateAf(_A, _Z, _energy);
            this->Ap = my_nucleus->CalculateAp(_A, _Z, _energy);
            this->Bp = my_nucleus->CalculateBp(_A, _Z, _energy);
            this->Ba = my_nucleus->CalculateBa(_A, _Z, _energy);
            this->Vp = my_nucleus->CalculateVp(_A, _Z, _energy);
            this->Aa = my_nucleus->CalculateAa(_A, _Z, _energy);
            this->Va = my_nucleus->CalculateVa(_A, _Z, _energy);

            delete my_nucleus;

            //            double minEnergy =MIN(this->Bn, this->Bf);
            //            if(minEnergy > _energy){
            // minEnergy =this->Bn;

            /* neutron */
            if(this->Bn < 0.){
                neutronChannel =false;
                //                cout << "err.." << endl;
            }
            else
                neutronChannel =true;

            /* proton */
            if(this->Bp < 0.)
                protonChannel =false;
            else
                protonChannel =true;

            /* alpha */
            if(this->Ba < 0.)
                alphaChannel =false;
            else
                alphaChannel =true;

            double Gf =0.0;
            double Gp =0.0;
            double Ga =0.0;
            double Gn =0.0;
            double G_sum =0.0;
            double p_fission =0.0;

            Gf = this->Gamma_f(_A, _Z, _energy);
            Gp = this->Gamma_p(_A, _Z, _energy);
            Ga = this->Gamma_a(_A, _Z, _energy);
            Gn = 1.0;

            eps = 2.;

            //            if( Ba < 0. || Ba > _energy)
            //                Ga =0.;
            //            if( Bp < 0. || Bp > _energy )
            //                Gp =0.;
            //            if( Bn < 0. || Bn > _energy )
            //                Gn =0.;

            if( Ba < 0.)
                Ga =0.;
            if( Bp < 0.)
                Gp =0.;
            if( Bn < 0.)
                Gn =0.;

            //                        cout  << setw(12) << setprecision(8)  << neutronChannel;
            //                        cout  << setw(12) << setprecision(8)  << protonChannel;
            //                        cout  << setw(12) << setprecision(8)  << alphaChannel;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << this->Bn;
            //                        cout  << setw(12) << setprecision(8)  << this->Bp;
            //                        cout  << setw(12) << setprecision(8)  << this->Ba;
            //                        cout  << setw(12) << setprecision(8)  << this->Bf;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << Gn;
            //                        cout  << setw(12) << setprecision(8)  << Gp;
            //                        cout  << setw(12) << setprecision(8)  << Ga;
            //                        cout  << setw(14) << setprecision(8)  << Gf;
            //                        cout << endl;
            //                        cout  << setw(12) << setprecision(8)  << _A;
            //                        cout  << setw(12) << setprecision(8)  << _Z;
            //                        cout  << setw(12) << setprecision(8)  << _energy;
            //                        cout << endl;


            if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
            if(Bp != Bp)	Gp = 0.0;
            if(Bn != Bn)	Gn = 0.0;
            if(Bf != Bf)	Gf = 0.0;
            if(Gn != Gn)	Gn = 0.0;
            if(Ga != Ga)	Ga = 0.0;
            if(Gp != Gp)	Gp = 0.0;
            if(Gf != Gf)	Gf = 0.0;

            if( (_A-_Z)<2 || _Z<2 )
                Ga = 0.0; //se o nucleo não tem neutons e protons suficientes ((A-Z)<2 || Z<2 ) entao é impossivel sair um alfa
            if( (_A-_Z)<1 )
                Gn = 0.0; // se não tem neutrons suficiente ((A-Z) < 1), entao eh impossivel sair um neuton
            if( _Z<1 )
                Gp = 0.0; // se não tem protons suficiente (Z < 1), entao eh impossivel sair um proton


            G_sum = Gn + Gf + Gp + Ga;
            if(G_sum == 0.0)
                break;
            p_fission = Gf / ( G_sum );

            W += NF * p_fission;
            NF = NF * (1. - p_fission);

            double x = ran2(&idum);

            if ( x <= Gp/G_sum && protonChannel ==true ) { // proton out
                /* testing */
                _energyMax=_energy-this->Bp-this->Vp;
                eps =kinetic_energy(_energyMax, this->Ap, _A, false);
                _energy -= (this->Bp + eps + this->Vp);
                /* end testing */

                //                if( (this->Bp + eps) > _energy ){
                //                    eps = _energy - this->Bp;
                //                    _energy = 0.0;
                //                } else {
                //                    _energy -= (this->Bp + eps);
                //                }
                _A--;
                _Z--;
                _n_proton++;
                //                cout << "proton out:= " << this->Bp + eps << endl;
                //                _energy -= (this->Bp + eps + this->Vp);
                //                                                energy_file << setw(12) << setprecision(10)  << eps;
                //                                                energy_file << endl;
                //                p_energy_file << setw(0) << setprecision(5)  << eps + this->Vp;
                //                p_energy_file << endl;

                rootFile->pro_E->Fill( eps + this->Vp);
            }
            else{
                if ( x <= (Gp + Ga)/G_sum && alphaChannel ==true) { // alpha out
                    /* testing */
                    _energyMax=_energy-this->Ba-this->Va;
                    eps =kinetic_energy(_energyMax, this->Ba, _A, false);
                    _energy -= (this->Ba + eps + this->Va);
                    /* end testing */
                    //                    if( (this->Ba + eps) > _energy ){
                    //                        eps = _energy - this->Ba;
                    //                        _energy = 0.0;
                    //                    } else {
                    //                        _energy -= (this->Ba + eps);
                    //                    }
                    _A -= 4;
                    _Z -= 2;
                    _n_alpha++;
                    //                    cout << "alpha out:= " << this->Ba + eps << endl;
                    //                    a_energy_file << setw(0) << setprecision(5)  << eps + this->Va;
                    //                    a_energy_file << endl;

                    rootFile->alp_E->Fill(eps + this->Va);
                }
                else {
                    if ( x <= ( Gp + Ga + Gf)/G_sum ) { // fissão
                        _n_fission++;
                        _fissility = W;
                        //return _fissility;
                        //return true;
                        //                        return;
                        boolFission =true;
                        //                        cout << "fission out" << endl;
                    }else {
                        if ( x > ( Gp + Ga + Gf)/G_sum ) { // nêutron out
                            /* testing */
                            bool neutron =true;
                            _energyMax=_energy-this->Bn;
                            if(_energyMax < this->An){
                                break;
                            }
                            eps =kinetic_energy(_energyMax, this->An, _A, neutron);
                            _energy -= (this->Bn + eps);
                            /* end testing */

                            //                            if( (this->Bn + eps) > _energy ){
                            //                                eps = _energy - this->Bn;
                            //                                _energy = 0.0;
                            //                            } else {
                            //                                _energy -= (this->Bn + eps);
                            //                            }

                            _A--;
                            _n_neutron++;
                            //                            cout << "neutron out:= " << this->Bn + eps << endl;
                            //cout << "neutron out:= " << this->Bn<< endl;
                            //                            n_energy_file << setw(0) << setprecision(5)  << eps;
                            //                            n_energy_file << endl;
                            rootFile->neu_E->Fill(eps);
                        }
                    }
                }
            }
            _fissility = W;

            //            cout  << setw(12) << setprecision(10)  << _n_neutron;
            //            cout  << setw(12) << setprecision(10)  << _n_proton;
            //            cout  << setw(12) << setprecision(10)  << _n_alpha;
            //            cout  << setw(12) << setprecision(10)  << _n_fission;
            //            cout  << setw(12) << setprecision(10)  << _A;
            //            cout  << setw(12) << setprecision(10)  << _Z;
            //            cout << endl;

            //cout << "diff:= " << old_energy-_energy << endl;
        }while ( _energy > 0. && _A > 0 && _Z > 0 && boolFission==false);
        //        cout  << setw(12) << setprecision(8)  << "end" << endl;

        //            /* actualizar los "observables" */
        n_proton +=_n_proton;
        n_neutron +=_n_neutron;
        n_alpha +=_n_alpha;
        n_fission +=_n_fission;
        n_fissility +=_fissility;

        /**
         *opcion temporal
         **/
        elements.A =_A;
        elements.Z =_Z;
        elements.energy=_energy;
        elements.fissility=_fissility;
        elements.neutrons =_n_neutron;
        elements.protons =_n_proton;
        elements.alphas=_n_alpha;
        elements.fission=_n_fission;

        rootFile->write_rootFile(elements);

        //        if(observable.CompareTo("fission") == 0){
        //            if( n_fission > 0 ){
        //                fiss.fiss_A = _A;
        //                fiss.fiss_Z = _Z;
        //                fiss.fiss_E = _energy;
        //                fiss.fissility = _fissility;
        //                fiss.fiss_neutron = _n_neutron;
        //                fiss.fiss_proton = _n_proton;
        //                fiss.fiss_alpha = _n_alpha;
        //                Hist->Fill();

        //                //                cout  << setw(-10) << setprecision(0)  << "fiss.???";
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_neutron;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_proton;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_alpha;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fissility;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_A;
        //                //                cout  << setw(12) << setprecision(10)  << fiss.fiss_Z;
        //                //                cout << endl;
        //            }
        //        }
        //        if(observable.CompareTo("spallation") == 0){
        //            if( n_fission == 0 ){
        //                spall.spall_A = _A;
        //                spall.spall_Z = _Z;
        //                spall.spall_neutron = _n_neutron;
        //                spall.spall_proton = _n_proton;
        //                spall.spall_alpha = _n_alpha;
        //                //frequencies[_Z][_A] += 1;
        //                Hist->Fill();
        //            }
        //        }

        //Hist->Print();
        // Hist->Scan("fiss_Z:fiss_A");


        //        cout  << setw(-10) << setprecision(0)  << "->";
        //        cout  << setw(12) << setprecision(10)  << _n_neutron;
        //        cout  << setw(12) << setprecision(10)  << _n_proton;
        //        cout  << setw(12) << setprecision(10)  << _n_alpha;
        //        cout  << setw(12) << setprecision(10)  << _n_fission;
        //        cout  << setw(12) << setprecision(10)  << _A;
        //        cout  << setw(12) << setprecision(10)  << _Z;
        //        cout  << setw(12) << setprecision(10)  << A-(_n_proton-_n_neutron-4*_n_alpha);
        //        cout  << setw(12) << setprecision(10)  << Z-(_n_proton-2*_n_alpha);
        //        cout << endl;

        //        cout  << setw(-10) << setprecision(0)  << "##";
        //        cout  << setw(12) << setprecision(10)  << n_neutron;
        //        cout  << setw(12) << setprecision(10)  << n_proton;
        //        cout  << setw(12) << setprecision(10)  << n_alpha;
        //        cout  << setw(12) << setprecision(10)  << n_fission;
        //        cout << endl;
    }
    //    cout  << setw(0) << setprecision(0)  << "#";
    //    cout  << setw(12) << setprecision(10)  << n_neutron;
    //    cout  << setw(12) << setprecision(10)  << n_proton;
    //    cout  << setw(12) << setprecision(10)  << n_alpha;
    //    cout  << setw(12) << setprecision(10)  << n_fission;
    //    cout << endl;

}


/**
 * @ref dostrovsky, paper .... Cesar thesis
 * @return
 */

double dynamic::probability_energy_fun(double Ex, double _x_max, double Ex_max, double aj){
    double exponent =0.;
    exponent =aj*_x_max-sqrt(aj*(Ex_max - Ex));
    return Ex/_x_max*exp(exponent);
}

double dynamic::kinetic_energy(double Ex_max, double aj, int A, bool neutron){

    double _eps =0.;
    double _x_max =0.;
    double GXmax =0;
    double beta = (2.12*pow(A, -2./3.) - 0.05)/(0.76 + 2.2*pow(A, -1./3.));
    double rEx =0.;
    double rProb =0.;
    double prob =0.;

    _x_max =(sqrt(aj*Ex_max+0.25)-0.5)/aj;
    GXmax =2.*_x_max;

    if(Ex_max < aj && neutron == true){
        //if(Ex_max < beta){
        //        return Ex_max;
        return 0.;
    }

    do{
        if(neutron == true){
            do{
                rEx =ran2(&idum)*Ex_max;
            }while( (rEx - beta) < 0.);
        }

        rEx =ran2(&idum)*Ex_max;
        rProb =ran2(&idum);
        if( rEx < GXmax )
            prob =probability_energy_fun(rEx, _x_max, Ex_max, aj);
        else
            prob =(rEx/_x_max)*exp(1. - rEx/_x_max);
    }while(rProb > prob);

    _eps =rEx;
    return _eps;
}
