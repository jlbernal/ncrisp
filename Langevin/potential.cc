/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "potential.hh"

/**
 * author:: Ubaldo Bannos Rodriguez && Drow
 * status:: PHD students
 * date (last update): 05/04/2014
 **/


using namespace std;

double NuclearPotentialStruc::potentialMS(int A, int Z, double q){

    //    return a2*(1.0 - /*k*pow(N-Z,A)*/ k*pow(A-2*Z,pow(A, -1))*pow(A-2*Z,pow(A, -1)))*pow(A, 2.0*pow(3.0 ,-1))*(Bs(q)-1.0)
    //            + c3*Z*Z*pow(A, 2.0*pow(3.0 ,-1))*(Bc(q) - 1.0)
    //            + cr*L*L*pow(A, -5.0*pow(3.0 ,-1))*Br(q, c, h);

    //    printf ("Bs:= %.5e Bc:= %.5e Br:= %.5e \n",Bs(q), Bc(q), Br(q, c, h));

    //    std::cout << q << " " << Bc(q) << std::endl;

    int N =A-Z;

    return  a2*(1.0 - k*(N-Z)/A*(N-Z)/A)*pow(A, 2.0/3.0)*(Bs(q)-1.0) + c3*(Z*Z/pow(A, 1.0/3.0))*(Bc(q) - 1.0)
            /*+ cr*L*L*pow(A,-5.0/3.0)*Br(q, c, h)*/;

    /**
             ****** Abe article ***********
             * */
    //double condition =1.27;
    //if( ( 0.0 < q ) && (q < condition))
    //return 37.46*(q - 1.)*(q - 1.);
    //if( q > condition )
    //return 8.0-18.73*(q - 1.8)*(q - 1.8);
    //if( ( q < 4.0 ) && (q > condition))
    //return 0.0;

    /**
             **********************
             * */

    //    return a2*(1.0 - k*pow((A-2*Z)/A,2))*pow(A, 2.0/3.0)*(Bs(q)-1.0) + c3*Z*Z/pow(A, 1.0/3.0)*(Bc(q) - 1.0);
    //    //            + cr*L*L*pow(A,-5.0/3.0)*Br(q, c, h);  // quito la parte angular

    //~ double condition =1.27;
    //~ if( ( 0.0 < q ) && (q < condition))
    //~ return 37.46*(q - 1.)*(q - 1.);
    //~ if( q > condition )
    //~ return 8.0-18.73*(q- 1.8)*(q - 1.8);
    //~ if( ( q < 4.0 ) && (q > condition))
    //~ return 0.0;


}
double NuclearPotentialStruc::potentialMS(int A, int Z, double q, double L){
   // cout<<"Bs "<<Bs(0.05)<<" Bc "<<Bc(0.05)<<endl;
    int N =A-Z;
    //if(q>0.450 && q <0.455)
      //printf("%lf %.6lf %.6lf\n",q,Bs(q),Bc(q));
    //cout <<q<<" "<< Bs(q) << " \t" << Bc(q) << "\t" <<Br(q) << endl;
    return  a2*(1.0 - k*(N-Z)/A*(N-Z)/A)*pow(A, 2.0/3.0)*(Bs(q)-1.0) + c3*(Z*Z/pow(A, 1.0/3.0))*(Bc(q) - 1.0)
            + cr*L*L*pow(A,-5.0/3.0)*Br(q);

}

double NuclearPotentialStruc::Bs(double q){
    double criterium =0.452;
    
    if( q < criterium )
        //        return 1.0 + 0.4*(64./9.)*pow(q-0.375,2);
        //        return 1.0 + 2.844*(q - 0.375)*(q - 0.375);
        
        return 0.99994 + 0.4*(64./9.)*(q-0.375)*(q-0.375);
    else
        return 0.983 + 0.439*(q-0.375);
}


//fissilityParam
double NuclearPotentialStruc::X(double qsd){
    //std::cout << qsd << " " << 0.05*log(0.875*(pow((qsd - 0.375),-1)-1.0)) + 0.74 << std::endl;
    return 0.05*log(0.875*(pow((qsd - 0.375),-1)-1.0)) + 0.74;
}


double NuclearPotentialStruc::Bf_Essp(double X){
    double criterium =0.6;
    double Y =1.0-X;

    if( X < criterium ){
        return 0.2599 - 0.2151*X - 0.1643*X*X - 0.0673*X*X*X;
    }
    else{
        return 0.7259*Y*Y*Y - 0.3302*Y*Y*Y*Y + 0.6387*pow(Y,5) + 7.8727*pow(Y,6) - 12.0061*pow(Y,7);
    }
}


double NuclearPotentialStruc::Bc(double q){
    double criterium =0.452;
    if( q < criterium ){
        return 0.99983 - 0.2*(64./9.)*(q - 0.375)*(q - 0.375);
    }
    else{
        //return 1.0 + (1.0 - Bs(q) + Bf_Essp(X(1.2)))/(2.0*X(1.2));
        return 1.0 + (1.0 - Bs(q) + Bf_Essp(X(q)))/(2.0*X(q));

        //        return 1.0 + (1.0 - Bs(q) + Bf_Essp(X(q)))/(2.0*X(q));
        //        return 1.0 + (1.0 - Bs(q) + Bf_Essp(0.83)/(2.0*0.83));
    }
}


double NuclearPotentialStruc::Br(double q){

    double criterium =0.375;
    double c = 0.0146 + 2.1935*q + 3.3895*q*q - 7.9124*q*q*q + 5.7135*q*q*q*q - 1.4342*q*q*q*q*q;
    double h = Calc_h(c); 	
	
    //return 0.;
    //if(q==0.) return 20;             //ESTO LO PUSE PARA QUE NO SE INDEFINA, ESTE PUNTO NUNCA APARECE
    double Jpar =Jparall(c, h);
    double Jtra =Jtrans(c, h);
    
    //if(q==0.) return 20;
    if( Jtra < Jpar){
        if( q > criterium){
            return pow(Jpar,-1);
        }
    }
    else{
        return pow(Jtra,-1);
    }
}

double NuclearPotentialStruc::Calc_h(double c){
double c_ = c -1.;

if(c_ < 0 )
	return -0.07*c_*c_;
else
	return c_/4.;



}


double NuclearPotentialStruc::Jtrans(double c, double h){
    return c*c*(1.0 + pow(c,-3) + 4.0*pow(35.0,-1)*Bsh(c,h)*(2.0*pow(c,3) + 4.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3) - 1.0 ))*0.5;
}

double NuclearPotentialStruc::Jparall(double c, double h){
    return c*c*(pow(c,-3) + 4.0*pow(35.0,-1)*Bsh(c,h)*( 4.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3) - 1.0 ));
}

double NuclearPotentialStruc::Bsh(double c, double h){
    return 2.0*h + (c - 1.0)*0.5;
}

double NuclearPotentialStruc::q_func(double c, double h){
    return 3.0*pow(8.0,-1)*c*(1.0 + 2.0*pow(15.0,-1)*Bsh(c,h)*pow(c,3));
}

double NuclearPotentialStruc::HelmholtzFEnergy(double Ex, int A, int Z, double q){
    /* variante */
    double potential =potentialMS(A, Z, q);
    return potential - a(q, A)*((Ex - potential)/a(q,A));
}

double NuclearPotentialStruc::HelmholtzFEnergy(double Ex, int A, int Z, double q, double L){
    /* variante */
    double potential =potentialMS(A, Z, q, L);
    return potential - a(q, A)*((Ex - potential)/a(q,A));
}



/**
 * Calculate Force
 **/



double potential_prot(double q, void *param){
    int A = ((struct paramFunctionPot *) param)->A;
    int Z = ((struct paramFunctionPot *) param)->Z;
    NuclearPotentialStruc potentialStruct_fun;
    return potentialStruct_fun.potentialMS(A, Z, q);
}

double potential_prot1(double q, void *param){
    int A = ((struct paramFunctionPot1 *) param)->A;
    int Z = ((struct paramFunctionPot1 *) param)->Z;
    double L = ((struct paramFunctionPot1 *) param)->L;

    NuclearPotentialStruc potentialStruct_fun;
    return potentialStruct_fun.potentialMS(A, Z, q, L);
}


double NuclearPotentialStruc::force(int A, int Z, double q){

    paramFunctionPot param;
    param.A = A;
    param.Z	= Z;
    
    function func;
    func.function =&potential_prot;
    func.params =&param;

    double result2, abserr_round2;
    deriv_forward(&func, q, 4E-6, &result2, &abserr_round2);

    return result2;
};


double NuclearPotentialStruc::force(int A, int Z, double q, double L){

    paramFunctionPot1 param;
    param.A = A;
    param.Z	= Z;
    param.L	= L;
    
    function func;
    func.function =&potential_prot1;
    func.params =&param;

    double result2, abserr_round2;
    deriv_forward(&func, q, 4E-6, &result2, &abserr_round2);

    return result2;
};




/**
 * Calculate derivEntropy
 **/

double  entropy_prot(double q, void *param){
    int A = ((struct paramFunctionPot *) param)->A;
    int Z = ((struct paramFunctionPot *) param)->Z;
    double E = ((struct paramFunctionPot *) param)->E;
    NuclearPotentialStruc potentialStruct_fun;
    return potentialStruct_fun.entropy(E, A, Z, q);
}

double  entropy_prot1(double q, void *param ){
    int A = ((struct paramFunctionPot1 *) param)->A;
    int Z = ((struct paramFunctionPot1 *) param)->Z;
    double E = ((struct paramFunctionPot1 *) param)->E;
    double L = ((struct paramFunctionPot1 *) param)->L;
    NuclearPotentialStruc potentialStruct_fun;
    return potentialStruct_fun.entropy(E, A, Z, q, L);
}


double NuclearPotentialStruc::deriv_entropy(double E, int A, int Z, double q){

    paramFunctionPot param;
    param.A = A;
    param.Z	= Z;
    param.E	= E;
    
    function func;
    func.function =&entropy_prot;
    func.params =&param;

    double result2, abserr_round2;
    deriv_forward(&func, q, 4E-6, &result2, &abserr_round2);
    deriv_backward(&func, q, 4E-6, &result2, &abserr_round2);

    return result2;
};

double NuclearPotentialStruc::deriv_entropy(double E, int A, int Z, double q, double L){

    paramFunctionPot1 param;
    param.A = A;
    param.Z= Z;
    param.E= E;
    param.L= L;
    
    function func;
    func.function =&entropy_prot1;
    func.params =&param;

    double result2, abserr_round2;
    deriv_forward(&func, q, 1E-8, &result2, &abserr_round2);
    //deriv_backward(&func, q, 4E-8, &result2, &abserr_round2);

    return result2;
};

