/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "dynamic.h"


using namespace std;

dynamic::dynamic(int A, int Z, double Ex, int nRun, double qsci, double teq, int mode)
{

    this->mode =mode;

    this->A =A;
    this->Z =Z;
    this->Ex =Ex;
    this->nRun =nRun;

    this->qsci =qsci;
    this->teq  =teq;

    Bn =0.;
    Bf =0.;

    Nres =0;
    Ndin =0;

    nStat =0;
    Nfiss =0;

    finish_run =false;

    tmax =500.;

    idum = -(long)time(NULL);

    /* esto no es necesario.. pero me aseguro que la semilla aun sea aun mas semi-aleatoria...*/
    int numb1 =1, numb =100*ran2(&idum);
    for(int i=0; i<numb ; i++)
        numb1 =ran2(&idum);

    /* langevin*/

    q_eq =0.375; //posicion de equilibrio
    step =0.005;  //paso de tiempo
    //step =0.01;  //paso de tiempo
    //step =1E-6;  //paso de tiempo
}

void dynamic::run(){
    //    double _t =0.;
    //    double _q =0.;
    //    double _pq =0.;
    //    double _bathEnergy =0;

    //    double _h =0.;



    //    int _A_copy =0;
    //    int _Z_copy =0;
    //    double _Ex_copy =0.;

    n_proton =0;
    n_neutron =0;
    n_alpha =0;
    n_fission =0;
    n_fissility =0.;

    finish_run =false;


    traj_file.open("trajFile.dat", ios::app | ios::out | ios::binary);
    out_file.open("outFile.dat", ios::app | ios::out | ios::binary);
    init_file.open("initFile.dat", ios::app | ios::out | ios::binary);

    if( Ex > 0.0){

        std::cout << "running..." << std::endl;

        if ( mode < 0){
            std::cout << "dynamical Langevin ..." << std::endl;
            Langevin();
        }
        if( mode == 0 ){
            std::cout << "dynamical Langevin && statisticall emission ..." << std::endl;
            Langevin_Statisticall();
        }
        if( mode > 0 ){
            std::cout << "statisticall emission ..." << std::endl;
            Statisticall();
        }
    }
}


/*dynamical branch emmision*/

void dynamic::dynamicalEmission(int &A, int &Z, double &En){

    //    std::cout << "dynamical emission ..." << std::endl;

    double _energy =En;
    int _A  =A;
    int _Z  =Z;

    int proton =0;
    int neutron =0;
    int alpha = 0;

    double eps = 2. ;

    double Gp =0.0;
    double Ga =0.0;
    double Gn =0.0;
    double G_sum =0.0;

    Gp = this->Gamma_p(_A, _Z, _energy);
    Ga = this->Gamma_a(_A, _Z, _energy);
    Gn = 1.0;

    if(Ba != Ba)	Ga = 0.0;	// erro no calculo do Ba
    if(Bp != Bp)	Gp = 0.0;
    if(Bn != Bn)	Gn = 0.0;
    if(Gn != Gn)	Gn = 0.0;
    if(Ga != Ga)	Ga = 0.0;
    if(Gp != Gp)	Gp = 0.0;

    if( (_A-_Z)<2 || _Z<2 ) // se o nucleo não tem neutons e protons suficientes
        Ga = 0.0;
    if( (_A-_Z)<1 )         // se não tem neutrons suficiente ((A-Z) < 1),
        Gn = 0.0;
    if( _Z<1 )              // se não tem protons suficiente (Z < 1)
        Gp = 0.0;

    G_sum = Gn + Gp + Ga;
    if(G_sum == 0.0){
        return ;
        //        break;
    }

    double x = ran2(&idum);

    if ( x <= Gp/G_sum ) { // proton out
        if( (this->Bp + eps) > _energy ){
            eps = _energy - this->Bp;
            _energy = 0.0;
        } else {
            _energy -= (this->Bp + eps);
        }
        _A--;
        _Z--;
        proton++;
        //        Ndin++;
    }
    else{
        if ( x <= (Gp + Ga)/G_sum ) { // alpha out
            if( (this->Ba + eps) > _energy ){
                eps = _energy - this->Ba;
                _energy = 0.0;
            } else {
                _energy -= (this->Ba + eps);
            }
            _A -= 4;
            _Z -= 2;
            alpha++;
            //            Ndin++;
        }
        else {
            if ( x > ( Gp + Ga )/G_sum ) { // nêutron out
                if( (this->Bn + eps) > _energy ){
                    eps = _energy - this->Bn;
                    _energy = 0.0;
                    //                            cout  << setw(8) << setprecision(10)  << 0 << endl;
                } else {
                    _energy -= (this->Bn + eps);
                }
                _A--;
                neutron++;
                //                Ndin++;
                //                        cout  << setw(8) << setprecision(10)  << this->Bn  << endl;
            }
        }
    }

    A =_A;
    Z =_Z;

    n_proton +=proton;
    n_neutron +=neutron;
    n_alpha +=alpha;
}

double dynamic::Gamma_p(int A, int Z, double E){
    double Ep = E - this->Bp - this->Vp;
    double En = E - this->Bn;
    if ( (Ep > 0.) && (En > 0.) ) {
        double Kp = Ep / En;
        double x = 2. * (sqrt(this->Ap) * sqrt(Ep) - sqrt(this->An) * sqrt(En));
        return Kp * exp(x);
    }
    return 0.;
}


double dynamic::Gamma_a(int A, int Z, double E){
    double Ea = E - this->Ba - this->Va;
    double En = E - this->Bn;
    if ( Ea > 0. && En > 0. ) {
        double Ka = 2.0 * Ea / En;
        double x = 2. * (sqrt(this->Aa) * sqrt(Ea) - sqrt(this->An) * sqrt(En));
        return Ka * exp(x);
    }
    return 0.;
}

double dynamic::Gamma_f(int A, int, double E){
    double Ef = E - this->Bf;
    double En = E - this->Bn;

    if (Ef > 0. && En > 0. ) {
        //		double rf = 1.;
        if( ( this->Af * Ef ) <= 0 ){
            std::cout << "\n" << "Af = " << Af << ", Ef = " << Ef << ", An = " << An << "\n";
            throw std::exception();
        }
        double Kf = 14.39  * this->An * (2. * sqrt( this->Af * Ef) - 1.) / (4.  * this->Af * pow( (double)A, 2./3.) * En );
        //double Kf = 15. * (2. * sqrt( rf * mcef_p.An * Ef) - 1.) / (4. * rf * pow( (double)A, 2./3.) * En );
        double x  =  2. * (sqrt(this->Af) * sqrt(Ef) - sqrt(this->An) * sqrt(En));
        return Kf * exp(x);
    }
    return 0.;
}


double dynamic::boltzmanFunction(double q, double pq, void *param){
    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double fact =(pq*pq/2./mass)/kT;
    return exp(-fact/kT);
}

void dynamic::initial_Condition(double *q, double *pq, void *param){
    /**
     *
     *boltzman distributions of velocities...
     *
     **/

    double mass = ((struct paramFunctionStruct *) param)->mass;
    double _A = ((struct paramFunctionStruct *) param)->A;
    double _Z = ((struct paramFunctionStruct *) param)->Z;
    double kT = ((struct paramFunctionStruct *) param)->kT;

    double rnd_x =q_eq;

    double eps =1E-8;

    double px_min =-sqrt(-2.*mass*kT*log(eps));
    double px_max =sqrt(-2.*mass*kT*log(eps));

    //std::cout << " mass:= " << idum;
    //std::cout << " px_max:= " << px_max;
    //std::cout << " px_min:= " << px_min;
    //std::cout << std::endl;
    
    //std::cout << "idum:= " << idum << endl;

    double rnd_px =0.;
    double rnd_y =0.;
    double energy =0.0;

    do{
        rnd_px =px_min + (double)( px_max - px_min )*ran2(&idum);
        rnd_y =(double)ran2(&idum);

        //std::cout << " rnd_px:= " << rnd_px;
        //std::cout << " rnd_y:= "  << rnd_y;
        //std::cout << std::endl;

    }while(rnd_y >= fabs(boltzmanFunction(rnd_x, rnd_px, param)));

    *q =rnd_x;
    *pq =rnd_px;
}


double dynamic::beta_OBD(double _q){
    double beta =0.0;
    if(_q<=0.38)
        beta =32.-32.21*_q;

    else
        beta =15./pow(_q, 0.43) + 1. - 10.5*pow(_q, 0.9) + _q*_q;


    if(beta < 0.)
        beta =0.;
    return beta;
}


void dynamic::Langevin_Statisticall(){
    int _A =0;
    int _Z =0;
    double _Ex =0.;

    _A =this->A;
    _Z =this->Z;
    _Ex =this->Ex;

    double _t =0.;
    double _q =0.;
    double _pq =0.;
    double _h =0.;
    
    bool _resNres =false;
    
    n_fission =0;
    
    if( Ex > 0){

        for(int i=0; i<nRun; i++){
            //        std::cout << "i:=" << i << " ..." << " of " << nRun << std::endl;

            finish_run =false;

            _A =this->A;
            _Z =this->Z;
            _Ex =this->Ex;

            _t =0.;
            _q =0.;
            _pq =0.;
            _h =0.;

            /*preparadndo Langevine*/
            _q =q_eq;   //comienza en el equilibrio
            paramFunction.mass =CalculateMass(_A, _Z)*MeVcc2MW;  //<-- revizar unidades de la masas...
            paramFunction.randValue =0.;
            paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
            double beta_coef =beta_OBD(_q);

            double temp_energy =0.;
            do{
                initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
                temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
            }while( temp_energy >= _Ex );


            while( !finish_run ){
                /**
             *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
             *puede ser fuera del cilco.. y por tanto en el constructor
             **/

                my_nucleus =new nucleus(_A, _Z, _Ex);
                this->Bn = my_nucleus->CalculateBn(_A, _Z);
                this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);

                this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
                this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
                this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
                this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
                this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
                this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
                this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
                this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);

                delete my_nucleus;

                double minBnBf =0.;

                if(Bn > Bf)
                    minBnBf =Bf;
                else
                    minBnBf =Bn;

                if( _Ex > minBnBf ){

                    /* ...aqui va la dinamica Langevin... */
                    /* Abe article */
                    paramFunction.randValue=gaus(&idum);
                    paramFunction.mass =CalculateMass(_A, _Z)*MeVcc2MW;  //<-- revizar unidades de la masas...
                    paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
                    beta_coef =beta_OBD(_q);

                    _t +=step;

                    _h =paramFunction.kT*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) - beta_coef*_pq;
                    _q  +=_pq/paramFunction.mass*step;
                    _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
                    
                    temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
                    _Ex =Ex-temp_energy;
                    /* End Abe article */
                    
                    if(temp_energy > _Ex ){ //not possible
                        finish_run =true;
                        //cout << "temp_energy > _Ex..." << endl;
                    }

                    if( _q > qsci ){ //fission
                        //Nfiss++;
                        n_fission++;
                        finish_run =true;
                    }

                    else{   //dynamical  branch

                        double Gp = this->Gamma_p(_A, _Z, _Ex);
                        double Ga = this->Gamma_a(_A, _Z, _Ex);
                        double shi = ran2(&idum);
                        double tau =1./(Gp + Ga + 1.)*0.658217;

                        if( shi <= step/tau){ 		//MC dynamical emited

                            //cout << "dynamical  branch...:= " << tau << endl;

                            statisticalB =new MCEF(_A, _Z, _Ex, &idum);
                            statisticalB->dynamicalSB();

                            /* actualizar los "observables" de la evaporacion dinamica */
                            _A  -=(statisticalB->Get_proton() + statisticalB->Get_neutron() + statisticalB->Get_alpha()*4);
                            _Z  -=(statisticalB->Get_proton()  + statisticalB->Get_alpha()*2);
                            double en =statisticalB->Get_energy();
                            //_Ex -=statisticalB->Get_energy();
                            //Ex  -=statisticalB->Get_energy();

                            _Ex -=en;
                            Ex  -=en;

                            n_proton +=statisticalB->Get_proton();
                            n_neutron +=statisticalB->Get_neutron();
                            n_alpha +=statisticalB->Get_alpha();

                            //cout  << setw(14) << setprecision(10)  << _A;
                            //cout  << setw(14) << setprecision(10)  << _Z;
                            //cout  << setw(14) << setprecision(10)  << _Ex;
                            //cout  << setw(14) << setprecision(10)  << en;
                            //cout << endl;

                            delete statisticalB;
                        }
                    }

                    //if(_q < 0.0)
                    //cout << "q negative..." << endl;

                    if(_t > tmax ){	 //not fission:: statistical branch
                        /**/
                        statisticalB =new MCEF(_A, _Z, _Ex, &idum);
                        _resNres =statisticalB->statisticalB();


                        if(!_resNres) // evaporation residue
                            Nres++;
                        //else 		  // fission event
                        //n_fission++;

                        finish_run =true;


                        /* actualizar los "observables" de la evaporacion pura */
                        /*revizar*/

                        n_proton +=statisticalB->Get_proton();
                        n_neutron +=statisticalB->Get_neutron();
                        n_alpha +=statisticalB->Get_alpha();
                        n_fission +=statisticalB->Get_fission();
                        n_fissility +=statisticalB->Get_fissility();
                        _Ex -=statisticalB->Get_energy();
                        Ex -=statisticalB->Get_energy();
                        delete statisticalB;
                        /**/
                    }



                    //traj_file << setw(15) << setprecision(8) << _t;
                    //traj_file << setw(15) << setprecision(8) << _q;
                    //traj_file << setw(15) << setprecision(8) << _pq;
                    //traj_file << setw(15) << setprecision(8) << _pq*_pq/2./paramFunction.mass;
                    //traj_file << setw(15) << setprecision(8) << potStruct.potentialMS(_A, _Z, _q);
                    //traj_file << setw(15) << setprecision(8) << temp_energy + _Ex;
                    //traj_file << setw(15) << setprecision(8) << _h;
                    //traj_file << setw(15) << setprecision(8) << sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step);
                    //traj_file << setw(15) << setprecision(8) << beta_coef;
                    //traj_file << setw(15) << setprecision(8) << paramFunction.mass;
                    //traj_file << setw(15) << setprecision(8) << paramFunction.kT;
                    //traj_file << endl;

                }
                else
                    finish_run =true;

            }

            /* actualizar los "observables" */
            /* Publicar los resultados de la corrida n_run[i] */
            
            //out_file << setw(15) << setprecision(8) << i;
            out_file << setw(15) << setprecision(8) << _t;
            out_file << setw(15) << setprecision(8) << _q;
            out_file << setw(15) << setprecision(8) << _pq;
            out_file << endl;
        }
    }
}

void dynamic::Langevin(){
    int _A =0;
    int _Z =0;
    double _Ex =0.;

    _A =this->A;
    _Z =this->Z;
    _Ex =this->Ex;

    double _t =0.;
    double _q =0.;
    double _pq =0.;
    double _h =0.;
    
    n_fission =0;
    
    if( Ex > 0){

        for(int i=0; i<nRun; i++){
            //        std::cout << "i:=" << i << " ..." << " of " << nRun << std::endl;

            finish_run =false;

            _A =this->A;
            _Z =this->Z;
            _Ex =this->Ex;

            _t =0.;
            _q =0.;
            _pq =0.;
            _h =0.;

            /*preparadndo Langevine*/
            _q =q_eq;   //comienza en el equilibrio
            paramFunction.A =_A;
            paramFunction.Z =_Z;
            paramFunction.Exit =Ex;
            paramFunction.mass =CalculateMass(_A, _Z)*MeVcc2MW;  //<-- revizar unidades de la masas...
            paramFunction.randValue =0.;
            paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
            double beta_coef =beta_OBD(_q);

            double temp_energy =0.;
            do{
                initial_Condition(&_q, &_pq, &paramFunction); //Inicializo los momento. segun una distribucion de Boltzman.
                temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
            }while( temp_energy >= _Ex );
            
            //init_file << setw(15) << setprecision(8) << i;
            //init_file << setw(15) << setprecision(8) << _q;
            //init_file << setw(15) << setprecision(8) << _pq;
            //init_file << setw(15) << setprecision(8) << temp_energy;
            //init_file << endl;


            while( !finish_run ){
                /**
             *Quizas no deba crear aqui el objeto nucleus, ... ver bien ...
             *puede ser fuera del cilco.. y por tanto en el constructor
             **/

                my_nucleus =new nucleus(_A, _Z, _Ex);
                this->Bn = my_nucleus->CalculateBn(_A, _Z);
                this->Bf = my_nucleus->CalculateBf(_A, _Z, _Ex);

                this->An = my_nucleus->CalculateAn(_A, _Z, _Ex);
                this->Af = my_nucleus->CalculateAf(_A, _Z, _Ex);
                this->Ap = my_nucleus->CalculateAp(_A, _Z, _Ex);
                this->Bp = my_nucleus->CalculateBp(_A, _Z, _Ex);
                this->Ba = my_nucleus->CalculateBa(_A, _Z, _Ex);
                this->Vp = my_nucleus->CalculateVp(_A, _Z, _Ex);
                this->Aa = my_nucleus->CalculateAa(_A, _Z, _Ex);
                this->Va = my_nucleus->CalculateVa(_A, _Z, _Ex);

                delete my_nucleus;

                double minBnBf =0.;

                if(Bn > Bf)
                    minBnBf =Bf;
                else
                    minBnBf =Bn;

                if( _Ex > minBnBf ){

                    /* ...aqui va la dinamica Langevin... */
                    /* Abe article */
                    paramFunction.randValue=gaus(&idum);
                    paramFunction.mass =CalculateMass(_A, _Z)*MeVcc2MW;  //<-- revizar unidades de la masas...
                    paramFunction.kT = sqrt(_Ex/(potStruct.a(_q, _A)));
                    beta_coef =beta_OBD(_q);

                    _t +=step;

                    _h =paramFunction.kT*potStruct.deriv_entropy(_Ex, _A,  _Z, _q) - beta_coef*_pq;
                    _q  +=_pq/paramFunction.mass*step;
                    _pq +=step*_h + sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step)*paramFunction.randValue;
                    
                    temp_energy =_pq*_pq/2./paramFunction.mass + potStruct.potentialMS(_A, _Z, _q);
                    _Ex =Ex-temp_energy;

                    /* End Abe article */
                    //                    cout << "here..." << endl;

                    if( _q > qsci ){ //fission
                        //Nfiss++;
                        n_fission++;
                        finish_run =true;
                    }
                    if(_q < 0.0)
                        cout << "q negative..." << endl;

                    if(_t > tmax )  //not fission
                        finish_run =true;

                    if(temp_energy > _Ex ){ //not possible
                        finish_run =true;
                        cout << "temp_energy > _Ex..." << endl;
                    }

                    //traj_file << setw(15) << setprecision(8) << _t;
                    //traj_file << setw(15) << setprecision(8) << _q;
                    //traj_file << setw(15) << setprecision(8) << _pq;
                    //traj_file << setw(15) << setprecision(8) << _pq*_pq/2./paramFunction.mass;
                    //traj_file << setw(15) << setprecision(8) << potStruct.potentialMS(_A, _Z, _q);
                    //traj_file << setw(15) << setprecision(8) << temp_energy + _Ex;
                    //traj_file << setw(15) << setprecision(8) << _h;
                    //traj_file << setw(15) << setprecision(8) << sqrt(beta_coef*paramFunction.mass*paramFunction.kT*step);
                    //traj_file << setw(15) << setprecision(8) << beta_coef;
                    //traj_file << setw(15) << setprecision(8) << paramFunction.mass;
                    //traj_file << setw(15) << setprecision(8) << paramFunction.kT;
                    //traj_file << endl;

                }
                else
                    finish_run =true;
            }

            /* actualizar los "observables" */
            /* Publicar los resultados de la corrida n_run[i] */
            
            //out_file << setw(15) << setprecision(8) << i;
            out_file << setw(15) << setprecision(8) << _t;
            out_file << setw(15) << setprecision(8) << _q;
            out_file << setw(15) << setprecision(8) << _pq;
            out_file << endl;
        }
    }
}

void dynamic::Statisticall(){

    int _A =0;
    int _Z =0;
    double _Ex =0.;

    _A =this->A;
    _Z =this->Z;
    _Ex =this->Ex;

    for(int i=0; i<nRun; i++){

        statisticalB =new MCEF(_A, _Z, _Ex, &idum);
        statisticalB->statisticalB();
        finish_run =true;

        /* actualizar los "observables" */
        /*revizar*/

        n_proton +=statisticalB->Get_proton();
        n_neutron +=statisticalB->Get_neutron();
        n_alpha +=statisticalB->Get_alpha();
        n_fission +=statisticalB->Get_fission();
        n_fissility +=statisticalB->Get_fissility();
        delete statisticalB;
    }
}

