/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef HELPERTEMP_H
#define HELPERTEMP_H


/**
 *Esto esta en DataHelper.h
 *temporalmente lo declaramos aqui.
*/

struct Fission{
    Int_t fiss_A;
    Int_t fiss_Z;
    Double_t fiss_E;//final configuration
    Double_t fissility;
    Int_t fiss_neutron;
    Int_t fiss_proton;
    Int_t fiss_alpha; //multiplicities
};
struct Spallation{
    Int_t spall_A;
    Int_t spall_Z; //final configuration
    Int_t spall_neutron;
    Int_t spall_proton;
    Int_t spall_alpha; //multiplicities
};


#endif // HELPERTEMP
