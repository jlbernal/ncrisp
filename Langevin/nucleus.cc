/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "nucleus.hh"
#include <math.h>


#include <iostream>


using namespace std;

nucleus::nucleus(int A, int Z, int Ex){

    this->A =A;
    this->Z =Z;
    this->Ex=Ex;

    initialization();
}

nucleus::nucleus(){
    initialization();
}


void nucleus::initialization(){

}

//double nucleus::level(int A, enum partiType particle){

//    double level_neutron =0.134*A - 1.21*10E-4*A*A;

//    double rp =1.;
//    double ra =1.;

//    if (particle == neutron)
//        return  level_neutron ;//MeV^-1

//    if (particle == proton){
//        return rp*level_neutron ;//MeV^-1
//    }

//    if (particle == alpha){
//        return ra*level_neutron ;//MeV^-1
//    }
//}

///**
//*/

//double nucleus::calculate_B(int A, int Z, enum partiType particle){
//    /**
//     * @brief calculate_B //Calcula la energia de separaci\'on para cada particula
//     * @param A
//     * @param Z
//     * @param particle
//     * @return
//     */

//    if(particle == alpha){
//        double ma =mass(2,2);

//        //        cout << "B(alpha):= " << ma + mass(A - 4,  Z - 2) - mass(A, Z) <<  endl;
//        return ma + mass(A - 4,  Z - 2) - mass(A, Z);
//    }

//    if(particle == proton){

//        //        cout << "B(proton):= " << mp + mass(A - 1,  Z - 1) - mass(A, Z) <<  endl;

//        return mp + mass(A - 1,  Z - 1) - mass(A, Z);
//    }

//    if(particle == neutron){

//        //        cout << "B(neutron):= " << (-0.16*(A - Z) + 0.25*Z + 5.6) <<  endl;

//        return (-0.16*(A - Z) + 0.25*Z + 5.6); //MeV
//    }


//    if(particle == fission){
//        double C =1. - get_Ex()/get_EB();

//        //        cout << "B(fission):= " << C*(0.22*(A-Z) - 1.40*Z + 101.5) <<  endl;

//        return C*(0.22*(A-Z) - 1.40*Z + 101.5); //MeV
//    }
//}

//double nucleus::Coulomb(int A, int Z, partiType particle){

//    double K_p =0.7;
//    double K_a =0.83;

//    if(particle == proton){
//        double C =1. - get_Ex()/get_EB();
//        double potential;

//        //        cout << "coulomb_C(proton):=  " << get_EB() << endl;

//        potential =K_p*(Z-1)*e*e/(r0*pow( A-1 ,1./3.) + Rp);
//        return C*potential;
//    }

//    if(particle == alpha){
//        double C =1. - get_Ex()/get_EB();
//        double potential;


//        //        cout << "coulomb_C(alpha):=  " << C << endl;

//        potential =K_a*(Z-2)*e*e/(r0*pow( A-4 ,1./3.) + Ra);
//        return C*potential;
//    }

//}



/**
 * Del CRISP
 */


//double nucleus::CalculateBf(int A, int Z, double){
//    double Bf = 0.;
//    int N = A - Z;
//    const double 	as = 17.9439,  //MeV
//            k  = 1.7826;
//    double Es = 1. - k *  pow( (double)(N - Z)/(double)A, 2);
//    Es = as * Es * pow(A, (2./3.) );
//    double 	p1 = 50.88,
//            p2 = 1.7826,
//            xo = ( (double)(Z * Z)/(double)A ) / ( p1 * (1. - p2 * pow( (double)(N-Z)/(double)A, 2) ) );
//    if( xo <= 0.06 )
//        xo = 0.06;
//    double fo = GetF(xo);
//    Bf = fo * Es;
//    Bf = Bf + FissionBarrierCorrection(A)
//            /* + FissionBarrierCorrection4(A, Z)*/ ;
//    /* idef inp
//if (A>=180 || A<=150){
//        Bf=0.22*(A-Z)-1.40*Z+110.5;
//        //Bf=0.22*(A-Z)-1.40*Z+101.5;
//        //Bf=0.22*(A-Z)-1.40*Z+102.5; //corre��o para o Am
//        //Bf=Bf*(1-E/getB());
//        Bf=Bf*(1.-E/(A*CPT::p_mass - CalculateMass(A, Z)));
//        if (Bf < 0){
//            Bf =0;
//            A=0;
//        }
//}
//*/
//    //	double y = (double)(Z*Z) /(double)A;
//    return Bf;
//}

double nucleus::CalculateBf(int A, int Z, double E){
    double Bf = 0.;
    int N = A - Z;

    if (A>=180 || A<=150){
        Bf=0.22*(A-Z)-1.40*Z+110.5;
        Bf=Bf*(1.-E/(A*_mp - CalculateMass(A, Z)));
//         if (Bf < 0){
//             Bf =0;
//             A=0;
//         }
    }
    return Bf;
}

double nucleus::GetF(double xo){
    const double fission_parm[48] = {0.00000, 0.00001,  0.00005,  0.00016,
                                     0.00037, 0.00073,  0.00126,  0.00201,
                                     0.00303, 0.00436,  0.00606,  0.00819,
                                     0.01084, 0.01409,  0.01808,  0.02293,
                                     0.02876, 0.03542,  0.04258,  0.04997,
                                     0.05747, 0.06503,  0.07260,  0.08017,
                                     0.08773, 0.09526,  0.10276,  0.11023,
                                     0.11765, 0.12502,  0.13235,  0.13962,
                                     0.14684, 0.15400,  0.16110,  0.16814,
                                     0.17510, 0.18199,  0.18880,  0.19553,
                                     0.20217, 0.20871,  0.21514,  0.22144,
                                     0.22761, 0.23363,  0.23945,  0.24505 };
    double X = 1.;
    int i = 0;
    const double d = .02;
    for ( i = 0; i < 48; i++ ) {
        if ( xo > X )
            break;
        X -= d;
    }
    double X_1 = X - d,
            fo = 0.;
    if ( i < 47 )
        fo = ( (xo - X_1) * fission_parm[i] + (X - xo) * fission_parm[i+1]) / d;
    else
        fo = fission_parm[47];
    return fo;
}

double nucleus::FissionBarrierCorrection(int A){
    double x = A;
    const double 	a1 = 18.03,
            a2 = 101.8,
            a3 = 934.6,
            a4 = 822.3,
            a5 = 30.34,
            a6 = 341.4,
            a7 = 736.5,
            //
            b1 = 10.92,
            b2 = 19.77,
            b3 = 47.3,
            b4 = 94.15,
            b5 = 122.8,
            b6 = 137.2,
            b7 = 167.5,
            //
            c1 = 7.319,
            c2 = 17.01,
            c3 = 40.18,
            c4 = 37.25,
            c5 = 22.5,
            c6 = 35.46,
            c7 = 67.54,
            //
            p1 = -0.0003578,
            p2 = 0.2026,
            p3 = -28.03,
            p4 = -230.9;

    return 	p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
            a1 * exp(-pow(((x - b1) / c1), 2)) +
            a2 * exp(-pow(((x - b2) / c2), 2)) +
            a3 * exp(-pow(((x - b3) / c3), 2)) +
            a4 * exp(-pow(((x - b4) / c4), 2)) +
            a5 * exp(-pow(((x - b5) / c5), 2)) +
            a6 * exp(-pow(((x - b6) / c6), 2)) +
            a7 * exp(-pow(((x - b7) / c7), 2));
}


/**/

double nucleus::CalculateAf(int A, int Z, double E){
    double y = (double)Z*(double)Z/(double)A;
    double rf = CalculateRf(y);
    double an = CalculateAn(A, Z, E);
    return rf *  an;
}

double nucleus::CalculateRf(double y){
    double rf = 1.;
    if(y > 37.50)			rf = prf[0];
    if(y > 37.00 && y <= 37.50)	rf = prf[1];
    if(y > 36.50 && y <= 37.00)	rf = prf[2];
    if(y > 36.00 && y <= 36.50)	rf = prf[3];
    if(y > 35.50 && y <= 36.00)	rf = prf[4];
    if(y > 35.00 && y <= 35.50)	rf = prf[5];
    if(y > 34.50 && y <= 35.00)	rf = prf[6];
    if(y > 34.00 && y <= 34.50)	rf = prf[7];
    if(y > 33.50 && y <= 34.00)	rf = prf[8];
    if(y > 33.00 && y <= 33.50)	rf = prf[9];
    if(y > 32.50 && y <= 33.00)	rf = prf[10];
    if(y > 32.00 && y <= 32.50)	rf = prf[11];
    if(y > 31.50 && y <= 32.00)	rf = prf[12];
    if(y > 31.00 && y <= 31.50)	rf = prf[13];
    if (y > 30.50 && y <= 31.00)	rf = prf[14];
    if (y > 30.00 && y <= 30.50)	rf = prf[15];
    if(y > 29.50 && y <= 30.00)	rf = prf[16];
    if(y > 29.00 && y <= 29.50)	rf = prf[17];
    if(y > 28.50 && y <= 29.00)	rf = prf[18];
    if(y <= 28.50)			rf = prf[19];
    return rf;
}

/**/

double nucleus::CalculateAn(int A, int Z, double){
    const double a_dostrovsk = (double)A/pn1;  // "a" da distibuicao de Dostrovsk
    //const double a_dostrovsk = (double)A/20;  // "a" da distibuicao de Dostrovsk
    int 	N = A - Z;
    double 	theta = (double)(N - Z) / (double)A;
    return a_dostrovsk * sqrt(1. - ((pn2 * theta)/(double)A) );
    //return a_dostrovsk * sqr(1. - ((1.3 * theta)/(double)A) );


    /*article*/
    //    return 0.134*A - 1.21*10E-4*A*A;
}

/**/

double nucleus::CalculateAp(int A, int Z, double E){
    const double a_dostrovsk = (double)A/pp1;
    int 	N = A - Z;
    double 	theta = (double)(N - Z) / (double)A;
    return a_dostrovsk * sqrt(1. + ((pp2 * theta)/(double)A) );

    /*article*/
    //    return 0.134*A - 1.21*10E-4*A*A;
}

/**/

double nucleus::CalculateAa(int A, int Z, double){
    const double a_dostrovsk = (double)A/pa1;
    //return a_dostrovsk * sqr(1. - ( pa9/ (2.0 * A) ) );
    return a_dostrovsk * sqrt(1. - ( pa2/double(A) ));
    //      aa=a*sqr(1-(3/(1.5*A))); // no original em vez de 0.9 o valor e' 2.0 (sergio) < ---- FRAGMENTO DO CRISP SERGIO


    /*article*/
    //    return 0.134*A - 1.21*10E-4*A*A;
}

/**/

double nucleus::CalculateBn(int A, int Z){
    //return -.16 * (A - Z) + .25 * Z + 5.6;
    return _mn + CalculateMass(A - 1, Z ) - CalculateMass(A, Z);
}

/**/

double nucleus::CalculateBp(int A, int Z, double) {
    return _mp + CalculateMass(A - 1, Z - 1) - CalculateMass(A, Z);
}

/**/

double nucleus::CalculateBa(int A, int Z, double){
    const double mcef_ma = 3727.4;    // the alpha mass in MeV.
    return mcef_ma + CalculateMass(A - 4, Z - 2) - CalculateMass(A, Z);
}


/**/

double nucleus::CalculateVa(int A, int Z, double E){
//     const double 	r0_ = 1.18;
     const double    eletron2 = 1.44;
    //segun gontchar:
     double Kv=1.32;
     double Rv=1.21*(pow(A-4,1./3.)+pow(4.,1./3.));
     double Va=(Z-2)*2.*Kv/(Rv+1.6);  
 //      Va = ( 2. * .83 * (Z - 2) * eletron2 ) / ( Rv);
//      double  F = Va * ( 1.0 - E / ( A * _mp - CalculateMass(A, Z) ) );
//      if (F <= 0.){
//         A = 0.;
//         F = 0.;
//      }
    
   //asi estaba antes: 
   // double Va = ( 2. * .83 * (Z - 2) * eletron2 ) / ( r0_ * pow( (double)(A - 4), 1./3.) + 2.16 );
    return Va;
}

/**/

double nucleus::CalculateVp(int A, int Z, double E){
    const double r0_ = 1.21;
    double	eletron2 = 1.44;
//            Vp = (.70 * (Z - 1) * eletron2) / (r0_ * pow((double)(A - 1),1./3.) + 1.14 ),
     double Kv=1.15;
     double Rv=1.21*(pow(A-1,1./3.)+1.0);
     double Vp=(Z-1.)*Kv/(Rv+1.6);
//     Vp = (.70 * (Z - 1) * eletron2) / (Rv);
//     double  F = Vp * ( 1.0 - E / ( A * _mp - CalculateMass(A, Z) ) );
//     if (F <= 0.){
//         A = 0.;
//         F = 0.;
//     }
    return Vp;
}

double nucleus::CalculateVd(int A, int Z, double E){
    const double r0_ = 1.44;
    double	eletron2 = 1.44;
//            Vp = (.70 * (Z - 1) * eletron2) / (r0_ * pow((double)(A - 1),1./3.) + 1.14 ),
     double Kv=1.44;
     double Rv=1.21*(pow(A-2,1./3.)+pow(2,1./3.));
     double Vp=(Z-1.)*Kv/(Rv);
     Vp = (.70 * (Z - 1) * eletron2) / (Rv);
//     double  F = Vp * ( 1.0 - E / ( A * _mp - CalculateMass(A, Z) ) );
//     if (F <= 0.){
//         A = 0.;
//         F = 0.;
//     }
    return Vp;
}
