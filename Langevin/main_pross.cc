/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <time.h>

#include <vector>

#include "function.h"
#include "derivFunction.h"
#include "util.h"

using namespace std;


int main (void)
{

    //~ ifstream timesFiles("outFile.dat", ios::in | ios::binary);

    //~ char line[256];
    //~ double times;
    //~ for(int i=0; i<100; i++){
    //~ timesFiles >> times;
    //~ timesFiles.getline(line, 128);
    //~
    //~ cout << times << endl;
    //~
    //~ }

    //~ char line[256];
    //~ double times =0.;
    //~ int iter =0;;
    //~ while(!timesFiles.eof()){
    //~ timesFiles >> times;
    //~ timesFiles.getline(line, 128);
    //~ cout << times << endl;
    //~
    //~ iter++
    //~ };


    //~ for(int i=0; i<(iter-1); i++){
    //~ }

    /*=====*/

    ifstream file;//("outFile1.dat");
    ofstream file_out;//("outFile1.dat");

    file.open("outFile.dat");

    //~ if(!file.good()){        /*verify if the input file not exist*/
    //~ printf("No file...%s \n", "outFile1.dat");
    //~ abort();
    //~ }

    char line[512];

    int lines =0;
    int qlines =0;
    while(!file.eof()){
        file.getline( line,512);
        lines++;
    }
    file.close();
    lines--;//correccion al numero real de lineas....

    vector<double> times_array;
    double time =0.0;
    double max_Time =0.0;
    double swap_Time =0.0;

    file.open("outFile.dat");
    for(int i=0; i<lines; i++){
        file >> time;
        file.getline( line,512);

        //~ /*	quitar los tiempos , de las que no se dis..*/
        //~ if( time == 100){
        //~ qlines++;
        //~ continue;
        //~ }

        times_array.push_back(time);

        swap_Time =time;
        if( max_Time < swap_Time )
            max_Time =swap_Time;
    }
    
    //   if( max_Time > 50. )
    //    	max_Time = 50.;

    //~ cout << max_Time << endl;

    double pob_counter =0.0;

    double pob_counter_1 =0.0;
    double pob_counter_2 =0.0;

    int temp_number =0;
    int realNumber =times_array.size();
    double value =0.0;

    bool flag =true;
    double itime_step =0.0001;

    file_out.open("outPut_file");
    for(double itime=0.; itime < max_Time-1.; itime+=itime_step/*itime+=max_Time/1E5*/){
        pob_counter =0.;
        pob_counter_1 =0.;
        pob_counter_2 =0.;
        for(int i =0; i<times_array.size(); i++){

            value =times_array[i];

            if( (flag == true) && ((int)value == (int)max_Time) ){
                realNumber--;
                //cout << value << endl;
            }

            if( ( value - itime ) <= 1E-6 ){
                pob_counter++;
            }

            //            /**  other rate **/

            //            if( ( value - itime_step/2. - itime/2. ) < 0. ){
            //                pob_counter_1++;
            //            }

            //            if( ( value + itime_step/2. -  itime ) < 0. ){
            //                pob_counter_2++;
            //            }


        }
        flag =false;
        
        file_out << setw(15) << setprecision(8) << itime;
        file_out << setw(15) << setprecision(8) << 1.0-pob_counter/times_array.size();
        file_out << setw(15) << setprecision(8) << (double)(pob_counter/realNumber);
        file_out << setw(15) << setprecision(8) << (double)(pob_counter/times_array.size());
        //        file_out << setw(15) << setprecision(8) << pob_counter_1/times_array.size();
        //        file_out << setw(15) << setprecision(8) << pob_counter_2/times_array.size();
        //        file_out << setw(15) << setprecision(8) << (double)(log(pob_counter_1/pob_counter_2))/itime_step;
        //        file_out << setw(15) << setprecision(8) << pob_counter_2;
        file_out << endl;
    }

    return 0;
}

