/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "work_rootfiles.hh"

work_rootFiles::work_rootFiles()
{
}

work_rootFiles::~work_rootFiles()
{
}

void work_rootFiles::create_rootFile(param_rootFiles names){

    this->param =names;

    file =new TFile(names.fname_out.Data(), "recreate");
    //file(names.fname_out.Data(), "recreate");
    Hist = new TTree("history", "Langevin Tree");
    if(names.observable.CompareTo("fission") == 0){
        Hist->Branch("Fission",&fiss.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha");
    }
    if(names.observable.CompareTo("spallation") == 0){
        Hist->Branch("Spallation",&spall.spall_A,"spall_A/I:spall_Z:spall_neutron:spall_proton:spall_alpha");
    }
    
    ntree = new TTree("neutrons", "Langevin neutrons Tree");	 
	ntree->Branch("Evap_part",&evapora.p_energy,"p_energy/D:dir_PX:dir_PY:dir_PZ");
   
    ptree = new TTree("protons", "Langevin proton Tree");	 
	ptree->Branch("Evap_part",&evapora.p_energy,"p_energy/D:dir_PX:dir_PY:dir_PZ");

    fiss_tree = new TTree("fiss_hist", "Langevin fission nuclei Tree");
	fiss_tree->Branch("Fission",&fiss.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha");	

    res_tree = new TTree("res_hist", "Langevin fission nuclei Tree");
	res_tree->Branch("Spall",&fiss.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha");   


    fiss_l_tree = new TTree("fiss_hist_langevin", "Langevin fission nuclei Tree");
	fiss_l_tree->Branch("Fission",&fiss_l.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha/D:fiss_q:fiss_qes:fiss_l:E0:iteration/I");	

    res_l_tree = new TTree("res_hist_langevin", "Langevin fission nuclei Tree");
	res_l_tree->Branch("Spall",&fiss_l.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha/D:fiss_q:fiss_qes:fiss_l:E0:iteration/I");   
	


    pro_E = new TH1D("prot_E","Kinetic Energy Distribution - proton",100,0,100);
    neu_E = new TH1D("neut_E","Kinetic Energy Distribution - neutron",100,0,100);
    alp_E = new TH1D("alph_E","Kinetic Energy Distribution - alpha",100,0,100);


	ntree_t = new TTree("neutrons_t", "Langevin neutrons_t Tree");	 
	ntree_t->Branch("Evap_part_l",&evaporation.q_gs,"q_gs/D:q_sd/D:q/D:p_energy/D:dir_PX:dir_PY:dir_PZ:e_time:iteration/I");
   
   	ptree_t = new TTree("protons_t", "Langevin proton_t Tree");	 
	ptree_t->Branch("Evap_part_l",&evaporation.q_gs,"q_gs/D:q_sd/D:q/D:p_energy/D:dir_PX:dir_PY:dir_PZ:e_time:iteration/I");
	
	gtree_t = new TTree("gammas_t", "Langevin gamma_t Tree");	 
	gtree_t->Branch("Evap_part_l",&evaporation.q_gs,"q_gs/D:q_sd/D:q/D:p_energy/D:dir_PX:dir_PY:dir_PZ:e_time:iteration/I");

	atree_t = new TTree("alphas_t", "Langevin gamma_t Tree");	 
	atree_t->Branch("Evap_part_l",&evaporation.q_gs,"q_gs/D:q_sd/D:q/D:p_energy/D:dir_PX:dir_PY:dir_PZ:e_time:iteration/I");	
	
	dtree_t = new TTree("deuterons_t", "Langevin gamma_t Tree");	 
	dtree_t->Branch("Evap_part_l",&evaporation.q_gs,"q_gs/D:q_sd/D:q/D:p_energy/D:dir_PX:dir_PY:dir_PZ:e_time:iteration/I");	
	
}

void work_rootFiles::write_rootFile(elements_rootFiles elements){

    if(this->param.observable.CompareTo("fission") == 0){
        if( elements.fission > 0 ){
            fiss.fiss_A = elements.A;
            fiss.fiss_Z = elements.Z;
            fiss.fiss_E = elements.energy;
            fiss.fissility = elements.fissility;
            fiss.fiss_neutron = elements.neutrons;
            fiss.fiss_proton = elements.protons;
            fiss.fiss_alpha = elements.alphas;
            Hist->Fill();
        }
    }
    if(this->param.observable.CompareTo("spallation") == 0){
        if( elements.fission == 0 ){
            spall.spall_A = elements.A;
            spall.spall_Z = elements.Z;
            spall.spall_neutron = elements.neutrons;
            spall.spall_proton = elements.protons;
            spall.spall_alpha = elements.alphas;
            //            frequencies[final_z][final_a] += 1;
            Hist->Fill();
        }
    }
}

void work_rootFiles::end_rootFile(){
   // Hist->Write();
   // ntree->Write();
   // ptree->Write();
 //   fiss_tree->Write();
 //   res_tree->Write(); 
     fiss_l_tree->Write();
    res_l_tree->Write(); 	
   // pro_E->Write();
   // neu_E->Write();
   // alp_E->Write();
    ntree_t->Write();
    ptree_t->Write();
    gtree_t->Write();
    atree_t->Write();
    dtree_t->Write();
    file->Close();
}


void work_rootFiles::write_ntree_rootFile( Evap_part n_evap){
	evapora.p_energy = n_evap.p_energy;
	evapora.dir_PX = n_evap.dir_PX;
	evapora.dir_PY = n_evap.dir_PY;
	evapora.dir_PZ = n_evap.dir_PZ;
	ntree->Fill();
}

void work_rootFiles::write_ptree_rootFile( Evap_part n_evap){
	evapora.p_energy = n_evap.p_energy;
	evapora.dir_PX = n_evap.dir_PX;
	evapora.dir_PY = n_evap.dir_PY;
	evapora.dir_PZ = n_evap.dir_PZ;
	ptree->Fill();
}

void work_rootFiles::write_fiss_tree_rootFile(elements_rootFiles elements ){
	    fiss.fiss_A = elements.A;
            fiss.fiss_Z = elements.Z;
            fiss.fiss_E = elements.energy;
            fiss.fissility = elements.fissility;
            fiss.fiss_neutron = elements.neutrons;
            fiss.fiss_proton = elements.protons;
            fiss.fiss_alpha = elements.alphas;
            fiss_tree->Fill();
	}

void work_rootFiles::write_res_tree_rootFile(elements_rootFiles elements ){
	    fiss.fiss_A = elements.A;
            fiss.fiss_Z = elements.Z;
            fiss.fiss_E = elements.energy;
            fiss.fissility = elements.fissility;
            fiss.fiss_neutron = elements.neutrons;
            fiss.fiss_proton = elements.protons;
            fiss.fiss_alpha = elements.alphas;
            res_tree->Fill();
	}

void work_rootFiles::write_ntree_t_rootFile( Evap_part_l n_evap){
        evaporation.q_gs=n_evap.q_gs;
	evaporation.q_sd=n_evap.q_sd;
	evaporation.q=n_evap.q;
	evaporation.p_energy = n_evap.p_energy;
	evaporation.dir_PX = n_evap.dir_PX;
	evaporation.dir_PY = n_evap.dir_PY;
	evaporation.dir_PZ = n_evap.dir_PZ;
	evaporation.e_time = n_evap.e_time;
	evaporation.iteration = n_evap.iteration;
	ntree_t->Fill();
}

void work_rootFiles::write_ptree_t_rootFile( Evap_part_l n_evap){
        evaporation.q_gs=n_evap.q_gs;
	evaporation.q_sd=n_evap.q_sd;
	evaporation.q=n_evap.q;  
	evaporation.p_energy = n_evap.p_energy;
	evaporation.dir_PX = n_evap.dir_PX;
	evaporation.dir_PY = n_evap.dir_PY;
	evaporation.dir_PZ = n_evap.dir_PZ;
	evaporation.e_time = n_evap.e_time;
	evaporation.iteration = n_evap.iteration;
	ptree_t->Fill();
}

void work_rootFiles::write_gtree_t_rootFile( Evap_part_l n_evap){
        evaporation.q_gs=n_evap.q_gs;
	evaporation.q_sd=n_evap.q_sd;
	evaporation.q=n_evap.q;  
	evaporation.p_energy = n_evap.p_energy;
	evaporation.dir_PX = n_evap.dir_PX;
	evaporation.dir_PY = n_evap.dir_PY;
	evaporation.dir_PZ = n_evap.dir_PZ;
	evaporation.e_time = n_evap.e_time;
	evaporation.iteration = n_evap.iteration;
	gtree_t->Fill();
}

void work_rootFiles::write_atree_t_rootFile( Evap_part_l n_evap){
        evaporation.q_gs=n_evap.q_gs;
	evaporation.q_sd=n_evap.q_sd;
	evaporation.q=n_evap.q;  
	evaporation.p_energy = n_evap.p_energy;
	evaporation.dir_PX = n_evap.dir_PX;
	evaporation.dir_PY = n_evap.dir_PY;
	evaporation.dir_PZ = n_evap.dir_PZ;
	evaporation.e_time = n_evap.e_time;
	evaporation.iteration = n_evap.iteration;
	atree_t->Fill();
}

void work_rootFiles::write_dtree_t_rootFile( Evap_part_l n_evap){
        evaporation.q_gs=n_evap.q_gs;
	evaporation.q_sd=n_evap.q_sd;
	evaporation.q=n_evap.q;  
	evaporation.p_energy = n_evap.p_energy;
	evaporation.dir_PX = n_evap.dir_PX;
	evaporation.dir_PY = n_evap.dir_PY;
	evaporation.dir_PZ = n_evap.dir_PZ;
	evaporation.e_time = n_evap.e_time;
	evaporation.iteration = n_evap.iteration;
	dtree_t->Fill();
}


void work_rootFiles::write_fiss_l_tree_rootFile(elements_rootFiles elements ){
	    fiss_l.fiss_A = elements.A;
            fiss_l.fiss_Z = elements.Z;
            fiss_l.fiss_E = elements.energy;
            fiss_l.fissility = elements.fissility;
            fiss_l.fiss_neutron = elements.neutrons;
            fiss_l.fiss_proton = elements.protons;
            fiss_l.fiss_alpha = elements.alphas;
	    fiss_l.fiss_q = elements.q;
	    fiss_l.fiss_qes = elements.qes;
	    fiss_l.fiss_l = elements.l; 
	    fiss_l.iteration=elements.iteration;
            fiss_l.E0=elements.E0;
            fiss_l_tree->Fill();


	}

void work_rootFiles::write_res_l_tree_rootFile(elements_rootFiles elements ){
	    fiss_l.fiss_A = elements.A;
            fiss_l.fiss_Z = elements.Z;
            fiss_l.fiss_E = elements.energy;
            fiss_l.fissility = elements.fissility;
            fiss_l.fiss_neutron = elements.neutrons;
            fiss_l.fiss_proton = elements.protons;
            fiss_l.fiss_alpha = elements.alphas;
	    fiss_l.fiss_q = elements.q;
	    fiss_l.fiss_qes = elements.qes;
	    fiss_l.fiss_l = elements.l; 
	    fiss_l.iteration=elements.iteration;
	    fiss_l.E0=elements.E0;
            res_l_tree->Fill();
	}



