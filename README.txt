
Basic instructions and important information for users.

NOTE: CRISP code is constantly under development. Some features may not be fully
functional.

-------------------------------------------------------------------------------------------------------
- 1. Code
     1.1. Event Generators
	  1.2. Modular Struture

- 2. Compilation and Execution
-------------------------------------------------------------------------------------------------------




-------------------------------------------------------------------------------------------------------
1. Code
-------------------------------------------------------------------------------------------------------

CRISP is a C++ program developed to simulate nuclear reactions in a realistic
fashion using Monte Carlo method. It requires the ROOT framework from CERN to work 
(homepage: http://root.cern.ch/drupal/ - Class Index: http://root.cern.ch/root/html/ClassIndex.html).
The Open MPI - Message Passing Interface (http://www.open-mpi.org/) is also required for the
compilation and execution of parallel computations in CRISP. The MathMore module of ROOT
is a package enabled during configuration step (see ROOT installation tutorial on the homepage)
that provides advanced numerical functionality but is dependent on the external 
library GSL (http://www.gnu.org/software/gsl/). Special mathematical functions like Laguerre Polynomials
and Bessel functions are included in GSL.  
 
The code was designed for Linux Operating System. An attempt to run the program in any other OS may 
result on incompatibilities.

Other possible issues may arise from some incompatibility between ROOT and the operating system.
Up to now, CRISP has been tested on ubuntu linux 10.04 LTS with ROOT v5.30 and v5.32 and 
on ubuntu linux 12.04 LTS with ROOT v5.34. No issues regarding GSL and Open MPI have been found so far.


1.1 Event Generators

For each kind of reaction there is a event generator placed in the generators
directory. Each generator is responsible for loading the possible interaction channels 
between projectile and target and the initial interaction itself. The list of current 
implemented generators is the following:

	- proton;
	- photon(monochromatic);
	- bremsstrahlung;
	- virtual photon for ultra-peripheral collisions (UNDER DEVELOPMENT);
	- hyperon;
	- deuteron (UNDER DEVELOPMENT)
	- ion-ion collision (UNDER DEVELOPMENT)

Up to now it is possible to calculate ultra-peripheral collisions with quasi-real photons by means 
of photo-nuclear reactions since both ions colliding interact only by their electrogmanetic field. This field can 
be treated as a flux of virtual photons. The final observables and quantities must take into account a proper treatment
of the flux of photons according the Weiszacker-Williams method of the virtual quanta (Jackson, Electrodynamics).  


1.2 Modular Structure

Several modules inside CRISP code performs all steps of a reaction:

cascade: 
	necessary code for intra-nuclear cascade simulation (step one).

mcef: 
	code for the evaporation-fission competition simulation (step two). Both fissioning nuclei 
	and spallation parabolas can be obtained.

multimode_fission: 
	code for the determination of the distribution of fragments from fission (step three).
	The Symmetric and three asymmetric modes are considered as possible channels for fission.

mcmm:
	the multifragmentation module is implemented but NOT YET tested. It concerns another possible
	step two which is a final point for the reaction.

Langevin:
	an alternative approach to the evaporation fission competition step.


There are some others important folders in the CRISP code which are worth mentioning.

A directory named "scripts" keeps all necessary routines for the execution of all currently
implemented steps of a reaction. The routines are separated in sub-directories according
to the modular division of the program.

The "Minuit" directory keeps (also in a modular manner) routines for adjustment of parameters of
the models used in CRISP. All routines use the Minuit or Minuit2 implementaions from ROOT.
Some of them are yet under testing.

Collected experimental data are placed in the "exp_data" directory. Other important data such
as tables of more fundamental information (particle table, binding energies, etc) are in the
"data" directory.

The "helpers" folder contains some useful code for general use.

Each simulation module (cascade, mcef, etc) requires a specific input file. All of them
must be placed in the "input" folder and have a specific extension associated to the simulation to be
performed by CRISP. Some of these files are there as examples and specific instructions can be found inside 
each one. 

All results from each simulation module are stored in root files (a compressed file created for use 
with the ROOT framework). All information about creation, reading and other manipulations of this kind 
of file can be found on the ROOT User's Guide. The results generated by CRISP are created in the "results" 
folder. The sub-directories are automatically created by CRISP. They are organized according to the 
simulation modules, generators, target nucleus and so on.




-------------------------------------------------------------------------------------------------------
2. Compilation and Execution
-------------------------------------------------------------------------------------------------------

The CRISP code must be compiled in order to generate the executable file that will perform a
specific simulation. The "Makefile" is the file responsible for this task. It is placed in the root
CRISP directory. The simulations regarding nuclear reactions are predefined inside the Makefile. They are:

Cascade:
	mcmc (serial)
	mcmc-mpi (MPI)

Evaporation-Fission Competition:
	mcef

Multimodal Fission:
	multimodal-fission (serial)
	multimodal-fission-mpi (MPI)

Multimodal Fission adjustment of the model by Minuit
	multimodal-fission-minuit (serial)
	multimodal-fission-minuit-mpi (MPI)

The compilation and execution of a CRISP executable must be done inside the top CRISP directory.
The compilation in linux command line is performed like this: 

$ make <target> (ex. make mcmc, make mcef...)

 Lots of information...

Then you can execute the created executable file which has extension .exec. The address of the
input file is mandatory. For example:

$ ./mcmc.exec input/example.crisp

For the MPI situation, here is an example:

$ make mcmc-mpi
$ mpirun -np <number_of_processes> ./mcmc-mpi.exec input/example.crisp

The <number_of_processes> argument means the number of cores (if in a computer) or the number of nodes 
(in a cluster). In a cluster the command for execution may vary but usually it is as follows:

$ mpirun -np <number_of_processes> -hostfile <hostfile_name> ./mcmc-mpi.exec input/example.crisp

The hostfile file must contain the name of the nodes in the cluster. An example of a hostfile accompanies
the CRISP package under the name of host.file.




