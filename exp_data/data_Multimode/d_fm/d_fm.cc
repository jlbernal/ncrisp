{
#include "TGraphErrors.h"
#include "TF1.h"
#include "TString.h"
#include "TCanvas.h"
#include "TMath.h"

using namespace std;

Int_t Z=0, A=0, n=15, i=0;
Double_t d=0., ed=0.; 

std::ifstream SL;
std::ifstream SI;
std::ifstream SII;

TString inStr;
TString delim = " ";

TF1 *f = new TF1("f","[0]*x+[1]",1200.,1400.);
gStyle->SetTitleFillColor(10);

//Modo SL
//__________________________________________________________________________________________

TCanvas *c1 = new TCanvas("c1","SL",200,10,700,500);
c1->SetBorderMode(0);
c1->SetFillColor(10);

TGraphErrors *grSL = new TGraphErrors(n);
grSL->SetTitle("SL");
grSL->SetMarkerStyle(20);
//grSL->SetMarkerColor();

SL.open("exp_data/data_Multimode/d_fm/por_canal/SL", ifstream::in);
while( SL.good() ) {
			
		SL >> Z >> A >> d >> ed;
		grSL->GetX()[i]= (double)sqr(Z)/pow((double)A,1./3.);
		grSL->GetY()[i]= d;
		grSL->GetEY()[i]= ed;
		i++;

}
SL.close();

i=0;

f->SetParameter(0,1.);
f->SetParameter(1,1.);

grSL->Draw("AP");
grSL->Fit(f,"R");

grSL->GetXaxis()->SetTitle("Z^{2}/A^{1/3}");
grSL->GetYaxis()->SetTitle("d (fm)");
grSL->GetXaxis()->SetTitleOffset(1);

gStyle->SetOptTitle(kFALSE);

TLegend *l = new TLegend(0.12,0.7,0.40,0.88);
l->AddEntry(grSL, "Experimental", "P");
l->AddEntry(f, "Ajuste linear", "L");
l->Draw();

l->SetFillColor(10);

c1->Print("SL.eps");
/*
//Modo SI
//__________________________________________________________________________________________

TCanvas *c2 = new TCanvas("c2","SI",200,10,450,300);
c2->SetBorderMode(0);
c2->SetFillColor(10);

TGraphErrors *grSI = new TGraph(n);
grSI->SetTitle("SI");
grSI->SetMarkerStyle(21);
grSI->SetMarkerColor(3);

SI.open("exp_data/data_Multimode/d_fm/por_canal/SI", ifstream::in);
while( SI.good() ) {
			
		SI >> Z >> A >> d >> dd;
		grSI->GetX()[i]= (double)sqr(Z)/pow((double)A,1./3.);
		grSI->GetY()[i]= d;
		//grSI->GetEY()[i]= dd;
		i++;

}
SI.close();

i=0;

f->SetParameter(0,1.);
f->SetParameter(1,1.);

grSI->Draw("AP");
grSI->Fit(f,"R");

c2->Print("SI.eps");

//Modo SII
//__________________________________________________________________________________________

TCanvas *c3 = new TCanvas("c3","SII",200,10,450,300);
c3->SetBorderMode(0);
c3->SetFillColor(10);

TGraphErrors *grSII = new TGraph(n);
grSII->SetTitle("SII");
grSII->SetMarkerStyle(22);
grSII->SetMarkerColor(4);

SII.open("exp_data/data_Multimode/d_fm/por_canal/SII", ifstream::in);
while( SII.good() ) {
			
		SII >> Z >> A >> d >> dd;
		grSII->GetX()[i]= (double)sqr(Z)/pow((double)A,1./3.);
		grSII->GetY()[i]= d;
		//grSII->GetEY()[i]= dd;
		i++;

}
SII.close();

f->SetParameter(0,1.);
f->SetParameter(1,1.);

grSII->Draw("AP");
grSII->Fit(f,"R");

c3->Print("SII.eps");
*/
}
