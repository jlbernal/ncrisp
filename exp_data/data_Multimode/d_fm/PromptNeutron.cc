{
#include "TH1.h"
#include "TString.h"
#include "TCanvas.h"
#include "TMath.h"
  
#include <vector>

std::ifstream ifs;

Double_t A_simm = 0.,
			A_fissao = 0.,
			Z_fissao = 0.,
			R_neck = 0.,
			d_fmSL = 0.,
			d_fmSI = 0.,
			d_fmSII = 0.,
			Vol_cil = 0.,
			alpha = 1.,
			nu_n = 0;

Double_t Vol_n = 4*TMath::Pi()*pow(1.12,3)/3.;

vector <Double_t> vectA_fissao,
						vectZ_fissao;

TString title[4] = {"Am241", "Np237", "U238 - 50 MeV", "U238 - 3.5 GeV"};

TString file_name[4] = {"results/proton/Am241/massfrag.txt",
								"results/proton/Np237/massfrag.txt",
								"results/photon/U238/Random_cascade_photon/50_massfrag.txt",
								"results/photon/U238/Random_cascade_photon/3500_massfrag.txt"};

TString graph[4] = {"Am241_nprompt.eps", "Np237_nprompt.eps", "U238_50MeV_nprompt.eps", "U238_3500MeV_nprompt.eps"};


for(int i=0 ; i<4 ; i++){

	TCanvas *c = new TCanvas("c",title[i].Data(),200,10,800,200);
	c->SetBorderMode(0);
	c->SetFillColor(10);
	c->Divide(3,1);

	gStyle->SetTitleFillColor(10);
	gStyle->SetStatColor(10);
	gStyle->SetStatH(0.22);
	gStyle->SetStatW(0.30);

	TH1D *h_SI = new TH1D("h_SI", title[i].Data(),50,0.,2.);
	TH1D *h_SII = new TH1D("h_SII", title[i].Data(),50,0.,10.);
	TH1D *h_SL = new TH1D("h_SL", title[i].Data(),50,-5.,16.);
  
	ifs.open(file_name[i].Data(), ifstream::in);
	while(ifs.good()){
    	ifs >> A_simm >> A_fissao >> Z_fissao;
		vectA_fissao.push_back(A_fissao);
		vectZ_fissao.push_back(Z_fissao);
	}
	ifs.close();

	for(int j=0 ; j<vectA_fissao.size() ; j++){

		R_neck = 0.27 * 1.2*pow(vectA_fissao[j],0.3333333);
		
		d_fmSI = -0.000316559 * ( pow(vectZ_fissao[j],2)/pow(vectA_fissao[j],0.3333333) ) + 1.06265;
		Vol_cil = TMath::Pi()*pow(R_neck,2.)*d_fmSI;
		nu_n = alpha*Vol_cil/Vol_n;
		h_SI->Fill(nu_n);

		d_fmSII = 0.00586971 * ( pow(vectZ_fissao[j],2)/pow(vectA_fissao[j],0.3333333) ) -6.27076;
		Vol_cil = TMath::Pi()*pow(R_neck,2.)*d_fmSII;
		nu_n = alpha*Vol_cil/Vol_n;
		h_SII->Fill(nu_n);

		d_fmSL = 0.0138294 * ( pow(vectZ_fissao[j],2)/pow(vectA_fissao[j],0.3333333) ) -16.4586;
		Vol_cil = TMath::Pi()*pow(R_neck,2.)*d_fmSL;
		nu_n = alpha*Vol_cil/Vol_n;
		h_SL->Fill(nu_n);

	}

	c->cd(1);
	h_SI->Draw();

	c->cd(2);
	h_SII->Draw();

	c->cd(3);
	h_SL->Draw();

	c->Print(graph[i].Data());

	delete c;
	delete h_SI;
	delete h_SII;
	delete h_SL;	
}

}
