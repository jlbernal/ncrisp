//#include<stdio.h>
#include<cstdio>
//#include<fstream.h>
#include<fstream>
//#include<math.h>
#include<cmath>
//#include<time.h>
#include<ctime>
//#include<stdlib.h>
#include<cstdlib>
//#include<iostream.h>
#include<iostream>
using namespace std;

/*
#include<fstream>
#include<math>
#include<time>
#include<stdlib>
#include<stdio.h>
*/

ofstream file_multW800("W800mult.dat") ;
ofstream multn_eventoW800("W800multn_evento.dat") ;
ofstream file_spallW800("W800spall.dat") ;
ofstream file_histoW800("W800histo.dat");
ofstream file_pcascataW800("W800pcascata.dat");
ofstream file_pevapW800("W800pevap.dat");
ofstream file_azcascataW800("W800azcascata.dat");

int energia_neutron ;
double ein ;
int tot_event;

double mprotons ;
double mneutrons ;
double malfa ;

double nprotons ;
double nneutrons ;
double nalfas ;

double A_spall, Z_spall ;

int npontos ;
int flag_fissao ;
double nfissoes ;

const int  tot_event1 = 100000 ;
double nA[tot_event1], nZ[tot_event1], n_cascata[tot_event1], p_cascata[tot_event1] ;
double n2[tot_event1], n3[tot_event1], p2[tot_event1], p3[tot_event1] ;
double n1[tot_event1], p1[tot_event1], a1[tot_event1];
double excit_energy[tot_event1], pn[tot_event1][3];

//c�lculo "r�pido" de quadrado
inline double sqr(double x)
  {
   return x*x ;
  }

//numero de linhas do arquivo

int num_linhas(char *file_in)
  {
   ifstream in(file_in) ;
   int i, j = 0 ;
   double x[7] ;
   while(in)
     {
      j++ ;
      for(i=1; i<=6; i++) in >> x[i] ;
     }
   --j ;
   return j ;
  }

// Faz o arredondamento para duas casa decimais
float trunca(double x)
  {
   double inteiro ;
   float fracao ;
   double x2 = x*100.0 ;
   fracao = modf(x2, &inteiro) ;
   if(fracao >= 0.5) ++inteiro ;
   return  (inteiro/100.0) ;
  }

// Calcula secao de choque de para determinada multiplicidade de neutrons
double multnxs(int freq, int valid_event, int z_number, int dn)
  {
   double sigma_multn ;
   double probab = double(freq)/double(valid_event);
   double potencia = pow(double(z_number),(1./3.));
   sigma_multn = probab * (2*3.14159*sqr(2.34*potencia))/double(dn) ; //  (rp+r0)=((1.2+1.14)*potencia)
   return  sigma_multn ;
  }

// Gera dados de multiplicidade de neutrons por evento
double frequencia[500] ;
void histogram(double *n1, int excit_cont)
  {
    int i ;
    for (i=0 ; i<=499 ; i++) frequencia[i] = 0 ;
    for (i=1 ; i<=excit_cont ; i++) 
     {
      ++frequencia[int(n1[i])] ;
//      cout << frequencia[int(n1[i])] << "\t" << int(n1[i]) << endl ;
     }
  }

// Ordena os valores de um vetor de 2 colunas em ordem crescente
void ordena(double AZfinal[100000][3], int nspall)
  {
    int i, j, contador, posicao, cont;
    double AZordenado[100000][3], menor[3], temp[3];
    contador = nspall ;
    cont = 0 ;   
    do
      {
       ++cont ;
       menor[1]=300;
       for (i=cont ; i<=contador ; i++)
	 {
	   if (AZfinal[i][1]<menor[1])
	     {
              menor[1]=AZfinal[i][1];
              menor[2]=AZfinal[i][2];
              posicao=i;
	     }
         }

       temp[1]=AZfinal[cont][1];
       temp[2]=AZfinal[cont][2];
       AZfinal[cont][1]=menor[1];
       AZfinal[cont][2]=menor[2];
       AZfinal[posicao][1]=temp[1];
       AZfinal[posicao][2]=temp[2];
      }
    while (cont<nspall) ;
  }

//media e desvio padrao
class media_desviop
  {
   public:
   double media_grandeza ;
   int  i ;

   double desvio_padrao(double *vetor, double ntotal)
     {
      double desvio = 0 ;
      if(ntotal <= 1)
        {
         desvio = vetor[1] ;
        }
      else
        {
         media(vetor, ntotal) ;
         for(i=1 ; i<=ntotal; i++) desvio += sqr(vetor[i])  ;
         desvio = desvio * (1/(ntotal-1.0)) ;
         desvio = desvio - ((ntotal)/(ntotal-1.0))*sqr(media_grandeza) ;
         if(desvio <0 ) desvio = 0 ;
         desvio = sqrt(desvio) ;
         desvio = desvio/(sqrt(ntotal)) ;
        }
      return desvio ;
     }

   double media(double *vetor, double ntotal)
     {
      media_grandeza = 0 ;
      for(i=1 ; i<=ntotal; i++) media_grandeza += vetor[i] ;
      media_grandeza = media_grandeza/ntotal ;
      return media_grandeza ;
     }
  };


//gerador de n�meros rad�micos, utiliza semente (ran3.dat)

int idum, iff, inext, inextp ;

double ma[56] ;

double ran3()
  {
   double fac, ran3 ;

   int i, ii, k, mbig, mj, mk, mseed, mz ;

   mbig = 4000000.0 ;
   mseed = 1618033.0 ;
   mz = 0 ;
   fac = 2.5e-7 ;

   if(idum < 0 || iff == 0)
     {
      iff = 1 ;
      mj = mseed - abs(idum) ;
      mj = mj%mbig ;
      ma[55] = mj ;
      mk = 1 ;
      for(i=1; i<=54; i++)
        {
         ii = 21*i%55 ;
         ma[ii] = mk ;
         mk = mj - mk ;
         if(mk < mz) mk = mk + mbig ;
         mj = ma[ii] ;
        }
      for(k=1; k<=4; k++)
        {
         for(i=1; i<=55; i++)
           {
            ma[i] = ma[i] - ma[1 + (i + 30)%55] ;
            if(ma[i] < mz) ma[i] = ma[i] + mbig ;
           }
        }
      inext = 0 ;
      inextp = 31 ;
      idum = 1 ;
     }
   inext++ ;
   if(inext == 56) inext = 1 ;
   inextp++ ;
   if(inextp == 56) inextp = 1 ;
   mj = ma[inext] - ma[inextp] ;
   if(mj < mz) mj = mj + mbig ;
   ma[inext] = mj ;
   ran3 = mj * fac ;
   return  ran3 ;
  }

void erro_sqrt(double radicando, char *function)
  {
   if(radicando < 0)
     {
      cout << "negative square root em " << function << endl ;
      double abc ;
      cout << "digite qualquer numero\n" ;
      cin >> abc ;
     }
  }

class FissionBarrier
  {
   int A,Z,N;
   double Bf;
   double x[48],f[50];

	public: FissionBarrier()
     {
      double data[2][48], d = 1.0 ;
      int i ;
      for(int i=0; i<48; i++)
        {
         x[i] = d ;
         d -= 0.02 ;
        }
      f[0] = 0 ; f[1] = 0.00001 ; f[2] = 0.00005 ; f[3] = 0.00016 ;
      f[4] = 0.00037 ; f[5] = 0.00073 ; f[6] = 0.00126 ; f[7] = 0.00201 ;
      f[8] = 0.00303 ; f[9] = 0.00436 ; f[10] = 0.00606 ; f[11] = 0.00819 ;
      f[12] = 0.01084 ; f[13] = 0.01409 ; f[14] = 0.01808 ; f[15] = 0.02293 ;
      f[16] = 0.02876 ; f[17] = 0.03542 ; f[18] = 0.04258 ; f[19] = 0.04997 ;
      f[20] = 0.05747 ; f[21] = 0.06503 ; f[22] = 0.07260 ; f[23] = 0.08017 ;
      f[24] = 0.08773 ; f[25] = 0.09526 ; f[26] = 0.10276 ; f[27] = 0.11023 ;
      f[28] = 0.11765 ; f[29] = 0.12502 ; f[30] = 0.13235 ; f[31] = 0.13962 ;
      f[32] = 0.14684 ; f[33] = 0.15400 ; f[34] = 0.16110 ; f[35] = 0.16814 ;
      f[36] = 0.17510 ; f[37] = 0.18199 ; f[38] = 0.18880 ; f[39] = 0.19553 ;
      f[40] = 0.20217 ; f[41] = 0.20871 ; f[42] = 0.21514 ; f[43] = 0.22144 ;
      f[44] = 0.22761 ; f[45] = 0.23363 ; f[46] = 0.23945 ; f[47] = 0.24505 ;

	  }

	public: double calcBf(int A1, int Z1)
     {
		A=A1;
		Z=Z1;
		int N=A-Z;
		double as=17.9439;  //MeV
		double k=1.7826;
		double Es=1-k*pow((double)(N-Z)/A,2);
		Es=as*Es*pow(A,(2./3.));
		double p1=50.88;
		double p2=1.7826;
		double xo=((double)(Z*Z)/A)/(p1*(1-p2*pow((double)(N-Z)/A,2)));
		if(xo<=0.06)xo=0.06;
		double fo=getF(xo);

		Bf=fo*Es;
		return Bf;
	  }

	public: double getF(double xo)
     {
      double fo=0;
      int i=0;
      while(xo<x[i])
       {
         i++;
       }
      if(i<47) fo=((xo-x[i+1])*f[i]+(x[i]-xo)*f[i+1])/(x[i]-x[i+1]);
      else fo=f[47];
      return fo;
	  }
  };

class Weisskopf
  {
   private:
   double E,P,T,a,step,invT2;

   public:
   void init_Weisskopf(double E1, double a1)
     {
      E=E1;
      a=a1;
      step=0.1;
     }

   void Probability(double eps)
     {
      if((E/a) < 0)
      cout << "raiz negativa 1\n" ;
      T=sqrt(E/a);
      if(((E-T)/a) < 0)
        {
         cout << "raiz negativa 2\n" ;
         cout << "E = " << E << endl ;
         cout << "T = " << T << endl ;
         cout << "a = " << a << endl ;
        } 

      T=sqrt((E-T)/a); //recalcula a temperatura, que deve ser aquela do n�cleo residual
      double invT2=1.0/(T*T);
      P=invT2*eps*exp(-eps/T)*step;
     }

   double getEnergy()
     {
      double rand = ran3();
      if(rand > 0.99) rand = 0.99 ;
      double eps=0;
      double Pacum=0;

      while (Pacum<rand)
        {
         Probability(eps);
         Pacum=P+Pacum;
         eps=eps+step;
         //cout << Pacum << "\t" << rand << endl ;
	     }
	   return eps;
     }
  };

class Nucleus
  {
   int A,Z,N;
   double Bn,Bf,Bp,Ba,B;
   double Vp,Va;
   double an,af,ap,aa;
   double Gf,Gp,Ga;
   double m,E,Ex,T;
   double ro;

   FissionBarrier bf ;

   public: void init_Nucleus(int A1, int Z1, double E1)
     {
	   A=A1;
	   Z=Z1;
	   E=E1;
	   Ex=0;
      Bn=Bf=Bp=Ba=B=0;
      Vp=Va=0;
      an=af=ap=aa=0;
      Gf=Gp=Ga=0;
      m=T=0;
      ro=1.2;
     }

   public: void calcAn()
     {
	   double shell=0;
      an=0.121*(A-1)-1.3E-4*(A-1)*(A-1);
//      an=0.135*(A-1)-1.3E-4*(A-1)*(A-1);
	   an=(1+(1-exp(-0.061*E))*shell/E)*an;
     }

   public: double getAn()
     {
	   if (an==0) calcAn();
	   return an;
     }

   public: void calcAf()
     {
	   //getAn();
	   //af=1.05*getAn();  //238U para Vladimir
	   af=getAf_Martins();
     }

   public: double getAf_Martins()
     {
      double rf=1.0;
      double parametro;

      parametro=double(Z)*double(Z)/double(A);

//      if (parametro > 34.25) rf=1+0.06*(parametro-30.5);
//      if (parametro > 31.5 && parametro <=34.25) rf=1+0.03*(parametro-31.05);
//      if (parametro <= 31.5 && parametro > 24.90) rf=1.1-0.02*(parametro-21.6);
//      if (parametro <= 24.90) rf=1;
      if (parametro > 34.25) rf=1+0.06*(parametro-30.5);
      if (parametro > 31.5 && parametro <=34.25) rf=1+0.08*(parametro-30.05);
      if (parametro <= 31.5 && parametro > 24.90) rf=1.3-0.02*(parametro-20.6);
      if (parametro <= 24.90) rf=1;
      af=rf*getAn();

      return af;
     }

   public: double getAf()
     {
	   if (af==0) calcAf();
	   return af;
     }

   public: double calcAp()
     {
      double rp = 1 ;
	   double shell=0;
      ap=0.121*(A-1)-1.21E-4*(A-1)*(A-1);
//      ap=0.133*(A-1)-1.21E-4*(A-1)*(A-1);
	   ap=(1+(1-exp(-0.061*E))*shell/E)*ap;

      double parametro;

      parametro=double(Z)*double(Z)/double(A);

//      if (parametro > 34.0) rp=1+0.06*(parametro-36);
//      if (parametro > 33. && parametro <=34.0) rp=1+0.03*(parametro-30.20);
//      if (parametro <= 33. && parametro > 23.90) rp=1.1-0.02*(parametro-20.50);
//      if (parametro <= 23.90) rp=1;
      if (parametro > 34.0) rp=1+0.06*(parametro-36);
      if (parametro > 31. && parametro <=34.0) rp=1+0.08*(parametro-30.20);
      if (parametro <= 31. && parametro > 24.90) rp=1.3-0.02*(parametro-20.50);
      if (parametro <= 24.90) rp=1;
      ap = rp*ap ;
	   return ap;
     }

   public: double getAp()
     {
	   if (ap==0) calcAp();
	   return ap;
     }

   public: void calcAa()
     {
      double ra = 1 ;
	   double shell=0;
      aa=0.12*(A-4)-1.5E-4*(A-4)*(A-4);
//      aa=0.134*(A-4)-1.5E-4*(A-4)*(A-4);
	   aa=(1+(1-exp(-0.061*E))*shell/E)*aa;

      double parametro;

      parametro=double(Z)*double(Z)/double(A);

//      if (parametro > 35.0) ra=1+0.06*(parametro-30.34);
//      if (parametro > 33.20 && parametro <=35.0) ra=1+0.05*(parametro-31.2);
//      if (parametro <= 33.20 && parametro > 23.90) ra=1.2-0.02*(parametro-24.2);
//      if (parametro <= 23.90) ra=1.4;
      if (parametro > 35.0) ra=1+0.06*(parametro-30.34);
      if (parametro > 31.20 && parametro <=35.0) ra=1+0.08*(parametro-29.2);
      if (parametro <= 31.20 && parametro > 24.90) ra=1.3-0.02*(parametro-22.2);
      if (parametro <= 24.90) ra=1.4;
      aa = ra*aa ;
     }

   public: double getAa()
     {
	   if (aa==0) calcAa();
	   return aa;
     }

   public: void calcBn()
     {
      Bn=-0.16*(A-Z)+0.25*Z+5.6;
      //Bn=-0.16*(A-Z)+0.25*Z+5.4; //corre��o para o Am
     }

   public: double getBn()
     {
	   if (Bn==0) calcBn();
	   //Bn=7.97;  //valor tabelado para Bi(209)
      return Bn;
     }

   public: void calcBf()
     {
	   Bf=0.22*(A-Z)-1.40*Z+101.5;
	   //Bf=0.22*(A-Z)-1.40*Z+102.5; //corre��o para o Am
	   Bf=Bf*(1-E/getB());
	   if (A<200 && A>150) calcBf_Nix();
      //if (A<200 && A>150)Bf=Bf-1.5;
     }

	public: void calcBf_Nix()
     {
      //FissionBarrier bf ;
      Bf = bf.calcBf(A,Z);
	  }

   public: double getBf()
     {
      if (Bf==0) calcBf();
      //Bf=12.0;
      //if(Bf==0) calcBf_Nix();
      return Bf;
     }

   public: void calcBp()
     {
      double mp=938.767;
      Nucleus residual ;
      residual.init_Nucleus(A-1,Z-1,0);
      Bp=mp+residual.getMassa()-getMassa();
     }

   public: double getBp()
     {
      if (Bp==0) calcBp();
      return Bp;
     }

   public: void calcBa()
     {
      double ma=3727.4;
      Nucleus residual ;
      residual.init_Nucleus(A-4,Z-2,0);
      Ba = ma + residual.getMassa()- getMassa() ;
      //Nucleus alfa ;
      //alfa.init_Nucleus(4,2,0);
      //Ba=alfa.getMassa()+residual.getMassa()-getMassa();
     }

   public: double getBa()
     {
	   if (Ba==0) calcBa();
	   return Ba;
     }

   public: void calcB()
     {
	   double mp=938.;
	   B=A*mp-getMassa();
     }

   public: double getB()
     {
	   if(B==0) calcB();
	   return B;
     }

  public: void calcVp() // perguntar Gilson: "eletron2" e "E*" -> o mesmo p/ Bf
     {
	   double eletron2=1.44;
	   Vp=(0.70*(Z-1)*eletron2)/(ro*pow(A-1,1./3.)+1.14);
	   Vp=Vp*(1-E/getB());
     }

   public: double getVp()
     {
	   if (Vp==0) calcVp();
	   return Vp;
     }

   public: void calcVa() // perguntar Gilson: "eletron2" e "E*" -> o mesmo p/ Bf
     {
	   double eletron2=1.44;
      Va=(2*0.83*(Z-2)*eletron2)/(ro*pow(A-4,1./3.)+2.16);
      Va=Va*(1-E/getB());
     }

   public: double getVa()
     {
	   if (Va==0) calcVa();
	   return Va;
     }

   public: void Gammaf()
     {
	   double Kf,rf,Ef,En,expoente, An ;
      Kf=rf=Ef=En=expoente=0;

      rf=1.0;
      Ef=E-getBf();
      En=E-getBn();
      An = getAn() ;


      if (Ef>0 && En>0)
        {
         double Af ;
         Af = getAf() ;

         if((rf*Af*Ef) < 0)
         erro_sqrt((rf*Af*Ef)-1, "Gammaf, rf*getAf()*Ef") ;

         Kf=15*(2*sqrt(rf*Af*Ef)-1)/(4*rf*pow(A,2./3.)*En);

         if((getAf()) < 0) erro_sqrt(Af, "Gammaf, getAf()") ;
         if((rf*Ef) < 0) erro_sqrt(rf*Ef, "Gammaf, rf*Ef") ;
         if((En) < 0) erro_sqrt(En, "Gammaf, En") ;

         expoente=2*(sqrt(Af)*sqrt(Ef)-sqrt(An)*sqrt(En)) ;

         Gf=Kf*exp(expoente);
	     }
     }

   public: double getGammaf()
     {
	   if (Gf==0) Gammaf();
	   return Gf;
     }

   public: void Gammap()
     {
      double Kp,rp,Ep,En,expoente, Ap, An;
      Kp=rp=Ep=En=expoente=0;
      rp=1.0;
      Ep=E-getBp()-getVp();
      En=E-getBn();
      Ap = getAp() ;
      An = getAn() ;

      if (Ep>0 && En>0)
        {
         Kp=Ep/En;
         if(Ap < 0) erro_sqrt(Ap, "Gammap") ;
         if(Ep < 0) erro_sqrt(Ep, "Gammap") ;
         if(En < 0) erro_sqrt(En, "Gammap") ;
         expoente=2*sqrt(Ap)*(sqrt(Ep)-sqrt(En));

         expoente=2*(sqrt(Ap)*sqrt(Ep)-sqrt(An)*sqrt(En));

         Gp=Kp*exp(expoente);
        }
     }

   public: double getGammap()
     {
	   if (Gp==0) Gammap();
	   return Gp;
     }

   public: void Gammaa()
     {
      double Ka,ra,Ea,En,expoente,Aa, An;
      Ka=ra=Ea=En=expoente=0;
      ra=1.0;
      Ea=E-getBa()-getVa();
      En=E-getBn();
      Aa = getAa() ;
      An = getAn() ;

      if (Ea>0 && En>0)
        {
         Ka=Ea/En;
         if(Aa < 0) erro_sqrt(Aa, "Gammaa") ;
         if(Ea < 0) erro_sqrt(Ea, "Gammaa") ;
         if(En < 0) erro_sqrt(En, "Gammaa") ;
         expoente=2*(sqrt(Aa)*sqrt(Ea)-sqrt(An)*sqrt(En));
         Ga=Ka*exp(expoente);
        }
     }

   public: double getGammaa()
     {
	   if (Ga==0) Gammaa();
	   return Ga;
     }

   public: void Massa()
     {
      double pn=1.008665;
      double pz=1.007825;
      double a1=16.710;
      double a2=18.500;
      double a3=100.00;
      double a4=0.750;
      double delta=0;
      double f=931.478;


      if(2*ceil(double(Z)/2.)== double(Z))
        {
         if(2*ceil(double(Z-A)/2.)== double(Z-A)) delta=-36*pow(A,-3.0/4.0);
        }
      else
        {
         if(2*ceil(double(Z-A)/2.)!= double(Z-A)) delta=36*pow(A,-3.0/4.0);
        }

      m=pn*(A-Z)*f;
      m=m+pz*Z*f;
      m=m+0.511*Z;
      m=m-a1*A*f/1000.;
      m=m+a2*pow(A,2.0/3.0)*f/1000.;
      m=m+a3*(f/1000.)*pow(double(A)/2-double(Z),2)/A;
      m=m+a4*pow(double(Z),2)/pow(double(A),1./3.);
      m=m+delta*f/1000.;
    }

   public: double getMassa()
     {
	   if (m==0) Massa();
	   return m;
     }
  };

class Fissility
  {
   Nucleus n;
   Weisskopf w ;
   double W,F,NF;
   int A,Z,N;
   double E;
   double r;
   double eps, an ;

   public: void init_Fissility(int A1, int Z1, double E1)
     {
      E=E1;
      A=A1;
      Z=Z1;
      W=F=NF=0;
     }

   public: double Probfis(double Ex)
     {
      n.init_Nucleus(A,Z,Ex);
      return n.getGammaf()/(1+n.getGammaf()+n.getGammap()+n.getGammaa());
     }

   public: double Fissao()
     {
      flag_fissao = 0 ;
      double Ex=E;
      W=0;
      NF=1;
      double F;
      double rand1;
      double Gf,Gp,Ga,Gt;
      mprotons = 0 ;
      mneutrons = 0 ;
      malfa = 0 ;
      nprotons=0;
      nneutrons=0;
      nalfas=0;
      double modBa = 0 ;
      F=rand1=Gf=Gp=Ga=Gt=0;
      n.init_Nucleus(A,Z,E);
      A_spall=0;
      Z_spall=0;

      while (Ex>0 && Z>0 && A>0)
        {
         eps = 0 ;
         F=Probfis(Ex);
         W=W+NF*F;
         NF=NF*(1-F);
         Gf=n.getGammaf();
         Gp=n.getGammap();
         Ga=n.getGammaa();
         Gt=1+Gf+Gp+Ga;
         rand1=ran3() ;
         if(rand1<Gp/Gt)
           {
            A=A-1;
            Z=Z-1;
            Ex=Ex-n.getBp()-n.getVp();
            mprotons += NF ;
            nprotons++;
           }
         else
           {
            if(rand1<(Ga+Gp)/Gt)
              {
               A=A-4;
               Z=Z-2;
               modBa = fabs(n.getBa()) ;
               Ex=Ex-modBa-n.getVa() ;
               malfa += NF ;
               nalfas++;
              }
            else
              {
               if(rand1 < (Ga + Gp + Gf)/Gt)
                 {
                  ++nfissoes ;
                  flag_fissao = 1 ;
                  return W ;
                 }
               else
                 {
                  A=A-1;
                  Ex=Ex-n.getBn()-2.0;
                  if(energia_neutron == 0 && Ex > 1)
                    {
                     an = n.getAn() ;
                     //w.init_Weisskopf(Ex, an) ;
                     //eps = w.getEnergy() ;
                     //file_energy << eps << endl ;
                     //Ex=Ex-n.getBn()- eps ;
                    }
                  mneutrons += NF ;
                  nneutrons++;
                 }
               //A=A-1;
               //Ex=Ex-n.getBn()-2.0;
               //mneutrons += NF ;
               //nneutrons++;
              }
           }
        }
      A_spall = A;
      Z_spall = Z;
      return W;
     }
  };

int main()
  {
   ifstream mcef("mcef08GevW40000.dat");
   ifstream fm("ran3.dat") ;
   Fissility F ;
   tot_event = 100000;
   int i, j=1 ;
   double m_n_c, em_n_c, m_n_e, em_n_e, m_p_c, em_p_c, m_p_e, em_p_e ;
   double m_n_c2, em_n_c2, m_n_e2, em_n_e2, m_p_c2, em_p_c2, m_p_e2, em_p_e2, media_neutrons2, desvio_neutrons2, media_protons2, desvio_protons2, media_alfas2, desvio_alfas2, fissil ;

   double media_neutrons, desvio_neutrons ; //n1 = numero de neutrons da cascata + evaporacao/fissao
   double media_protons, desvio_protons ; //p1 = numero de protons da cascata + evaporacao/fissao
   double media_alfas, desvio_alfas ; //n1 = numero de alfas evaporacao/fissao

   media_desviop md;
   int spall ;
   double dsigmadn ;

   while(fm)
    {
     fm >> idum >> iff >> inext >> inextp ;
     for(int i=1; i<=55; i++) fm >> ma[i] ;
    }

   for(i=1; i<=40238; i++)
     {
      mcef >> ein >> nA[i] >> nZ[i] >> excit_energy[i] >> p_cascata[i] >> n_cascata[i] ;
     }
   spall = 0;
   double AZfinal[tot_event][3] ;
   for(i=1; i<=40238; i++)
     {
      file_pcascataW800 << p_cascata[i] << endl;
      file_azcascataW800 << nA[i] << "\t" << nZ[i] << endl;
     }
  
   file_spallW800 << "ein     A(i)   Z(i)     A(f)    Z(f)   (n)     (p)     (a)" << endl ;   
   for(i=1; i<=40238; i++)   
     {
      F.init_Fissility(nA[i],nZ[i],excit_energy[i]);
      F.Fissao();
      n2[i] = n_cascata[i] ;
      n3[i] = nneutrons ;
      p2[i] = p_cascata[i] ;
      p3[i] = nprotons ;
      n1[i] = n_cascata[i] + nneutrons ;
      p1[i] = p_cascata[i] + nprotons ;
      a1[i] = nalfas ;
      if(flag_fissao==0)
        {
         ++spall ;
         pn[spall][1] = p1[i] ;
         pn[spall][2] = n1[i] ;
         file_spallW800 << ein << "\t" << "197" << "\t" << "79" <<"\t" << A_spall << "\t" << Z_spall << "\t" << nneutrons << "\t" << nprotons << "\t" << nalfas << endl ;         
         file_pevapW800 << nprotons << endl;         
         AZfinal[spall][1] = Z_spall ;
         AZfinal[spall][2] = A_spall ;
	}
     }

   histogram(n1,40238) ;

   for(i=0; i<=250; i++)
     {
      dsigmadn = multnxs(frequencia[i],spall,184,1) ;
      if (frequencia[i] != 0)
        {
	  multn_eventoW800 << dsigmadn << "\t" << (i) << endl ;
        }
     }

   i=40238 ;

   m_n_c = md.media(n2,i) ;
   em_n_c = md.desvio_padrao(n2,i) ;
   m_n_e = md.media(n3,i) ;
   em_n_e = md.desvio_padrao(n3,i) ;

   m_p_c = md.media(p2,i) ;
   em_p_c = md.desvio_padrao(p2,i) ;
   m_p_e = md.media(p3,i) ;
   em_p_e = md.desvio_padrao(p3,i) ;

   media_neutrons = md.media(n1,i) ;
   desvio_neutrons = md.desvio_padrao(n1,i) ;

   media_protons = md.media(p1,i) ;
   desvio_protons = md.desvio_padrao(p1,i) ;

   media_alfas = md.media(a1,i) ;
   desvio_alfas = md.desvio_padrao(a1,i) ;

   m_n_c2 = trunca(m_n_c) ;
   em_n_c2 = trunca(em_n_c) ;
   m_n_e2 = trunca(m_n_e) ;
   em_n_e2 = trunca(em_n_e) ;

   m_p_c2 = trunca(m_p_c) ;
   em_p_c2 = trunca(em_p_c) ;
   m_p_e2 = trunca(m_p_e) ;
   em_p_e2 = trunca(em_p_e) ;

   media_neutrons2 = trunca(media_neutrons) ;
   desvio_neutrons2 = trunca(desvio_neutrons) ;

   media_protons2 = trunca(media_protons) ;
   desvio_protons2 = trunca(desvio_protons) ;

   media_alfas2 = trunca(media_alfas) ;
   desvio_alfas2 = trunca(desvio_alfas) ;

   fissil = trunca((nfissoes/(i))*100) ;

   file_multW800 << "Proton energy = " << ein << "MeV" <<  "\t" << "Target = " << "184W74\n" <<
     "Fissility" << "\t" << fissil << "%\n" << "_____________________________________________________\n" << "\n" ;

   file_multW800 << "ein" << "\t" <<  "n(cas)" << "\t" << 
     "std" << "\t" << "n(evap)" << "\t" << "std" << "\t" << "n(tot)" << "\t" << "std\n" ; 

   file_multW800 << ein << "\t" << m_n_c2 << "\t" << em_n_c2 << "\t" << m_n_e2 << "\t" << em_n_e2 << "\t" <<
     media_neutrons2 << "\t" << desvio_neutrons2 << "\n" << endl ;

   file_multW800 << "ein" << "\t" <<  "p(cas)" << "\t" << 
     "std" << "\t" << "p(evap)" << "\t" << "std" << "\t" << "p(tot)" << "\t" << "std\n" ;

   file_multW800 << ein << "\t" << m_p_c2 << "\t" << em_p_c2 << "\t" << m_p_e2 << "\t" << em_p_e2 << "\t" <<
     media_protons2 << "\t" << desvio_protons2 << "\n" << endl ;

   file_multW800 << "ein" << "\t" <<  "a(cas)" << "\t" << 
     "std" << "\t" << "a(evap)" << "\t" << "std" << "\t" << "a(tot)" << "\t" << "std\n" ;

   file_multW800 << ein << "\t" << "====" << "\t" << "====" << "\t" << media_alfas2 << "\t" << desvio_alfas2  << "\t" <<
     media_alfas2 << "\t" << desvio_alfas2 << "\n" << 
     "_____________________________________________________" << endl ;


   double vetor[tot_event], dsigmadn1;
   int cont;
 
   ordena(AZfinal,spall);
   cont=0;
   for (i=1 ; i<=(spall+1) ; i++)
     {
      if ((AZfinal[i][1] != AZfinal[i-1][1]) && (i != 1))
        {
         histogram(vetor,cont);
         file_histoW800 << AZfinal[i-1][1] << endl ;
         for (j=1 ; j<=250 ; j++)
           {
            dsigmadn1 = multnxs(frequencia[j],spall,184,1) ;
            if (frequencia[j] != 0)
              {
		file_histoW800 << j << "\t" << (j-AZfinal[i-1][1]) << "\t" << dsigmadn1 <<  endl ;
              }
           }
         file_histoW800 << endl ;
         cont=1 ;
         vetor[cont]=AZfinal[i][2];
        }
      else
        {
	 cont++ ;
         vetor[cont]=AZfinal[i][2];
        }
     }

//-------------------------------------------------------

  }
