ENTRY            O1708   20090311   20090323   20090318       O038
SUBENT        O1708001   20090311   20090323   20090318       O038
BIB                 12         42
TITLE      Proton-induced fission on actinide nuclei at energies
            27 and 63 MeV.
AUTHOR     (S.Isaev, R.Prieels, Th.Keutgen, J.Van Mol, Y.El Masri,
           P.Demetriou)
REFERENCE  (J,NP/A,809,1,2008)
           # (J,NP/A,809,1,2008)  Journ.: Nuclear Physics, Section A, Vol.809, p.1 (2008)  Netherlands
           #+        #NSR=2008IS03 #DOI=10.1016/j.nuclphysa.2008.05.012
INSTITUTE  (2BLGLVN)
           (2GRCATH)
           #(2BLGLVN) Catholic Univ. of Louvain, Louvain-la-Neuve, Belgium
           #(2GRCATH) NCSR Demokritos, Aghia Paraskevi, Athens, Greece
FACILITY   (CYCLO,2BLGLVN) Louvain-La-Neuve (LLN) Cyclotron
                           facility CYCLONE
           #(CYCLO) Cyclotron
           #(2BLGLVN) Catholic Univ. of Louvain, Louvain-la-Neuve, Belgium
METHOD     (TOF)
           (COINC) neutron and fission fragments in coincidence
           #(TOF) Time-of-flight
           #(COINC) Coincidence
DETECTOR   (SPEC) Three different types of detectors were used-
            Two microchannel Si-diode detector, two large position
            sensitive Multi-Wire Proportional gas Counters(MWPC),
            69 liquid-scintillator cells allowed the determination
            of the neutron energy and angular distributions
            associatted with the fission process(DEMON
            multidetector).Each DEMON counter, consisting of an
            active volume of 16 cm diameter by 20 cm depth, is
            filled with NE213 liquid-type scintillator.
            (SI) In order to monitor the reaction chamber residual
             activity(contamination from the target sputtering
             under the beam impact), up to three thin si detectors
             shielded from the target by thick aluminum bloks,
             allow the detection of the 5 MeV alpha-type particles
             emitted by the eventual actinide material
             contamination in the reaction chamber wall.
            (MWPC) for fission fragments
            (SCIN) Neutrons were detected with the multidetector
             DEMON.
           #(SPEC) Large spectrometer system
PART-DET   (FF) Fission fragments were detected.
SAMPLE     The targets were sealed between two C-12 layers, thick
           enough(50 mug/cm**2) to stop either the recoiling
           target nucleus or the evaporation heavy residues of the
           reactions but not the fission fragments. The targets
           themselves had the following thickness-Th-232 145 mug/
           cm**2, Np-237 159 mug/cm**2, U-238 180 mug/cm**2,
           Pu-239 106 mug/cm**2, Am-241 124 mug/cm**2.
ERR-ANALYS (ERR-1).The uncertainty in the target thickness.
           (ERR-2).The uncertainty in the fission fragment masses.
STATUS     (TABLE)
HISTORY    (20090109C) SB
ENDBIB              42
COMMON               2          3
ERR-1      ERR-2      
PER-CENT   PER-CENT
5.         6.
ENDCOMMON            3
ENDSUBENT           49
SUBENT        O1708011   20090311   20090323   20090318       O038
BIB                  3         23
REACTION  1(95-AM-241(P,F),,AKE,FF) table 2
          2(95-AM-241(P,F),,NU,,MSC) pre-scission multiplicity
          3(95-AM-241(P,F),,NU,,MSC) compound nucleus neutron
           multiplicity.
          4(95-AM-241(P,F),,NU,,MSC) post-scission neutron
           multiplicity.
           #(95-AM-241(P,F),,AKE,FF)   Quantity: [E] Average kinetic energy of specified fragment
           #           Process: [F] Fission
           #(95-AM-241(P,F),,NU,,MSC)   Quantity: [MFQ] Total neutron yield (nu-bar)
           #           Modifier: [MSC] Approximate definition only, see REACTION text   ==Note==This is approximate definition only, see text General purpose modifier for unusual data types. Exact definition is given in free text following the reaction code. Use with code from dict.36 which is closest to data given.
ERR-ANALYS (DATA-ERR).The uncertainty is reported by authors in
            table.
ANALYSIS    The relative contributions of pre-equilibrium neutron
           emission and compound nucleus evaporation, as well as
           of post-scission evaporation from the fission fragments
           were analyzed with moving multisource fits-
            -the emission spectra of neutron from a compound
           nuclei are described by a Maxwellian type distribution.
            -the spectra for neutron emitted from the FFs are
           described by the Watt distribution.
            -the PE source is not thermally equilibrated, the
           surface source form used to fit the corresponding
           curves and the fitting parameter are non-physical and
           hence inadeqquate to describe PE neutron emission.
           Due to the very weak PE contributions, the preceding
           observation will not significantly affect the results
           of the other source contribution.
ENDBIB              23
NOCOMMON             0          0
DATA                 9          2          9
EN         DATA      1DATA-ERR  1DATA      2DATA      2DATA      3DATA-ERR  3DATA      4DATA-ERR  4
MEV        MEV        MEV        PRT/FIS    PRT/FIS    PRT/FIS    PRT/FIS    PRT/FIS    PRT/FIS
26.5       179.1      29.3                             1.03       0.24       2.09       0.1
62.9       186.4      25.5       0.16       0.06       2.53       0.03       2.09       0.15
ENDDATA              8
ENDSUBENT           36
ENDENTRY             2

