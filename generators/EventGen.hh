/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef EventGen_pHH
#define EventGen_pHH

#include "TObject.h"
#include "TObjArray.h"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"


class EventGen: public TObject{

protected:
	EventGen();
  
public: 
	virtual ~EventGen() {} 
	virtual TObjArray& GetChannels() = 0;
	Int_t Generate(	Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt = 0 );
	virtual Int_t Generate(	Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os) = 0;
	virtual Int_t Generate(	Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os) = 0;

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(EventGen,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
