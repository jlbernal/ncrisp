/* ================================================================================
 * 
 * 	Copyright 2013 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "HyperonEventGen.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(HyperonEventGen);
#endif // CRISP_SKIP_ROOTDICT
static const Double_t E_lammda = -9.1;                 
const Double_t E_nns = -37;                
const Double_t E_nnp = -18.72;             
const Double_t E_nps = -34;                
const Double_t E_npp = -15.96;             
const Double_t SWAVE_NEUTRON_DELTA = 176 + E_lammda + E_nns; 
const Double_t PWAVE_NEUTRON_DELTA = 176 + E_lammda + E_nnp;
const Double_t SWAVE_PROTON_DELTA = 176 + E_lammda + E_nps;
const Double_t PWAVE_PROTON_DELTA = 176 + E_lammda + E_npp;
const Int_t _N = 101;
const Int_t Pass = 0.02;

HyperonEventGen::HyperonEventGen(Int_t Hymodel):EventGen(){
	HypModel = Hymodel;
	p1_nuc_idx = -1;
	p2_nuc_idx = -1;
	if (HypModel == 0){
		//modelo 1
		this->DhhP2 =  new DataHelper_Hyp("data/hyperon/C12/C12_p2_theta_0.out","P2:Cos:n1:n2:nn:p1:p2:np");
		this->DhhCos = new DataHelper_Hyp("data/hyperon/C12/C12_CosTheta_0.out","Cos:n1:n2:p1:p2:nn:np");
		//modelo 1
		SWAVE_NEUTRON_PROBABILITY = 0.601699;
		PWAVE_NEUTRON_PROBABILITY = 0.410783;
		SWAVE_PROTON_PROBABILITY = 1.85644;
		PWAVE_PROTON_PROBABILITY = 1.98328;
		np = 3.83972, nn = 1.01248;
		// the razon nn/np = 0.263685894
	} else if (HypModel == 1){
		//modelo 2
		this->DhhP2 =  new DataHelper_Hyp("data/hyperon/C12/C12_p2_theta_1.out","P2:Cos:n1:n2:nn:p1:p2:np");
		this->DhhCos = new DataHelper_Hyp("data/hyperon/C12/C12_CosTheta_1.out","Cos:n1:n2:nn:p1:p2:np");
		//modelo 2
		SWAVE_NEUTRON_PROBABILITY = 0.495303;
		PWAVE_NEUTRON_PROBABILITY = 0.332089;
		SWAVE_PROTON_PROBABILITY = 1.54715;
		PWAVE_PROTON_PROBABILITY = 1.6221;
		np = 3.16923, nn = 0.827399;
		// the razon nn/np = 0,261072563
	} else if (HypModel == 2){
		//modelo 3
		this->DhhP2 =  new DataHelper_Hyp("data/hyperon/C12/C12_p2_theta_con_recoil.out","P2:Cos:n1:n2:nn:p1:p2:np");
		this->DhhCos = new DataHelper_Hyp("data/hyperon/C12/C12_CosTheta_con_recoil.out","Cos:n1:n2:p1:p2:nn:np");
		SWAVE_NEUTRON_PROBABILITY =  0.495311;
		PWAVE_NEUTRON_PROBABILITY =  0.332088;
		SWAVE_PROTON_PROBABILITY =  1.54717;
		PWAVE_PROTON_PROBABILITY =  1.6221;
		np = 3.16926, nn = 0.827397;
		// the razon nn/np = 0.261069
	} else if (HypModel == 3){
		//modelo 4
		this->DhhP2 =  new DataHelper_Hyp("data/hyperon/C12/C12_p2_theta_sin_recoil.out","P2:Cos:n1:n2:nn:p1:p2:np");
		this->DhhCos = new DataHelper_Hyp("data/hyperon/C12/C12_CosTheta_sin_recoil.out","Cos:n1:n2:p1:p2:nn:np");
		SWAVE_NEUTRON_PROBABILITY =  0.497027;
		PWAVE_NEUTRON_PROBABILITY =  0.332376;
		SWAVE_PROTON_PROBABILITY =  1.55528;
		PWAVE_PROTON_PROBABILITY =  1.62848;
		np = 3.18376, nn = 0.829403;
		// the razon nn/np = 0.260511
	} else {
		std::cout << "Hyperon model error\n";
	}
	_N_angle = new Double_t [_N];
	for (Int_t k = 0; k < _N; k++) _N_angle[k] = -1.0 + k*Pass;
//	this->Read_Data();
}

HyperonEventGen::~HyperonEventGen(){
}

void HyperonEventGen::Read_Data(){
	this->DhhP2->ReadData();
	this->DhhCos->ReadData();
}
Double_t HyperonEventGen::get_channel_delta(ChannelType ch){
	switch((Int_t) ch){
		case 1:{
				return SWAVE_NEUTRON_DELTA;
				break;}
		case 2:{
				return  PWAVE_NEUTRON_DELTA;
				break;}
		case 3:{
				return  SWAVE_PROTON_DELTA;
				break;}
		case 4:{
				return  PWAVE_PROTON_DELTA;
				break;}
		default:{
				std::cout << "Wrong Channel entry" << std::endl;
				return NULL;}
	}
}

TGraphErrors* HyperonEventGen::get_P_BychannelANDangle(ChannelType n_case, Int_t N_Ini){
	switch((Int_t) n_case){
		case 1:{
				return this->DhhP2->GetTGraphErrorsN("P2","n1",N_Ini,_N);
				break;}
		case 2:{
				return  this->DhhP2->GetTGraphErrorsN("P2","n2",N_Ini,_N);
				break;}
		case 3:{
				return  this->DhhP2->GetTGraphErrorsN("P2","p1",N_Ini,_N);
				break;}
		case 4:{
				return  this->DhhP2->GetTGraphErrorsN("P2","p2",N_Ini,_N);
				break;}
		default:{
				std::cout << "Wrong Channel entry" << std::endl;
				return NULL;}
	}
}

TGraphErrors* HyperonEventGen::get_Cos_BychannelANDangle(ChannelType n_case){
	switch((Int_t) n_case){
		case 1:{
				return this->DhhCos->GetTGraphErrorsN("Cos","n1",0,1);
				break;}
		case 2:{
				return  this->DhhCos->GetTGraphErrorsN("Cos","n2",0,1);
				break;}
		case 3:{
				return  this->DhhCos->GetTGraphErrorsN("Cos","p1",0,1);
				break;}
		case 4:{
				return  this->DhhCos->GetTGraphErrorsN("Cos","p2",0,1);
				break;}
		default:{
				std::cout << "Wrong Channel entry" << std::endl;
				return NULL;}
	}
}

Int_t HyperonEventGen::select_channel(){
	Double_t total = np + nn;  
	Double_t psi = gRandom->Uniform();  
	if ( psi < np / total ) {
		Double_t sum = SWAVE_PROTON_PROBABILITY + PWAVE_PROTON_PROBABILITY;
		Double_t eta = gRandom->Uniform();
		return (eta < SWAVE_PROTON_PROBABILITY / sum ) ? np_swave : np_pwave;    
	}
	else {
		Double_t sum = SWAVE_NEUTRON_PROBABILITY + PWAVE_NEUTRON_PROBABILITY;
		Double_t eta = gRandom->Uniform();
		return (eta < SWAVE_NEUTRON_PROBABILITY / sum ) ? nn_swave : nn_pwave;    
	}
}

Int_t HyperonEventGen::select_nucleon_in_nucleus(NucleusDynamics& nuc, Int_t pdgId) {
	while (true) {    
		Int_t i = (Int_t)(gRandom->Uniform() * nuc.GetA());
		if ( nuc[i].PdgId() == pdgId ) 	return i;
	}	
}

Double_t HyperonEventGen::select_momentum_by_dist(TGraphErrors* tg){  
    	TAxis *xAxis = tg->GetXaxis();
    	TAxis *yAxis = tg->GetYaxis();
   	while (true) {
		Double_t p2 = gRandom->Uniform() * xAxis->GetXmax();
		// Double_t cos_theta = 2. * gRandom->Uniform() - 1.;
		Double_t z = gRandom->Uniform() * yAxis->GetXmax();   
		if ( z < tg->Eval(p2) )
	    		return p2;
	}
	xAxis->Delete();
	yAxis->Delete();
}

Double_t HyperonEventGen::select_angular_dist(TGraphErrors* tg){  
	while(true) {
		Double_t psi = 2. * gRandom->Uniform() - 1.;
		Double_t y = gRandom->Uniform() * tg->GetYaxis()->GetXmax();   
		// Double_t f = TMath::Exp(-6.02945 -4.4791 * psi );
		if ( y < tg->Eval(psi) ) return  psi;      
	}
}

TVector3 HyperonEventGen::nucleon_center_position(Double_t radius){
	TVector3 vec(0.,0.,0.);
	double x,y,z;
	Double_t r = radius * TMath::Power(gRandom->Uniform(),1./3.) ;
	gRandom->Sphere(x,y,z,r);
//	Double_t theta = TMath::ACos(1 - 2* gRandom->Uniform());
//	Double_t phi =	TMath::TwoPi()*gRandom->Uniform();
//	vec.SetPtThetaPhi(r,theta,phi);
	vec.SetXYZ(x,y,z);
	return vec;
}

Int_t HyperonEventGen::Generate( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char *opt, std::ostream *os){
	ChannelType channel = (ChannelType)select_channel();// seleciona o canal nn(S,P) ou np(S,P)
	std::cout << "selected Channel type = " << channel << std::endl;
	p1_nuc_idx = select_nucleon_in_nucleus(nuc, CrispParticleTable::neutron_ID);// selectiona as particulas do nucleo.
	Int_t p2_ID = (channel == np_swave || channel == np_pwave) ? CrispParticleTable::proton_ID : CrispParticleTable::neutron_ID; // selectiona o PdgId da partícula 2.
	do{
		p2_nuc_idx = select_nucleon_in_nucleus(nuc, p2_ID); 				
	}while (p1_nuc_idx == p2_nuc_idx);
	ParticleDynamics nucleon1(CrispParticleTable::neutron_ID);
	ParticleDynamics nucleon2(p2_ID);  
	Double_t Delta_F = get_channel_delta(channel);// obtem a distribuicao e o valor de delta correto para cada canal.
	TVector3 p1_vect(0., 0., 0.);
	TVector3 p2_vect(0., 0., 0.);  
	const Double_t m = CrispParticleTable::p_mass;
	const Double_t M_f = m * (nuc.GetA() - 2);
	Double_t __M = M_f + m;
	TGraphErrors *TG_p2;
	TGraphErrors *TG_Cos;
	Double_t cos_theta, p1, p2;
	do{
		TG_Cos = get_Cos_BychannelANDangle(channel);
		cos_theta = select_angular_dist(TG_Cos);  
		Int_t Ind = 0;
/* This block return an integer number refers to the index for cos theta
*********************/
		if ((cos_theta <=1) && (cos_theta >= -1)){
			Double_t Min_Val = 100.;
			for(Int_t k = 0; k < _N ; k++){
				Double_t Val = abs( _N_angle[k] - cos_theta);
				if (Val < Min_Val){
					Min_Val = Val;
					Ind = k;
				}
			}
		} else std::cout << "there is an error on cos Theta's selection" << std::endl ;
		TG_p2 = get_P_BychannelANDangle(channel,Ind);
		p2 = select_momentum_by_dist(TG_p2); 
		p1 = ( -p2 * cos_theta * m + TMath::Sqrt( sqr(p2 * cos_theta * m) - sqr(__M * p2) + 2. * Delta_F * M_f * m * __M ) ) / __M;   
/*********************/   
// Adjusting to the nuclear media.
//		Double_t alp = sqrt( 1 + 80/Delta_F); 
//		cout << " los valores de momento son: " << p1 << " y " << p2 << endl;
//		Double_t p1N = p1*alp; Double_t p2N = p2*alp;
//		Double_t p1N = sqrt( p1*p1 + m*m*(1 - sqr(CPT::effn) ) );
//		Double_t p2N = sqrt( p2*p2 + m*m*(1 - sqr(CPT::effn) ) );
		Double_t p1N = sqrt( TMath::Power( (sqrt( p1*p1 + m*m) + 40) ,2.) - m*m);
		Double_t p2N = sqrt( TMath::Power( (sqrt( p2*p2 + m*m) + 40) ,2.) - m*m);
// Momento Vetorial p1 e p2, 
		Double_t sin_theta = TMath::Sqrt(1. - sqr(cos_theta));
		p2_vect.SetXYZ( 0., 0., p1N); 
		p1_vect.SetXYZ( 0., p2N * sin_theta, p2N * cos_theta );

/// Dando uma direcao aleatoria aos vetores p1_vect e p2_vect  
		Double_t theta = TMath::ACos(gRandom->Uniform(-1,1));
		Double_t phi = gRandom->Uniform() * TMath::TwoPi();
		p1_vect.RotateY(theta); p2_vect.RotateY(theta);
		p1_vect.RotateX(phi); p2_vect.RotateX(phi);      
//  set nucleus characters momentum and mass
		nucleon1.SetMass( nucleon1.GetInvariantMass() * CrispParticleTable::effn );
		nucleon2.SetMass( nucleon2.GetInvariantMass() * CrispParticleTable::effn );    
		nucleon1.SetMomentum(p1_vect);
		nucleon2.SetMomentum(p2_vect);
	} while ( !nuc.NucleonPairChange(p1_nuc_idx, p2_nuc_idx,nucleon1, nucleon2) );
	Par[1] = p1_nuc_idx;
	Par[2] = p2_nuc_idx;
	Par[3] = cos_theta;
	Par[4] = p1;
	Par[5] = p2;
	TVector3 pos1, pos2;
	pos1 = nucleon_center_position(nuc.GetRadium());	//decay resulting nucleons putted randomly on the nucleus
	pos2 = nucleon_center_position(nuc.GetRadium());
//	pos1 = nucleon_center_position(nuc.GetRadium()*0.0001);  //decay resulting nucleons putted at the center of the nucleus
//	pos2 = nucleon_center_position(nuc.GetRadium()*0.0001);
	nuc[p1_nuc_idx].SetPosition(pos1);
	nuc[p2_nuc_idx].SetPosition(pos2);
	TG_p2 ->Delete();
	TG_Cos->Delete();
	return 1;
}

//__________________________________________________________________________________________

Int_t HyperonEventGen::Generate(Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  
  return -1;
}

