/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "AbstractChannel.hh"

//_________________________________________________________________________________________________										

AbstractChannel::AbstractChannel():TObject(), cs(){

	counts = 0;
	blocked = 0;
}

//_________________________________________________________________________________________________										

AbstractChannel::AbstractChannel(const CrossSectionChannel& ch):TObject(), cs(){

	ch.Copy(cs);
	counts = 0;
	blocked = 0;
}

//_________________________________________________________________________________________________										

AbstractChannel::AbstractChannel(AbstractChannel& u):TObject(), cs(u.CrossSection()){

	// Copy(*this);
	counts = u.counts;
	blocked = u.blocked;
}

//_________________________________________________________________________________________________										

AbstractChannel::~AbstractChannel(){} 

//_________________________________________________________________________________________________										

