/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef bb_cross_sections_pHH
#define bb_cross_sections_pHH

#include "TLorentzVector.h"
#include "base_defs.hh"
#include "BasicException.hh"
#include "CrispParticleTable.hh"
#include "Dynamics.hh"

///
//_________________________________________________________________________________________________										
// Kikuchi-Kawai Cross Section.
 
double bb_elastic_cross_section( Dynamics* q1 , Dynamics* q2, double* cch );

///
//_________________________________________________________________________________________________										

double bb_one_pion_cross_section( Dynamics* p1, Dynamics* p2, double* cch );

///
//_________________________________________________________________________________________________										
// Reacao p p --> p p pi+ pi-

double pp_pion_twopion_1_cross_section ( Dynamics* p1, Dynamics* p2, double* cch );

///
//_________________________________________________________________________________________________										
// Reacao p p --> p n pi+ pi0

double pp_pion_twopion_2_cross_section ( Dynamics* p1, Dynamics* p2, double* cch );

///
//_________________________________________________________________________________________________										
// Reacao p n --> p n pi+ pi-

double pn_pion_twopion_1_cross_section( Dynamics* p1, Dynamics* p2, double* cch );

#endif
