/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "photon_actions.hh"

/*
 * NOTE: all over this file, resonance_mass routine already takes care of effective mass
 */

Double_t RhoN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

 	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
 	double z = VM_Mand_z(s,0,t,MN,MVO);
 	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
 	return f* 1000;
}
Double_t OmegaN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000;
}
Double_t PhiN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::phi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtPhi(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000;
}

Double_t JPsiN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::J_Psi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtJ_Psi(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000;
}
//_________________________________________________________________________________________________										
void createMeson( const ParticleDynamics& gamma, const ParticleDynamics& nucleon, std::vector<ParticleDynamics>& out_particles, double s  ){
	if ( out_particles.size() == 2 ) {    
		TLorentzVector pboost = gamma.Momentum() + nucleon.Momentum();
		// rotaciona pboost para o referencial do centro de massa.
		TLorentzVector pcm = lorentzRotation(pboost, pboost);
		double m1 = out_particles[0].GetMass(), m2 = out_particles[1].GetMass();
		double pm2 = ( ( s - TMath::Power( m1 + m2, 2) ) * ( s - TMath::Power( m1 - m2, 2) ) ) / ( 4. * s ); 
		if ( pm2 < 0. ) {
			std::vector<ParticleDynamics> v;
			v.push_back(out_particles[0]); v.push_back(out_particles[1]);
			throw BasicException( "photon_n action (...): Negative Kinetic Energy\n",v);
		}
		double pm = TMath::Sqrt(pm2);
		double gamma_energy = gamma.Momentum().E() / 1000.;
		// get the meson id ... 
		ParticleDynamics meson;
		Int_t meson_id;
		if ( CPT::IsNucleon(out_particles[0].PdgId()) || CPT::IsImplRessonance(out_particles[0].PdgId()) ) {
			meson = out_particles[1]; meson_id = meson.PdgId();
		} 
		else {
			meson = out_particles[0]; meson_id = meson.PdgId();
		} 
		double cosTheta = 0;  
		if  ( CPT::IsPion( meson_id ) ) cosTheta = one_pion_angular_dist(gamma_energy, nucleon, meson);
		else if  ( CPT::IsRho( meson_id ) ){
			//std::cout <<  "RhoN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << meson.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << std::endl;
//			RhoN_elas_AngDist;

			/*
			* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
			* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
			* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
			* compromising the calculation
			*/
			double MVO = CPT::rho_0_mass/1000.; // mass in GeV (VERY IMPORTANT)
			double MN = CPT::n_mass/1000.;
			double MVO_2 = TMath::Power(MVO,2);
			double MN_2 = TMath::Power(MN,2);
			double t_min = -2.5;
			s = s/1000/1000; //MeV^2 -> GeV^2
			double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)
		
			if(cosMin < -1)	cosMin = -1.;	
		
			TF1 *f1 = new TF1("RhoN_AngDist",RhoN_AngDist,cosMin,1,1);
			f1->SetParameter(0,s); //GeV^2
			f1->SetNpx(1000);
			cosTheta = f1->GetRandom();
			//std::cout <<  "RhoN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f1;
		} 
		else if  ( CPT::IsOmega( meson_id ) ){
			//std::cout <<  "OmgN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << meson.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << std::endl;
//			OmegaN_elas_AngDist;

			/*
			* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
			* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
			* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
			* compromising the calculation
			*/
			double MVO = CPT::omega_mass/1000.; // mass in GeV (VERY IMPORTANT)
			double MN = CPT::n_mass/1000.;
			double MVO_2 = TMath::Power(MVO,2);
			double MN_2 = TMath::Power(MN,2);
			double t_min = -2.5;
			s = s/1000/1000; //MeV^2 -> GeV^2
			double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)

			if(cosMin < -1)	cosMin = -1.;
	
			TF1 *f2 = new TF1("OmegaN_AngDist",OmegaN_AngDist,cosMin,1,1);
			f2->SetParameter(0,s); //GeV^2
			f2->SetNpx(1000);
			cosTheta = f2->GetRandom();
			//std::cout <<  "OmgN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f2;
		} 
		else if  ( CPT::IsPhi( meson_id ) ) {
			//std::cout <<  "PhiN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << meson.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << std::endl;
//			PhiN_elas_AngDist;

			/*
			* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
			* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
			* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
			* compromising the calculation
			*/
			double MVO = CPT::phi_mass/1000.; // mass in GeV (VERY IMPORTANT)
			double MN = CPT::n_mass/1000.;
			double MVO_2 = TMath::Power(MVO,2);
			double MN_2 = TMath::Power(MN,2);
			double t_min = -2.5;
			s = s/1000/1000; //MeV^2 -> GeV^2
			double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)
		
			if(cosMin < -1)	cosMin = -1.;

			TF1 *f3 = new TF1("PhiN_AngDist",PhiN_AngDist,cosMin,1,1);
			f3->SetParameter(0,s); //GeV^2
			f3->SetNpx(1000);
			cosTheta = f3->GetRandom();
			//std::cout <<  "PhiN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f3;
		}else if  ( CPT::IsJ_Psi( meson_id ) ) {
//			std::cout <<  "JPsiN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << meson.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << std::endl;
//			PhiN_elas_AngDist;

			/*
			* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
			* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
			* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
			* compromising the calculation
			*/
			double MVO = CPT::J_Psi_mass/1000.; // mass in GeV (VERY IMPORTANT)
			double MN = CPT::n_mass/1000.;
			double MVO_2 = TMath::Power(MVO,2);
			double MN_2 = TMath::Power(MN,2);
			double t_min = -2.5;
			s = s/1000/1000; //MeV^2 -> GeV^2
			double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)
		
			if(cosMin < -1)	cosMin = -1.;

			TF1 *f3 = new TF1("JPsiN_AngDist",JPsiN_AngDist,cosMin,1,1);
			f3->SetParameter(0,s); //GeV^2
			f3->SetNpx(1000);
			cosTheta = f3->GetRandom();
//			std::cout <<  "JPsiN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f3;
		} else {
			cosTheta = 1. - 2. * gRandom->Uniform();
			std::cout << " Error in particle selection \n";
		}

		if(cosTheta < -1. || cosTheta > 1) std::cout << "Error in photon_action, cosTheta = " << cosTheta << std::endl;
		if(pcm.M() < 0) std::cout << "Error in photon_action, pcm.M() = " << pcm.M() << std::endl;

		double sinTheta = sqrt( 1. - sqr(cosTheta) ) ;    
		double phi = gRandom->Uniform() * TMath::TwoPi();      
		TLorentzVector k1( pm * sinTheta * cos(phi), pm * sinTheta * sin(phi), pm * cosTheta, sqrt(sqr(pm) + sqr(m1)) );
		TLorentzVector k2 = pcm - k1;
		out_particles[0].SetMomentum(inverseLorentzRotation(pboost, k1)); 		// back to lab frame ...
		out_particles[1].SetMomentum(inverseLorentzRotation(pboost, k2));		// back to lab frame ...

		double s2 = (out_particles[0].Momentum() + out_particles[1].Momentum()).M2();

		if( fabs((pboost.M2() - s2)/pboost.M2()) > 1e-7 ){
			//std::cout << "Energy conservation failure at photon action when creating a meson" << std::endl;
			std::cout << "out_particles[0].Momentum().Print(): " << std::endl;
			out_particles[0].Momentum().Print();
			std::cout << "out_particles[1].Momentum().Print(): " << std::endl;
			out_particles[1].Momentum().Print();
			throw BasicException("Energy conservation failure at photon action when creating a meson\n");
		}

	}  
}
//_________________________________________________________________________________________________										
bool put_Ressonance( ParticleDynamics& gamma, Int_t nix, NucleusDynamics& nuc, MesonsPool& mpool, Int_t rss_id){
	ParticleDynamics rss(rss_id);    
	TLorentzVector W = gamma.Momentum() + nuc[nix].Momentum();
	double sqrt_s = W.M();
	double em_upper = sqrt_s;
	double rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass( rss_mass );
	//TODO: melhorar o corte inferior no ajuste da massa da ressonancia.

	double pm = sqrt(sqr(W.E()) - sqr(rss.GetMass()));  
	rss.SetMomentum(W.Vect().Unit()*pm); //same direction as center-of-mass momentum
	rss.SetPosition(nuc[nix].Position());
	rss.HalfLife();    
	if (nuc.PutRessonance( nix, rss ))  
		return true;
	else
		return false;
}
//_________________________________________________________________________________________________										
bool qd_photonAction( ParticleDynamics* gamma, Int_t&, NucleusDynamics* nuc, MesonsPool*) {
	Int_t 	idx1=0, idx2=0;
	do {
		idx1 = (Int_t)( gRandom->Uniform() * nuc->GetA() );
		idx2 = (Int_t)( gRandom->Uniform() * nuc->GetA() );    

	} while( (*nuc)[idx1].PdgId() == (*nuc)[idx2].PdgId() );    
	TLorentzVector p = gamma->Momentum() + (*nuc)[idx1].Momentum() + (*nuc)[idx2].Momentum();
	std::vector<ParticleDynamics> v;
	v.push_back((*nuc)[idx1]); 
	v.push_back((*nuc)[idx2]);
	particlesOutMomentum(p, v);  
	if ((nuc->ChangeNucleon(idx1, v[0]))&&(nuc->ChangeNucleon(idx2, v[1]))){
		return true;    
	}
	return false;
}
//_________________________________________________________________________________________________										
// The particles interact with each other via pion exchange.
bool op_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
	double s =  (gamma->Momentum() + (*nuc)[nix].Momentum()).M2() ;
	Int_t id = (*nuc)[nix].PdgId();  
	Int_t pion_id = 0;
	Int_t new_nuc_id = 0;
	TVector3 r = (*nuc)[nix].Position();
	double y = gRandom->Uniform();	  
	if ( y <= 1./3. ) { 
		pion_id = CPT::pion_0_ID;
		new_nuc_id = id;
	}
	else {
		pion_id = ( id == CPT::proton_ID ) ? CPT::pion_p_ID : CPT::pion_m_ID;
		new_nuc_id = ( id == CPT::proton_ID) ? CPT::neutron_ID: CPT::proton_ID;
	}  
	std::vector<ParticleDynamics> v;   
	v.push_back( ParticleDynamics(pion_id) );
	v[0].SetPosition(r);
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v.push_back( ParticleDynamics(new_nuc_id) );
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	createMeson( *gamma, (*nuc)[nix], v , s);
	if ( nuc->ChangeNucleon( nix, v[1] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		v[0].Position() += dr0;
		(*nuc)[nix].Position() += dr1;
		mpool->PutMeson(v[0]);
		return true;
	}
	return false;  
}
//_________________________________________________________________________________________________										
bool p33_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
	// P33
	// if id == neutronID then a delta(1232)0
	// if id == protonID then a delta(1232)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::p33_0_ID: CPT::p33_p_ID;  
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool f37_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ) {
	// F37
	// if id == neutronID then a Delta(1950)0
	// if id == protonID then a Delta(1950)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::f37_0_ID: CPT::f37_p_ID;  
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool p11_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ) {
	// P11
	// if id == neutronID then a N(1440)0
	// if if == protonID then a N(1440)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::p11_0_ID: CPT::p11_p_ID;
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool s11_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
	// S11
	// if id == neutronID then a N(1535)0
	// if if == protonID then a N(1535)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::s11_0_ID: CPT::s11_p_ID;
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool d13_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ) { 
	// D13
	// if id == neutronID then a N(1520)0
	// if if == protonID then a N(1520)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::d13_0_ID: CPT::d13_p_ID;
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool f15_photonAction ( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
	// F15
	// if id == neutronID then a Delta(1680)0   
	// if if == protonID then a Delta(1680)+
	Int_t id = (*nuc)[nix].PdgId();
	Int_t kb_id = ( id == CPT::neutron_ID ) ? CPT::f15_0_ID: CPT::f15_p_ID;
	return put_Ressonance(*gamma, nix, *nuc, *mpool, kb_id);
}
//_________________________________________________________________________________________________										
bool rho_0_photonAction( ParticleDynamics* p, Int_t &nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool ){       
	Int_t k = nucleonIndex;  
	double s =  (p->Momentum() + (*nuc)[k].Momentum()).M2() ;
	ParticleDynamics nucleon = (*nuc)[k];   
	std::vector<ParticleDynamics> v;
	v.push_back( ParticleDynamics(CPT::rho_0_ID) );
	v.push_back( nucleon ); // push to vec a copy of nucleon.
	TVector3 dr(nucleon.Position().X(),p->Position().Y(),p->Position().Z()); // same y,z's components of gamma and x's components of the nucleon
	v[0].SetPosition(dr); // set meson and nucleon position 
	v[1].SetPosition(nucleon.Position());
	// the upper limit: rho_invariant_mass + rho_width ...
	double em_upper = v[0].GetMass() + v[0].GetTotalWidth();
	em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
	double rho_mass = resonance_mass( em_upper, CPT::rho_0_ID ); //NOTE: resonance_mass routine already takes care of effective mass
	//std::cout << "mass 1 = " << v[0].GetMass() << ", mass 2 = " <<  v[1].GetMass() << ", massp 1 = " <<  v[0].Momentum().Mag() << ", massp 2 = " << v[1].Momentum().Mag() << std::endl;
	v[0].SetMass(rho_mass);      
	v[0].SetLifeTime( v[0].HalfLife() ); 
	//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	createMeson( *p,  (*nuc)[k], v, s );  
	if ( nuc->ChangeNucleon( nucleonIndex,  v[1] ) ){
		// add a rho meson into the Mesons Pool.
		//v[0].Momentum().Print();
		//v[1].Momentum().Print();
		mpool->PutMeson(v[0]);
		return true;
	}  
	return false;  
}
//_________________________________________________________________________________________________	
bool rho_photonAction( ParticleDynamics* p, Int_t &nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool ){       
	Int_t k = nucleonIndex;
	double s =  (p->Momentum() + (*nuc)[k].Momentum()).M2() ;
	ParticleDynamics nucleon;
	ParticleDynamics meson;
	if ( (*nuc)[k].PdgId() == CrispParticleTable::neutron_ID ) {
		nucleon = ParticleDynamics(CrispParticleTable::proton_ID);
		meson = ParticleDynamics(CPT::rho_m_ID);
	}
	if ( (*nuc)[k].PdgId() == CrispParticleTable::proton_ID ) {
		nucleon = ParticleDynamics(CrispParticleTable::neutron_ID);
		meson = ParticleDynamics(CPT::rho_p_ID);
  	}
	std::vector<ParticleDynamics> v;
	v.push_back( meson );
	v.push_back( nucleon ); // push to vec a copy of nucleon.
	v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);       
	TVector3 dr((*nuc)[k].Position().X(),p->Position().Y(),p->Position().Z()); // same y,z's components of gamma and x's components of the nucleon
	v[0].SetPosition(dr); // set meson and nucleon position 
	v[1].SetPosition((*nuc)[k].Position());
	// the upper limit: rho_invariant_mass + rho_width ...
	double em_upper = v[0].GetMass() + v[0].GetTotalWidth();
	em_upper = ( em_upper > TMath::Sqrt(s) -  v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
	//NOTE: resonance_mass routine already takes care of effective mass
	double rho_mass = (meson.PdgId() == CPT::rho_m_ID ) ? resonance_mass( em_upper, CPT::rho_m_ID ):resonance_mass( em_upper, CPT::rho_p_ID);
//	std::cout << "mass 1 = " << v[0].GetMass() << ", mass 2 = " <<  v[1].GetMass() << ", massp 1 = " <<  v[0].Momentum().Mag() << ", massp 2 = " << v[1].Momentum().Mag() << std::endl; 
	v[0].SetMass(rho_mass);      
	v[0].SetLifeTime( v[0].HalfLife() ); 
	//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	createMeson( *p,  (*nuc)[k], v, s );  
	// Check if is Pauli Permitible, if it is then, update the momentum for nucleon and for meson rho, else return false.
	if ( nuc->ChangeNucleon( nucleonIndex,  v[1] ) ){
		// add a rho meson into the Mesons Pool.
		//v[0].Momentum().Print();
		//v[1].Momentum().Print();
		mpool->PutMeson(v[0]);
		return true;
	}  
	return false;  
}
//_________________________________________________________________________________________________	

bool omega_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool ){
	Int_t k = nucleonIndex;  
	double s =  (p->Momentum() + (*nuc)[k].Momentum()).M2() ;
	ParticleDynamics nucleon = (*nuc)[k];   
	// container for the scattered particles
	std::vector<ParticleDynamics> v;
	v.push_back( ParticleDynamics(CPT::omega_ID) );
	v.push_back( nucleon ); // push to vec a copy of nucleon.
	TVector3 dr(nucleon.Position().X(),p->Position().Y(),p->Position().Z()); // same y,z's components of gamma and x's components of the nucleon
	v[0].SetPosition(dr); // set meson and nucleon position 
	v[1].SetPosition(nucleon.Position());
	// the upper limit: omega_invariant_mass + omega_width ...
	double em_upper = v[0].GetMass() + v[0].GetTotalWidth();
	em_upper = ( em_upper > TMath::Sqrt(s) -  v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
	double omega_mass = resonance_mass( em_upper, CPT::omega_ID ); //NOTE: resonance_mass routine already takes care of effective mass
	v[0].SetMass(omega_mass);      
	v[0].SetLifeTime( v[0].HalfLife() ); 
	//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	//std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", omega_mass = " << omega_mass << std::endl;
	createMeson( *p,  (*nuc)[k], v, s );  
	if ( nuc->ChangeNucleon( nucleonIndex,  v[1] ) ){
		// add a rho meson into the Mesons Pool.
		//v[0].Momentum().Print();
		//v[1].Momentum().Print();
		mpool->PutMeson(v[0]);
		return true;
	}  
	return false;  
}
//_________________________________________________________________________________________________	
bool phi_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool ){
	Int_t k = nucleonIndex;  
	double s =  (p->Momentum() + (*nuc)[k].Momentum()).M2() ;
	ParticleDynamics nucleon = (*nuc)[k];   
	// container for the scattered particles
	std::vector<ParticleDynamics> v;
	// Create the phi meson 
	v.push_back( ParticleDynamics(CPT::phi_ID) );
	v.push_back( nucleon ); // push to vec a copy of nucleon.
	// set phi position 
	TVector3 dr(nucleon.Position().X(),p->Position().Y(),p->Position().Z()); // same y,z's components of gamma and x's components of the nucleon
	v[0].SetPosition(dr); // set meson and nucleon position 
	v[1].SetPosition(nucleon.Position());
	// the upper limit: phi_invariant_mass + phi_width ...
	double em_upper = v[0].GetMass() + v[0].GetTotalWidth();
	em_upper = ( em_upper > TMath::Sqrt(s) -  v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
	double phi_mass = resonance_mass( em_upper, CPT::phi_ID ); //NOTE: resonance_mass routine already takes care of effective mass
	v[0].SetMass(phi_mass);      
	v[0].SetLifeTime( v[0].HalfLife() ); 
	//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	//std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", phi_mass = " << phi_mass << std::endl;
	createMeson( *p,  (*nuc)[k], v, s );  
	// Check if is Pauli Permitible, if it is then, update the momentum for 
	// nucleon and for meson rho, else return false.
	if ( nuc->ChangeNucleon( nucleonIndex,  v[1] ) ){
		// add a rho meson into the Mesons Pool.
		//v[0].Momentum().Print();
		//v[1].Momentum().Print();
		mpool->PutMeson(v[0]);
		return true;
	}  
	return false;  
}
//_________________________________________________________________________________________________

bool J_Psi_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool ){
	Int_t k = nucleonIndex;  
	double s =  (p->Momentum() + (*nuc)[k].Momentum()).M2() ;
	ParticleDynamics nucleon = (*nuc)[k];   
	// container for the scattered particles
	std::vector<ParticleDynamics> v;
	// Create the rho meson 
	v.push_back( ParticleDynamics(CPT::J_Psi_ID) );
	v.push_back( nucleon ); // push to vec a copy of nucleon.
	// set J/Psi position 
	TVector3 dr(nucleon.Position().X(),p->Position().Y(),p->Position().Z()); // same y,z's components of gamma and x's components of the nucleon
	v[0].SetPosition(dr); // set meson and nucleon position 
	v[1].SetPosition(nucleon.Position());
	// the upper limit: J_Psi_invariant_mass + J_Psi_width ...
	double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
	em_upper = ( em_upper > TMath::Sqrt(s) -  v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
	double psi_mass = resonance_mass( em_upper, CPT::J_Psi_ID ); //NOTE: resonance_mass routine already takes care of effective mass
	v[0].SetMass(psi_mass);      
	v[0].SetLifeTime( v[0].HalfLife() ); 
//	std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	createMeson( *p,  (*nuc)[k], v, s );  
	// Check if is Pauli Permitible, if it is then, update the momentum for 
	// nucleon and for meson, else return false.
	if ( nuc->ChangeNucleon( nucleonIndex,  v[1] ) ){
		// add a J/Psi meson into the Mesons Pool.
//		v[0].Momentum().Print();
//		v[1].Momentum().Print();
		mpool->PutMeson(v[0]);
		return true;
	}  
	return false;  
}

///====================================================   2 pions   =====================================================

bool gp_pPipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){

	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
// 	cout << endl;
// 	cout << "before interaction: " << endl;
// 	gamma->Momentum().Print();
// 	(*nuc)[nix].Momentum().Print();
// 	cout << endl;
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);
// 		std::cout << "two pions channel!" << std::endl;
// 		cout << "nucleon: " << (*nuc)[nix].PdgId() << endl;
// 		(*nuc)[nix].Momentum().Print();
// 		(*nuc)[nix].Position().Print();
// 		cout << "pion 1: " << v[1].PdgId()  << endl;
// 		v[1].Momentum().Print();
// 		v[1].Position().Print();
// 		cout << "pion 2: " << v[2].PdgId() << endl;
// 		v[2].Momentum().Print();
// 		v[2].Position().Print();
		
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_nPipPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){

	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_nPipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){ 

	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};
	
	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);		
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);		
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 3, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);		
		return true;
	}
	return false;  
}

///====================================================   3 pions   =====================================================

bool gp_pPipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_nPip2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n2PipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_nPipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[4] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 4, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		return true;
	}
	return false;  
}

///====================================================   4 pions   =====================================================

bool gp_p2Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_pPipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_nPip3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n2PipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n2Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_nPipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = id;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[5] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 5, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		return true;
	}
	return false;  
}


///====================================================   5 pions   =====================================================

bool gp_p2Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_pPipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_nPip4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n2PipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n3Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n2Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_nPipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_p2Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[6] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 6, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		return true;
	}
	return false;  
}

///====================================================   6 pions   =====================================================

bool gp_p3Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_pPipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p2Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_nPip5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n2PipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_n3Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n3Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_nPipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_n2Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_pPip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_p2Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[7] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., v[6].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 7, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		return true;
	}
	return false;  
}

///====================================================   7 pions   =====================================================

bool gp_p3Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gp_p7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_pPipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_p2Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_nPip6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n2PipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n3Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n4Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n3Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}

//__________________________________________________________________________________________________________________________

bool gn_nPipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n2Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_pPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_pPip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_p2Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_p3Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[8] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 8, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		return true;
	}
	return false;  
}

///====================================================   8 pions   =====================================================

bool gp_p4Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_p8Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_pPipPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_p2Pip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_p3Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_nPip7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n2PipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n3Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gp_n4Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n4Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n8Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_nPipPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n2Pip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_n3Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::neutron_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_pPim7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_pPip2Pim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_p2Pip3Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}


//__________________________________________________________________________________________________________________________

bool gn_p3Pip4PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool ){
   
	//Int_t id = (*nuc)[nix].PdgId();
	Int_t new_nuc_id = CPT::proton_ID;
	TVector3 r = (*nuc)[nix].Position();

	std::vector<ParticleDynamics> v;   //vector of products
	v.push_back( ParticleDynamics(new_nuc_id) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(CPT::pion_0_ID) );
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetPosition(r);
	v[2].SetPosition(r);
	v[3].SetPosition(r);
	v[4].SetPosition(r);
	v[5].SetPosition(r);
	v[6].SetPosition(r);
	v[7].SetPosition(r);
	v[8].SetPosition(r);
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	v[3].SetMass( v[3].GetInvariantMass() * CPT::effn );
	v[4].SetMass( v[4].GetInvariantMass() * CPT::effn );
	v[5].SetMass( v[5].GetInvariantMass() * CPT::effn );
	v[6].SetMass( v[6].GetInvariantMass() * CPT::effn );
	v[7].SetMass( v[7].GetInvariantMass() * CPT::effn );
	v[8].SetMass( v[8].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = gamma->Momentum() * (1./1000.) + (*nuc)[nix].Momentum() * (1./1000.);
	
	//(Momentum, Energy units are Gev/C, GeV)
	Double_t masses[9] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000., 
				v[3].GetMass()/1000., v[4].GetMass()/1000., v[5].GetMass()/1000., 
				v[6].GetMass()/1000., v[7].GetMass()/1000., v[8].GetMass()/1000.};

	TGenPhaseSpace event;
	event.SetDecay(W, 9, masses);
	
	event.Generate();
	
	v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
	v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
	v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
	v[3].SetMomentum( *(event.GetDecay(3)) * 1000. ); //back to MeV	
	v[4].SetMomentum( *(event.GetDecay(4)) * 1000. ); //back to MeV
	v[5].SetMomentum( *(event.GetDecay(5)) * 1000. ); //back to MeV
	v[6].SetMomentum( *(event.GetDecay(6)) * 1000. ); //back to MeV
	v[7].SetMomentum( *(event.GetDecay(7)) * 1000. ); //back to MeV
	v[8].SetMomentum( *(event.GetDecay(8)) * 1000. ); //back to MeV
	
	if ( nuc->ChangeNucleon( nix, v[0] ) ){     
		// Passed by Pauli Blocking ... 
		TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
		TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
		TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
		TVector3 dr3 = v[3].Momentum().Vect(); dr3 *= .01 / v[3].Momentum().E();
		TVector3 dr4 = v[4].Momentum().Vect(); dr4 *= .01 / v[4].Momentum().E();
		TVector3 dr5 = v[5].Momentum().Vect(); dr5 *= .01 / v[5].Momentum().E();
		TVector3 dr6 = v[6].Momentum().Vect(); dr6 *= .01 / v[6].Momentum().E();
		TVector3 dr7 = v[7].Momentum().Vect(); dr7 *= .01 / v[7].Momentum().E();
		TVector3 dr8 = v[7].Momentum().Vect(); dr8 *= .01 / v[8].Momentum().E();
		(*nuc)[nix].Position() += dr0;
		v[1].Position() += dr1;
		v[2].Position() += dr2;
		v[3].Position() += dr3;
		v[4].Position() += dr4;
		v[5].Position() += dr5;
		v[6].Position() += dr6;
		v[7].Position() += dr7;
		v[8].Position() += dr8;
		mpool->PutMeson(v[1]);
		mpool->PutMeson(v[2]);	
		mpool->PutMeson(v[3]);	
		mpool->PutMeson(v[4]);	
		mpool->PutMeson(v[5]);
		mpool->PutMeson(v[6]);
		mpool->PutMeson(v[7]);
		mpool->PutMeson(v[8]);
		return true;
	}
	return false;  
}
