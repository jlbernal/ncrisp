/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "angular_dists.hh"

//_________________________________________________________________________________________________										
// One pion creation angular distribution.
// return cos(theta) where theta is the azimutal angle
// @param energy in GeV

Double_t one_pion_angular_dist( Double_t energy, const ParticleDynamics& nucleon, const ParticleDynamics& pion ){

	Double_t tg = energy;
	Double_t tg2 = sqr(energy);
	Double_t costhetag = 0.; 
  
	Int_t nucleon_id = nucleon.PdgId();
	Int_t pion_id = pion.PdgId();
    
	// resultant nucleon id
	Int_t p_id = nucleon_id;

	if ( pion_id == CPT::pion_m_ID ) 
		p_id = CPT::proton_ID;
  
	if ( pion_id == CPT::pion_p_ID )
		p_id = CPT::neutron_ID;
   
	Double_t x = gRandom->Uniform();
  
	if ( tg > 1.3 ){
		costhetag = 1. - pow(2., x);
	
		if (costhetag < -1.) 
			costhetag = -1.;
	
		if (costhetag >  1.) 
			costhetag =  1.;
    
		return costhetag;
	}  

	// the kinetic energy tg is greater than 1.3 GeV ... 

	if ( (( nucleon_id == CPT::proton_ID ) && ( p_id == CPT::neutron_ID )) || (( nucleon_id == CPT::neutron_ID ) && ( p_id == CPT::proton_ID )) ){
    
		if ( tg < .5 ) {
      
			Double_t a0 = 0.48173 - 4.4804 * tg + 16.306 * tg2 - 15.968 * tg2 * tg;
			Double_t a1 = 5.7726 - 38.582 * tg + 110.46 * tg2 - 80.140 * tg2 * tg;
			Double_t a2 = -13.745 + 111.59 * tg - 330.45 * tg2 + 246.16 * tg2 * tg;
			Double_t a3 = 27.125 - 243.05 * tg + 722.70 * tg2 - 607.53 * tg2 * tg;      
			Double_t sum1 = a0 + a1 * x + a2 * sqr(x) + a3 * sqr(x) * x;
			Double_t sum2 = 1. - a0 - a1 - a2 - a3 ;
			
			costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;
		}

		if ( tg >= .5 && tg <= 1. ) {
      
			Double_t a0 = -5.1646 + 21.871 * tg - 27.993 * tg2 + 11.587 * tg2 * tg;
			Double_t a1 = -6.0776 + 56.915*tg - 94.670*tg2 + 45.998 * tg2 * tg;
			Double_t a2 = 78.989 - 401.59*tg + 569.28*tg2 - 245.66 * tg2 * tg;
			Double_t a3 = -107.05 + 512.15*tg - 696.21*tg2 + 284.52 * tg2 * tg;
			Double_t sum1 = a0 + a1 * x + a2 * sqr(x) + a3 * sqr(x) * x ;      
			Double_t sum2 = 1.0- a0 - a1 - a2 - a3 ;
      
			costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;
		}

		if ( tg > 1. ) {
      
			Double_t a0 = -53.067 + 147.50 * tg - 134.36 * tg2 + 40.253 * tg2 * tg;
			Double_t a1 = 576.12 - 1663.8 * tg + 1578.0 * tg2 - 488.60 * tg2 * tg;
			Double_t a2 = -1543.8 + 4592.3 * tg - 4446.3 * tg2 + 1400.1 * tg2 * tg;
			Double_t a3 = 1645.5 - 4994.9 * tg + 4902.2 * tg2 - 1560.6 * tg2 * tg;
			Double_t sum1 = a0 + a1 * x + a2 * sqr(x) + a3 * sqr(x) * x;
			Double_t sum2 = 1. - a0 - a1 - a2 - a3;
      
			costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;      
		}
	}  
  
	if ( (( nucleon_id == CPT::proton_ID) && ( p_id == CPT::proton_ID )) || (( nucleon_id == CPT::neutron_ID) && ( p_id == CPT::neutron_ID )) ){

		if ( tg < .5 ) {

			Double_t a0 = 0.40693 - 3.6799 * tg + 14.556 * tg2 - 12.621 * tg2 * tg;
			Double_t a1 = -4.1404 + 59.610*tg - 175.50*tg2 + 149.64*tg2 * tg;
			Double_t a2 = 14.044 - 162.69*tg + 458.39*tg2 - 381.18*tg2 * tg;
			Double_t a3 = -17.265 + 188.73*tg - 533.90 * tg2 + 451.41 * tg2 * tg;
			Double_t sum1 = a0 + a1*x + a2 * sqr(x) + a3 * sqr(x) * x;
			Double_t sum2 = 1.0- a0 - a1 - a2 - a3;
      
			costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;
      
		} 
		else{

			Double_t a0 = -0.4755 + 5.1620 * tg - 8.1117 * tg2 + 3.5187 * tg2 * tg;
			Double_t a1 = 2.2641 - 9.9236*tg + 19.315*tg2 - 9.1783 * tg2 * tg;
			Double_t a2 = -12.528 + 55.623*tg - 84.255*tg2 + 34.950*tg2 * tg;
			Double_t a3 = 24.647 - 104.62*tg + 139.08*tg2 - 51.243*tg2 * tg;
			Double_t sum1 = a0 + a1*x + a2*sqr(x) + a3*sqr(x) * x;
			Double_t sum2 = 1.0- a0 - a1 - a2 - a3;
      
			costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;
		}    
	}
  
	if ( costhetag < -1.) 
		costhetag = -1.;
	if ( costhetag >  1.) 
		costhetag =  1.;

	return costhetag;
}

//_________________________________________________________________________________________________										
// return cos(theta) where theta is the azimutal angle

Double_t resonance_angular_dist( Double_t energy ){

	Double_t tg = energy;
	Double_t tg2 = sqr(energy);
	Double_t costhetag = 0.;

	Double_t x = gRandom->Uniform();
  
	if ( tg > 0.45 && tg < 0.985 ) {
		Double_t a0 = -1.0306 + 7.9586 * tg  - 14.797 * tg2 + 8.2309 * tg2 * tg;
		Double_t a1 = 32.849 - 125.72 * tg  + 165.90 * tg2  -  67.871 * tg2 * tg;
		Double_t a2 = -75.052 + 256.04 * tg  - 279.91 * tg2  + 85.762 * tg2 * tg;
		Double_t a3 = 60.255 - 165.47 * tg  + 113.33 * tg2  + 5.9727 * tg2 * tg;
		Double_t sum1 = a0 + a1 * x + a2 * sqr(x) + a3 * sqr(x) * x;
		Double_t sum2 = 1. - a0 - a1 - a2 - a3;
    
		costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;    
	}
	else{

		Double_t a0 = - 237.22 + 658. * tg  - 606.53 * tg2 + 186.04 * tg2 * tg;    
		Double_t a1 = 968.90 - 2694.1 * tg  + 2498.3 * tg2 - 769.33 * tg2 * tg;
		Double_t a2 = -1621.9 + 4548. * tg  - 4249.8 * tg2 + 1316.6 * tg2 * tg;
		Double_t a3 = 1363.7 - 3846.9 * tg  + 3613.6 * tg2 - 1124.3 * tg2 * tg;
		Double_t sum1 = a0 + a1 * x + a2 * sqr(x) + a3 * sqr(x) * x;
		Double_t sum2 = 1. - a0 - a1 - a2 - a3;

		costhetag = 2. * sqrt(x) * ( sum1 + sum2 * sqr(x)*sqr(x) ) - 1.;    
	}
  
	if (costhetag < -1.) 
		costhetag = -1.;
	if (costhetag >  1.) 
		costhetag =  1.;
  
	return costhetag;
}

//_________________________________________________________________________________________________										
