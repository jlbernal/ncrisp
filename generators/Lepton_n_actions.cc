/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

// needed to do 
#include "Lepton_n_actions.hh"
#include "../base/ParticleDynamics.hh"


void Create_particles(const ParticleDynamics& lep, const ParticleDynamics& nucleon, std::vector<ParticleDynamics>& out_particles, double s){
  // this function name should be changed
  
  TLorentzVector pboost = lep.Momentum() + nucleon.Momentum();
  TLorentzVector pcm = lorentzRotation(pboost, pboost);
  
  
  if ( out_particles.size() == 2 ) {   
    
    
		double m1 = out_particles[0].GetMass();
		double m2 = out_particles[1].GetMass(); 
		
		if(CPT::IsImplRessonance(out_particles[1].PdgId()) ){
		 
//		  std::cout << " resonance " << std::endl; 
		  double em_upper = TMath::Sqrt(s)-m1;
  // 		  std::cout << " energy at disposal " << em_upper << " s "  << TMath::Sqrt(s) <<  std::endl; 

		  m2 = resonance_mass(em_upper,out_particles[1].PdgId());
//  		  std::cout << " energy after set mass " <<TMath::Sqrt(s)-m1-m2 << " s "  << TMath::Sqrt(s) <<  std::endl; 
	  
		}
		
		
//		cout << "here test" << endl;
		double pm2 = ( ( s - TMath::Power( m1 + m2, 2) ) * ( s - TMath::Power( m1 - m2, 2) ) ) / ( 4. * s ); 
		if ( pm2 < 0. ) {
			std::vector<ParticleDynamics> v;
			v.push_back(out_particles[0]); v.push_back(out_particles[1]);
			throw BasicException( "Lepton_n action (...): Negative Kinetic Energy\n",v);
		}
		double pm = TMath::Sqrt(pm2);
		 
  
		double cosTheta = gRandom->Uniform(); // ojo momentaneamente distrub angular isotropica

		if(cosTheta < -1. || cosTheta > 1.) std::cout << "Error in Meson_n_action, cosTheta = " << cosTheta << std::endl;
		if(pcm.M() < 0) std::cout << "Error in Meson_n_action, pcm.M() = " << pcm.M() << std::endl;
  
		double sinTheta = sqrt( 1. - sqr(cosTheta) ) ;    
		double phi = gRandom->Uniform() * TMath::TwoPi();      

		TLorentzVector k1( pm * sinTheta * cos(phi), pm * sinTheta * sin(phi), pm * cosTheta, sqrt(sqr(pm) + sqr(m1)) );
    
		TLorentzVector k2 = pcm - k1;
		// back to lab frame ...
		out_particles[0].SetMomentum(inverseLorentzRotation(pboost, k1));
		out_particles[1].SetMomentum(inverseLorentzRotation(pboost, k2));
		
		}
}


//______________________________________________________________________________________

bool nun_mump_ccqe_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc,Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v){
  
  
	ParticleDynamics lepton = (*lpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	
	if( lepton.PdgId()!=CPT::neutrino_m_ID || nucleon.PdgId()!=CPT::neutron_ID  )		
		return false;
	
	double s =  (lepton.Momentum() + nucleon.Momentum()).M2() ;	
	v.push_back( ParticleDynamics(CPT::muon_m_ID) ); // Create the muon 
	v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon
	
	v[0].SetPosition(lepton.Position());
	v[0].SetMass(v[0].GetInvariantMass()); // no need pf effn because it is a lepton
	v[1].SetPosition(nucleon.Position());
	v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);           

	Create_particles(lepton, nucleon, v , s);
	
	if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			lpool->RemoveLepton(key);
		        v[0].BindIt(false);
			lpool->PutLepton(v[0]);  
			v.clear();  // erase nucleon particle of vector, so vector just have new mesons
			return true;
		}  
		v.clear();  
		return false;
		
}

  


bool nuN_qelastic_NCe_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v){
   
	ParticleDynamics lepton = (*lpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	
	
	if( lepton.PdgId()!=CPT::neutrino_m_ID)		
	  return false;  

// // 	cout << "nuN_qelastic_NCe_action inside here" << endl << endl;
// 	cout << " lepton id " << lepton.PdgId() << endl;
/*

    \f$ \nu_\mu + n \rightarrow	\nu_\mu + n  \f$
    \f$ \nu_\mu + n \rightarrow	\nu_\mu + n  \f$
 
 */

	v.push_back( ParticleDynamics(lepton.PdgId())); // Create the new neutrino 
	std::cout << nucleon.PdgId() << std::endl; 
	v.push_back( ParticleDynamics(nucleon.PdgId()) ); // Create the new nucleon 
	
	v[0].SetPosition(lepton.Position());
	v[0].SetMass(v[0].GetInvariantMass()); // no need pf effn because it is a lepton
	
	v[1].SetPosition(nucleon.Position());
	v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);     
	


	elasticCollision( v[0], v[1] ); // Check if is Pauli Permitible, if it is then, update the momentum for 
// 	cout << "elastic colition" << endl;
// 	
// 	cout << "v0 " << v[0].PdgId() << endl;
// 	cout << "v1 " << v[1].PdgId() << endl;
// 	
	if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			// add a lepton into the Mesons Pool.
			
	  lpool->RemoveLepton(key);
  	  v[0].BindIt(false);
	  lpool->PutLepton(v[0]);  
	
	  cout << "last v0 " <<  v[0].PdgId() << endl;
	

	  v.clear();  // erase nucleon particle of vector, so vector just have new leptons
		  
	  return true;
	}  
	else{
// 	  cout << "bloquedo por pauling " << endl;
	  return false;
	}
	
	v.clear();  
	return false;  
}


//_________________________________________________________________________________________________________________________

bool nuN_res1_CCres_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v){
  
	
	ParticleDynamics lepton = (*lpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	
	

	
	Int_t id = nucleon.PdgId();
	Int_t D_id = ( id == CPT::neutron_ID ) ? CPT::p33_p_ID: CPT::p33_pp_ID; 

	// This is a provosional method i have see how to get a proper resonance mass
	
	double s =  (lepton.Momentum() + nucleon.Momentum()).M2() ;	
	v.push_back( ParticleDynamics(CPT::muon_m_ID));
	v.push_back( ParticleDynamics(D_id)); // Create the new nucleon
	v[0].SetPosition(lepton.Position());
	v[0].SetMass(v[0].GetInvariantMass()); // no need pf effn because it is a lepton
	v[1].SetPosition(nucleon.Position());
	v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);  // must me corrected this a possible problem source
	
	
	Create_particles(lepton, nucleon, v , s);	

	
	
	if ( nuc->PutRessonance( idx,  v[1] ) ){
			// add a rho meson into the Mesons Pool.
	  lpool->RemoveLepton(key);
	  v[0].BindIt(false);
	  lpool->PutLepton(v[0]); 
	    
	  
	

	  v.clear();  // erase nucleon particle of vector, so vector just have new leptons
//			mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
	  return true;
	}  
	
  
    v.clear();
  return false;
}


bool nuN_res2_NCres_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v){
      

	ParticleDynamics lepton = (*lpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   

	
//   	cout << "nuN_res2_NCres_action inside here" << endl << endl;
// 	cout << " lepton id " << lepton.PdgId() << endl;
//   

	Int_t id = nucleon.PdgId();
	Int_t D_id = ( id == CPT::neutron_ID ) ? CPT::p33_0_ID: CPT::p33_p_ID; 

	// This is a provosional method i have see how to get a proper resonance mass
	
	double s =  (lepton.Momentum() + nucleon.Momentum()).M2() ;	
	v.push_back( ParticleDynamics(lepton.PdgId() ) );
	v.push_back( ParticleDynamics(D_id)); // Create the new nucleon
	v[0].SetPosition(lepton.Position());
	v[0].SetMass(v[0].GetInvariantMass()); // no need pf effn because it is a lepton
	v[1].SetPosition(nucleon.Position());
	v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);  // must me corrected this a possible problem source
	
	
	
	Create_particles(lepton, nucleon, v , s);	
	
		
// 	cout << "before add resonance" << endl;	
// 	cout << "v0 " << v[0].PdgId() << endl;
// 	cout << "v1 " << v[1].PdgId() << endl;
	
	if ( nuc->PutRessonance( idx,  v[1] ) ){
	  
	  lpool->RemoveLepton(key);
	  v[0].BindIt(false);
	  lpool->PutLepton(v[0]); 
	  v.clear();  // erase nucleon particle of vector, so vector just have new leptons
//			
	  return true;
	}  
	else{
//   	  cout << "bloquedo por pauling " << endl;
	  v.clear();
	  return false;
	  
	}
	
  
    
  return false;
}
    
   
  
