/* ================================================================================
 * 
 * 	Copyright 2008, 2014, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __gn_cross_sections_pHH
#define __gn_cross_sections_pHH

#include "TMath.h"
#include "TGraphErrors.h"
#include "base_defs.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "NucleusDynamics.hh"
//#include "PhotonEventGen.hh"
#include "CrispParticleTable.hh"
#include "TDecayChannel.h"
#include "TDatabasePDG.h"


// Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
// Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
// Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
// Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
// Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
// Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };

Double_t totalNonResonantCS( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t p33Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );  /// Delta(1232) P33
Double_t p11Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );  /// N(1440)     P11
Double_t d13Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );  /// N(1520)     D13
Double_t f15Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); /// N(1680)     F15
Double_t s11Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );  /// N(1535)     S11
Double_t f37Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );  /// N(1950)     F37
Double_t qdCrossSection( Dynamics* gamma, Dynamics* nucleus, Double_t* cch ); // reference at: Rossi P. Phys Rev C 40(1989)2412
///Vector Meson Production reference at PHYSICAL REVIEW D 67, 074023 ͑2003͒
Double_t rho_0_PhotonCrossSectionP( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t rho_0_PhotonCrossSectionN( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t rho_p_m_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t omega_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t phi_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ); 
Double_t J_Psi_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );
Double_t residual_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch );

///2 pions production
Double_t gp_pPipPim_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPipPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///3 pions production
Double_t gp_pPipPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_pPip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///4 pions production
Double_t gp_p2Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_pPipPim2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_n2Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPip2PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///5 pions production
Double_t gp_p2Pip2PimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p5Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_pPipPim3Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n3Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_n2Pip2PimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n5Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim3Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p2Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///6 pions production
Double_t gp_p3Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_pPipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p2Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n3Pip2PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_n3Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n2Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p2Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///7 pions production
Double_t gp_p3Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_pPipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p2Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n3Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n4Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_n3Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n2Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p2Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p3Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

///8 pions production
Double_t gp_p4Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p8Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_pPipPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p2Pip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_p3Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_nPip7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n2PipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n3Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gp_n4Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

Double_t gn_n4Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n8Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_nPipPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n2Pip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_n3Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPim7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_pPip2Pim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p2Pip3Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);
Double_t gn_p3Pip4PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch);

#endif
