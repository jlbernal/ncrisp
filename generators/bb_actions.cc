/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "bb_actions.hh"

//_________________________________________________________________________________________________										

bool bb_elastic ( Int_t &idx1, Int_t &idx2, Double_t &, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ){
	ParticleDynamics n1 = nuc[idx1], n2 = nuc[idx2];
	elasticCollision( n1, n2 );
	return nuc.NucleonPairChange( idx1, idx2, n1, n2 );
}
//_________________________________________________________________________________________________										

bool bb_elastic_np ( Int_t &idx1, Int_t &idx2, Double_t &, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ){
	ParticleDynamics 	n1 = nuc[idx1], n2 = nuc[idx2];
	
	if(n1.PdgId()==n2.PdgId())		
	elasticCollision( n1, n2 );  
	else {
		// going to CM frame		
		TLorentzVector pboost = n1.Momentum() + n2.Momentum();
		TLorentzVector pcm = lorentzRotation(pboost, pboost);
		Double_t s = pboost.M2();
		Double_t m1 = n1.GetMass(); 
		Double_t m2 = n2.GetMass();
		Double_t pm2 = ( ( s - TMath::Power( m1 + m2, 2) ) * ( s - TMath::Power( m1 - m2, 2) ) ) / ( 4. * s ); 
		if ( pm2 < 0. ) {
			std::vector<ParticleDynamics> v;
			throw BasicException( "bb_action (...): Negative Kinetic Energy bb_elastic_np scatering\n",v);
		}

		Double_t pm =  TMath::Sqrt(pm2);		
		Double_t Elab  = (s-(m1*m1)-(m2*m2))/(2*m2);
		Double_t np_costheta = 0.;
			//angular dist depends of energy in lab system 	
		if(Elab>=300 && Elab<2000){
			TF1 *f_np = new TF1("elastic_np_angle", elastic_np_angle, -1, 1, 3);
			f_np->SetParameter(0,s); 
			f_np->SetParameter(1,m1);
			f_np->SetParameter(2,m2);
 	
			np_costheta = f_np->GetRandom();
			//if(np_costheta < -1 || np_costheta > 1) cout << "np_costheta: " << np_costheta << endl;
		
			Double_t sinTheta = sqrt( 1. - sqr(np_costheta) ) ;    
			Double_t phi = gRandom->Uniform() * TMath::TwoPi();      
			TLorentzVector k1( pm * sinTheta * cos(phi), pm * sinTheta * sin(phi), pm * np_costheta, sqrt(sqr(pm) + sqr(m1)) );
			TLorentzVector k2 = pcm - k1;
		
			// back to lab frame ...
			n1.SetMomentum(inverseLorentzRotation(pboost, k1));
			n2.SetMomentum(inverseLorentzRotation(pboost, k2));
			
			double s2 = ( n1.Momentum() + n2.Momentum() ).M2();						
			if ( fabs((s - s2)/s) > 1.E-7 ) { 
				// Energy conservation.
				std::vector<ParticleDynamics> v;
				v.push_back(n1); v.push_back(n2);
				throw BasicException("Energy conservation failure on bb_elastic_np\n", v);
			}
							
			delete 	f_np;	
		}

		else{  // put here np-angular distribution for Elab < 300 MeV 
			elasticCollision( n1, n2 );  

		}		

	}

	//if( TMath::IsNaN( n1.Momentum().Vect().Mag() ) != 0 ) cout << "Error in bb_elastic_np, n1 momentum = " << n1.Momentum().Vect().Mag() << endl;
	//if( TMath::IsNaN( n2.Momentum().Vect().Mag() ) != 0 ) cout << "Error in bb_elastic_np, n2 momentum = " << n2.Momentum().Vect().Mag() << endl;
	
	return nuc.NucleonPairChange( idx1, idx2, n1, n2 );
}

bool NN_inelastic ( Int_t &idx1, Int_t &idx2, Double_t &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ){
	ParticleDynamics 	n1 = nuc[idx1], n2 = nuc[idx2];      
	Double_t y = gRandom->Uniform();
	Int_t 	id1 = 0, id2 = 0;
	// escolha das particulas emergentes ...
	if ( n1.PdgId() == CPT::neutron_ID && n2.PdgId() == CPT::neutron_ID ){
		// if (y > .25) ===>  nn -> n + p33_0
		// else         ===>  nn -> p + p33_m
		id1 = ( y > .25 )? CPT::p33_0_ID: CPT::p33_m_ID;
		id2 = ( y > .25 )? CPT::neutron_ID: CPT::proton_ID;
	}    
	if ( n1.PdgId() == CPT::proton_ID && n2.PdgId() == CPT::proton_ID ){
		// if (y > .25) ===>  pp -> n + p33_pp
		// else         ===>  pp -> p + p33_p
		id1 = ( y > .25 )? CPT::p33_pp_ID: CPT::p33_p_ID;
		id2 = ( y > .25 )? CPT::neutron_ID: CPT::proton_ID;
	}
	// Create a Delta(1232) and a nucleon 
	ParticleDynamics delta(id1);
	ParticleDynamics nucleon(id2);
	nucleon.SetMass( nucleon.GetInvariantMass() * CPT::effn );
	// setting the resonance and nucleon mass    
	Double_t sqrt_s = (n1.Momentum() + n2.Momentum()).M();
	Double_t em_upper = sqrt_s - nucleon.GetMass();
	//CPT *cpt1 = CPT::Instance();
	//if(em_upper < CPT::n_mass) {return false;}
	Double_t rss_mass = resonance_mass(em_upper, id1); //NOTE: resonance_mass routine already takes care of effective mass
	if(rss_mass<0) return false;
	delta.SetMass( rss_mass );
	// setting the new ressonance decay time.    
	delta.SetLifeTime( delta.HalfLife() + t );        
	inelasticCollision( n1, n2, nucleon, delta);
	
	if ( nuc.NucleonPairChange( idx1, idx2, nucleon, delta ) ) { 
		return true;
	}
	return false;
}
//_________________________________________________________________________________________________										
bool np_inelastic ( Int_t &idx1, Int_t &idx2, Double_t &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ) {
	// neutron-proton Inelastic Process ...
	ParticleDynamics n1 = nuc[idx1], n2 = nuc[idx2];      
	Int_t id1, id2;  
	// if (y > .5) ===>   pn -> n + p33_p
	// else        ===>   pn -> p + p33_0
	Double_t y = gRandom->Uniform();
	id1 = ( y < .5 )? CPT::p33_p_ID: CPT::p33_0_ID;
	id2 = ( y < .5 )? CPT::neutron_ID: CPT::proton_ID;
	// Create a Delta(1232) 
	ParticleDynamics delta(id1); 
	ParticleDynamics nucleon(id2);
	nucleon.SetMass( nucleon.GetInvariantMass() * CPT::effn );
	Double_t sqrt_s = (n1.Momentum() + n2.Momentum()).M();
	Double_t em_upper = sqrt_s  - nucleon.GetMass(); 
	//if(em_upper < CPT::n_mass) {return false;}
	Double_t rss_mass = resonance_mass(em_upper, id1); //NOTE: resonance_mass routine already takes care of effective mass
	if(rss_mass<0) return false;
	delta.SetMass( rss_mass );    
	// setting the new ressonance decay time.
	delta.SetLifeTime( delta.HalfLife() + t );
	inelasticCollision( n1 ,n2, nucleon, delta );
	
	return  nuc.NucleonPairChange(idx1, idx2, nucleon, delta);
}
//_________________________________________________________________________________________________										
bool NDelta_inelastic ( Int_t &idx1, Int_t &idx2, Double_t &, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ){
	// The inelastic processes for delta(1232) + nucleon ...
	ParticleDynamics n1 = nuc[idx1], n2 = nuc[idx2];  
	Int_t 	delta_id = 0, n_id = 0;
	if ( CPT::IsNucleon(n1.PdgId()) ) {
		delta_id = n2.PdgId(); n_id = n1.PdgId();
	}else {
		delta_id = n1.PdgId(); n_id = n2.PdgId();
	}
	if ( ( (delta_id == CPT::p33_0_ID) && (n_id == CPT::neutron_ID) ) || ( (delta_id == CPT::p33_m_ID) && (n_id == CPT::proton_ID)  )   ){	
		// Delta0 n -> n n
		// Delta- p -> n n
		ParticleDynamics neutron1(CPT::neutron_ID), neutron2(CPT::neutron_ID);
		Double_t neutron_mass = neutron1.GetInvariantMass() * CPT::effn;
		neutron1.SetMass ( neutron_mass ); neutron1.SetPosition(n1.Position());
		neutron2.SetMass ( neutron_mass ); neutron2.SetPosition(n2.Position());
		inelasticCollision (n1, n2, neutron1, neutron2);	
		// do the pauli blocking. Return true if succeed.
		
		return nuc.NucleonPairChange(idx1, idx2, neutron1, neutron2);  
	}	
	if ( ( (delta_id == CPT::p33_p_ID) && (n_id == CPT::neutron_ID) ) || ( (delta_id == CPT::p33_0_ID) && (n_id == CPT::proton_ID)  )	){	
		// Delta+ n -> n p
		// Delta0 p -> n p
		ParticleDynamics n(CPT::neutron_ID), p(CPT::proton_ID); 
		n.SetMass( n.GetInvariantMass() * CPT::effn );
		p.SetMass( p.GetInvariantMass() * CPT::effn );
		n.SetPosition(n1.Position()); p.SetPosition(n2.Position());
		inelasticCollision (n1, n2, n, p);
		
		return nuc.NucleonPairChange( idx1, idx2, n, p);    
	}
	if ( ( (delta_id == CPT::p33_pp_ID) && (n_id == CPT::neutron_ID) ) || ( (delta_id == CPT::p33_p_ID)  && (n_id == CPT::proton_ID)  )  ) {
		// Delta++ n -> pp
		// Delta+  p -> pp	 
		ParticleDynamics p1(CPT::proton_ID),  p2(CPT::proton_ID); 
		p1.SetMass( CPT::p_mass * CPT::effn );
		p2.SetMass( CPT::p_mass * CPT::effn );
		p1.SetPosition(n1.Position()); p2.SetPosition(n2.Position());
		inelasticCollision( n1, n2, p1, p2);
		
		return nuc.NucleonPairChange(idx1, idx2, p1, p2);
	}
	return false;
}
//_________________________________________________________________________________________________										
// The inelastic channel facilitator.
bool bb_inelastic ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v){
	int pdgId_1 = nuc[idx1].PdgId();
	int pdgId_2 = nuc[idx2].PdgId();  
	if ( CPT::IsNucleon(pdgId_1) && CPT::IsNucleon(pdgId_2) ) {
		if ( pdgId_1 == pdgId_2 ) {
			// reaction is inelastic p + p or n + n
			return NN_inelastic ( idx1, idx2, t, nuc, v);
		}
		else { 
			// reaction is inelastic n + p 
			return np_inelastic ( idx1, idx2, t, nuc, v);
		}
	}      
	else if ( CPT::IsP33(pdgId_1) || CPT::IsP33(pdgId_2) ) {
		return NDelta_inelastic( idx1, idx2, t, nuc, v);
	}
	return false;
}
//_________________________________________________________________________________________________										
bool NLambda_inelastic ( Int_t &idx1, Int_t &idx2, Double_t &, NucleusDynamics& nuc, std::vector<ParticleDynamics>& ) {
	if ( idx1 == idx2 ) {
		return false;
	}
	ParticleDynamics n1 = nuc[idx1], n2 = nuc[idx2];  
	Int_t lambda_id = 0, n_id = 0;
	if ( CPT::IsNucleon(n1.PdgId()) ) {
		lambda_id = n2.PdgId(); n_id = n1.PdgId();
	}else {
		lambda_id = n1.PdgId(); n_id = n2.PdgId();
	} 
	if ( lambda_id == CPT::lambda_ID && CPT::IsNucleon(n_id) ) {
		ParticleDynamics q1(CPT::neutron_ID);
		ParticleDynamics q2(n_id);    
		q1.SetMass(CPT::n_mass * CPT::effn);
		q2.SetMass(q2.GetInvariantMass() * CPT::effn );
		q1.SetPosition(n1.Position());
		q2.SetPosition(n2.Position());
		inelasticCollision (n1, n2, q1, q2);	
		return nuc.NucleonPairChange(idx1, idx2, q1, q2);  
	}
	return false;
}


// angular dists
//___________________________________________________

Double_t elastic_np_angle(Double_t *x, Double_t *par  ) {
	
	Double_t s = 0.000001*par[0];
		     
	Double_t m1 = 0.001*par[1];
	Double_t m2 = 0.001*par[2];
//		
//	
	Double_t Pc= 0.25*(s-4.*m1*m1);
	

	Double_t Pl2 = (0.25*lamda_malds(s,m1*m1,m2*m2))/(m2*m2);
	Double_t Pl3= TMath::Sqrt(Pl2);	

	//cout << Pl2 << endl;

	Double_t Bnp,aa,cc,ac;
	if (Pl3 < 1.1)
		Bnp = 9.87 - 4.88*Pl3; // article  
	else
		Bnp = 3.68+0.76*Pl3;	
	
	Double_t aa1 = 0.8/Pl3;
	aa = aa1*aa1;
	if (Pl3 < 1.7)
		cc = 6.23*TMath::Exp(-1.78*Pl3);	
	else
		cc = 0.3;	// ok
	
	ac = 100.; // ok
	
//	cout << Bnp << " Bnp  " <<  aa << " aa  " << cc << " cc  " << endl;	
//	cout << cc <<endl; 	
	Double_t cos = x[0];
	Double_t t = -2.*Pc*(1.-cos);
	Double_t u = -2.*Pc*(1.+cos);
	Double_t f = 0.;
	f = 9.5*(TMath::Exp(Bnp*t)+aa*TMath::Exp(Bnp*u)+1.*cc*TMath::Exp(ac*u));
//	cout << s << " Pc " << 2.*Pc <<  " pl3 " << Pl3	<< " " << Bnp << " Bnp  " <<  aa << " aa  " << cc << " cc  " << endl;	

	
	
return f;
} 
