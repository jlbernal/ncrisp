/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __CrossSectionData_pHH
#define __CrossSectionData_pHH

#include "TNamed.h"
#include "TString.h"

///
//_________________________________________________________________________________________________										

class CrossSectionData: public TNamed{

private:
  
	Double_t tcs;
	Double_t b2;
	Int_t idx;

public:
  
	///
	//_________________________________________________________________________________________________										

	CrossSectionData();
  
	///
	//_________________________________________________________________________________________________										

	CrossSectionData( Int_t idx, Double_t b2, Double_t tcs );

	///
	//_________________________________________________________________________________________________										

	virtual ~CrossSectionData();

	///
	//_________________________________________________________________________________________________										

	inline Bool_t operator <(const CrossSectionData& u) const{ return (this->tcs < u.tcs); }
  
	///
	//_________________________________________________________________________________________________										
	// valor da secao de choque total.

	inline Double_t GetTcsValue() const { return tcs; }
  
	///
	//_________________________________________________________________________________________________										
	// retorna o parametro de impacto ao quadrado.

	inline Double_t GetB2() const { return b2; }
  
	///
	//_________________________________________________________________________________________________										

	inline Int_t GetIdx() const { return idx; }
  
	///
	//_________________________________________________________________________________________________										

	TString ToString() const;

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(CrossSectionData,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif


