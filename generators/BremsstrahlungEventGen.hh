/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __BremsstrahlungEventGen_pHH
#define __BremsstrahlungEventGen_pHH

#include "TMath.h"
#include "TObjArray.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "EventGen.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "PhotonChannel.hh"
#include "gn_cross_sections.hh"
#include "Measurement.hh"
#include "CrossSectionData.hh"
#include "TF1.h"
#include "Math/SpecFunc.h"

class BremsstrahlungEventGen: public EventGen{

private:
	TObjArray channels;     // The photon channels.
	Double_t tcs;           // the total photon absorption.
	Int_t num_photons;      // number of incoming photons.
	Int_t selected_ch;      // the index of the selected channel.
	Int_t selected_nucleon; // the index of the selected nucleon.

protected:
	void SelectNucleons( ParticleDynamics& gamma, NucleusDynamics& nuc, TObjArray& csData );
	
public:
	BremsstrahlungEventGen();
	~BremsstrahlungEventGen();
	TObjArray& GetChannels() { return channels; }
	Measurement Generate( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, Int_t nEvents, char *opt = 0 , std::ostream *os = 0 );
	Int_t Generate( Double_t MaxEnergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par,  char *opt = 0 , std::ostream *os =  0  );
	Int_t Generate(	Double_t MaxEnergy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os=0);

	static TVector3 StartPosition(NucleusDynamics& nuc);
	inline Double_t TotalCrossSection( ParticleDynamics* gamma, Int_t idx, NucleusDynamics* nuc );
	inline Int_t NumPhotons() { return num_photons; }
	inline void ResetNumPhotons() { num_photons = 0; }
	//inline void SetVirtuality(Double_t Q2) { Q_2 = Q2; }
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BremsstrahlungEventGen,0);
#endif // CRISP_SKIP_ROOTDICT
};

Double_t BremsstrahlungEventGen::TotalCrossSection( ParticleDynamics* gamma, Int_t idx, NucleusDynamics* nuc ){
	Double_t gn = 0.;
	for (Int_t j = 0; j < channels.GetEntriesFast(); j++ ) {
		Double_t gn_temp;
		const TString qdStr = "qd";
		TString phChStr = TString(  ((PhotonChannel*)channels[j])->CrossSection().GetName() );
		if ( phChStr.CompareTo(qdStr) == 0 )
			gn_temp = ((PhotonChannel*)channels[j])->CrossSection().Calculate(gamma, nuc); //quasi-deuteron cross section model depends on nucleus
		else
			gn_temp = ((PhotonChannel*)channels[j])->CrossSection().Calculate( gamma, &(*nuc)[idx] );
		if (gn_temp >= 0) gn = gn + gn_temp;
//		std::cout << "Calculating TotalCrossSection... channel[" << j << "] = " << ((PhotonChannel*)channels[j])->CrossSection().GetName() << ", cross section value = " << gn_temp << std::endl; 
	}
//	std::cout << " total cross section = " << gn << std::endl << std::endl;
	return gn;
}

#endif
