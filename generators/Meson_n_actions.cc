/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Meson_n_actions.hh"

/*
 * NOTE: all over this file, resonance_mass routine already takes care of effective mass
 */

Double_t RhoN_elas_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const double NC = 3; 
	const double NV = 1./sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

 	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
 	double z = VM_Mand_z(s,0,t,MN,MVO);
 	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
 	return f* 1000/C;
}
Double_t OmegaN_elas_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const double NC = 3; 
	const double NV = 1./3/sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000/C;
}
Double_t PhiN_elas_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const double NC = 3; 
	const double NV = 1./3; 
	double C = NC*NV*NC*NV;
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::phi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtPhi(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000/C;
}
Double_t JPsiN_elas_AngDistVDM(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const double NC = 3; 
	const double NV = 2./3; 
	double C = NC*NV*NC*NV;
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::J_Psi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtJ_Psi(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000/C;
}
Double_t JPsiN_elas_AngDist(Double_t *x, Double_t *par){
	const double NC = 3; 
	const double NV = 2./3; 
	double C = NC*NV*NC*NV;
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVO = CPT::J_Psi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	Double_t diffCS =  (23.15*TMath::Power(s, 0.16) + 0.034*TMath::Power(s, 0.88) + 1.49*TMath::Power(s, 0.52) )*TMath::Exp( (-1.64 + 0.83*TMath::Log(s) ) * t);
	return diffCS/C/10.;
}
Double_t PionN_OmegaN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVP = CPT::pion_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MN,MVO) - s ); //Eq (13)
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PionNeut__ProtOmega(s,t,cos)*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}
Double_t OmegaN_PionN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVP = CPT::pion_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MN_2 = TMath::Power(MN,2);
	
	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MN,MVO) - s ); //Eq (13)
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_ProtOmega__PionNeut(s,t,cos)*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}
Double_t OmegaN_RhoN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVP = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MN,MVO) - s ); //Eq (13)
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_NOmega__NRho(s,t)*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}
Double_t RhoN_OmegaN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVP = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MN,MVO) - s ); //Eq (13)
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_NRho__NOmega(s,t)*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}

Double_t NJPsi_ND_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	Double_t f =  _GeV2_mbar*VM_Diff_CrossSection_NJPsi__HypD(s,cos);
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}

Double_t NJPsi_NDast_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	Double_t f =  _GeV2_mbar*VM_Diff_CrossSection_NJPsi__HypDast(s,cos);
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}

void VMcreateMeson( const ParticleDynamics& mes, const ParticleDynamics& nucleon, std::vector<ParticleDynamics>& out_particles, double s ){
	if ( out_particles.size() == 2 ) {    
    
		TLorentzVector pboost = mes.Momentum() + nucleon.Momentum();
		TLorentzVector pcm = lorentzRotation(pboost, pboost);

		double m1 = out_particles[0].GetMass(), m2 = out_particles[1].GetMass();    
		double pm2 = ( ( s - TMath::Power( m1 + m2, 2) ) * ( s - TMath::Power( m1 - m2, 2) ) ) / ( 4. * s ); 
		if ( pm2 < 0. ) {
			std::vector<ParticleDynamics> v;
			v.push_back(out_particles[0]); v.push_back(out_particles[1]);
			throw BasicException( "Meson_n action (...): Negative Kinetic Energy\n",v);
		}
		double pm = TMath::Sqrt(pm2);
    
		double cosTheta = 0;  
		if (  ( out_particles[0].PdgId() == mes.PdgId() )&& ( out_particles[1].PdgId() == nucleon.PdgId() )  ) { //elastic
			if (CPT::IsRho( mes.PdgId() ) ){
				//std::cout <<  "RhoN_elas_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//				RhoN_elas_AngDist;

				/*
				* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
				* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
				* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
				* compromising the calculation
				*/
				double MVO = CPT::rho_0_mass/1000.; // mass in GeV (VERY IMPORTANT)
				double MN = CPT::n_mass/1000.;
				double MVO_2 = TMath::Power(MVO,2);
				double MN_2 = TMath::Power(MN,2);
				double t_min = -2.5;
				s = s/1000/1000; //MeV^2 -> GeV^2
				double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)
				
				if(cosMin < -1) cosMin = -1.;
				
				TF1 *f1 = new TF1("RhoN_elas_AngDist",RhoN_elas_AngDist,cosMin,1,1);
				f1->SetParameter(0,s); //GeV^2
				f1->SetNpx(1000);
				cosTheta = f1->GetRandom();
				//std::cout <<  "RhoN_elas_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
				delete f1;
			} 
			if (CPT::IsOmega( mes.PdgId() ) ){
				//std::cout <<  "OmgN_elas_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//				OmegaN_elas_AngDist;

				/*
				* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
				* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
				* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
				* compromising the calculation
				*/
				double MVO = CPT::omega_mass/1000.; // mass in GeV (VERY IMPORTANT)
				double MN = CPT::n_mass/1000.;
				double MVO_2 = TMath::Power(MVO,2);
				double MN_2 = TMath::Power(MN,2);
				double t_min = -2.5;
				s = s/1000/1000; //MeV^2 -> GeV^2
				double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)

				if(cosMin < -1) cosMin = -1.;
				
				TF1 *f2 = new TF1("OmegaN_elas_AngDist",OmegaN_elas_AngDist,cosMin,1,1);
				f2->SetParameter(0,s); //GeV^2
				f2->SetNpx(1000);
				cosTheta = f2->GetRandom();
				//std::cout <<  "OmgN_elas_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
				delete f2;
			} 
			if (CPT::IsPhi( mes.PdgId() ) ){
				//std::cout <<  "PhiN_elas_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//				PhiN_elas_AngDist;

				/*
				* NOTE: Numerical issues prevent the angular distribution to be calculated in full domain (-1 < cosTheta < 1).
				* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
				* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
				* compromising the calculation
				*/
				double MVO = CPT::phi_mass/1000.; // mass in GeV (VERY IMPORTANT)
				double MN = CPT::n_mass/1000.;
				double MVO_2 = TMath::Power(MVO,2);
				double MN_2 = TMath::Power(MN,2);
				double t_min = -2.5;
				s = s/1000/1000; //MeV^2 -> GeV^2
				double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)

				if(cosMin < -1) cosMin = -1.;
				
				TF1 *f3 = new TF1("PhiN_elas_AngDist",PhiN_elas_AngDist,cosMin,1,1);
				f3->SetParameter(0,s); //GeV^2
				f3->SetNpx(1000);
				cosTheta = f3->GetRandom();
				//std::cout <<  "PhiN_elas_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
				delete f3;
			} 
			if (CPT::IsJ_Psi( mes.PdgId() ) ){
				//std::cout <<  "JPsiN_elas_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//				JPsiN_elas_AngDist;

				/*
				* NOTE: Numerical issues prevent the VDM angular distribution to be calculated in full domain (-1 < cosTheta < 1)
				* This model of vector meson production (soft dipole Pomeron) is valid for |t| (momentum transfered) not 
				* higher than 2.5 GeV^2, so this condition can be used to determine the lower limit in the cos(theta) without
				* compromising the calculation
				*/
				double MVO = CPT::J_Psi_mass/1000.; // mass in GeV (VERY IMPORTANT)
				double MN = CPT::n_mass/1000.;
				double MVO_2 = TMath::Power(MVO,2);
				double MN_2 = TMath::Power(MN,2);
				double t_min = -2.5;
				s = s/1000/1000; //MeV^2 -> GeV^2
				double cosMin = ( 2.*t_min + (MN_2 - MVO_2)*(MN_2 - MVO_2)/s - Mand_Sigma(MN,MVO,MN,MVO) + s ) / (sqrt(Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2))/s); //Eq (13)
                                
				if(cosMin < -1) cosMin = -1.;
				
				TF1 *f4 = new TF1("JPsiN_elas_AngDist",JPsiN_elas_AngDistVDM,cosMin,1,1);
				f4->SetParameter(0,s); //GeV^2
				f4->SetNpx(1000);
				cosTheta = f4->GetRandom();
				//std::cout <<  "JPsiN_elas_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
				delete f4;
			} 
		} else  if ( ( ( out_particles[0].PdgId() == CPT::pion_p_ID )|| ( out_particles[0].PdgId() == CPT::pion_m_ID ) )&& (mes.PdgId() == CPT::omega_ID)){
			//std::cout <<  "OmegaN_PionN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			OmegaN_PionN_AngDist;
			TF1 *f4 = new TF1("OmegaN_PionN_AngDist",OmegaN_PionN_AngDist,-1,1,1);
			f4->SetParameter(0,s/1000/1000); //MeV^2 -> GeV^2
			f4->SetNpx(1000);
			cosTheta = f4->GetRandom();
			//std::cout <<  "OmegaN_PionN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f4;
		} else	if ( (  out_particles[0].PdgId() == CPT::omega_ID ) && ((mes.PdgId() == CPT::pion_m_ID)|| ( mes.PdgId() == CPT::pion_p_ID )) ){
			//std::cout <<  "PionN_OmegaN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			PionN_OmegaN_AngDist;
			TF1 *f5 = new TF1("OmegaN_PionN_AngDist",PionN_OmegaN_AngDist,-1,1,1);
			f5->SetParameter(0,s/1000/1000); //MeV^2 -> GeV^2
			f5->SetNpx(1000);
			cosTheta = f5->GetRandom();
			//std::cout <<  "PionN_OmegaN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f5;
		} else	if ( ( ( out_particles[0].PdgId() == CPT::rho_p_ID )|| ( out_particles[0].PdgId() == CPT::rho_m_ID ) )&& (mes.PdgId() == CPT::omega_ID)){
			//std::cout <<  "OmegaN_RhoN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			OmegaN_RhoN_AngDist;
			TF1 *f6 = new TF1("OmegaN_RhoN_AngDist",OmegaN_RhoN_AngDist,-1,1,1);
			f6->SetParameter(0,s/1000/1000); //MeV^2 -> GeV^2
			f6->SetNpx(1000);
			cosTheta = f6->GetRandom();
			//std::cout <<  "OmegaN_RhoN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f6;
		} else if (  ( out_particles[0].PdgId() == CPT::omega_ID ) && ((mes.PdgId() == CPT::rho_m_ID)|| ( mes.PdgId() == CPT::rho_p_ID ) ) ){
			//std::cout <<  "RhoN_OmegaN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			RhoN_OmegaN_AngDist;
			TF1 *f7 = new TF1("RhoN_OmegaN_AngDist",RhoN_OmegaN_AngDist,-1,1,1);
			f7->SetParameter(0,s/1000/1000); //MeV^2 -> GeV^2
			f7->SetNpx(1000);
			cosTheta = f7->GetRandom();
			//std::cout <<  "RhoN_OmegaN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f7;
		} else	if ( ( CPT::IsRho(out_particles[0].PdgId() )   )&& (  CPT::IsPion(mes.PdgId() )  )  ){
			//std::cout <<  "PionN_RhoN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			PionN_OmegaN_AngDist;
			TF1 *f8 = new TF1("PionN_RhoN_AngDist",PionN_OmegaN_AngDist,-1,1,1); // due to the simmilar mass of rho and omega meson we use the same angular distribution
			Double_t s1;
			s1 = (s < 2974000)? 2974000:s;
			//std::cout << "s1 = " << s1 << std::endl;
			f8->SetParameter(0,s1/1000/1000); //MeV^2 -> GeV^2
			f8->SetNpx(1000);
			cosTheta = f8->GetRandom();
			//std::cout <<  "PionN_RhoN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f8;
		} else	if ( (  CPT::IsPion(out_particles[0].PdgId() )  )&&(   CPT::IsRho(mes.PdgId() )  )  ){
			//std::cout <<  "RhoN_PionN_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			OmegaN_PionN_AngDist;
			TF1 *f9 = new TF1("RhoN_PionN_AngDist", OmegaN_PionN_AngDist, -1, 1, 1); // due to the simmilar mass of rho and omega meson we use the same angular distribution
			Double_t s1;
			s1 = (s < 2967000)? 2967000:s;
			//std::cout << "s1 = " << s1 << std::endl;
			f9->SetParameter(0,s1/1000/1000); //MeV^2 -> GeV^2
			f9->SetNpx(1000);
			cosTheta = f9->GetRandom();
//			std::cout <<  "RhoN_PionN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f9;
		} else	if ( (  CPT::IsD(out_particles[0].PdgId() )  )&&(   CPT::IsJ_Psi(mes.PdgId() )  )  ){
//			std::cout <<  "NJPsi_ND_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			NJPsi_ND_AngDist;
			TF1 *f10 = new TF1("NJPsi_ND_AngDist", NJPsi_ND_AngDist, -1, 1, 1);
			Double_t s1;
			s1 = (s < 17.3*1e6)? 17.3*1e6:s;
//			std::cout << "s1 = " << s1 << ", s = " << s << std::endl;
			f10->SetParameter(0,s1/1000/1000); //MeV^2 -> GeV^2
			f10->SetNpx(1000);
			cosTheta = f10->GetRandom();
//			std::cout <<  "NJPsi_ND_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f10;
		} else	if ( (  CPT::IsDast(out_particles[0].PdgId() )  )&&(   CPT::IsJ_Psi(mes.PdgId() )  )  ){
//			std::cout <<  "NJPsi_NDast_AngDist 1 , s = " << s/1000./1000. << ", mes.PdgId() = " << mes.PdgId() << ", out_particles[0].PdgId() = " << out_particles[0].PdgId() << ", out_particles[0].GetMass() = " << out_particles[0].GetMass()  << ", out_particles[0].Momentum().M() = " << out_particles[0].Momentum().M() << std::endl;
//			NJPsi_NDast_AngDist;
			TF1 *f11 = new TF1("NJPsi_NDast_AngDist", NJPsi_NDast_AngDist, -1, 1, 1);
			Double_t s1;
			s1 = (s < 17.3*1e6)? 17.3*1e6:s;
//			std::cout << "s1 = " << s1 << ", s = " << s  << std::endl;
			f11->SetParameter(0,s1/1000/1000); //MeV^2 -> GeV^2
			f11->SetNpx(1000);
			cosTheta = f11->GetRandom();
//			std::cout <<  "NJPsi_NDast_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
			delete f11;
		} else{  
			cosTheta = 1.; // phi case
//			std::cout <<  "else , cosTheta = " <<  cosTheta  << std::endl;
		}
//		else if  ( CPT::IsOmega( meson_id ) ) cosTheta = 0;
//		else if  ( CPT::IsPhi( meson_id ) ) cosTheta = 0;
//		else cosTheta = 1. - 2. * gRandom->Uniform();

		if(cosTheta < -1. || cosTheta > 1.) std::cout << "Error in Meson_n_action, cosTheta = " << cosTheta << std::endl;
		if(pcm.M() < 0) std::cout << "Error in Meson_n_action, pcm.M() = " << pcm.M() << std::endl;

		double sinTheta = sqrt( 1. - sqr(cosTheta) ) ;    
		double phi = gRandom->Uniform() * TMath::TwoPi();      

		TLorentzVector k1( pm * sinTheta * cos(phi), pm * sinTheta * sin(phi), pm * cosTheta, sqrt(sqr(pm) + sqr(m1)) );
    
		TLorentzVector k2 = pcm - k1;
		// back to lab frame ...
		out_particles[0].SetMomentum(inverseLorentzRotation(pboost, k1));
		out_particles[1].SetMomentum(inverseLorentzRotation(pboost, k2));


 		double s2 = (out_particles[0].Momentum() + out_particles[1].Momentum()).M2();

                if( fabs((pboost.M2() - s2)/pboost.M2()) > 1e-7 ){
                        std::cout << "Energy conservation failure at meson_n_actions when creating a meson" << std::endl;
                        std::cout << "out_particles[0].Momentum().Print(): " << std::endl;
                        out_particles[0].Momentum().Print();
                        std::cout << "out_particles[1].Momentum().Print(): " << std::endl;
                        out_particles[1].Momentum().Print();
                }

	}  
}

inline Int_t pion_n_P33_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::p33_m_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::p33_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::p33_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::p33_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::p33_p_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::p33_pp_ID;
	}  
	return 0;
}

inline Int_t pion_n_Delta1700_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::delta1700_m_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::delta1700_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::delta1700_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::delta1700_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::delta1700_p_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::delta1700_pp_ID;
	}  
	return 0;
}

inline Int_t pion_n_Delta1950_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::f37_m_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::f37_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::f37_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::f37_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::f37_p_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::f37_pp_ID;
	}  
	return 0;
}

inline Int_t pion_n_N1440_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_0_ID )
			return CPT::p11_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::p11_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::p11_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::p11_p_ID;
	}  
	return 0;
}

inline Int_t pion_n_N1520_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_0_ID )
			return CPT::d13_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::d13_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::d13_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::d13_p_ID;
	}  
	return 0;
}

inline Int_t pion_n_N1535_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_0_ID )
			return CPT::s11_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::s11_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::s11_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::s11_p_ID;
	}  
	return 0;
}

inline Int_t pion_n_N1680_id( ParticleDynamics& nucleon, ParticleDynamics& pion ){
	Int_t n_id = nucleon.PdgId();
	Int_t m_id = pion.PdgId();
	if ( n_id == CPT::neutron_ID ){
		if ( m_id == CPT::pion_0_ID )
			return CPT::f15_0_ID;
		if ( m_id == CPT::pion_p_ID )
			return CPT::f15_p_ID;    
	}
	if ( n_id == CPT::proton_ID ) {
		if ( m_id == CPT::pion_m_ID ) 
			return CPT::f15_0_ID;
		if ( m_id == CPT::pion_0_ID )
			return CPT::f15_p_ID;
	}  
	return 0;
}

bool pion_absorption_Delta1232( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();
	//Double_t pp = w.Vect().Mag2();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_P33_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {    
		//std::cout << "Putting a delta1232..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_Delta1700( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_Delta1700_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {         
		//std::cout << "Putting a delta1700..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_Delta1950( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_Delta1950_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) { 
		//std::cout << "Putting a delta1950..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_N1440( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_N1440_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {     
		//std::cout << "Putting a N1440..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_N1520( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_N1520_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {       
		//std::cout << "Putting a N1520..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_N1535( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_N1535_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {   
		//std::cout << "Putting a N1535..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

bool pion_absorption_N1680( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>&){

	ParticleDynamics & meson = (*mpool)[key];

	TLorentzVector w1 = (*nuc)[idx].Momentum();
	TLorentzVector w2 = meson.Momentum(); 
	TLorentzVector w = w1 + w2;
	double sqrt_s = w.M();

	// create a ressonance, set the momentum and the running mass.
	Int_t rss_id = pion_n_N1680_id( (*nuc)[idx], meson );    
	ParticleDynamics rss(rss_id);   
	double em_upper = sqrt_s;
	Double_t rss_mass = resonance_mass(em_upper, rss_id); //NOTE: resonance_mass routine already takes care of effective mass
	rss.SetMass (rss_mass);
	
	double pm = sqrt(sqr(w.E()) - sqr(rss.GetMass()));
	
	rss.SetMomentum(w.Vect().Unit()*pm); //same direction as center of mass momentum
	rss.SetLifeTime( rss.HalfLife() + t );
	// put it in the nucleus.  
	if ( nuc->PutRessonance( idx, rss) ) {    
		//std::cout << "Putting a N1680..." << std::endl;
		mpool->RemoveMeson(key); // remove the absorbed meson from meson pool.      
		return true;
	}
	return false;
}

//_________________________________________________________________________________________________										

bool meson_elastic_scattering_action ( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &, std::vector<ParticleDynamics>&) {
	ParticleDynamics n1 = (*nuc)[idx], aMeson = (*mpool)[key];
	elasticCollision( n1, aMeson );
	if (nuc->ChangeNucleon(idx, n1)){
		(*mpool)[key].SetMomentum( aMeson.Momentum() );    
		return true;    
	}
	return false;
}

// Deg1:Np_No:No_Np:No_Nr:Nr_No:No_els:No_N2p:Np_No1:Np_Nh:Np_Nr:No_Np1:Nh_Np:Nr_Np
bool mesonN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "mesonN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2){
		v.push_back( meson.PdgId()   ) ; // Create the meson 
		v.push_back( nucleon.PdgId() ) ; // Create the new nucleon 
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		//std::cout <<  "mesonN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool rhoN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
//  built with the elastic amplitude form photo absortion process relation.
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "rhoN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > sqr(1.745)){
		v.push_back( meson.PdgId()   ) ; // Create the meson 
		v.push_back( nucleon.PdgId() ) ; // Create the new nucleon 
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool omegaN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
//  built with the elastic amplitude form photo absortion process relation.
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "omegaN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > sqr(1.745)){
		v.push_back( meson.PdgId()   ) ; // Create the meson 
		v.push_back( nucleon.PdgId() ) ; // Create the new nucleon 
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		//std::cout <<  "mesonN_elastic_action, omega key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		// nucleon and for meson rho, else return false.
		//
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool phiN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
//  built with the elastic amplitude form photo absortion process relation.
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "phiN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > sqr(2.0132)){
		v.push_back( meson.PdgId()   ) ; // Create the meson 
		v.push_back( nucleon.PdgId() ) ; // Create the new nucleon 
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		//std::cout <<  "mesonN_elastic_action, phi key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			// add a rho meson into the Mesons Pool.
			mpool->RemoveMeson(key);
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just have new mesons
//			mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}

bool JPsiN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
//  built with the elastic amplitude form photo absortion process relation.
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "JPsiN_elastic_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > sqr(4.0396)){
		v.push_back( meson.PdgId()   ) ; // Create the meson 
		v.push_back( nucleon.PdgId() ) ; // Create the new nucleon 
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		//std::cout <<  "mesonN_elastic_action, phi key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			// add a rho meson into the Mesons Pool.
			mpool->RemoveMeson(key);
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just have new mesons
//			mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}

bool PionN_OmegaN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "PionN_OmegaN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.97345){
		if ((meson.PdgId()  == CPT::pion_p_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::omega_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::pion_m_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::omega_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);           
		//std::cout <<  "PionN_OmegaN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		// the upper limit: rho_invariant_mass + omega_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double omega_mass = resonance_mass( em_upper, CPT::omega_ID ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(omega_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
		//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
		//std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", phi_mass = " << omega_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool OmegaN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "OmegaN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.96603){
		if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::pion_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else	if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::pion_p_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);           
		//std::cout <<  "OmegaN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool OmegaN_RhoN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "OmegaN_RhoN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.96902){
		if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::rho_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::rho_p_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);          
		//std::cout <<  "OmegaN_RhoN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		// the upper limit: Vector_invariant_mass + omega_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
		//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
		//std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
			//v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl; 
			//v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool RhoN_OmegaN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "RhoN_OmegaN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.96843){
		if ((meson.PdgId()  == CPT::rho_p_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::omega_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::rho_m_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::omega_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);           
	//	std::cout <<  "RhoN_OmegaN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		// the upper limit: Vector_invariant_mass + omega_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
	//	std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	//	std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//	v[0].Momentum().Print(); 
		//	std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
		//	v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool OmegaN_2PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
//	std::cout <<  "OmegaN_2PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.96615){
		if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::pion_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::pion_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::omega_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::pion_p_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::pion_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::neutron_ID) ); // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
	  
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(meson.Position());
		v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);
		v[2].SetPosition(nucleon.Position());
		v[2].SetMass(v[2].GetInvariantMass() * CPT::effn);           

	//***********************************************************************/	
	   //(Momentum, Energy units are Gev/C, GeV)
		TLorentzVector target = nucleon.Momentum()*(1./1000.);
		TLorentzVector beam = meson.Momentum()*(1./1000.);
		TLorentzVector W = beam + target;
	   	Double_t masses[3] = { v[0].GetMass()/1000.,  v[1].GetMass()/1000.,  v[2].GetMass()/1000.} ;
		TLorentzVector P = lorentzRotation(W, W);
		TGenPhaseSpace event;
		TLorentzVector Wfinal;
		if ( event.SetDecay(P, 3, masses) ) {
			event.Generate();
			for (unsigned int i = 0; i < v.size(); i++) {
				v[i].SetMomentum( inverseLorentzRotation (W, *(event.GetDecay(i)))*1000. ); // set meson and nucleon Momentum back to MeV	
				Wfinal += v[i].Momentum();
			}
			
			double s2 = Wfinal.M2();
			if ( fabs((s - s2)/s) > 1.E-7 ) { 
				// Energy conservation.
				throw BasicException("Energy conservation failure on OmegaN_2PionN_action\n");
			}
			// Check if is Pauli Permitible, if it is then, update the momentum for 
		//	std::cout <<  "OmegaN_2PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
			if ( nuc->ChangeNucleon( idx,  v[2] ) ){
				mpool->RemoveMeson(key);
		//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//		mpool->PutMeson(v[1]);
			//	v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() <<  ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
			//	v[1].Momentum().Print();
				//v[2].Momentum().Print();
				v.pop_back();  // erase nucleon particle of vector, so vector containe just new mesons
				return true;
			} else return false;  
		} else{ 
			std::cout <<  " TGenPhaseSpace warning" << std::endl;
			v.clear();  
			return false;
		}
	} else return false;
}

bool PionN_PhiN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "PionN_PhiN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 3.8434){
		if ((meson.PdgId()  == CPT::pion_p_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::phi_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::pion_m_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::phi_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);           
	//	std::cout <<  "PionN_PhiN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		// the upper limit: Vector_invariant_mass + omega_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
	//	std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
	//	std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//	v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
	//  	v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool PionN_RhoN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "PionN_RhoN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.94142){
		if ((meson.PdgId()  == CPT::pion_p_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::rho_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::pion_0_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::rho_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::pion_m_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::rho_0_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else	if ((meson.PdgId()  == CPT::pion_0_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::rho_p_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass()*CPT::effn);         
		//std::cout <<  "PionN_RhoN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
		// the upper limit: Vector_invariant_mass + meson_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
		//std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
		//std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//	v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
		//	v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool PhiN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "PhiN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 3.8372){
		if ((meson.PdgId()  == CPT::phi_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::pion_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		}else if ((meson.PdgId()  == CPT::phi_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::pion_p_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);      
	//	std::cout <<  "PhiN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId() << ", v[0].PdgId() = " << v[0].PdgId() << ", v[1].PdgId() = " << v[1].PdgId() << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//	v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
		//	v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}
bool RhoN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
//	std::cout <<  "RhoN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 2.94163){
		if ((meson.PdgId()  == CPT::rho_p_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::pion_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::rho_0_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::pion_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		}else 	if ((meson.PdgId()  == CPT::rho_m_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::pion_0_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::rho_0_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::pion_p_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::neutron_ID) );  // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);          
	//	std::cout <<  "RhoN_PionN_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId() << ", v[0].PdgId() = " << v[0].PdgId() << ", v[1].PdgId() = " << v[1].PdgId() << std::endl;
		VMcreateMeson( meson, nucleon, v , s);  
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if ( nuc->ChangeNucleon( idx,  v[1] ) ){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
	//		v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
		//	v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}
		v.clear();  
		return false;  
	} else return false;
}

bool RhoN_mPionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v){


	/*
	 * NOTE: Until we find proper final states for all missing FSI of the rho meson
	 * we'll keep this action as always Pauli permitible and maintain the nucleon in
	 * the same state. As far as the system goes this is not ideal
	 * but it is the best option considering that the other possibilities
	 * would involve arbitrary non-realistic nucleon momentum anyway, or invalid final state
	 * with blocking of the event and consequently error in the account of emitted mesons.
	 * This last situation proved to be the most likely to happen.
	 * Considering the high energies involved, it would be unlikely for Pauli principle
	 * to block the FSI anyway so we're going to allow it everytime and leave the nucleon
	 * unchanged.
	 */

	//cout << "Rho0 mPionN chosen" << endl;
	mpool->RemoveMeson(key);
	return true;
/*
	Int_t new_nucleon_id = (*nuc)[idx].PdgId();
	TVector3 r = (*nuc)[idx].Position();
	
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	
	v.push_back( ParticleDynamics(CPT::pion_p_ID) );  
	v.push_back( ParticleDynamics(CPT::pion_m_ID) );
	v.push_back( ParticleDynamics(new_nucleon_id) );
	v[0].SetPosition(r);
	v[1].SetPosition(r);
	v[0].SetMass( v[0].GetInvariantMass() * CPT::effn );
	v[1].SetMass( v[1].GetInvariantMass() * CPT::effn );
	v[2].SetMass( v[2].GetInvariantMass() * CPT::effn );
	
	TLorentzVector W = meson.Momentum()*(1./1000.) + nucleon.Momentum()*(1./1000.);
	
	//(Momentum, Energy units are GeV/c, GeV)
	Double_t masses[3] = { v[0].GetMass()/1000, v[1].GetMass()/1000., v[2].GetMass()/1000.};

	TGenPhaseSpace event;
	if( event.SetDecay(W, 3, masses) ){
		
		event.Generate();
	
		v[0].SetMomentum( *(event.GetDecay(0)) * 1000. ); //back to MeV
		v[1].SetMomentum( *(event.GetDecay(1)) * 1000. ); //back to MeV
		v[2].SetMomentum( *(event.GetDecay(2)) * 1000. ); //back to MeV	
		
		TLorentzVector Wfinal = v[0].Momentum() + v[1].Momentum() + v[2].Momentum();
		
		double s = W.M2();
		double s2 = Wfinal.M2();
		if ( fabs((s - s2)/s) > 1.E-7 ) { 
			// Energy conservation.
			throw BasicException("Energy conservation failure on RhoN_mPionN_action\n");
		}
		
		if ( nuc->ChangeNucleon( idx, v[2] ) ){ 
			mpool->RemoveMeson(key);
			//std::cout << "rho0N -> mPionN allowed!" << std::endl;    
			// Passed by Pauli Blocking ... 
			TVector3 dr0 = v[0].Momentum().Vect(); dr0 *= .01 / v[0].Momentum().E();
			TVector3 dr1 = v[1].Momentum().Vect(); dr1 *= .01 / v[1].Momentum().E();    
			TVector3 dr2 = v[2].Momentum().Vect(); dr2 *= .01 / v[2].Momentum().E();
			v[0].Position() += dr0;
			v[1].Position() += dr1;
			(*nuc)[idx].Position() += dr2;
			v.pop_back();  // erase nucleon particle of vector, so vector contain just new mesons
			return true;
		}
		return false;
	}
	else{ 
		std::cout <<  " TGenPhaseSpace warning" << std::endl;
		v.clear();  
		return false;
	}
*/	
}

bool NJPsi_ND_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "NJPsi_ND_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 17.3){
		if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::D_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::lambdac_p_ID) ); // Create the hyperon_c
		}else if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::Dbar_0_ID) ); // Create the meson
			v.push_back( ParticleDynamics(CPT::lambdac_p_ID) );  // Create the hyperon_c
		} else return false;
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(nucleon.Position());
		v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);           
//		std::cout <<  "NJPsi_ND_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId() << ", v[0].PdgId() = " << v[0].PdgId() << ", v[1].PdgId() = " << v[1].PdgId() << std::endl;
		// the upper limit: Vector_invariant_mass + omega_width ...
//		std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
//		std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;

//		std::cout << "lambda LifeTime() = " << v[1].LifeTime() << std::endl; 
//		std::cout <<   " v[1].GetInvariantMass() = " << v[1].GetInvariantMass() <<   " v[1].GetMass() = " << v[1].GetMass() << ", v[1].GetTotalWidth() " << v[1].GetTotalWidth() << ", lambda_mass = " << lam_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);
//		std::cout <<   " meson created\n";
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if (nuc->PutRessonance( idx, v[1] )){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
//			v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
//			v[1].Momentum().Print(); std::cout  <<   " v[1].GetMass() = " << v[1].GetMass() << ", Abs(P) = " << v[1].Momentum().Mag() << std::endl;
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}

bool NJPsi_NDast_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];   
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
	//std::cout <<  "NJPsi_NDast_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 17.3){
		if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::Dast_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::lambdac_p_ID) ); // Create the hyperon_c
		}else if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::Dastbar_0_ID) );  // Create the meson
			v.push_back( ParticleDynamics(CPT::lambdac_p_ID) );  // Create the hyperon_c
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[1].SetPosition(nucleon.Position());
//		std::cout <<  "NJPsi_NDast_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId() << ", v[0].PdgId() = " << v[0].PdgId() << ", v[1].PdgId() = " << v[1].PdgId() << std::endl;
		// the upper limit: Vector_invariant_mass + omega_width ...
		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
//		std::cout << "meson LifeTime() = " << v[0].LifeTime() << std::endl; 
//		std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << ", vec_mass = " << vec_mass << std::endl;

		double em_upperlam = v[1].GetInvariantMass() + v[1].GetTotalWidth();
		em_upperlam = ( em_upperlam > TMath::Sqrt(s) - v[0].GetMass() ) ? TMath::Sqrt(s) -  v[0].GetMass(): em_upperlam;
		double lam_mass = resonance_mass( em_upperlam, v[1].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
		v[1].SetMass(lam_mass);      
		v[1].SetLifeTime( v[1].HalfLife() ); 
//		std::cout << "lambda LifeTime() = " << v[1].LifeTime() << std::endl; 
//		std::cout <<   " v[1].GetInvariantMass() = " << v[1].GetInvariantMass() <<   " v[1].GetMass() = " << v[1].GetMass() << ", v[1].GetTotalWidth() " << v[1].GetTotalWidth() << ", lambda_mass = " << lam_mass << std::endl;
		VMcreateMeson( meson, nucleon, v , s);
 
		// Check if is Pauli Permitible, if it is then, update the momentum for 
		if (nuc->PutRessonance( idx, v[1] )){
			mpool->RemoveMeson(key);
	//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
//			v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() << ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
//			v[1].Momentum().Print();
			v.pop_back();  // erase nucleon particle of vector, so vector just contains new mesons
			return true;
		}  
		v.clear();  
		return false;  
	} else return false;
}

bool NJPsi_NDDbar_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v){
	ParticleDynamics meson = (*mpool)[key];
	ParticleDynamics nucleon = (*nuc)[idx];
	double s =  (meson.Momentum() + nucleon.Momentum()).M2() ;
//	std::cout <<  "NJPsi_NDDbar_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << ", s = " << s/1e6 << std::endl;
	if (s/1000./1000. > 21.863){
		if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::neutron_ID)){
			v.push_back( ParticleDynamics(CPT::D_m_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::D_p_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::neutron_ID) ); // Create the new nucleon 
		} else if ((meson.PdgId()  == CPT::J_Psi_ID) && (nucleon.PdgId()  == CPT::proton_ID)){
			v.push_back( ParticleDynamics(CPT::D_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::Dbar_0_ID) ); // Create the meson 
			v.push_back( ParticleDynamics(CPT::proton_ID) ); // Create the new nucleon 
		} else return false;
		// the upper limit: meson_invariant_mass + meson_width ...
	  
		v[0].SetPosition(meson.Position()); // set meson and nucleon position 
		v[0].SetMass(v[0].GetInvariantMass() * CPT::effn);
		v[1].SetPosition(meson.Position());
		v[1].SetMass(v[1].GetInvariantMass() * CPT::effn);
		 // set mesons mass 
/*		double em_upper = v[0].GetInvariantMass() + v[0].GetTotalWidth();
		em_upper = ( em_upper > TMath::Sqrt(s) - v[1].GetMass() ) ? TMath::Sqrt(s) -  v[1].GetMass(): em_upper;
		double vec_mass = resonance_mass( em_upper, v[0].PdgId() );
		v[0].SetMass(vec_mass);      
		v[0].SetLifeTime( v[0].HalfLife() ); 
		std::cout << "meson 1  LifeTime() = " << v[0].LifeTime() << std::endl; 
		std::cout <<   " v[0].GetInvariantMass() = " << v[0].GetInvariantMass() <<   " v[0].GetMass() = " << v[0].GetMass() << ", v[0].GetTotalWidth() " << v[0].GetTotalWidth() << std::endl;

		double em_upperlam = v[1].GetInvariantMass() + v[1].GetTotalWidth();
		em_upperlam = ( em_upperlam > TMath::Sqrt(s) - v[0].GetMass() ) ? TMath::Sqrt(s) -  v[0].GetMass(): em_upperlam;
		double lam_mass = resonance_mass( em_upperlam, v[1].PdgId() );
		v[1].SetMass(lam_mass);      
		v[1].SetLifeTime( v[1].HalfLife() ); 
		std::cout << "meson 1 LifeTime() = " << v[1].LifeTime() << std::endl; 
		std::cout <<   " v[1].GetInvariantMass() = " << v[1].GetInvariantMass() <<   " v[1].GetMass() = " << v[1].GetMass() << ", v[1].GetTotalWidth() " << v[1].GetTotalWidth() << std::endl;
*/		v[2].SetPosition(nucleon.Position());
		v[2].SetMass(v[2].GetInvariantMass()*CPT::effn);           

	//***********************************************************************/	
	   //(Momentum, Energy units are Gev/C, GeV)
		TLorentzVector target = nucleon.Momentum()*(1./1000.);
		TLorentzVector beam = meson.Momentum()*(1./1000.);
		TLorentzVector W = beam + target;
	   	Double_t masses[3] = { v[0].GetMass()/1000.,  v[1].GetMass()/1000.,  v[2].GetMass()/1000.} ;
		TLorentzVector P = lorentzRotation(W, W);
		TGenPhaseSpace event;
		TLorentzVector Wfinal;
		if ( event.SetDecay(P, 3, masses) ) {
			event.Generate();
			for (unsigned int i = 0; i < v.size(); i++) {
				v[i].SetMomentum( inverseLorentzRotation(W, *(event.GetDecay(i))) * 1000. ); // set meson and nucleon Momentum back to MeV	
				Wfinal += v[i].Momentum();
			}
			
			double s2 = Wfinal.M2();
			if ( fabs((s - s2)/s) > 1.E-7 ) { 
				// Energy conservation.
				throw BasicException("Energy conservation failure on NJPsi_NDDbar_action\n");
			}
      
			// Check if is Pauli Permitible, if it is then, update the momentum for 
//			std::cout <<  "NJPsi_NDDbar_action, key = " << key << ", idx = " << idx  << ", meson.PdgId() = " << meson.PdgId()  << ", nucleon.PdgId() = " << nucleon.PdgId()<< std::endl;
			if ( nuc->ChangeNucleon( idx,  v[2] ) ){
				mpool->RemoveMeson(key);
		//		mpool->PutMeson(v[0]);  // because is adding in timeOrdering::UpdateAll()
		//		mpool->PutMeson(v[1]);
//				v[0].Momentum().Print(); std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() <<  ", Abs(P) = " << v[0].Momentum().Mag() << std::endl;
//				v[1].Momentum().Print();
//				v[2].Momentum().Print();
//				std::cout  <<   " v[0].GetMass() = " << v[0].GetMass() <<  ", 0 Abs(P) = " << v[0].Momentum().E() <<   ", v[1].GetMass() = " << v[1].GetMass() <<  ", 1 Abs(P) = " << v[1].Momentum().E()<<   ", v[2].GetMass() = " << v[2].GetMass() <<  ", 2 Abs(P) = " << v[2].Momentum().E() << std::endl;
				v.pop_back();  // erase nucleon particle of vector, so vector containe just new mesons
				return true;
			} else return false;  
		} else{ 
			std::cout <<  " TGenPhaseSpace warning" << std::endl;
			v.clear();  
			return false;
		}
	} else return false;
}
