/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Lepton_n_cross_sections.hh"

// begin

//CCqe 

Double_t nun_mump_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
  /*
   * reaction nu + neutron -> mu^- + proton 
   * quasielastic
   * 
   */
  
    
	if(CPT::IsMeson(lep->PdgId())){
	    return 0;
	}
	
/*	bool our = false; // refer to BW ansatz
	bool expfit = false; //exp data fit
	bool cimc = false; // cim model set up */


	
	Double_t CS=0.;

	Double_t v_coef[7] = {-0.3450223152762,4.276563615134,-6.340240301694,4.621189710321,-1.730245737751,0.3037288208099,-0.01749117839797};  //experimental data fit only deuteron

//	Double_t factor =  0.1 ; //to let crosssections in microbarn 
	
	if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::neutron_ID  ){		
		return 0;
	}
	else {
	

		Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t En = 0.;
		En = (W - nucleon->GetMass())/1000; //gev units
		if(En<=0.111)
			return 0;

//	      	return 1;  //for full usage

		CS = v_coef[6];
//		std::cout << "initial Cs " << CS  << "En " << En << std::endl;
		for(int i=5; i>=0;i--){
			CS = En*CS + v_coef[i]; 

		}
		CS = CS*factor;

		
//		std::cout << " calculo CS " <<  CS << std::endl;
//		std:: cout << " nun_mump_cross_section " << CS << std::endl;

		return ( CS < 0 ) ? 0. : CS;
	}


return 0;

}


// CCres

// Double_t nup_mump33pp_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result muon + delta plus plus 

Double_t nup_mump33pp_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
   // nu + proton --> Delta++ + muon

  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::proton_ID  )		
      return 0;

  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_pp_ID);
    ParticleDynamics *muon = new ParticleDynamics(CPT::muon_m_ID);

    
    double min_mass  = 1.e-8;
    double sigma_0 = 0.65;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_pp_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_pp_ID);
    double inte_W = ps_int(W-Del_mass,muon->GetInvariantMass())/ps_int(W-Del_mass,min_mass);
    
    //Double_t 
    
    // resonance_mass() CPT::p33_pp_ID // delta pp mass CPT::effd // efective mass  
    
    delete delta_p;
    delete muon;
 //   std:: cout << " nup_mump33pp_cross_section " << sigma_0*BW_W*inte_W << std::endl;

    return sigma_0*BW_W*inte_W;
    }

return 0;

}

//____________________________________________________________________________


Double_t nun_mump33p_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + neutron  --> Delta+ + muon --> muon + pi+ + neutron


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::neutron_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_pp_ID);
    ParticleDynamics *muon = new ParticleDynamics(CPT::muon_m_ID);

    
    double min_mass  = 1.e-8;
    double sigma_0 = .28;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_p_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_p_ID);
    double inte_W = ps_int(W-Del_mass,muon->GetInvariantMass())/ps_int(W-Del_mass,min_mass);
    
    //Double_t 
    
    // resonance_mass() CPT::p33_pp_ID // delta pp mass CPT::effd // efective mass  
    
    delete delta_p;
    delete muon;

  //  std:: cout << " nun_mump33p_pip_cross_section " << sigma_0*BW_W*inte_W << std::endl;

    return sigma_0*BW_W*inte_W;
    }

return 0;

}


//____________________________________________________________________________

// Double_t nun_mump33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result muon + delta plus 

Double_t nun_mump33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + neutron  --> Delta+ + muon --> muon + pi0 + proton


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::neutron_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_pp_ID);
    ParticleDynamics *muon = new ParticleDynamics(CPT::muon_m_ID);

    
    double min_mass  = 1.e-7;
    double sigma_0 = .29;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_p_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_p_ID);
    double inte_W = ps_int(W-Del_mass,muon->GetInvariantMass())/ps_int(W-Del_mass,min_mass);
    
    //Double_t 
    
    // resonance_mass() CPT::p33_pp_ID // delta pp mass CPT::effd // efective mass  
    
    delete delta_p;
    delete muon;
  //      std:: cout << " nun_mump33p_pi0_cross_section " << sigma_0*BW_W*inte_W << std::endl;

    return sigma_0*BW_W*inte_W;
    }

return 0;

}

//____________________________________________________________________________________________________


//Nress

// Double_t nun_nup330_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result neutrino + delta 0
// Double_t nun_nup330_pim_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result neutrino + delta 0



Double_t nun_nup330_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + neutron  --> Delta0 + neutrino --> neutrino + pi0 + neutron


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::neutron_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_0_ID);
    
    double min_mass  = 1.e-8;
    double sigma_0 = .09;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_0_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_0_ID);
//    double inte_W = ps_int(W-Del_mass,min_mass)/ps_int(W-Del_mass,min_mass);
    double inte_W = 1.;
    //Double_t 
    
    // resonance_mass() CPT::p33_pp_ID // delta pp mass CPT::effd // efective mass  
    
    delete delta_p;
      //      std:: cout << " nun_nup330_pip_cross_section " << sigma_0*BW_W*inte_W << std::endl;

    return sigma_0*BW_W;
    }

return 0;

}

//____________________________________________________________________________________________________


Double_t nun_nup330_pim_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + neutron  --> Delta0 + neutrino --> neutrino + pi- + proton


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::neutron_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;
      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_0_ID);
    
    double min_mass  = 1.e-8;
    double sigma_0 = .09;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_0_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_0_ID);
   // double inte_W = ps_int(W-Del_mass,muon->GetInvariantMass())/ps_int(W-Del_mass,min_mass);
    double inte_W = 1.;
    //Double_t 
    
    // resonance_mass() CPT::p33_pp_ID // delta pp mass CPT::effd // efective mass  
    
 //   std:: cout << " nun_nup330_pim_cross_section " << sigma_0*BW_W*inte_W << std::endl;

    delete delta_p;
    return sigma_0*BW_W;
    }

return 0;

}

//____________________________________________________________________________

// Double_t nup_nup33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);//result neutrino + delta plus
// Double_t nup_nup33p_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);//result neutrino + delta plus

Double_t nup_nup33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + proton  --> Delta+ + neutrino --> neutrino  + pi0 + proton


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::proton_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_p_ID);
    
    double min_mass  = 1.e-8;
    double sigma_0 = .05;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_p_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_p_ID);
    double inte_W = ps_int(W-Del_mass,min_mass)/ps_int(W-Del_mass,min_mass);
    
 //   std:: cout << " nup_nup33p_pi0_cross_section " << sigma_0*BW_W*inte_W << std::endl;
      
    delete delta_p;
    return sigma_0*BW_W*inte_W;
    }

return 0;

}


//______________________________________________________________________________
Double_t nup_nup33p_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch){
  
     // nu + proton  --> Delta+ + neutrino --> neutrino  + pi+ + neutron


  if( lep->PdgId()!=CPT::neutrino_m_ID || nucleon->PdgId()!=CPT::proton_ID  ){		
      return 0;
  }
  
  bool cim_model = false; 
  bool BWanzt = true; 

  
	Double_t CS=0.;
	
Double_t W = (lep->Momentum() + nucleon->Momentum()).M(); //MeV
	
  if(cim_model) {
    // poner aqui los datos que envie cesar 
    Double_t v_coef[7] ={-0.0042163747,0.1353091885,1.0816695603,-2.4965144108,2.2320429265,-0.9137581302,0.1421407148};
    Double_t En = 0.;
    En = (W - nucleon->GetMass())/1000; //gev units
    
    if(En<=0.123)
      return 0;
	
    CS = v_coef[6];
    
    for(Int_t i=5; i>=0;i--){
      CS = En*CS + v_coef[i]; 
      }
    CS = CS*factor;

      return ( CS < 0 ) ? 0. : CS;
      }
	
    

  if(BWanzt){
    
    /* this is going to be aike for all other think into make a single funtion */
    ParticleDynamics *delta_p = new ParticleDynamics(CPT::p33_p_ID);
    
    double min_mass  = 1.e-8;
    double sigma_0 = 0.05;
    
 //   std::cout << delta_p->GetInvariantMass() << "GetInvariantMass  " << std::endl;

 //   std::cout << CPT::effd*delta_p->GetInvariantMass() << "GetInvariantMass*effd  " << std::endl;
    
    Double_t Del_mass = resonance_mass(W,CPT::p33_p_ID);
    
    double BW_W = Del_mass/resonance_mass(delta_p->GetInvariantMass(),CPT::p33_p_ID);
   // double inte_W = ps_int(W-Del_mass,muon->GetInvariantMass())/ps_int(W-Del_mass,min_mass);
    double inte_W = 1;

 //   std:: cout << " nup_nup33p_pip_cross_section " << sigma_0*BW_W*inte_W << std::endl;
  
    delete delta_p;
    return sigma_0*BW_W;
    }

return 0;

}

/////// Helper function
double ps_int(double E, double m ){
  
  double E2 = E*E;
	double m2 = m*m;
	double rest = E2-m2;
	if(rest<=0)
		return 0;
	else{	
	double P = TMath::Sqrt(rest);
	
	return E*P + m2*TMath::Log(m/(E+P));
	}
  
}


