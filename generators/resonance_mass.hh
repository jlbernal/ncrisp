/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __resonance_mass_pHH
#define __resonance_mass_pHH

#include "TMath.h"
#include "TRandom.h"
#include "CrispParticleTable.hh"

///
//_________________________________________________________________________________________________										

double resonance_mass( double em_upper, int p_id );
double resonance_mass_outside( double em_upper, int p_id ); //outside nucleus, no effective mass is considered

#endif
