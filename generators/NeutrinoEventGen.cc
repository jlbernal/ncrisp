/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "NeutrinoEventGen.hh"
//#include <cstdlib>

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(NeutrinoEventGen);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

NeutrinoEventGen::NeutrinoEventGen(): EventGen(), channels(){
	
	channels.SetOwner();
	tcs = 0.;
}

//_________________________________________________________________________________________________										

NeutrinoEventGen::~NeutrinoEventGen(){

	channels.Delete();
}

//_________________________________________________________________________________________________										

Double_t NeutrinoEventGen::TotalCrossSection(Dynamics& p1, Dynamics& p2){

	Double_t tcs1 = 0.;
  
	for ( Int_t i = 0; i < channels.GetEntriesFast(); i++ ) {
		tcs1 += ((LeptonNucleonChannel*)channels[i])->CrossSection().Calculate( &p1, &p2);
	}
	return 10*tcs1; //testfase
}

//_________________________________________________________________________________________________										

Int_t NeutrinoEventGen::Generate(Double_t TKinetic, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char* opt, std::ostream *os) {
		return -1;
}

//_________________________________________________________________________________________________


Int_t NeutrinoEventGen::Generate(Double_t TKinetic, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os) {
	
	TVector3 temp_pos;	
	ParticleDynamics neutrino(CPT::neutrino_m_ID);
	neutrino.SetMass( 0. );
	neutrino.BindIt(true);


//	system("pwd");

    
    bool flux = true;
    if(flux){
		TFile *f = new TFile("exp_data/neutrino/flux.root","read");
    
		TH1D *sh;
		sh = (TH1D*) f->Get("Neutrino Flux");
    
		TKinetic=0;
		while(TKinetic<50)
			TKinetic = sh->GetRandom()*1000;
    
		delete sh;
		delete f;
	}
    	
	cout <<  "energia cinetica " << TKinetic << endl;
	
	neutrino.SetMomentum( TLorentzVector( TKinetic, 0., 0., TKinetic ) ) ; //the beam axis is X
	neutrino.SetPosition( StartPosition(nuc) );
	
	Int_t n_add = lpool.PutLepton(neutrino);

	
//	cout << "Aqui ok" <<endl;
	for ( Int_t i = 0; i < nuc.GetA() - 1; i++ ) {
		Double_t tcs = TotalCrossSection(neutrino, nuc[i]);
		Double_t sum = 0.;
		Double_t x = gRandom->Uniform();
		for ( Int_t j = 0; j < channels.GetEntriesFast(); j++ ) { 	
		  sum += ((LeptonNucleonChannel*)channels[j])->CrossSection().Value() / tcs;
			if ( x < sum ) {
				temp_pos =  nuc[i].Position();
				std::vector<ParticleDynamics> v;
				Double_t t = 0.;
				if (((LeptonNucleonChannel*)channels[j])->DoAction(&lpool, n_add, i, &nuc, t, &mpool, &v)){
					nuc.SetMomentum(nuc.Momentum() + neutrino.Momentum());
					nuc.SetAngularMomentum(nuc.AngularMomentum() + neutrino.Position().Cross(neutrino.Momentum().Vect()));
					std::cout << "Channel: " << ((LeptonNucleonChannel*)channels[j])->CrossSection().GetName() << std::endl;
					lpool[n_add].SetPosition(temp_pos);
					nuc.Set_InitE(TKinetic);
					return j;					  
					}
				}	
			}
		   
	} // First 'for' closing brace.
    lpool.RemoveLepton(n_add);
    return -1;
}

//_________________________________________________________________________________________________										

TVector3 NeutrinoEventGen::StartPosition(NucleusDynamics& nuc){

	Double_t r = pow(gRandom->Uniform(),0.5) * nuc.GetRadium();
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z);
	Double_t x = - TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	
	return TVector3(x, y, z);
}

//_________________________________________________________________________________________________										

