/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __Meson_n_actions_pHH
#define __Meson_n_actions_pHH

#include "CrispParticleTable.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "resonance_mass.hh"
#include "TGenPhaseSpace.h"
#include "transform_coords.hh" 
#include "TLorentzVector.h"
#include "TF1.h"
#include "VectorMeson_Diff_Cross_Sections.hh"
#include "relativisticKinematic.hh"


//
//_________________________________________________________________________________________________										
// Deg1:Np_No:No_Np:No_Nr:Nr_No:No_els:No_N2p:Np_No1:Np_Nh:Np_Nr:No_Np1:Nh_Np:Nr_Np

bool mesonN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool rhoN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool omegaN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool phiN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool JPsiN_elastic_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);

bool pion_absorption_Delta1232( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_Delta1700( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_Delta1950( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_N1440( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_N1520( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_N1535( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );
bool pion_absorption_N1680( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );

bool meson_elastic_scattering_action ( Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t, std::vector<ParticleDynamics>& v );

bool PionN_OmegaN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool OmegaN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool OmegaN_RhoN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool RhoN_OmegaN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool OmegaN_2PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool PionN_PhiN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool PionN_RhoN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool PhiN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool RhoN_PionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);
bool RhoN_mPionN_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t &t , std::vector<ParticleDynamics>& v);

bool NJPsi_ND_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v);
bool NJPsi_NDast_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v);
bool NJPsi_NDDbar_action(Int_t &key, MesonsPool* mpool, Int_t &idx, NucleusDynamics* nuc, Double_t & , std::vector<ParticleDynamics>& v);

#endif
