/* ================================================================================
 * 
 * 	Copyright 2008, 2014, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Math/SpecFunc.h"

#include "gn_cross_sections.hh"

bool FermiMotion(true);

//____________________________________________________________________________________________										
Double_t breitWigner( Double_t sqrt_s, Double_t* p ){
	Double_t mass_adjust = p[0];
	Double_t Gamma_0 = p[1];  
	Double_t sigma0 = p[2];
	Double_t d = ( TMath::Power( TMath::Power(sqrt_s, 2) - TMath::Power(mass_adjust,2), 2) + TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2) );
	return sigma0 * (TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2))/d; 
}
//_____________________________________________________________________________________________	


Double_t functionL(unsigned n, Double_t x){
	Double_t alpha = 2.; //according to A.S. Iljinov et al. Nuclear Physics A 616 (1997) 575-605
	return exp(-x/2.)*pow(x,alpha/2.)*ROOT::Math::assoc_laguerre(n, alpha, x);
}
//_____________________________________________________________________________________________	


Double_t totalNonResonantCS( Dynamics* gamma, Dynamics* nucleon, Double_t* ){
	// in microbarn (GEV)^1/2  
	if (gamma->PdgId() == CPT::photon_ID){
		const Double_t bp1 = 91., bp2 = 71.4; 
		const Double_t bn1 = 87., bn2 = 65.;
		const Double_t pMass = (CPT::p_mass*CPT::effn)/1000.; //mass in GeV
		const Double_t nMass = (CPT::n_mass*CPT::effn)/1000.; //mass in GeV
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.; //center mass energy in GeV
		// the energy in GeV
		Double_t E = 0.;
		if ( nucleon->PdgId() == CPT::proton_ID ){
			if(FermiMotion){
				E = W*W/pMass/2. - pMass/2 ;
			}
			else{
				E = gamma->Momentum().E() / 1000.; //without Fermi motion
			}
			Double_t x = -2. * ( E - 0.139 );      
			return ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );  
		} else if ( nucleon->PdgId() == CPT::neutron_ID )    {
			if(FermiMotion){
				E = W*W/nMass/2. - nMass/2;
			}
			else{
				E = gamma->Momentum().E() / 1000.; //without Fermi motion
			}
			Double_t x = -2. * ( E - 0.139 );      
			return  ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
		}else return 0.; 
	} else return 0;
}
// Delta(1232) P33
Double_t p33Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}
		
		Double_t p33[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		p33[0] = cch[i];
		p33[1] = cch[1 + i];
		p33[2] = cch[2 + i];
		return  breitWigner (E_cm, p33);
	} else return  0.;
}
// N(1440)     P11
Double_t p11Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;    
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}
			
		Double_t p11[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		p11[0] = cch[i];
		p11[1] = cch[1 + i];
		p11[2] = cch[2 + i];
		return  breitWigner (E_cm, p11);
	}return  0.;
}
// N(1520)     D13
Double_t d13Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;    
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}
		
		Double_t d13[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		d13[0] = cch[i];
		d13[1] = cch[1 + i];
		d13[2] = cch[2 + i];
		return  breitWigner (E_cm, d13);
	}else return  0.;
}
// N(1680)     F15
Double_t f15Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;    
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}

		Double_t f15[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		f15[0] = cch[i];
		f15[1] = cch[1 + i];
		f15[2] = cch[2 + i];
		return  breitWigner (E_cm, f15);
	} else return  0.;
}
// N(1535)     S11
Double_t s11Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;    
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}

		Double_t s11[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		s11[0] = cch[i];
		s11[1] = cch[1 + i];
		s11[2] = cch[2 + i];
		return  breitWigner (E_cm, s11);
	} return  0.;  
}
// Delta(1950)     F37
Double_t f37Formation ( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	if (gamma->PdgId() == CPT::photon_ID){
		Double_t E_cm = 0.;
		if(FermiMotion){
			E_cm = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;    
		}
		else{
			//without Fermi motion
	 		Double_t ncMass = nucleon->GetMass()/1000.;
	 		E_cm = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}

		Double_t f37[3];
		int i = 0;
		if ( nucleon->PdgId() == CPT::neutron_ID )
			i = 3;
		f37[0] = cch[i];
		f37[1] = cch[1 + i];
		f37[2] = cch[2 + i];
		return  breitWigner (E_cm, f37);
	} else return  0.;
}
// reference at: Rossi P. Phys Rev C 40(1989)2412
Double_t qdCrossSection( Dynamics* gamma, Dynamics* nucleus, Double_t* cch){
	if (gamma->PdgId() == CPT::photon_ID){
		const Double_t c1 = 27.57;     // microbarn/sr
		const Double_t c2 = -21.69;    // GeV^-1
		const Double_t c3 = 143.19;    // microbarn/sr
		const Double_t c4 = -84.93;    // 1/GeV
		const Double_t c5 = 9.69;      // microbarn/sr
		const Double_t c6 = -16.34;    // 1/GeV
		const Double_t c7 = 0.290;     // GeV
		const Double_t c8 = 68.46;     // 1/(GeV*GeV)
		Double_t A = ((NucleusDynamics*)nucleus)->GetA();
		Double_t Z = ((NucleusDynamics*)nucleus)->GetZ();
		Double_t N = A - Z;  
		Double_t L = 6.8 - 11.2/pow(A, 0.666) + 5.7/pow(A, 1.333);
		// M. B. Chadwick Phys Rev C 44(1991) 814  
		// gamma energy in MeV ...
		Double_t omega = gamma->Momentum().E()/1000.; 
		Double_t fe = 8.3714E-2 - 9.8343E-3 * omega + 4.1222E-4 * TMath::Power(omega,2) - 3.4762E-6 * omega * TMath::Power(omega,2) + 9.3537E-9 * TMath::Power(omega,2) * TMath::Power(omega,2);
		Double_t E = c1 * TMath::Exp( c2 * omega ) + c3 * TMath::Exp( c4 * omega ) + (c5 + c6 * omega) / ( 1. + c8 * TMath::Power(omega - c7, 2) );    
		Double_t sigma_deuteron = fe * ((L * N * Z) / TMath::Power(A, 2)) * 4. * TMath::Pi() * E;   
		return  (sigma_deuteron > 0) ? sigma_deuteron : 0.;
	} else return  0.;
}
//_____________________________________________________________________________________________										
// Double_t residual_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
// 	Double_t W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
// 	Double_t CS;
// 	if (gamma->PdgId() == CPT::photon_ID){
// 		if ( nucleon->PdgId() == CPT::neutron_ID ) {
// 			if (W < 100 ){
// 				CS = CPT::Instance()->GetT_PhotonResidN()->Eval(W);
// 				CS = CS - gn_nPipPim_Dt(gamma, nucleon, cch);
// 				CS = CS - gn_pPimPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gn_n2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPip2Pim(gamma, nucleon, cch);
// 				CS = CS - gn_n3Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_nPipPimPi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_n2Pip2Pim(gamma, nucleon, cch);
// 				CS = CS - gn_n4Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_nPipPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPim3Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPip2PimPi0(gamma, nucleon, cch);
// 			}
// 			else{
// 				Double_t bn1 = 87., bn2 = 65.;
// 				Double_t nMass = (CPT::n_mass*CPT::effn)/1000.; //mass in GeV
// 				Double_t E =  W*W/nMass/2. - nMass/2;  
// 				Double_t x = -2. * ( E - 0.139 );      
// 
// 				CS = ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
// 				CS = CS - 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) );
// 				CS = CS - 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) );
// 				CS = CS - 2.36765 * ( 0.113964*pow(W,0.328556 ) );
// 				CS = CS - 2.55277 * ( 0.000957289*pow(W,0.753558 ) );
// 				CS = CS - gn_nPipPim_Dt(gamma, nucleon, cch);
// 				CS = CS - gn_pPimPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gn_n2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPip2Pim(gamma, nucleon, cch);
// 				CS = CS - gn_n3Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_nPipPimPi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_n2Pip2Pim(gamma, nucleon, cch);
// 				CS = CS - gn_n4Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_nPipPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPim3Pi0(gamma, nucleon, cch);
// 				CS = CS - gn_pPip2PimPi0(gamma, nucleon, cch);
// 			}
// 			return  CS; //milibar -> microbar
// 		}else if ( nucleon->PdgId() == CPT::proton_ID ) {
// 			if (W < 100 ){
// 				CS = CPT::Instance()->GetT_PhotonResidP()->Eval(W);
// 				CS = CS - gp_pPipPim_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_nPipPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_p2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_pPipPimPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_p3Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_nPip2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_n2PipPim(gamma, nucleon, cch);
// 				CS = CS - gp_p2Pip2Pim(gamma, nucleon, cch);
// 				CS = CS - gp_p4Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_pPipPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_nPip3Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_n2PipPimPi0(gamma, nucleon, cch);
// 			}
// 			else{
// 				Double_t bp1 = 91., bp2 = 71.4; 
// 				Double_t pMass = (CPT::p_mass*CPT::effn)/1000.; //mass in GeV
// 				Double_t E = W*W/pMass/2. - pMass/2;  
// 				Double_t x = -2. * ( E - 0.139 );      
// 				CS = ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );
// 				CS = CS - 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) );
// 				CS = CS - 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) );
// 				CS = CS - 2.36765 * ( 0.113964*pow(W,0.328556 ) );
// 				CS = CS - 2.55277 * ( 0.000957289*pow(W,0.753558 ) );
// 				CS = CS - gp_pPipPim_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_nPipPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_p2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_pPipPimPi0_Dt(gamma, nucleon, cch);
// 				CS = CS - gp_p3Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_nPip2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_n2PipPim(gamma, nucleon, cch);
// 				CS = CS - gp_p2Pip2Pim(gamma, nucleon, cch);
// 				CS = CS - gp_p4Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_pPipPim2Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_nPip3Pi0(gamma, nucleon, cch);
// 				CS = CS - gp_n2PipPimPi0(gamma, nucleon, cch);
// 			}
// 			return  CS; //milibar -> microbar
// 		} else return 0;
// 	} else return 0;
// }

Double_t residual_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	//Double_t W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
	Double_t CS;
	if (gamma->PdgId() == CPT::photon_ID){
		if ( nucleon->PdgId() == CPT::neutron_ID ) {
		  
			CS = totalNonResonantCS(gamma, nucleon, cch);
			CS = CS - rho_0_PhotonCrossSectionN(gamma, nucleon, cch);
			CS = CS - rho_p_m_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - omega_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - phi_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - J_Psi_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - gn_nPipPim_NR(gamma, nucleon, cch);
			CS = CS - gn_pPimPi0_NR(gamma, nucleon, cch);
			CS = CS - gn_n2Pi0_NR(gamma, nucleon, cch);
			
			CS = CS - gn_pPip2Pim(gamma, nucleon, cch);
			CS = CS - gn_n3Pi0(gamma, nucleon, cch);
			CS = CS - gn_nPipPimPi0_NR(gamma, nucleon, cch);
			CS = CS - gn_pPim2Pi0(gamma, nucleon, cch);
			
			CS = CS - gn_n2Pip2Pim(gamma, nucleon, cch);
			CS = CS - gn_n4Pi0(gamma, nucleon, cch);
			CS = CS - gn_nPipPim2Pi0_NR(gamma, nucleon, cch);
			CS = CS - gn_pPim3Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPip2PimPi0(gamma, nucleon, cch);
			
			CS = CS - gn_n2Pip2PimPi0_NR(gamma, nucleon, cch);
			CS = CS - gn_n5Pi0_NR(gamma, nucleon, cch);
			CS = CS - gn_nPipPim3Pi0_NR(gamma, nucleon, cch);
			CS = CS - gn_pPim4Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPip2Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gn_p2Pip3Pim(gamma, nucleon, cch);
			
			CS = CS - gn_n3Pip3Pim(gamma, nucleon, cch);
			CS = CS - gn_n6Pi0(gamma, nucleon, cch);
			CS = CS - gn_nPipPim4Pi0(gamma, nucleon, cch);
			CS = CS - gn_n2Pip2Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPim5Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPip2Pim3Pi0(gamma, nucleon, cch);
			CS = CS - gn_p2Pip3PimPi0(gamma, nucleon, cch);
			
			CS = CS - gn_n3Pip3PimPi0(gamma, nucleon, cch);
			CS = CS - gn_n7Pi0(gamma, nucleon, cch);
			CS = CS - gn_nPipPim5Pi0(gamma, nucleon, cch);
			CS = CS - gn_n2Pip2Pim3Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPim6Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPip2Pim4Pi0(gamma, nucleon, cch);
			CS = CS - gn_p2Pip3Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gn_p3Pip4Pim(gamma, nucleon, cch);
			
			CS = CS - gn_n4Pip4Pim(gamma, nucleon, cch);
			CS = CS - gn_n8Pi0(gamma, nucleon, cch);
			CS = CS - gn_nPipPim6Pi0(gamma, nucleon, cch);
			CS = CS - gn_n2Pip2Pim4Pi0(gamma, nucleon, cch);
			CS = CS - gn_n3Pip3Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPim7Pi0(gamma, nucleon, cch);
			CS = CS - gn_pPip2Pim5Pi0(gamma, nucleon, cch);
			CS = CS - gn_p2Pip3Pim3Pi0(gamma, nucleon, cch);
			CS = CS - gn_p3Pip4PimPi0(gamma, nucleon, cch);
			
			return  CS > 0 ? CS : 0; //microbarn
			
		}else if ( nucleon->PdgId() == CPT::proton_ID ) {
		  
			CS = totalNonResonantCS(gamma, nucleon, cch);
			CS = CS - rho_0_PhotonCrossSectionP(gamma, nucleon, cch);
			CS = CS - rho_p_m_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - omega_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - phi_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - J_Psi_PhotonCrossSection(gamma, nucleon, cch);
			CS = CS - gp_pPipPim_NR(gamma, nucleon, cch);
			CS = CS - gp_nPipPi0_NR(gamma, nucleon, cch);
			CS = CS - gp_p2Pi0_NR(gamma, nucleon, cch);
			
			CS = CS - gp_pPipPimPi0_NR(gamma, nucleon, cch);
			CS = CS - gp_p3Pi0(gamma, nucleon, cch);
			CS = CS - gp_nPip2Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPim(gamma, nucleon, cch);
			
			CS = CS - gp_p2Pip2Pim(gamma, nucleon, cch);
			CS = CS - gp_p4Pi0(gamma, nucleon, cch);
			CS = CS - gp_pPipPim2Pi0_NR(gamma, nucleon, cch);
			CS = CS - gp_nPip3Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPimPi0(gamma, nucleon, cch);
			
			CS = CS - gp_p2Pip2PimPi0_NR(gamma, nucleon, cch);
			CS = CS - gp_p5Pi0_NR(gamma, nucleon, cch);
			CS = CS - gp_pPipPim3Pi0_NR(gamma, nucleon, cch);
			CS = CS - gp_nPip4Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPim2Pi0(gamma, nucleon, cch);
			CS = CS - gp_n3Pip2Pim(gamma, nucleon, cch);
			
			CS = CS - gp_p3Pip3Pim(gamma, nucleon, cch);
			CS = CS - gp_p6Pi0(gamma, nucleon, cch);
			CS = CS - gp_pPipPim4Pi0(gamma, nucleon, cch);
			CS = CS - gp_p2Pip2Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gp_nPip5Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPim3Pi0(gamma, nucleon, cch);
			CS = CS - gp_n3Pip2PimPi0(gamma, nucleon, cch);
			
			CS = CS - gp_p3Pip3PimPi0(gamma, nucleon, cch);
			CS = CS - gp_p7Pi0(gamma, nucleon, cch);
			CS = CS - gp_pPipPim5Pi0(gamma, nucleon, cch);
			CS = CS - gp_p2Pip2Pim3Pi0(gamma, nucleon, cch);
			CS = CS - gp_nPip6Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPim4Pi0(gamma, nucleon, cch);
			CS = CS - gp_n3Pip2Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gp_n4Pip3Pim(gamma, nucleon, cch);
			
			CS = CS - gp_p4Pip4Pim(gamma, nucleon, cch);
			CS = CS - gp_p8Pi0(gamma, nucleon, cch);
			CS = CS - gp_pPipPim6Pi0(gamma, nucleon, cch);
			CS = CS - gp_p2Pip2Pim4Pi0(gamma, nucleon, cch);
			CS = CS - gp_p3Pip3Pim2Pi0(gamma, nucleon, cch);
			CS = CS - gp_nPip7Pi0(gamma, nucleon, cch);
			CS = CS - gp_n2PipPim5Pi0(gamma, nucleon, cch);
			CS = CS - gp_n3Pip2Pim3Pi0(gamma, nucleon, cch);
			CS = CS - gp_n4Pip3PimPi0(gamma, nucleon, cch);
			
			return  CS > 0 ? CS : 0; //microbarn
			
		} else return 0;
	} else return 0;
}

//_________________________________________________________________________________________________
//Rho0 via proton interaction
Double_t rho_0_PhotonCrossSectionP( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	
	if ( nucleon->PdgId() == CPT::proton_ID ){
		Double_t W = 0.;
		if(FermiMotion){
			W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
		}
		else {
			//without Fermi motion
			Double_t ncMass = nucleon->GetMass()/1000.;
			W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}
	
		if ( (gamma->PdgId() == CPT::photon_ID) && (W > 1.745) ){
			Double_t CS;
			if (W < 100 )
				CS = CPT::Instance()->GetT_rho()->Eval(W);
			else 
				CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
			return  CS*1000; //milibar -> microbar
		} else return  0.;
	} else return 0.;
}

//Rho0 via neutron interaction
Double_t rho_0_PhotonCrossSectionN( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	
	if ( nucleon->PdgId() == CPT::neutron_ID ){
		Double_t W = 0.;
		if(FermiMotion){
			W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
		}
		else {
			//without Fermi motion
			Double_t ncMass = nucleon->GetMass()/1000.;
			W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
		}

		if ( (gamma->PdgId() == CPT::photon_ID) && (W > 1.745) ){
			Double_t CS;
			if (W < 100 )
				CS = CPT::Instance()->GetT_rho()->Eval(W);
			else 
				CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
			return  0.15*CS*1000; //milibar -> microbar
		} else return  0.;
	} else return 0.;
}

//Rho+ and Rho-
Double_t rho_p_m_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	Double_t W = 0.;
	if(FermiMotion){
		W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
	}
	else {
		//without Fermi motion
		Double_t ncMass = nucleon->GetMass()/1000.;
		W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
	}

	if ( (gamma->PdgId() == CPT::photon_ID) && (W > 1.745) ){
		Double_t CS;
		if (W < 100 )
			CS = CPT::Instance()->GetT_rho()->Eval(W);
		else 
			CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
		return  0.3*CS*1000; //milibar -> microbar
	} else return  0.;
}

//_____________________________________________________________________________________________										
Double_t omega_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	Double_t W = 0.;
	if(FermiMotion){
		W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
	}
	else {
		//without Fermi motion
		Double_t ncMass = nucleon->GetMass()/1000.;
		W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
	}

	if ( (gamma->PdgId() == CPT::photon_ID) && (W > 1.745) ){
		Double_t CS;
		if (W < 100 )
			CS = CPT::Instance()->GetT_omg()->Eval(W);
		else 
			CS = 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
		return  CS*1000; //milibar -> microbar
	} else return  0.; 
}
//_____________________________________________________________________________________________										
Double_t phi_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	Double_t W = 0.;
	if(FermiMotion){
		W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
	}
	else {
		//without Fermi motion
		Double_t ncMass = nucleon->GetMass()/1000.;
		W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
	}

	if ( (gamma->PdgId() == CPT::photon_ID) && (W > 2.0132) ){
		Double_t CS;
		if (W < 100 )
			CS = CPT::Instance()->GetT_phi()->Eval(W);
		else
			CS = 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
		return  CS*1000; //milibar -> microbar
	} return  0.;
}
//_____________________________________________________________________________________________										
Double_t J_Psi_PhotonCrossSection( Dynamics* gamma, Dynamics* nucleon, Double_t* cch ){
	Double_t W = 0.;
	if(FermiMotion){
		W = (gamma->Momentum() + nucleon->Momentum()).M() / 1000.;
	}
	else {
		//without Fermi motion
		Double_t ncMass = nucleon->GetMass()/1000.;
		W = sqrt(2.*ncMass*(gamma->Momentum().E()/1000.) + ncMass*ncMass);
	}

	if ( (gamma->PdgId() == CPT::photon_ID) && (W > 4.0396) ){
		Double_t CS;
		if (W < 100 )
			CS = CPT::Instance()->GetT_J_Psi()->Eval(W);
		else
			CS = 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
		return CS*1000; //milibar -> microbar 
	}else return  0.;
}


///====================================================   2 pions   =====================================================


///======================================================================================================================
///
/// 	AUXILIARY SECTION:
/// 
///		- CALCULATION OF THE RESONANT PARTS OF 2 PIONS TOTAL CROSS SECTIONS    
///		- CALCULATION OF TOTAL CROSS SECTIONS FOR 2 PIONS
///
///		OBJECTIVE: CALCULATE THE NON-RESONANT CHANNELS FOR THE 2 PIONS PHOTO-PRODUCTION     
///
///======================================================================================================================


Double_t gp_pPipPim_ResPart( Dynamics* gamma, Dynamics* nucleon){
	
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_p_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_p_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_p_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_p_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_p_ID);

	TParticlePDG *delta_0 = CPT::Instance()->ParticlePDG(CPT::p33_0_ID);
	TParticlePDG *delta_pp = CPT::Instance()->ParticlePDG(CPT::p33_pp_ID);
	TParticlePDG *rho_0 = CPT::Instance()->ParticlePDG(CPT::rho_0_ID);
	
	double N1440_p_1 = 0., 
	       N1440_p_2 = 0., 
	       N1440_p_3 = 0.,
	      
	       N1520_p_1 = 0., 
	       N1520_p_2 = 0., 
	       N1520_p_3 = 0., 
	       N1520_p_4 = 0., 
	      
	       N1535_p_1 = 0., 
	       N1535_p_2 = 0., 
	      
	       N1680_p_1 = 0., 
	       N1680_p_2 = 0., 
	       N1680_p_3 = 0., 
	       N1680_p_4 = 0., 
	      
	       Delta1950_p_1 = 0.,
	       Delta1950_p_2 = 0.,
	       Delta1950_p_3 = 0.;
	
	//Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
	
	N1440_p_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(3)->BranchingRatio() * delta_pp->DecayChannel(0)->BranchingRatio();
	N1440_p_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1440_p_3 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(5)->BranchingRatio();
	  
	N1520_p_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(3)->BranchingRatio() * delta_pp->DecayChannel(0)->BranchingRatio();
	N1520_p_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1520_p_3 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(5)->BranchingRatio();
	N1520_p_4 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(9)->BranchingRatio() * rho_0->DecayChannel(0)->BranchingRatio();
	  
	N1535_p_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(3)->BranchingRatio() * delta_pp->DecayChannel(0)->BranchingRatio();
	N1535_p_2 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	  
	N1680_p_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(3)->BranchingRatio() * delta_pp->DecayChannel(0)->BranchingRatio();
	N1680_p_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1680_p_3 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(5)->BranchingRatio() * rho_0->DecayChannel(0)->BranchingRatio();
	N1680_p_4 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(7)->BranchingRatio();
	  
	Delta1950_p_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	Delta1950_p_2 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(5)->BranchingRatio() * delta_pp->DecayChannel(0)->BranchingRatio();
	Delta1950_p_3 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(6)->BranchingRatio() * rho_0->DecayChannel(0)->BranchingRatio();
	
	double Ress = N1440_p_1 + N1440_p_2 + N1440_p_3 + 
		      N1520_p_1 + N1520_p_2 + N1520_p_3 + N1520_p_4 + 
		      N1535_p_1 + N1535_p_2 + 
		      N1680_p_1 + N1680_p_2 + N1680_p_3 + N1680_p_4 +
		      Delta1950_p_1 + Delta1950_p_2 + Delta1950_p_3;
	
	return Ress;
}


Double_t gp_pPipPim_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
	Double_t ncMass = nucleon->GetMass(); //mass in MeV
	Double_t E = 0.;
	if(FermiMotion){
		E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
	}
	else{
		E = gamma->Momentum().E(); //without Fermi motion
	}
	
	Double_t Eth = 321; //MeV
	if(E > Eth){
	      
	      TParticlePDG *rho_0 = CPT::Instance()->ParticlePDG(CPT::rho_0_ID);
	      
	      Double_t NotRes = gp_pPipPim_NR(gamma, nucleon, cch);
	      Double_t Res = gp_pPipPim_ResPart(gamma, nucleon) + rho_0_PhotonCrossSectionP( gamma, nucleon, cch ) * rho_0->DecayChannel(0)->BranchingRatio();
	      
	      double CS =  NotRes + Res;
	      
	      return ( CS < 0 ) ? 0. : CS;
  	} else return 0.;
}

//______________________________________________________________________________________________________________________

Double_t gp_nPipPi0_ResPart( Dynamics* gamma, Dynamics* nucleon ){
  
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_p_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_p_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_p_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_p_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_p_ID);

	TParticlePDG *delta_0 = CPT::Instance()->ParticlePDG(CPT::p33_0_ID);
	TParticlePDG *delta_p = CPT::Instance()->ParticlePDG(CPT::p33_p_ID);
	TParticlePDG *rho_p = CPT::Instance()->ParticlePDG(CPT::rho_p_ID);
	
	double N1440_p_1 = 0., 
	      N1440_p_2 = 0., 
	      N1440_p_3 = 0.,
	      
	      N1520_p_1 = 0., 
	      N1520_p_2 = 0., 
	      N1520_p_3 = 0., 
	      N1520_p_4 = 0., 
	      
	      N1535_p_1 = 0., 
	      N1535_p_2 = 0., 
	      
	      N1680_p_1 = 0., 
	      N1680_p_2 = 0., 
	      N1680_p_3 = 0., 
	      N1680_p_4 = 0., 
	      
	      Delta1950_p_1 = 0.,
	      Delta1950_p_2 = 0.;
	
	//Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
  
	N1440_p_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1440_p_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1440_p_3 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(6)->BranchingRatio();
	
	N1520_p_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1520_p_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1520_p_3 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(6)->BranchingRatio();
	N1520_p_4 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(8)->BranchingRatio() * rho_p->DecayChannel(0)->BranchingRatio();
	
	N1535_p_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1535_p_2 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	
	N1680_p_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1680_p_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1680_p_3 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(6)->BranchingRatio() * rho_p->DecayChannel(0)->BranchingRatio();
	N1680_p_4 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(8)->BranchingRatio();
	
	Delta1950_p_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(3)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	Delta1950_p_2 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	
	double Ress = N1440_p_1 + N1440_p_2 + N1440_p_3 + 
		      N1520_p_1 + N1520_p_2 + N1520_p_3 + N1520_p_4 + 
		      N1535_p_1 + N1535_p_2 + 
		      N1680_p_1 + N1680_p_2 + N1680_p_3 + N1680_p_4 +
		      Delta1950_p_1 + Delta1950_p_2;
	
	return Ress;
}

//______________________________________________________________________________________________________________________

Double_t gn_nPipPim_ResPart( Dynamics* gamma, Dynamics* nucleon ){
  
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_0_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_0_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_0_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_0_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_0_ID);

	TParticlePDG *delta_m = CPT::Instance()->ParticlePDG(CPT::p33_m_ID);
	TParticlePDG *delta_p = CPT::Instance()->ParticlePDG(CPT::p33_p_ID);
	TParticlePDG *rho0 = CPT::Instance()->ParticlePDG(CPT::rho_0_ID);
	
	double N1440_0_1 = 0., 
	      N1440_0_2 = 0., 
	      N1440_0_3 = 0.,
	      
	      N1520_0_1 = 0., 
	      N1520_0_2 = 0., 
	      N1520_0_3 = 0., 
	      N1520_0_4 = 0., 
	      
	      N1535_0_1 = 0., 
	      N1535_0_2 = 0., 
	      
	      N1680_0_1 = 0., 
	      N1680_0_2 = 0., 
	      N1680_0_3 = 0., 
	      N1680_0_4 = 0., 
	      
	      Delta1950_0_1 = 0.,
	      Delta1950_0_2 = 0.,
	      Delta1950_0_3 = 0.;
  
	//Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
  
	N1440_0_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1440_0_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(4)->BranchingRatio() * delta_m->DecayChannel(0)->BranchingRatio();
	N1440_0_3 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(6)->BranchingRatio();
	
	N1520_0_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1520_0_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(4)->BranchingRatio() * delta_m->DecayChannel(0)->BranchingRatio();
	N1520_0_3 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(6)->BranchingRatio();
	N1520_0_4 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(8)->BranchingRatio() * rho0->DecayChannel(0)->BranchingRatio();
	
	N1535_0_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1535_0_2 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(4)->BranchingRatio() * delta_m->DecayChannel(0)->BranchingRatio();
	
	N1680_0_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	N1680_0_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(4)->BranchingRatio() * delta_m->DecayChannel(0)->BranchingRatio();
	N1680_0_3 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(6)->BranchingRatio() * rho0->DecayChannel(0)->BranchingRatio();
	N1680_0_4 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(8)->BranchingRatio();
	
	Delta1950_0_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(3)->BranchingRatio() * delta_p->DecayChannel(1)->BranchingRatio();
	Delta1950_0_2 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(5)->BranchingRatio() * delta_m->DecayChannel(0)->BranchingRatio();
	Delta1950_0_3 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(7)->BranchingRatio() * rho0->DecayChannel(0)->BranchingRatio();
	
	double Ress = N1440_0_1 + N1440_0_2 + N1440_0_3 + 
		      N1520_0_1 + N1520_0_2 + N1520_0_3 + N1520_0_4 + 
		      N1535_0_1 + N1535_0_2 + 
		      N1680_0_1 + N1680_0_2 + N1680_0_3 + N1680_0_4 +
		      Delta1950_0_1 + Delta1950_0_2 + Delta1950_0_3;
	
	return Ress;
}

Double_t gn_nPipPim_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
	Double_t ncMass = nucleon->GetMass(); //mass in MeV
	Double_t E = 0.;
	if(FermiMotion){
		E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
	}
	else{
		E = gamma->Momentum().E(); //without Fermi motion
	}
  
	Double_t Eth = 322; //MeV
	if(E > Eth){
	
	      TParticlePDG *rho_0 = CPT::Instance()->ParticlePDG(CPT::rho_0_ID);
	      
	      Double_t Ress = gn_nPipPim_ResPart(gamma, nucleon) 
			      + rho_0_PhotonCrossSectionN( gamma, nucleon, cch ) * rho_0->DecayChannel(0)->BranchingRatio();
			      
	      Double_t NotRes = gn_nPipPim_NR(gamma, nucleon, cch);
	      
	      double CS = Ress + NotRes;
	      
	      return ( CS < 0 ) ? 0. : CS;
	}else return 0.;
}

//______________________________________________________________________________________________________________________

Double_t gn_pPimPi0_ResPart( Dynamics* gamma, Dynamics* nucleon ){
  
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_0_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_0_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_0_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_0_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_0_ID);

	TParticlePDG *delta_0 = CPT::Instance()->ParticlePDG(CPT::p33_0_ID);
	TParticlePDG *delta_p = CPT::Instance()->ParticlePDG(CPT::p33_p_ID);
	TParticlePDG *rho_m = CPT::Instance()->ParticlePDG(CPT::rho_m_ID);
	
	double N1440_0_1 = 0., 
	      N1440_0_2 = 0., 
	      N1440_0_3 = 0.,
	      
	      N1520_0_1 = 0., 
	      N1520_0_2 = 0., 
	      N1520_0_3 = 0., 
	      N1520_0_4 = 0., 
	      
	      N1535_0_1 = 0., 
	      N1535_0_2 = 0., 
	      
	      N1680_0_1 = 0., 
	      N1680_0_2 = 0., 
	      N1680_0_3 = 0., 
	      N1680_0_4 = 0., 
	      
	      Delta1950_0_1 = 0.,
	      Delta1950_0_2 = 0.,
	      Delta1950_0_3 = 0.;
  
	//Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
  
	N1440_0_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1440_0_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1440_0_3 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(5)->BranchingRatio();
	
	N1520_0_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1520_0_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1520_0_3 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(5)->BranchingRatio();
	N1520_0_4 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(9)->BranchingRatio() * rho_m->DecayChannel(0)->BranchingRatio();
	
	N1535_0_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1535_0_2 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	
	N1680_0_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1680_0_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	N1680_0_3 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(5)->BranchingRatio() * rho_m->DecayChannel(0)->BranchingRatio();
	N1680_0_4 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(7)->BranchingRatio();
	
	Delta1950_0_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(3)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	Delta1950_0_2 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(0)->BranchingRatio();
	Delta1950_0_3 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(6)->BranchingRatio() * rho_m->DecayChannel(0)->BranchingRatio();
	
	double Ress = N1440_0_1 + N1440_0_2 + N1440_0_3 + 
		N1520_0_1 + N1520_0_2 + N1520_0_3 + N1520_0_4 + 
		N1535_0_1 + N1535_0_2 + 
		N1680_0_1 + N1680_0_2 + N1680_0_3 + N1680_0_4 +
		Delta1950_0_1 + Delta1950_0_2 + Delta1950_0_3;
	
	return Ress;
}

Double_t gn_pPimPi0_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
	Double_t ncMass = nucleon->GetMass(); //mass in MeV
	Double_t E = 0.;
	if(FermiMotion){
		E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
	}
	else{
		E = gamma->Momentum().E(); //without Fermi motion
	}

	Double_t Eth = 313.;
	if(E > Eth){
	
	      TParticlePDG *rho_m = CPT::Instance()->ParticlePDG(CPT::rho_m_ID);
	      
	      Double_t Ress = gn_pPimPi0_ResPart(gamma, nucleon) + rho_p_m_PhotonCrossSection( gamma, nucleon, cch ) * rho_m->DecayChannel(0)->BranchingRatio();      
	      Double_t NotRes = gn_pPimPi0_NR(gamma, nucleon, cch);
	      
	      double CS = Ress + NotRes;
	      
	      return ( CS < 0 ) ? 0. : CS;
	}else return 0.;
}

Double_t gp_p2Pi0_ResPart( Dynamics* gamma, Dynamics* nucleon ){
  
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_p_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_p_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_p_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_p_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_p_ID);

	TParticlePDG *delta_p = CPT::Instance()->ParticlePDG(CPT::p33_p_ID);
	
	double N1440_p_1 = 0., 
	      N1440_p_2 = 0.,
	      
	      N1520_p_1 = 0., 
	      N1520_p_2 = 0., 
	      
	      N1535_p_1 = 0.,
	      
	      N1680_p_1 = 0., 
	      N1680_p_2 = 0.,
	      
	      Delta1950_p_1 = 0.;
	      
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
	
	N1440_p_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1440_p_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(7)->BranchingRatio();
	
	N1520_p_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1520_p_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(7)->BranchingRatio();
	
	N1535_p_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	
	N1680_p_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(2)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();
	N1680_p_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(9)->BranchingRatio();
	
	Delta1950_p_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(3)->BranchingRatio() * delta_p->DecayChannel(0)->BranchingRatio();

	
	double Ress = N1440_p_1 + N1440_p_2 + 
		      N1520_p_1 + N1520_p_2 + 
		      N1535_p_1 + 
		      N1680_p_1 + N1680_p_2 +
		      Delta1950_p_1;
	
	return Ress;
}

Double_t gn_n2Pi0_ResPart( Dynamics* gamma, Dynamics* nucleon ){
  
	TParticlePDG *p11 = CPT::Instance()->ParticlePDG(CPT::p11_0_ID);
	TParticlePDG *d13 = CPT::Instance()->ParticlePDG(CPT::d13_0_ID);
	TParticlePDG *s11 = CPT::Instance()->ParticlePDG(CPT::s11_0_ID);
	TParticlePDG *f15 = CPT::Instance()->ParticlePDG(CPT::f15_0_ID);
	TParticlePDG *f37 = CPT::Instance()->ParticlePDG(CPT::f37_0_ID);

	TParticlePDG *delta_0 = CPT::Instance()->ParticlePDG(CPT::p33_0_ID);
	
	double N1440_0_1 = 0., 
	      N1440_0_2 = 0.,
	      
	      N1520_0_1 = 0., 
	      N1520_0_2 = 0., 
	      
	      N1535_0_1 = 0.,
	      
	      N1680_0_1 = 0., 
	      N1680_0_2 = 0.,
	      
	      Delta1950_0_1 = 0.;
	      
	Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
	Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
	Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
	Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
	Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
	
	N1440_0_1 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1440_0_2 = p11Formation(gamma, nucleon, p11_params) * p11->DecayChannel(7)->BranchingRatio();
	
	N1520_0_1 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1520_0_2 = d13Formation(gamma, nucleon, d13_params) * d13->DecayChannel(7)->BranchingRatio();
	
	N1535_0_1 = s11Formation(gamma, nucleon, s11_params) * s11->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	
	N1680_0_1 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(3)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();
	N1680_0_2 = f15Formation(gamma, nucleon, f15_params) * f15->DecayChannel(9)->BranchingRatio();
	
	Delta1950_0_1 = f37Formation(gamma, nucleon, f37_params) * f37->DecayChannel(4)->BranchingRatio() * delta_0->DecayChannel(1)->BranchingRatio();

	
	double Ress = N1440_0_1 + N1440_0_2 + 
		      N1520_0_1 + N1520_0_2 + 
		      N1535_0_1 + 
		      N1680_0_1 + N1680_0_2 +
		      Delta1950_0_1;
	
	return Ress;
}

///======================================================================================================================
///	END OF AUXILIARY SECTION
///======================================================================================================================


///===========================================            DIRECT  	        ===========================================
///===========================================    PHOTO-ABSORPTION CHANNELS    ===========================================

Double_t gp_pPipPim_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 10000.) return 0.;
  
		double p1 = 0.16517900,
		       p2 = -0.0311800,
		       p3 = 0.1297600,
		       p4 = 0.0013,
		       p5 = -0.04,
		       p6 = 0.036;

		Double_t Eth = 480; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 6; 
			Double_t p[n] = {p1, p2, p3, p4, p5, p6};

			Double_t x = log(E/Eth); 

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f); //microbarn
		} else return 0.;
			
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gp_nPipPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
		
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 10000.) return 0.;
  
		double p1 = 0.095179,
		       p2 = -0.02418,
		       p3 = 0.11976,
		       p4 = 0.0013,
		       p5 = -0.064,
		       p6 = 0.054;
	       
		Double_t Eth = 470; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 6;
			Double_t p[n] = {p1, p2, p3, p4, p5, p6};

			Double_t x = log(E/Eth); 

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f); //microbarn
		} else return 0.;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_nPipPim_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 60000.) return 0.;

		double p1 = 0.11143,
		       p2 = 0.171525,
		       p3 = -0.082725,
		       p4 = 0.044725,
		       p5 = -0.002,
		       p6 = 0.02;
  
		Double_t Eth = 420; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 6; 
			Double_t p[n] = {p1, p2, p3, p4, p5, p6};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_pPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 5000.) return 0.;

		double p1 = 0.004743,
		       p2 = 0.110925,
		       p3 = -0.0082725,
		       p4 = 0.052725,
		       p5 = -0.05,
		       p6 = 0.05;

		Double_t Eth = 500; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 6; 
			Double_t p[n] = {p1, p2, p3, p4, p5, p6};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gp_p2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
		
		double Total = (0.1667/0.5)*gp_pPipPim_TOTAL(gamma, nucleon, cch);	
		double Ress = gp_p2Pi0_ResPart(gamma, nucleon);
		
		double CS = Total - Ress;
		
		return CS < 0. ? 0. : CS;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_n2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		double Total = (0.1667/0.5)*gn_nPipPim_TOTAL(gamma, nucleon, cch);
		double Ress = gn_n2Pi0_ResPart(gamma, nucleon);
		
		double CS = Total - Ress;
		
		return CS < 0. ? 0. : CS;
	} else return 0.;
}

///====================================================   3 pions   =====================================================

///======================================================================================================================
///
/// 	AUXILIARY SECTION:
///   
///		- CALCULATION OF TOTAL CROSS SECTIONS FOR 3 PIONS
///
///		OBJECTIVE: CALCULATE THE NON-RESONANT CHANNELS FOR THE 3 PIONS PHOTO-PRODUCTION     
///
///======================================================================================================================


Double_t gp_pPipPimPi0_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 506; //MeV
		if(E > Eth){
		      
		      TParticlePDG *omg_0 = CPT::Instance()->ParticlePDG(CPT::omega_ID);
			      
		      Double_t NotRes = gp_pPipPimPi0_NR(gamma, nucleon, cch);
		      Double_t Res = omega_PhotonCrossSection(gamma, nucleon, cch) * omg_0->DecayChannel(0)->BranchingRatio();
			      
		      double CS =  NotRes + Res;
				
		      return CS < 0. ? 0. : CS;
		} else return 0.;
	} else return 0.;
}

///======================================================================================================================
///	END OF AUXILIARY SECTION
///======================================================================================================================


///===========================================            DIRECT  	        ===========================================
///===========================================    PHOTO-ABSORPTION CHANNELS    ===========================================

Double_t gp_pPipPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 25000.) return 0.;
  
		double p1 = 0.03379,
		       p2 = 0.14304,
		       p3 = -0.0457;
			
		Double_t Eth = 556; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 3; 
			Double_t p[n] = {p1, p2, p3};

			Double_t x = log(E/Eth); 

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
				
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}
//_______________________________________________________________________________________________________________________________

Double_t gp_p3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
		
	return (0.03333/0.6333)*gp_pPipPimPi0_TOTAL( gamma, nucleon, cch );
}

//_______________________________________________________________________________________________________________________________

Double_t gp_nPip2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
		
	return (0.1333/0.6333)*gp_pPipPimPi0_TOTAL( gamma, nucleon, cch );
}

//_______________________________________________________________________________________________________________________________

Double_t gp_n2PipPim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
		
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 14000.) return 0.;
		
		Double_t Eth = 516; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 3; 
			Double_t p[n] = {-0.11102, 0.24976, -0.10591};

			Double_t x = log(E/Eth); 

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_pPip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		Double_t Eth = 512; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2;
			Double_t p[n] = {0.07522, 0.017378};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_n3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
		
	return (0.03333/0.2)*gn_pPip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gn_nPipPimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		TParticlePDG *omg_0 = CPT::Instance()->ParticlePDG(CPT::omega_ID);
	  
		double CS_Total = (0.6333/0.2)*gn_pPip2Pim(gamma, nucleon, cch);
		double MesVec = omega_PhotonCrossSection(gamma, nucleon, cch) * omg_0->DecayChannel(0)->BranchingRatio();
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_pPim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.1333/0.2)*gn_pPip2Pim(gamma, nucleon, cch);
	 
}

///====================================================   4 pions   =====================================================

Double_t gp_p2Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
		
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		if(E >= 34000.) return 0.;
		
		Double_t Eth = 727; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 3; 
			Double_t p[n] = {-0.00098, 0.098325, -0.047548};

			Double_t x = log(E/Eth); 

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gp_p4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.03333/0.2667)*gp_p2Pip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gp_pPipPim2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_p = CPT::Instance()->ParticlePDG(CPT::kaon_p_ID);
		TParticlePDG *kaon_m = CPT::Instance()->ParticlePDG(CPT::kaon_m_ID);
		
		double CS_Total = (0.3667/0.2667)*gp_p2Pip2Pim(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(0)->BranchingRatio() * 
				kaon_p->DecayChannel(1)->BranchingRatio() * 
				kaon_m->DecayChannel(1)->BranchingRatio();
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gp_nPip3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	return (0.06667/0.2667)*gp_p2Pip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gp_n2PipPimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.2667/0.2667)*gp_p2Pip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gn_n2Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		Double_t Eth = 728; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2;
			Double_t p[n] = {0.08550, 0.02417};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_n4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.03333/0.2667)*gn_n2Pip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gn_nPipPim2Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
  
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_p = CPT::Instance()->ParticlePDG(CPT::kaon_p_ID);
		TParticlePDG *kaon_m = CPT::Instance()->ParticlePDG(CPT::kaon_m_ID);
		
		double CS_Total = (0.3667/0.2667)*gn_n2Pip2Pim(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(0)->BranchingRatio() * 
				kaon_p->DecayChannel(1)->BranchingRatio() * 
				kaon_m->DecayChannel(1)->BranchingRatio();
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//_______________________________________________________________________________________________________________________________

Double_t gn_pPim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.06667/0.2667)*gn_n2Pip2Pim(gamma, nucleon, cch);
}

//_______________________________________________________________________________________________________________________________

Double_t gn_pPip2PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){

	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){

		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
		
		Double_t Eth = 716; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2;
			Double_t p[n] = {0.09550, 0.01617};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

///====================================================   5 pions   =====================================================

///======================================================================================================================
///
/// 	AUXILIARY SECTION:
///   
///		- CALCULATION OF TOTAL CROSS SECTIONS FOR 5 PIONS
///
///		OBJECTIVE: CALCULATE THE NON-RESONANT CHANNELS FOR THE 5 PIONS PHOTO-PRODUCTION     
///
///======================================================================================================================

Double_t gp_p2Pip2PimPi0_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 952.0; //MeV
		if(E > Eth){
		      
		      TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		      TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		      TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);
			      
		      Double_t NotRes = gp_p2Pip2PimPi0_NR(gamma, nucleon, cch);
		      Double_t Res = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				      kaon_L0->DecayChannel(1)->BranchingRatio() * 
				      kaon_S0->DecayChannel(0)->BranchingRatio();
			      
		      double CS =  NotRes + Res;
				
		      return (CS < 0.) ? 0. : CS;
		} else return 0.;
	} else return 0.;
}

Double_t gn_n2Pip2PimPi0_TOTAL(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 952.0; //MeV
		if(E > Eth){
		      
		      TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		      TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		      TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);
			      
		      Double_t NotRes = gn_n2Pip2PimPi0_NR(gamma, nucleon, cch);
		      Double_t Res = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				      kaon_L0->DecayChannel(1)->BranchingRatio() * 
				      kaon_S0->DecayChannel(0)->BranchingRatio();
			      
		      double CS =  NotRes + Res;
				
		      return (CS < 0.) ? 0. : CS;
		} else return 0.;
	} else return 0.;
}

///======================================================================================================================
///	END OF AUXILIARY SECTION
///======================================================================================================================


///===========================================            DIRECT  	        ===========================================
///===========================================    PHOTO-ABSORPTION CHANNELS    ===========================================

Double_t gp_p2Pip2PimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
  
		double p1 = 0.15568, 
		       p2 = -0.03299;

		Double_t Eth = 952.0; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {p1, p2};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//____________________________________________________________________________________________________________________

Double_t gp_p5Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);

		double CS_Total = (0.004762/0.4381)*gp_p2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				kaon_L0->DecayChannel(0)->BranchingRatio() * 
				kaon_S0->DecayChannel(1)->BranchingRatio();
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//____________________________________________________________________________________________________________________

Double_t gp_pPipPim3Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);

		double CS_Total = (0.2238/0.4381)*gp_p2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				( kaon_L0->DecayChannel(0)->BranchingRatio() * kaon_S0->DecayChannel(0)->BranchingRatio() +
				  kaon_L0->DecayChannel(1)->BranchingRatio() * kaon_S0->DecayChannel(1)->BranchingRatio() );
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//____________________________________________________________________________________________________________________

Double_t gp_nPip4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.02857/0.4381)*gp_p2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
}

//____________________________________________________________________________________________________________________

Double_t gp_n2PipPim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.2095/0.4381)*gp_p2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
}

//____________________________________________________________________________________________________________________

Double_t gp_n3Pip2Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 964; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 3; 
			Double_t p[n] = {0.14433, -0.05102, -0.00067};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}


//______________________________________________________________________________________________________________________

Double_t gn_n2Pip2PimPi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}
  
		double p1 = 0.20568,
		       p2 = -0.06299;

		Double_t Eth = 952.0; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {p1, p2};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

//______________________________________________________________________________________________________________________

Double_t gn_n5Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);

		double CS_Total = (0.004762/0.4381)*gn_n2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				kaon_L0->DecayChannel(0)->BranchingRatio() * 
				kaon_S0->DecayChannel(1)->BranchingRatio();
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//______________________________________________________________________________________________________________________

Double_t gn_nPipPim3Pi0_NR(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	
		TParticlePDG *phi_0 = CPT::Instance()->ParticlePDG(CPT::phi_ID);
		TParticlePDG *kaon_L0 = CPT::Instance()->ParticlePDG(CPT::kaon_L0_ID);
		TParticlePDG *kaon_S0 = CPT::Instance()->ParticlePDG(CPT::kaon_S0_ID);

		double CS_Total = (0.2238/0.4381)*gn_n2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
		double MesVec = phi_PhotonCrossSection(gamma, nucleon, cch) * phi_0->DecayChannel(1)->BranchingRatio() * 
				( kaon_L0->DecayChannel(0)->BranchingRatio() * kaon_S0->DecayChannel(0)->BranchingRatio() +
				  kaon_L0->DecayChannel(1)->BranchingRatio() * kaon_S0->DecayChannel(1)->BranchingRatio() );
	  
		return ( CS_Total - MesVec ) < 0 ? 0. : ( CS_Total - MesVec );
	} else return 0.;
}

//______________________________________________________________________________________________________________________

Double_t gn_pPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.02857/0.4381)*gn_n2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
}

//____________________________________________________________________________________________________________________

Double_t gn_pPip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.2095/0.4381)*gn_n2Pip2PimPi0_TOTAL(gamma, nucleon, cch);
}

//____________________________________________________________________________________________________________________

Double_t gn_p2Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 964; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 3; 
			Double_t p[n] = {0.14433, -0.05102, -0.00067};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

///=====================================================  6 pions ============================================================

Double_t gp_p3Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1215; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.06190, -0.01921};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gp_p6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.00476/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}

Double_t gp_pPipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.1155/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}

Double_t gp_p2Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.4214/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}

Double_t gp_nPip5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.0119/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}

Double_t gp_n2PipPim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.1429/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}

Double_t gp_n3Pip2PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.1786/0.125)*gp_p3Pip3Pim(gamma, nucleon, cch);
}


Double_t gn_n3Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1215; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.06190, -0.01921};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gn_n6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.00476/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}

Double_t gn_nPipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.1155/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}

Double_t gn_n2Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.4214/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}

Double_t gn_pPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.0119/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}

Double_t gn_pPip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.1429/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}

Double_t gn_p2Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.1786/0.125)*gn_n3Pip3Pim(gamma, nucleon, cch);
}


///=====================================================  7 pions ============================================================

Double_t gp_p3Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1481; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.11137, -0.04094};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gp_p7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.00061/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_pPipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.05632/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_p2Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3342/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_nPip6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.004884/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_n2PipPim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.08425/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_n3Pip2Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.2015/0.2755)*gp_p3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gp_n4Pip3Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1495; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.01684, -0.00054};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}


Double_t gn_n3Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1481; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.11137, -0.04094};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gn_n7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.00061/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_nPipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.05632/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_n2Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3342/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_pPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.004884/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_pPip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.08425/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_p2Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.2015/0.2755)*gn_n3Pip3PimPi0(gamma, nucleon, cch);
}

Double_t gn_p3Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1495; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.01684, -0.00054};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

///===================================================== 8 pions ======================================================

Double_t gp_p4Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::proton_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1788; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.033678, -0.013025};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gp_p8Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.0006105/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_pPipPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.02624/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_p2Pip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.226/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_p3Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3577/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_nPip7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.003831/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_n2PipPim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.09195/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_n3Pip2Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3563/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}

Double_t gp_n4Pip3PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.2146/0.05614)*gp_p4Pip4Pim(gamma, nucleon, cch);
}


Double_t gn_n4Pip4Pim(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	if ( (gamma->PdgId() == CPT::photon_ID) && (nucleon->PdgId() == CPT::neutron_ID) ){
	  
		Double_t W = (gamma->Momentum() + nucleon->Momentum()).M(); //MeV
		Double_t ncMass = nucleon->GetMass(); //mass in MeV
		Double_t E = 0.;
		if(FermiMotion){
			E = W*W/ncMass/2. - ncMass/2; //MeV - model requires MeV for photon energy
		}
		else{
			E = gamma->Momentum().E(); //without Fermi motion
		}

		Double_t Eth = 1788; //MeV
		if(E > Eth){
			Double_t f = 0.;
			const int n = 2; 
			Double_t p[n] = {0.033678, -0.013025};

			Double_t x = log(E/Eth);

			for(int i=0; i<n; i++){
				f += p[i]*functionL(i, x);
			}
			return 1000.*TMath::Abs(f)*TMath::Abs(f);
		} else return 0.;
	} else return 0.;
}

Double_t gn_n8Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.0006105/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_nPipPim6Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.02624/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_n2Pip2Pim4Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
 
	return (0.226/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_n3Pip3Pim2Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3577/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_pPim7Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.003831/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_pPip2Pim5Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.09195/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_p2Pip3Pim3Pi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.3563/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}

Double_t gn_p3Pip4PimPi0(Dynamics* gamma, Dynamics* nucleon, Double_t* cch){
  
	return (0.2146/0.05614)*gn_n4Pip4Pim(gamma, nucleon, cch);
}
