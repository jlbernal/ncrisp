/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __photon_actions_pHH
#define __photon_actions_pHH

#include "TMath.h"
#include "TLorentzVector.h"
#include "TF1.h"
#include "TGenPhaseSpace.h"

#include "base_defs.hh"
#include "CrispParticleTable.hh"
#include "ParticleDynamics.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "resonance_mass.hh"
#include "transform_coords.hh"
#include "angular_dists.hh"
#include "relativisticKinematic.hh"
#include "VectorMeson_Diff_Cross_Sections.hh"
#include "transform_coords.hh" 


void createMeson( const ParticleDynamics& gamma, const ParticleDynamics& nucleon, std::vector<ParticleDynamics>& out_particles );
bool qd_photonAction( ParticleDynamics* gamma, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );
bool op_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool p33_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool f37_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool p11_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool s11_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool d13_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool f15_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool op_photonAction ( ParticleDynamics* gamma, Int_t& nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool rho_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );
bool rho_0_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );

/// Vector meson production actions
bool omega_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );
bool phi_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );
bool J_Psi_photonAction( ParticleDynamics* p, Int_t& nucleonIndex, NucleusDynamics* nuc, MesonsPool* mpool );

/// 2 pions production actions
bool gp_pPipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPipPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 3 pions production actions
bool gp_pPipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 4 pions production actions
bool gp_p2Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_pPipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 5 pions production actions
bool gp_p2Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_pPipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n3Pip2Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p2Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 6 pions production actions
bool gp_p3Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_pPipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p2Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n3Pip2PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

bool gn_n3Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p2Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 7 pions production actions
bool gp_p3Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_pPipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p2Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n3Pip2Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n4Pip3Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

bool gn_n3Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p2Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p3Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

/// 8 pions production actions
bool gp_p4Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p8Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_pPipPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p2Pip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_p3Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_nPip7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n2PipPim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n3Pip2Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gp_n4Pip3PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

bool gn_n4Pip4Pim_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n8Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_nPipPim6Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n2Pip2Pim4Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_n3Pip3Pim2Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPim7Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_pPip2Pim5Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p2Pip3Pim3Pi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );
bool gn_p3Pip4PimPi0_photonAction( ParticleDynamics* gamma, Int_t &nix, NucleusDynamics* nuc, MesonsPool* mpool );

#endif
