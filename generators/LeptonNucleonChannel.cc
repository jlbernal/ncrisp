/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "LeptonNucleonChannel.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(LeptonNucleonChannel);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

LeptonNucleonChannel::LeptonNucleonChannel():AbstractChannel(){

	f_type = 0;
	f_ptr = 0;
	f_call = 0;
}

//_________________________________________________________________________________________________										

LeptonNucleonChannel::LeptonNucleonChannel(const CrossSectionChannel& ch, void* act):AbstractChannel(ch){

	f_type = 1;
	f_ptr = 0;
  
	char *functionName = G__p2f2funcname(act);
  
	if ( functionName ) {
		f_call = new TMethodCall();
		f_call->InitWithPrototype(functionName, "LeptonPool*,Int_t&,Int_t&,NucleusDynamics&,Double_t&,MesonsPool*,std::vector<ParticleDynamics>*");    
	}
}

//_________________________________________________________________________________________________										

LeptonNucleonChannel::LeptonNucleonChannel(LeptonNucleonChannel& u):AbstractChannel((AbstractChannel&)u){
	((LeptonNucleonChannel&)u).Copy(*this);
}

//_________________________________________________________________________________________________										

#ifndef __CINT__															
LeptonNucleonChannel::LeptonNucleonChannel ( const char *name, Double_t (*cs)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( LeptonsPool*, Int_t&,  Int_t&, NucleusDynamics*, Double_t& ,MesonsPool*, std::vector<ParticleDynamics>&) ):AbstractChannel(CrossSectionChannel(name, cs, 0., infty, num_param)) 
{
	f_type = 2;
	f_ptr = act;
	f_call = 0; 
}
#endif

//_________________________________________________________________________________________________										

LeptonNucleonChannel::~LeptonNucleonChannel(){
	if (f_call) 
		delete f_call;
}

//_________________________________________________________________________________________________		



bool LeptonNucleonChannel::DoAction( ParticleDynamics*, Int_t, NucleusDynamics*, MesonsPool*, Double_t){
	return false;
}



//_________________________________________________________________________________________________										
  
bool LeptonNucleonChannel::DoAction( Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool, std::vector<ParticleDynamics>* v){
	return false;
}



//_________________________________________________________________________________________________

bool LeptonNucleonChannel::DoAction(LeptonsPool *lpool, Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool *mpool, std::vector<ParticleDynamics>* v ){
  if (f_type == 0) 
		return false;
	if (f_type == 2) {
	  return (*f_ptr)(lpool, idx1, idx2, nuc, t, mpool, *v); 
	  
	}
	if (f_type == 1) {
	  Int_t 	i1 = idx1, 
		i2 = idx2;
		
		Long_t args[7];
		Long_t answ;
    
		args[0] = (Long_t)lpool;
		args[1] = (Long_t)&i1;
		args[2] = (Long_t)&i2;
		args[3] = (Long_t)nuc;   
		args[4] = (Long_t)&t;
		args[5] = (Long_t)mpool;
		args[6] = (Long_t)v;

		f_call->SetParamPtrs(args);
		f_call->Execute(answ);
		
		return ((Bool_t)answ);

	}    
	return false;
}

//_________________________________________________________________________________________________

void LeptonNucleonChannel::Copy(TObject& obj) const{

	// AbstractChannel::Copy(obj);
	((LeptonNucleonChannel&)obj).f_type = f_type;
	((LeptonNucleonChannel&)obj).f_ptr = f_ptr;
  
	if (f_call){
		TMethodCall* m = new TMethodCall();
		m->InitWithPrototype(f_call->GetMethodName(), f_call->GetProto());    
		((LeptonNucleonChannel&)obj).f_call = m;
	}  
}

//_________________________________________________________________________________________________										


