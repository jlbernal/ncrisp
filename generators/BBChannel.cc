/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BBChannel.hh"
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(BBChannel);
#endif // CRISP_SKIP_ROOTDICT
BBChannel::BBChannel():TObject(), cs(){

	f_type = 0;
	f_ptr = 0;
	f_call = 0;
	counts =  0;
	blocked = 0;    
}
BBChannel::BBChannel( const CrossSectionChannel& ch, void* act ):TObject(), cs(ch){
	f_type = 1;
	f_ptr = 0;  
	// ROOT Low level function pointer helper ... 
	char *funcname = G__p2f2funcname(act);
	if ( funcname ) { 
		// OK ... just pass the prototype of the function to f_call object.
		f_call = new TMethodCall();
		f_call->InitWithPrototype ( funcname, "Int_t&,Int_t&,Double_t&,NucleusDynamics&,std::vector<ParticleDynamics>&");
	}
  
	counts = 0;
	blocked = 0;
}

#ifndef __CINT__
BBChannel::BBChannel( const char* name, Double_t (*csec)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( Int_t&, Int_t&, Double_t&, NucleusDynamics&, std::vector<ParticleDynamics>&) ):TObject(), cs(name, csec, num_param){

	f_type = 2;
	f_ptr = act;
	f_call = 0;
	counts = 0;
	blocked = 0;
}
#endif

BBChannel::BBChannel(BBChannel& u):TObject(){
	((BBChannel&)u).Copy(*this);
}
//_________________________________________________________________________________________________										
BBChannel::~BBChannel(){ 

	if ( f_call ) 
		delete f_call; 
}
//_________________________________________________________________________________________________										
bool BBChannel::DoAction( Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t &t, MesonsPool *mpool , std::vector<ParticleDynamics>* v ){
	if (f_type == 0) 
		return false;
	if (f_type == 1) {
		Long_t args[5];
		Long_t answ;
		Int_t i1 = idx1, i2 = idx2;
		args[0] = (Long_t)&i1;
		args[1] = (Long_t)&i2;
		args[2] = (Long_t)&t;
		args[3] = (Long_t)nuc;   
		args[4] = (Long_t)v;
		f_call->SetParamPtrs(args);
		f_call->Execute(answ);
		return ((Bool_t)answ);
	}
	if (f_type == 2) {
		return ((*f_ptr)( idx1, idx2, t, *nuc, *v));
	}    
	return false;
}
//_________________________________________________________________________________________________										
void BBChannel::Copy(TObject& obj) const {
	((BBChannel&)obj).f_type = f_type;
	cs.Copy( ((BBChannel&)obj).CrossSection() );
	((BBChannel&)obj).f_ptr = f_ptr;
	if (f_call){
		TMethodCall* m = new TMethodCall();
		m->InitWithPrototype(f_call->GetMethodName(), f_call->GetProto());    
		((BBChannel&)obj).f_call = m;
	}
	((BBChannel&)obj).counts = counts;
	((BBChannel&)obj).blocked = blocked;  
}
