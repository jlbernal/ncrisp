/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __Meson_n_actions_help_pHH
#define __Meson_n_actions_help_pHH

#include "CrispParticleTable.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "photon_actions.hh"
#include "VectorMeson_Diff_Cross_Sections.hh"


//
//_________________________________________________________________________________________________										
// Deg1:Np_No:No_Np:No_Nr:Nr_No:No_els:No_N2p:Np_No1:Np_Nh:Np_Nr:No_Np1:Nh_Np:Nr_Np

class  MyFunctionObject {
 public:
   // use constructor to customize your function object
   double operator() (double *x, double *p) {
      // function implementation using class data members
   }
};
#endif
