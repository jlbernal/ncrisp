/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "MesonNucleonChannel.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(MesonNucleonChannel);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

MesonNucleonChannel::MesonNucleonChannel():AbstractChannel(){

	f_type = 0;
	f_ptr = 0;
	f_call = 0;
}

//_________________________________________________________________________________________________										

MesonNucleonChannel::MesonNucleonChannel(const CrossSectionChannel& ch, void* act):AbstractChannel(ch){

	f_type = 1;
	f_ptr = 0;
  
	char *functionName = G__p2f2funcname(act);
  
	if ( functionName ) {
		f_call = new TMethodCall();
		f_call->InitWithPrototype(functionName, "Int_t&,MesonsPool&,Int_t&,NucleusDynamics&,Double_t&,std::vector<ParticleDynamics>*");    
	}
}

//_________________________________________________________________________________________________										

MesonNucleonChannel::MesonNucleonChannel(MesonNucleonChannel& u):AbstractChannel((AbstractChannel&)u){
	((MesonNucleonChannel&)u).Copy(*this);
}

//_________________________________________________________________________________________________										

#ifndef __CINT__
MesonNucleonChannel::MesonNucleonChannel ( const char *name, Double_t (*cs)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( Int_t&, MesonsPool*, Int_t&, NucleusDynamics*, Double_t& , std::vector<ParticleDynamics>&) ):AbstractChannel(CrossSectionChannel(name, cs, 0., infty, num_param)) {
	f_type = 2;
	f_ptr = act;
	f_call = 0; 
}
#endif

//_________________________________________________________________________________________________										

MesonNucleonChannel::~MesonNucleonChannel(){
	if (f_call) 
		delete f_call;
}

//_________________________________________________________________________________________________										

bool MesonNucleonChannel::DoAction( ParticleDynamics*, Int_t, NucleusDynamics*, MesonsPool*, Double_t){
	return false;
}

//_________________________________________________________________________________________________										
  
bool MesonNucleonChannel::DoAction( Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool, std::vector<ParticleDynamics>* v){
	if (f_type == 0) 
		return false;
	if (f_type == 1) {

		Int_t 	i1 = idx1, 
				i2 = idx2;
		
		Long_t args[6];
		Long_t answ;
    
		args[0] = (Long_t)&i1;
		args[1] = (Long_t)mpool;
		args[2] = (Long_t)&i2;
		args[3] = (Long_t)nuc;   
		args[4] = (Long_t)&t;
		args[5] = (Long_t)v;

		f_call->SetParamPtrs(args,6);
		f_call->Execute(answ);
    
		return ((Bool_t)answ);
	}
	if (f_type == 2) {
		return (*f_ptr)(idx1, mpool, idx2, nuc, t, *v);
	}    
	return false;
}

//_________________________________________________________________________________________________	
bool MesonNucleonChannel::DoAction( LeptonsPool* lpool, Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool ,std::vector<ParticleDynamics>* v  ){
	return false;
}
//_________________________________________________________________________________________________

void MesonNucleonChannel::Copy(TObject& obj) const{

	// AbstractChannel::Copy(obj);
	((MesonNucleonChannel&)obj).f_type = f_type;
	((MesonNucleonChannel&)obj).f_ptr = f_ptr;
  
	if (f_call){
		TMethodCall* m = new TMethodCall();
		m->InitWithPrototype(f_call->GetMethodName(), f_call->GetProto());    
		((MesonNucleonChannel&)obj).f_call = m;
	}  
}

//_________________________________________________________________________________________________										
