/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __NeutrinoEventGen_pHH
#define __NeutrinoEventGen_pHH

#include "TMath.h"
#include "TObjArray.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "EventGen.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "LeptonNucleonChannel.hh"
#include "CrossSectionData.hh"
#include "time_collision.hh"

class NeutrinoEventGen: public EventGen{
private :
	TObjArray channels;
	Double_t tcs;
public:
	NeutrinoEventGen();
	virtual ~NeutrinoEventGen();
	TObjArray& GetChannels() { return channels; }
	Double_t TotalCrossSection( Dynamics& p1, Dynamics& p2);
	Int_t Generate(Double_t TKinetic, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par,  char* opt = 0, std::ostream *os = 0);  
	Int_t Generate(Double_t TKinetic, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os);

	static TVector3 StartPosition( NucleusDynamics& nuc);
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(NeutrinoEventGen, 0);
#endif // CRISP_SKIP_ROOTDICT
};
#endif
