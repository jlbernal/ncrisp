/* ================================================================================
 * 
 * 	Copyright 2013 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __DeuteronEventGen_pHH
#define __DeuteronEventGen_pHH

#include "TMath.h"
#include "TObjArray.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "EventGen.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "BBChannel.hh"         
#include "CrossSectionData.hh"
#include "time_collision.hh"

#include "DataHelper.hh"

///
//_________________________________________________________________________________________________										

class DeuteronEventGen: public EventGen{

private :
	TObjArray channels;
	Double_t tcs;
public:
  
	DeuteronEventGen();

	virtual ~DeuteronEventGen();

	TObjArray& GetChannels() { return channels; }

	Double_t TotalCrossSection( Dynamics& p1, Dynamics& p2);

	Int_t Generate(Double_t DeutEnergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par,  char* opt = 0, std::ostream *os = 0);  

	Int_t GenerateSingleNucleon(Int_t ID, Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, TVector3& vecPos/*, TVector3& angMomentum*/);
	
	Int_t Generate(Double_t DeutEnergy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os);
	
	static TVector3 StartPosition( NucleusDynamics& nuc);
	
	static TVector3 StartPosition2(NucleusDynamics& nuc, TVector3 v1);
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(DeuteronEventGen, 0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
