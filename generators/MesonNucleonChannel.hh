/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __MesonNucleonChannel_pHH
#define __MesonNucleonChannel_pHH

#include "AbstractChannel.hh"
#include "TObject.h"
#include "TMethodCall.h"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"

///
//_________________________________________________________________________________________________										

class MesonNucleonChannel: public AbstractChannel{

private:
	
	TMethodCall* f_call;
	Int_t f_type;  

	///
	//_________________________________________________________________________________________________										

	bool (*f_ptr)( Int_t&, MesonsPool*, Int_t&, NucleusDynamics*, Double_t& , std::vector<ParticleDynamics>&);
	
public:
  
	///
	//_________________________________________________________________________________________________										

	MesonNucleonChannel();
	
	///
	//_________________________________________________________________________________________________										

	MesonNucleonChannel(const CrossSectionChannel& ch, void* act);
  
	///
	//_________________________________________________________________________________________________										

	MesonNucleonChannel(MesonNucleonChannel& u);

#ifndef __CINT__
	MesonNucleonChannel ( const char *name, Double_t (*cs)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( Int_t&, MesonsPool*, Int_t&, NucleusDynamics*, Double_t& , std::vector<ParticleDynamics>&) );
#endif

	///
	//_________________________________________________________________________________________________										

	virtual ~MesonNucleonChannel();
  
	///
	//_________________________________________________________________________________________________										

	bool DoAction( ParticleDynamics* p, Int_t idx, NucleusDynamics* nuc, MesonsPool* mpool, Double_t t); 
  
	///
	//_________________________________________________________________________________________________										

	bool DoAction( Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool = 0, std::vector<ParticleDynamics>* v = 0);

	///
	//_________________________________________________________________________________________________		
	bool DoAction( LeptonsPool* lpool, Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool = 0 ,std::vector<ParticleDynamics>* v = 0 );


	void Copy(TObject& obj) const;
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(MesonNucleonChannel, 0);  
#endif // CRISP_SKIP_ROOTDICT
  
};


#endif
