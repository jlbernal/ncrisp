/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __Meson_n_cross_sections_pHH
#define __Meson_n_cross_sections_pHH

#include "CrispParticleTable.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "base_defs.hh"
#include "BasicException.hh"

//
//_________________________________________________________________________________________________										
// Deg1:Np_No:No_Np:No_Nr:Nr_No:No_els:No_N2p:Np_No1:Np_Nh:Np_Nr:No_Np1:Nh_Np:Nr_Np

Double_t rho_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t omega_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t phi_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t JPsi_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t JPsi_elastic_cross_sectionVDM(Dynamics* mes, Dynamics* nucleon, Double_t* cch);


Double_t pion_n_f15_formation( Dynamics* pion, Dynamics* nucleon, Double_t* cch );
Double_t pion_n_p33_formation ( Dynamics* pion, Dynamics* nucleon, Double_t* cch );
Double_t piN_Delta1232(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_Delta1700(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_Delta1950(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_N1440(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_N1520(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_N1535(Dynamics* pion, Dynamics* nucleon, Double_t* cch);
Double_t piN_N1680(Dynamics* pion, Dynamics* nucleon, Double_t* cch);

Double_t PionN_OmegaN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t OmegaN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t OmegaN_RhoN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t RhoN_OmegaN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t OmegaN_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t OmegaN_2PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t PionN_OmegaN1_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t PionN_PhiN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t PionN_RhoN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t OmegaN_PionN1_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t PhiN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t RhoN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t RhoN_mPionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);

Double_t NJPsi_ND_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t NJPsi_NDast_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);
Double_t NJPsi_NDDbar_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch);


#endif
