/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

/** @file */


#ifndef __Lepton_n_actions_pHH
#define __Lepton_n_actions_pHH

#include "CrispParticleTable.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"
#include "resonance_mass.hh"
#include "TGenPhaseSpace.h"
#include "transform_coords.hh" 
#include "TLorentzVector.h"
#include "TF1.h"
#include "VectorMeson_Diff_Cross_Sections.hh"
#include "relativisticKinematic.hh"





//_________________________________________________________________________________________________										



/**
 * @brief Made the interaction and create the child particles
 *
 * @param lep lepton 
 * @param nucleon interacting nuclei 
 * @param out_particles vector with child particles already created, the properties will be changed inside funtions and return here.  
 * @param s center of mass energy
 * @return void 
 **/

void Create_particles(const ParticleDynamics& lep, const ParticleDynamics& nucleon, std::vector<ParticleDynamics>& out_particles, double s);

//_________________________________________________________________________________________________										



    /**
     * @brief Neutrino charged current quasi-elastic scattering  
     * 
     *  \f$ \nu_\mu + n \rightarrow	\mu^- + p  \f$
     *  
     * @param lpool leptons container
     * @param key position of the lepton in container 
     * @param idx position of the nucleon in the nuclei
     * @param nuc nuclei 
     * @param t ...
     * @param mpool mesons container so far  for compatibility, but can be use in future
     * @param v particle vectors 
     * @return bool true if reactions was sucessfull, false if blocked.
     **/
   bool nun_mump_ccqe_action( LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v); 	

   
//_________________________________________________________________________________________________										

    /**
     * @brief Neutrino neutral current elastic scattering  
     * \n
     *  \f$ \nu_\mu + n \rightarrow	\nu_\mu + n  \f$
     * \n
     * or 
     * \n
     *  \f$ \nu_\mu + p \rightarrow	\nu_\mu + p  \f$
     * 
     * @param lpool leptons container
     * @param key position of the lepton in container 
     * @param idx position of the nucleon in the nuclei
     * @param nuc nuclei 
     * @param t ...
     * @param mpool mesosn container so far  for compatibility, but can be use in future
     * @param v particle vectors
     * @return bool true if reactions was sucessfull, false if blocked.
     **/   
   bool nuN_qelastic_NCe_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v);


//_________________________________________________________________________________________________										
   
    /**
     * @brief Neutrino charged current Resonance production  
     * 
     *  \f$ \nu_\mu + n \rightarrow \mu^- + \Delta^+  \f$
     * \n
     * or 
     * \n
     *  \f$ \nu_\mu + p \rightarrow \mu^- + \Delta^++  \f$
     * 
     * @param lpool leptons container
     * @param key position of the lepton in container 
     * @param idx position of the nucleon in the nuclei
     * @param nuc nuclei 
     * @param t ...
     * @param mpool mesosn container so far  for compatibility, but can be use in future
     * @param v particle vectors
     * @return bool true if reactions was sucessfull, false if blocked.
     **/      
   bool nuN_res1_CCres_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v);


   
//_________________________________________________________________________________________________										
   
    /**
     * @brief Neutrino neutral current Resonance production  
     * 
     *  \f$ \nu_\mu + n \rightarrow \nu_\mu + \Delta^0  \f$
     * \n
     * or 
     * \n
     *  \f$ \nu_\mu + p \rightarrow \nu_\mu + \Delta^+  \f$
     * 
     * @param lpool leptons container
     * @param key position of the lepton in container 
     * @param idx position of the nucleon in the nuclei
     * @param nuc nuclei 
     * @param t ...
     * @param mpool mesosn container so far  for compatibility, but can be use in future
     * @param v particle vectors
     * @return bool true if reactions was sucessfull, false if blocked.
     **/      
   bool nuN_res2_NCres_action(LeptonsPool* lpool,Int_t &key, Int_t &idx, NucleusDynamics* nuc, Double_t &t , MesonsPool* mpool, std::vector<ParticleDynamics>& v);



  
#endif
