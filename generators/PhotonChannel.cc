/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "PhotonChannel.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(PhotonChannel);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

PhotonChannel::PhotonChannel():TObject(), cs(){

	f_type = 0;
	f_ptr = 0;
	f_call = 0;
	counts =  0;
	blocked = 0;    
}

//_________________________________________________________________________________________________										

PhotonChannel::PhotonChannel( const CrossSectionChannel& ch, void* act ):TObject(), cs(ch){

	f_type = 1;
	f_ptr = 0;  
	char *funcname = G__p2f2funcname(act);
  
	if ( funcname ) { 
		f_call = new TMethodCall();
		f_call->InitWithPrototype ( funcname, "ParticleDynamics*,Int_t&,NucleusDynamics*,MesonsPool*");
	}
	
	counts = 0;
	blocked = 0;
}

//_________________________________________________________________________________________________										

#ifndef __CINT__

PhotonChannel::PhotonChannel( const char* name, Double_t (*csec)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( ParticleDynamics*, Int_t&, NucleusDynamics*, MesonsPool*) ):TObject(), cs(name, csec, 0., infty, num_param){
  
	f_type = 2;
	f_ptr = act;
	f_call = 0;
	counts = 0;
	blocked = 0;
}

#endif

//_________________________________________________________________________________________________										

PhotonChannel::PhotonChannel(PhotonChannel& u):TObject(){

	((PhotonChannel&)u).Copy(*this);
}

//_________________________________________________________________________________________________										

PhotonChannel::~PhotonChannel(){
 
	if ( f_call ) 
		delete f_call; 
}

//_________________________________________________________________________________________________										

bool PhotonChannel::DoAction( ParticleDynamics* gamma, Int_t idx, NucleusDynamics* nuc, MesonsPool* mpool){
  
	if (f_type == 0) 
		return false;
	
	if (f_type == 1) {

		Int_t i = idx;
		Long_t args[4];
		Long_t answ;
		args[0] = (Long_t)gamma;
		args[1] = (Long_t)&i;
		args[2] = (Long_t)nuc;
		args[3] = (Long_t)mpool;   

		f_call->SetParamPtrs(args);
		f_call->Execute(answ);
		return ((Bool_t)answ);
	}
	if (f_type == 2) {
		return (*f_ptr)(gamma, idx, nuc, mpool);
	}    
	return false;
}

//_________________________________________________________________________________________________										

void PhotonChannel::Copy(TObject& obj)const{

	((PhotonChannel&)obj).f_type = f_type;
	cs.Copy( ((PhotonChannel&)obj).CrossSection() );
	((PhotonChannel&)obj).f_ptr = f_ptr;
	
	if (f_call){
		TMethodCall* m = new TMethodCall();
		m->InitWithPrototype(f_call->GetMethodName(), f_call->GetProto());    
		((PhotonChannel&)obj).f_call = m;
	}
	
	((PhotonChannel&)obj).counts = counts;
	((PhotonChannel&)obj).blocked = blocked;  
}

//_________________________________________________________________________________________________										
