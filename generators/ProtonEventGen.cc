/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "ProtonEventGen.hh"

#include <fstream>
#include <iostream>

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(ProtonEventGen);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

ProtonEventGen::ProtonEventGen(): EventGen(), channels(){
	
	channels.SetOwner();
	tcs = 0.;
}

//_________________________________________________________________________________________________										

ProtonEventGen::~ProtonEventGen(){

	channels.Delete();
}

//_________________________________________________________________________________________________										

Double_t ProtonEventGen::TotalCrossSection(Dynamics& p1, Dynamics& p2){

	Double_t tcs1 = 0.;
  
	for ( Int_t i = 0; i < channels.GetEntriesFast(); i++ ) {
		tcs1 += ((BBChannel*)channels[i])->CrossSection().Calculate( &p1, &p2);
	}
	return tcs1;
}

//_________________________________________________________________________________________________										

Int_t ProtonEventGen::Generate(Double_t TKinetic, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char* opt, std::ostream *os) {
	
	// Initializing particle ...
	TVector3 temp_pos;
	
        	
	ParticleDynamics proton(CPT::proton_ID);
	proton.SetMass( proton.GetInvariantMass() * CPT::effn );
	proton.BindIt(true);
	Double_t v0 = nuc.NuclearPotentialWell(CPT::proton_ID);
	Double_t p = TMath::Sqrt( sqr( TKinetic + v0 + proton.GetMass() ) - sqr(proton.GetMass()) );
	proton.SetMomentum( TLorentzVector( p, 0., 0., TKinetic + v0 + proton.GetMass() ) ); //the beam axis is X
	proton.SetPosition( StartPosition(nuc) );
	Time_order(proton,nuc);
	double tsrf=time_surface(proton,nuc);
	//cout<<"Time surface "<<tsrf<<endl;
	// put the proton into the nucleus.
 	//position_order(nuc);
	
	//aqui llama la funcion   RAMON
	//distance_order(nuc,proton.Position());
	
	Int_t n_add = nuc.CaptureNucleon(proton);
	// Calculate the total cross section for all nucleons, and select
	// the interaction channel.
	for ( Int_t i = 0; i < nuc.GetA() - 1; i++ ) {
		Double_t tcs = TotalCrossSection(proton, nuc[i]);
		Double_t b2 = impact_parameter2(proton, nuc[i]);
		if(time_collision(proton,nuc[i],tcs)>tsrf) continue;
		//cout<<"time collision "<<i<<" "<<time_collision(proton,nuc[i],tcs)<<" tcs "<<tcs<<endl;
		if ( TMath::Pi() * b2 <= tcs* .1 ) {
			Double_t sum = 0.;
			Double_t x = gRandom->Uniform();
			for ( Int_t j = 0; j < channels.GetEntriesFast(); j++ ) { 	
				sum += ((BBChannel*)channels[j])->CrossSection().Value() / tcs;
				if ( x < sum ) {
					temp_pos =  nuc[i].Position();
					std::vector<ParticleDynamics> v;
					Double_t t = 0.;
					if (((BBChannel*)channels[j])->DoAction( i, n_add, &nuc, t, &mpool, &v)){
						nuc.SetMomentum(nuc.Momentum() + proton.Momentum());
						nuc.SetAngularMomentum(nuc.AngularMomentum() + proton.Position().Cross(proton.Momentum().Vect()));
						std::cout << "Channel: " << ((BBChannel*)channels[j])->CrossSection().GetName() << std::endl;
						nuc[n_add].SetPosition(temp_pos); 
						
						//estas tres lineas son para la grafica RAMON                                     	
// 						ofstream position("position.txt",ios::app);
// 						position << nuc[i].Position().X() << "\t" << nuc[i].Position().Y() << "\t" << nuc[i].Position().Z() << endl;
// 						position.close();
						return j;
					}
				}	
			}
		}    
	} // First 'for' closing brace.*/
	return -1;
}

void ProtonEventGen::Time_order(ParticleDynamics &p,NucleusDynamics& nuc){
  
  int A=nuc.GetA();
  int B=A-1;
  double t1,t2,cs1,cs2;
  
  for (int i=0;i<B;i++)
    for (int j=i+1;j<A;j++){
      cs1=TotalCrossSection(p, nuc[i]);
      cs2=TotalCrossSection(p, nuc[j]);
      t1 = time_collision(p,nuc[i],cs1);
      t2 = time_collision(p,nuc[j],cs2);
      if (t1>t2) {std::swap(nuc[i], nuc[j]);}
    }  
}

//_________________________________________________________________________________________________


Int_t ProtonEventGen::Generate(	Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  return -1;
}


//_________________________________________________________________________________________________

TVector3 ProtonEventGen::StartPosition(NucleusDynamics& nuc){

	Double_t r = pow(gRandom->Uniform(),0.5) * nuc.GetRadium();
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z);
	Double_t x = - TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	
	return TVector3(x, y, z);
}


void ProtonEventGen::position_order (NucleusDynamics& ion){

  ParticleDynamics c;
  
  for(int i=0;i<ion.GetA()-1;i++){
    for(int j=i+1; j<ion.GetA();j++){
      if(ion[i].Position().X()>ion[j].Position().X()){
	std::swap(ion[i], ion[j]);
      }
    }
  }
}


//aqui empieza la funcion para elegir la menor distancia   RAMON
void ProtonEventGen::distance_order (NucleusDynamics& ion, TVector3 position){

  ParticleDynamics c;
  double d1,d2;
  
  for(int i=0;i<ion.GetA()-1;i++){
    for(int j=i+1; j<ion.GetA();j++){
      
        d1=(position-ion[i].Position()).Mag();
	d2=(position-ion[j].Position()).Mag();
        if(d1>d2){
	  std::swap(ion[i], ion[j]);
        }
      
     
    }
  }
}
//_________________________________________________________________________________________________										

