/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __angular_dists_pHH
#define __angular_dists_pHH

#include "TRandom.h"
#include "base_defs.hh"
#include "ParticleDynamics.hh"

/// One pion creation angular distribution.
/// return cos(theta) where theta is the azimutal angle
/// @param energy in GeV
//_________________________________________________________________________________________________										

Double_t one_pion_angular_dist( Double_t energy, const ParticleDynamics& nucleon, const ParticleDynamics& pion );

/// return cos(theta) where theta is the azimutal angle
//_________________________________________________________________________________________________										

Double_t resonance_angular_dist( Double_t energy );

#endif
