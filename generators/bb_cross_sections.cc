/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "bb_cross_sections.hh"

//_________________________________________________________________________________________________										
// Kikuchi-Kawai Cross Section.

double bb_elastic_cross_section( Dynamics* q1 , Dynamics* q2, double* ){
	const double pbeam_threshold = .800; 
	TLorentzVector W = q1->Momentum() + q2->Momentum();
	double s = W.M2();
	double m1 = q1->GetMass(), m2 = q2->GetMass();
//	 std::cout << "m1 = " << m1 << "m2 = " << m2 << std::endl;
	int 	id1 = q1->PdgId(), id2 = q2->PdgId();   
	if( CPT::IsNucleon(id1) && CPT::IsNucleon(id2) ) {
		// init of Kikuchi-Kawai Cross Section
		double 	s_nn = 0., s_np = 0.;
		// Kinetic energy in the lab frame.
		double T = (s - sqr(m1 + m2))/(2. * m1);
		if ( fabs(T) < 1.E-7 )
			return 0.;
		if ( T < 0 ) {      
			TString msg = Form("Error in elasticCrossSection(), (T < 0 )\n T = %f\n", T);            
			msg += q1->ToString();
			msg += q2->ToString();
			throw( BasicException(msg));
		}
		// double beta = sqrt(T * (T + 2 * m1)) / sqrt(T * (T + 2* m1) + sqr(m1));
		double beta = sqrt(T * (T + 2 * m1)) / (T + m1);
		s_nn = (10.63 / sqr(beta)) - (29.92 / beta) + 42.9;
		s_np = (34.10 / sqr(beta)) - (82.20 / beta) + 82.2;
//		std::cout << "salio del primer if \n"; 
		return (id1 == id2)? s_nn: s_np;
	} 
	else {  
		const double tot[31] = { 0., 
									800.0, 113.0, 54.50, 33.00, 25.50, 23.00,
									22.00, 22.50, 24.50, 27.50, 32.55, 38.00, 
									42.50, 46.00, 47.00, 47.70, 47.50, 47.50, 
									47.50, 47.00, 47.00, 47.00, 46.50, 46.20, 
									46.00, 45.50, 45.00, 44.50, 44.50, 44.00 };

		const double pbeam[31] = { 0.,				  
									800.0, 113.0, 54.50, 33.00, 25.50, 23.00,
									22.00, 22.50, 23.00, 24.00, 25.00, 25.50,
									26.00, 26.80, 27.00, 27.00, 26.20, 25.50,
									24.50, 23.00, 22.50, 21.50, 21.00, 20.50,
									20.30, 19.50, 19.50, 19.00, 18.50, 18.00 };
  
		const double elast[31] = { 0., 
									0.10, 0.20, 0.30, 0.40, 0.50, 0.60,
									0.70, 0.80, 0.90, 1.00, 1.10, 1.20,
									1.30, 1.40, 1.50, 1.60, 1.70, 1.80,
									1.90, 2.00, 2.10, 2.20, 2.30, 2.40,
									2.50, 2.60, 2.70, 2.80, 2.90, 3.00 };  
      
		//    std::cout << ((ParticleDynamics*)q1)->name() 
		//      << " m = " << q1->Momentum()
		//      << ", " << ((ParticleDynamics*)q2)->name() 
		//      << " m = " << q2->Momentum()
		//      << std::endl;
		double sqr2m = sqr(m1 + m2);
		//std::cout << "s = " << s 
		//	      << " sqr2m = " << sqr2m << std::endl;

		// Sorry, i don't know what is xx ...
		// it's in (MeV/c)**2
		double xx = s * (s - sqr2m) / sqr2m ;  
		if ( xx < 0. ) 
			return 0.;
		double pp = sqrt(xx) * .001;   // pp is in units of GeV/c
		// Which region of momentum are we in ?   
		int ip = (int)(10. * (pp - .10) + 1.10); 
//		std::cout << "ippppppp = " << ip << std::endl;
		if (ip > 1) {
			if (ip >= 30){
//				std::cout << "salio del 1 if \n"; 
				return ( 7.0 + 33.0/pp);
			}
			if (pp > pbeam_threshold){
//				cout << "salio del 2 if \n"; 
				return (elast[ip] + (pp - pbeam[ip]) * 10.0 * (elast[ip+1] - elast[ip]));      
			}
		}
		if( ip < 0 )// ip = 0; //std::cout << "Erro!\n";
		{
			TString msg = Form("Error in elasticCrossSection(), (ip < 0 ) ip == %i\n", ip);   //std::stringstream ss; ss <<;//         
//			msg += q1->ToString();
//			msg += q2->ToString();
			throw( BasicException(msg));
		}
//		cout << "salio del 3 if \n"; 
		return ( (tot[ip] + (pp - pbeam[ip]) * 10. * (tot[ip + 1] - tot[ip])) / pp );
	} // closing brace of first else ...  
}

//_________________________________________________________________________________________________										
// reference to the inelastic NN cross-sections below 
// B.J. VerWest and R.A. Arndt, "NN single pion production cross sections
// below 1500 MeV", Phys. Rev. C 25 1979(1982).

 double delta_10_11_CrossSection( double s, double alpha, double beta, double gamma, double mass){
 
	const double epsilon = 1.E-6;
  
	// hbarc = 197.32705e-12 MeV*mm
	const double c1 = TMath::Pi() * sqr(197.32705)/2.;
	const double empi0 = 139.0;

	double emnmpi2 = sqr(CPT::p_mass - empi0);
	double emnppi2 = sqr(CPT::p_mass + empi0);
  
	// note: CPT::p_mass is the proton mass... defined in defs.hh
	double s0 = sqr( CPT::p_mass + mass );
	double p02 = .250 * s0 - sqr(CPT::p_mass);
  
	double q02 = ( sqr(mass) - emnmpi2) * ( sqr(mass) - emnppi2)/(4. * sqr(mass));

	if ( q02 <= 0. ) 
		return 0.;

	double zminus = (CPT::p_mass + empi0 - 1220.) * 2./120.;
  
	// zplus = (sqrts_ds - emn - 1220.0) * 2.0/120.0 ;
	double zplus  = (sqrt(s) - CPT::p_mass - 1220.) * 2./120.;

	double diff_atan = atan(zplus) - atan(zminus);
	double rlog = log ( (1. + sqr(zplus)) / (1. + sqr(zminus)) ); 

	if ( diff_atan < epsilon ) 
		return 0.;
  
	double aver_mass = 1220. + ( 1./( diff_atan) ) * 30. * rlog;
	double sstar = sqr(aver_mass);
	double pr2 = (s - sqr(CPT::p_mass - aver_mass)) * (s - sqr(CPT::p_mass + aver_mass))/(4. * s);
  
	double q2 = ( sstar - emnmpi2) * ( sstar - emnppi2)/(4. * sstar );
  
	if ( (pr2 < 0.) || (q2 < 0.) ) 
		return 0.;
  
	double p2 = s/4. - sqr(CPT::p_mass);
  
	double a1 = alpha * pow ( sqrt(pr2)/sqrt(p02), beta);
	double a2 = sqr(mass) * sqr(gamma) * pow( sqrt(q2)/sqrt(q02), 3.);
	double a3 = p2 *  (sqr( sstar - sqr(mass)) + sqr(mass) * sqr(gamma));
	double sigma = (10. * c1 * a1 * a2) / a3;

	return sigma;  
}

//_________________________________________________________________________________________________										

double __pionExchangeCrossSection(double s){

	const double alpha_11 = 3.7720;
	const double beta_11  = 1.2620;
	const double gamma_11 = 99.020;
	const double em0_11   = 1188.;
  
	const double alpha_10 = 15.280; 
	const double beta_10  = 0.;
	const double gamma_10 = 137.4;
	const double em0_10   = 1245.;  
  
	double sigma_10 = delta_10_11_CrossSection( s, alpha_10, beta_10, gamma_10, em0_10);
  
	double sigma_11 = delta_10_11_CrossSection( s, alpha_11, beta_11, gamma_11, em0_11 );    
  
	return ( sigma_10 + 2. * sigma_11 );
}

//_________________________________________________________________________________________________										

double bb_one_pion_cross_section( Dynamics* p1, Dynamics* p2, double* ){

	int 	id1 = p1->PdgId(), 
			id2 = p2->PdgId();      
  
	double s = (p1->Momentum() + p2->Momentum()).M2();
  
	if ( sqrt(s) < 2000. ) 
		return 0.;
  
	if ( CPT::IsNucleon(id1) && CPT::IsNucleon(id2) ) 
		return __pionExchangeCrossSection(s);
  
	int n_id = ( CPT::IsNucleon(id1) ) ? id1 : id2;
	int d_id = ( CPT::IsP33(id1) ) ? id1 : id2;
  
	// (n Delta0)   or (p Delta-) -> n n
	// (n Delta+)   or (p Delta0) -> n p
	// (n Delta_pp) or (p Delta+) -> p p

	bool n_delta = ( (d_id == CPT::p33_0_ID) || (d_id == CPT::p33_p_ID) || (d_id == CPT::p33_pp_ID) );

	bool p_delta = ( (d_id == CPT::p33_0_ID) || (d_id == CPT::p33_p_ID) || (d_id == CPT::p33_m_ID) );
  
	if ( (n_id == CPT::neutron_ID && n_delta) || (d_id == CPT::proton_ID && p_delta) ) {
		return __pionExchangeCrossSection(s);
	} 
	return 0.;  
}

//_________________________________________________________________________________________________										
// Calcula as secoes de choque de producao de pions para as
// reacoes proton-proton e proton-neutron
//  Reacao p p --> p p pi+ pi-

double pp_pion_twopion_1_cross_section ( Dynamics* p1, Dynamics* p2, double* ){
	if ( p1->PdgId() == CPT::proton_ID && p2->PdgId() == CPT::proton_ID ) {
		
		const double a = 5.19635E-9;
		const double b = -1.99216E-5;
		const double c = .02726;
		const double d = -10.39548;

		TLorentzVector u = ( p1->Momentum(), p2->Momentum() );
		double T = u.E() - p2->GetMass();  
    
		return d + c * T + b * sqr(T) + a * (sqr(T) * T);
	}
	return 0.;
}

//_________________________________________________________________________________________________										
// Reacao p p --> p n pi+ pi0
 
double pp_pion_twopion_2_cross_section ( Dynamics* p1, Dynamics* p2, double* ){

	if ( p1->PdgId() == CPT::proton_ID && p2->PdgId() == CPT::proton_ID ) {
		const double a = -2.88858E-6;
		const double b = .0101;
		const double c = -5.31208;
    
		TLorentzVector u = ( p1->Momentum(), p2->Momentum() );
		double T = u.E() - p2->GetMass();  
    
		return c + b * T + a * sqr(T); 
	}
	return 0.;
}

//_________________________________________________________________________________________________										
// Reacao p n --> p n pi+ pi-

double pn_pion_twopion_1_cross_section( Dynamics* p1, Dynamics* p2, double* ){

	if ( ( p1->PdgId() == CPT::proton_ID  && p2->PdgId() == CPT::neutron_ID ) || ( p1->PdgId() == CPT::neutron_ID && p2->PdgId() == CPT::proton_ID ) )  {
    
		TLorentzVector u = ( p1->Momentum(), p2->Momentum() );
		double T = u.E() - p2->GetMass();  
    
		const double a = 1.45407E-8;
		const double b = -5.03237E-5;
		const double c = .0593;
		const double d = -20.81822;
      
		return d + c * T + b * sqr(T) + a * (sqr(T) * T);
	}
	return 0.;
}

//_________________________________________________________________________________________________										
