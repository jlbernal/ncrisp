/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "IonEventGen.hh"
#include "../base/NucleusDynamics.hh"
using namespace std;

/*
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(IonEventGen);
#endif // CRISP_SKIP_ROOTDICT
*/

//_________________________________________________________________________________________________										

IonEventGen::IonEventGen(): EventGen(), channels(){
	
	channels.SetOwner();
	ion = 0;
	tcs = 0.;
}

//_________________________________________________________________________________________________										

IonEventGen::~IonEventGen(){

	channels.Delete();
}

//_________________________________________________________________________________________________										

Double_t IonEventGen::TotalCrossSection(Dynamics& p1, Dynamics& p2){

	Double_t tcs1 = 0.;
  
	for ( Int_t i = 0; i < channels.GetEntriesFast(); i++ ) {
		tcs1 += ((BBChannel*)channels[i])->CrossSection().Calculate( &p1, &p2);
	}
	return tcs1;
}

//_________________________________________________________________________________________________										

Int_t IonEventGen::GenerateSingleNucleon( ParticleDynamics nucleon, NucleusDynamics& nuc, MesonsPool& mpool){
  
	// Initializing particle ...
	
        Time_order(nucleon,nuc);
	double tsrf=time_surface(nucleon,nuc);
// 	  cout<<"time_surface "<<tsrf<<endl;
// 	for(int i=0;i<15;i++)
// 	  cout<<"Time: "<<time_collision(nucleon,nuc[i],TotalCrossSection(nucleon, nuc[i]))<<" IPS "<<TMath::Pi()*impact_parameter2(nucleon, nuc[i])<<" TCS: "<<TotalCrossSection(nucleon, nuc[i])<<endl;
	
	nucleon.BindIt(true);
	
	Int_t n_add = nuc.CaptureNucleon(nucleon);
	// Calculate the total cross section for all nucleons, and select
	// the interaction channel.
	//cout<<"Nuevo nucleon"<<endl;

	for ( Int_t i = 0; (i < nuc.GetA() - 1) && nuc[i].IsBind()==true; i++ ) {
		Double_t tcs = TotalCrossSection(nucleon, nuc[i]);
		Double_t b2 = impact_parameter2(nucleon, nuc[i]);
		if(time_collision(nucleon,nuc[i],tcs)>tsrf) continue;
		if ( TMath::Pi() * b2 <= tcs* .1 ) {
			Double_t sum = 0.;
			Double_t x = gRandom->Uniform();
			for ( Int_t j = 0; j < channels.GetEntriesFast(); j++ ) { 	
				sum += ((BBChannel*)channels[j])->CrossSection().Value() / tcs;
				if ( x < sum ) {
					std::vector<ParticleDynamics> v;
					Double_t t = 0.;
					//cout<<"intentando"<<endl;
					int intento=100000;
					while(intento--){//cout<<"intentos "<<intento<<endl;
					if (((BBChannel*)channels[j])->DoAction( i, n_add, &nuc, t, &mpool, &v)){
						//nuc.SetMomentum(nuc.Momentum() + nucleon.Momentum()); // ojo quitar
						nuc.SetAngularMomentum(nuc.AngularMomentum() + nucleon.Position().Cross(nucleon.Momentum().Vect()));
						//cout << "Channel: " << ((BBChannel*)channels[j])->CrossSection().GetName() << endl;
						//cout << "Interact with nucleon "<< i<<endl;
						return i;
					}else {
					        
						continue;
					 // return -1;
					}
					}
				}	
			}
		}    
	}
/*	if( nucleon.PdgId() == CPT::neutron_ID && nucleon.TKinetic() <= nuc.NuclearPotential( CPT::neutron_ID )){
	   double level = nuc.NeutronFermiLevels().GetIntLevelOf(nucleon.Momentum().Vect().Mag());
	   nuc.NeutronFermiLevels()[level]--;
	}
	if( nucleon.PdgId() == CPT::proton_ID && nucleon.TKinetic() <= nuc.NuclearPotential( CPT::proton_ID )){
	   double level = nuc.ProtonFermiLevels().GetIntLevelOf(nuc.GetProtonMomentum(nucleon.Momentum().Vect()));
	   nuc.ProtonFermiLevels()[level]--;
	}// First 'for' closing brace. */
	
	nucleon.BindIt(false);
	nuc[nuc.GetA()-1].SetMomentum(TVector3(0,0,0));
	nuc[nuc.GetA()-1].BindIt(false);
	
	//cout<<"Nucleon BindIt "<<nucleon.IsBind()<<" in the nucleus "<<nuc[nuc.GetA()-1].IsBind()<<endl;
	return -1;
}

//_______________________________________________________________________________________________________________

void IonEventGen::Create_Ion(Int_t Ai, Int_t Zi, NucleusDynamics& nuc){
	ion = new NucleusDynamics(Ai, Zi); 
	
	double kfn = nuc.NeutronFermiLevels().GetFermiMomentum();
	double ion_kfn=ion->NeutronFermiLevels().GetFermiMomentum();
	for ( Int_t i = 0; i < Ai; i++) {
		double px = (*ion)[i].Momentum().Px();
		double py = (*ion)[i].Momentum().Py();
		double pz = (*ion)[i].Momentum().Pz(); 
		double m2 = (*ion)[i].Momentum().Vect().Mag2();    
		double m = TMath::Sqrt(m2);
		double f = ( m + kfn-ion_kfn) / m;    
		
		TVector3 k( f*px, f*py, f*pz ); 
		(*ion)[i].SetMomentum(k);         
	}
	
	ion->SetMass(ion->GetMass());
} 

//_______________________________________________________________________________________________________________

Int_t IonEventGen::Generate(Double_t IonEnergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char* opt, std::ostream *os) {

	cout << " ________________________________________________________.. " << endl;	
	
	this->Create_Ion(Par[1], Par[0], nuc);
        position_order(*ion);
	Double_t ion_r = ion->GetRadium();		
	TVector3 vecP, vecP1;
	vecP = StartPosition(nuc,ion_r);
	
/*        for(int i=0;i<ion->GetA();i++) 
	  if( (*ion)[i].PdgId()==CPT::proton_ID)cout << (*ion)[i].Momentum().Vect().Mag()<<"\t"<< (*ion)[i].Momentum().E()<<" "<<endl;
        cout<<endl;
        for(int i=0;i<ion->GetA();i++) 
	  if((*ion)[i].PdgId()==CPT::neutron_ID)cout <<(*ion)[i].Momentum().Vect().Mag()<<"\t"<<(*ion)[i].Momentum().E()<<" "<<endl;
	cout<<endl;  
	
        for(int i=0;i<nuc.GetA();i++) 
	  if(nuc[i].PdgId()==CPT::proton_ID)cout << nuc[i].Momentum().Vect().Mag()<<"\t"<< nuc[i].Momentum().E()<<" "<<endl;
        cout<<endl;
        for(int i=0;i<nuc.GetA();i++) 
	  if(nuc[i].PdgId()==CPT::neutron_ID)cout << nuc[i].Momentum().Vect().Mag()<<"\t"<< nuc[i].Momentum().E()<<" "<<endl;	*/  

//         cout<<nuc.ProtonFermiLevels().GetFermiMomentum()<<" "<<nuc.NeutronFermiLevels().GetFermiMomentum()<<endl;
	Double_t gamma =1.; 
	
	IonEnergy =  gamma*IonEnergy; 


	int counter = 0;
	int nuc_A = ion->GetA();
	double level=0;
	TVector3 ion_momentum(0,0,0);
	TLorentzVector nuc_momentum(0,0,0,0);
	
        	
		 double pxo = 0,tk=IonEnergy/double(nuc_A);
		 TVector3 k;
		 		 
		 counter=0;
	         for(int i=0; i<nuc_A; i++){ 
	         vecP1 = StartPosition2(vecP, (*ion)[i].Position(),nuc.GetRadium());
		 pxo = (*ion)[i].Momentum().Px();
		 k  =  TVector3(TMath::Sqrt(pxo*pxo + tk*(tk+ 2.0*(*ion)[i].Momentum().E())),(*ion)[i].Momentum().Py(),(*ion)[i].Momentum().Pz());  
		 (*ion)[i].SetMomentum(k);
	  	 (*ion)[i].SetPosition(vecP1);
		 
		 //cout<<"ion_final "<<(*ion)[i].Momentum().Vect().Mag()<<" "<<(*ion)[i].Momentum().E()<<endl;
 
		        if(vecP1.Mag() <= nuc.GetRadium() + pow(10,-10)){//cout<<"Ion "<<i<<" quiere entrar"<<endl;
				if (this->Ion_Capture((*ion)[i],nuc,mpool)){
			         counter++;
				}		
		        }
	         }
	           
                 std::cout<< "Interacting Nucleons  " << counter << std::endl;
		 nuc.SetMass(nuc.GetMass());
	         ion_momentum = TVector3(sqrt(IonEnergy*IonEnergy+2.0*ion->GetMass()*IonEnergy)/double(nuc_A)*counter,0,0);
		 nuc_momentum = TLorentzVector(ion_momentum,sqrt(nuc.GetMass()*nuc.GetMass()+ion_momentum.Mag2()));
		 nuc.SetMomentum(nuc_momentum);
		 
/*        for(int i=0;i<ion->GetA();i++) 
	  if( (*ion)[i].PdgId()==CPT::proton_ID)cout << (*ion)[i].Momentum().Vect().Mag()<<"\t"<< (*ion)[i].Momentum().E()<<" "<<endl;
        cout<<endl;
        for(int i=0;i<ion->GetA();i++) 
	  if((*ion)[i].PdgId()==CPT::neutron_ID)cout <<(*ion)[i].Momentum().Vect().Mag()<<"\t"<<(*ion)[i].Momentum().E()<<" "<<endl;
	cout<<endl;		*/ 

	if(counter > 0){
		  nuc.Set_InitE(counter*tk); 	
	  	  //std::cout << counter << std::endl;
		  delete ion;
	return counter;
	} 
	else{
	delete ion;   
	return -1;
	}
	
}

//_________________________________________________________________________________________________										

Int_t IonEventGen::Generate(	Double_t IonEnergy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  cout << "It shlod not be here " << endl;
  exit(-1);
  
  return -1;
}



TVector3 IonEventGen::StartPosition(NucleusDynamics& nuc, Double_t ion_rad){

	Double_t r = pow(gRandom->Uniform(),0.5) * (nuc.GetRadium()+ion_rad); 
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z); 
	Double_t x;
	if(r<=nuc.GetRadium())
	x = - TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	else x=0;
	return TVector3(x, y, z);
}

//_________________________________________________________________________________________________										

TVector3 IonEventGen::StartPosition2(TVector3 v1, TVector3 v2, double nuc_radium2){
       nuc_radium2=nuc_radium2*nuc_radium2;
       v2 = v2+v1;
       Double_t r2 = sqr(v2.Z()) + sqr(v2.Y());
       if (r2<=nuc_radium2){
	 v2.SetX(-sqrt(nuc_radium2-r2)); 
      }
	return v2;
}

bool IonEventGen::Ion_Capture (ParticleDynamics nucleon, NucleusDynamics& nuc, MesonsPool& mpool){
  
  TVector3 P_in = nucleon.Momentum().Vect();
  Double_t level;
  
  if(nucleon.Position().Mag() > nuc.GetRadium() + pow(10,-10)) return false;
  
  if( nucleon.PdgId() != CPT::proton_ID && nucleon.PdgId() != CPT::neutron_ID ) return false;
  
  if(nucleon.Position().Mag()<=nuc.GetRadium()+pow(10,10)){
      
/*         if( nucleon.PdgId() == CPT::proton_ID && nucleon.TKinetic() <= nuc.NuclearPotential( CPT::proton_ID )){
	   
	      level = nuc.ProtonFermiLevels().GetIntLevelOf(nuc.GetProtonMomentum(P_in));
	      
	      cout<<"proton level "<<level<<" Freelevels "<<nuc.ProtonFermiLevels().GetFreeLevels(level)<<endl;
	      
	          if(nuc.ProtonFermiLevels().GetFreeLevels(level)>0){
		    
		       nuc.ProtonFermiLevels()[level]++;
		       		    
		  }
		  else return false;
	   
	}
	
	
         else if( nucleon.PdgId() == CPT::neutron_ID && nucleon.TKinetic() <= nuc.NuclearPotential( CPT::neutron_ID )){
	   
	      level = nuc.NeutronFermiLevels().GetIntLevelOf(P_in.Mag());
	      
	      cout<<"neutron level "<<level<<" Freelevels "<<nuc.ProtonFermiLevels().GetFreeLevels(level)<<endl;
	      
	          if(nuc.NeutronFermiLevels().GetFreeLevels(level)>0){
		    
		       nuc.NeutronFermiLevels()[level]++;
		       		    
		  }
		  else return false;
	   
	}
	*/
	//else return false;
	
	if(GenerateSingleNucleon(nucleon,nuc,mpool)>=0){
 	//{nucleon.BindIt(true);                   //EESTO ES TRUE
 	//Int_t n_add = nuc.CaptureNucleon(nucleon);
 	//nuc.SetAngularMomentum(nuc.AngularMomentum() + nucleon.Position().Cross(nucleon.Momentum().Vect()));
	return true;
	}
	
	
  }
  
  return false;       //CAMBIAR A FALSE AL TERMINAR LA FUNCION
}

void IonEventGen::position_order (NucleusDynamics& ion){
  cout<<endl;
  
  ParticleDynamics c;
  
  for(int i=0;i<ion.GetA()-1;i++){
    for(int j=i+1; j<ion.GetA();j++){
      if(ion[i].Position().X()<ion[j].Position().X()){
	std::swap(ion[i], ion[j]);
      }
    }
  }
  
/*  for(int i=0;i<ion.GetA();i++){
	  //cout << "NucleusDynamics P E " << nuc[i].Momentum().Vect().Mag() << " " << nuc[i].Momentum().E() <<  endl;
          
	  printf("%lf\t%lf\t%lf\t%lf\t%lf\n",ion[i].Position().X(),ion[i].Momentum().Px(),ion[i].Momentum().Py(),ion[i].Momentum().Pz(),ion[i].Momentum().E());
	} 
cout<<endl; */ 
}


void IonEventGen::energy_order (NucleusDynamics& ion){
  cout<<endl;
  
  ParticleDynamics c;
  
  for(int i=0;i<ion.GetA()-1;i++){
    for(int j=i+1; j<ion.GetA();j++){
      if(ion[i].Momentum().E()<ion[j].Momentum().E()){
	std::swap(ion[i], ion[j]);
      }
    }
  }
  
  for(int i=0;i<ion.GetA();i++){
	  //cout << "NucleusDynamics P E " << nuc[i].Momentum().Vect().Mag() << " " << nuc[i].Momentum().E() <<  endl;
          
	  printf("%lf\t%lf\t%lf\t%lf\t%lf\n",ion[i].Position().X(),ion[i].Momentum().Px(),ion[i].Momentum().Py(),ion[i].Momentum().Pz(),ion[i].Momentum().E());
	} 
cout<<endl;  
}


TLorentzVector IonEventGen::Lorentz_Transformation (TLorentzVector &nuc_momentum, TLorentzVector &pp)
{
     double betta = nuc_momentum.Vect().Mag()/nuc_momentum.E();
     double gamma = 1.0/sqrt(1-betta*betta);
     double px = -betta*gamma*pp.E()+gamma*pp.Px();
     double E = gamma*pp.E()-gamma*betta*pp.Px();
     pp.SetPx(px);
     pp.SetE(E);
     return pp;
}

void IonEventGen::Time_order(ParticleDynamics &p,NucleusDynamics& nuc){
  
  int A=nuc.GetA();
  int B=A-1;
  double t1,t2,cs1,cs2;
  
  for (int i=0;i<B;i++)
    for (int j=i+1;j<A;j++){
      cs1=TotalCrossSection(p, nuc[i]);
      cs2=TotalCrossSection(p, nuc[j]);
      t1 = time_collision(p,nuc[i],cs1);
      t2 = time_collision(p,nuc[j],cs2);
      if (t1>t2) {std::swap(nuc[i], nuc[j]);}
    }
  
}		/*	if((*ion)[i].PdgId()==CPT::proton_ID)	{
			level =  nuc.ProtonFermiLevels().GetIntLevelOf(nuc.GetProtonMomentum(k));	
		 	cout << "Nivel de fermi del proton " << level << " free " << nuc.ProtonFermiLevels().GetFreeLevels(level) << " ocuped " << nuc.ProtonFermiLevels()[level] <<   endl;
			}
		        if((*ion)[i].PdgId()==CPT::neutron_ID)	{
		 	level = nuc.NeutronFermiLevels().GetIntLevelOf(k.Mag());
			cout << "Nivel de fermi del neutron " << level << " free " << nuc.NeutronFermiLevels().GetFreeLevels(level) << " ocuped " << nuc.NeutronFermiLevels()[level] << endl;
			}		
		*/