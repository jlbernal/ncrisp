/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "kaon_n_cross_sections.hh"

//_________________________________________________________________________________________________										

// rho_elastic_cross_section uses this function ... 
double bb_elastic_cross_section( Dynamics* q1 , Dynamics* q2, double* cch );

//_________________________________________________________________________________________________										

Double_t kaon_elastic_cross_section(Dynamics* kaon, Dynamics* nucleon, Double_t* cch){

	if ( CPT::IsKaon( ((ParticleDynamics*)(kaon))->PdgId() ) && CPT::IsNucleon( ((ParticleDynamics*)(nucleon))->PdgId() ) ){
		return bb_elastic_cross_section(kaon, nucleon, cch);
	}
	return 0.;
}
