/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __PhotonChannel_pHH
#define __PhotonChannel_pHH

#include "TObject.h"
#include "Api.h"
#include "TMethodCall.h"
#include "CrossSectionChannel.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"

class PhotonChannel: public TObject{
private:
	Int_t f_type;
	CrossSectionChannel cs;
	bool (*f_ptr)( ParticleDynamics*, Int_t&, NucleusDynamics*, MesonsPool*);
	TMethodCall* f_call;
	Int_t counts;
	Int_t blocked;
public:
	PhotonChannel();
	PhotonChannel(const CrossSectionChannel& ch, void* act);
	PhotonChannel(PhotonChannel& u);
	virtual ~PhotonChannel();
#ifndef __CINT__
	PhotonChannel( const char* name, Double_t (*csec)(Dynamics*, Dynamics*, Double_t*), Int_t num_param, bool (*act)( ParticleDynamics*, Int_t&, NucleusDynamics*, MesonsPool*) );
#endif
	inline CrossSectionChannel& CrossSection() { return cs; }
	inline Int_t Counts() const { return counts; }
	inline void AddCount() { counts++; } 
	inline Int_t Blocked() const { return blocked; }
	inline void AddBlocked() { blocked++; }   
	inline void Reset() { counts = 0; blocked = 0; } 
	bool DoAction( ParticleDynamics* gamma, Int_t idx, NucleusDynamics* nuc, MesonsPool* mpool);  
	virtual void Copy(TObject& obj) const;
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(PhotonChannel,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
