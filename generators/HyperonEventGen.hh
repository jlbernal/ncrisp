/* ================================================================================
 * 
 * 	Copyright 2013 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __HyperonEventGen_pHH
#define __HyperonEventGen_pHH

#include <iostream>
#include <fstream>
#include <map>

#include "TMath.h"
#include "TObjArray.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TAxis.h"
#include "TGraphErrors.h"
#include "EventGen.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "CrossSectionData.hh"
#include "DataHelperH.hh"

class HyperonEventGen: public EventGen{

private:
	Int_t p1_nuc_idx; // the index of the selected nucleon 1.
	Int_t p2_nuc_idx; // the index of the selected nucleon 2.
	Int_t HypModel;	// Francisco Hyperon model (1 or 2).
	TObjArray channels;     // The channels fulfilling the virtual fuction of EventGen.hh
	enum ChannelType {
		nn_swave = 1,
		nn_pwave,
		np_swave,
		np_pwave,
	};
	Double_t* _N_angle;	
	DataHelper_Hyp* DhhP2;
	DataHelper_Hyp* DhhCos;
	Double_t np,nn;
	Double_t SWAVE_NEUTRON_PROBABILITY;
	Double_t PWAVE_NEUTRON_PROBABILITY;
	Double_t SWAVE_PROTON_PROBABILITY;
	Double_t PWAVE_PROTON_PROBABILITY;

protected:
	void SelectNucleons( ParticleDynamics& gamma, NucleusDynamics& nuc, TObjArray& csData );
	Int_t select_channel();
	Int_t select_nucleon_in_nucleus(NucleusDynamics& nuc, Int_t pdgId);
	Double_t get_channel_delta(ChannelType ch); // Liberated Energy of Decay
	Double_t select_momentum_by_dist(TGraphErrors* tg);
	Double_t select_angular_dist(TGraphErrors* tg);
	TGraphErrors* get_P_BychannelANDangle(ChannelType n_case, Int_t N_Ini);
	TGraphErrors* get_Cos_BychannelANDangle(ChannelType n_case);
	TVector3 nucleon_center_position(Double_t radius);
public:
	HyperonEventGen(Int_t Hymodel);
	~HyperonEventGen();
	TObjArray& GetChannels() { return channels; }
	inline Int_t GetHypModel() { return HypModel; }
	inline Int_t GetHypNucleon1() { return p1_nuc_idx; }
	inline Int_t GetHypNucleon2() { return p2_nuc_idx; }
	void Read_Data();
	Int_t Generate( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char *opt = 0 , std::ostream *os = 0  );
	Int_t Generate(Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os=0);

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(HyperonEventGen,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
