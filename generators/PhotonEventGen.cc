/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "PhotonEventGen.hh"
using namespace std;
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(PhotonEventGen);
#endif // CRISP_SKIP_ROOTDICT
//_____________________________________________________________________________________________										
PhotonEventGen::PhotonEventGen():EventGen(), channels(){
	channels.SetOwner();
	tcs = 0.;
	num_photons = 0;
	selected_ch = -1;
	selected_nucleon = -1;
	Q_2 = 0;
}

PhotonEventGen::~PhotonEventGen(){
	channels.Delete(); 
}

Measurement PhotonEventGen::Generate( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, Int_t times, char *opt, std::ostream *os ){
	if ( channels.IsEmpty() )
		return Measurement();  
	Double_t sf = (TMath::Pi() * sqr(nuc.GetRadium()) * 10000) / nuc.GetA();
	std::vector<Double_t> v(0);
	Int_t count = 0;
	for ( Int_t i = 0; i < times; i++ ) {
		while( this->Generate(energy, nuc, mpool, v, opt, os) < 0 )
			count++;
		nuc.DoInitConfig();
		mpool.clear();
	}
	Double_t cs = (Double_t)times/(Double_t)(count) * sf;
	return Measurement(cs, 0.);
}

Int_t PhotonEventGen::Generate( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char *opt, std::ostream *os){
	
	selected_ch = -1;
	num_photons++;
	ParticleDynamics gamma( CPT::photon_ID );
	gamma.SetMomentum( TLorentzVector(energy, 0., 0., energy) ); //the beam axis is X    
	gamma.SetPosition(StartPosition(nuc));
	TObjArray nucleons_arr;
	this->SelectNucleons(gamma, nuc, nucleons_arr);  
	if ( nucleons_arr.GetEntriesFast() == 0 ){ 
		return -1;
  	}
	TString stropt(opt);
	for ( Int_t i = 0; i < nucleons_arr.GetEntriesFast() ; i++ ) {
		Int_t idx = ((CrossSectionData*)nucleons_arr[i])->GetIdx();
		Double_t total_cs = this->TotalCrossSection( &gamma, idx, &nuc);
//		std::cout  << "channel by channel ..... acepted nucleon[" << idx << "]'s total CS = " << total_cs << std::endl;
		Double_t sum = 0.;      
		Double_t x = gRandom->Uniform();
		 /*
		  * NOTE: Next loop on interaction channels is the Monte Carlo integration of
		  * a probability density distribution, in this case, the cross section. The random variable 
		  * x above must be where it is, outside the channels loop, by this manner being uniqe 
		  * for each integration because this is how Monte Carlo works, by the
		  * comparison of one random number with the integral of 
		  * a probability density distribution
		  */
		for ( Int_t j = 0; j < channels.GetEntriesFast() ; j++ ) {
			PhotonChannel* ch = ((PhotonChannel*)channels[j]);      
//			std::cout << "channel[" << j << "] = " << ch->CrossSection().GetName() << ", cross section value = " << ch->CrossSection().Value() << std::endl;
			sum += ch->CrossSection().Value()/total_cs;      
			if ( x <= sum ) { 
//				std::cout << "quasi Selected : " << ch->CrossSection().GetName() << std::endl;  
				if ( ch->DoAction( &gamma, idx, &nuc, &mpool) ) {
					selected_ch = j; ch->AddCount();
					nucleons_arr.Delete();
					nuc.SetMomentum(nuc.Momentum() + gamma.Momentum());
					nuc.SetAngularMomentum(nuc.AngularMomentum() + gamma.Position().Cross(gamma.Momentum().Vect()));
					if ( stropt.CompareTo("quiet") != 0 )
						if( os ) (*os) << "Selected : " << ch->CrossSection().GetName() << std::endl;
					return j;	  
				} 
				else {
					ch->AddBlocked();
					break;
				}	
			}
		} // closing brace of 'for(...)'
	}
	nucleons_arr.Delete();
	return -2; //-2 stands for Pauli Blocking photo event
}

void PhotonEventGen::SelectNucleons( ParticleDynamics& gamma, NucleusDynamics& nuc, TObjArray& csData ){ //using the total cross section for nucleon selection
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		Double_t b2 = sqr( nuc[i].Position().Y() - gamma.Position().Y() ) + sqr( nuc[i].Position().Z() - gamma.Position().Z() );
		Double_t W = (gamma.Momentum() + nuc[i].Momentum()).M() / 1000.; //GeV
		Double_t gn = 0;
		Double_t gn_Res = 0;
		Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
		Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
		Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
		Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
		Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
		Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
		gn_Res = gn_Res + p33Formation ( &gamma, &nuc[i], p33_params );  /// Delta(1232) P33
		gn_Res = gn_Res + p11Formation ( &gamma, &nuc[i], p11_params );  /// N(1440)     P11
		gn_Res = gn_Res + d13Formation ( &gamma, &nuc[i], d13_params );  /// N(1520)     D13
		gn_Res = gn_Res + f15Formation ( &gamma, &nuc[i], f15_params ); /// N(1680)     F15
		gn_Res = gn_Res + s11Formation ( &gamma, &nuc[i], s11_params );  /// N(1535)     S11
		gn_Res = gn_Res + f37Formation ( &gamma, &nuc[i], f37_params );  /// N(1950)     F37
		gn_Res = gn_Res + qdCrossSection( &gamma, &nuc, f37_params ); // reference at: Rossi P. Phys Rev C 40(1989)2412
		if ( nuc[i].PdgId() == CPT::neutron_ID ) {
			Double_t nMass = (CPT::n_mass*CPT::effn)/1000.; //mass in GeV
			Double_t E =  W*W/nMass/2. - nMass/2;
			Double_t bn1 = 87., bn2 = 65.;
			Double_t x = -2. * ( E - 0.139 );      
			gn = ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
			gn = gn + gn_Res;
		}
		if ( nuc[i].PdgId() == CPT::proton_ID ) {
			Double_t pMass = (CPT::p_mass*CPT::effn)/1000.; //mass in GeV
			Double_t E = W*W/pMass/2 - pMass/2;
			Double_t bp1 = 91., bp2 = 71.4; 
			Double_t x = -2. * ( E - 0.139 );      
			gn = ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );
			gn = gn + gn_Res;
		}
//		Double_t gn = this->TotalCrossSection(&gamma, i, &nuc);
		// .0001 is due to conversion: micro barn -> fm^2
		if ( TMath::Pi() * b2 <= gn * .0001 ) {     
			//cout << " W = " << W << ", nuc[i].Momentum().M() = " << nuc[i].Momentum().M() << ", selection gn = " << gn << endl;
			csData.Add(new CrossSectionData(i, b2, gn));
		}
	}
}

//_____________________________________________________________________________________________


Int_t PhotonEventGen::Generate(	Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  return -2;
  
  
}


//_____________________________________________________________________________________________

TVector3 PhotonEventGen::StartPosition(NucleusDynamics& nuc){
//	Double_t Rad = CPT::r0 * TMath::Power((Double_t)250, 1./3.);
//	Double_t r = pow(gRandom->Uniform(),0.5) * Rad;
	Double_t r = pow(gRandom->Uniform(),0.5) * nuc.GetRadium();
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z);
//	Double_t x = TMath::Sqrt(sqr(Rad) - r2);
	Double_t x = TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	return TVector3(x, y, z);
}
