/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "CrossSectionChannel.hh" 

//_________________________________________________________________________________________________										

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(CrossSectionChannel);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

CrossSectionChannel::~CrossSectionChannel(){
 
	// if ( params ) delete []params; 
	if ( f_call ) 
		delete f_call;
}  
//_________________________________________________________________________________________________										

CrossSectionChannel::CrossSectionChannel():TNamed(){
	cs = 0.;
	lower_cut = 0.;
	upper_cut = 0.;
	f_type = 0;
	f_ptr = 0;
	f_call = 0;
	// params = 0;
	for ( Int_t i = 0; i < __csc_max_params; i++ ) 
		params[i] = 0.;
	
	num_params = 0;
	active = false;
}
//_________________________________________________________________________________________________										

//#ifdef __CINT__ //for g++ compiler
#if defined(CRISP_SKIP_ROOTDICT) //for root compiler
CrossSectionChannel::CrossSectionChannel( const char* branch_name, void* fcn, Int_t num_params ):TNamed(branch_name, branch_name){
  
	cs = 0.;
	lower_cut = 0.;
	upper_cut = infty;    
	this->num_params = num_params;
	f_type = 1;
	f_ptr = 0;
	char *funcname = G__p2f2funcname(fcn);
	SetTitle(funcname);
	if (funcname) {
		f_call = new TMethodCall();
		f_call->InitWithPrototype(funcname,"Dynamics*,Dynamics*,Double_t*");    
	} 
	for ( Int_t i = 0; i < __csc_max_params; i++ ) 
		params[i] = 0.;
	//if ( num_params > 0 )
	//  params = new Double_t[num_params];
	//else 
	//  params = 0;
	active = true;
}
#else
CrossSectionChannel::CrossSectionChannel( const char* branch_name, Double_t (*fcn)(Dynamics*, Dynamics*, Double_t*), Double_t l_cut, Double_t u_cut, Int_t num_params):TNamed(branch_name, branch_name){  
  
	cs = 0.;
	lower_cut = l_cut;
	upper_cut = u_cut;  
	this->num_params = num_params;
	f_type = 2;
	f_call = 0;
	f_ptr = fcn;   
	//if ( num_params > 0 )
	//  params = new Double_t[num_params];
	//else 
	//  params = 0;
	for ( Int_t i = 0; i < __csc_max_params; i++ ) 
		params[i] = 0.;
  
	active = true;
}
#endif

CrossSectionChannel::CrossSectionChannel( const CrossSectionChannel& u ):TNamed(u){
	((CrossSectionChannel&)u).Copy(*this);
}
//_________________________________________________________________________________________________										

Double_t CrossSectionChannel::Calculate( Dynamics* p1, Dynamics* p2 ){
	cs = 0.;
	if ( active == false || f_type == 0 ) 
		return 0.;  
	Double_t en = (p1->Momentum() + p2->Momentum()).M();  
	if ( f_type == 1 ) { 
		if ( (en < lower_cut) || (en > upper_cut) )
			return 0.;
		Long_t args[3];
		args[0] = (Long_t)p1;
		args[1] = (Long_t)p2;
		args[2] = (Long_t)&params[0];
    
		f_call->SetParamPtrs(args);
		f_call->Execute(cs);
		return cs;
	}
	if ( f_type == 2 ) { 
		cs = ( (en < lower_cut) || (en > upper_cut) ) ? 0. : (*f_ptr)(p1, p2, &params[0]);
		return cs;
	}     
	return cs;
}

//_________________________________________________________________________________________________										

void CrossSectionChannel::Copy(TObject& obj) const{

	TNamed::Copy(obj);
  
	((CrossSectionChannel&)obj).cs = cs;
	((CrossSectionChannel&)obj).lower_cut = lower_cut;
	((CrossSectionChannel&)obj).upper_cut = upper_cut;
	((CrossSectionChannel&)obj).f_type = f_type;
	((CrossSectionChannel&)obj).f_ptr = f_ptr;
  
	if (f_call) {
		TMethodCall *m = new TMethodCall();
		m->InitWithPrototype(f_call->GetMethodName(),f_call->GetProto());
		((CrossSectionChannel&)obj).f_call =  m;
	}
 
	((CrossSectionChannel&)obj).num_params = num_params;
  
	if ( num_params ) {
		// ((CrossSectionChannel&)obj).params = new Double_t[num_params];
		for ( Int_t i = 0; i < num_params; i++ ) 
			((CrossSectionChannel&)obj).params[i] = params[i];
	} 
	else 
		for ( Int_t i = 0; i < __csc_max_params; i++ ) 
			((CrossSectionChannel&)obj).params[i] = 0.;
  
	((CrossSectionChannel&)obj).active = active;
}

//_________________________________________________________________________________________________										
