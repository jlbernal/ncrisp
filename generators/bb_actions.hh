/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __bb_actions_pHH
#define __bb_actions_pHH

#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "relativisticKinematic.hh"
#include "resonance_mass.hh"

///
//_________________________________________________________________________________________________										

bool bb_elastic ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool bb_elastic_np ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool NN_inelastic ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool np_inelastic ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool NDelta_inelastic ( int &idx1, int &idx2, double &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool bb_inelastic ( int& idx1, int& idx2, double& t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);
bool NLambda_inelastic ( Int_t &idx1, Int_t &idx2, Double_t &t, NucleusDynamics& nuc, std::vector<ParticleDynamics>& v);

//

Double_t elastic_np_angle(Double_t *x, Double_t *par  ) ;


#endif
