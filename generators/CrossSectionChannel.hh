/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __CrossSectionChannel_pHH
#define __CrossSectionChannel_pHH

#include "TNamed.h"
#include "TMath.h"
#include "Api.h"
#include "TMethodCall.h"
#include "TLorentzVector.h"
#include "Dynamics.hh"
#include "ParticleDynamics.hh" 
#include "TBuffer.h"

#define __csc_max_params 20
//_____________________________________________________________________________________________										
class CrossSectionChannel: public TNamed{
public:  
	virtual ~CrossSectionChannel();
	CrossSectionChannel();
	CrossSectionChannel( const CrossSectionChannel& u );  
//#ifdef __CINT__ // for g++ compiler
#if defined(CRISP_SKIP_ROOTDICT) // for root compiler
	CrossSectionChannel( const char* branch_name, void* fcn, Int_t num_params = 0);  
#else
	CrossSectionChannel( const char* branch_name, Double_t (*fcn)(Dynamics*, Dynamics*, Double_t*), Double_t l_cut = 0., Double_t u_cut = infty, Int_t num_params = 0);  
#endif
	inline Double_t CrossSection() const { return cs; }  
	inline Double_t Value() const { return cs; }  
	inline Double_t GetLowerEnergyCut() const { return lower_cut; }
	inline void SetLowerEnergyCut(Double_t lc) { lower_cut = lc; }
	inline Double_t GetUpperEnergyCut() const { return upper_cut; } 
	inline void SetUpperEnergyCut(Double_t uc) { upper_cut = uc; }
	inline Int_t GetNpar() const { return num_params; } 
	inline Double_t* GetParams() { return &params[0]; }  
	inline void SetParams(Double_t* p);
	inline bool IsActive() { return active; }  
	void SetActive(bool b) { active = b; }
	Double_t Calculate( Dynamics* p1, Dynamics* p2 );    
	virtual void Copy(TObject &obj) const; 
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(CrossSectionChannel,2);
#endif // CRISP_SKIP_ROOTDICT
private:    
	Double_t cs;
	Double_t lower_cut, upper_cut;  
	Int_t f_type;
	Double_t (*f_ptr)(Dynamics*, Dynamics*, Double_t *);
	TMethodCall* f_call;
	Double_t params[__csc_max_params];
	Int_t num_params;
	bool active;   
}; 
void CrossSectionChannel::SetParams(Double_t *p){
	for ( Int_t i = 0; i < num_params; i++ ) 
		params[i] = p[i];
}
// Some useful shorthand.
	typedef CrossSectionChannel CSC;
#endif
