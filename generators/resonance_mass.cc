/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "resonance_mass.hh"

//_________________________________________________________________________________________________										
// return the ressonance mass we are using the Breit-Wigner formula
// @param em_upper is the superior limit to the ressonance mass.

double resonance_mass( double em_upper, int p_id ){
   
	CPT *cpt = CPT::Instance();
	
	double width = cpt->ParticlePDG(p_id)->Width() * 1000.; // GeV -> MeV
	
	double em = cpt->ParticlePDG(p_id)->Mass() * 1000.;	// GeV -> MeV
	double lower_cut = em - ( width + 20. ); 
	//double lower_cut = 1000;
	double m_rss = 0.;
        //std::cout<<"CALCULANDO MASA DE RESONANCIA ADENTRO"<<std::endl;
	//std::cout<<"LIMITE MINIMO PARA RESONANCIA "<<lower_cut<<" MAXIMO "<<em_upper<<std::endl;
	if ( em_upper >= lower_cut ) { 
		double alpha = atan( 2. * ( lower_cut - em ) / width );  
		double beta  = atan( 2. * ( em_upper - em ) / width );    
		double psi = gRandom->Uniform();
		
		m_rss = em + .5 * width * tan( alpha + psi * ( beta - alpha ) );    
    
		if ( m_rss > em_upper ) 
			m_rss = em_upper;
		if ( m_rss < lower_cut )
			m_rss = lower_cut;    
	}
	else{ 
		//std::cout << "Warning" << std::endl;
		return em_upper * CPT::effn;
		//return -10;
	}  
	//std::cout<<" MASSA DE RESONANCIA "<<m_rss<<std::endl;
	return m_rss * CPT::effn;  
}

//_________________________________________________________________________________________________										
double resonance_mass_outside( double em_upper, int p_id ){
   
	CPT *cpt = CPT::Instance();
	
	double width = cpt->ParticlePDG(p_id)->Width() * 1000.; // GeV -> MeV
	
	double em = cpt->ParticlePDG(p_id)->Mass() * 1000.;	// GeV -> MeV
	//double lower_cut = em - ( width + 20. ); 
	double lower_cut = 1000; 
	double m_rss = 0.;
  
	if ( em_upper >= lower_cut ) { 
		double alpha = atan( 2. * ( lower_cut - em ) / width );  
		double beta  = atan( 2. * ( em_upper - em ) / width );    
		double psi = gRandom->Uniform();
		
		m_rss = em + .5 * width * tan( alpha + psi * ( beta - alpha ) );    
    
		if ( m_rss > em_upper ) 
			m_rss = em_upper;
		if ( m_rss < lower_cut )
			m_rss = lower_cut;    
	}
	else{ 
		//std::cout << "Warning" << std::endl;
		return em_upper;
	}  
	return m_rss;  
}

//_________________________________________________________________________________________________										
