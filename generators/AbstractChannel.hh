/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __AbstractChannel_pHH
#define __AbstractChannel_pHH

#include <vector>
#include "TObject.h"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"

#include "CrossSectionChannel.hh"

/// channel base class
//_________________________________________________________________________________________________										

class AbstractChannel: public TObject{

	Int_t counts; /// counter
	Int_t blocked; /// blocked number
	
	CrossSectionChannel cs;
  
protected:
  
	/// constructor
	//_________________________________________________________________________________________________										

	AbstractChannel();
  
	/// constructor
	//_________________________________________________________________________________________________										

	AbstractChannel(const CrossSectionChannel& ch);
  
	/// copy constructor
	//_________________________________________________________________________________________________										

	AbstractChannel(AbstractChannel& u);

public:

	/// destructor
	//_________________________________________________________________________________________________										

	virtual ~AbstractChannel();
  
	///  
	//_________________________________________________________________________________________________										

	inline CrossSectionChannel& CrossSection() { return cs; }
  
	///
	//_________________________________________________________________________________________________										

	inline Int_t Counts() const { return counts; }
  
	///
	//_________________________________________________________________________________________________										

	inline void AddCount() { counts++; } 
  
	/// 
	//_________________________________________________________________________________________________										

	inline Int_t Blocked() const { return blocked; }
  
	///
	//_________________________________________________________________________________________________										

	inline void AddBlocked() { blocked++; }   
  
	///
	//_________________________________________________________________________________________________										

	inline void Reset() { counts = 0; blocked = 0; } 
  
	///
	//_________________________________________________________________________________________________										

	virtual bool DoAction( ParticleDynamics* p, Int_t idx, NucleusDynamics* nuc, MesonsPool* mpool, Double_t t) = 0; 

	///
	//_________________________________________________________________________________________________										

	virtual bool DoAction( Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool = 0, std::vector<ParticleDynamics>* v = 0 ) = 0;

	virtual bool DoAction(LeptonsPool* lpool, Int_t idx1, Int_t idx2, NucleusDynamics* nuc, Double_t& t, MesonsPool* mpool = 0,std::vector<ParticleDynamics>* v = 0 ) = 0;

	// virtual void Copy(TObject& obj) const;  

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(AbstractChannel, 0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
