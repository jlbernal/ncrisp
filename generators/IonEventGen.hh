/* ================================================================================
 * 
 * 	Copyright 2015 2016 José Bernal Castillo 
 * 			   Ramon Pérez Varona
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __IonEventGen_pHH
#define __IonEventGen_pHH

#include "../base/transform_coords.hh"
#include "TMath.h"
#include "TObjArray.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "EventGen.hh"
#include "base_defs.hh"
#include "NucleusDynamics.hh"
#include "BBChannel.hh"         
#include "CrossSectionData.hh"
#include "time_collision.hh"
#include "time_surface.hh"

#include "DataHelper.hh"


class IonEventGen: public EventGen{

private :
	TObjArray channels;
	Double_t tcs;
	Int_t iz; Int_t ia;
	
public:
	NucleusDynamics* ion;    
  
	IonEventGen();

	virtual ~IonEventGen();

	TObjArray& GetChannels() { return channels; }

	Double_t TotalCrossSection( Dynamics& p1, Dynamics& p2);

	Int_t Generate(Double_t IonEnergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par,  char* opt = 0, std::ostream *os = 0);
//	Int_t Generate(	Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os);
	Int_t Generate(	Double_t IonEnergy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os);

	
	
	void position_order (NucleusDynamics& ion);
	
	void energy_order (NucleusDynamics& ion);
	
	void Time_order(ParticleDynamics &p,NucleusDynamics& nuc);

	Int_t GenerateSingleNucleon( ParticleDynamics nucleon, NucleusDynamics& nuc, MesonsPool& mpool);
	
	static TVector3 StartPosition( NucleusDynamics& nuc, Double_t);
	
	static TVector3 StartPosition2(TVector3 v1, TVector3 v2, double);
	
	void Create_Ion(Int_t Ai, Int_t Zi, NucleusDynamics& nuc); 
	
	bool Ion_Capture (ParticleDynamics nucleon, NucleusDynamics& nuc, MesonsPool& mpool);
	
	TLorentzVector Lorentz_Transformation (TLorentzVector &, TLorentzVector &);

/*	
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(IonEventGen, 0);
#endif // CRISP_SKIP_ROOTDICT*/
};

#endif
