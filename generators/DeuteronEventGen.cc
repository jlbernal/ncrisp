/* ================================================================================
 * 
 * 	Copyright 2013 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "DeuteronEventGen.hh"
using namespace std;
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(DeuteronEventGen);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

DeuteronEventGen::DeuteronEventGen(): EventGen(), channels(){
	
	channels.SetOwner();
	tcs = 0.;
}

//_________________________________________________________________________________________________										

DeuteronEventGen::~DeuteronEventGen(){

	channels.Delete();
}

//_________________________________________________________________________________________________										

Double_t DeuteronEventGen::TotalCrossSection(Dynamics& p1, Dynamics& p2){

	Double_t tcs1 = 0.;
  
	for ( Int_t i = 0; i < channels.GetEntriesFast(); i++ ) {
		tcs1 += ((BBChannel*)channels[i])->CrossSection().Calculate( &p1, &p2);
	}
	return tcs1;
}

//_________________________________________________________________________________________________										

Int_t DeuteronEventGen::GenerateSingleNucleon(Int_t ID, Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, TVector3& vecPos/*, TVector3& angMomentum*/){
  
  	// Initializing particle ...
	
	Double_t TKinetic = energy/2.;
	
	ParticleDynamics nucleon(ID);
	nucleon.SetMass( nucleon.GetInvariantMass() * CPT::effn );
	nucleon.BindIt(true);
	Double_t v0 = nuc.NuclearPotential(ID);
	Double_t qi = TMath::Sqrt( sqr( TKinetic + v0 + nucleon.GetMass() ) - sqr(nucleon.GetMass()) );
	nucleon.SetMomentum( TLorentzVector( qi, 0., 0., TKinetic + v0 + nucleon.GetMass() ) );
	
	//cout << "4-momemntum: " << endl;
	//nucleon.Momentum().Print();
	
	//TVector3 p = nucleon.Momentum().Vect();
	//cout << "momemntum: " << endl;
	//p.Print();
	
	TVector3 vNull;
	if(vecPos == vNull){ //on the first time it passes here 
	    vecPos = StartPosition(nuc);
	    nucleon.SetPosition( vecPos );
	  
	  //  angMomentum = vecPos.Cross(p);
	  //  cout << "Angular momemntum 1: " << endl;
	  //  angMomentum.Print();
	}
	else if(vecPos != vNull){ //on the second time it passes here
	    TVector3 vecPos2 = StartPosition2(nuc, vecPos);
	    nucleon.SetPosition( vecPos2 );
	  
	  //  angMomentum = vecPos2.Cross(p);
	   // cout << "Angular momemntum 2: " << endl;
	   // angMomentum.Print();
	}
	  
	// put the nucleon into the nucleus.
	Int_t n_add = nuc.CaptureNucleon(nucleon);
	// Calculate the total cross section for all nucleons, and select
	// the interaction channel.
	for ( Int_t i = 0; i < nuc.GetA() - 1; i++ ) {
		Double_t tcs = TotalCrossSection(nucleon, nuc[i]);
		Double_t b2 = impact_parameter2(nucleon, nuc[i]);
		if ( TMath::Pi() * b2 <= tcs* .1 ) {
			Double_t sum = 0.;
			Double_t x = gRandom->Uniform();
			for ( Int_t j = 0; j < channels.GetEntriesFast(); j++ ) { 	
				sum += ((BBChannel*)channels[j])->CrossSection().Value() / tcs;
				if ( x < sum ) {
					std::vector<ParticleDynamics> v;
					Double_t t = 0.;
					if (((BBChannel*)channels[j])->DoAction( i, n_add, &nuc, t, &mpool, &v)){
						nuc.SetMomentum(nuc.Momentum() + nucleon.Momentum());
						nuc.SetAngularMomentum(nuc.AngularMomentum() + nucleon.Position().Cross(nucleon.Momentum().Vect()));
						cout << "Channel: " << ((BBChannel*)channels[j])->CrossSection().GetName() << endl;
						return i;
					}else {
					  return -1;
					}
				}	
			}
		}    
	} // First 'for' closing brace.
	return -1;
}

//_______________________________________________________________________________________________________________

Int_t DeuteronEventGen::Generate(Double_t DeutEnergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char* opt, std::ostream *os) {
	
	Int_t interacProt = -1;
	Int_t interacNeut = -1;
	TVector3 vecP; //incident point of the first entering particle
	//TVector3 angM1, angM2, angMTotal;

	interacProt = this->GenerateSingleNucleon(CPT::proton_ID, DeutEnergy, nuc, mpool, vecP);
	interacNeut = this->GenerateSingleNucleon(CPT::neutron_ID, DeutEnergy, nuc, mpool, vecP);
	
// 	angMTotal = angM1 + angM2;
// 	Par[0] = angMTotal.Px();
// 	Par[1] = angMTotal.Py();
// 	Par[2] = angMTotal.Pz();
	//cout << "Total Angular Momentum: " << endl;
	//angMTotal.Print();
	
	if(interacProt >= 0 && interacNeut >= 0){
	  return 1;
	} 
	else if( ( interacProt == -1 && interacNeut >= 0 ) || ( interacProt >= 0 && interacNeut == -1 ) ){
	  return 1;
	}
	else if( interacProt == -1 && interacNeut == -1 ){
	  return -1;
	}
	return -1;
}

//_________________________________________________________________________________________________

Int_t DeuteronEventGen::Generate(Double_t energy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  return -1;
  
}


//_________________________________________________________________________________________________										

TVector3 DeuteronEventGen::StartPosition(NucleusDynamics& nuc){

	Double_t r = pow(gRandom->Uniform(),0.5) * nuc.GetRadium();
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z);
	Double_t x = - TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	
	return TVector3(x, y, z);
}

//_________________________________________________________________________________________________										

TVector3 DeuteronEventGen::StartPosition2(NucleusDynamics& nuc, TVector3 v1){

	Double_t theta0 = v1.Theta();
	Double_t phi0 = v1.Phi();
	Double_t rho = v1.Mag(); // nuclear radium
	
	Double_t Dalpha = TMath::ATan(CPT::r0/rho); //angular displacement inside a r0 distance from first particle incident point
	Double_t Dtheta = 0.;
	Double_t Dphi = 0.;
	
	while(Dtheta == 0. && Dphi == 0.){
	  Dtheta = gRandom->Uniform(-1.,1.) * Dalpha;
	  Dphi = gRandom->Uniform(-1.,1.) * Dalpha;
	}
	
	TVector3 v2;
	v2.SetMagThetaPhi(rho, theta0 + Dtheta, phi0 + Dphi);
	
	//cout << "v2: ";
	//v2.Print();
	//cout << endl;
	
	return v2;
}