/* ================================================================================
 * 
 * 	Copyright 2014 Israel Gonzalez, Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "UltraEventGen.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(UltraEventGen);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										
const Double_t hbar = 1., c = 1., e = 0.0854358;
const Double_t alpha = 1/137.034;
//const double limit = pow(10.,7);

UltraEventGen::UltraEventGen():EventGen(), channels(){
	channels.SetOwner();
	tcs = 0.;
	num_photons = 0;
	selected_ch = -1;
	selected_nucleon = -1;
	Q_2 = 0;
	PhotEnSpec = new TH1D("PhotEnSpec","Photon Energy Distribution",2500,0,500);
	PhotEnSpec->GetXaxis()->SetTitle("E_{#gamma} [GeV]");
	PhotEnSpec->GetYaxis()->SetTitle("N");
}

//_________________________________________________________________________________________________										

UltraEventGen::~UltraEventGen(){
	channels.Delete(); 
}

//_________________________________________________________________________________________________										

Measurement UltraEventGen::GenerateSinglePhoton( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, Int_t times, char *opt , std::ostream *os){
	if ( channels.IsEmpty() )
		return Measurement();
	int Nch = channels.GetEntriesFast();
	std::vector <double> VSig(Nch);	
	std::vector <double> VVal(Nch);	
	Double_t sf = (TMath::Pi() * sqr(nuc.GetRadium()) * 10000) / nuc.GetA();
	TString stropt(opt);  
	bool simple = ( stropt.CompareTo("simple") != 0 ) ? false : true;     
	Int_t count = 0;
	for ( Int_t i = 0; i < times; i++ ) {
		int ChNum = -1;
//		bool ini = true; //initial cicle
		std::cout << i << ": ";
		if ( simple ) {
			while( true ) {// resulting the resulting channel so far
				ChNum = this->GenerateSimple(energy, nuc, mpool);
				if (ChNum > 0) break;
				count++;
			}
			VVal[ChNum]++;
		} 
		else {      
			while( this->GenerateSinglePhoton(energy, nuc, mpool, opt) < 0 ) {// resulting the resulting interacting atom so far
				count++;
			}
		}
		nuc.DoInitConfig();
		mpool.clear();
	}
	Double_t cs = (Double_t)times/(Double_t)(count) * sf;
	for ( Int_t i = 0; i < Nch; i++ ) {
		VSig[i] = (Double_t)VVal[i]/(Double_t)(count) * sf;
	}  
	if ( simple ) {        
		if(os)
			(*os) << "energy = " << energy << "\tcount = " << count << "\tcs = " << cs;
	}
	return Measurement(cs, 0., VVal, VSig);
}

//_________________________________________________________________________________________________										

Int_t UltraEventGen::GenerateSimple( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool ){
	// initialize the photon ...
	ParticleDynamics gamma( CPT::photon_ID );
	gamma.SetMomentum( TLorentzVector(energy, 0., 0., energy) );    
	gamma.SetPosition(StartPosition(nuc));
  
	Double_t tcs = 0.;
	Double_t b2 = 0.;
	int idx = -1;
  
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		b2 = sqr( nuc[i].Position().Y() - gamma.Position().Y() ) + sqr( nuc[i].Position().Z() - gamma.Position().Z() );
		tcs = this->TotalCrossSection(&gamma, i, &nuc);    
		// .0001 is due to conversion: micro barn -> fm^2
		if ( TMath::Pi() * b2 <= tcs * .0001 ) {            
			idx = i;
			Double_t sum = 0.;      
			Double_t x = gRandom->Uniform();
			for ( Int_t j = 0; j < channels.GetEntriesFast() ; j++ ) {
				PhotonChannel* ch = ((PhotonChannel*)channels[j]);      
				sum += ch->CrossSection().Value() / tcs;      
				if ( x <= sum ) { 	  
					if ( ch->DoAction( &gamma, idx, &nuc, &mpool) ) {
						nuc.SetMomentum(nuc.Momentum() + gamma.Momentum());
						nuc.SetAngularMomentum(nuc.AngularMomentum() + gamma.Momentum().Vect().Cross(gamma.Position()));
						std::cout << " energy: " << energy << ", Selected : " << ch->CrossSection().GetName() <<  std::endl;
						return j;	  
					}else return -1;
				}	
			} // closing brace of 'for(...)'      
			// break;
		}
	}   
	return -1;  
}

//_________________________________________________________________________________________________										

Int_t UltraEventGen::GenerateSinglePhoton( Double_t energy, NucleusDynamics& nuc, MesonsPool& mpool, char *opt, std::ostream *os){
	selected_ch = -1;
	num_photons++;
	ParticleDynamics gamma( CPT::photon_ID );
	gamma.SetMomentum( TLorentzVector(energy, 0., 0., energy) );    
	gamma.SetPosition(StartPosition(nuc));
  
	TObjArray nucleons_arr;
	this->SelectNucleons(gamma, nuc, nucleons_arr);  
	if ( nucleons_arr.GetEntriesFast() == 0 ){ 
		return -1;
  	}
	TString stropt(opt);
//	std::cout << " array entries = " << nucleons_arr.GetEntriesFast()  << std::endl;
	for ( Int_t i = 0; i < nucleons_arr.GetEntriesFast() ; i++ ) {
		Int_t idx = ((CrossSectionData*)nucleons_arr[i])->GetIdx();
		Double_t total_cs = this->TotalCrossSection( &gamma, idx, &nuc);
//		std::cout  << "channel by channel ..... acepted nucleon[" << idx << "]'s total CS = " << total_cs << std::endl;
		Double_t sum = 0.;      
		Double_t x = gRandom->Uniform();
		for ( Int_t j = 0; j < channels.GetEntriesFast() ; j++ ) {
			PhotonChannel* ch = ((PhotonChannel*)channels[j]);      
//			std::cout << "channel[" << j << "] = " << ch->CrossSection().GetName() << ", cross section value = " << ch->CrossSection().Value() << std::endl;
			sum += ch->CrossSection().Value()/total_cs;      
			if ( x <= sum ) { 
//				std::cout << "quasi Selected : " << ch->CrossSection().GetName() << std::endl;  
				if ( ch->DoAction( &gamma, idx, &nuc, &mpool) ) {
					selected_ch = j; ch->AddCount();
					nucleons_arr.Delete();
//					if ( stropt.CompareTo("quiet") != 0 )
					std::cout << "Selected : " << ch->CrossSection().GetName() << std::endl;
					nuc.SetMomentum(nuc.Momentum() + gamma.Momentum());
					nuc.SetAngularMomentum(nuc.AngularMomentum() + gamma.Position().Cross(gamma.Momentum().Vect()));
//					if( os ) (*os) << ch->CrossSection().GetName();
					return j;	  
				} 
				else {
					ch->AddBlocked();
					break;
				}	
			}
		} // closing brace of 'for(...)'
	}
	nucleons_arr.Delete();
	return -1;
}
//_________________________________________________________________________________________________	


Int_t UltraEventGen::Generate(	Double_t CMenergy, NucleusDynamics& nuc, LeptonsPool& lpool, MesonsPool& mpool, std::vector<Double_t>& Par, char* opt, std::ostream *os){
  
  return -1;
}




//_________________________________________________________________________________________________


Double_t survival_probability( Double_t *x, Double_t *par ){
	
	double b = x[0];
	double Z_targ = par[0];
	double Z_proj = par[1];

	double sigNN = 3.9; //nucleon-nucleon total cross section (fm^2)
	
	//parameters for Pb
	double rho0 = 0.16; //(fm^-3)
	double R = 6.62; // (fm)
	double aPb = 0.546; // (fm)
	
	//double ftNucDensPb = 0.; //Fourier transform of nuclear density for Pb
	//double ftNucDensp = 0.; //Fourier transform of nuclear density for proton
	
	TF1 *J0_bessel = new TF1("J0","TMath::BesselJ0(x*[0])",0.,1000.); //ROOT::Math::cyl_bessel_j(0, x*[0])
	J0_bessel->SetParameter(0,b);
	
	double min = 0., max = 10.;
	double Chi = 0.;
	
	if(Z_targ == 82.){
		TF1 *ftTarg = new TF1("ftTarg","((4.*TMath::Pi()*[0])/(x*x*x)) * ( sin(x*[1]) - x*[1]*cos(x*[1]) ) * ( 1./(1.+x*x*[2]*[2]) )",min,max);
		ftTarg->SetParameter(0, rho0);
		ftTarg->SetParameter(1, R);
		ftTarg->SetParameter(2, aPb);
		
		if(Z_proj == Z_targ){
			
			TF1 *integrand = new TF1("integrand","x*ftTarg*ftTarg*J0",min,max);
			double INT = integrand->Integral(min,max);
			Chi = (sigNN/(4.*TMath::Pi())) * INT;
			delete integrand;
			delete ftTarg;
			delete J0_bessel;
			//cout << "Chi: " <<  Chi << "  T: " << TMath::Exp(-2. * Chi) << endl;
			return TMath::Exp(-2. * Chi);
		}
		else if(Z_proj == 1.){
			
			double a2Prot = 0.71; //(fm^-2)
			
			TF1 *ftProt = new TF1("ftProt","1./(1.+(x*x)/([0]))",min,max);
			ftProt->SetParameter(0, a2Prot);
			
			TF1 *integrand = new TF1("integrand","x*ftTarg*ftProt*J0",min,max);
			double INT = integrand->Integral(min,max);
			Chi = (sigNN/(4.*TMath::Pi())) * INT;
			delete integrand;
			delete ftTarg;
			delete ftProt;
			delete J0_bessel;
			return TMath::Exp(-2. * Chi);
		}
	}
	else {
		cout << "Parameters for transmission probability not ye defined for this target. Assumed sharp cut-off on b_min." << endl;
		return 1.;
	}
	
	return 1.;
	
}

Double_t PhotonFluxFunction(Double_t *x, Double_t *par){
  
	Double_t b = par[0],
		 R_targ = par[1],
		 beta = par[2],
		 Z_proj = par[3];
				
  	Double_t arg = 1. - beta*beta,
		 gamma = 1./sqrt(arg); //relativistic factor
	
	Double_t xp = ( x[0]*(b+R_targ) ) / ( beta*c*gamma*hbar );
	Double_t xm = ( x[0]*(b-R_targ) ) / ( beta*c*gamma*hbar );
	
// 	Double_t K0p = ROOT::Math::cyl_bessel_k(0, xp ),
// 		 K0n = ROOT::Math::cyl_bessel_k(0, xm ),
// 		 K1p = ROOT::Math::cyl_bessel_k(1, xp ),
// 		 K1n = ROOT::Math::cyl_bessel_k(1, xm ),
// 		 K2p = ROOT::Math::cyl_bessel_k(2, xp ),
// 		 K2n = ROOT::Math::cyl_bessel_k(2, xm );
	Double_t K0p = TMath::BesselK(0, xp ),
		 K0n = TMath::BesselK(0, xm ),
		 K1p = TMath::BesselK(1, xp ),
		 K1n = TMath::BesselK(1, xm ),
		 K2p = TMath::BesselK(2, xp ),
		 K2n = TMath::BesselK(2, xm );

	Double_t f = ( ( 2.*pow(Z_proj*e, 2) ) / ( hbar*x[0]*TMath::Pi()*c ) ) *
			( 1./(beta*beta) ) *
			( (1./2.)*pow( x[0]/(beta*c*gamma*hbar) ,2) ) 
			*
			( pow(b+R_targ, 2) * ( K1p*K1p - K0p*K2p ) - 
			  pow(b-R_targ, 2) * ( K1n*K1n - K0n*K2n )
			  +
			  ( pow(b+R_targ, 2)/(gamma*gamma) ) * ( K0p*K0p - K1p*K1p ) -
			  ( pow(b-R_targ, 2)/(gamma*gamma) ) * ( K0n*K0n - K1n*K1n )   ); //function calculated in 2pi

	//ratio of the projection area of the target to the area of the ring between b-R and b+R
	Double_t ratio_NR = (1./4.)*(R_targ/b); 

	return ratio_NR*f;
}

//not currently being used
Double_t integrand(Double_t *x, Double_t *par){

	Double_t E = par[0],
		 b0 = par[1],
		 R_targ = par[2],
		 beta = par[3],
		 Z_proj = par[4];
		 
		 
	Double_t arg = 1. - beta*beta,
		 gamma = 1./sqrt(arg); //relativistic factor
	
	Double_t y = ( E*x[0] ) / ( beta*c*gamma*hbar );

	Double_t K1 = TMath::BesselK(1, y),
		 K0 = TMath::BesselK(0, y);
	
	Double_t f = alpha * pow(Z_proj/TMath::Pi(), 2) * pow(y/(beta*x[0]), 2) * (1./E) * ( K1*K1 + (1./(gamma*gamma))*K0*K0 );
	
	double cos = (x[0]*x[0] + b0*b0 - R_targ*R_targ) / (2.*x[0]*b0);
	
	Double_t INT = 2.*x[0]*f*TMath::ACos(cos);
	
	return INT;
}

//not currently being used
Double_t spectro(Double_t *x, Double_t *par){
	
	double E = x[0];
	double b0 = par[0];
	double R_targ = par[1];
	double beta = par[2];
	double Z_proj = par[3];

	TF1 *spec = new TF1("spec",integrand,0,100,5);
	spec->SetParameters(E,b0,R_targ,beta,Z_proj);
	
	double N = spec->Integral(b0-R_targ,b0+R_targ);
	return N;
}
/*
//_________________________________________________________________________________________________	
Double_t PhotonFluxFunction(Double_t *x, Double_t *par){

	Double_t	gamma_L = 1/sqrt(1 - par[0]*par[0]);

	Double_t z = ( x[0]*par[1] )/( par[0]*c*gamma_L*hbar );
	Double_t K0 = ROOT::Math::cyl_bessel_k(0, z ),
				K1 = ROOT::Math::cyl_bessel_k(1, z );

	Double_t f = ((2.*hbar)/(TMath::Pi()*x[0]))*((par[2]*par[2]*alpha)/(par[0]*par[0]))*(z*K0*K1-((par[0]*par[0]*z*z)/2.)*(K1*K1 - K0*K0));

	return f;
}
//_________________________________________________________________________________________________										
*/

Int_t UltraEventGen::Generate( Double_t CMenergy, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<Double_t> &Par, char *opt, std::ostream *os){
	
	//Double_t b = Par[2]*(1./197.); //impact parameter of the reaction (MeV^(-1))
	//Int_t A_proj = (Int_t)Par[0], Z_proj = (Int_t)Par[1]; //Mass and charge of projectile nucleus
	Int_t A_proj = nuc.GetA(), Z_proj = nuc.GetZ(); //Mass and charge of projectile nucleus
	Double_t R_targ = CPT::r0*pow(nuc.GetA(),1./3.); //target nuclear radius (fm)
	Int_t Z_targ = nuc.GetZ(); //Charge of the target
	Double_t R_proj = CPT::r0*pow(A_proj,1./3.); //nuclear radius (fm) of projectile nucleus
	Double_t bmin = R_targ+R_proj; //minimum impact parameter (fm)
	Double_t bmax = 102.; //maximum impact parameter (fm)
	Double_t b = 0.;
	
	//cout << "R_targ+R_proj: " << R_targ+R_proj << endl;
	
// 	if(b <= bmin){
// 		cout << "WARNING!! - impact parameter smaller than or equal to minimum. Collision may not be ultra-peripheral" << endl;
// 		cout << "b: " << Par[2] << "  bmin: " << CPT::r0*pow(nuc.GetA(),1./3.) + CPT::r0*pow(A_proj,1./3.) << endl;
// 		cout << "Verifying survival probability of nuclei..." << endl;
// 	}
	
	TF1 *T = new TF1("T",survival_probability,0.,100.,2);
	T->SetParameters(Z_targ, Z_proj);
	double transP = 1.;
	while(transP > 10e-4){
		transP = T->Eval(bmin);
		bmin = bmin - 1.;
	}
	//cout << "b_min: " << bmin << endl;
	
	Double_t s = CMenergy * CMenergy;
	Double_t mp = CPT::p_mass;
	Double_t E = ( ( s - 2.*nuc.GetA()*A_proj*mp*mp )*c*c ) / ( 2.*nuc.GetA()*mp ); //projectile energy on target reference
	Double_t p = pow( (E*E)/(c*c) - (A_proj*A_proj*mp*mp)*c*c, 1./2. ); //projectile momentum on target reference
	Double_t beta = p/E;
	Double_t gamma_L = 1./sqrt(1. - beta*beta);
	Double_t xmin = 5., 
		 xmax = ( gamma_L*hbar*beta*c )/(bmax*(1./197.));/*upper energy limit in the photon flux (MeV) 
								in the rest frame of the target*/

	TF1 *f1 = new TF1("N1",PhotonFluxFunction,xmin,xmax,4);
	f1->SetParameters(bmax*(1./197.),R_targ*(1./197.),beta,Z_proj);
	
	while(f1->Integral(xmin,xmax) < 1.){
		bmax = bmax - 1.; //fm
		xmax = ( gamma_L*hbar*beta*c )/(bmax*(1./197.));
		f1->SetRange(xmin, xmax);
		f1->SetParameters(bmax*(1./197.),R_targ*(1./197.),beta,Z_proj);
	}
	
	//cout << "b_max: " << bmax << endl;
	
// 	if(transP < 10e-4) transP = 0.;
// 	
// 	if( transP <= 0 ){
// 		cout << endl << "WARNING!! Collision not ultra-peripheral - no chance of survival of nuclei!  T = " << T->Eval(Par[2]) << endl;
// 		cout << endl << "\t\t\t==========================================================" << endl;
// 		cout << endl << "\t\t\tABORT SIMULATION, increase impact parameter and try again!" << endl;
// 		cout << endl << "\t\t\t==========================================================" << endl;
// 		delete T;
// 		return -1;
// 	}
	double r = 1.;
	transP = 0.;
	double r2 = 0.;
	while(r > transP){
		r2 = gRandom->Uniform(bmin*bmin, bmax*bmax);
		b = TMath::Sqrt(r2);
		transP = T->Eval(b);
		r = gRandom->Uniform();
	}
// 	if( r > transP ){
// 		cout << "NOTIFICATION: Collision not ultra-peripheral!! T = " << transP << " Going on to the next event..." << endl;
// 		delete T;
// 		return -1111;
// 	}
// 	else if( r <= transP ){
		
	// 	if( xmax > limit ){
	// 		  cout << xmax << endl;
	// 		  xmax = limit; //overflow in this function produces underflow_error (c++ exception) on GSL
	// 		  cout << "UNDERFLOW!!!!!!!!!!!!!!!!!!!" << endl;
	// 	}
		
	//TF1 *f1 = new TF1("N1",PhotonFluxFunction,xmin,xmax,4);
	xmax = ( gamma_L*hbar*beta*c )/(b*(1./197.));
	f1->SetRange(xmin, xmax);
	f1->SetParameters(b*(1./197.),R_targ*(1./197.),beta,Z_proj);
		
		//TF1 *f1 = new TF1("N1",spectro,xmin,xmax,4);
		//f1->SetParameters(b,R_targ,beta,Z_proj);
	
	// 	Int_t np = 1000;
	// 	double *xf=new double[np];
	// 	double *wf=new double[np];
	// 	f1->CalcGaussLegendreSamplingPoints(np,xf,wf,1e-15);
	//	Int_t NumPho = (Int_t)f1->IntegralFast(np,xf,wf,0,5);
	Int_t NumPho = 1;
	double TOTAL = f1->Integral(xmin,xmax);
	if(TOTAL > 1.) NumPho = TMath::Nint(TOTAL);
		//std::cout << "Number of virtual photons = " << NumPho << std::endl;
		//f1->SetNpx(1000);
		//cout << "NumPho: " << NumPho << endl;

	Bool_t UltraPass = false;
	Double_t totalPhotE = 0.;
	Int_t passPhot = 0;
	for ( Int_t i = 0; i < NumPho; i++ ) {
		Double_t PhotonEne = f1->GetRandom();
		Int_t IntChn = this->GenerateSinglePhoton( PhotonEne, nuc, mpool);
		if (IntChn >= 0) {
			PhotEnSpec->Fill(PhotonEne/1000.); //recording in GeV
			std::cout << "Total Number of photons: " << NumPho << " - Virtual photon [" << i << "] absorbed! Energy = " << PhotonEne << std::endl;
			UltraPass = true;
			passPhot++;
			totalPhotE += PhotonEne;
		}
	}
	Par[0] = NumPho;
	Par[1] = passPhot;
	Par[2] = totalPhotE;
	Par[3] = b;
	delete f1;
	delete T;
	// 	delete[] xf;
	// 	delete[] wf;
	return (UltraPass ? 1 : -1);
}

//_________________________________________________________________________________________________										

void UltraEventGen::SelectNucleons( ParticleDynamics& gamma, NucleusDynamics& nuc, TObjArray& csData ){ //using the total cross section for nucleon selection
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		Double_t b2 = sqr( nuc[i].Position().Y() - gamma.Position().Y() ) + sqr( nuc[i].Position().Z() - gamma.Position().Z() );
		Double_t W = (gamma.Momentum() + nuc[i].Momentum()).M() / 1000.; //GeV
		Double_t gn = 0;
		Double_t gn_Res = 0;
		Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
		Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
		Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
		Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
		Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
		Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };
		gn_Res = gn_Res + p33Formation ( &gamma, &nuc[i], p33_params );  /// Delta(1232) P33
		gn_Res = gn_Res + p11Formation ( &gamma, &nuc[i], p11_params );  /// N(1440)     P11
		gn_Res = gn_Res + d13Formation ( &gamma, &nuc[i], d13_params );  /// N(1520)     D13
		gn_Res = gn_Res + f15Formation ( &gamma, &nuc[i], f15_params ); /// N(1680)     F15
		gn_Res = gn_Res + s11Formation ( &gamma, &nuc[i], s11_params );  /// N(1535)     S11
		gn_Res = gn_Res + f37Formation ( &gamma, &nuc[i], f37_params );  /// N(1950)     F37
		gn_Res = gn_Res + qdCrossSection( &gamma, &nuc, f37_params ); // reference at: Rossi P. Phys Rev C 40(1989)2412
		if ( nuc[i].PdgId() == CPT::neutron_ID ) {
			Double_t nMass = (CPT::n_mass*CPT::effn)/1000.; //mass in GeV
			Double_t E =  W*W/nMass/2. - nMass/2;
			Double_t bn1 = 87., bn2 = 65.;
			Double_t x = -2. * ( E - 0.139 );      
			gn = ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
			gn = gn + gn_Res;
		}
		if ( nuc[i].PdgId() == CPT::proton_ID ) {
			Double_t pMass = (CPT::p_mass*CPT::effn)/1000.; //mass in GeV
			Double_t E = W*W/pMass/2 - pMass/2;
			Double_t bp1 = 91., bp2 = 71.4; 
			Double_t x = -2. * ( E - 0.139 );      
			gn = ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );
			gn = gn + gn_Res;
		}
//		Double_t gn = this->TotalCrossSection(&gamma, i, &nuc);
		// .0001 is due to conversion: micro barn -> fm^2
		if ( TMath::Pi() * b2 <= gn * .0001 ) {     
			//cout << " W = " << W << ", nuc[i].Momentum().M() = " << nuc[i].Momentum().M() << ", selection gn = " << gn << endl;
			csData.Add(new CrossSectionData(i, b2, gn));
		}
	}
}

//_________________________________________________________________________________________________										

TVector3 UltraEventGen::StartPosition(NucleusDynamics& nuc){

	Double_t r = pow(gRandom->Uniform(),0.5) * nuc.GetRadium();
	Double_t theta = TMath::TwoPi() * gRandom->Uniform();  
	Double_t z = r * TMath::Cos(theta);
	Double_t y = r * TMath::Sin(theta);  
	Double_t r2 = sqr(y) + sqr(z);
	Double_t x = TMath::Sqrt(sqr(nuc.GetRadium()) - r2);
	return TVector3(x, y, z);
}

//_________________________________________________________________________________________________										

