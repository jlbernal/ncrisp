/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Meson_n_cross_sections.hh"


Double_t Breit_Wigner( Double_t sqrt_s, Double_t* p ){
	Double_t mass_adjust = p[0];
	Double_t Gamma_0 = p[1];  
	Double_t sigma0 = p[2];
	Double_t d = ( TMath::Power( TMath::Power(sqrt_s, 2) - TMath::Power(mass_adjust,2), 2) + TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2) );
	return sigma0 * (TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2))/d; 
}



// Deg1:Np_No:No_Np:No_Nr:Nr_No:No_els:No_N2p:Np_No1:Np_Nh:Np_Nr:No_Np1:Nh_Np:Nr_Np
double bb_elastic_cross_section( Dynamics* q1 , Dynamics* q2, double* cch );

/*
 * NOTE: This p33 formation cross section was already replaced by a better model below. It is going to be kept here for some time though.
 */
Double_t pion_n_p33_formation ( Dynamics* pion, Dynamics* nucleon, Double_t* ){
	Int_t m_id = pion->PdgId();
	Int_t n_id = nucleon->PdgId();
	Double_t sigma_delta_max = 0.;
	double result;
	if ( ( (m_id == CPT::pion_m_ID) && (n_id == CPT::neutron_ID) ) || ( (m_id == CPT::pion_p_ID) && (n_id == CPT::proton_ID ) )    ){ 

		// Cross Sections for:  pi- n || pi+ p
		sigma_delta_max = 200.;  
	} else 	if ( ((m_id == CPT::pion_0_ID) && (n_id == CPT::neutron_ID) ) || ((m_id == CPT::pion_0_ID) && (n_id == CPT::proton_ID)  )   ){
		sigma_delta_max = 135.;  
  	} else return 0;
	const Double_t em_delta = 1232.;
	const Double_t width_delta = 120.;
	// w is the formed ressonance momentum.
	TLorentzVector w = pion->Momentum() + nucleon->Momentum();
	Double_t s = w.M2(); 
	Double_t sqrts_dp = sqrt(s) ;  
	Double_t wd4 = 0.25 * sqr(width_delta);
	result = sigma_delta_max * wd4 / ( sqr(sqrts_dp - em_delta) + wd4) /1000.;
//	cout << "double result = " << result << endl;
//	return 0;
	return result;
}

///Delta 1232
Double_t piN_Delta1232(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + p || pi0 + n
		if( W/1000. < 3. ){ //GeV			
			double par[3] = {1232., 117., 60}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi- + p || pi- + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1232., 117., 60}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if(pi_ID == CPT::pion_p_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){
		// pi+ + p || pi+ + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1232., 117., 200}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///Delta 1700
Double_t piN_Delta1700(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if(pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){
		// pi0 + p || pi0 + n	
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1670., 200., 2}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if(pi_ID == CPT::pion_m_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){
		// pi- + p || pi- + n	
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1670., 200., 2}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if(pi_ID == CPT::pion_p_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){
		// pi+ + p || pi+ + n	
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1670., 200., 9}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///N 1440
Double_t piN_N1440(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + n || pi0 + p
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1430, 250, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_p_ID && N_ID == CPT::neutron_ID ){ 
		// pi+ + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1430, 250, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && N_ID == CPT::proton_ID ){ 
		// pi- + p
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1430, 250, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///N 1520
Double_t piN_N1520(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + n || pi0 + p
		if( W/1000. < 3. ){ //GeV		
			double par[3] = {1515, 115, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_p_ID && N_ID == CPT::neutron_ID ){ 
		// pi+ + n
		if( W/1000. < 3. ){ //GeV			
			double par[3] = {1515, 115, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && N_ID == CPT::proton_ID ){ 
		// pi- + p
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1515, 115, 10}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///N 1535
Double_t piN_N1535(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;

	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + n || pi0 + p
		if( W/1000. < 3. ){ //GeV			
			double par[3] = {1535, 125, 15}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_p_ID && N_ID == CPT::neutron_ID ){ 
		// pi+ + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1535, 125, 15}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && N_ID == CPT::proton_ID ){ 
		// pi- + p
		if( W/1000. < 3. ){ //GeV			
			double par[3] = {1535, 125, 15}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///N 1680
Double_t piN_N1680(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + n || pi0 + p
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1685, 130, 45}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_p_ID && N_ID == CPT::neutron_ID ){ 
		// pi+ + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1685, 130, 45}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && N_ID == CPT::proton_ID ){ 
		// pi- + p
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1685, 130, 45}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

///Delta 1950
Double_t piN_Delta1950(Dynamics* pion, Dynamics* nucleon, Double_t*){

	int pi_ID = pion->PdgId();
	int N_ID = nucleon->PdgId(); 
	double s = (pion->Momentum() + nucleon->Momentum()).M2();
	double W = sqrt(s); //MeV
	double CS = 0.;
	
	if( pi_ID == CPT::pion_0_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi0 + p || pi0 + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1930, 335, 32}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_m_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi- + p || pi- + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1930, 335, 32}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else if( pi_ID == CPT::pion_p_ID && ( N_ID == CPT::proton_ID || N_ID == CPT::neutron_ID ) ){ 
		// pi+ + p || pi+ + n
		if( W/1000. < 3. ){ //GeV
			double par[3] = {1930, 335, 38}; //mass, width and sigma0
			CS = Breit_Wigner(W, par);
			return CS > 0 ? CS : 0; //milibarn
		}
		else return 0.;
	}
	else return 0.;
}

//_________________________________________________________________________________________________										
Double_t rho_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./sqrt(2); 
	double C = NC*NV*NC*NV;
	bool Part_Accept = false;
	if ( (CPT::IsRho(mes->PdgId() ) ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) ) Part_Accept = true;
	Double_t W = (mes->Momentum() + nucleon->Momentum()).M() / 1000.;
	double 	s = W*W*1e6;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in rho_elastic_cross_section(), (T < 0 )\n T = %f\n", T); 
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}
	if ( (W > 1.745) && (Part_Accept)){
		if (W < 2000 ){
			Double_t CS = CPT::Instance()->GetT_rho()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}

//_________________________________________________________________________________________________										

Double_t omega_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){

	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3./sqrt(2); 
	double C = NC*NV*NC*NV;
	bool Part_Accept = false;
	if ( (CPT::IsOmega(mes->PdgId())) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) ) Part_Accept = true;
	Double_t W = (mes->Momentum() + nucleon->Momentum()).M() / 1000.;

	double 	s = W*W*1e6;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in omega_elastic_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}
	if ( (W > 1.745) && (Part_Accept)){
		if (W < 2000 ){
			Double_t CS = CPT::Instance()->GetT_omg()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}

//_________________________________________________________________________________________________										
Double_t phi_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){

	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3.; 
	double C = NC*NV*NC*NV;
	bool Part_Accept = false;
	if ( (CPT::IsPhi(mes->PdgId())) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	Double_t W = (mes->Momentum() + nucleon->Momentum()).M() / 1000.;
	double 	s = W*W*1e6;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in phi_elastic_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}
	if ( (W > 2.0132) && (Part_Accept)){
		if (W < 2000 ){
			Double_t CS = CPT::Instance()->GetT_phi()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}


//_________________________________________________________________________________________________
Double_t JPsi_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 2./3.; 
	double C = NC*NV*NC*NV;
	bool Part_Accept = false;
	if ( (CPT::IsJ_Psi(mes->PdgId())) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	Double_t W = (mes->Momentum() + nucleon->Momentum()).M() / 1000.;
	double 	s = W*W*1e6;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in JPsi_elastic_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}
	if ( (W > 4.0396) && (Part_Accept)){
		Double_t diffCS = ( 23.15*TMath::Power(s, 0.16) + 0.034*TMath::Power(s, 0.88) + 1.49*TMath::Power(s, 0.52))/(-1.64 + 0.83*TMath::Log(s));
		return diffCS/C/10.;
	} else return 0;
}

//_________________________________________________________________________________________________
Double_t JPsi_elastic_cross_sectionVDM(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 2./3.; 
	double C = NC*NV*NC*NV;
	bool Part_Accept = false;
	if ( (CPT::IsJ_Psi(mes->PdgId())) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	Double_t W = (mes->Momentum() + nucleon->Momentum()).M() / 1000.;
	double 	s = W*W*1e6;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in JPsi_elastic_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}
	if ( (W > 4.0396) && (Part_Accept)){
		if (W < 2000 ){
			Double_t CS = CPT::Instance()->GetT_J_Psi()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}

//_________________________________________________________________________________________________
Double_t PionN_OmegaN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000;  // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in PionN_OmegaN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::pion_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 2.97345)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega()->Eval(s);
//		std::cout << " PionN_OmegaN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}

Double_t OmegaN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000;  // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in OmegaN_PionN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if  ( (mes->PdgId() == CPT::omega_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 2.96603)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion()->Eval(s);
//		std::cout << " OmegaN_PionN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t OmegaN_RhoN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in OmegaN_RhoN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::omega_ID )&&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) ) Part_Accept = true;
	if ((s > 2.96902)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nomega_Nrho()->Eval(s);
//		std::cout << " OmegaN_RhoN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t RhoN_OmegaN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in RhoN_OmegaN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::rho_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 2.96843)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nrho_Nomega()->Eval(s);
//		std::cout << " RhoN_OmegaN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t OmegaN_elastic_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000;// MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in OmegaN_elastic_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::omega_ID )&&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) ) Part_Accept = true;
	if ((s > 2.96603)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nomega_elast()->Eval(s);
//		std::cout << " OmegaN_elastic, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_2PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in OmegaN_2PionN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::omega_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 2.96615)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nomega_N2pion()->Eval(s);
//		std::cout << " OmegaN_2PionN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
/*************************************************
Ressonace Model
*************************************************/
Double_t PionN_OmegaN1_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){ 
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000;// MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in PionN_OmegaN1_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::pion_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 2.96603)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega1()->Eval(s);
//		std::cout << " PionN_OmegaN1, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t PionN_PhiN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000;// MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in PionN_PhiN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::pion_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 3.8434)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Npion_Nphi()->Eval(s);
//		std::cout << " PionN_PhiN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t PionN_RhoN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in PionN_RhoN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::pion_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_0_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::pion_0_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 2.94142)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Npion_Nrho()->Eval(s);
//		std::cout << " PionN_RhoN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t OmegaN_PionN1_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in OmegaN_PionN1_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::omega_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 2.96624)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion1()->Eval(s);
//		std::cout << " OmegaN_PionN1, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t PhiN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in PhiN_PionN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::phi_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 3.8372)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nphi_Npion()->Eval(s);
//		std::cout << " PhiN_PionN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t RhoN_PionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in RhoN_PionN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::rho_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_0_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_0_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ((s > 2.94163)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_Nrho_Npion()->Eval(s);
//		std::cout << " RhoN_PionN, s = " << s << ", cross section = " << CS*1000 << std::endl;
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t RhoN_mPionN_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){ //"mPion" means >= 2 pions
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in RhoN_mPionN_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::rho_p_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_0_ID )&&( nucleon->PdgId() == CPT::neutron_ID ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_m_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	if ( (mes->PdgId() == CPT::rho_0_ID )&&( nucleon->PdgId() == CPT::proton_ID  ) ) Part_Accept = true;
	
	double W = 0., CS = 0., RhoN_PionN_cs = 0., RhoN_OmegaN_cs = 0., rho_elastic_cs = 0.;
	
	if ((s > 4.)&&(Part_Accept)){
		W = sqrt(s); //GeV
		CS = 80.5807*pow(W,-0.597137) + 9.49625*pow(W,0.326840); //milibarn
		
		RhoN_PionN_cs = RhoN_PionN_cross_section(mes, nucleon, cch); 
		RhoN_OmegaN_cs = RhoN_OmegaN_cross_section(mes, nucleon, cch);
		rho_elastic_cs = rho_elastic_cross_section(mes, nucleon, cch);
		
		CS = CS - RhoN_PionN_cs - RhoN_OmegaN_cs - rho_elastic_cs;
		//std::cout << "CS Rho0N -> mPionN: " << CS << "mb" << std::endl;		
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_ND_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in NJPsi_ND_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::J_Psi_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 17.3)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_NJPsi_ND()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_NDast_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in NJPsi_ND_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::J_Psi_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 18.5)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDast()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_NDDbar_cross_section(Dynamics* mes, Dynamics* nucleon, Double_t* cch){
	TLorentzVector P = mes->Momentum() + nucleon->Momentum();
	double s = P.Mag2()/1000/1000; // MeV^2 -> GeV^2
//	const double VMDs = 100./3000.;
	double m1 = mes->GetMass(), m2 = nucleon->GetMass();
	// Kinetic energy in the lab frame.
	double T = (s*1e6 - sqr(m1 + m2))/(2. * m1);
	if ( fabs(T) < 1.E-7 )
		return 0.;
	if ( T < 0 ) {      
		TString msg = Form("Error in NJPsi_ND_cross_section(), (T < 0 )\n T = %f\n", T);            
		msg += mes->ToString();
		msg += nucleon->ToString();
		throw( BasicException(msg));
	}

	bool Part_Accept = false;
	if ( (mes->PdgId() == CPT::J_Psi_ID ) &&( ( nucleon->PdgId() == CPT::neutron_ID) || ( nucleon->PdgId() == CPT::proton_ID)) )Part_Accept = true;
	if ((s > 21.863)&&(Part_Accept)){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDDbar()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
