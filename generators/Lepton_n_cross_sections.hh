/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __Lepton_n_cross_sections_pHH
#define __Lepton_n_cross_sections_pHH

#include "CrispParticleTable.hh"
#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "base_defs.hh"
#include "BasicException.hh"
#include "CrispParticleTable.hh"
#include "TDecayChannel.h"
#include "TDatabasePDG.h"
#include "resonance_mass.hh"


//

//______________________________________________________________________									
// nu stands as neutrino 

const double factor =  1; //to let crosssections in microbarn  final use imporatan 1.E-8


//CCqe 
Double_t nun_mump_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);


// CCres
Double_t nun_mump33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result muon + delta plus 
Double_t nun_mump33p_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result muon + delta plus
Double_t nup_mump33pp_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result muon + delta plus plus 

// NCres
Double_t nun_nup330_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result neutrino + delta 0
Double_t nun_nup330_pim_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch); //result neutrino + delta 0
Double_t nup_nup33p_pi0_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);//result neutrino + delta plus
Double_t nup_nup33p_pip_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);//result neutrino + delta plus


// Nce
Double_t nun_elastic_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);
Double_t nup_elastic_cross_section(Dynamics* lep, Dynamics* nucleon, Double_t* cch);

// helpers

double ps_int(double E, double m );


#endif
