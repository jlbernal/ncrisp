
{
/*
scrip for integrate every channel by angle and momentum
*/
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");

	DataHelper_Hyp *dhh;
	char *c[6];
	c[0] = "n1"; 
	c[1] = "n2"; 
	c[2] = "nn"; 
	c[3] = "p1";
	c[4] = "p2";
	c[5] = "np";
	Double_t Ip, I, Inn, Inp;
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";


	for (Int_t m = 0; m < 4; m++) {
		if (m >= 2){
			c[0] = "p1";
			c[1] = "p2";
			c[2] = "np";
			c[3] = "n1"; 
			c[4] = "n2"; 
			c[5] = "nn"; 
		}
//		dhh = new DataHelper_Hyp("data/hyperon/C12/C12_p2_theta_1.out","P2:Cos:n1:n2:nn:p1:p2:np");
		dhh = new DataHelper_Hyp(FileName[m],FileCode[m]);
		dhh->ReadData();
		std::cout << "DATA ************" << FileName[m] << "****************\n";
		for (Int_t n = 0; n < 6; n++) {
			I = 0.0;
			for (Int_t i = 0; i < 22; i++) {
				Ip = 0;
				for (Int_t j = 0; j < 101; j++)
					Ip = Ip + dhh->Get_tuple_1Value(c[n], 101 * i + j) * 0.02;
				I = I + Ip * 25 ;
			}
			I *= TMath::TwoPi();
			if (n == 2) Inn = I;
			if (n == 5){
				Inp = I;
				std::cout  << "np = " << " = " << Inp  << ", nn = " << Inn << ";" << std::endl << "// the razon nn/np = " << Inn/Inp << std::endl;
			}
			if (n == 0) std::cout << "SWAVE_NEUTRON_PROBABILITY =  " << I << ";" << std::endl;
			if (n == 1) std::cout << "PWAVE_NEUTRON_PROBABILITY =  " << I << ";" << std::endl;
			if (n == 3) std::cout << "SWAVE_PROTON_PROBABILITY =  " << I << ";" << std::endl;
			if (n == 4) std::cout << "PWAVE_PROTON_PROBABILITY =  " << I << ";" << std::endl;
		}
		std::cout << "\n\n"; 
	}
	delete dhh;
	delete FileName;
	delete FileCode;
	delete c;
}
