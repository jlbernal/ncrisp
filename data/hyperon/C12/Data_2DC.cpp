
{
/*
*	Scrip for get data from the file
	Is very important to point out that:
		it must be no space before the # symbol for comment line
		it must be no blank line between data
*
*/
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");
	char *c[6];
	c[0] = "n1"; 
	c[1] = "n2"; 
	c[2] = "nn"; 
	c[3] = "p1";
	c[4] = "p2";
	c[5] = "np";
	Double_t Ip, I, Inn, Inp;
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";

	dhh = new DataHelper_Hyp(FileName[1],FileCode[1]);
	dhh->ReadData();
	const char *br="p1";
	TGraph2D* t = dhh->GetTGraph2D("P2","Cos",br);

/*	const char *br1="p1";
	TGraph2D* t1 = dhh->GetTGraph2D("P2","Cos",br1);

	const char *br2="p2";
	TGraph2D* t2 = dhh->GetTGraph2D("P2","Cos",br2);
*/

	t->SetTitle(br);
	t->SetMarkerSize(0.5);
//	t1->SetMarkerSize(0.5);
//	t2->SetMarkerSize(0.5);
//	t2->SetMarkerStyle(21);
//	t2->SetMarkerStyle(24);
    	t->GetXaxis()->SetTitle("P (MeV)");
	t->GetYaxis()->SetTitle("Cos (#Theta)");
       	t->GetZaxis()->SetTitle("Prob");
	t->GetXaxis()->CenterTitle();
	t->GetYaxis()->CenterTitle();
	t->GetZaxis()->CenterTitle();

	TPostScript myps("np.eps",113);
//	myps.Range(xsize,ysize);
	t->Draw("p0");
//	t1->Draw("samep0");
//	t2->Draw("samep0");
	myps.Close();
	delete dhh;
}
