{
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");
	DataHelper_Hyp *DhhCos = new DataHelper_Hyp("data/hyperon/C12/C12_CosTheta_0.out","Cos:n1:n2:p1:p2:nn:np");
	DhhCos->ReadData();
//	DhhCos->PrintAll_Tuple();

	TNtuple *t = DhhCos->GetTNtuple();
//	t->Print();
	const char* Branchx = "Cos";
	const char* Branchy1 = "n2";
	const char* Branchy2 = "n1";
	t->Show(25);
	Float_t x,y;
	t->SetBranchAddress(Branchx, &x);
	t->SetBranchAddress(Branchy1, &y);
	t->GetEntry(25);
//	t->Show(25);
	std::cout << "los valores que entran al tuple para o branch " << Branchy1  << " são x = " << x << ", y  " << y << std::endl;

	Float_t x1,y1;
	t->SetBranchAddress(Branchx, &x1);
	t->SetBranchAddress(Branchy2, &y1);
	t->GetEntry(25);
//	t->Show(25);
	std::cout << "los valores que entran al tuple para o branch " << Branchy2  << " são x1 = " << x1 << ", y1  " << y1 << std::endl;
//	t->Show(25);

/*	TGraphErrors *t1,*t2,*t3,*t4; 
	t1 = DhhCos->GetTGraphErrorsN("Cos","n1",0,1);
	t2 = DhhCos->GetTGraphErrorsN("Cos","n2",0,1);
	DhhCos->Print1_Tuple(100);
	t3 = DhhCos->GetTGraphErrorsN("Cos","p1",0,1);
	t4 = DhhCos->GetTGraphErrorsN("Cos","p2",0,1);
	TCanvas *c10 = new TCanvas("c10","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c10->SetFrameFillColor(10);
	c10->SetFillColor(10);

	t1->SetLineColor(1);
	t2->SetLineColor(2);
	t3->SetLineColor(3);
	t4->SetLineColor(4);

	t3->Draw("ALP");
	t2->Draw("LPsame");
	t4->Draw("LPsame");
	t1->Draw("LPsame");
	TLegend *legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(t1,"t1","L");
	legend->AddEntry(t2,"t2","L");
	legend->AddEntry(t3,"t3","L");
	legend->AddEntry(t4,"t4","L");
	legend->Draw("same");
	c10->SaveAs("SinglePartProtonBfIPSM.eps");
*/
}
