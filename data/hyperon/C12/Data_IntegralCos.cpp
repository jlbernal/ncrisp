
{
/*
scrip for integrate every channel by angle and momentum
*/
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");

	DataHelper_Hyp *dhh;
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";

	dhh = new DataHelper_Hyp(FileName[1],FileCode[1]);
	dhh->ReadData();
	const char* c = "nn";
	Double_t Ip;
	TGraphErrors* t = new 	TGraphErrors(22);
	for (Int_t i = 0; i < 22; i++) {
		Ip = 0;
		for (Int_t j = 0; j < 101; j++)
			Ip = Ip + dhh->Get_tuple_1Value(c, 101 * i + j) * 0.02;
		Double_t valor = Ip*TMath::TwoPi();
		cout << "valor with no rec = " << valor << endl;
		t->SetPoint(i,i*25,valor) ;
	}
	t->Draw("ALP");
	delete dhh;
	delete FileName;
	delete FileCode;
}
