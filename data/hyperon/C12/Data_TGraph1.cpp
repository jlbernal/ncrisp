
{
/*
scrip for integrate every channel by angle and momentum
*/
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");

	DataHelper_Hyp *dhh;
	DataHelper_Hyp *dhh1;
	TLegend *legend;

	cout << "Entre archivo caso: ";
	Int_t cas;
	cin >> cas;
	Double_t Ip, I, Inn, Inp;
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";

	dhh = new DataHelper_Hyp(FileName[cas],FileCode[cas]);
	dhh->ReadData();
//	dhh->PrintAll_Tuple();
	Int_t bchini = 0 ;
	const char *banch= "p1";
	TGraphErrors* t = dhh->GetTGraphErrorsN("P2", banch, bchini, 101);
	t->SetTitle(banch);

	t->SetLineColor(1);
	t->SetLineWidth(2);

	TCanvas *c1 = new TCanvas("c1","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);

	t->GetXaxis()->SetTitle("p(MeV/c)");
	t->GetYaxis()->SetTitle("S_{N}");
	t->SetTitle("Momenta Distribuition S_{N}|_{cos #Theta = -1}");
	t->Draw("ALP");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(t,banch,"L");
	legend->Draw("same");
}
