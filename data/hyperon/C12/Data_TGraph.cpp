
{
/*
scrip for integrate every channel by angle and momentum
*/
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");

	DataHelper_Hyp *dhh;
	char *c[6];
	c[0] = "n1"; 
	c[1] = "n2"; 
	c[2] = "nn"; 
	c[3] = "p1";
	c[4] = "p2";
	c[5] = "np";
	Double_t Ip, I, Inn, Inp;
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";

	dhh = new DataHelper_Hyp(FileName[2],FileCode[2]);
	dhh->ReadData();

	char * c= "np";
	cout << "el numero de columna es: " << dhh->Get_NCol() << endl;
	TGraphErrors* t = dhh->GetTGraphErrorsN("P2",c,0,101);
	t->SetTitle(c);
	t->Draw("ALP");

	delete dhh;
	delete FileName;
	delete FileCode;
	delete c;
}
