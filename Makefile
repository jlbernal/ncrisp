
CPP         = g++
LINKER      = g++

MPICPP      = mpicxx
MPILINKER   = mpicxx

# source code postfix
CPPSRCPOST  = cc
CPPHDRPOST  = hh
OBJPOST     = o
EXEPOST     = exec
SOPOST      = so

# Set variables ############################################

ROOTPATH    = $(ROOTSYS)

IPATH       = $(SYSIPATH) -I$(ROOTPATH)/include -I. -I./scripts/mcmc -I./scripts/mcef -I./scripts/multimode_fission -I./mcef -I./helpers -I./cascade -I./Amplitude/CrossSection -I./generators -I./base -I./Minuit/multimode_fission -I./tinyxml -I./multimode_fission -I/Langevin


# -D CRISP_SKIP_ROOTDICT
CFLAGSDBG   = $(shell root-config --cflags) -ggdb3 -D DEBUG -D _DEBUG -Wall -D MPICH_SKIP_MPICXX -D OMPI_SKIP_MPICXX -fexceptions -fPIC -D TIXML_USE_STL
CFLAGSREL   = $(shell root-config --cflags) -O2 -D MPICH_SKIP_MPICXX -D OMPI_SKIP_MPICXX -fPIC -D TIXML_USE_STL -lRIO -lNet

CFLAGS      = $(CFLAGSDBG)

LINKSPECDBG = -ggdb3 -Wall -fexceptions -DEBUG -fPIC
LINKSPECREL = -O2 -fPIC

LINKSPEC    = $(LINKSPECDBG)

RM          = rm -f

# Libs
LIBS        = \
			$(shell root-config --libs) \
			-lMathMore \
			-lEG \
			-lgslcblas \
			-lgsl

# Set File names ###########################################

CNAME       = mcmc
MPICNAME    = mcmc-mpi
MPICNAMETH  = mcmc-therma-mpi
ENAME       = mcef
#MMNAME      = mcef-minuit
#MPIENAME    = mcef-mpi
#MPIMMNAME   = mcef-minuit-mpi
MFNAMED		= multimodal-fission
MPIMFNAMED	= multimodal-fission-mpi
MFNAMEE		= multimodal-fission-minuit
MPIMFNAMEE	= multimodal-fission-minuit-mpi
LNAME       = langevin

LIBNAME     = libcrisp

PCH         = Minuit/stdafx.h


COBJECT     = $(CNAME).$(EXEPOST)
MPICOBJECT  = $(MPICNAME).$(EXEPOST)
MPICOBJECTTH  = $(MPICNAMETH).$(EXEPOST)
EOBJECT     = $(ENAME).$(EXEPOST)
#MMOBJECT    = $(MMNAME).$(EXEPOST)
#MPIEOBJECT  = $(MPIENAME).$(EXEPOST)
#MPIMMOBJECT = $(MPIMMNAME).$(EXEPOST)
MFOBJECTD	= $(MFNAMED).$(EXEPOST)
MPIMFOBJECTD= $(MPIMFNAMED).$(EXEPOST)
MFOBJECTE	= $(MFNAMEE).$(EXEPOST)
MPIMFOBJECTE= $(MPIMFNAMEE).$(EXEPOST)
LOBJECT		= $(LNAME).$(EXEPOST)


CLINKOBJ     = -o $(COBJECT)
MPICLINKOBJ  = -o $(MPICOBJECT)
MPICLINKOBJTH  = -o $(MPICOBJECTTH)
ELINKOBJ     = -o $(EOBJECT)
#MMLINKOBJ    = -o $(MMOBJECT)
#MPIELINKOBJ  = -o $(MPIEOBJECT)
#MPIMMLINKOBJ = -o $(MPIMMOBJECT)
MFLINKOBJD   = -o $(MFOBJECTD)
MPIMFLINKOBJD= -o $(MPIMFOBJECTD)
MFLINKOBJE   = -o $(MFOBJECTE)
MPIMFLINKOBJE= -o $(MPIMFOBJECTE)
LLINKOBJ     = -o $(LOBJECT)


PCHGHC      = $(PCH).gch
LIBCRISP    = $(LIBNAME).$(SOPOST)

# lista de arquivos fontes
CPPSRCFILES = \
			helpers/DataHelper.cc \
			helpers/DataHelperH.cc \
			helpers/root_utils.cc \
			helpers/Timer.cc \
			helpers/NameTable.cc \
			helpers/Util.cc \
			\
			base/BasicException.cc \
			base/CrispParticleTable.cc \
			base/decay.cc \
			base/Dynamics.cc \
			base/FermiLevels.cc \
			base/Measurement.cc \
			base/MesonsPool.cc \
			base/LeptonsPool.cc \
			base/NucleusDynamics.cc \
			base/ParticleDynamics.cc \
			base/relativisticKinematic.cc \
			base/time_collision.cc \
			base/time_surface.cc \
			base/transform_coords.cc \
			base/BindingEnergyTable.cc \
			\
			Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc \
			Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc \
			Amplitude/CrossSection/VectorMeson_Amplitudes.cc \
			Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc \
			\
			generators/AbstractChannel.cc \
			generators/angular_dists.cc \
			generators/bb_actions.cc \
			generators/bb_cross_sections.cc \
			generators/BBChannel.cc \
			generators/CrossSectionChannel.cc \
			generators/CrossSectionData.cc \
			generators/EventGen.cc \
			generators/gn_cross_sections.cc \
			generators/kaon_n_cross_sections.cc \
			generators/MesonNucleonChannel.cc \
			generators/LeptonNucleonChannel.cc \
			generators/photon_actions.cc \
			generators/PhotonChannel.cc \
			generators/PhotonEventGen.cc \
			generators/ProtonEventGen.cc \
			generators/IonEventGen.cc \
			generators/DeuteronEventGen.cc \
			generators/NeutrinoEventGen.cc \
			generators/resonance_mass.cc \
			generators/Meson_n_cross_sections.cc \
			generators/Lepton_n_cross_sections.cc \
			generators/Meson_n_actions.cc \
			generators/Lepton_n_actions.cc \
			generators/BremsstrahlungEventGen.cc \
			generators/UltraEventGen.cc \
			generators/HyperonEventGen.cc \
			\
			cascade/AbstractCascadeProcess.cc \
			cascade/BaryonBaryon.cc \
			cascade/BaryonDecay.cc \
			cascade/BaryonMeson.cc \
			cascade/BaryonSurface.cc \
			cascade/Cascade.cc \
			cascade/MesonDecay.cc  \
			cascade/MesonSurface.cc \
			cascade/BaryonLepton.cc \
			cascade/LeptonSurface.cc \
			cascade/SelectedProcess.cc \
			cascade/TimeOrdering.cc \
			\
			mcef/Mcef.cc \
			mcef/McefModel.cc \
			mcef/BsfgModel.cc \
			\
			scripts/mcmc/init_event_gen.cc \
			scripts/mcmc/analysis_event_gen.cc \
			\
			multimode_fission/MultimodalFission.cc \
			\
			Minuit/multimode_fission/FragmentCrossSectionFCN.cc\
			\
			Langevin/binding_energy.cc\
			Langevin/work_rootfiles.cc\
			Langevin/nucleus.cc\
			Langevin/mcefl.cc\
			Langevin/util.cc\
			Langevin/dynamic.cc\
			Langevin/potential.cc\
			Langevin/derivFunction.cc\
			Langevin/dampcoeff.cc


			

CPPEXTRAFILES = \
			tinyxml/ticpp.$(OBJPOST) \
			tinyxml/tinyxml.$(OBJPOST) \
			tinyxml/tinystr.$(OBJPOST) \
			tinyxml/tinyxmlerror.$(OBJPOST) \
			tinyxml/tinyxmlparser.$(OBJPOST)

# lista de arquivos objetos (mesma lista anterior trocando ".cc" por ".$(OBJPOST)")
CPPOFILES =	$(CPPSRCFILES:.cc=.$(OBJPOST)) dictionary/crisp_dict.$(OBJPOST) $(CPPEXTRAFILES)


# lista de arquivos cabeçalhos (mesma lista anterior trocando ".cc" por ".hh" + outros)
CPPHEADER =	$(CPPSRCFILES:.cc=.hh) base/base_defs.hh base/LeptonsPool.hh

# os arquivos de dicionário do CRISP (necessário para utilização das TTree com o códio do CRISP)
DICTFILES = dictionary/crisp_dict.cc dictionary/crisp_dict.h


mcmc: \
	$(PCHGHC) \
	$(COBJECT)

mcmc-mpi: \
	$(MPICOBJECT)

mcmc-therma-mpi: \
	$(MPICOBJECTTH)

mcef: \
	$(EOBJECT)

#mcef-minuit: \
#	MINUITPCH \
#	$(MMOBJECT)

#mcef-minuit-mpi: \
#	$(MPIMMOBJECT)

multimodal-fission: \
	$(MFOBJECTD)

multimodal-fission-mpi: \
	$(MPIMFOBJECTD)

multimodal-fission-minuit: \
	$(MFOBJECTE)

multimodal-fission-minuit-mpi: \
	$(MPIMFOBJECTE)

langevin: \
	$(PCHGHC) \
	$(LOBJECT)



# deleta os arquivos intermediários
clean:
	@$(RM) $(CPPOFILES) $(PCHGHC) $(DICTFILES) *.cxx *.user *Dict.* *.ncb *.def *.exp *.d *.log *.pdb *.ilk *.manifest *.idb *.so base/*.so base/*.d cascade/*.so cascade/*.d generators/*.so generators/*.d helpers/*.so helpers/*.d mcef/*.so mcef/*.d Minuit/*.o Minuit/mcef/*.o Minuit/multimode_fission/*.o multimode_fission/*.so multimode_fission/*.d scripts/mcef/*.o scripts/mcmc/*.o scripts/multimode_fission/*.o Amplitude/CrossSection/*.d Amplitude/CrossSection/*.o Amplitude/CrossSection/*.so Langevin/*.o Langevin/*.so Langevin/*.d scripts/langevin/*.o scripts/langevin/*.so scripts/langevin/*.d

# deleta os arquivos intermediários e finais
clear:
	@$(RM) $(CPPOFILES) $(PCHGHC) $(DICTFILES) *.cxx *.user *Dict.* *.ncb *.def *.exp *.d *.log *.pdb *.ilk *.manifest *.idb *.so base/*.so base/*.d cascade/*.so cascade/*.d generators/*.so generators/*.d helpers/*.so helpers/*.d mcef/*.so mcef/*.d Minuit/*.o Minuit/mcef/*.o Minuit/multimode_fission/*.o multimode_fission/*.so multimode_fission/*.d scripts/mcef/*.o scripts/mcmc/*.o scripts/multimode_fission/*.o Amplitude/CrossSection/*.d Amplitude/CrossSection/*.o Amplitude/CrossSection/*.so Langevin/*.o Langevin/*.so Langevin/*.d scripts/langevin/*.o scripts/langevin/*.so scripts/langevin/*.d *.exec


# cria os Cabeçalhos Pre-compilados que fazem o processo de compilação ser mais rapido
# obs.: Precompiled header não funciona quando compilado com MPI (mpicxx)
$(PCHGHC) : $(PCH)
	$(CPP) $(IPATH) $(CFLAGS) $(PCH)

MINUITPCH : $(PCH)
	$(CPP) $(IPATH) $(CFLAGS) -D CRISP_MINUIT $(PCH)



# regra de inferência para compilar todos os arquivos .cc em .o
.$(CPPSRCPOST).$(OBJPOST) : $(PCHGHC)
	$(CPP) -include "$(PCH)" $(IPATH) $(CFLAGS) -c $*.$(CPPSRCPOST) -o $*.$(OBJPOST)


# regra para gerar os arquivos fontes do dicionário do CRISP
$(DICTFILES): $(CPPHEADER)
	rootcint -f dictionary/crisp_dict.cc -c -I. -I./mcef -I./helpers -I./cascade -I./generators -I./base -I./Amplitude/CrossSection -I./multimode_fission -I./Minuit/multimode_fission $(CPPHEADER) dictionary/Linkdef.h


# regra para compilar o dicionário
dictionary/crisp_dict.o: $(DICTFILES)
	$(CPP) -include "$(PCH)" $(IPATH) $(CFLAGS) -c $*.$(CPPSRCPOST) -o $*.$(OBJPOST)


# regra para compilar um Shared Object com o código do CRISP (sem utilidade atualmente)
$(LIBCRISP):
	$(LINKER) $(LINKSPEC) -shared -o $(LIBCRISP) $(CPPOFILES) $(LIBS)



# compila a cascata sem MPI
$(COBJECT) : CPPHEADER += scripts/mcmc/cascade.hh 
$(COBJECT) : $(PCHGHC) $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBCRISP)
	$(LINKER) $(LINKSPEC) $(CLINKOBJ) $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBS)

# compila a cascata com MPI
$(MPICOBJECT) : CPPHEADER += scripts/mcmc/cascade.hh
$(MPICOBJECT) : CFLAGS += -D CRISP_MPI
$(MPICOBJECT) : CPP = $(MPICPP)
$(MPICOBJECT) : LINKER = $(MPILINKER)
$(MPICOBJECT) : $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBCRISP)
	$(MPILINKER) $(LINKSPEC) $(MPICLINKOBJ) $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBS)

# compila a cascata com MPI e termalização
$(MPICOBJECTTH) : CPPHEADER += scripts/mcmc/cascade.hh
$(MPICOBJECTTH) : CFLAGS += -D CRISP_MPI -D THERMA
$(MPICOBJECTTH) : CPP = $(MPICPP)
$(MPICOBJECTTH) : LINKER = $(MPILINKER)
$(MPICOBJECTTH) : $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBCRISP)
	$(MPILINKER) $(LINKSPEC) $(MPICLINKOBJTH) $(CPPOFILES) scripts/mcmc/main_mcmc.$(OBJPOST) scripts/mcmc/cascade.$(OBJPOST) $(LIBS)

#compila o mcef
$(EOBJECT) : CPPHEADER += scripts/mcef/mcef.hh
$(EOBJECT) : $(PCHGHC) $(CPPOFILES) scripts/mcef/main_mcef.$(OBJPOST) scripts/mcef/mcef.$(OBJPOST) $(LIBCRISP)
	$(LINKER) $(LINKSPEC) $(ELINKOBJ) $(CPPOFILES) scripts/mcef/main_mcef.$(OBJPOST) scripts/mcef/mcef.$(OBJPOST) $(LIBS)

# compila o MCEF com MINUIT
#$(MMOBJECT) : CPPHEADER += Minuit/mcef/McefAdjust.hh
#$(MMOBJECT) : CFLAGS += -D CRISP_MINUIT
#$(MMOBJECT) : $(PCHGHC) $(CPPOFILES) Minuit/mcef/main_mcef_minuit.$(OBJPOST) Minuit/mcef/McefAdjust.$(OBJPOST) $(LIBCRISP)
#	$(LINKER) $(LINKSPEC) $(MMLINKOBJ) $(CPPOFILES) Minuit/mcef/main_mcef_minuit.$(OBJPOST) Minuit/mcef/McefAdjust.$(OBJPOST) $(LIBS) -lMinuit2

# compila o MCEF com MINUIT e com MPI
#$(MPIMMOBJECT) : CPPHEADER += Minuit/mcef/McefAdjust.hh
#$(MPIMMOBJECT) : CFLAGS += -D CRISP_MINUIT -D CRISP_MPI
#$(MPIMMOBJECT) : CPP = $(MPICPP)
#$(MPIMMOBJECT) : LINKER = $(MPILINKER)
#$(MPIMMOBJECT) : $(CPPOFILES) Minuit/mcef/main_mcef_minuit.$(OBJPOST) Minuit/mcef/McefAdjust.$(OBJPOST) $(LIBCRISP)
#	$(MPILINKER) $(LINKSPEC) $(MPIMMLINKOBJ) $(CPPOFILES) Minuit/mcef/main_mcef_minuit.$(OBJPOST) Minuit/mcef/McefAdjust.$(OBJPOST) $(LIBS) -lMinuit2

# compila a fissao multimodal usando a classe MultimodalFission.hh
$(MFOBJECTD) : CPPHEADER += scripts/multimode_fission/FissionFrag.hh
$(MFOBJECTD) : $(CPPOFILES) scripts/multimode_fission/main_FissionFrag.$(OBJPOST) scripts/multimode_fission/FissionFrag.$(OBJPOST)
	$(LINKER) $(LINKSPEC) $(MFLINKOBJD) $(CPPOFILES) scripts/multimode_fission/main_FissionFrag.$(OBJPOST) scripts/multimode_fission/FissionFrag.$(OBJPOST) $(LIBS)

# compila a fissao multimodal com MPI usando a classe MultimodalFission.hh
$(MPIMFOBJECTD) : CPPHEADER += scripts/multimode_fission/FissionFrag.hh
$(MPIMFOBJECTD) : CFLAGS += -D CRISP_MPI
$(MPIMFOBJECTD) : CPP = $(MPICPP)
$(MPIMFOBJECTD) : LINKER = $(MPILINKER)
$(MPIMFOBJECTD) : $(CPPOFILES) scripts/multimode_fission/main_FissionFrag.$(OBJPOST) scripts/multimode_fission/FissionFrag.$(OBJPOST)
	$(MPILINKER) $(LINKSPEC) $(MPIMFLINKOBJD) $(CPPOFILES) scripts/multimode_fission/main_FissionFrag.$(OBJPOST) scripts/multimode_fission/FissionFrag.$(OBJPOST) $(LIBS)

# compila a fissao multimodal usando a classe MultimodalFission.hh e MINUIT2
#$(MFOBJECTE) : CPPHEADER += Minuit/multimode_fission/FragmentCrossSectionFCN.hh
$(MFOBJECTE) : $(CPPOFILES) Minuit/multimode_fission/FissionFrag_mpi_Minuit.$(OBJPOST)
	$(LINKER) $(LINKSPEC) $(MFLINKOBJE) $(CPPOFILES) Minuit/multimode_fission/FissionFrag_mpi_Minuit.$(OBJPOST) $(LIBS) -lMinuit2

# compila a fissao multimodal com MPI usando a classe MultimodalFission.hh e MINUIT2
#$(MPIMFOBJECTE) : CPPHEADER += Minuit/multimode_fission/FragmentCrossSectionFCN.hh
$(MPIMFOBJECTE) : CFLAGS += -D CRISP_MPI
$(MPIMFOBJECTE) : CPP = $(MPICPP)
$(MPIMFOBJECTE) : LINKER = $(MPILINKER)
$(MPIMFOBJECTE) : $(CPPOFILES) Minuit/multimode_fission/FissionFrag_mpi_Minuit.$(OBJPOST) 
	$(MPILINKER) $(LINKSPEC) $(MPIMFLINKOBJE) $(CPPOFILES) Minuit/multimode_fission/FissionFrag_mpi_Minuit.$(OBJPOST) $(LIBS) -lMinuit2


#compila o Langevin
$(LOBJECT) : CPPHEADER += scripts/langevin/langevin.hh
$(LOBJECT) : $(PCHGHC) $(CPPOFILES) scripts/langevin/main_langevin.$(OBJPOST) scripts/langevin/langevin.$(OBJPOST) $(LIBCRISP)
	$(LINKER) $(LINKSPEC) $(LLINKOBJ) $(CPPOFILES) scripts/langevin/main_langevin.$(OBJPOST) scripts/langevin/langevin.$(OBJPOST) $(LIBS)



