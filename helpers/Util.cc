/* ================================================================================
 * 
 * 	Copyright 2009 Júlio Cesar
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#include "Util.hh"

// carrega os dados no formato (x,y,dy) de um arquivo texto simples. 
TGraphErrors MultFillTGraphYError( const char* filename, 
							   Double_t energy_min, 
							   Double_t energy_max)
{ 
	ifstream ifs;
	ifs.open(filename);

//	std::stringstream ss;
//	ss << "fillTGraphYError_" << opt << ".root";
//	std::string str;
//	ss >> str;

	Float_t x, y, dy;  
//	TFile f(str.c_str(),"RECREATE");
	TNtuple *tuple = new TNtuple("aTuple","","x:y:dy");

	Int_t size = 0;
	while (true) {

		if (!ifs.good()) break;   

		std::string pdline;
		std::getline(ifs, pdline);

		char c = pdline.c_str()[0];

		if ( c != '#' ) {      
			std::istringstream istr(pdline);      
			istr >> x >> y >> dy;
			if ( x > energy_min && x <= energy_max )
			{
				tuple->Fill(x, y, dy);   
				size++;      
			}
		} 
	}

	tuple->SetBranchAddress("x",&x);
	tuple->SetBranchAddress("y",&y);
	tuple->SetBranchAddress("dy",&dy);

	Double_t *X = new Double_t[size];
	Double_t *Y = new Double_t[size];
	Double_t *DY = new Double_t[size];  
	for ( Int_t i = 0; i < size; i++ ) {    
		tuple->GetEntry(i);
		X[i] = x; Y[i] = y; DY[i] = dy;   
	}
//	f.Write();
	TGraphErrors tge(size, X, Y, 0, DY);

	return tge;
}

/*
// carrega os dados no formato (x,y,dy) de um arquivo texto simples. 
TGraphErrors MultFillTGraphYError( const char* filename, 
							   Double_t energy_min, 
							   Double_t energy_max)
{ 
	ifstream ifs;
	ifs.open(filename);

//	std::stringstream ss;
//	ss << "fillTGraphYError_" << opt << ".root";
//	std::string str;
//	ss >> str;

	Float_t x, y, dy;  
//	TFile f(str.c_str(),"RECREATE");
	TNtuple *tuple = new TNtuple("aTuple","","x:y:dy");

	Int_t size = 0;
	while (true) {

		if (!ifs.good()) break;   

		std::string pdline;
		std::getline(ifs, pdline);

		char c = pdline.c_str()[0];

		if ( c != '#' ) {      
			std::istringstream istr(pdline);      
			istr >> x >> y >> dy;
			if ( x > energy_min && x <= energy_max )
			{
				tuple->Fill(x, y, dy);   
				size++;      
			}
		} 
	}

	tuple->SetBranchAddress("x",&x);
	tuple->SetBranchAddress("y",&y);
	tuple->SetBranchAddress("dy",&dy);

	Double_t *X = new Double_t[size];
	Double_t *Y = new Double_t[size];
	Double_t *DY = new Double_t[size];  
	for ( Int_t i = 0; i < size; i++ ) {    
		tuple->GetEntry(i);
		X[i] = x; Y[i] = y; DY[i] = dy;   
	}
//	f.Write();
	TGraphErrors tge(size, X, Y, 0, DY);

	return tge;
}
*/

// carrega os dados no formato (x,y,dy) de um arquivo texto simples. 
TGraphErrors MultFillTGraph( const char* filename, 
							 Double_t energy_min, 
							 Double_t energy_max)
{ 
	ifstream ifs;
	ifs.open(filename);

//	std::stringstream ss;
//	ss << "fillTGraphYError_" << opt << ".root";
//	std::string str;
//	ss >> str;

	Float_t x, y;  
//	TFile f(str.c_str(),"RECREATE");
	TNtuple *tuple = new TNtuple("aTuple","","x:y");

	Int_t size = 0;
	while (true) {

		if (!ifs.good()) break;   

		std::string pdline;
		std::getline(ifs, pdline);

		char c = pdline.c_str()[0];

		if ( c != '#' ) {      
			std::istringstream istr(pdline);      
			istr >> x >> y;
			if ( x > energy_min && x <= energy_max )
			{
				tuple->Fill(x, y);   
				size++;      
			}
		} 
	}

	tuple->SetBranchAddress("x",&x);
	tuple->SetBranchAddress("y",&y);

	Double_t *X = new Double_t[size];
	Double_t *Y = new Double_t[size];
	for ( Int_t i = 0; i < size; i++ ) {    
		tuple->GetEntry(i);
		X[i] = x; Y[i] = y; 
	}
//	f.Write();
	TGraphErrors tge(size, X, Y, 0, 0);

	return tge;
}




void StringExplode(std::string str, std::string separator, std::vector< std::string >& results)
{
	int found;
	found = str.find_first_of(separator);
	while((uint)found != std::string::npos)
	{
		if(found > 0)
		{
			results.push_back(str.substr(0,found));
		}
		str = str.substr(found+1);
		found = str.find_first_of(separator);
	}
	if(str.length() > 0)
	{
		results.push_back(str);
	}
}


void ReadCascadeLine( std::istream& ifs, Double_t &Energy, Int_t &A, Int_t &Z, Int_t &Counts, Double_t &ExEnergy )
{
	TString inStr;
	TString delim = " ";

	if ( ifs.good() )
	{
		inStr.ReadLine(ifs);
		if ( inStr.Length() == 0 ) return;
		TObjArray* arr = inStr.Tokenize(delim);
	
		if ( (( TObjString *)(*arr)[1])->GetString()[0] != '#' )
		{
			Energy = (( TObjString *)(*arr)[1])->GetString().Atof();
			Counts = (( TObjString *)(*arr)[2])->GetString().Atoi();
			ExEnergy = (( TObjString *)(*arr)[3])->GetString().Atof();
			A = (( TObjString *)(*arr)[4])->GetString().Atoi();
			Z = (( TObjString *)(*arr)[5])->GetString().Atoi();
				
			arr->Delete();
			delete arr;
		}
	} else {
		std::cout << "Erro ao ler arquivo..." << "\n";
	}
}




void SaveGraph( TGraph *tg, std::string titulo, char DrawOption[] )
{
	TCanvas *c = new TCanvas;

//	tg->SetTitle( titulo.c_str() );
//	tg->SetMarkerColor(4);
//	tg->SetMarkerStyle(24);
//	tg->SetMarkerSize(0.64);
	tg->Draw( DrawOption ); // tg->Draw("ALP");
//	c->SaveAs("c.png");

    TImage *img = TImage::Create();
	//img->FromPad(c, 10, 10, 300, 200);
	img->FromPad(c);

	img->WriteImage( (titulo+".png").c_str() );

	delete c;
	delete img;
}



void SaveMultiGraph( TMultiGraph *tg, std::string titulo )
{
	TCanvas *c = new TCanvas;

//	tg->SetTitle( titulo.c_str() );
//	tg->SetMarkerColor(4);
//	tg->SetMarkerStyle(21);
	tg->Draw( "A" ); // tg->Draw("ALP");
//	c->SaveAs("c.png");

    TImage *img = TImage::Create();
	//img->FromPad(c, 10, 10, 300, 200);
	img->FromPad(c);

	img->WriteImage( (titulo+".png").c_str() );

	delete c;
	delete img;
}

double cint(double x){

	double intpart = 0;
	if (modf(x,&intpart)>=.5){
		return ceil(x);
	} else{
		return floor(x);
	}
}
