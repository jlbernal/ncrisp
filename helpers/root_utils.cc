/* ================================================================================
 * 
 * 	Copyright 2009 Júlio Cesar
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


// carrega os dados no formato (x,y,dy) de um arquivo texto simples. 
// args
// filename: nome do arquivo.
// cm_energy_max: energia do centro de massa maxima a ser lida. 

#include "root_utils.hh"

#include "TString.h"
#include "TNtuple.h"

#include <sstream>
#include <fstream>
#include <string>

using namespace std;

TGraphErrors* fillTGraphYError(const char* filename, Double_t energy_min, Double_t energy_max ){ 
  
	ifstream ifs;
	ifs.open(filename);
  
	Float_t x, y, dy;  
	//TFile f("fillTGraphYError.root","RECREATE");
	TNtuple *tuple = new TNtuple("aTuple","","x:y:dy");

	Int_t size = 0;
	while (true) {
    
		if (!ifs.good()) 
			break;   
    
		string pdline;
		getline(ifs, pdline);
    
		char c = pdline.c_str()[0];
    
		if ( c != '#' ) {      
			istringstream istr(pdline);      
			istr >> x >> y >> dy;
			
			if ( x > energy_min && x <= energy_max ) {	
				tuple->Fill(x, y, dy);   
				size++;      
			}
		}	 
	}
  
	tuple->SetBranchAddress("x",&x);
	tuple->SetBranchAddress("y",&y);
	tuple->SetBranchAddress("dy",&dy);
  
	Double_t *X = new Double_t[size];
	Double_t *Y = new Double_t[size];
	Double_t *DY = new Double_t[size];  
  
	for ( Int_t i = 0; i < size; i++ ) {    
		tuple->GetEntry(i);
		X[i] = x; 
		Y[i] = y; 
		DY[i] = dy;   
	}
	//f.Write();
	TGraphErrors* tge = new TGraphErrors(size, X, Y, 0, DY);

	return tge;
}


// Carrega os dados em um arquivo texto simples em um histograma.
// Retorna o numero de dados carregados.

 int fillTH1( char* f_name, TH1* hpx ){
 
	ifstream ifs(f_name);
	Int_t i = 0;
  
	while(true){
		Double_t m;
    
		if ( !ifs.good() ) 
			break;
    
		ifs >> m;
		hpx->Fill(m);
		i++;
	}
	return i;
}

void LS(const char *path){

	if (!path || !path[0]) path = gSystem->WorkingDirectory();
		TString mypath(path); 

	void *dir = gSystem->OpenDirectory(mypath);
	const char *name;
	int n = 0;

	while ( (name = gSystem->GetDirEntry(dir)) ) {
	 
		TString full(mypath);
		full +="/"; full += name;
		Long_t id,size,flags,modtime;
		gSystem->GetPathInfo(full, &id, &size, &flags, &modtime);    
		printf("%ld - %s\n",size,name);
		n++;
	}
}
