/* ================================================================================
 * 
 * 	Copyright 2012 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __DataHelper_HypH
#define __DataHelper_HypH

#include "TString.h"
#include "TNtuple.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TGraph2D.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

//need to be very careful with this class.
//reading tuples depends of the lateest reading.
class DataHelper_Hyp
{
  private:
	TNtuple *tuple; // Angular Distributions Array
	TString lista; // List of branches for tuple
	Int_t columnsLength; // Number of branches
	std::ifstream ifs; // Data File
	Int_t N_Col; // total entries number of Ntuple

  public:
	DataHelper_Hyp (const char* fileName, const char* varList);
 	~DataHelper_Hyp ();
	void ReadData ();
	Int_t Get_NCol();
	TNtuple* GetTNtuple(){ return tuple;};
	void Print1_Tuple(Int_t t);
	void PrintAll_Tuple();
	
	Float_t Get_tuple_1Value(const char* Branch, Int_t Index );
	void    Get_tuple_2Value(const char* Branchx, const char* Branchy, Float_t &x, Float_t &y, Int_t Index );
	void    Get_tuple_3Value(const char* Branchx, const char* Branchy1, const char* Branchy2, Float_t &x, Float_t &y1, Float_t &y2, Int_t Index );

	TGraphErrors *GetTGraphErrorsN(const char* branchx, const char* branchy, Int_t N_Initial, Int_t _PASS, bool Ask_Print = false ,Float_t X_Err = 0.0, Float_t Y_Err = 0.0);
	TGraphErrors *GetTGraphErrorsCombN(const char* branchx, const char* branchy1, const char* branchy2, Int_t N_Initial, Int_t _PASS, bool Ask_Print = false, Float_t X_Err = 0.0, Float_t Y_Err= 0.0);
	TGraph2D *GetTGraph2D(const char* branchx, const char* branchy, const char* branchz, bool Ask_Print = false);

};

#endif
