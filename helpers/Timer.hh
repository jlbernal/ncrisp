/* ================================================================================
 * 
 * 	Copyright 2009 Júlio Cesar
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#ifndef _timer_hh_
#define _timer_hh_

#ifndef WIN32
//#include <sys/time.h>
#include <time.h>
#else
#include <time.h>
#include <windows.h>
#pragma comment (lib, "winmm.lib")
#endif

long CRISPGetTime();


void FormTime( double tempototal, std::ostream &os );

/*
inline std::ostream& FormTime2( double tempototal )
{
	std::ostream os;
	long dias(0), horas(0), minutos(0), segundos(0);
	segundos = (long)tempototal;

	if( segundos < 0 )
	{
		os << "??? (Erro ao calcular o tempo)";
	} else if( segundos > 86400 )
	{
		minutos = segundos/60;
		segundos = segundos%60;
		horas = minutos/60;
		minutos = minutos%60;
		dias = horas/24;
		horas = horas%24;
		os << dias << " d " << horas << " h " << minutos << " m " << segundos << " s.";
	} else if( segundos > 3600 )
	{
		minutos = segundos/60;
		segundos = segundos%60;
		horas = minutos/60;
		minutos = minutos%60;
		os << horas << " h " << minutos << " m " << segundos << " s.";
	} else if( segundos > 60 )
	{
		minutos = segundos/60;
		segundos = segundos%60;
		os << minutos << " m " << segundos << " s.";
	} else {
		os << segundos << " s.";
	}
}
*/

std::string getTimeStamp();

#endif // _timer_hh_

