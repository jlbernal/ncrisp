/* ================================================================================
 * 
 * 	Copyright 2012 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "DataHelper.hh"

DataHelper_struct::DataHelper_struct(const char* varList, const char* tupleName){
	this->varList = new TString(varList);    
	rootFile = new TFile(tupleName, "RECREATE");
	tuple = new TNtuple(tupleName, "", varList);
	TString lista(varList);
	TObjArray *vars = lista.Tokenize(TString(":"));
	columnsLength = vars->GetEntriesFast();
	columns = new Float_t[columnsLength];
	delete vars; vars = 0;
}
Int_t DataHelper_struct::ReadDataFrom(const char* fileName){
	std::ifstream ifs;
	ifs.open(fileName);
	int n = 0;
	while (true) {      
		if (!ifs.good()) break;         
		std::string pdline;
		getline(ifs, pdline);            
		char c = pdline.c_str()[0];      
		if ( c != '#' ) {    
			std::istringstream __istr(pdline.c_str());            
			for ( int h = 0; h < columnsLength; h++ ) {
				__istr >> columns[h];
			}
			tuple->Fill(columns);     
			n++;
		}     
	}        
	rootFile->Write();
	return n;
} 

void Nuc_Struct::Print(){
	std::cout << "***********************NUCLEUS PRINT***********************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Mass Index = " << A << std::endl;
	std::cout << "Charge Number = " << Z << std::endl;
	std::cout << "*********************Fermi Levels*********************" << std::endl;
	std::cout << "Proton Levels:  [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		std::cout <<  PLevels[i] << ", ";
	std::cout <<  PLevels[MAXLEV - 1] << " ]" << std::endl;
	std::cout << "Neutron Levels: [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		std::cout <<  NLevels[i] << ", ";
	std::cout << NLevels[MAXLEV - 1] << " ]" << std::endl;
	std::cout << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < A; i++){
		if (Nuc_IsB[i])
			std::cout << "Index = " << Nuc_IDX[i] << ", PID = " << Nuc_PID[i] << ", Bind" << std::endl;
		else
			std::cout << "Index = " << Nuc_IDX[i] << ", PID = " << Nuc_PID[i] << ", UnBind" << std::endl;
		std::cout << "POSITION (X,Y,Z) = ( " << Nuc_X[i] << ", " << Nuc_Y[i] << ", " << Nuc_Z[i] << " )" << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Nuc_PX[i] << ", " << Nuc_PY[i] << ", " << Nuc_PZ[i] << ", " << Nuc_E[i] << " )" << std::endl;
		std::cout << "MOMENTUM P = " << Nuc_P[i] << std::endl;
		std::cout << "Kinetic Energy = " << Nuc_K[i]  << std::endl;
	}
	std::cout << "****************************END****************************" << std::endl << std::endl;
}

void Nuc_Struct::Print_Levels(){
	std::cout << "***********************NUCLEUS LEVELS PRINT***********************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Mass Index = " << A << std::endl;
	std::cout << "Charge Number = " << Z << std::endl;
	std::cout << "Proton Levels:  [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		std::cout <<  PLevels[i] << ", ";
	std::cout <<  PLevels[MAXLEV - 1] << " ]" << std::endl;
	std::cout << "Neutron Levels: [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		std::cout <<  NLevels[i] << ", ";
	std::cout << NLevels[MAXLEV - 1] << " ]" << std::endl;
}

void Mes_Struct::Print(){
	std::cout << "***********************MESON POOL PRINT***********************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Number of mesons = " << NM << std::endl;
	std::cout << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < NM; i++){
		if (Mes_IsB[i])
			std::cout << "Key = " << Mes_KEY[i] << ", PID = " << Mes_PID[i] << ", Bind" << std::endl;
		else
			std::cout << "Key = " << Mes_KEY[i] << ", PID = " << Mes_PID[i] << ", UnBind" << std::endl;
		std::cout << "POSITION (X,Y,Z) = ( " << Mes_X[i] << ", " << Mes_Y[i] << ", " << Mes_Z[i] << " )" << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Mes_PX[i] << ", " << Mes_PY[i] << ", " << Mes_PZ[i] << ", " << Mes_E[i] << " )" << std::endl;
		std::cout << "MOMENTUM P = " << Mes_P[i] << std::endl;
		std::cout << "Kinetic Energy = " << Mes_K[i]  << std::endl;
	}
	std::cout << "****************************END****************************" << std::endl << std::endl;
}


void Lep_Struct::Print(){
	std::cout << "***********************LEPTON POOL PRINT***********************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Number of leptons = " << NM << std::endl;
	std::cout << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < NM; i++){
		if (Lep_IsB[i])
			std::cout << "Key = " << Lep_KEY[i] << ", PID = " << Lep_PID[i] << ", Bind" << std::endl;
		else
			std::cout << "Key = " << Lep_KEY[i] << ", PID = " << Lep_PID[i] << ", UnBind" << std::endl;
		std::cout << "POSITION (X,Y,Z) = ( " << Lep_X[i] << ", " << Lep_Y[i] << ", " << Lep_Z[i] << " )" << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Lep_PX[i] << ", " << Lep_PY[i] << ", " << Lep_PZ[i] << ", " << Lep_E[i] << " )" << std::endl;
		std::cout << "MOMENTUM P = " << Lep_P[i] << std::endl;
		std::cout << "Kinetic Energy = " << Lep_K[i]  << std::endl;
	}
	std::cout << "****************************END****************************" << std::endl << std::endl;
}



void Proc_Struct::Print(){
	std::cout << "***********************PROCESS PRINT***********************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Process: " << ProN  <<  std::endl;
	std::cout << "Time: " << TIME  <<  std::endl;
	std::cout << "Initial particles Number = " << NPartIn  <<  std::endl;
	std::cout << "Final particles Number = " << NTotal - NPartIn <<  std::endl;
	std::cout << "Total particles Number  = " << NTotal <<  std::endl;
	std::cout << "Index 1 = " << I1  <<  std::endl;
	if (I2 != -1) std::cout << "Index 2 = " << I2  <<  std::endl;
	std::cout << "*****************Initials Particles****************" << std::endl;
	for (int i = 0; i < NPartIn; i++){
		if (Proc_IsB)
			std::cout  << "PID = " << Proc_PID[i] << ", Bind" << std::endl;
		else
			std::cout  << "PID = " << Proc_PID[i] << ", UnBind" << std::endl;
		std::cout << "POSITION (X,Y,Z) = ( " << Proc_X[i] << ", " << Proc_Y[i] << ", " << Proc_Z[i] << " )" << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Proc_PX[i] << ", " << Proc_PY[i] << ", " << Proc_PZ[i] << ", " << Proc_E[i] << " )" << std::endl;
		std::cout << "MOMENTUM P = " << Proc_P[i] << std::endl;
		std::cout << "Kinetic Energy = " << Proc_K[i]  << std::endl;
	}
	std::cout << "*****************Finals Particles****************" << std::endl;
	for (int i = NPartIn; i < NTotal; i++){
		if (Proc_IsB[i])
			std::cout << "PID = " << Proc_PID[i] << ", Bind" << std::endl;
		else
			std::cout << "PID = " << Proc_PID[i] << ", UnBind" << std::endl;
		std::cout << "POSITION (X,Y,Z) = ( " << Proc_X[i] << ", " << Proc_Y[i] << ", " << Proc_Z[i] << " )" << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Proc_PX[i] << ", " << Proc_PY[i] << ", " << Proc_PZ[i] << ", " << Proc_E[i] << " )" << std::endl;
		std::cout << "MOMENTUM P = " << Proc_P[i] << std::endl;
		std::cout << "Kinetic Energy = " << Proc_K[i]  << std::endl;
	}
	std::cout << "****************************END****************************" << std::endl << std::endl;
}

void UnBindPart_Struct::Print(){
	std::cout << "********************UNBIND PARTICLES PRINT*****************" << std::endl;
	std::cout << "***************************BEGIN***************************" << std::endl;
	std::cout << "Number of unbind particles = " << NPart << std::endl;
	std::cout << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < NPart; i++){
		std::cout << "PID = " << Part_PID[i]  << std::endl;
		std::cout << "MOMENTUM (Px,Py,Pz,E) = ( " << Part_PX[i] << ", " << Part_PY[i] << ", " << Part_PZ[i] << ", " << Part_E[i] << " )" << std::endl;
		std::cout << "Kinetic Energy = " << Part_K[i]  << std::endl;
	}
	std::cout << "****************************END****************************" << std::endl << std::endl;
}

void Nuc_Struct::CPT_Print(ofstream &out){
	out << "***********************NUCLEUS PRINT***********************" << std::endl;
	out << "***************************BEGIN***************************" << std::endl;
	out << "Mass Index = " << A << std::endl;
	out << "Charge Number = " << Z << std::endl;
	out << "*********************Fermi Levels*********************" << std::endl;
	out << "Proton Levels:  [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		out <<  PLevels[i] << ", ";
	out <<  PLevels[MAXLEV - 1] << " ]" << std::endl;
	out << "Neutron Levels: [ ";
	for (int i = 0; i < MAXLEV - 1; i++)
		out <<  NLevels[i] << ", ";
	out << NLevels[MAXLEV - 1] << " ]" << std::endl;
/*	out << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < A; i++){
		if (Nuc_IsB[i])
			out << "Index = " << Nuc_IDX[i] << ", PID = " << Nuc_PID[i] << ", Bind" << std::endl;
		else
			out << "Index = " << Nuc_IDX[i] << ", PID = " << Nuc_PID[i] << ", UnBind" << std::endl;
		out << "POSITION (X,Y,Z) = ( " << Nuc_X[i] << ", " << Nuc_Y[i] << ", " << Nuc_Z[i] << " )" << std::endl;
		out << "MOMENTUM (Px,Py,Pz,E) = ( " << Nuc_PX[i] << ", " << Nuc_PY[i] << ", " << Nuc_PZ[i] << ", " << Nuc_E[i] << " )" << std::endl;
		out << "Kinetic Energy = " << Nuc_K[i]  << std::endl;
	}
*/
	out << "****************************END****************************" << std::endl << std::endl;
}
void Mes_Struct::CPT_Print(ofstream &out){
	out << "***********************MESON POOL PRINT***********************" << std::endl;
	out << "***************************BEGIN***************************" << std::endl;
	out << "Number of mesons = " << NM << std::endl;
	out << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < NM; i++){
		if (Mes_IsB[i])
			out << "Key = " << Mes_KEY[i] << ", PID = " << Mes_PID[i] << ", Bind" << std::endl;
		else
			out << "Key = " << Mes_KEY[i] << ", PID = " << Mes_PID[i] << ", UnBind" << std::endl;
		out << "POSITION (X,Y,Z) = ( " << Mes_X[i] << ", " << Mes_Y[i] << ", " << Mes_Z[i] << " )" << std::endl;
		out << "MOMENTUM (Px,Py,Pz,E) = ( " << Mes_PX[i] << ", " << Mes_PY[i] << ", " << Mes_PZ[i] << ", " << Mes_E[i] << " )" << std::endl;
		out << "Kinetic Energy = " << Mes_K[i]  << std::endl;
	}
	out << "****************************END****************************" << std::endl << std::endl;
}
void Proc_Struct::CPT_Print(ofstream &out){
	out << "***********************PROCESS PRINT***********************" << std::endl;
	out << "***************************BEGIN***************************" << std::endl;
	out << "Process: " << ProN  <<  std::endl;
	out << "Time: " << TIME  <<  std::endl;
	out << "Initial particles Number = " << NPartIn  <<  std::endl;
	out << "Final particles Number = " << NTotal - NPartIn <<  std::endl;
	out << "Total particles Number  = " << NTotal <<  std::endl;
	out << "Index 1 = " << I1  <<  std::endl;
	if (I2 != -1) out << "Index 2 = " << I2  <<  std::endl;
	out << "*****************Initials Particles****************" << std::endl;
	for (int i = 0; i < NPartIn; i++){
		if (Proc_IsB)
			out  << "PID = " << Proc_PID[i] << ", Bind" << std::endl;
		else
			out  << "PID = " << Proc_PID[i] << ", UnBind" << std::endl;
		out << "POSITION (X,Y,Z) = ( " << Proc_X[i] << ", " << Proc_Y[i] << ", " << Proc_Z[i] << " )" << std::endl;
		out << "MOMENTUM (Px,Py,Pz,E) = ( " << Proc_PX[i] << ", " << Proc_PY[i] << ", " << Proc_PZ[i] << ", " << Proc_E[i] << " )" << std::endl;
		out << "Kinetic Energy = " << Proc_K[i]  << std::endl;
	}
	out << "*****************Finals Particles****************" << std::endl;
	for (int i = NPartIn; i < NTotal; i++){
		if (Proc_IsB[i])
			out << "PID = " << Proc_PID[i] << ", Bind" << std::endl;
		else
			out << "PID = " << Proc_PID[i] << ", UnBind" << std::endl;
		out << "POSITION (X,Y,Z) = ( " << Proc_X[i] << ", " << Proc_Y[i] << ", " << Proc_Z[i] << " )" << std::endl;
		out << "MOMENTUM (Px,Py,Pz,E) = ( " << Proc_PX[i] << ", " << Proc_PY[i] << ", " << Proc_PZ[i] << ", " << Proc_E[i] << " )" << std::endl;
		out << "Kinetic Energy = " << Proc_K[i]  << std::endl;
	}
	out << "****************************END****************************" << std::endl << std::endl;
}

void UnBindPart_Struct::CPT_Print(ofstream &out){
	out << "********************UNBIND PARTICLES PRINT*****************" << std::endl;
	out << "***************************BEGIN***************************" << std::endl;
	out << "Number of unbind particles = " << NPart << std::endl;
	out << "*********************Particles*********************" << std::endl;
	for (int i = 0; i < NPart; i++){
		out << "PID = " << Part_PID[i]  << std::endl;
		out << "MOMENTUM (Px,Py,Pz,E) = ( " << Part_PX[i] << ", " << Part_PY[i] << ", " << Part_PZ[i] << ", " << Part_E[i] << " )" << std::endl;
		out << "Kinetic Energy = " << Part_K[i]  << std::endl;
	}
	out << "****************************END****************************" << std::endl << std::endl;
}


void Evap_part::set_momentum(Double_t p_x, Double_t p_y, Double_t p_z){
	dir_PX = p_x; 
	dir_PY = p_y;
	dir_PY = p_z;
}

void Evap_part::set_momentum(Double_t p){
	dir_PX = p; 
	dir_PY = p;
	dir_PY = p;
}


void Evap_part_l::set_momentum(Double_t p_x, Double_t p_y, Double_t p_z){
	dir_PX = p_x; 
	dir_PY = p_y;
	dir_PY = p_z;
}

void Evap_part_l::set_momentum(Double_t p){
	dir_PX = p; 
	dir_PY = p;
	dir_PY = p;
}

