/* ================================================================================
 * 
 * 	Copyright 2009 Júlio Cesar
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#ifndef NameTable_pHH
#define NameTable_pHH

#include <map>
#include <string>
#include <sstream>

class NameTable {

	std::map< int, std::string > table;
	
public:
	NameTable()
	{
		push(0, "n");
		push(1, "H");
		push(2, "He");
		push(3, "Li");
		push(4, "Be");
		push(5, "B");
		push(6, "C");
		push(7, "N");
		push(8, "O");
		push(9, "F");
		push(10, "Ne");
		push(11, "Na");
		push(12, "Mg");
		push(13, "Al");
		push(14, "Si");
		push(15, "P");
		push(16, "S");
		push(17, "Cl");
		push(18, "Ar");
		push(19, "K");
		push(20, "Ca");
		push(21, "Sc");
		push(22, "Ti");
		push(23, "V");
		push(24, "Cr");
		push(25, "Mn");
		push(26, "Fe");
		push(27, "Co");
		push(28, "Ni");
		push(29, "Cu");
		push(30, "Zn");
		push(31, "Ga");
		push(32, "Ge");
		push(33, "As");
		push(34, "Se");
		push(35, "Br");
		push(36, "Kr");
		push(37, "Rb");
		push(38, "Sr");
		push(39, "Y");
		push(40, "Zr");
		push(41, "Nb");
		push(42, "Mo");
		push(43, "Tc");
		push(44, "Ru");
		push(45, "Rh");
		push(46, "Pd");
		push(47, "Ag");
		push(48, "Cd");
		push(49, "In");
		push(50, "Sn");
		push(51, "Sb");
		push(52, "Te");
		push(53, "I");
		push(54, "Xe");
		push(55, "Cs");
		push(56, "Ba");
		push(57, "La");
		push(58, "Ce");
		push(59, "Pr");
		push(60, "Nd");
		push(61, "Pm");
		push(62, "Sm");
		push(63, "Eu");
		push(64, "Gd");
		push(65, "Tb");
		push(66, "Dy");
		push(67, "Ho");
		push(68, "Er");
		push(69, "Tm");
		push(70, "Yb");
		push(71, "Lu");
		push(72, "Hf");
		push(73, "Ta");
		push(74, "W");
		push(75, "Re");
		push(76, "Os");
		push(77, "Ir");
		push(78, "Pt");
		push(79, "Au");
		push(80, "Hg");
		push(81, "Tl");
		push(82, "Pb");
		push(83, "Bi");
		push(84, "Po");
		push(85, "At");
		push(86, "Rn");
		push(87, "Fr");
		push(88, "Ra");
		push(89, "Ac");
		push(90, "Th");
		push(91, "Pa");
		push(92, "U");
		push(93, "Np");
		push(94, "Pu");
		push(95, "Am");
		push(96, "Cm");
		push(97, "Bk");
		push(98, "Cf");
		push(99, "Es");
		push(100, "Fm");
		push(101, "Md");
		push(102, "No");
		push(103, "Lr");
		push(104, "Rf");
		push(105, "Db");
		push(106, "Sg");
		push(107, "Bh");
		push(108, "Hs");
		push(109, "Mt");
		push(110, "Uun");
		push(111, "Uuu");
		push(112, "Uub");
		push(113, "Uut");
		push(114, "Uuq");
		push(115, "Uup");
		push(116, "Uuh");
		push(117, "Uus");
		push(118, "Uuo");
	}
	
	void push( int index, std::string value )
	{
		table[index] = value;
	}
	
	std::string get(int Z)
	{
		std::map< int, std::string >::iterator it = table.find(Z);
		if( it == table.end() )
			return std::string("??");
		else
			return table[Z];
	}
	
	std::string get(int Z, int A)
	{
		std::stringstream ss;
		ss << get(Z) << A;
		return ss.str();
	}
	
	
};

#endif // NameTable_pHH

