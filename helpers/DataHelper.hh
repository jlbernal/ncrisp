/* ================================================================================
 * 
 * 	Copyright 2012, 2014, 2015 Israel Gonzalez, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __DataHelper_pHH
#define __DataHelper_pHH

#include "TString.h"
#include "TFile.h"
#include "TNtuple.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

const Int_t MAXNUC = 250;
const Int_t MAXLEV = 30;
const Int_t MAXPRC = 10;
const Int_t MAXUNBND = 500;

struct DataHelper_struct{
	TFile *rootFile;
	TNtuple *tuple;
	Float_t* columns;
	TString *varList;
	Int_t columnsLength;
	DataHelper_struct(const char* var_List, const char* tupleName = "default.root");
	virtual ~DataHelper_struct(){
		// std::cout << "dying ..." << std::endl;
		if(varList) delete varList;
		if(columns) delete columns;
		if(tuple) delete tuple;

		if(rootFile){ rootFile->Close(); delete rootFile; }
	}
	Int_t ReadDataFrom(const char* fileName);  
};
	struct Nuc_Struct{
		Int_t A, Z;
		Int_t PLevels[MAXLEV];
		Int_t NLevels[MAXLEV];
		Int_t Nuc_IDX[MAXNUC];
		Int_t Nuc_PID[MAXNUC];
		bool Nuc_IsB[MAXNUC];
		Double_t Nuc_X[MAXNUC];
		Double_t Nuc_Y[MAXNUC];
		Double_t Nuc_Z[MAXNUC];
		Double_t Nuc_P[MAXNUC];
		Double_t Nuc_PX[MAXNUC];
		Double_t Nuc_PY[MAXNUC];
		Double_t Nuc_PZ[MAXNUC];
		Double_t Nuc_E[MAXNUC];
		Double_t Nuc_K[MAXNUC];
		void Print();
		void Print_Levels();
		void CPT_Print(ofstream &out);
	} ;
	struct  Mes_Struct{
		Int_t NM;
		Int_t Mes_KEY[MAXNUC];
		Int_t Mes_PID[MAXNUC];
		bool Mes_IsB[MAXNUC];
		Double_t Mes_X[MAXNUC];
		Double_t Mes_Y[MAXNUC];
		Double_t Mes_Z[MAXNUC];
		Double_t Mes_P[MAXNUC];
		Double_t Mes_PX[MAXNUC];
		Double_t Mes_PY[MAXNUC];
		Double_t Mes_PZ[MAXNUC];
		Double_t Mes_E[MAXNUC];
		Double_t Mes_K[MAXNUC];
		void Print();
		void CPT_Print(ofstream &out);
	};
struct  Lep_Struct{
		Int_t NM;
		Int_t Lep_KEY[MAXNUC];
		Int_t Lep_PID[MAXNUC];
		bool Lep_IsB[MAXNUC];
		Double_t Lep_X[MAXNUC];
		Double_t Lep_Y[MAXNUC];
		Double_t Lep_Z[MAXNUC];
		Double_t Lep_P[MAXNUC];
		Double_t Lep_PX[MAXNUC];
		Double_t Lep_PY[MAXNUC];
		Double_t Lep_PZ[MAXNUC];
		Double_t Lep_E[MAXNUC];
		Double_t Lep_K[MAXNUC];
		void Print();
		void CPT_Print(ofstream &out);
	};
	struct Proc_Struct{
		Int_t ProN;
		Double_t TIME;
		Int_t I1,I2;
		Int_t NPartIn;
		Int_t NTotal;
		Int_t Proc_PID[MAXPRC];
		bool Proc_IsB[MAXPRC];
		bool Proc_IsF[MAXPRC]; // (Is Final) false = initial particles, true final particles
		Double_t Proc_X[MAXPRC];
		Double_t Proc_Y[MAXPRC];
		Double_t Proc_Z[MAXPRC];
		Double_t Proc_P[MAXPRC];
		Double_t Proc_PX[MAXPRC];
		Double_t Proc_PY[MAXPRC];
		Double_t Proc_PZ[MAXPRC];
		Double_t Proc_E[MAXPRC];
		Double_t Proc_K[MAXPRC];
		void Print();
		void CPT_Print(ofstream &out);
	};

	struct UnBindPart_Struct{
		Int_t NPart;
		Int_t Part_PID[MAXUNBND];
		Double_t Part_PX[MAXUNBND];
		Double_t Part_PY[MAXUNBND];
		Double_t Part_PZ[MAXUNBND];
		Double_t Part_E[MAXUNBND];
		Double_t Part_K[MAXUNBND];
		void Print();
		void CPT_Print(ofstream &out);
	};

	
	struct Fission{
	  Int_t fiss_A;
	  Int_t fiss_Z; 
	  Double_t fiss_E;//final configuration
	  Double_t fissility;
	  Int_t fiss_neutron;
	  Int_t fiss_proton;
	  Int_t fiss_alpha; //multiplicities
	};
	struct Spallation{
	  Int_t spall_A;
	  Int_t spall_Z; //final configuration
	  Double_t spall_E;
	  Int_t spall_neutron;
	  Int_t spall_proton;
	  Int_t spall_alpha; //multiplicities
	};

struct Spallation_l{
	  Int_t spall_A;
	  Int_t spall_Z; //final configuration
	  Int_t spall_neutron;
	  Int_t spall_proton;
	  Int_t spall_alpha; //multiplicities
	  Double_t spall_q;
          Double_t spall_qes;
	  Double_t l;
	};


struct Fission_l{
	  Int_t fiss_A;
	  Int_t fiss_Z; 
	  Double_t fiss_E;//final configuration
	  Double_t fissility;
	  Int_t fiss_neutron;
	  Int_t fiss_proton;
	  Int_t fiss_alpha; //multiplicities
          Double_t fiss_q;
          Double_t fiss_qes;
	  Double_t fiss_l;
	  double E0;
	  int iteration;
	  
  
	};

	
	
struct Evap_part{
	  Double_t p_energy;
	  Double_t dir_PX; 
	  Double_t dir_PY;
	  Double_t dir_PZ; 
	
	void set_momentum(Double_t px, Double_t py, Double_t pz);
	void set_momentum(Double_t p);

	};

	struct Evap_part_l{
	  Double_t q_gs;
	  Double_t q_sd;
	  Double_t q;
	  Double_t p_energy;
	  Double_t dir_PX; 
	  Double_t dir_PY;
	  Double_t dir_PZ;
	  Double_t e_time; 
	  int iteration;
	  
	
	void set_momentum(Double_t px, Double_t py, Double_t pz);
	void set_momentum(Double_t p);

	};
	
	
typedef DataHelper_struct DataHelper;


#endif
