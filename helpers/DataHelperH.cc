/* ================================================================================
 * 
 * 	Copyright 2012 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "DataHelperH.hh"
//need to be very careful with this class.
DataHelper_Hyp::DataHelper_Hyp (const char* fileName, const char* varList)
{
	this->ifs.open (fileName);
	if (!this->ifs.good ()) std::cout << "File Location Error " << std::endl;
	else{
		 std::cout << " File was sucessfully opened"<< std::endl;
		this->N_Col = -1;
		this->tuple = new TNtuple ("Tuple", "Dist", varList);
		this->lista = TString(varList);
		TObjArray *vars = this->lista.Tokenize (TString (":"));
		this->columnsLength = vars->GetEntriesFast ();
		delete vars;
	}
}

DataHelper_Hyp::~DataHelper_Hyp ()
{
	tuple->Delete();
	ifs.close();
}

Int_t DataHelper_Hyp::Get_NCol(){
	return N_Col;
}

void DataHelper_Hyp::ReadData () // for a correct reading it must not be clear line between
{
	this->ifs.seekg(0,std::ios::beg);// Set Cursor on the begin
	this->tuple->Reset();
	Float_t* columns = new Float_t [columnsLength];
	while (true) {      
		if (!this->ifs.good ()) break;  
		std::string pdline;
		getline(this->ifs, pdline);            
		char c = pdline.c_str ()[0]; // pick a line   
		if (c != '#'){    
			std::istringstream __istr (pdline.c_str ());            
			for (int h = 0; h < this->columnsLength; h++) __istr >> columns [h];
			this->tuple->Fill (columns);
			this->N_Col++;     
		 }
	}
	delete columns;
} 
void DataHelper_Hyp::PrintAll_Tuple(){
	for (int i = 0; i < this->N_Col; i++){
		this->tuple->Show(i);
		std::cout << "\n";
	}
}
void DataHelper_Hyp::Print1_Tuple(Int_t t){
	if (t <= N_Col)	
		this->tuple->Show(t);
	else 
		std::cout << " Index Error...... Index must be menor than " << this-> N_Col << std::endl;
}

Float_t DataHelper_Hyp::Get_tuple_1Value(const char* Branch, Int_t Index ){
	if (Index <= N_Col){
		Float_t x1;
		this->tuple->SetBranchAddress(Branch, &x1);
		this->tuple->GetEntry(Index);
		this->tuple->ResetBranchAddresses();
//		std::cout << "El valor del TBranch " << Branch << ", x = " << x1  << ", y = " << y << std::endl;
		return x1;
	} else {
		std::cout << " Index Error...... Index must be menor than " << this-> N_Col << std::endl;
		return 0.0;
	}
}

void DataHelper_Hyp::Get_tuple_2Value(const char* Branchx, const char* Branchy, Float_t &x, Float_t &y, Int_t Index ){
	if (Index <= N_Col){
		Float_t x1,y1;
		this->tuple->SetBranchAddress(Branchx, &x1);
		this->tuple->SetBranchAddress(Branchy, &y1);
		this->tuple->GetEntry(Index);
		x = x1;
		y = y1;
		this->tuple->ResetBranchAddresses();
	} else 
		std::cout << " Index Error...... Index must be menor than " << this-> N_Col << std::endl;
}
void DataHelper_Hyp::Get_tuple_3Value(const char* Branchx, const char* Branchy1, const char* Branchy2, Float_t &x, Float_t &y1, Float_t &y2, Int_t Index ){
	if (Index <= N_Col){
		Float_t x1,y11,y12;
		this->tuple->SetBranchAddress(Branchx, &x1);
		this->tuple->SetBranchAddress(Branchy1, &y11);
		this->tuple->SetBranchAddress(Branchy2, &y12);
		this->tuple->GetEntry(Index);
		x = x1;
		y1 = y11;
		y2 = y12;
		this->tuple->ResetBranchAddresses();
	} else 
		std::cout << " Index Error...... Index must be menor than " << this-> N_Col << std::endl;
}

TGraphErrors *DataHelper_Hyp::GetTGraphErrorsN(const char* branchx, const char* branchy, Int_t N_Initial, Int_t _PASS, bool Ask_Print, Float_t X_Err, Float_t Y_Err){
	TGraphErrors *T = new TGraphErrors;
	if ((_PASS < 1) ||(_PASS > this->N_Col)){
		std::cout << "_PASS must be  >= 1 and  < " << this->N_Col << std::endl;
		return NULL;
	}else {
		Float_t x,y;
		Int_t I_TN = N_Initial, I_TG = 0;
		this->Get_tuple_2Value(branchx, branchy, x, y, I_TN);
		T->SetPoint(0,x,y);
		T->SetPointError(0, 0.,0.);
		while ( I_TN + _PASS < this->N_Col) {
			I_TN = I_TN + _PASS;
			I_TG++;
			this->Get_tuple_2Value(branchx, branchy, x, y, I_TN);
			if ( Ask_Print )	std::cout << "los valores que entran al tuple son " << x << " y  " << y << std::endl;
			T->SetPoint(I_TG,x,y);
			T->SetPointError(I_TG, X_Err, Y_Err);
		}
		return T;
	}
}

TGraphErrors *DataHelper_Hyp::GetTGraphErrorsCombN(const char* branchx, const char* branchy1, const char* branchy2, Int_t N_Initial, Int_t _PASS, bool Ask_Print, Float_t X_Err, Float_t Y_Err)
{
	TGraphErrors *T = new TGraphErrors;
	if ((_PASS < 1) ||(_PASS > this->N_Col)){
		std::cout << "_PASS must be  >= 1 and  < " << this->N_Col << std::endl;
		return NULL;
	}else {
		Float_t x,y1,y2;
		Int_t I_TN = N_Initial, I_TG = 0;
		this->Get_tuple_3Value(branchx, branchy1, branchy2, x, y1, y2, I_TN);
		T->SetPoint(0, x ,y1 + y2);
		T->SetPointError(0, 0.,0.);
		while ( I_TN + _PASS < this->N_Col) {
			I_TN = I_TN + _PASS;
			I_TG++;
			this->Get_tuple_3Value(branchx, branchy1, branchy2, x, y1, y2, I_TN);
			if ( Ask_Print ) std::cout << "los valores que entran al tuple son " << x << "   " << y1 << "  " << y2 << std::endl;
			T->SetPoint(I_TG,x,y1+y2);
			T->SetPointError(I_TG, X_Err, Y_Err);
		}
		return T;
	}
}

TGraph2D *DataHelper_Hyp::GetTGraph2D(const char* branchx, const char* branchy, const char* branchz, bool Ask_Print)
{
	TGraph2D *T = new TGraph2D;
	Float_t x,y,z;
	for (int i = 0; i < this->N_Col; i++){
		this->Get_tuple_3Value(branchx, branchy, branchz, x, y, z, i);
		T->SetPoint(i, x, y, z);
		if ( Ask_Print ) std::cout << "los valores de entrada [" << i << "]  al tuple son: "<< TString(branchx) << " = " << x << ", " << TString(branchy) << " = " << y << ", " << TString(branchz)  << " = " <<  z << std::endl;
	}
	return T;
}

