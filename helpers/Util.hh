/* ================================================================================
 * 
 * 	Copyright 2009 Júlio Cesar
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#ifndef Util_pHH
#define Util_pHH

#include "TAxis.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TImage.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TSystem.h"




#define SAFE_DELETE(x)			if(x) { delete x; x = 0; }
#define SAFE_DELETE_ARRAY(x)	if(x) { delete[] x; x = 0; }


// NaN == Not a Number
template< typename R > // R should be float or double or long double
bool isNaN( R r )
{
	return r != r;
}


// procura um elemento num vector de inteiros e retorna indice se encontrou ou -1 se nao encontrou
template< typename T >
int LinearVectorFind( std::vector< T > v, T i )
{
	for(unsigned int a(0); a < v.size(); a++)
		if(v[a] == i)
			return a;
	return -1;
}




void StringExplode(std::string str, std::string separator, std::vector< std::string >& results);


TGraphErrors MultFillTGraphYError( const char* filename, Double_t energy_min, Double_t energy_max);
TGraphErrors MultFillTGraph( const char* filename, Double_t energy_min, Double_t energy_max);

void ReadCascadeLine( std::istream& ifs, Double_t &Energy, Int_t &A, Int_t &Z, Int_t &Counts, Double_t &ExEnergy );

void SaveGraph( TGraph *tg, std::string titulo, char DrawOption[] = "AP" );
void SaveMultiGraph( TMultiGraph *tg, std::string titulo );

double cint(double x);

#endif // Util_pHH

