/* ================================================================================
 * 
 * 	Copyright 2011 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#include "TMath.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TPaletteAxis.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

double GetF(double xo)
{
	const double fission_parm[48] = { 0.00000, 0.00001,  0.00005,  0.00016,
		0.00037, 0.00073,  0.00126,  0.00201,
		0.00303, 0.00436,  0.00606,  0.00819,
		0.01084, 0.01409,  0.01808,  0.02293, 
		0.02876, 0.03542,  0.04258,  0.04997, 
		0.05747, 0.06503,  0.07260,  0.08017, 
		0.08773, 0.09526,  0.10276,  0.11023, 
		0.11765, 0.12502,  0.13235,  0.13962, 
		0.14684, 0.15400,  0.16110,  0.16814, 
		0.17510, 0.18199,  0.18880,  0.19553,
		0.20217, 0.20871,  0.21514,  0.22144, 
		0.22761, 0.23363,  0.23945,  0.24505 
	};
	double X = 1.;
	const double d = .02;   
	int i = 0;  
	for ( i = 0; i < 48; i++ ) {
		if ( xo > X ) 
			break;     
		X -= d;
	}  
	double X_1 = X - d;
	double fo = 0.;
	if ( i < 47 ) 
		fo = ( (xo - X_1) * fission_parm[i] + (X - xo) * fission_parm[i+1]) / d;
	else 
		fo = fission_parm[47];  
	return fo;
}

double FissionBarrierCorrection(int A)
{
	double x = A;
	const double a1 = 18.03, a2 = 101.8, a3 = 934.6, a4 = 822.3, 
		a5 = 30.34, a6 = 341.4, a7 = 736.5,
		b1 = 10.92, b2 = 19.77, b3 = 47.3, b4 = 94.15, b5 = 122.8,
		b6 = 137.2, b7 = 167.5, 
		c1 = 7.319, c2 = 17.01, c3 = 40.18, c4 = 37.25, c5 = 22.5,
		c6 = 35.46, c7 = 67.54,
		p1 = -0.0003578, p2 = 0.2026, p3 = -28.03, p4 = -230.9; 

	return p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
		a1 * exp(-pow(((x - b1) / c1), 2)) +
		a2 * exp(-pow(((x - b2) / c2), 2)) +
		a3 * exp(-pow(((x - b3) / c3), 2)) +
		a4 * exp(-pow(((x - b4) / c4), 2)) +
		a5 * exp(-pow(((x - b5) / c5), 2)) +
		a6 * exp(-pow(((x - b6) / c6), 2)) +
		a7 * exp(-pow(((x - b7) / c7), 2));
}

double CalculateBf_Antigo(int A, int Z)
{
	double Bf = 0.;

	int N = A - Z;
	const double as = 17.9439;  //MeV
	const double k  = 1.7826;

	double Es = 1. - k *  pow( (double)(N - Z)/(double)A, 2.0);
	Es = as * Es * pow(A, (2./3.) );

	double p1 = 50.88;
	double p2 = 1.7826;
	double xo = ( (double)(Z * Z)/(double)A ) / 
		( p1 * (1. - p2 * pow( (double)(N-Z)/(double)A, 2.0) ) );

	if( xo <= 0.06 ) 
		xo = 0.06;

	double fo = GetF(xo);  
	Bf = fo * Es;	// *
	
	double Zo = (double)A / ( 1.98 + 0.015 * pow( (double)A, 2.0/3.0 ) );

	Bf = Bf + FissionBarrierCorrection(A);// * exp(pbf1 * pow( ((double)Z - Zo), 2.0 )); //-0.34  //-0.25 // *

	return Bf;

}

double BarrierCorrection(int A, int Z, double R, double Ao, double Zo, double sigA, double sigZ){

	return R * exp( - ( ((A-Ao)*(A-Ao))/(2.*sigA*sigA) + ((Z-Zo)*(Z-Zo))/(2.*sigZ*sigZ) ) );
}

int Barreira_Fissao_Dif(){ //interpretado
//int main(){				//compilado

	TCanvas *c = new TCanvas("c","barreira de fissao",200,100,900,600);
	c->SetFillColor(10);
	c->SetBorderMode(0);
   c->SetGrid();
	//TGraph *gr = new TGraph();
	//TGraph *gr2 = new TGraph();
	//TGraph *gr3 = new TGraph();
	TH2D *h = new TH2D("h","Barreira de Fissao em MeV (NIX com correcao)",25,75,100,110,190,300);

	//ajuste para todo o intervalo (modelo novo)
/*	double par[11] = {   3.75400e+02 ,
    						    -1.37942e+01 ,
      						 1.63045e+00 ,
     							  6.56994e+00 , 
								4.14125e-02 ,
								-1.24359e-03,
								 2.35406e+01,
								 2.09123e+02	,
								 7.48396e+01	,
								 4.63939e+00,
								6.02541e+00 };
*/
	//ajuste com restrição de intervalo (modelo novo)
	double par[11] = {   1.15209e+03 , 
    							-1.56042e+01 ,
      						 -4.69923e+00,
     							 8.60587e+00 , 
								4.21750e-02,
								1.31525e-02,
								7.11532e+05,				//R
								2.12321e+02,				//Ao
								4.99741e+01,				//Zo
								2.90635e+00,				//sigA
								-6.92444e+00};				//sigZ


	//AJUSTE DA CORREÇÃO NO MODELO ANTIGO SEM RESTRIÇÃO NO INTERVALO
	//double par[5] = {  	-3.50117e+09,				//R
	//							-4.97603e+02,				//Ao
	//							6.78389e+01,				//Zo
	//						1.15426e+02,				//sigA
	//						4.63522e+02};				//sigZ

/*	//AJUSTE DA CORREÇÃO NO MODELOA ANTIGO COM RESTRIÇÃO DE INTERVALO
	double par[5] = { -1.96109e+06 ,				//R
								-2.93509e+07,				//Ao
								2.81287e+07,				//Zo
							9.69921e+06,				//sigA
							7.32800e+06};				//sigZ
*/
	long double delta = 0;

	int i = 0;
	int Z = 0, A = 0, N = 0;
	double B_exp = 0., B_mod = 0., B_in = 0., B_ex = 0.;

   ifstream ifs("exp_data/mcef/fis-barrier-etfsi_int_ext.dat");

   ofstream ofs("barreira_ETFSI.dat");

	while ( ifs.good() ) {

		ifs >> Z >> A >> B_in >> B_ex;

		if ( B_in <= B_ex ){
			B_exp = B_ex;
		}
		else{
			B_exp = B_in;
		}

		B_mod = CalculateBf_Antigo(A, Z);
		 //  		+ BarrierCorrection(A, Z, par[0], par[1], par[2], par[3], par[4]);

		//B_mod = par[0] + par[1]*Z + par[2]*A + par[3]*pow(Z,2.0)/A + par[4]*pow(Z,2.0) + par[5]*pow(A,2.0)
			//	  + BarrierCorrection(A, Z, par[6], par[7], par[8], par[9], par[10]);

		h->Fill(Z,A,B_exp - B_mod);

		ofs << Z << "\t" << A << "\t" << B_exp << endl;
	
		i++;

		//delta = 0;
	}

	//h->GetXaxis()->SetTitle("A");
	//h->GetYaxis()->SetTitle("M");

	h->GetXaxis()->SetTitle("Z");
	h->GetYaxis()->SetTitle("A");
	h->GetZaxis()->SetTitle("(MeV)");
	h->GetXaxis()->SetLabelSize(0.03);
	h->GetYaxis()->SetLabelSize(0.03);
	h->GetZaxis()->SetLabelSize(0.03);


//	TMultiGraph *mg = new TMultiGraph();
//	mg->Add(gr, "P");
//	mg->Add(gr2, "L");
//	mg->Add(gr3, "L");

//	gr->SetMarkerStyle(20);
//	gr->SetMarkerSize(1.);
	//gr->Draw("AP");

//	gr2->SetLineColor(2);
//	gr3->SetLineColor(3);

//	mg->GetXaxis()->SetRangeUser(0.,290.);
//	mg->GetYaxis()->SetRangeUser(925.,945.);
//	mg->Draw("a");

	gStyle->SetOptTitle(kFALSE);

	h->Draw("contz");
/*
	TFile f("mass_diference_pearson.root","recreate");
	//gr->Write();
	h->Write();
	f.Close();
*/
	gStyle->SetPalette(1);
	gStyle->SetOptStat(kFALSE);

	//gStyle->SetStatX(0.35);
	//gStyle->SetStatY(0.85);



	//c->Print("Mass_Diference.eps");
}
  
