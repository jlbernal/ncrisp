/* ================================================================================
 * 
 * 	Copyright 2011 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//================================================================================================================
//	This code makes the fit of a surface to fission barrier experimental points using TMinuit.h
//	
// 	List of Minuit commands: http://wwwasdoc.web.cern.ch/wwwasdoc/minuit/node18.html
//
//================================================================================================================

#include "TMinuit.h"
#include "TMath.h"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

int countCall = 0;

double GetF(double xo)
{
	const double fission_parm[48] = { 0.00000, 0.00001,  0.00005,  0.00016,
		0.00037, 0.00073,  0.00126,  0.00201,
		0.00303, 0.00436,  0.00606,  0.00819,
		0.01084, 0.01409,  0.01808,  0.02293, 
		0.02876, 0.03542,  0.04258,  0.04997, 
		0.05747, 0.06503,  0.07260,  0.08017, 
		0.08773, 0.09526,  0.10276,  0.11023, 
		0.11765, 0.12502,  0.13235,  0.13962, 
		0.14684, 0.15400,  0.16110,  0.16814, 
		0.17510, 0.18199,  0.18880,  0.19553,
		0.20217, 0.20871,  0.21514,  0.22144, 
		0.22761, 0.23363,  0.23945,  0.24505 
	};
	double X = 1.;
	const double d = .02;   
	int i = 0;  
	for ( i = 0; i < 48; i++ ) {
		if ( xo > X ) 
			break;     
		X -= d;
	}  
	double X_1 = X - d;
	double fo = 0.;
	if ( i < 47 ) 
		fo = ( (xo - X_1) * fission_parm[i] + (X - xo) * fission_parm[i+1]) / d;
	else 
		fo = fission_parm[47];  
	return fo;
}

double FissionBarrierCorrection(int A)
{
	double x = A;
	const double a1 = 18.03, a2 = 101.8, a3 = 934.6, a4 = 822.3, 
		a5 = 30.34, a6 = 341.4, a7 = 736.5,
		b1 = 10.92, b2 = 19.77, b3 = 47.3, b4 = 94.15, b5 = 122.8,
		b6 = 137.2, b7 = 167.5, 
		c1 = 7.319, c2 = 17.01, c3 = 40.18, c4 = 37.25, c5 = 22.5,
		c6 = 35.46, c7 = 67.54,
		p1 = -0.0003578, p2 = 0.2026, p3 = -28.03, p4 = -230.9; 

	return p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
		a1 * exp(-pow(((x - b1) / c1), 2)) +
		a2 * exp(-pow(((x - b2) / c2), 2)) +
		a3 * exp(-pow(((x - b3) / c3), 2)) +
		a4 * exp(-pow(((x - b4) / c4), 2)) +
		a5 * exp(-pow(((x - b5) / c5), 2)) +
		a6 * exp(-pow(((x - b6) / c6), 2)) +
		a7 * exp(-pow(((x - b7) / c7), 2));
}

double CalculateBf_Antigo(int A, int Z)
{
	double Bf = 0.;

	int N = A - Z;
	const double as = 17.9439;  //MeV
	const double k  = 1.7826;

	double Es = 1. - k *  pow( (double)(N - Z)/(double)A, 2.0);
	Es = as * Es * pow(A, (2./3.) );

	double p1 = 50.88;
	double p2 = 1.7826;
	double xo = ( (double)(Z * Z)/(double)A ) / 
		( p1 * (1. - p2 * pow( (double)(N-Z)/(double)A, 2.0) ) );

	if( xo <= 0.06 ) 
		xo = 0.06;

	double fo = GetF(xo);  
	Bf = fo * Es;	// *
	
	double Zo = (double)A / ( 1.98 + 0.015 * pow( (double)A, 2.0/3.0 ) );

	Bf = Bf + FissionBarrierCorrection(A);// * exp(pbf1 * pow( ((double)Z - Zo), 2.0 )); //-0.34  //-0.25 // *

	return Bf;

}

double BarrierCorrection(int A, int Z, double R, double Ao, double Zo, double sigA, double sigZ){

	return R * exp( - ( ((A-Ao)*(A-Ao))/(2.*sigA*sigA) + ((Z-Zo)*(Z-Zo))/(2.*sigZ*sigZ) ) );
}

void ChiSquare(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){

	++countCall;

	long double ChiSqr = 0,
					chisquare = 0;

	long double delta = 0;

	int i = 0;
	int Z = 0, A = 0, N = 0;
	double B_exp = 0., B_mod = 0., B_in = 0., B_ex = 0.;

   ifstream ifs("exp_data/mcef/fis-barrier-etfsi_int_ext.dat");

	TString inStr;
	TString delim = " ";

	while ( ifs.good() ) {

		ifs >> Z >> A >> B_in >> B_ex;

		if( (Z>= 75 && Z<=95) && (A>=190 && A<=240) ){
		//MODELO ANTIGO
		B_mod = CalculateBf_Antigo(A, Z)
				  + BarrierCorrection(A, Z, par[0], par[1], par[2], par[3], par[4]);

/*
		//MODELO NOVO
		if( (Z>= 75 && Z<=95) && (A>=190 && A<=240) ){
	
			if ( B_in <= B_ex ){
				B_exp = B_ex;
			}
			else{
				B_exp = B_in;
			}

			//cout << "Z: " << Z << " A: " << A << " B_in: " << B_in << " B_ex: " << B_ex << " B_exp: " << B_exp << endl;

			B_mod = par[0] + par[1]*Z + par[2]*A + par[3]*pow(Z,2.0)/A + par[4]*pow(Z,2.0) + par[5]*pow(A,2.0)
					  + BarrierCorrection(A, Z, par[6], par[7], par[8], par[9], par[10]);
*/
			delta = (B_exp - B_mod) / 1.;

			chisquare += (delta*delta);

			//cout << "chis: " << delta << endl;

			i++;

		}
	}


	ChiSqr = chisquare / (double)( i-1. - npar );

	f = ChiSqr;
}



//int main(){  //compilado
int Barreira_Fissao_Minuit(){    //interpretado


	TMinuit min(15);

	min.SetPrintLevel(3);
	//  select verbose level:
  	//    default :     (58 lines in this test)
  	//    -1 : minimum  ( 4 lines in this test)
  	//     0 : low      (31 lines)
  	//     1 : medium   (61 lines)
  	//     2 : high     (89 lines)
  	//     3 : maximum (199 lines in this test)

	min.SetFCN(ChiSquare);

	static const int arg = 2;
	double arglist[arg];

	int ierflg = 0;

	//Definição do parâmetro de erro UP
  	arglist[0] = 1; //UP=1 para chisquare
  	min.mnexcm("SET ERR", arglist ,1 , ierflg);


//MODELO NOVO

  	//Configurando parâmetros, starts, steps e limites
	static const int nPar = 11;

//CONJUNTO DE PARÂMETROS
//________________________________________________________________

/*
	double vstart[nPar] = {   1.15209e+03 , 
    							    -1.56042e+01 ,
      							  -4.69923e+00,
     								  8.60587e+00 , 
										4.21750e-02,
										1.31525e-02,
										7.11532e+05,				//R
										2.12321e+02,				//Ao
										4.99741e+01,				//Zo
										2.90635e+00,				//sigA
										-6.92444e+00};				//sigZ

//_______________________________________________________________________________________


  	//double step[nPar] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
	double step = 0.01;
  	min.mnparm(0, "p1", vstart[0], step, 0, 0, ierflg); 
  	min.mnparm(1, "p2", vstart[1], step, 0, 0, ierflg);  
  	min.mnparm(2, "p3", vstart[2], step, 0, 0, ierflg);  
  	min.mnparm(3, "p4", vstart[3], step, 0, 0, ierflg);  
	min.mnparm(4, "p5", vstart[4], step, 0, 0, ierflg);
  	min.mnparm(5, "p6", vstart[5], step, 0, 0, ierflg);  

	//parâmetros da correção
  	min.mnparm(6, "R", vstart[6], step, 0, 0, ierflg);   
  	min.mnparm(7, "Ao", vstart[7], step, 0, 0, ierflg);   
  	min.mnparm(8, "Zo", vstart[8], step, 0, 0, ierflg);
  	min.mnparm(9, "sigA", vstart[9], step, 0, 0, ierflg);  
  	min.mnparm(10, "sigZ", vstart[10], step, 0, 0, ierflg);   


	min.FixParameter(0);
	min.FixParameter(1);         
	min.FixParameter(2);
	min.FixParameter(3);
	min.FixParameter(4);
	min.FixParameter(5);

//	min.FixParameter(6);
//	min.FixParameter(7);
//	min.FixParameter(8);
//	min.FixParameter(9);
//	min.FixParameter(10);   
*/

//MODELO ANTIGO

  	//Configurando parâmetros, starts, steps e limites
	static const int nPar = 5;

//CONJUNTO DE PARÂMETROS
//________________________________________________________________

	double vstart[nPar] = {  	3.,				//R
										208,				//Ao
										82.,				//Zo
										2.90635e+00,				//sigA
										6.92444e+00};				//sigZ

//_______________________________________________________________________________________


  	//double step[nPar] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
	double step = 0.01;

	//parâmetros da correção
  	min.mnparm(0, "R", vstart[0], step, 0, 0, ierflg);   
  	min.mnparm(1, "Ao", vstart[1], step, 0, 0, ierflg);   
  	min.mnparm(2, "Zo", vstart[2], step, 0, 0, ierflg);
  	min.mnparm(3, "sigA", vstart[3], step, 0, 0, ierflg);  
  	min.mnparm(4, "sigZ", vstart[4], step, 0, 0, ierflg);   


//	min.FixParameter(0);
//	min.FixParameter(1);         
//	min.FixParameter(2);
//	min.FixParameter(3);
//	min.FixParameter(4);



	cout << "\n Minimizacao..." << endl << endl;

  	//Minimização
  	arglist[0] = 3; //level
	min.mnexcm("SET PRIntout", arglist, 1, ierflg);

	//min.mnexcm("SCAN", 0, 0, ierflg);

	//Configuração do MINImize. Se MIGrad não converge, muda para SIMplex 
	//0.001*[tolerance]*UP
	//UP=1 para chisquare (comando SET ERR)
  	arglist[0] = 20000; //máximo de chamadas
  	arglist[1] = 0.001;  //tolerance
  	//min.mnexcm("MINI", arglist, 2, ierflg);
	//min.mnexcm("SIMplex", arglist, 2, ierflg);
	min.mnexcm("MIGrad", arglist, 2, ierflg);

	//Busca por mínimo local adicional - Comando IMProve
	//arglist[0] = 100; //máximo de chamadas
  	//min->mnexcm("IMP", arglist ,1 , ierflg);

	return 0;
}
  
