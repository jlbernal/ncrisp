/* ================================================================================
 * 
 * 	Copyright 2008, 2012, 2015 Israel González, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "cascade.hh"

using namespace std;
DataStruct::DataStruct(){
	IsDataOk = false;
	CascNumber = 0;
}
void DataStruct::ReadData(fstream &file){
	while (true) {      
		if (!file.good ()) break;  
		string pdline, Stemp;
		double Dtemp;
		int Itemp;
		bool Btemp;
		getline(file, pdline);
		char c = pdline.c_str()[0];
		if (c == '$'){
			string text;     
			istringstream istr(pdline.c_str() );      
			istr >> text;
			if ( text == "$NucleusName" ) {	
				istr >> Stemp;   
				nucleusName.push_back(Stemp);      
			}
			if ( text == "$MassIndex" ) {	
				istr >> Itemp; 
				A.push_back(Itemp);      
			}
			if ( text == "$NucleusCharge" ) {	
				istr >> Itemp;   
				Z.push_back(Itemp);      
			}
			if ( text == "$CascadeType" ) {	
				istr >> Stemp;   
				TyCas.push_back(Stemp);      
			}
			if ( text == "$InitialEnergy" ) {	
				istr >> Dtemp;   
				start_energy.push_back(Dtemp);      
			}
			if ( text == "$StepEnergy" ) {	
				istr >> Dtemp;   
				step_energy.push_back(Dtemp);      
			}
			if ( text == "$EnergiesNumber" ) {	
				istr >> Itemp;   
				number_of_energies.push_back(Itemp);      
			}
			if ( text == "$SimulationCases" ) {	
				istr >> Itemp;   
				number_of_times_run.push_back(Itemp);      
			}
			if ( text == "$Parameters" ) {
				VectPar.clear();
				while (istr >> Dtemp)
					VectPar.push_back(Dtemp);
				Parameters.push_back(VectPar);
			}
//			if ( text == "$InfoLevelInformation" ) {	
			if ( text == "$UnbindParticles" ) {	
				istr >> Stemp;
				if (Stemp == "true") Btemp = true;
				if (Stemp == "false") Btemp = false;
				printUnbindParticles.push_back(Btemp);      
			}
			if ( text == "$InternalCascadeHistory" ) {	
				istr >> Stemp;
				if (Stemp == "true") Btemp = true;
				if (Stemp == "false") Btemp = false;
				Print_Internal_Casc.push_back(Btemp);      
			}
			if ( text == "$VariableSeed" ) {	
				istr >> Stemp;
				if (Stemp == "true") Btemp = true;
				if (Stemp == "false") Btemp = false;
				seed.push_back(Btemp);      
			}
		}
	}
	if( (nucleusName.size() == TyCas.size()) && (nucleusName.size() == A.size()) && (nucleusName.size() == Z.size()) && (nucleusName.size() == number_of_times_run.size()) && (nucleusName.size() == number_of_energies.size()) && (nucleusName.size() == printUnbindParticles.size()) && (nucleusName.size() == start_energy.size()) && (nucleusName.size() == step_energy.size()) && (nucleusName.size() == Print_Internal_Casc.size()) && (nucleusName.size() == Parameters.size()) && (nucleusName.size() == seed.size())  ) {
		IsDataOk = true;
		CascNumber = nucleusName.size();
	}
}



void execute_cascade( NucleusDynamics* nuc, MesonsPool* mpool,  LeptonsPool *lpool, Double_t proj_energy, Int_t N, const char* outFile, std::vector<Double_t> Par, const char* projectile_opt, bool PrntUnbPart, bool PrntIntCasc){
#ifdef CRISP_MPI
	// MPI ----------------------------------------------------------------------
	int my_id = 0;	//!< Id do processo MPI
	int size = 1;	//!< Numero total de processos MPI
	int resultlen;
	char name[30] = " ";
	MPI_Comm_rank( MPI_COMM_WORLD, &my_id ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // size dos processos
	MPI_Get_processor_name(name, &resultlen);
	name[resultlen] = '\0';
	int hostsNRuns = (int)(N / size);
	// Created file names
  	std::string str_outFile = outFile;
	std::string outExt = ".root";	
	std::string str_outFile_Nproc = str_outFile;
	str_outFile_Nproc.replace(str_outFile.find(outExt),outExt.length(),"_%d.root");
	std::cout << "str_outFile_Nproc = " << str_outFile_Nproc << std::endl;
	TFile F( Form(str_outFile_Nproc.c_str(),my_id), "recreate");
	// End MPI ------------------------------------------------------------------
#else 
	TFile F(outFile,"recreate");
#endif // CRISP_MPI
	int start = 0, end = N;

	TString stropt(projectile_opt);
	EventGen* pev = 0;
	TTree* Hist = NULL, *InternalHist = NULL;
	Int_t counts, ChnIdx, a, z, numTotPho, numPassPho;
	Long_t TotalCount = 0;
	
	

	
	
	Double_t ex = proj_energy, ex_energy(0.), initRotKinect(0.), finalRotKinect(0.), TotEnPassPho(0), cascBlockIndex, InitBlockIndex, impactPar(0.);
	Int_t  A = nuc->GetA(), Z = nuc->GetZ(), NHistograms(0), InitBlocked, cascCount, cascBlocked, InitBlockCount;
	TVector3 pVector, lVector;
	Double_t px, py, pz, lx, ly, lz, pfx, pfy, pfz, lfx, lfy, lfz;
	TH1D *Histograms = NULL;
        Double_t te;
	//print basic information
	//Resulting Nucleus general information
	Hist = new TTree("History", "Cascade Tree");
	Hist->Branch("finalNuc_A",&a,"FA/I");
	Hist->Branch("finalNuc_Z",&z,"FZ/I");
	Hist->Branch("finalNuc_Ex",&ex_energy,"Ex/D");
	Hist->Branch("initialproj_Energy",&proj_energy,"proj_energy/D");
	Hist->Branch("initialNuc_RotK",&initRotKinect,"initRotK/D");
	Hist->Branch("finalNuc_RotK",&finalRotKinect,"finalRotK/D");
	Hist->Branch("ElapsedTime",&te,"te/D");
	Hist->Branch("initialNuc_Px",&px,"px/D");
	Hist->Branch("initialNuc_Py",&py,"py/D");
	Hist->Branch("initialNuc_Pz",&pz,"pz/D");
	Hist->Branch("initialNuc_Lx",&lx,"lx/D");
	Hist->Branch("initialNuc_Ly",&ly,"ly/D");
	Hist->Branch("initialNuc_Lz",&lz,"lz/D");
	Hist->Branch("finalNuc_Px",&pfx,"pfx/D");
	Hist->Branch("finalNuc_Py",&pfy,"pfy/D");
	Hist->Branch("finalNuc_Pz",&pfz,"pfz/D");
	Hist->Branch("finalNuc_Lx",&lfx,"lfx/D");
	Hist->Branch("finalNuc_Ly",&lfy,"lfy/D");
	Hist->Branch("finalNuc_Lz",&lfz,"lfz/D");
	Hist->Branch("initialAttempts",&counts,"initialAttempts/I");
	Hist->Branch("InitialChannel",&ChnIdx,"InitialChannel/I");
	Hist->Branch("InitialBlocked",&InitBlocked,"InitialBlocked/I");
	Hist->Branch("CascBlocked",&cascBlocked,"CascBlocked/I");
	Hist->Branch("CascCount",&cascCount,"cascCount/I");
	//Resulting unbind particles information
	UnBindPart_Struct UnBindPart;
	if (PrntUnbPart){
		Hist->Branch("UnBindPart_Number",&UnBindPart.NPart,"NPart/I");
		Hist->Branch("UnBindPart_Pid",UnBindPart.Part_PID, "UnBindPart_PID[NPart]/I");
		Hist->Branch("UnBindPart_PX",UnBindPart.Part_PX, "UnBindPart_PX[NPart]/D");
		Hist->Branch("UnBindPart_PY",UnBindPart.Part_PY, "UnBindPart_PY[NPart]/D");
		Hist->Branch("UnBindPart_PZ",UnBindPart.Part_PZ, "UnBindPart_PZ[NPart]/D");
		Hist->Branch("UnBindPart_E",UnBindPart.Part_E, "UnBindPart_E[NPart]/D");
		Hist->Branch("UnBindPart_K",UnBindPart.Part_K, "UnBindPart_K[NPart]/D");
	}
	
#ifdef THERMA
	double proton_cell_size = 0.;
	double neutron_cell_size = 0.;
	double proton_FermiEn = 0.;
	double neutron_FermiEn = 0.;
	double proton_FermiMom = 0.;
	double neutron_FermiMom = 0.;
	int Nlevels = 30;
	double ProtonLevel_i[30];
	double ProtonLevel_m[30];
	double ProtonLevel_f[30];
	double NeutronLevel_i[30];
	double NeutronLevel_m[30];
	double NeutronLevel_f[30];
	Hist->Branch("Proton_dp",&proton_cell_size,"Proton_dp/D");
	Hist->Branch("Neutron_dp",&neutron_cell_size,"Neutron_dp/D");
	Hist->Branch("proton_FermiEn",&proton_FermiEn,"proton_FermiEn/D");
	Hist->Branch("proton_FermiMom",&proton_FermiMom,"proton_FermiMom/D");
	Hist->Branch("neutron_FermiEn",&neutron_FermiEn,"neutron_FermiEn/D");
	Hist->Branch("neutron_FermiMom",&neutron_FermiMom,"neutron_FermiMom/D");
	Hist->Branch("Nlevels",&Nlevels,"Nlevels/I");
	Hist->Branch("ProtonLevel_i",ProtonLevel_i, "ProtonLevel_i[Nlevels]/D");
	Hist->Branch("ProtonLevel_m",ProtonLevel_m, "ProtonLevel_m[Nlevels]/D");
	Hist->Branch("ProtonLevel_f",ProtonLevel_f, "ProtonLevel_f[Nlevels]/D");
	Hist->Branch("NeutronLevel_i",NeutronLevel_i, "NeutronLevel_i[Nlevels]/D");
	Hist->Branch("NeutronLevel_m",NeutronLevel_m, "NeutronLevel_m[Nlevels]/D");
	Hist->Branch("NeutronLevel_f",NeutronLevel_f, "NeutronLevel_f[Nlevels]/D");
#endif
	
	if ( stropt.CompareTo("photon") == 0 ) {
		pev = new PhotonEventGen();
		init_photon_evt_gen((PhotonEventGen*)pev);
	} 
	else if ( stropt.CompareTo("proton") == 0 ) {
		pev = new ProtonEventGen();
		init_proton_evt_gen((ProtonEventGen*)pev);
	}
	else if ( stropt.CompareTo("deuteron") == 0 ) {
		pev = new DeuteronEventGen();
		init_deuteron_evt_gen((DeuteronEventGen*)pev);
		
	}else if ( stropt.CompareTo("ion") == 0 ) {
		pev = new IonEventGen();
		init_ion_evt_gen((IonEventGen*)pev);
		
	}
	else if ( stropt.CompareTo("ultra") == 0 ) {
		pev = new UltraEventGen();
		init_ultra_evt_gen((UltraEventGen*)pev);
		for (int i = 0; i < 4; i++) Par.push_back(0);
		Hist->Branch("NumTotPho",&numTotPho,"NTPho/I");
		Hist->Branch("NumPassPho",&numPassPho,"NPPho/I");
		Hist->Branch("TotEnPassPho",&TotEnPassPho,"TEPPho/D");
		Hist->Branch("ImpactParam",&impactPar,"ImpPar/D");
	} 	
	else if ( stropt.CompareTo("bremss") == 0 ) {
		pev = new BremsstrahlungEventGen();
		Par.push_back(0);
		init_bremsstrahlung_evt_gen((BremsstrahlungEventGen*)pev);
		Hist->Branch("Photon_En",&ex,"Ph_En/D");
	}
	else if ( stropt.CompareTo("hyperon") == 0 ) {
		pev = new HyperonEventGen( (Int_t)Par[0]);
		init_hyperon_evt_gen((HyperonEventGen*)pev);
		for (int i = 0; i < 5; i++) Par.push_back(0);  	//need 5 parameters more for extracting data from hyperon event generation process
		NHistograms = 22;
		Histograms = new  TH1D[NHistograms];
		hyperon_histograms_init(Histograms);		// initializing histograms
	}else if ( stropt.CompareTo("neutrino") == 0 ) {
		pev = new NeutrinoEventGen();
		init_neutrino_evt_gen((NeutrinoEventGen*)pev);
		
	}else {
		std::cout << " incorrect cascade type \n";
		return;
	} 

#ifdef CRISP_MPI
	if( my_id == size - 1 ){
		start = hostsNRuns * my_id; 
		end = N;
		std::cout << " Process number = " << my_id << " from a total of " << size << " will run events from " << start << " to " << end-1 << std::endl;
	} else{
		start = hostsNRuns * my_id;
		end = hostsNRuns * (my_id+1);
		std::cout << " Process number = " << my_id << " from a total of " << size << " will run events from " << start << " to " << end-1 << std::endl;
	}
#endif // CRISP_MPI

	for ( int i = start; i < end; i++ ){
	       

		std::cout << Form ("Cascade #%d: Nucleus target A = %d, Z = %d, energy = %g MeV", i , A, Z, ex) << std::endl;
		
		Cascade *c = 0; 
		ChnIdx = -1;	
		counts = 1;
		InitBlockCount = 0;
		
		
		try {
		        ex=proj_energy;
		//	cout << "ExEnergy before interaction = " << ExcitationEnergy(*nuc, *mpool) << endl;
		  
			/********************* NUCLEUS EVENT GENERATOR    ************************/
			if(stropt.CompareTo("neutrino") == 0){
			   while( (ChnIdx = pev->Generate( ex, *nuc, *lpool, *mpool, Par,0,0) ) < 0 ) {
				counts++;
				if (ChnIdx == -2) InitBlockCount++;
				  nuc->DoInitConfig(A, Z);
				
				/*
				 * // this is usefull when below threshould reaction are tried --- improve coments
				 * thi make code slower de-activate if not necesary
				 * 
				 * */
				
				if(counts > 1000000000)
				  break;
			  }
			  
			}
			else{
			  while( (ChnIdx = pev->Generate( ex, *nuc, *mpool, Par) ) < 0 ) {
				counts++;
				if (ChnIdx == -2) InitBlockCount++;
				nuc->DoInitConfig(A, Z);
			  }
			  
			}
			if(stropt.CompareTo("ultra") == 0){
				numTotPho = Par[0];
				numPassPho = Par[1];
				TotEnPassPho = Par[2];
				impactPar = Par[3];
			}
			
			/********************* ANALYSIS BREFORE CASCADE    ************************/
		//	cout << "ExEnergy right after first interaction = " << ExcitationEnergy(*nuc, *mpool) << endl;
			double hslash_scale = 0.005; //hslash is Planck constant over 2pi
			 if ( stropt.CompareTo("ion") == 0 ||  stropt.CompareTo("neutrino") == 0 )
			ex=nuc->Get_InitE();
			//double initial_totalE = nuc->TotalSystemEnergy()-nuc->GetMass();
// 			int aaa=0, zzz=0;
// 			for(int jjj=0;jjj<nuc->GetA();jjj++){
// 			  if(nuc->GetNucleon(jjj).IsBind()==true)
// 			    aaa++;
// 			}
// 			cout<<"Nucleus for cascade: "<<aaa<<endl;
			pVector = nuc->Momentum().Vect(); px = pVector.X(); py = pVector.Y(); pz = pVector.Z();
			lVector = nuc->AngularMomentum(); lx = lVector.X()*hslash_scale; ly = lVector.Y()*hslash_scale; lz = lVector.Z()*hslash_scale;
			initRotKinect = nuc->RotatKineticEnergy();
			TotalCount = TotalCount + counts;
			if(stropt.CompareTo("hyperon") == 0) analysis_hyperon_evt_genBf(*nuc, *mpool, Histograms, NHistograms, Par);
			if(stropt.CompareTo("bremss") == 0) ex = Par[0];
			if(stropt.CompareTo("ultra") == 0) ex = TotEnPassPho;
			/**************************************************************************/

//   			for(int aq=0;aq<nuc->GetA();aq++){
//   			  if(nuc->GetNucleon(aq).IsBind())
//   			  cout<<"Energia salida "<<nuc->GetNucleon(aq).Momentum().E()-nuc->GetNucleon(aq).GetInvariantMass()<<endl;
//  			}
// 			cout<<endl;
//   			nuc->DoInitConfig(A, Z);
//   			ex=0;
			
			/*********************     CASCADE EXECUTION       ************************/
			c = new Cascade(*nuc, *mpool,*lpool, ex, PrntIntCasc);
			if (ChnIdx==1111){   //this is from BremsstrahlungEventGen
			  ex_energy = ex;
			} else {
			  ex_energy = c->Execute(*nuc, *mpool, *lpool);
			}
			c->FinalNucleus(a, z, *nuc);
			//NucleusDynamics nuc_basico(a,z);
			//ex_energy = -nuc->TotalSystemEnergy()+nuc->GetMass()+initial_totalE;

			
			
			//ex_energy=nuc->TotalSystemEnergy()-nuc_basico.TotalSystemEnergy();
			
//			cout<<"Energia "<<nuc->GetBasicEnergy()<<endl;
			
//  			for(int aq=0;aq<nuc->GetA();aq++){
//  			  if(nuc->GetNucleon(aq).IsBind() && nuc->GetNucleon(aq).PdgId()==CPT::neutron_ID)
//  			  cout<<nuc->GetIntLevelOf(nuc->GetNucleon(aq))<<"\t"<<nuc->GetNucleon(aq).Momentum().E()<<endl;
//  			}
/*			for(int aq=0;aq<nuc_basico.GetA();aq++){
			  if(nuc_basico.GetNucleon(aq).IsBind() && nuc_basico.GetNucleon(aq).PdgId()==CPT::neutron_ID)
			  cout<<nuc_basico.GetIntLevelOf(nuc_basico.GetNucleon(aq))<<"\t"<<nuc_basico.GetNucleon(aq).Momentum().E()<<endl;
			}*/			
			//ex_energy=nuc->GetMass()-nuc_basico.GetMass();
			//cout<<"d_tenergy "<<ex_energy<<"d_Kenergy "<<nuc
			//nuc->Print();
//			cout<<endl;
			//nuc_basico.Print();
//			cout<<endl;
			cascBlocked = c->TotalBlocked();
			cascCount = c->TotalCount();
			InitBlocked = InitBlockCount;
			cascBlockIndex = (Double_t)cascBlocked/cascCount;
			InitBlockIndex = (Double_t)InitBlocked/counts;

			/**************************************************************************/

			/*********************   ANALYSIS AFTER CASCADE    ************************/
			//cout << "ExEnergy after cascade = " << ExcitationEnergy(*nuc, *mpool) << endl;
			finalRotKinect = nuc->RotatKineticEnergy();
			pVector = nuc->Momentum().Vect(); pfx = pVector.X(); pfy = pVector.Y(); pfz = pVector.Z();
			lVector = nuc->AngularMomentum(); lfx = lVector.X()*hslash_scale; lfy = lVector.Y()*hslash_scale; lfz = lVector.Z()*hslash_scale;
 			if(stropt.CompareTo("hyperon") == 0) analysis_hyperon_evt_gen(*nuc, *mpool, Histograms, NHistograms, Par);

			if(PrntUnbPart) UnBindPart = GetUnBindParticles(*nuc, *mpool,*lpool);

#ifdef THERMA
			proton_cell_size = nuc->ProtonFermiLevels().GetCellSize();
			proton_FermiEn = nuc->ProtonFermiLevels().GetFermiEnergy();
			proton_FermiMom = nuc->ProtonFermiLevels().GetFermiMomentum();
			neutron_cell_size = nuc->NeutronFermiLevels().GetCellSize();
			neutron_FermiEn = nuc->NeutronFermiLevels().GetFermiEnergy();
			neutron_FermiMom = nuc->NeutronFermiLevels().GetFermiMomentum();
			
			for(int j = 0; j < Nlevels; j++){
				ProtonLevel_i[j] = nuc->GetOccupationProton(j, "initial");
				ProtonLevel_m[j] = nuc->GetOccupationProton(j, "sys_closed");
				ProtonLevel_f[j] = nuc->GetOccupationProton(j, "final");
				NeutronLevel_i[j] = nuc->GetOccupationNeutron(j, "initial");
				NeutronLevel_m[j] = nuc->GetOccupationNeutron(j, "sys_closed");
				NeutronLevel_f[j] = nuc->GetOccupationNeutron(j, "final");
			}
#endif
			
///Filling Cascade Tree here
			Hist->Fill();

			if (PrntIntCasc){
 	 	   		InternalHist = c->GetHistory();
  				InternalHist->SetName(Form ("Tree_%d",i));
				InternalHist->Write();
  			}
			/**************************************************************************/
			std::cout << Form("Cascade Blocking Index = %f, Initial Blocking Index = %f, Initial Blocked = %d", cascBlockIndex, InitBlockIndex, InitBlockCount) << std::endl;
			te = c->ElapsedTime();
			if(stropt.CompareTo("ultra") == 0){
				std::cout << Form ("Final[%d]: A = %d Z = %d, Ex_energy = %f, t = %f, b = %.3f", i, a, z, ex_energy, te, impactPar) << std::endl << std::endl;
			} else{
				std::cout << Form ("Final[%d]: A = %d Z = %d, Ex_energy = %f, t = %f", i, a, z, ex_energy, te) << std::endl << std::endl;
			}
			//cout << "About to move on..." << endl;
			nuc->DoInitConfig(A, Z);
			mpool->clear();
			lpool->clear();
			delete c; c = 0;
			//cout << "Moving on..." << endl;
			if(stropt.CompareTo("bremss") == 0 || stropt.CompareTo("ultra") == 0) ex = proj_energy;
		}catch (BasicException e){
			std::cout << "catched ..." << std::endl;
			e.PrintOn();
			nuc->DoInitConfig(A, Z);
			mpool->clear();
			lpool->clear();
			// verificar se o operador new realmente retornou um ponteiro para c.
			if (c != 0 && !c->IsZombie() ) { 
				std::cout << "[INFO] Deleting Cascade" << std::endl;	  
				delete c;
			}
			std::cout << "[INFO] Deleting ofs" << std::endl;
			i--;
		}
	}
	if (Hist){
		#ifdef CRISP_MPI
			Hist->SetDirectory(&F);
		#endif
		F.cd();
		Hist->Write();
		delete Hist; 
	}
	if(Histograms){
		for( int i = 0; i < NHistograms; i++) Histograms[i].Write();
		delete[] Histograms;
	}
 	if(stropt.CompareTo("ultra") == 0){
 		TH1D photEnDist = ((UltraEventGen*)pev)->PhotEnSpecDist();
 		photEnDist.Write();
 	}
	F.Close();
	delete pev;
	
#ifdef CRISP_MPI
	std::cout << " proceso # " << my_id << "Finished." << std::endl;
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	if(my_id == 0){
		
		std::cout << endl << "Merging files from different processes..." << std::endl;
		
		TString filename;
		
		TChain Chain("History");
		
		for(int i=0; i<size; i++){
			filename = Form(str_outFile_Nproc.c_str(),i);
			Chain.Add(filename.Data());
		}
		
		Chain.Merge(outFile);
		
		if(PrntIntCasc) cout << endl << "InternalCascadeHistory option was chosen by user. Keeping files from different processes. Unable to merge this part." << endl << endl;
		
		if(!PrntIntCasc){
			for(int i=0; i<size; i++){
				filename = Form(str_outFile_Nproc.c_str(),i);
				cout << "removing ... " << filename.Data() << endl;
				system( ("rm " + filename).Data() );
			}
		}
	}
		
//===============================================================================================
//	BEING TESTED. NOT WORKING YET
//===============================================================================================
/*		
		if(PrntIntCasc){
			
			cout << endl;
			cout << "Internal cascade option was chosen. Merging in a separate file..." << endl;
			
			TList *treeList = new TList();
			
			std::string outfileIntCasc = outFile;
			outfileIntCasc.replace(str_outFile.find(outExt),outExt.length(),"_InternalCascade.root");
			
			TFile *outputIntCasc = new TFile(outfileIntCasc.data(), "RECREATE");

			for(int i=0; i<size; i++){
				
				filename = Form(str_outFile_Nproc.c_str(),i);
				TFile *RankFile = new TFile(filename.Data());
				
				if( i == size - 1 ){
					start = hostsNRuns * i; 
					end = N;
				} else{
					start = hostsNRuns * i;
					end = hostsNRuns * (i+1);
				}
				
				for(int j=start; j<end; j++){
					
					TTree* RankTree = (TTree*)RankFile->Get(Form("Tree_%d",j));
					treeList->Add(RankTree);
				}
				RankFile->Close();
				
			}
			//outputIntCasc.Write();
			outputIntCasc->cd();
			//gDirectory->pwd();
			treeList->Write("treeList",1);
			outputIntCasc->Close();

		}
		cout << endl;
		for(int i=0; i<size; i++){
			filename = Form(str_outFile_Nproc.c_str(),i);
			cout << "removing ... " << filename.Data() << endl;
			system( ("rm " + filename).Data() );
		}
		
		cout << endl;
	}
	*/

//===============================================================================================
//	TO BE DELETED IN A LATER MOMENT
//===============================================================================================

/*	if( !(PrntUnbPart || PrntIntCasc) ){
		MPI_Barrier(MPI_COMM_WORLD);
		
		if(my_id == 0){
			 std::cout << "Merging files from different processes..." << std::endl;

			 TString filename;
			 TList tree_list, tree_list2; 
			 Int_t total_events = 0;
			 TH1D *hypHist = new TH1D[22];
			 if( stropt.CompareTo("hyperon") == 0 ) hyperon_histograms_init(hypHist);
			 
			 for(int i=0; i<size; i++){
			 
				  filename = Form(str_outFile_Nproc.c_str(),i);

				  TFile *f = new TFile(filename.Data());
				  TTree *tree = (TTree *)f->Get("History");
				  TH1D **hypHistVec = NULL; 
				  hypHistVec = new TH1D*[22];
				  

				  cout << "Adding file: " << filename.Data() << endl;
				  tree_list.Add(tree);
				  total_events += (Int_t )tree->GetEntries();
				  
				  if( stropt.CompareTo("hyperon") == 0 ) {
						  hypHistVec[0] = (TH1D *)f->Get("HypPNN_n1B");
						  hypHistVec[1] = (TH1D *)f->Get("HypPNN_n2B");
						  hypHistVec[2] = (TH1D *)f->Get("HypPNP_n1B");
						  hypHistVec[3] = (TH1D *)f->Get("HypPNP_p2B");
						  hypHistVec[4] = (TH1D *)f->Get("HypPNN_n1BL");
						  hypHistVec[5] = (TH1D *)f->Get("HypPNN_n2BL");
						  hypHistVec[6] = (TH1D *)f->Get("HypPNP_n1BL");
						  hypHistVec[7] = (TH1D *)f->Get("HypPNP_p2BL");
						  hypHistVec[8] = (TH1D *)f->Get("HypCosThetaNNB");
						  hypHistVec[9] = (TH1D *)f->Get("HypCosThetaNPB");
						  hypHistVec[10] = (TH1D *)f->Get("HypCosThetaNNBL");
						  hypHistVec[11] = (TH1D *)f->Get("HypCosThetaNPBL");
						  hypHistVec[12] = (TH1D *)f->Get("HypPairNNBf");
						  hypHistVec[13] = (TH1D *)f->Get("HypPairNPBf");
						  hypHistVec[14] = (TH1D *)f->Get("HypKN");
						  hypHistVec[15] = (TH1D *)f->Get("HypKP");
						  hypHistVec[16] = (TH1D *)f->Get("HypKNN");
						  hypHistVec[17] = (TH1D *)f->Get("HypKNP");
						  hypHistVec[18] = (TH1D *)f->Get("HypCosThetaNN");
						  hypHistVec[19] = (TH1D *)f->Get("HypCosThetaNP");
						  hypHistVec[20] = (TH1D *)f->Get("HypPairNN");
						  hypHistVec[21] = (TH1D *)f->Get("HypPairNP");
						  
						  for(int k=0; k<22; k++) hypHist[k].Add(hypHistVec[k]);
						  
						  for(int k=0; k<22; k++){
								delete hypHistVec[k];
						  }
						  delete[] hypHistVec;
				  }
				  
				  cout << "removing ... " << filename.Data() << endl;
				  system( ("rm " + filename).Data() );
			 }
			 TString outfile = outFile;
			 cout << "Opening output file: " << outfile.Data() << endl;
			 TFile output(outfile.Data(), "RECREATE");
			 
			 cout << "Merging trees...patience..." << endl;
			 TTree::MergeTrees(&tree_list);
			 if( stropt.CompareTo("hyperon") == 0 ){ 
				  for(int i=0; i<22; i++) hypHist[i].Write(); 
				  delete[] hypHist;
			 }
			 output.Write();
			 output.Close();
			 cout << "Total Events: " << total_events << endl;
		}
	}*/
#endif // CRISP_MPI
}
