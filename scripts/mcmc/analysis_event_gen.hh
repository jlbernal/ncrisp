/* ================================================================================
 * 
 * 	Copyright 2012 Israel González
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "TH1D.h"
#include "TVector3.h"

#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"
#include "DataHelper.hh"

#include "PhotonEventGen.hh"
#include "BremsstrahlungEventGen.hh"
#include "UltraEventGen.hh"
#include "ProtonEventGen.hh"
#include "HyperonEventGen.hh"

#include "PhotonChannel.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "bb_cross_sections.hh"
#include "bb_actions.hh"
#include "BBChannel.hh"
#include "DataHelper.hh"

#ifdef CRISP_MINUIT
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/FCNBase.h"
#endif // CRISP_MINUIT


void Tree_init(TTree* InfoTree, Int_t InfoLev);
void hyperon_histograms_init(TH1D *hists);

double ExcitationEnergy(NucleusDynamics& nuc, MesonsPool& mpool);

void analysis_photon_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool, Int_t *Channels);
void analysis_proton_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par);
void analysis_bremsstrahlung_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par);
void analysis_ultra_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par);
void analysis_hyperon_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par);
void analysis_hyperon_evt_genBf(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par);
UnBindPart_Struct GetUnBindParticles(NucleusDynamics& nuc, MesonsPool& mpool);
UnBindPart_Struct GetUnBindParticles(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool);

