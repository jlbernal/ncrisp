/* ================================================================================
 * 
* 	Copyright 2008, 2012, 2015 Israel González, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include <fstream>
#include <iostream>
#include <iomanip>
#include <exception>
#include <vector>
#include <sstream>
#include <string>

#include "TGraphErrors.h"
#include "TObjArray.h"
#include "TList.h"
#include "TChain.h"

#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "Cascade.hh"
#include "bb_cross_sections.hh"
#include "bb_actions.hh"
#include "BBChannel.hh"
#include "init_event_gen.hh"
#include "analysis_event_gen.hh"
//Event Generators
#include "HyperonEventGen.hh"
#include "PhotonEventGen.hh"
#include "BremsstrahlungEventGen.hh"
#include "UltraEventGen.hh"
#include "ProtonEventGen.hh"
#include "IonEventGen.hh"
#include "NeutrinoEventGen.hh"


#ifdef CRISP_MINUIT
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/FCNBase.h"
#endif // CRISP_MINUIT


#include "Timer.hh"
#include "Util.hh"

#ifdef CRISP_MPI
#include "mpi.h"
#endif // CRISP_MPI

struct DataStruct{
	std::vector<TString> nucleusName, TyCas;
	std::vector<Int_t> A, Z, number_of_times_run, number_of_energies;
	std::vector<Double_t> start_energy, step_energy, VectPar;
	std::vector<Bool_t> Print_Internal_Casc, printUnbindParticles, seed;
	std::vector<std::vector<Double_t> > Parameters;
	DataStruct();
	Bool_t IsDataOk;
	Int_t CascNumber;
	void ReadData(fstream &file);
};

void execute_cascade( NucleusDynamics* nuc, MesonsPool* mpool, LeptonsPool *lpool, Double_t photon_energy, Int_t N, const char* outFile, std::vector<Double_t> Par, const char* projectile_opt = "photon", bool PrntUnbPart = false, bool PrntIntCasc = false);

