/* ================================================================================
 * 
 * 	Copyright 2008, 2012, 2015, 2016 
 *	Israel González, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "init_event_gen.hh"

using namespace std;

Double_t p33_params[6] = {1.18146, 0.166511, 381.488, 1.18146, 0.166511, 364.76};
Double_t f37_params[6] = {1.893, .290, 2.9,   1.893, .290, 5.73};
Double_t d13_params[6] = {1.33347, 0.0766583, 101.478, 1.33347, 0.0766583, 91.6495};
Double_t p11_params[6] = {1.33334, 0.00201571, 5.51541e-11, 1.33334, 0.00201571, 0.000113983};
Double_t s11_params[6] = {1.473, 0.167, 15, 1.473, 0.167, 0 };
Double_t f15_params[6] = {1.50292, 0.0341276, 177.945, 1.50292, 0.0341276, 0 };

void init_proton_evt_gen(ProtonEventGen* p){
	TObjArray& bb_ch = p->GetChannels();
#ifndef __CINT__
	bb_ch.Add(new BBChannel( "elastic"  , bb_elastic_cross_section , 0, bb_elastic_np  ));
	bb_ch.Add(new BBChannel( "inelastic", bb_one_pion_cross_section, 0, bb_inelastic));  
#else
	bb_ch.Add(new BBChannel( CrossSectionChannel( "elastic",   bb_elastic_cross_section),  bb_elastic_np  ));
	bb_ch.Add(new BBChannel( CrossSectionChannel( "inelastic", bb_one_pion_cross_section), bb_inelastic));
#endif
}

void init_deuteron_evt_gen(DeuteronEventGen* deut){
	TObjArray& bb_ch = deut->GetChannels();
#ifndef __CINT__
	bb_ch.Add(new BBChannel( "elastic"  , bb_elastic_cross_section , 0, bb_elastic_np  ));
	bb_ch.Add(new BBChannel( "inelastic", bb_one_pion_cross_section, 0, bb_inelastic));  
#else
	bb_ch.Add(new BBChannel( CrossSectionChannel( "elastic",   bb_elastic_cross_section),  bb_elastic_np  ));
	bb_ch.Add(new BBChannel( CrossSectionChannel( "inelastic", bb_one_pion_cross_section), bb_inelastic));
#endif
}

void init_ion_evt_gen(IonEventGen* ion){
	TObjArray& bb_ch = ion->GetChannels();
#ifndef __CINT__
	bb_ch.Add(new BBChannel( "elastic"  , bb_elastic_cross_section , 0, bb_elastic_np  ));
	bb_ch.Add(new BBChannel( "inelastic", bb_one_pion_cross_section, 0, bb_inelastic));  
#else
	bb_ch.Add(new BBChannel( CrossSectionChannel( "elastic",   bb_elastic_cross_section),  bb_elastic_np  ));
	bb_ch.Add(new BBChannel( CrossSectionChannel( "inelastic", bb_one_pion_cross_section), bb_inelastic));
#endif
}

void init_hyperon_evt_gen(HyperonEventGen* p){
 	p->Read_Data();
}


void init_neutrino_evt_gen(NeutrinoEventGen* neutrino){
    
	TObjArray& lb_ch = neutrino->GetChannels();

	lb_ch.Add(new LeptonNucleonChannel("nun_mump_ccqe_action", nun_mump_cross_section, 0, nun_mump_ccqe_action)); 
//	lb_ch.Add(new LeptonNucleonChannel( "nuN_qelastic_NCe_action", nun_mump_cross_section, 0, nuN_qelastic_NCe_action));  
//	lb_ch.Add( new LeptonNucleonChannel( "nuN_qelastic_NCe_action", nup_elastic_cross_section, 0, nuN_qelastic_NCe_action )); // solo antineutrino  
// Cres
	lb_ch.Add( new LeptonNucleonChannel( "nuN_res1_CCres_action", nup_mump33pp_cross_section, 0, nuN_res1_CCres_action ));  
	lb_ch.Add( new LeptonNucleonChannel( "nuN_res1_CCres_action", nun_mump33p_pip_cross_section, 0, nuN_res1_CCres_action )); 
	lb_ch.Add( new LeptonNucleonChannel( "nuN_res1_CCres_action", nun_mump33p_pi0_cross_section, 0, nuN_res1_CCres_action )); 
//NRes
//	lb_ch.Add( new LeptonNucleonChannel( "nuN_res2_NCres_action", nun_nup330_pip_cross_section, 0, nuN_res2_NCres_action )); 
//	lb_ch.Add( new LeptonNucleonChannel( "nuN_res2_NCres_action", nun_nup330_pim_cross_section, 0, nuN_res2_NCres_action )); 
//	lb_ch.Add( new LeptonNucleonChannel( "nuN_res2_NCres_action", nup_nup33p_pi0_cross_section, 0, nuN_res2_NCres_action ));  
//	lb_ch.Add( new LeptonNucleonChannel( "nuN_res2_NCres_action", nup_nup33p_pip_cross_section, 0, nuN_res2_NCres_action ));  



// terminar aqui 
  
}
  


void init_photon_evt_gen(PhotonEventGen* pev){

	if ( pev == 0 ) 
		return;
#ifndef __CINT__
	PhotonChannel*  qd = new PhotonChannel("qd", qdCrossSection, 0, qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel("bg", residual_PhotonCrossSection, 0, op_photonAction);
	PhotonChannel* p33 = new PhotonChannel("p33", p33Formation, 6, p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel("f37", f37Formation, 6, f37_photonAction);
	PhotonChannel* d13 = new PhotonChannel("d13", d13Formation, 6, d13_photonAction);
	PhotonChannel* p11 = new PhotonChannel("p11", p11Formation, 6, p11_photonAction);
	PhotonChannel* s11 = new PhotonChannel("s11", s11Formation, 6, s11_photonAction);
	PhotonChannel* f15 = new PhotonChannel("f15", f15Formation, 6, f15_photonAction);
	PhotonChannel* rho = new PhotonChannel("rho", rho_p_m_PhotonCrossSection, 0, rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel("rho0_p", rho_0_PhotonCrossSectionP, 0, rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel("rho0_n", rho_0_PhotonCrossSectionN, 0, rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel("omg", omega_PhotonCrossSection, 0, omega_photonAction);
	PhotonChannel* phi = new PhotonChannel("phi", phi_PhotonCrossSection, 0, phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel("J_Psi", J_Psi_PhotonCrossSection, 0, J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel("p_pPipPim", gp_pPipPim_NR, 0, gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel("p_nPipPi0", gp_nPipPi0_NR, 0, gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel("n_nPipPim", gn_nPipPim_NR, 0, gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel("n_pPimPi0", gn_pPimPi0_NR, 0, gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel("p_p2Pi0", gp_p2Pi0_NR, 0, gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel("n_n2Pi0", gn_n2Pi0_NR, 0, gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0, gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel("p_p3Pi0", gp_p3Pi0, 0, gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2iP0 = new PhotonChannel("p_nPip2Pi0", gp_nPip2Pi0, 0, gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel("p_n2PipPim", gp_n2PipPim, 0, gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel("n_pPip2Pim", gn_pPip2Pim, 0, gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel("n_n3Pi0", gn_n3Pi0, 0, gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0, gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel("n_pPim2Pi0", gn_pPim2Pi0, 0, gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0, gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel("p_p4Pi0", gp_p4Pi0, 0, gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0, gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel("p_nPip3Pi0", gp_nPip3Pi0, 0, gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0, gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0, gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel("n_n4Pi0", gn_n4Pi0, 0, gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0, gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel("n_pPim3Pi0", gn_pPim3Pi0, 0, gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0, gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0, gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel("p_p5Pi0", gp_p5Pi0_NR, 0, gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0, gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel("p_nPip4Pi0", gp_nPip4Pi0, 0, gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0, gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0, gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0, gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel("n_n5Pi0", gn_n5Pi0_NR, 0, gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0, gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel("n_pPim4Pi0", gn_pPim4Pi0, 0, gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0, gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0, gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0, gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel("p_p6Pi0", gp_p6Pi0, 0, gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0, gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0, gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel("p_nPip5Pi0", gp_nPip5Pi0, 0, gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0, gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0, gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0, gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel("n_n6Pi0", gn_n6Pi0, 0, gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0, gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0, gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel("n_pPim5Pi0", gn_pPim5Pi0, 0, gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0, gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0, gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0, gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel("p_p7Pi0", gp_p7Pi0, 0, gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0, gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0, gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel("p_nPip6Pi0", gp_nPip6Pi0, 0, gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0, gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0, gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0, gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0, gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel("n_n7Pi0", gn_n7Pi0, 0, gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0, gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0, gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel("n_pPim6Pi0", gn_pPim6Pi0, 0, gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0, gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0, gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0, gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0, gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel("p_p8Pi0", gp_p8Pi0, 0, gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0, gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0, gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0, gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel("p_nPip7Pi0", gp_nPip7Pi0, 0, gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0, gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0, gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0, gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0, gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel("n_n8Pi0", gn_n8Pi0, 0, gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0, gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0, gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0, gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel("n_pPim7Pi0", gn_pPim7Pi0, 0, gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0, gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0, gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0, gn_p3Pip4PimPi0_photonAction);

#else 
	PhotonChannel*  qd = new PhotonChannel(CrossSectionChannel("qd",qdCrossSection),qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel(CrossSectionChannel("bg",residual_PhotonCrossSection), op_photonAction); 
	PhotonChannel* p33 = new PhotonChannel(CrossSectionChannel("p33", p33Formation,6),p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel(CrossSectionChannel("f37", f37Formation,6),f37_photonAction);    
	PhotonChannel* d13 = new PhotonChannel(CrossSectionChannel("d13", d13Formation,6),d13_photonAction);  
	PhotonChannel* p11 = new PhotonChannel(CrossSectionChannel("p11", p11Formation,6),p11_photonAction); 
	PhotonChannel* s11 = new PhotonChannel(CrossSectionChannel("s11", s11Formation,6),s11_photonAction);  
	PhotonChannel* f15 = new PhotonChannel(CrossSectionChannel("f15", f15Formation,6), f15_photonAction);    
	PhotonChannel* rho = new PhotonChannel(CrossSectionChannel("rho", rho_p_m_PhotonCrossSection, 0), rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel(CrossSectionChannel("rho0_p", rho_0_PhotonCrossSectionP, 0), rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel(CrossSectionChannel("rho0_n", rho_0_PhotonCrossSectionN, 0), rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel(CrossSectionChannel("omg", omega_PhotonCrossSection, 0), omega_photonAction);
	PhotonChannel* phi = new PhotonChannel(CrossSectionChannel("phi", phi_PhotonCrossSection, 0), phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel(CrossSectionChannel("J_Psi", J_Psi_PhotonCrossSection, 0), J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel(CrossSectionChannel("p_pPipPim", gp_pPipPim_NR, 0), gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel(CrossSectionChannel("p_nPipPi0", gp_nPipPi0_NR, 0), gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel(CrossSectionChannel("n_nPipPim", gn_nPipPim_NR, 0), gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel(CrossSectionChannel("n_pPimPi0", gn_pPimPi0_NR, 0), gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pi0", gp_p2Pi0_NR, 0), gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pi0", gn_n2Pi0_NR, 0), gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel(CrossSectionChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0), gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pi0", gp_p3Pi0, 0), gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip2Pi0", gp_nPip2Pi0, 0), gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel(CrossSectionChannel("p_n2PipPim", gp_n2PipPim, 0), gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel(CrossSectionChannel("n_pPip2Pim", gn_pPip2Pim, 0), gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pi0", gn_n3Pi0, 0), gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel(CrossSectionChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0), gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim2Pi0", gn_pPim2Pi0, 0), gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0), gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel(CrossSectionChannel("p_p4Pi0", gp_p4Pi0, 0), gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0), gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip3Pi0", gp_nPip3Pi0, 0), gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0), gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0), gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel(CrossSectionChannel("n_n4Pi0", gn_n4Pi0, 0), gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0), gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim3Pi0", gn_pPim3Pi0, 0), gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0), gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0), gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel(CrossSectionChannel("p_p5Pi0", gp_p5Pi0_NR, 0), gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0), gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip4Pi0", gp_nPip4Pi0, 0), gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0), gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0), gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0), gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel(CrossSectionChannel("n_n5Pi0", gn_n5Pi0_NR, 0), gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0), gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim4Pi0", gn_pPim4Pi0, 0), gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0), gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0), gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0), gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel(CrossSectionChannel("p_p6Pi0", gp_p6Pi0, 0), gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0), gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0), gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip5Pi0", gp_nPip5Pi0, 0), gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0), gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0), gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0), gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel(CrossSectionChannel("n_n6Pi0", gn_n6Pi0, 0), gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0), gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0), gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim5Pi0", gn_pPim5Pi0, 0), gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0), gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0), gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0), gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel(CrossSectionChannel("p_p7Pi0", gp_p7Pi0, 0), gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0), gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0), gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip6Pi0", gp_nPip6Pi0, 0), gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0), gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0), gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel(CrossSectionChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0), gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0), gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel(CrossSectionChannel("n_n7Pi0", gn_n7Pi0, 0), gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0), gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0), gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim6Pi0", gn_pPim6Pi0, 0), gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0), gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0), gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel(CrossSectionChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0), gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel(CrossSectionChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0), gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel(CrossSectionChannel("p_p8Pi0", gp_p8Pi0, 0), gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0), gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0), gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0), gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip7Pi0", gp_nPip7Pi0, 0), gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0), gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0), gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0), gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel(CrossSectionChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0), gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel(CrossSectionChannel("n_n8Pi0", gn_n8Pi0, 0), gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0), gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0), gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0), gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim7Pi0", gn_pPim7Pi0, 0), gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0), gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0), gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel(CrossSectionChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0), gn_p3Pip4PimPi0_photonAction);
	
#endif  
	const double threshold_pion   = CPT::n_mass * CPT::effn + 140.;
//	const double threshold_two_pion = CPT::n_mass * CPT::effn + 280.;
   
	p33->CrossSection().SetParams(p33_params);
	f37->CrossSection().SetParams(f37_params);
	d13->CrossSection().SetParams(d13_params);
	p11->CrossSection().SetParams(p11_params);
	s11->CrossSection().SetParams(s11_params);
	f15->CrossSection().SetParams(f15_params);
  
	qd->CrossSection().SetLowerEnergyCut(40.);
	//qd->CrossSection().SetUpperEnergyCut(400.);
	bg->CrossSection().SetLowerEnergyCut(threshold_pion);
	p33->CrossSection().SetLowerEnergyCut(threshold_pion);
  
	pev->GetChannels().Add(qd);
	pev->GetChannels().Add(bg);
	pev->GetChannels().Add(p33);
	pev->GetChannels().Add(f37);
	pev->GetChannels().Add(d13);
	pev->GetChannels().Add(p11);
	pev->GetChannels().Add(s11);
	pev->GetChannels().Add(f15);  
	pev->GetChannels().Add(rho);  
	pev->GetChannels().Add(rho0_p);  
	pev->GetChannels().Add(rho0_n);
	pev->GetChannels().Add(omg);  
	pev->GetChannels().Add(phi);  
	pev->GetChannels().Add(J_Psi); 
	///2 pions production
	pev->GetChannels().Add(p_pPipPim);
	pev->GetChannels().Add(p_nPipPi0);
	pev->GetChannels().Add(n_nPipPim);
	pev->GetChannels().Add(n_pPimPi0);
	pev->GetChannels().Add(p_p2Pi0);
	pev->GetChannels().Add(n_n2Pi0);
	///3 pions production
	pev->GetChannels().Add(p_pPipPimPi0);
	pev->GetChannels().Add(p_p3Pi0);
	pev->GetChannels().Add(p_nPip2iP0);
	pev->GetChannels().Add(p_n2PipPim);
	pev->GetChannels().Add(n_pPip2Pim);
	pev->GetChannels().Add(n_n3Pi0);
	pev->GetChannels().Add(n_nPipPimPi0);
	pev->GetChannels().Add(n_pPim2Pi0);
	///4 pions production
	pev->GetChannels().Add(p_p2Pip2Pim);
	pev->GetChannels().Add(p_p4Pi0);
	pev->GetChannels().Add(p_pPipPim2Pi0);
	pev->GetChannels().Add(p_nPip3Pi0);
	pev->GetChannels().Add(p_n2PipPimPi0);
	pev->GetChannels().Add(n_n2Pip2Pim);
	pev->GetChannels().Add(n_n4Pi0);
	pev->GetChannels().Add(n_nPipPim2Pi0);
	pev->GetChannels().Add(n_pPim3Pi0);
	pev->GetChannels().Add(n_pPip2PimPi0);
	///5 pions production
	pev->GetChannels().Add(p_p2Pip2PimPi0);
	pev->GetChannels().Add(p_p5Pi0);
	pev->GetChannels().Add(p_pPipPim3Pi0);
	pev->GetChannels().Add(p_nPip4Pi0);
	pev->GetChannels().Add(p_n2PipPim2Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim);
	pev->GetChannels().Add(n_n2Pip2PimPi0);
	pev->GetChannels().Add(n_n5Pi0);
	pev->GetChannels().Add(n_nPipPim3Pi0);
	pev->GetChannels().Add(n_pPim4Pi0);
	pev->GetChannels().Add(n_pPip2Pim2Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim);
	///6 pions production
	pev->GetChannels().Add(p_p3Pip3Pim);
	pev->GetChannels().Add(p_p6Pi0);
	pev->GetChannels().Add(p_pPipPim4Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim2Pi0);
	pev->GetChannels().Add(p_nPip5Pi0);
	pev->GetChannels().Add(p_n2PipPim3Pi0);
	pev->GetChannels().Add(p_n3Pip2PimPi0);
	pev->GetChannels().Add(n_n3Pip3Pim);
	pev->GetChannels().Add(n_n6Pi0);
	pev->GetChannels().Add(n_nPipPim4Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim2Pi0);
	pev->GetChannels().Add(n_pPim5Pi0);
	pev->GetChannels().Add(n_pPip2Pim3Pi0);
	pev->GetChannels().Add(n_p2Pip3PimPi0);
	///7 pions production
	pev->GetChannels().Add(p_p3Pip3PimPi0);
	pev->GetChannels().Add(p_p7Pi0);
	pev->GetChannels().Add(p_pPipPim5Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim3Pi0);
	pev->GetChannels().Add(p_nPip6Pi0);
	pev->GetChannels().Add(p_n2PipPim4Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim2Pi0);
	pev->GetChannels().Add(p_n4Pip3Pim);
	pev->GetChannels().Add(n_n3Pip3PimPi0);
	pev->GetChannels().Add(n_n7Pi0);
	pev->GetChannels().Add(n_nPipPim5Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim3Pi0);
	pev->GetChannels().Add(n_pPim6Pi0);
	pev->GetChannels().Add(n_pPip2Pim4Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim2Pi0);
	pev->GetChannels().Add(n_p3Pip4Pim);
	///8 pions production
	pev->GetChannels().Add(p_p4Pip4Pim);
	pev->GetChannels().Add(p_p8Pi0);
	pev->GetChannels().Add(p_pPipPim6Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim4Pi0);
	pev->GetChannels().Add(p_p3Pip3Pim2Pi0);
	pev->GetChannels().Add(p_nPip7Pi0);
	pev->GetChannels().Add(p_n2PipPim5Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim3Pi0);
	pev->GetChannels().Add(p_n4Pip3PimPi0);
	pev->GetChannels().Add(n_n4Pip4Pim);
	pev->GetChannels().Add(n_n8Pi0);
	pev->GetChannels().Add(n_nPipPim6Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim4Pi0);
	pev->GetChannels().Add(n_n3Pip3Pim2Pi0);
	pev->GetChannels().Add(n_pPim7Pi0);
	pev->GetChannels().Add(n_pPip2Pim5Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim3Pi0);
	pev->GetChannels().Add(n_p3Pip4PimPi0);
}

void init_ultra_evt_gen(UltraEventGen* pev){

	if ( pev == 0 ) 
		return;
#ifndef __CINT__
	PhotonChannel*  qd = new PhotonChannel("qd", qdCrossSection, 0, qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel("bg", residual_PhotonCrossSection, 0, op_photonAction);
	PhotonChannel* p33 = new PhotonChannel("p33", p33Formation, 6, p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel("f37", f37Formation, 6, f37_photonAction);
	PhotonChannel* d13 = new PhotonChannel("d13", d13Formation, 6, d13_photonAction);
	PhotonChannel* p11 = new PhotonChannel("p11", p11Formation, 6, p11_photonAction);
	PhotonChannel* s11 = new PhotonChannel("s11", s11Formation, 6, s11_photonAction);
	PhotonChannel* f15 = new PhotonChannel("f15", f15Formation, 6, f15_photonAction);
	PhotonChannel* rho = new PhotonChannel("rho", rho_p_m_PhotonCrossSection, 0, rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel("rho0_p", rho_0_PhotonCrossSectionP, 0, rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel("rho0_n", rho_0_PhotonCrossSectionN, 0, rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel("omg", omega_PhotonCrossSection, 0, omega_photonAction);
	PhotonChannel* phi = new PhotonChannel("phi", phi_PhotonCrossSection, 0, phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel("J_Psi", J_Psi_PhotonCrossSection, 0, J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel("p_pPipPim", gp_pPipPim_NR, 0, gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel("p_nPipPi0", gp_nPipPi0_NR, 0, gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel("n_nPipPim", gn_nPipPim_NR, 0, gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel("n_pPimPi0", gn_pPimPi0_NR, 0, gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel("p_p2Pi0", gp_p2Pi0_NR, 0, gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel("n_n2Pi0", gn_n2Pi0_NR, 0, gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0, gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel("p_p3Pi0", gp_p3Pi0, 0, gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2Pi0 = new PhotonChannel("p_nPip2Pi0", gp_nPip2Pi0, 0, gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel("p_n2PipPim", gp_n2PipPim, 0, gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel("n_pPip2Pim", gn_pPip2Pim, 0, gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel("n_n3Pi0", gn_n3Pi0, 0, gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0, gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel("n_pPim2Pi0", gn_pPim2Pi0, 0, gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0, gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel("p_p4Pi0", gp_p4Pi0, 0, gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0, gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel("p_nPip3Pi0", gp_nPip3Pi0, 0, gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0, gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0, gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel("n_n4Pi0", gn_n4Pi0, 0, gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0, gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel("n_pPim3Pi0", gn_pPim3Pi0, 0, gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0, gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0, gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel("p_p5Pi0", gp_p5Pi0_NR, 0, gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0, gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel("p_nPip4Pi0", gp_nPip4Pi0, 0, gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0, gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0, gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0, gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel("n_n5Pi0", gn_n5Pi0_NR, 0, gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0, gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel("n_pPim4Pi0", gn_pPim4Pi0, 0, gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0, gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0, gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0, gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel("p_p6Pi0", gp_p6Pi0, 0, gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0, gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0, gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel("p_nPip5Pi0", gp_nPip5Pi0, 0, gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0, gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0, gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0, gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel("n_n6Pi0", gn_n6Pi0, 0, gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0, gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0, gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel("n_pPim5Pi0", gn_pPim5Pi0, 0, gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0, gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0, gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0, gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel("p_p7Pi0", gp_p7Pi0, 0, gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0, gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0, gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel("p_nPip6Pi0", gp_nPip6Pi0, 0, gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0, gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0, gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0, gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0, gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel("n_n7Pi0", gn_n7Pi0, 0, gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0, gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0, gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel("n_pPim6Pi0", gn_pPim6Pi0, 0, gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0, gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0, gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0, gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0, gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel("p_p8Pi0", gp_p8Pi0, 0, gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0, gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0, gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0, gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel("p_nPip7Pi0", gp_nPip7Pi0, 0, gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0, gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0, gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0, gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0, gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel("n_n8Pi0", gn_n8Pi0, 0, gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0, gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0, gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0, gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel("n_pPim7Pi0", gn_pPim7Pi0, 0, gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0, gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0, gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0, gn_p3Pip4PimPi0_photonAction);

#else 
	PhotonChannel*  qd = new PhotonChannel(CrossSectionChannel("qd",qdCrossSection),qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel(CrossSectionChannel("bg",residual_PhotonCrossSection), op_photonAction); 
	PhotonChannel* p33 = new PhotonChannel(CrossSectionChannel("p33", p33Formation,6),p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel(CrossSectionChannel("f37", f37Formation,6),f37_photonAction);    
	PhotonChannel* d13 = new PhotonChannel(CrossSectionChannel("d13", d13Formation,6),d13_photonAction);  
	PhotonChannel* p11 = new PhotonChannel(CrossSectionChannel("p11", p11Formation,6),p11_photonAction); 
	PhotonChannel* s11 = new PhotonChannel(CrossSectionChannel("s11", s11Formation,6),s11_photonAction);  
	PhotonChannel* f15 = new PhotonChannel(CrossSectionChannel("f15", f15Formation,6), f15_photonAction);    
	PhotonChannel* rho = new PhotonChannel(CrossSectionChannel("rho", rho_p_m_PhotonCrossSection, 0), rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel(CrossSectionChannel("rho0_p", rho_0_PhotonCrossSectionP, 0), rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel(CrossSectionChannel("rho0_n", rho_0_PhotonCrossSectionN, 0), rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel(CrossSectionChannel("omg", omega_PhotonCrossSection, 0), omega_photonAction);
	PhotonChannel* phi = new PhotonChannel(CrossSectionChannel("phi", phi_PhotonCrossSection, 0), phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel(CrossSectionChannel("J_Psi", J_Psi_PhotonCrossSection, 0), J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel(CrossSectionChannel("p_pPipPim", gp_pPipPim_NR, 0), gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel(CrossSectionChannel("p_nPipPi0", gp_nPipPi0_NR, 0), gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel(CrossSectionChannel("n_nPipPim", gn_nPipPim_NR, 0), gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel(CrossSectionChannel("n_pPimPi0", gn_pPimPi0_NR, 0), gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pi0", gp_p2Pi0_NR, 0), gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pi0", gn_n2Pi0_NR, 0), gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel(CrossSectionChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0), gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pi0", gp_p3Pi0, 0), gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip2Pi0", gp_nPip2Pi0, 0), gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel(CrossSectionChannel("p_n2PipPim", gp_n2PipPim, 0), gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel(CrossSectionChannel("n_pPip2Pim", gn_pPip2Pim, 0), gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pi0", gn_n3Pi0, 0), gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel(CrossSectionChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0), gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim2Pi0", gn_pPim2Pi0, 0), gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0), gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel(CrossSectionChannel("p_p4Pi0", gp_p4Pi0, 0), gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0), gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip3Pi0", gp_nPip3Pi0, 0), gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0), gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0), gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel(CrossSectionChannel("n_n4Pi0", gn_n4Pi0, 0), gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0), gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim3Pi0", gn_pPim3Pi0, 0), gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0), gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0), gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel(CrossSectionChannel("p_p5Pi0", gp_p5Pi0_NR, 0), gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0), gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip4Pi0", gp_nPip4Pi0, 0), gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0), gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0), gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0), gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel(CrossSectionChannel("n_n5Pi0", gn_n5Pi0_NR, 0), gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0), gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim4Pi0", gn_pPim4Pi0, 0), gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0), gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0), gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0), gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel(CrossSectionChannel("p_p6Pi0", gp_p6Pi0, 0), gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0), gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0), gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip5Pi0", gp_nPip5Pi0, 0), gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0), gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0), gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0), gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel(CrossSectionChannel("n_n6Pi0", gn_n6Pi0, 0), gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0), gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0), gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim5Pi0", gn_pPim5Pi0, 0), gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0), gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0), gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0), gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel(CrossSectionChannel("p_p7Pi0", gp_p7Pi0, 0), gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0), gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0), gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip6Pi0", gp_nPip6Pi0, 0), gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0), gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0), gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel(CrossSectionChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0), gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0), gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel(CrossSectionChannel("n_n7Pi0", gn_n7Pi0, 0), gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0), gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0), gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim6Pi0", gn_pPim6Pi0, 0), gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0), gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0), gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel(CrossSectionChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0), gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel(CrossSectionChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0), gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel(CrossSectionChannel("p_p8Pi0", gp_p8Pi0, 0), gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0), gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0), gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0), gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip7Pi0", gp_nPip7Pi0, 0), gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0), gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0), gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0), gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel(CrossSectionChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0), gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel(CrossSectionChannel("n_n8Pi0", gn_n8Pi0, 0), gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0), gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0), gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0), gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim7Pi0", gn_pPim7Pi0, 0), gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0), gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0), gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel(CrossSectionChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0), gn_p3Pip4PimPi0_photonAction);

#endif  
	const double threshold_pion   = CPT::n_mass * CPT::effn + 140.;
//	const double threshold_two_pion = CPT::n_mass * CPT::effn + 280.;
   
	p33->CrossSection().SetParams(p33_params);
	f37->CrossSection().SetParams(f37_params);
	d13->CrossSection().SetParams(d13_params);
	p11->CrossSection().SetParams(p11_params);
	s11->CrossSection().SetParams(s11_params);
	f15->CrossSection().SetParams(f15_params);
  
	qd->CrossSection().SetLowerEnergyCut(40.);
	//qd->CrossSection().SetUpperEnergyCut(400.);
	bg->CrossSection().SetLowerEnergyCut(threshold_pion);
	p33->CrossSection().SetLowerEnergyCut(threshold_pion);
  
	pev->GetChannels().Add(qd);
	pev->GetChannels().Add(bg);
	pev->GetChannels().Add(p33);
	pev->GetChannels().Add(f37);
	pev->GetChannels().Add(d13);
	pev->GetChannels().Add(p11);
	pev->GetChannels().Add(s11);
	pev->GetChannels().Add(f15);  
	pev->GetChannels().Add(rho);  
	pev->GetChannels().Add(rho0_p);  
	pev->GetChannels().Add(rho0_n);
	pev->GetChannels().Add(omg);  
	pev->GetChannels().Add(phi);  
	pev->GetChannels().Add(J_Psi);  
	///2 pions production
	pev->GetChannels().Add(p_pPipPim);
	pev->GetChannels().Add(p_nPipPi0);
	pev->GetChannels().Add(n_nPipPim);
	pev->GetChannels().Add(n_pPimPi0);
	pev->GetChannels().Add(p_p2Pi0);
	pev->GetChannels().Add(n_n2Pi0);
	///3 pions production
	pev->GetChannels().Add(p_pPipPimPi0);
	pev->GetChannels().Add(p_p3Pi0);
	pev->GetChannels().Add(p_nPip2Pi0);
	pev->GetChannels().Add(p_n2PipPim);
	pev->GetChannels().Add(n_pPip2Pim);
	pev->GetChannels().Add(n_n3Pi0);
	pev->GetChannels().Add(n_nPipPimPi0);
	pev->GetChannels().Add(n_pPim2Pi0);
	///4 pions production
	pev->GetChannels().Add(p_p2Pip2Pim);
	pev->GetChannels().Add(p_p4Pi0);
	pev->GetChannels().Add(p_pPipPim2Pi0);
	pev->GetChannels().Add(p_nPip3Pi0);
	pev->GetChannels().Add(p_n2PipPimPi0);
	pev->GetChannels().Add(n_n2Pip2Pim);
	pev->GetChannels().Add(n_n4Pi0);
	pev->GetChannels().Add(n_nPipPim2Pi0);
	pev->GetChannels().Add(n_pPim3Pi0);
	pev->GetChannels().Add(n_pPip2PimPi0);
	///5 pions production
	pev->GetChannels().Add(p_p2Pip2PimPi0);
	pev->GetChannels().Add(p_p5Pi0);
	pev->GetChannels().Add(p_pPipPim3Pi0);
	pev->GetChannels().Add(p_nPip4Pi0);
	pev->GetChannels().Add(p_n2PipPim2Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim);
	pev->GetChannels().Add(n_n2Pip2PimPi0);
	pev->GetChannels().Add(n_n5Pi0);
	pev->GetChannels().Add(n_nPipPim3Pi0);
	pev->GetChannels().Add(n_pPim4Pi0);
	pev->GetChannels().Add(n_pPip2Pim2Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim);
	///6 pions production
	pev->GetChannels().Add(p_p3Pip3Pim);
	pev->GetChannels().Add(p_p6Pi0);
	pev->GetChannels().Add(p_pPipPim4Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim2Pi0);
	pev->GetChannels().Add(p_nPip5Pi0);
	pev->GetChannels().Add(p_n2PipPim3Pi0);
	pev->GetChannels().Add(p_n3Pip2PimPi0);
	pev->GetChannels().Add(n_n3Pip3Pim);
	pev->GetChannels().Add(n_n6Pi0);
	pev->GetChannels().Add(n_nPipPim4Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim2Pi0);
	pev->GetChannels().Add(n_pPim5Pi0);
	pev->GetChannels().Add(n_pPip2Pim3Pi0);
	pev->GetChannels().Add(n_p2Pip3PimPi0);
	///7 pions production
	pev->GetChannels().Add(p_p3Pip3PimPi0);
	pev->GetChannels().Add(p_p7Pi0);
	pev->GetChannels().Add(p_pPipPim5Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim3Pi0);
	pev->GetChannels().Add(p_nPip6Pi0);
	pev->GetChannels().Add(p_n2PipPim4Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim2Pi0);
	pev->GetChannels().Add(p_n4Pip3Pim);
	pev->GetChannels().Add(n_n3Pip3PimPi0);
	pev->GetChannels().Add(n_n7Pi0);
	pev->GetChannels().Add(n_nPipPim5Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim3Pi0);
	pev->GetChannels().Add(n_pPim6Pi0);
	pev->GetChannels().Add(n_pPip2Pim4Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim2Pi0);
	pev->GetChannels().Add(n_p3Pip4Pim);
	///8 pions production
	pev->GetChannels().Add(p_p4Pip4Pim);
	pev->GetChannels().Add(p_p8Pi0);
	pev->GetChannels().Add(p_pPipPim6Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim4Pi0);
	pev->GetChannels().Add(p_p3Pip3Pim2Pi0);
	pev->GetChannels().Add(p_nPip7Pi0);
	pev->GetChannels().Add(p_n2PipPim5Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim3Pi0);
	pev->GetChannels().Add(p_n4Pip3PimPi0);
	pev->GetChannels().Add(n_n4Pip4Pim);
	pev->GetChannels().Add(n_n8Pi0);
	pev->GetChannels().Add(n_nPipPim6Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim4Pi0);
	pev->GetChannels().Add(n_n3Pip3Pim2Pi0);
	pev->GetChannels().Add(n_pPim7Pi0);
	pev->GetChannels().Add(n_pPip2Pim5Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim3Pi0);
	pev->GetChannels().Add(n_p3Pip4PimPi0);
}

void init_bremsstrahlung_evt_gen(BremsstrahlungEventGen* pev){

	if ( pev == 0 ) 
		return;
#ifndef __CINT__
	PhotonChannel*  qd = new PhotonChannel("qd", qdCrossSection, 0, qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel("bg", residual_PhotonCrossSection, 0, op_photonAction);
	PhotonChannel* p33 = new PhotonChannel("p33", p33Formation, 6, p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel("f37", f37Formation, 6, f37_photonAction);
	PhotonChannel* d13 = new PhotonChannel("d13", d13Formation, 6, d13_photonAction);
	PhotonChannel* p11 = new PhotonChannel("p11", p11Formation, 6, p11_photonAction);
	PhotonChannel* s11 = new PhotonChannel("s11", s11Formation, 6, s11_photonAction);
	PhotonChannel* f15 = new PhotonChannel("f15", f15Formation, 6, f15_photonAction);
	PhotonChannel* rho = new PhotonChannel("rho", rho_p_m_PhotonCrossSection, 0, rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel("rho0_p", rho_0_PhotonCrossSectionP, 0, rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel("rho0_n", rho_0_PhotonCrossSectionN, 0, rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel("omg", omega_PhotonCrossSection, 0, omega_photonAction);
	PhotonChannel* phi = new PhotonChannel("phi", phi_PhotonCrossSection, 0, phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel("J_Psi", J_Psi_PhotonCrossSection, 0, J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel("p_pPipPim", gp_pPipPim_NR, 0, gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel("p_nPipPi0", gp_nPipPi0_NR, 0, gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel("n_nPipPim", gn_nPipPim_NR, 0, gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel("n_pPimPi0", gn_pPimPi0_NR, 0, gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel("p_p2Pi0", gp_p2Pi0_NR, 0, gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel("n_n2Pi0", gn_n2Pi0_NR, 0, gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0, gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel("p_p3Pi0", gp_p3Pi0, 0, gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2Pi0 = new PhotonChannel("p_nPip2Pi0", gp_nPip2Pi0, 0, gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel("p_n2PipPim", gp_n2PipPim, 0, gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel("n_pPip2Pim", gn_pPip2Pim, 0, gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel("n_n3Pi0", gn_n3Pi0, 0, gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0, gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel("n_pPim2Pi0", gn_pPim2Pi0, 0, gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0, gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel("p_p4Pi0", gp_p4Pi0, 0, gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0, gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel("p_nPip3Pi0", gp_nPip3Pi0, 0, gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0, gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0, gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel("n_n4Pi0", gn_n4Pi0, 0, gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0, gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel("n_pPim3Pi0", gn_pPim3Pi0, 0, gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0, gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0, gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel("p_p5Pi0", gp_p5Pi0_NR, 0, gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0, gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel("p_nPip4Pi0", gp_nPip4Pi0, 0, gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0, gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0, gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0, gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel("n_n5Pi0", gn_n5Pi0_NR, 0, gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0, gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel("n_pPim4Pi0", gn_pPim4Pi0, 0, gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0, gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0, gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0, gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel("p_p6Pi0", gp_p6Pi0, 0, gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0, gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0, gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel("p_nPip5Pi0", gp_nPip5Pi0, 0, gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0, gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0, gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0, gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel("n_n6Pi0", gn_n6Pi0, 0, gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0, gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0, gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel("n_pPim5Pi0", gn_pPim5Pi0, 0, gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0, gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0, gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0, gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel("p_p7Pi0", gp_p7Pi0, 0, gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0, gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0, gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel("p_nPip6Pi0", gp_nPip6Pi0, 0, gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0, gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0, gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0, gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0, gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel("n_n7Pi0", gn_n7Pi0, 0, gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0, gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0, gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel("n_pPim6Pi0", gn_pPim6Pi0, 0, gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0, gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0, gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0, gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0, gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel("p_p8Pi0", gp_p8Pi0, 0, gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0, gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0, gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0, gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel("p_nPip7Pi0", gp_nPip7Pi0, 0, gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0, gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0, gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0, gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0, gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel("n_n8Pi0", gn_n8Pi0, 0, gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0, gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0, gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0, gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel("n_pPim7Pi0", gn_pPim7Pi0, 0, gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0, gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0, gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0, gn_p3Pip4PimPi0_photonAction);

#else 
	PhotonChannel*  qd = new PhotonChannel(CrossSectionChannel("qd",qdCrossSection),qd_photonAction);
	PhotonChannel*  bg = new PhotonChannel(CrossSectionChannel("bg",residual_PhotonCrossSection), op_photonAction); 
	PhotonChannel* p33 = new PhotonChannel(CrossSectionChannel("p33", p33Formation,6),p33_photonAction);
	PhotonChannel* f37 = new PhotonChannel(CrossSectionChannel("f37", f37Formation,6),f37_photonAction);    
	PhotonChannel* d13 = new PhotonChannel(CrossSectionChannel("d13", d13Formation,6),d13_photonAction);  
	PhotonChannel* p11 = new PhotonChannel(CrossSectionChannel("p11", p11Formation,6),p11_photonAction); 
	PhotonChannel* s11 = new PhotonChannel(CrossSectionChannel("s11", s11Formation,6),s11_photonAction);  
	PhotonChannel* f15 = new PhotonChannel(CrossSectionChannel("f15", f15Formation,6), f15_photonAction);    
	PhotonChannel* rho = new PhotonChannel(CrossSectionChannel("rho", rho_p_m_PhotonCrossSection, 0), rho_photonAction);
	PhotonChannel* rho0_p = new PhotonChannel(CrossSectionChannel("rho0_p", rho_0_PhotonCrossSectionP, 0), rho_0_photonAction);
	PhotonChannel* rho0_n = new PhotonChannel(CrossSectionChannel("rho0_n", rho_0_PhotonCrossSectionN, 0), rho_0_photonAction);
	PhotonChannel* omg = new PhotonChannel(CrossSectionChannel("omg", omega_PhotonCrossSection, 0), omega_photonAction);
	PhotonChannel* phi = new PhotonChannel(CrossSectionChannel("phi", phi_PhotonCrossSection, 0), phi_photonAction);
	PhotonChannel* J_Psi = new PhotonChannel(CrossSectionChannel("J_Psi", J_Psi_PhotonCrossSection, 0), J_Psi_photonAction);
	///2 pions production
	PhotonChannel* p_pPipPim = new PhotonChannel(CrossSectionChannel("p_pPipPim", gp_pPipPim_NR, 0), gp_pPipPim_photonAction);
	PhotonChannel* p_nPipPi0 = new PhotonChannel(CrossSectionChannel("p_nPipPi0", gp_nPipPi0_NR, 0), gp_nPipPi0_photonAction);
	PhotonChannel* n_nPipPim = new PhotonChannel(CrossSectionChannel("n_nPipPim", gn_nPipPim_NR, 0), gn_nPipPim_photonAction);
	PhotonChannel* n_pPimPi0 = new PhotonChannel(CrossSectionChannel("n_pPimPi0", gn_pPimPi0_NR, 0), gn_pPimPi0_photonAction);
	PhotonChannel* p_p2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pi0", gp_p2Pi0_NR, 0), gp_p2Pi0_photonAction);
	PhotonChannel* n_n2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pi0", gn_n2Pi0_NR, 0), gn_n2Pi0_photonAction);
	///3 pions production
	PhotonChannel* p_pPipPimPi0 = new PhotonChannel(CrossSectionChannel("p_pPipPimPi0", gp_pPipPimPi0_NR, 0), gp_pPipPimPi0_photonAction);
	PhotonChannel* p_p3Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pi0", gp_p3Pi0, 0), gp_p3Pi0_photonAction);
	PhotonChannel* p_nPip2Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip2Pi0", gp_nPip2Pi0, 0), gp_nPip2Pi0_photonAction);
	PhotonChannel* p_n2PipPim = new PhotonChannel(CrossSectionChannel("p_n2PipPim", gp_n2PipPim, 0), gp_n2PipPim_photonAction);
	PhotonChannel* n_pPip2Pim = new PhotonChannel(CrossSectionChannel("n_pPip2Pim", gn_pPip2Pim, 0), gn_pPip2Pim_photonAction);
	PhotonChannel* n_n3Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pi0", gn_n3Pi0, 0), gn_n3Pi0_photonAction);
	PhotonChannel* n_nPipPimPi0 = new PhotonChannel(CrossSectionChannel("n_nPipPimPi0", gn_nPipPimPi0_NR, 0), gn_nPipPimPi0_photonAction);
	PhotonChannel* n_pPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim2Pi0", gn_pPim2Pi0, 0), gn_pPim2Pi0_photonAction);
	///4 pions production
	PhotonChannel* p_p2Pip2Pim = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim", gp_p2Pip2Pim, 0), gp_p2Pip2Pim_photonAction);
	PhotonChannel* p_p4Pi0 = new PhotonChannel(CrossSectionChannel("p_p4Pi0", gp_p4Pi0, 0), gp_p4Pi0_photonAction);
	PhotonChannel* p_pPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim2Pi0", gp_pPipPim2Pi0_NR, 0), gp_pPipPim2Pi0_photonAction);
	PhotonChannel* p_nPip3Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip3Pi0", gp_nPip3Pi0, 0), gp_nPip3Pi0_photonAction);
	PhotonChannel* p_n2PipPimPi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPimPi0", gp_n2PipPimPi0, 0), gp_n2PipPimPi0_photonAction);
	PhotonChannel* n_n2Pip2Pim = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim", gn_n2Pip2Pim, 0), gn_n2Pip2Pim_photonAction);
	PhotonChannel* n_n4Pi0 = new PhotonChannel(CrossSectionChannel("n_n4Pi0", gn_n4Pi0, 0), gn_n4Pi0_photonAction);
	PhotonChannel* n_nPipPim2Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim2Pi0", gn_nPipPim2Pi0_NR, 0), gn_nPipPim2Pi0_photonAction);
	PhotonChannel* n_pPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim3Pi0", gn_pPim3Pi0, 0), gn_pPim3Pi0_photonAction);
	PhotonChannel* n_pPip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_pPip2PimPi0", gn_pPip2PimPi0, 0), gn_pPip2PimPi0_photonAction);
	///5 pions production
	PhotonChannel* p_p2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2PimPi0", gp_p2Pip2PimPi0_NR, 0), gp_p2Pip2PimPi0_photonAction);
	PhotonChannel* p_p5Pi0 = new PhotonChannel(CrossSectionChannel("p_p5Pi0", gp_p5Pi0_NR, 0), gp_p5Pi0_photonAction);
	PhotonChannel* p_pPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim3Pi0", gp_pPipPim3Pi0_NR, 0), gp_pPipPim3Pi0_photonAction);
	PhotonChannel* p_nPip4Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip4Pi0", gp_nPip4Pi0, 0), gp_nPip4Pi0_photonAction);
	PhotonChannel* p_n2PipPim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim2Pi0", gp_n2PipPim2Pi0, 0), gp_n2PipPim2Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim", gp_n3Pip2Pim, 0), gp_n3Pip2Pim_photonAction);
	PhotonChannel* n_n2Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2PimPi0", gn_n2Pip2PimPi0_NR, 0), gn_n2Pip2PimPi0_photonAction);
	PhotonChannel* n_n5Pi0 = new PhotonChannel(CrossSectionChannel("n_n5Pi0", gn_n5Pi0_NR, 0), gn_n5Pi0_photonAction);
	PhotonChannel* n_nPipPim3Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim3Pi0", gn_nPipPim3Pi0_NR, 0), gn_nPipPim3Pi0_photonAction);
	PhotonChannel* n_pPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim4Pi0", gn_pPim4Pi0, 0), gn_pPim4Pi0_photonAction);
	PhotonChannel* n_pPip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim2Pi0", gn_pPip2Pim2Pi0, 0), gn_pPip2Pim2Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim", gn_p2Pip3Pim, 0), gn_p2Pip3Pim_photonAction);
	///6 pions production
	PhotonChannel* p_p3Pip3Pim = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim", gp_p3Pip3Pim, 0), gp_p3Pip3Pim_photonAction);
	PhotonChannel* p_p6Pi0 = new PhotonChannel(CrossSectionChannel("p_p6Pi0", gp_p6Pi0, 0), gp_p6Pi0_photonAction);
	PhotonChannel* p_pPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim4Pi0", gp_pPipPim4Pi0, 0), gp_pPipPim4Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim2Pi0", gp_p2Pip2Pim2Pi0, 0), gp_p2Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_nPip5Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip5Pi0", gp_nPip5Pi0, 0), gp_nPip5Pi0_photonAction);
	PhotonChannel* p_n2PipPim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim3Pi0", gp_n2PipPim3Pi0, 0), gp_n2PipPim3Pi0_photonAction);
	PhotonChannel* p_n3Pip2PimPi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2PimPi0", gp_n3Pip2PimPi0, 0), gp_n3Pip2PimPi0_photonAction);
	PhotonChannel* n_n3Pip3Pim = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim", gn_n3Pip3Pim, 0), gn_n3Pip3Pim_photonAction);
	PhotonChannel* n_n6Pi0 = new PhotonChannel(CrossSectionChannel("n_n6Pi0", gn_n6Pi0, 0), gn_n6Pi0_photonAction);
	PhotonChannel* n_nPipPim4Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim4Pi0", gn_nPipPim4Pi0, 0), gn_nPipPim4Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim2Pi0", gn_n2Pip2Pim2Pi0, 0), gn_n2Pip2Pim2Pi0_photonAction);
	PhotonChannel* n_pPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim5Pi0", gn_pPim5Pi0, 0), gn_pPim5Pi0_photonAction);
	PhotonChannel* n_pPip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim3Pi0", gn_pPip2Pim3Pi0, 0), gn_pPip2Pim3Pi0_photonAction);
	PhotonChannel* n_p2Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3PimPi0", gn_p2Pip3PimPi0, 0), gn_p2Pip3PimPi0_photonAction);
	///7 pions production
	PhotonChannel* p_p3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3PimPi0", gp_p3Pip3PimPi0, 0), gp_p3Pip3PimPi0_photonAction);
	PhotonChannel* p_p7Pi0 = new PhotonChannel(CrossSectionChannel("p_p7Pi0", gp_p7Pi0, 0), gp_p7Pi0_photonAction);
	PhotonChannel* p_pPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim5Pi0", gp_pPipPim5Pi0, 0), gp_pPipPim5Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim3Pi0", gp_p2Pip2Pim3Pi0, 0), gp_p2Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_nPip6Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip6Pi0", gp_nPip6Pi0, 0), gp_nPip6Pi0_photonAction);
	PhotonChannel* p_n2PipPim4Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim4Pi0", gp_n2PipPim4Pi0, 0), gp_n2PipPim4Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim2Pi0", gp_n3Pip2Pim2Pi0, 0), gp_n3Pip2Pim2Pi0_photonAction);
	PhotonChannel* p_n4Pip3Pim = new PhotonChannel(CrossSectionChannel("p_n4Pip3Pim", gp_n4Pip3Pim, 0), gp_n4Pip3Pim_photonAction);
	PhotonChannel* n_n3Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3PimPi0", gn_n3Pip3PimPi0, 0), gn_n3Pip3PimPi0_photonAction);
	PhotonChannel* n_n7Pi0 = new PhotonChannel(CrossSectionChannel("n_n7Pi0", gn_n7Pi0, 0), gn_n7Pi0_photonAction);
	PhotonChannel* n_nPipPim5Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim5Pi0", gn_nPipPim5Pi0, 0), gn_nPipPim5Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim3Pi0", gn_n2Pip2Pim3Pi0, 0), gn_n2Pip2Pim3Pi0_photonAction);
	PhotonChannel* n_pPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim6Pi0", gn_pPim6Pi0, 0), gn_pPim6Pi0_photonAction);
	PhotonChannel* n_pPip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim4Pi0", gn_pPip2Pim4Pi0, 0), gn_pPip2Pim4Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim2Pi0", gn_p2Pip3Pim2Pi0, 0), gn_p2Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_p3Pip4Pim = new PhotonChannel(CrossSectionChannel("n_p3Pip4Pim", gn_p3Pip4Pim, 0), gn_p3Pip4Pim_photonAction);
	///8 pions production
	PhotonChannel* p_p4Pip4Pim = new PhotonChannel(CrossSectionChannel("p_p4Pip4Pim", gp_p4Pip4Pim, 0), gp_p4Pip4Pim_photonAction);
	PhotonChannel* p_p8Pi0 = new PhotonChannel(CrossSectionChannel("p_p8Pi0", gp_p8Pi0, 0), gp_p8Pi0_photonAction);
	PhotonChannel* p_pPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("p_pPipPim6Pi0", gp_pPipPim6Pi0, 0), gp_pPipPim6Pi0_photonAction);
	PhotonChannel* p_p2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("p_p2Pip2Pim4Pi0", gp_p2Pip2Pim4Pi0, 0), gp_p2Pip2Pim4Pi0_photonAction);
	PhotonChannel* p_p3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("p_p3Pip3Pim2Pi0", gp_p3Pip3Pim2Pi0, 0), gp_p3Pip3Pim2Pi0_photonAction);
	PhotonChannel* p_nPip7Pi0 = new PhotonChannel(CrossSectionChannel("p_nPip7Pi0", gp_nPip7Pi0, 0), gp_nPip7Pi0_photonAction);
	PhotonChannel* p_n2PipPim5Pi0 = new PhotonChannel(CrossSectionChannel("p_n2PipPim5Pi0", gp_n2PipPim5Pi0, 0), gp_n2PipPim5Pi0_photonAction);
	PhotonChannel* p_n3Pip2Pim3Pi0 = new PhotonChannel(CrossSectionChannel("p_n3Pip2Pim3Pi0", gp_n3Pip2Pim3Pi0, 0), gp_n3Pip2Pim3Pi0_photonAction);
	PhotonChannel* p_n4Pip3PimPi0 = new PhotonChannel(CrossSectionChannel("p_n4Pip3PimPi0", gp_n4Pip3PimPi0, 0), gp_n4Pip3PimPi0_photonAction);
	PhotonChannel* n_n4Pip4Pim = new PhotonChannel(CrossSectionChannel("n_n4Pip4Pim", gn_n4Pip4Pim, 0), gn_n4Pip4Pim_photonAction);
	PhotonChannel* n_n8Pi0 = new PhotonChannel(CrossSectionChannel("n_n8Pi0", gn_n8Pi0, 0), gn_n8Pi0_photonAction);
	PhotonChannel* n_nPipPim6Pi0 = new PhotonChannel(CrossSectionChannel("n_nPipPim6Pi0", gn_nPipPim6Pi0, 0), gn_nPipPim6Pi0_photonAction);
	PhotonChannel* n_n2Pip2Pim4Pi0 = new PhotonChannel(CrossSectionChannel("n_n2Pip2Pim4Pi0", gn_n2Pip2Pim4Pi0, 0), gn_n2Pip2Pim4Pi0_photonAction);
	PhotonChannel* n_n3Pip3Pim2Pi0 = new PhotonChannel(CrossSectionChannel("n_n3Pip3Pim2Pi0", gn_n3Pip3Pim2Pi0, 0), gn_n3Pip3Pim2Pi0_photonAction);
	PhotonChannel* n_pPim7Pi0 = new PhotonChannel(CrossSectionChannel("n_pPim7Pi0", gn_pPim7Pi0, 0), gn_pPim7Pi0_photonAction);
	PhotonChannel* n_pPip2Pim5Pi0 = new PhotonChannel(CrossSectionChannel("n_pPip2Pim5Pi0", gn_pPip2Pim5Pi0, 0), gn_pPip2Pim5Pi0_photonAction);
	PhotonChannel* n_p2Pip3Pim3Pi0 = new PhotonChannel(CrossSectionChannel("n_p2Pip3Pim3Pi0", gn_p2Pip3Pim3Pi0, 0), gn_p2Pip3Pim3Pi0_photonAction);
	PhotonChannel* n_p3Pip4PimPi0 = new PhotonChannel(CrossSectionChannel("n_p3Pip4PimPi0", gn_p3Pip4PimPi0, 0), gn_p3Pip4PimPi0_photonAction);

#endif  
	const double threshold_pion   = CPT::n_mass * CPT::effn + 140.;
//	const double threshold_two_pion = CPT::n_mass * CPT::effn + 280.;
   
	p33->CrossSection().SetParams(p33_params);
	f37->CrossSection().SetParams(f37_params);
	d13->CrossSection().SetParams(d13_params);
	p11->CrossSection().SetParams(p11_params);
	s11->CrossSection().SetParams(s11_params);
	f15->CrossSection().SetParams(f15_params);
  
	qd->CrossSection().SetLowerEnergyCut(40.);
	//qd->CrossSection().SetUpperEnergyCut(400.);
	bg->CrossSection().SetLowerEnergyCut(threshold_pion);
	p33->CrossSection().SetLowerEnergyCut(threshold_pion);
  
	pev->GetChannels().Add(qd);
	pev->GetChannels().Add(bg);
	pev->GetChannels().Add(p33);
	pev->GetChannels().Add(f37);
	pev->GetChannels().Add(d13);
	pev->GetChannels().Add(p11);
	pev->GetChannels().Add(s11);
	pev->GetChannels().Add(f15);  
	pev->GetChannels().Add(rho);  
	pev->GetChannels().Add(rho0_p);  
	pev->GetChannels().Add(rho0_n);
	pev->GetChannels().Add(omg);  
	pev->GetChannels().Add(phi);  
	pev->GetChannels().Add(J_Psi);  
	///2 pions production
	pev->GetChannels().Add(p_pPipPim);
	pev->GetChannels().Add(p_nPipPi0);
	pev->GetChannels().Add(n_nPipPim);
	pev->GetChannels().Add(n_pPimPi0);
	pev->GetChannels().Add(p_p2Pi0);
	pev->GetChannels().Add(n_n2Pi0);
	///3 pions production
	pev->GetChannels().Add(p_pPipPimPi0);
	pev->GetChannels().Add(p_p3Pi0);
	pev->GetChannels().Add(p_nPip2Pi0);
	pev->GetChannels().Add(p_n2PipPim);
	pev->GetChannels().Add(n_pPip2Pim);
	pev->GetChannels().Add(n_n3Pi0);
	pev->GetChannels().Add(n_nPipPimPi0);
	pev->GetChannels().Add(n_pPim2Pi0);
	///4 pions production
	pev->GetChannels().Add(p_p2Pip2Pim);
	pev->GetChannels().Add(p_p4Pi0);
	pev->GetChannels().Add(p_pPipPim2Pi0);
	pev->GetChannels().Add(p_nPip3Pi0);
	pev->GetChannels().Add(p_n2PipPimPi0);
	pev->GetChannels().Add(n_n2Pip2Pim);
	pev->GetChannels().Add(n_n4Pi0);
	pev->GetChannels().Add(n_nPipPim2Pi0);
	pev->GetChannels().Add(n_pPim3Pi0);
	pev->GetChannels().Add(n_pPip2PimPi0);
	///5 pions production
	pev->GetChannels().Add(p_p2Pip2PimPi0);
	pev->GetChannels().Add(p_p5Pi0);
	pev->GetChannels().Add(p_pPipPim3Pi0);
	pev->GetChannels().Add(p_nPip4Pi0);
	pev->GetChannels().Add(p_n2PipPim2Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim);
	pev->GetChannels().Add(n_n2Pip2PimPi0);
	pev->GetChannels().Add(n_n5Pi0);
	pev->GetChannels().Add(n_nPipPim3Pi0);
	pev->GetChannels().Add(n_pPim4Pi0);
	pev->GetChannels().Add(n_pPip2Pim2Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim);
	///6 pions production
	pev->GetChannels().Add(p_p3Pip3Pim);
	pev->GetChannels().Add(p_p6Pi0);
	pev->GetChannels().Add(p_pPipPim4Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim2Pi0);
	pev->GetChannels().Add(p_nPip5Pi0);
	pev->GetChannels().Add(p_n2PipPim3Pi0);
	pev->GetChannels().Add(p_n3Pip2PimPi0);
	pev->GetChannels().Add(n_n3Pip3Pim);
	pev->GetChannels().Add(n_n6Pi0);
	pev->GetChannels().Add(n_nPipPim4Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim2Pi0);
	pev->GetChannels().Add(n_pPim5Pi0);
	pev->GetChannels().Add(n_pPip2Pim3Pi0);
	pev->GetChannels().Add(n_p2Pip3PimPi0);
	///7 pions production
	pev->GetChannels().Add(p_p3Pip3PimPi0);
	pev->GetChannels().Add(p_p7Pi0);
	pev->GetChannels().Add(p_pPipPim5Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim3Pi0);
	pev->GetChannels().Add(p_nPip6Pi0);
	pev->GetChannels().Add(p_n2PipPim4Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim2Pi0);
	pev->GetChannels().Add(p_n4Pip3Pim);
	pev->GetChannels().Add(n_n3Pip3PimPi0);
	pev->GetChannels().Add(n_n7Pi0);
	pev->GetChannels().Add(n_nPipPim5Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim3Pi0);
	pev->GetChannels().Add(n_pPim6Pi0);
	pev->GetChannels().Add(n_pPip2Pim4Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim2Pi0);
	pev->GetChannels().Add(n_p3Pip4Pim);
	///8 pions production
	pev->GetChannels().Add(p_p4Pip4Pim);
	pev->GetChannels().Add(p_p8Pi0);
	pev->GetChannels().Add(p_pPipPim6Pi0);
	pev->GetChannels().Add(p_p2Pip2Pim4Pi0);
	pev->GetChannels().Add(p_p3Pip3Pim2Pi0);
	pev->GetChannels().Add(p_nPip7Pi0);
	pev->GetChannels().Add(p_n2PipPim5Pi0);
	pev->GetChannels().Add(p_n3Pip2Pim3Pi0);
	pev->GetChannels().Add(p_n4Pip3PimPi0);
	pev->GetChannels().Add(n_n4Pip4Pim);
	pev->GetChannels().Add(n_n8Pi0);
	pev->GetChannels().Add(n_nPipPim6Pi0);
	pev->GetChannels().Add(n_n2Pip2Pim4Pi0);
	pev->GetChannels().Add(n_n3Pip3Pim2Pi0);
	pev->GetChannels().Add(n_pPim7Pi0);
	pev->GetChannels().Add(n_pPip2Pim5Pi0);
	pev->GetChannels().Add(n_p2Pip3Pim3Pi0);
	pev->GetChannels().Add(n_p3Pip4PimPi0);
}
