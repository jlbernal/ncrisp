/* ================================================================================
 * 
 * 	Copyright 2012-2016 Israel González Evandro Segundo Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "analysis_event_gen.hh"

void analysis_photon_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool, Int_t* Channels){
	std::cout << mpool.size() << std::endl;
	for (int j = 0; j < mpool.size(); j++){
		if ( CPT::IsPion(mpool[j].PdgId()))  Channels[0]++;
		if (( mpool[j].PdgId() == CPT::rho_p_ID) ||  ( mpool[j].PdgId() == CPT::rho_m_ID) ) Channels[8]++;
		if ( mpool[j].PdgId() == CPT::rho_0_ID)  Channels[9]++;
		if ( CPT::IsOmega(mpool[j].PdgId()))  Channels[10]++;
		if ( CPT::IsPhi(mpool[j].PdgId()))  Channels[11]++;
		if ( CPT::IsJ_Psi(mpool[j].PdgId()) )  Channels[12]++;
	}
	std::cout << mpool.size() << std::endl;
}
void analysis_proton_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par){
}

void analysis_bremsstrahlung_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par){
}

void analysis_ultra_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par){
}

void analysis_hyperon_evt_genBf(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par){
	TVector3 P1 = nuc[(Int_t)Par[1]].Momentum().Vect();
	TVector3 P2 = nuc[(Int_t)Par[2]].Momentum().Vect();
	Double_t cosTheta = P1.Dot(P2)/(P1.Mag()*P2.Mag());
	if (nuc[(Int_t)Par[2]].PdgId() ==  CPT::neutron_ID){
		hists[0].Fill(Par[4]);  //neutron 1 NN channel Momentum Distribuition  CM coord system
		hists[1].Fill(Par[5]);  //neutron 2 NN channel Momentum Distribuition  CM coord system
		hists[4].Fill(P1.Mag());  //neutron 1 NN channel Momentum Distribuition LAB coord system
		hists[5].Fill(P2.Mag());  //neutron 2 NN channel Momentum Distribuition LAB coord system
		hists[8].Fill(Par[3]);  	//Angular distribution. NN channnel. CM  coord system
		hists[10].Fill(cosTheta);  	//Angular distribution. NN channnel. LAB coord system
		hists[12].Fill(1);		// Particle pair count. NN channel. 
	}
	if (nuc[(Int_t)Par[2]].PdgId() ==  CPT::proton_ID){
		hists[2].Fill(Par[4]);  //neutron 1 NP channel Momentum Distribuition CM coord system
		hists[3].Fill(Par[5]);  //proton  2 NP channel Momentum Distribuition CM coord system
		hists[6].Fill(P1.Mag());  //neutron 1 NP channel Momentum Distribuition LAB coord system
		hists[7].Fill(P2.Mag());  //proton  2 NP channel Momentum Distribuition LAB coord system
		hists[9].Fill(Par[3]);  	//Angular distribution. NP channnel. CM  coord system
		hists[11].Fill(cosTheta);  	//Angular distribution. NP channnel. LAB coord system
		hists[13].Fill(1);		// Particle pair count. NP channel.
	}
}
void analysis_hyperon_evt_gen(NucleusDynamics& nuc, MesonsPool& mpool,  TH1D *hists, Int_t Nhists, std::vector<Double_t> &Par){
	std::vector<ParticleDynamics> v;
	for (Int_t i = 0; i < nuc.GetA(); i++)
		if (!nuc[i].IsBind()){
			if ( CPT::IsNucleon(nuc[i].PdgId() ) )  v.push_back(nuc[i]);
			if (nuc[i].PdgId() ==  CPT::neutron_ID) hists[14].Fill( nuc[i].TKinetic()); // neutron single kinetic espectro
			if (nuc[i].PdgId() ==  CPT::proton_ID)  hists[15].Fill( nuc[i].TKinetic()); // proton  single kinetic espectro
		}
	if (v.size() == 2){
		Double_t mag = v[0].Momentum().Vect().Mag() * v[1].Momentum().Vect().Mag();
		Double_t cosTheta = v[0].Momentum().Vect().Dot(v[1].Momentum().Vect()) / mag;
		if ( (v[0].PdgId() ==  CPT::neutron_ID)&&(v[1].PdgId() ==  CPT::neutron_ID) ){
			hists[16].Fill(v[0].TKinetic() + v[1].TKinetic()); 				// Neutron-Neutron pair Total Kinetic energy
			hists[18].Fill(cosTheta); 							// Neutron-Neutron pair Angular distribution
			hists[20].Fill(1);								// Neutron Neutron pair count after cascade
		}
		if ( v[0].PdgId() !=  v[1].PdgId() ){
			hists[17].Fill(v[0].TKinetic() + v[1].TKinetic()); 				// Neutron-Proton pair Total Kinetic energy
			hists[19].Fill(cosTheta);							// Neutron-Proton pair Angular distribution
			hists[21].Fill(1);								// Neutron Proton pair count after cascade
		}
	}
}

void Tree_init(TTree* InfoTree, Int_t InfoLev){
	Nuc_Struct NUC;
	Mes_Struct MPOOL;
	if ( (InfoLev == 0)||(InfoLev == 1)||(InfoLev == 2)||(InfoLev == 3) ){
		if (InfoLev == 1){
		  	InfoTree->Branch("FNuc_A",&NUC.A,"FA/I");
			InfoTree->Branch("FNuc_Z",&NUC.Z,"FZ/I");
			InfoTree->Branch("Plevels",NUC.PLevels,"Nuc_PLevels[30]/I");
			InfoTree->Branch("Nlevels",NUC.NLevels,"Nuc_NLevels[30]/I");
			InfoTree->Branch("Nuc_P",NUC.Nuc_P, "Nuc_P[A]/D");
			InfoTree->Branch("Nuc_Inx",NUC.Nuc_IDX, "Nuc_IND[A]/I");
			InfoTree->Branch("Nuc_Pid",NUC.Nuc_PID, "Nuc_PID[A]/I");
			InfoTree->Branch("Nuc_IsBind",NUC.Nuc_IsB, "Nuc_IsB[A]/O");
			InfoTree->Branch("Nuc_PX",NUC.Nuc_PX, "Nuc_PX[A]/D");
			InfoTree->Branch("Nuc_PY",NUC.Nuc_PY, "Nuc_PY[A]/D");
			InfoTree->Branch("Nuc_PZ",NUC.Nuc_PZ, "Nuc_PZ[A]/D");
			InfoTree->Branch("Nuc_E",NUC.Nuc_E, "Nuc_E[A]/D");
			InfoTree->Branch("Nuc_K",NUC.Nuc_K, "Nuc_K[A]/D");
		}
		if (InfoLev == 2){
		  	InfoTree->Branch("Mes_NM",&MPOOL.NM,"NM/I");
			InfoTree->Branch("Mes_key",MPOOL.Mes_KEY, "Mes_KEY[NM]/I");
			InfoTree->Branch("Mes_Pid",MPOOL.Mes_PID, "Mes_PID[NM]/I");
			InfoTree->Branch("Mes_IsBind",MPOOL.Mes_IsB, "Mes_IsB[NM]/O");
			InfoTree->Branch("Mes_PX",MPOOL.Mes_PX, "Mes_PX[NM]/D");
			InfoTree->Branch("Mes_PY",MPOOL.Mes_PY, "Mes_PY[NM]/D");
			InfoTree->Branch("Mes_PZ",MPOOL.Mes_PZ, "Mes_PZ[NM]/D");
			InfoTree->Branch("Mes_E",MPOOL.Mes_E, "Mes_E[NM]/D");
			InfoTree->Branch("Mes_K",MPOOL.Mes_K, "Mes_K[NM]/D");

		}
		if (InfoLev == 3) {
			InfoTree->Branch("Nuc_X",NUC.Nuc_X, "Nuc_X[A]/D");
			InfoTree->Branch("Nuc_Y",NUC.Nuc_Y, "Nuc_Y[A]/D");
			InfoTree->Branch("Nuc_Z",NUC.Nuc_Z, "Nuc_Z[A]/D");
			InfoTree->Branch("Mes_X",MPOOL.Mes_X, "Mes_X[NM]/D");
			InfoTree->Branch("Mes_Y",MPOOL.Mes_Y, "Mes_Y[NM]/D");
			InfoTree->Branch("Mes_Z",MPOOL.Mes_Z, "Mes_Z[NM]/D");
		}
	} else {
		std::cout << "Wrong info level index\n";
		return;
	}
}

void hyperon_histograms_init(TH1D *hists){
//		Before cascade's histograms
		hists[0] = TH1D("HypPNN_n1B", "Neutron 1 Momentum distribution. NN channnel. Before cascade. CM coord system",20,0,1000);
		hists[1] = TH1D("HypPNN_n2B", "Neutron 2 Momentum distribution. NN channnel. Before cascade. CM coord system",20,0,1000);
		hists[2] = TH1D("HypPNP_n1B", "Neutron 1 Momentum distribution. NP channnel. Before cascade. CM coord system",20,0,1000);
		hists[3] = TH1D("HypPNP_p2B", "Proton  2 Momentum distribution. NP channnel. Before cascade. CM coord system",20,0,1000);

		hists[4] = TH1D("HypPNN_n1BL", "Neutron 1 Momentum distribution. NN channnel. Before cascade. LAB coord system",20,0,1000);
		hists[5] = TH1D("HypPNN_n2BL", "Neutron 2 Momentum distribution. NN channnel. Before cascade. LAB coord system",20,0,1000);
		hists[6] = TH1D("HypPNP_n1BL", "Neutron 1 Momentum distribution. NP channnel. Before cascade. LAB coord system",20,0,1000);
		hists[7] = TH1D("HypPNP_p2BL", "Proton  2 Momentum distribution. NP channnel. Before cascade. LAB coord system",20,0,1000);

		hists[8] = TH1D("HypCosThetaNNB", "Angular distribution. NN channnel. before cascade CM  coord system",32,-1,0.6);
		hists[9] = TH1D("HypCosThetaNPB", "Angular distribution. NP channnel. before cascade CM  coord system",32,-1,0.6);
		hists[10] = TH1D("HypCosThetaNNBL","Angular distribution. NN channnel. before cascade LAB coord system",32,-1,0.6);
		hists[11] = TH1D("HypCosThetaNPBL","Angular distribution. NP channnel. before cascade LAB coord system",32,-1,0.6);

		hists[12] = TH1D("HypPairNNBf","Neutron Neutron Pair count before cascade",2,0,2);
		hists[13] = TH1D("HypPairNPBf","Neutron Proton Pair count before cascade",2,0,2);
//		After cascade's histograms
		hists[14] = TH1D("HypKN","Single neutron kinetic energy distribution",18,0,180);
		hists[15] = TH1D("HypKP","Single proton  kinetic energy distribution",18,0,180);
		hists[16] = TH1D("HypKNN","kinectic energy pair Channel NN distribution",25,0,250);
		hists[17] = TH1D("HypKNP","kinectic energy pair Channel NP  distribution",25,0,250);
		hists[18] = TH1D("HypCosThetaNN","Neutron-Neutron Angular distribution",40,-1,0.6);
		hists[19] = TH1D("HypCosThetaNP","Neutron-Proton  Angular distribution",40,-1,0.6);
		hists[20] = TH1D("HypPairNN","Neutron Neutron Pair count after cascade",2,0,2);
		hists[21] = TH1D("HypPairNP","Neutron Proton Pair count after cascade",2,0,2);
}


UnBindPart_Struct GetUnBindParticles(NucleusDynamics& nuc, MesonsPool& mpool){
	UnBindPart_Struct UnBindPart;
	Int_t partCount = 0;
	for (int i = 0; i < nuc.GetA(); i++){
		if (!nuc[i].IsBind()){			
			if (nuc[i].PdgId() == 0) std::cout << "Nucleon with ID = 0. Too many of this can be a problem.\n";
			/*
			 * NOTE: The kinetic energy of an unbound nucleon is the kinetic energy in the infinity and
			 * is necessarily equal to the energy taken from the nucleus excitation energy which is given by:
			 * 
			 * nuc[i].Momentum().E() - nuc[i].GetMass()*CPT::effn - well; cascade/SelectedProcess.cc
			 * 
			 * The nuclear potential (regardless of nucleon charge) accounts for the difference between
			 * bound and unbound nucleon not the mass difference (m0 - m*) since in CRISP code the effective mass m*
			 * is only an average accounting for the effect of the nuclear potential over the masses only in average.
			 * 
			 * Being consistent with the excitation energy balance calculation throughout the cascade process
			 * the kinetic energy of scaping nucleons must be calculated according to the above mentioned equation.
			 * The class method TKinetic() works fine inside the nucleus but using it outside is equivalent to make use
			 * of the mass difference (m0 - m*).
			 * 
			 * For the mesons below, there is no problem in using the class method TKinetic() for now because
			 * up to this realease, the nuclear potential for mesons in being aproximated in CRISP exactly by the 
			 * mass difference (m0 - m*). See file base/NucleusDynamics.cc for confirmation.
			 */ 
			double well = nuc.NuclearPotentialWell( nuc[i].PdgId() ); 
			double KineticEn = nuc[i].Momentum().E() - nuc[i].GetMass()*CPT::effn - well;
			UnBindPart.Part_PID[partCount] = nuc[i].PdgId();
			UnBindPart.Part_PX[partCount] = nuc[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = nuc[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = nuc[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = nuc[i].Momentum().E();
			//UnBindPart.Part_K[partCount] = nuc[i].TKinetic();
			UnBindPart.Part_K[partCount] = KineticEn;
			partCount++;
		}
	}
	for (int i = 0; i < mpool.size(); i++){
		if (!mpool[i].IsBind()){
			if (mpool[i].PdgId() == 0) std::cout << "Meson with ID = 0. Too many of this can be a problem.\n";
			UnBindPart.Part_PID[partCount] = mpool[i].PdgId();
			UnBindPart.Part_PX[partCount] = mpool[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = mpool[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = mpool[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = mpool[i].Momentum().E();
			UnBindPart.Part_K[partCount] = mpool[i].TKinetic();
			partCount++;
		}
	}
	UnBindPart.NPart = partCount;
	return UnBindPart;
}


double ExcitationEnergy(NucleusDynamics& nuc, MesonsPool& mpool){

	double ExEnergy = 0.;
	int a = 0, z = 0;
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() ) {
			if ( nuc[i].PdgId() == CPT::neutron_ID ) 
				a++; 
			if ( nuc[i].PdgId() == CPT::proton_ID ) {
				a++; 
				z++;
			}
		}
	}
	//cout << "a: " << a << "  z: " << z << endl;
	NucleusDynamics groundState(a,z);
	double ground = groundState.RetrieveTotalSysEn();
	double current =  nuc.TotalSystemEnergy() + mpool.TotalEnergy();
	
	ExEnergy = current - ground;
	
	return ExEnergy;
}


/* overloading to add leptons		*/


UnBindPart_Struct GetUnBindParticles(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool){
	UnBindPart_Struct UnBindPart;
	Int_t partCount = 0;
	for (int i = 0; i < nuc.GetA(); i++){
		if (!nuc[i].IsBind()){			
			if (nuc[i].PdgId() == 0) std::cout << "Nucleon with ID = 0. Too many of this can be a problem.\n";
			/*
			 * NOTE: The kinetic energy of an unbound nucleon is the kinetic energy in the infinity and
			 * is necessarily equal to the energy taken from the nucleus excitation energy which is given by:
			 * 
			 * nuc[i].Momentum().E() - nuc[i].GetMass()*CPT::effn - well; cascade/SelectedProcess.cc
			 * 
			 * The nuclear potential (regardless of nucleon charge) accounts for the difference between
			 * bound and unbound nucleon not the mass difference (m0 - m*) since in CRISP code the effective mass m*
			 * is only an average accounting for the effect of the nuclear potential over the masses only in average.
			 * 
			 * Being consistent with the excitation energy balance calculation throughout the cascade process
			 * the kinetic energy of scaping nucleons must be calculated according to the above mentioned equation.
			 * The class method TKinetic() works fine inside the nucleus but using it outside is equivalent to make use
			 * of the mass difference (m0 - m*).
			 * 
			 * For the mesons below, there is no problem in using the class method TKinetic() for now because
			 * up to this realease, the nuclear potential for mesons in being aproximated in CRISP exactly by the 
			 * mass difference (m0 - m*). See file base/NucleusDynamics.cc for confirmation.
			 */ 
			double well = nuc.NuclearPotentialWell( nuc[i].PdgId() ); 
			double KineticEn = nuc[i].Momentum().E() - nuc[i].GetMass()*CPT::effn - well;
			UnBindPart.Part_PID[partCount] = nuc[i].PdgId();
			UnBindPart.Part_PX[partCount] = nuc[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = nuc[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = nuc[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = nuc[i].Momentum().E();
			//UnBindPart.Part_K[partCount] = nuc[i].TKinetic();
			UnBindPart.Part_K[partCount] = KineticEn;
			partCount++;
		}
	}
	for (int i = 0; i < mpool.size(); i++){
		if (!mpool[i].IsBind()){
			if (mpool[i].PdgId() == 0) std::cout << "Meson with ID = 0. Too many of this can be a problem.\n";
			UnBindPart.Part_PID[partCount] = mpool[i].PdgId();
			UnBindPart.Part_PX[partCount] = mpool[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = mpool[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = mpool[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = mpool[i].Momentum().E();
			UnBindPart.Part_K[partCount] = mpool[i].TKinetic();
			partCount++;
		}
	}

	cout << mpool.size() << " size of mesons pool" << endl;
	cout << lpool.size() << " size of leptons pool" << endl;

	for (int i = 0; i < lpool.size(); i++){
		if (!lpool[i].IsBind()){
			if (lpool[i].PdgId() == 0) {
				//std::cout << "Lepton with ID = 0. Too many of this can be a problem.\n";
				continue;
			}   	
			UnBindPart.Part_PID[partCount] = lpool[i].PdgId();
			UnBindPart.Part_PX[partCount] = lpool[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = lpool[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = lpool[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = lpool[i].Momentum().E();
			UnBindPart.Part_K[partCount] = lpool[i].TKinetic();
			partCount++;
		}
		else{
		  std::cout << "binded lepton" << endl;
		  if (lpool[i].PdgId() == 0) {
				//std::cout << "Lepton with ID = 0. Too many of this can be a problem.\n";
				continue;
			}   	
			UnBindPart.Part_PID[partCount] = lpool[i].PdgId();
			UnBindPart.Part_PX[partCount] = lpool[i].Momentum().Px();
			UnBindPart.Part_PY[partCount] = lpool[i].Momentum().Py();
			UnBindPart.Part_PZ[partCount] = lpool[i].Momentum().Pz();
			UnBindPart.Part_E[partCount] = lpool[i].Momentum().E();
			UnBindPart.Part_K[partCount] = lpool[i].TKinetic();
			partCount++;
		}
	}
	UnBindPart.NPart = partCount;
	return UnBindPart;
}
