/* ================================================================================
 * 
* 	Copyright 2008, 2012, 2015 Israel González, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include <iostream>
#include <fstream>
#include <map>
#include <exception>
#include <string>
#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "PhotonEventGen.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "MesonsPool.hh"
#include "LeptonsPool.hh"
#include "ProtonEventGen.hh"
#include "Cascade.hh"
#include "cascade.hh"
#include "Timer.hh"

#include "TString.h"
#include "TObjString.h"
#include "TGraphErrors.h"
#include "TRandom3.h"


#ifdef WIN32
#include <windows.h>
#pragma comment (lib, "WSock32.Lib")
#endif

#ifdef CRISP_MPI
#include <mpi.h>
#endif // CRISP_MPI

using namespace std;
int main( int argc, char *argv[] ){
// verificaciones iniciales para el correcto funcionamiento del programa
	fstream file;
	TString line = "--help";
	if (argc == 1) {
		printf ("Syntax Error:  <program> [file]\n");
		printf ("Type --help for help\n\n");
		return 1;		
	}
	if (argc > 1 && line.CompareTo(argv [1]) == 0) {
		printf ("Syntax:  <program> [file]\n\n");
		printf ("file:  full name of the cascade input file -this file must be *.crisp \n");
		return 1;		
	}	
	file.open (argv [1], ios::in);
	if (!file.is_open ()) {
		printf ("File not found: %s\n", argv [1]);
		return 1;
	}
	DataStruct Data;
	Data.ReadData(file);
	if (!Data.IsDataOk){
		std::cout << "Input error\n";
		return 1;
	}
	int rank(0);
	int size(1);
#ifdef CRISP_MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
	double tiempo_inicial, tiempo_final;
	tiempo_inicial = MPI_Wtime();
#endif // CRISP_MPI
	if( rank == 0 )
		std::cout << "Number of MPI processes: " << size << "\n";

	// Gerando numeros aleatórios diferentes para cada host
	// TRandom3 gera numeros aleatórios muito melhores que o default TRandom
	// Eh dado um numero primo diferente para cada noh como seed
	
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	// Loading Particles ...
	if( rank == 0 )	std::cout << std::endl << "Loading PDG Particle Data ..." << std::endl;
	cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");
	if( rank == 0 )	std::cout << "Finished loading PDG Particle Data." << std::endl;

#ifdef CRISP_MPI
	MPI_Barrier( MPI_COMM_WORLD );
#endif // CRISP_MPI
#ifdef _DEBUG
	char hostname[256];
	gethostname(hostname, sizeof(hostname));
#ifndef WIN32
	std::cout <<"Rank " << rank << " : PID " << getpid() << " on " << hostname << " ready for attach\n";
//	sleep(15);
#else
	std::cout <<"Rank " << rank << " : PID " << GetCurrentProcessId() << " on " << hostname << " ready for attach\n";
//	Sleep(15000);
#endif // WIN32
	fflush(stdout);
#endif // _DEBUG
	double TempoInicial(0.0), TempoFinal(0.0), TempoTotal(0.0);
	if( rank == 0 )
		TempoInicial = CRISPGetTime()*0.001;

	TString prefix = "results/mcmc/";
	NucleusDynamics* nuc;
	MesonsPool* mpool;
	LeptonsPool* lpool;
	TString fname;
  	for ( int i = 0 ; i < Data.CascNumber; i++ ) {
		if( gRandom )
			delete gRandom;
		gRandom = new TRandom3( primos[rank] );
		if(Data.seed[i])
			gRandom->SetSeed(0);
		std::cout << "Rank number = " << rank << ", Seed  = " << gRandom->GetSeed() << std::endl;


		gSystem->mkdir( ( prefix + Data.TyCas[i] + "/" + Data.nucleusName[i]).Data() , kTRUE);	
		
		fname = prefix + Data.TyCas[i] + "/" + Data.nucleusName[i]+ "/" + Data.nucleusName[i] + "_%dMeV_%d.root";
		
	  	nuc = new NucleusDynamics(Data.A[i], Data.Z[i]);
	  	mpool = new MesonsPool; 
		lpool =  new LeptonsPool;
		
		
	  	for ( int j = 0; j < Data.number_of_energies[i]; j++ ) {
	  		Double_t _energy = Data.start_energy[i] + j * Data.step_energy[i];
			TString completeFileName = Form(fname.Data(), (int)_energy, Data.number_of_times_run[i]);
			execute_cascade( nuc, mpool, lpool, _energy, Data.number_of_times_run[i], completeFileName.Data(), Data.Parameters[i], Data.TyCas[i], Data.printUnbindParticles[i], Data.Print_Internal_Casc[i]);
		}
		delete nuc;
		delete mpool;
	}
#ifdef CRISP_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif // CRISP_MPI
	if( rank == 0 ){
		TempoFinal = CRISPGetTime()*0.001;
		TempoTotal = TempoFinal - TempoInicial;
		std::cout << "Total simulation time: ";
		FormTime( TempoTotal, std::cout );
		std::cout << std::endl << std::endl << std::endl;
	}

#ifdef CRISP_MPI
	MPI_Finalize();
#endif // CRISP_MPI
	return 0;
}

