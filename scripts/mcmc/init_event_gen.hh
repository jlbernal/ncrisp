/* ================================================================================
 * 
* 	Copyright 2008, 2012, 2015 Israel González, Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "MesonsPool.hh"
#include "PhotonEventGen.hh"
#include "BremsstrahlungEventGen.hh"
#include "UltraEventGen.hh"
#include "ProtonEventGen.hh"
#include "DeuteronEventGen.hh"
#include "IonEventGen.hh"
#include "HyperonEventGen.hh"
#include "NeutrinoEventGen.hh"


#include "PhotonChannel.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "bb_cross_sections.hh"
#include "Lepton_n_cross_sections.hh"
#include "bb_actions.hh"
#include "Lepton_n_actions.hh"
#include "BBChannel.hh"
#include "LeptonNucleonChannel.hh"

#ifdef CRISP_MINUIT
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/FCNBase.h"
#endif // CRISP_MINUIT

void init_photon_evt_gen(PhotonEventGen* pev);
void init_proton_evt_gen(ProtonEventGen* p);
void init_deuteron_evt_gen(DeuteronEventGen* deut);
void init_ion_evt_gen(IonEventGen* ion);
void init_bremsstrahlung_evt_gen(BremsstrahlungEventGen* pev);
void init_ultra_evt_gen(UltraEventGen* pev);
void init_hyperon_evt_gen(HyperonEventGen* p);
void init_neutrino_evt_gen(NeutrinoEventGen* neutrino);


