/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//================================================================================================================
// Adjust of Parameters using TMinuit.h
//================================================================================================================

#include "TMinuit.h"
#include "TMath.h"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

#include "TCanonical.h"
#include "TPartition.h"
#include "TStoragef.h"
#include "TSMM.h"
#include "definedvalues.h"

#include <iomanip>
#include <fstream>
#include <cmath>
#include <string>
#include <map>

using namespace std;

Double_t __initial_energy;
Int_t __A, __Z;
Int_t __counts;
Double_t __ex_energy;

// Carrega o arquivo de cascata.
void read_cascade_line ( std::istream& ifs){

	TString inStr;
	TString delim = " ";

	if ( ifs.good() ) {
	
		inStr.ReadLine(ifs);
    
		if ( inStr.Length() == 0 ) 
			return;
    
		TObjArray* arr = inStr.Tokenize(delim);

		if ( (( TObjString *)(*arr)[1])->GetString()[0] != '#' ) {

			__initial_energy = (( TObjString *)(*arr)[1])->GetString().Atof();
			__counts = (( TObjString *)(*arr)[2])->GetString().Atoi();
			__ex_energy = (( TObjString *)(*arr)[3])->GetString().Atof();
			__A = (( TObjString *)(*arr)[4])->GetString().Atoi();
			__Z = (( TObjString *)(*arr)[5])->GetString().Atoi();

			arr->Delete();
			delete arr;
		}
	}
}

//ofstream out("multifrag_saida.txt");

void ChiSquare(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){

	TCanonical *can = new TCanonical();

	can->Seteps_0( par[0] );
	can->SetD_( par[1] );
	can->SetT_c( par[2] );

	TString nucleus = "Fe56";
	TString energy[4] = {"500", "750", "1000", "1500"};
	TString exp_file;
	TString casc_file;

	string str;

	int Z = 0, A = 0, ZA = 0;
	double expY = 0.,
			 Chi = 0.,
			 ChiT = 0.,
			 chi = 0.,
			 delta = 0.,
			 sf = 0.;

	//ofstream out("multifrag_saida.txt");

	TStoragef BANK;

	int linha = 0;
	int hist = 1000;	
	int count = 0;
	double mount = 0;

	int N = 0;
	for(int i=0; i<1; i++){	
	
		exp_file = "exp_data/multi_fragmentacao/" + nucleus + "/" + energy[i] + "MeVp/" + nucleus + "_plus_p_" + energy[i] + "MeV.txt";
		casc_file = "results/proton/" + nucleus + "/" + nucleus + "_" + energy[i] + ".cascade";

		ifstream casc(casc_file.Data());
		while(casc.good()){

			read_cascade_line(casc);
			if(__ex_energy/__A >= par[3]){

				TPartition *a = new TPartition(__A, __Z, __ex_energy);
	
				while ( count < hist ){
	
					mount = a->GetWeight();
					a->MakeProcess();
					BANK.AddWeight(mount);
					BANK.AddMultiplicity(a->GetMultiplicity(),mount);
					BANK.AddTemperature(a->GetTemperature(),mount);
					BANK.AddEntropy(a->GetEntropy(),mount);
					BANK.AddCv(a->GetCv(),mount);
					BANK.AddPartition(a->GetPartition(),mount);

					count++;
				}

				count = 0;
				delete a;
			}
		}

		BANK.MakeStatistics();

		//out << BANK;
	
		ifstream exp(exp_file.Data());
		while(exp.good()){

			getline(exp, str);
			if(str.size() != 0 && str[0] != '#'){

				exp >> Z >> A >> expY;

				ZA = 1000*Z+A;

				if( BANK.exist(ZA) ){

					sf = PI * (r_0*pow(A,1./3)) * (r_0*pow(A,1./3));
					delta = (expY - BANK.GetYield(ZA) * sf * 10.) / (sqrt(BANK.GetYield(ZA)) * sf * 10.);
					chi = delta*delta;
					N++;
				}
				else{
					chi = 1.e40;
					N++;
				}
				//cout << "expY : " << expY << "\t CRISP: " << BANK.GetYield(ZA) * sf * 10. << "\t chi: " << chi << endl;
			}
			//cout << "chi: " << chi << endl;
			Chi = Chi + chi;
			str.clear();
		}
		ChiT = ChiT + Chi;
		//cout << "Chi: " << Chi << endl;
		Chi = 0.;
		BANK.reset();

	} // loop for

	//cout << N - npar << endl;

	f = ChiT/(N - npar);

}

int MCMM_Minuit(){

	TMinuit min(15);

	//min.SetPrintLevel(3);
	//  select verbose level:
  	//    default :     (58 lines in this test)
  	//    -1 : minimum  ( 4 lines in this test)
  	//     0 : low      (31 lines)
  	//     1 : medium   (61 lines)
  	//     2 : high     (89 lines)
  	//     3 : maximum (199 lines in this test)

	min.SetFCN(ChiSquare);

	static const int arg = 2;
	double arglist[arg];

	int ierflg = 0;

	//Definição do parâmetro de erro UP
  	arglist[0] = 1; //UP=1 para chisquare
  	min.mnexcm("SET ERR", arglist ,1 , ierflg);

  	//Configurando parâmetros, starts, steps e limites
	static const int nPar = 4;

//CONJUNTO DE PARÂMETROS
//________________________________________________________________


	double vstart[nPar] = { 16.00,			//eps_0
									 2.3,				//D
									16.00,			//T_c
									 3.5};			//E_c

//_______________________________________________________________________________________


  	double step[nPar] = {1.5, 1.5, 1.5, 2.0}; 
  	min.mnparm(0, "eps_0", vstart[0], step[0], 0, 0, ierflg);  
  	min.mnparm(1, "D", vstart[1], step[1], 0, 0, ierflg); 
  	min.mnparm(2, "T_c", vstart[2], step[2], 0, 0, ierflg);
  	min.mnparm(3, "E_c", vstart[3], step[3], 0, 0, ierflg); 

//	min.FixParameter(0);
//	min.FixParameter(1);         
//	min.FixParameter(2);

	cout << "\n Minimizacao..." << endl << endl;

  	arglist[0] = 3; //level
	min.mnexcm("SET PRIntout", arglist, 1, ierflg);

	//min.mnexcm("SCAN", 0, 0, ierflg);

	//Configuração do MINImize. Se MIGrad não converge, muda para SIMplex 
	//0.001*[tolerance]*UP
	//UP=1 para chisquare (comando SET ERR)
  	arglist[0] = 100; //máximo de chamadas
  	arglist[1] = 0.001;  //tolerance
  	//min.mnexcm("MINI", arglist, 2, ierflg);
	//min.mnexcm("SIMplex", arglist, 2, ierflg);
	min.mnexcm("MIGrad", arglist, 2, ierflg);

	//Busca por mínimo local adicional - Comando IMProve
	//arglist[0] = 100; //máximo de chamadas
  	//min->mnexcm("IMP", arglist ,1 , ierflg);

	return 0;
}
