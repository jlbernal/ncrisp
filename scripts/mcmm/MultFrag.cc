/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#define _WIN32_

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <string>
#include <fstream>  
#include <string>
#include <sstream>	

#include "TPartition.h"
#include "TStoragef.h"
#include "TSMM.h"
#include "definedvalues.h"

using namespace std;

void Print(){

cout<<"###############################################################################"<<"\n";
cout<<"#                                                                             #"<<"\n";
cout<<"# ### ###  # ### ###   ### ###  ### ###  #   # ### #  # ### ### ### # ### #  ##"<<"\n";
cout<<"# #   # #  # #   # #   #   # #  # # #    ## ## #   ## #  #  # #  #  # # # ## ##"<<"\n";
cout<<"# #   ###  #   # ### # ##  ###  ### # ## # # # ##  ####  #  ###  #  # # # #####"<<"\n";
cout<<"# ### #  # # ### #     #   #  # # # ###  #   # ### #  #  #  # #  #  # ### #  ##"<<"\n";
cout<<"#                                                                             #"<<"\n";
cout<<"###############################################################################"<<"\n";                                                                                
cout<<"                                        BB                                     "<<"\n"; 
cout<<"        #                        M i SBX;r#BS                                  "<<"\n"; 
cout<<"                               33;#, :X;rrG9rH3#                               "<<"\n"; 
cout<<"                              H.; SXX.i&sSsX.,; r         ####B#HHAHAM, #      "<<"\n"; 
cout<<"                             r&h,MrS i ,B;iA;.hH:.M#BAAAHAHHMB#M## #           "<<"\n"; 
cout<<"                            H2H AA,9#2: G, 39S AG5M                            "<<"\n"; 
cout<<"      9 ; #M#M#M#M#M#B#B##M&;ihrMi ssG#9; X2.5 #HH2##M## M#M#M#M#BMB#B#BMHBAH3H"<<"\n"; 
cout<<"       2M B#B#M#######M#M#BH;i&:#9.S3S. rA:;.:A S., ##M###M#M#M#M#B#M#M#M###H&A"<<"\n"; 
cout<<"                            rsr9# M r.Xrr #i; ;ii B                            "<<"\n"; 
cout<<"  #                         r:,;..i2h #r S;;sB r; X#AHABAMHBB###               "<<"\n"; 
cout<<"           #                  BA; :: ,9,, i iX9iXM     # ##M#HMHHAAAHAAAH :    "<<"\n"; 
cout<<"                              # r:# , Xi,h;.;&A ;                        GA    "<<"\n"; 
cout<<"                                S.::Ghi:9 r ,X##                               "<<"\n"; 
cout<<"                                  #M2 A;i A#                                   "<<"\n"; 
cout<<"                                                                               "<<"\n"; 
cout<<"                                                                         ##    "<<"\n"; 
cout<<"                                                 &#                    M::s    "<<"\n"; 
cout<<"             #:,,sHX;.,2AHM                       9B                   #,Xi.#  "<<"\n"; 
cout<<"      Mr.,;s9  rMHX   Xh   s                    G: 9B#                 BB##B   "<<"\n"; 
cout<<"     A.ir  ,;r .. .2r:sA#9B;.2B                 #;S#MHH               AG       "<<"\n"; 
cout<<"     H ;i:   & .  B A3GHs ;;   :                    MAAG             BH        "<<"\n"; 
cout<<"        .  .:#;rh,r..s r#S,M#r  ;SsA                  H&XB          BA         "<<"\n"; 
cout<<"     5 2 i  3HM  X    i s.. S 3 :G ;                    B59   AiSii ;.r#       "<<"\n"; 
cout<<"    B, 2Hi.MA ,Hs:sisr.2. s; #A,&sr                       HGA;,      2AMh3A    "<<"\n"; 
cout<<"  h.  .i i&Gr :r& G#s ;   r  #A H;A                        h  r       ,Xi:5    "<<"\n"; 
cout<<"  i   s2 :: S :i.. si. :S; 5  r;. 5           ,s          &,    5,     ;B      "<<"\n"; 
cout<<"    X,  ri G,GM.,A; ;9r2; #S;#2 ,A#          #,r         #,           .X    X# "<<"\n"; 
cout<<"     :   .A; i  22S# A#A;r h ;, ,                          &         ;X     H  "<<"\n"; 
cout<<"    G  :AS, ,.rX r;#::;  :A .H&2  #                         .       :i         "<<"\n"; 
cout<<"      ;.   ; . ,;  ,     ;& Br5#  M                     BH#25 #AS2.s  &HM      "<<"\n"; 
cout<<"      M .r;:A G# ,r2:5i  X#9B   sH                    B&# 95       B   BH#     "<<"\n"; 
cout<<"        9  ,rHh,s#  ;  ,  i S  ;                MM9 #AB#AAM         Ah3        "<<"\n"; 
cout<<"                 i r  .#   , ,&                #rXih&  r;      &.   . .,       "<<"\n"; 
cout<<"         #Sri  5     ;    B                    #r;B:.  HM       M    i.S       "<<"\n"; 
cout<<"          #      ##                            #5 ;.#                          "<<"\n"; 
cout<<"                                                                               "<<"\n"; 
cout<<"###############################################################################"<<"\n";  

}

Double_t __initial_energy;
Int_t __A, __Z;
Int_t __counts;
Double_t __ex_energy;

// Carrega o arquivo de cascata.
void read_cascade_line ( std::istream& ifs){

	TString inStr;
	TString delim = " ";

	if ( ifs.good() ) {
	
		inStr.ReadLine(ifs);
    
		if ( inStr.Length() == 0 ) 
			return;
    
		TObjArray* arr = inStr.Tokenize(delim);

		if ( (( TObjString *)(*arr)[1])->GetString()[0] != '#' ) {

			__initial_energy = (( TObjString *)(*arr)[1])->GetString().Atof();
			__counts = (( TObjString *)(*arr)[2])->GetString().Atoi();
			__ex_energy = (( TObjString *)(*arr)[3])->GetString().Atof();
			__A = (( TObjString *)(*arr)[4])->GetString().Atoi();
			__Z = (( TObjString *)(*arr)[5])->GetString().Atoi();

			arr->Delete();
			delete arr;
		}
	}
}


void MultFrag()
{


	TString nucleus = "Fe56";
	TString energy[4] = {/*"300",*/ "500", "750", "1000", "1500"};
	TString exp_file;
	TString casc_file;

	int i = 0;

#if 0
	
	TString out_file = "results/proton/" + nucleus + "/multi_fragmentacao/" + energy[i] + "MeVp/multifrag_" + nucleus + "_" + energy[i] + "MeVp.txt"

	ofstream out(out_file.Data());

	TStoragef BANK;

	casc_file = "results/proton/" + nucleus + "/" + nucleus + "_" + energy[i] + ".cascade";

	int hist = 1000;	
	int count = 0;
	double mount = 0;

	ifstream casc(casc_file.Data());
	while(casc.good()){

		read_cascade_line(casc);
		if(__ex_energy/__A >= 3.5){

			TPartition *a = new TPartition(__A, __Z, __ex_energy);
	
			while ( count < hist ){
	
				mount = a->GetWeight();
				a->MakeProcess();
				BANK.AddWeight(mount);
				BANK.AddMultiplicity(a->GetMultiplicity(),mount);
				BANK.AddTemperature(a->GetTemperature(),mount);
				BANK.AddEntropy(a->GetEntropy(),mount);
				BANK.AddCv(a->GetCv(),mount);
				BANK.AddPartition(a->GetPartition(),mount);

				count++;
			}

			count = 0;
			delete a;
		}
	}

	BANK.MakeStatistics();

	out << BANK;
#endif

//Gráfico

	TString title = "Multifragmentation " + nucleus + " + " + energy[i] + " MeV p";
	TCanvas *c1 = new TCanvas( "c1", title.Data(), 200, 10, 900, 600 );
	c1->SetBorderMode(0);
	c1->SetFillColor(10);
	
	subpad = new TPad ("subpad","Titulo",.04,.04,.9,.9);
	gStyle->SetOptStat("");
	subpad->Draw();
	subpad->SetBorderMode(0);
	subpad->SetFillColor(10);

	TPaveText pt1(0.4,0.005,0.6,0.04);
	pt1.SetBorderSize(0);
	TText *t1=pt1.AddText("mass number (A)");
	pt1.SetFillColor(10);

	TPaveText pt2(0.005,0.35,0.04,0.65);
	pt2.SetBorderSize(0);
	pt2.SetFillColor(10);
	TText *t2=pt2.AddText("cross section (mb)");
	t2->SetTextAngle(90);
	t2->SetTextSize(0.03);

	TPaveText pt3(0.35,0.92,0.65,0.99);
	pt3.SetFillColor(10);
	TText *t3=pt3.AddText("^{56}Fe + 500 MeV p");
	t3->SetTextSize(0.05);

	TPaveText pt4(0.77,0.92,0.97,0.99);
	pt4.SetFillColor(10);
//	pt4.SetTextAlign(12);
	TText *t4=pt4.AddText("#bullet experimental");
	TText *t4=pt4.AddText("#circ CRISP");

	pt1.Draw();
	pt2.Draw();
	pt3.Draw();
	pt4.Draw();

	const int n = 16;

	TString Z_final[n] = { "25", "24", "23", "22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "12", "11", "10" };
	int Z[n] = { 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10 };

	subpad->Divide(4,4);

	int A=0;
	double Yield = 0., r = 0., Err = 0.;

	TString calc_file = "results/proton/" + nucleus + "/multi_fragmentacao/" + energy[i] + "MeVp/Z%d.txt";
	TString expe_file = "exp_data/multi_fragmentacao/" + nucleus + "/" + energy[i] + "MeVp/Z%d.txt";

	TString expFile;
	TString calcFile;

	TMultiGraph *mg = new TMultiGraph[n];

	int k = 1;
	for(int j=0; j<n; j++){

		expFile = Form(expe_file.Data(), Z[j]);
		calcFile = Form(calc_file.Data(), Z[j]);

		TGraph *exp = new TGraph(expFile.Data(),"%lg %lg");
		exp->SetMarkerStyle(20);
		exp->SetMarkerSize(0.38);		
		TGraph *calc = new TGraph();
		calc->SetMarkerStyle(24);
		calc->SetMarkerSize(0.38);

		ifstream ifs(calcFile.Data());
		while(ifs.good()){

			ifs >> A >> Yield >> Err;
			r = r_0 * pow(A, 1./3.);
			Yield = Yield * 10. * (PI*r*r);
			calc->SetPoint(k,A,Yield);

			//cout << k << "   " << A << "   " << Yield << endl;

			k++;
		}
		k = 1;		

		mg[j].Add(calc, "P");
		mg[j].Add(exp, "P");
		mg[j].SetTitle(Z_final[j].Data());
		
		subpad->cd(j+1);
		subpad->cd(j+1)->SetBorderMode(0);
		subpad->cd(j+1)->SetLogy();

		mg[j].Draw("A");

		gStyle->SetLabelSize(0.085,"X");
		gStyle->SetLabelSize(0.085,"Y");

		gStyle->SetTitleH(0.12);
		gStyle->SetTitleW(0.20);
	
	}
	
	//return 0;
}

