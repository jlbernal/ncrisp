/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#define _WIN32_

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <string>
#include <fstream>  
#include <string>
#include <sstream>
	

#include "TPartition.hh"
#include "TStoragef.hh"
#include "TSMM.hh"

using namespace std;

void Print(){

cout<<"###############################################################################"<<"\n";
cout<<"#                                                                             #"<<"\n";
cout<<"# ### ###  # ### ###   ### ###  ### ###  #   # ### #  # ### ### ### # ### #  ##"<<"\n";
cout<<"# #   # #  # #   # #   #   # #  # # #    ## ## #   ## #  #  # #  #  # # # ## ##"<<"\n";
cout<<"# #   ###  #   # ### # ##  ###  ### # ## # # # ##  ####  #  ###  #  # # # #####"<<"\n";
cout<<"# ### #  # # ### #     #   #  # # # ###  #   # ### #  #  #  # #  #  # ### #  ##"<<"\n";
cout<<"#                                                                             #"<<"\n";
cout<<"###############################################################################"<<"\n";                                                                                
cout<<"                                        BB                                     "<<"\n"; 
cout<<"        #                        M i SBX;r#BS                                  "<<"\n"; 
cout<<"                               33;#, :X;rrG9rH3#                               "<<"\n"; 
cout<<"                              H.; SXX.i&sSsX.,; r         ####B#HHAHAM, #      "<<"\n"; 
cout<<"                             r&h,MrS i ,B;iA;.hH:.M#BAAAHAHHMB#M## #           "<<"\n"; 
cout<<"                            H2H AA,9#2: G, 39S AG5M                            "<<"\n"; 
cout<<"      9 ; #M#M#M#M#M#B#B##M&;ihrMi ssG#9; X2.5 #HH2##M## M#M#M#M#BMB#B#BMHBAH3H"<<"\n"; 
cout<<"       2M B#B#M#######M#M#BH;i&:#9.S3S. rA:;.:A S., ##M###M#M#M#M#B#M#M#M###H&A"<<"\n"; 
cout<<"                            rsr9# M r.Xrr #i; ;ii B                            "<<"\n"; 
cout<<"  #                         r:,;..i2h #r S;;sB r; X#AHABAMHBB###               "<<"\n"; 
cout<<"           #                  BA; :: ,9,, i iX9iXM     # ##M#HMHHAAAHAAAH :    "<<"\n"; 
cout<<"                              # r:# , Xi,h;.;&A ;                        GA    "<<"\n"; 
cout<<"                                S.::Ghi:9 r ,X##                               "<<"\n"; 
cout<<"                                  #M2 A;i A#                                   "<<"\n"; 
cout<<"                                                                               "<<"\n"; 
cout<<"                                                                         ##    "<<"\n"; 
cout<<"                                                 &#                    M::s    "<<"\n"; 
cout<<"             #:,,sHX;.,2AHM                       9B                   #,Xi.#  "<<"\n"; 
cout<<"      Mr.,;s9  rMHX   Xh   s                    G: 9B#                 BB##B   "<<"\n"; 
cout<<"     A.ir  ,;r .. .2r:sA#9B;.2B                 #;S#MHH               AG       "<<"\n"; 
cout<<"     H ;i:   & .  B A3GHs ;;   :                    MAAG             BH        "<<"\n"; 
cout<<"        .  .:#;rh,r..s r#S,M#r  ;SsA                  H&XB          BA         "<<"\n"; 
cout<<"     5 2 i  3HM  X    i s.. S 3 :G ;                    B59   AiSii ;.r#       "<<"\n"; 
cout<<"    B, 2Hi.MA ,Hs:sisr.2. s; #A,&sr                       HGA;,      2AMh3A    "<<"\n"; 
cout<<"  h.  .i i&Gr :r& G#s ;   r  #A H;A                        h  r       ,Xi:5    "<<"\n"; 
cout<<"  i   s2 :: S :i.. si. :S; 5  r;. 5           ,s          &,    5,     ;B      "<<"\n"; 
cout<<"    X,  ri G,GM.,A; ;9r2; #S;#2 ,A#          #,r         #,           .X    X# "<<"\n"; 
cout<<"     :   .A; i  22S# A#A;r h ;, ,                          &         ;X     H  "<<"\n"; 
cout<<"    G  :AS, ,.rX r;#::;  :A .H&2  #                         .       :i         "<<"\n"; 
cout<<"      ;.   ; . ,;  ,     ;& Br5#  M                     BH#25 #AS2.s  &HM      "<<"\n"; 
cout<<"      M .r;:A G# ,r2:5i  X#9B   sH                    B&# 95       B   BH#     "<<"\n"; 
cout<<"        9  ,rHh,s#  ;  ,  i S  ;                MM9 #AB#AAM         Ah3        "<<"\n"; 
cout<<"                 i r  .#   , ,&                #rXih&  r;      &.   . .,       "<<"\n"; 
cout<<"         #Sri  5     ;    B                    #r;B:.  HM       M    i.S       "<<"\n"; 
cout<<"          #      ##                            #5 ;.#                          "<<"\n"; 
cout<<"                                                                               "<<"\n"; 
cout<<"###############################################################################"<<"\n";  

}
/*
void Help ()
{

}
void stop(){
		cout << " Monte Carlo Statistical Multifragmentatiton Module (MCSM)- Crisp Code \n\n";
		cout << " Use -h for details";
		exit(1);
}

double convert(char *I)
{
	double name;
	stringstream Convert;

	Convert << I;
	Convert >> name;

	return name;
}

int convert(char *I)
{
	int name;
	stringstream Convert;

	Convert << I;
	Convert >> name;

	return name;
}

string convert(char *I)
{
	string name;
	stringstream Convert;

	Convert << I;
	Convert >> name;

	return name;
}



*/
int main(int argc, char* argv[])
{

/*	cout.setf(ios::scientific, ios::floatfield);
	cout.adjustfield;

	int A,Z,H,step;
	double E,E_,R;
	string opt,meth,arq;

	opt = convert(argv[1]);

	if (argc == 1) {
		stop();	
	}
	else if ((argc == 7)&&(opt == "-p"))
	{
		Z=convert(argv[2]);
		A=convert(argv[3]);
		E=convert(argv[4]);
		H=convert(argv[5]);
		meth=convert(argv[6]);
		R=convert(argv[7]); 


		TSMM proc(A,Z,E,meth);
		proc.execute(H,R);
		proc.writefile();

	}
	else if ((argc == 6)&&(opt == "-p"))
	{
		Z=convert(argv[2]);
		A=convert(argv[3]);
		E=convert(argv[4]);
		H=convert(argv[5]);
		meth=convert(argv[6]);

		TSMM proc(A,Z,E,meth);
		proc.execute(H);
		proc.writefile();

	}
	else if ((argc == 5)&&(opt == "-p"))
	{
		Z=convert(argv[2]);
		A=convert(argv[3]);
		E=convert(argv[4]);
		H=convert(argv[5]);

		TSMM proc(A,Z,E,meth);
		proc.execute();
		proc.writefile();

	}
	else if ((argc == 5)&&(opt == "-pa"))
	{
		arq		= convert(argv[2]);
		H		= convert(argv[3]);
		meth	= convert(argv[4]);
		R		= convert(argv[5]); 

		TSMM proc(0,0,0,meth);
		proc.execute(H,R,arq);
		proc.writefile();

	}
	else if ((argc == 4)&&(opt == "-pa"))
	{
		arq		= convert(argv[2]);
		H		= convert(argv[3]);
		meth	= convert(argv[4]);

		TSMM proc(0,0,0,meth);
		proc.execute(H,arq);
		proc.writefile();
	}
	else if ((argc == 3)&&(opt == "-pa"))
	{
		arq		= convert(argv[2]);
		H		= convert(argv[3]);

		TSMM proc(0,0,0);
		proc.execute(H,arq);
		proc.writefile();

	}
	else if ((argc == 9)&&(opt == "-s"))
	{
		Z	=convert(argv[2]);
		A	=convert(argv[3]);
		E	=convert(argv[4]);
		E_	=convert(argv[5]);
		H	=convert(argv[6]);
		step=convert(argv[7]);
		meth=convert(argv[8]); 
		R	=convert(argv[9]); 


		TSMM proc(A,Z,E,meth);
		proc.scanningProcess(H, E,E_,step,R);

	}
	else if ((argc == 8)&&(opt == "-s"))
	{
		Z	=convert(argv[2]);
		A	=convert(argv[3]);
		E	=convert(argv[4]);
		E_	=convert(argv[5]);
		H	=convert(argv[6]);
		step=convert(argv[7]);
		meth=convert(argv[8]); 


		TSMM proc(A,Z,E,meth);
		proc.scanningProcess(H, E,E_,step);

	}
	else if ((argc == 7)&&(opt == "-s"))
	{
		Z	=convert(argv[2]);
		A	=convert(argv[3]);
		E	=convert(argv[4]);
		E_	=convert(argv[5]);
		H	=convert(argv[6]);
		step=convert(argv[7]);


		TSMM proc(A,Z,E);
		proc.scanningProcess(H, E,E_,step);

	}
	else {
	
		cout << "Error : option no recognized.  " << "\n";
		cout << " Use -h option for details";
		exit(1);
	
	}

*/

	Print();



	TSMM proc(100,48,1500,"stepsavemax");
	
	proc.scanningProcess(100000, 600,1800, 50, 1.0e-6);

	proc.execute(1000,.00001);


//	proc.writefile();

/*	int hist = 100;	
	int count = 0;
	
	TPartition *a = new TPartition(100,48,300);
	TStoragef bank;

	double mount = 0;
	
	while ( count < hist ){
	
		mount = a->GetWeight();
		a->MakeProcess();
		bank.AddWeight(mount);
		bank.AddMultiplicity(a->GetMultiplicity(),mount);
		bank.AddTemperature(a->GetTemperature(),mount);
		bank.AddEntropy(a->GetEntropy(),mount);
		bank.AddCv(a->GetCv(),mount);
		bank.AddPartition(a->GetPartition(),mount/(double(a->GetMultiplicity()))); //pensar como passar o jogo corretamente

		count++;

		cout << " hist : " << count << "\n";

	}

	bank.MakeStatistics();

	cout << bank;

	delete a;

*/

/*
	double maxweight	= 0;
	double tempweight	= 0;
	int hist			= 10;	
	int count			= 0;
	
	TRandomi rnd;

	bool flag=false;


	TPartition *a = new TPartition(200,88,1500);
	TStorage bank;

	double mount = 0;

	while ( count < hist ){
		
		flag=false;
		a->MakeProcess();

		tempweight = a->GetWeight();

		if (tempweight > maxweight ){
			maxweight = tempweight;
			flag = true;
		}
		else if(rnd.doubleRand()*maxweight*.000001 <= (tempweight)){

			flag = true;
		}
		if (flag == true){



			mount = tempweight;
			cout << "Peso : " << mount << "\n";
			bank.AddWeight(mount);
			bank.AddMultiplicity(a->GetMultiplicity(),mount);
			bank.AddTemperature(a->GetTemperature(),mount);
			bank.AddEntropy(a->GetEntropy(),mount);
			bank.AddCv(a->GetCv(),mount);
			bank.AddPartition(a->GetPartition(),mount/(double(a->GetMultiplicity()))); //pensar como passar o jogo corretamente

			count++;

			cout << " hist : " << count << "\n";

		}

	}

	bank.MakeStatistics();

	cout << bank;

	delete a;


	{
		ofstream fout ("test");
		fout << bank;
		fout.close();
	}
*/

	return 0;
}

