/* ================================================================================
 * 
 * 	Copyright 2011 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "TMath.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TPaletteAxis.h"

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int MassNuc_Dif_neutrons(){ //interpretado
//int main(){				   //compilado

	TCanvas *c = new TCanvas("c","formula de massa",200,100,900,600);
	c->SetFillColor(10);
	c->SetBorderMode(0);
   c->SetGrid();
	//TGraph2D *gr = new TGraph2D();
	//TH2D *h = new TH2D("h","diferenca de massa (modelo - exp)",100,0,118,290,0,293);
	

	//Todas as grandezas estão em MeV								 
	double m_n = 939.565, 
			 m_p = 938.272, 
			 m_e = 0.511, 	 
			 u  = 931.478,  
			 delta = 0.,
			 m = 0.;

	double par[5] = {16.710, 		//a_v
						  18.500, 		//a_s
						   0.750, 		//a_c
						 100.0, 			//a_sim
						  36.};			//par do delta

	int i = 0;
	int Z = 0, A = 0, N = 0;
	double M = 0, ErrM = 0;

   ifstream ifs("exp_data/nuclear_masses/nuclear_mass_exp.txt");
	ofstream ofs("dif_massa_pontos.txt");
	while(ifs.good()){

		ifs >> Z >> A >> M >> ErrM;
		N = A - Z;
		M = M*pow(10.,-6.)*u;
		ErrM = ErrM*pow(10.,-6.)*u;

		if( Z%2 == 0 ){
			if( N%2 == 0 ){

				delta = -par[4]*pow((double)A,-3./4.);
			}
		}
		else{
			if( N%2 != 0 ){

				delta = par[4]*pow((double)A,-3./4.);
			}
		}

		m = Z*m_p + N*m_n + Z*m_e
			 - par[0]*A 												//volume
			 + par[1]*pow(A,2./3.) 									//superfície
			 + par[2]*( (Z*(Z-1.))/pow(A,1./3.) ) 				//coulombiano
			 + par[3]*( ((double)(N-Z)*(N-Z))/(double)A ) 	//simetria
			 + delta;
/*		
		if(Z==82 && A == 208){
			cout<< "volume: " << par[0]*A << "  sup: " << par[1]*pow(A,2./3.) << "  coul: " << par[2]*( (Z*(Z-1.))/pow(A,1./3.) ) << "  sim: " << par[3]*( ((double)(N-Z)*(N-Z))/(double)A ) << "  delta: " << delta << endl;
		}
*/
		if( m-M > 700.){
			ofs << Z << setw(7) << A << setw(13) << A-Z << setw(20) << m-M << endl; 
		}
		
		//h->Fill(Z,A,m-M);
		//gr->SetPoint(i,Z,A,m-M); 
			
		i++;
		delta = 0;
	}
/*
	gr->SetTitle("Diferenca de massa atomica");
	gr->GetZaxis()->SetTitle("dif massa");
	gr->GetXaxis()->SetTitle("Z");
	gr->GetYaxis()->SetTitle("A");
	gr->SetMarkerSize(0.9);
	gr->Draw("surf3z");
*/
	//gStyle->SetPalette(1);
	//gStyle->SetStatX(0.35);
	//gStyle->SetStatY(0.85);

	//h->GetXaxis()->SetTitle("Z");
	//h->GetYaxis()->SetTitle("A");
	//h->Draw("contz");

	//c->Print("Mass_Diference.eps");
}
  
