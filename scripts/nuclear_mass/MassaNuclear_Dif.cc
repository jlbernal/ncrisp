/* ================================================================================
 * 
 * 	Copyright 2011 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//================================================================================================================
//	This code calculates the difference between experimental and model nuclear masses
//================================================================================================================

#include "TMath.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TPaletteAxis.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

double Z_min(int A){ 

	const double pn =   1.008665;
	const double pz =   1.007825;
	const double a1 =  16.710;
	const double a2 =  18.500;
	const double a3 = 100.00;
	const double a4 =   0.750;
	const double u  = 931.478;

	double a = pn*u - a1 + a3 + ( a2 / pow((double)A, 1./3.) ),
			 b = -4.*a3 - (pn - pz)*u - ( a4 / pow((double)A, 1./3.) ),
			 c = ( (4.*a3) / (double)A ) + ( a4 / pow((double)A, 1./3.) );
	
	double Zo = -b/(2.*c),
			 z = 0;

	double z_te = ceil( Zo ),
		    z_pi = floor( Zo );

	if( (z_te - Zo) > (Zo - z_pi) ){
		z = z_pi;
	}
	else if( (z_te - Zo) < (Zo - z_pi) ){
		z = z_te;
	}
	else{
		z = z_te;
	}

	return z;
}

double CalculateMass_Chung(int A, int Z){

	const double pn =   1.008665;
	const double pz =   1.007825;
	const double a1 =  16.710;
	const double a2 =  18.500;
	const double a3 = 100.00;
	const double a4 =   0.750;
	const double f  = 931.478;

	double delta = 0.;
  
	int	z2 = 2 * (int)(ceil( (double)Z / 2.)),
		n2 = 2 * (int)(ceil( (double)(Z - A) / 2.));    

	if( z2 == Z ) {    

		if( n2 == (Z - A) ) {
		
			delta= -36. * pow( (double)A, -3./4. );
		}
	}
	else {     
		if ( n2  != Z - A ) 
			delta = 36. * pow(A, -3.0/4.0);
	}  

	double m = 0.; 
	
	m = pn * (A - Z)*f;

	m = m + pz * Z * f;
	m = m + 0.511 * Z;
	m = m - a1 * A * f/1000.;
	m = m + a2 * pow( (double)A, 2.0/3.0) * f/1000.;
	m = m + a3 * (f/1000.) * pow( (double)A/2. - (double)Z, 2) / (double)A;
	m = m + a4 * pow( (double)Z, 2.) / pow( (double)A, 1./3.);
	m = m + delta * f/1000.;
	
	return m;
}

double mass_correction(int a=0, double c1=0., double c2=0.){

	return c1*a + c2;
}

int MassaNuclear_Dif(){ //interpretado
//int main(){				//compilado

	TCanvas *c = new TCanvas("c","formula de massa",200,100,900,600);
	c->SetFillColor(10);
	c->SetBorderMode(0);
   c->SetGrid();
	//TGraph *gr = new TGraph();
	//TGraph *gr2 = new TGraph();
	//TGraph *gr3 = new TGraph();
	TH2D *h = new TH2D("h","Diferenca de Massa em MeV (exp - modelo PEARSON)",118,0,118,293,0,293);

	//Todas as grandezas estão em MeV								 
	double m_n = 939.565, 
			 m_p = 938.272, 
			 m_e = 0.511, 	 
			 u  = 931.478,  
			 delta = 0.,
			 m = 0., m2 = 0.;


	double par[4] = {-15.65,	   //Av
						   17.63, 		//Asf
						   27.72, 		//Asym
						  -25.60};		//Ass

	//pearson ajustado
//	double par[4] = {      -1.50175e+01  ,
//        						1.55981e+01  ,
//     							  -7.09740e+00 ,
//       							1.44764e+02  };


	int i = 0;
	int Z = 0, A = 0, N = 0;
	double M = 0, ErrM = 0, delt = 0.;

	//parâmetros fixos Pearson
	double r0 = 1.233;
	double e = sqrt(1.44);	//carga elementar em unidades gaussianas
	double Ac = (3. * 1.44)/(5. * r0),
			 mN = 939.566, 
			 mH = 938.272;
	//variáveis úteis Pearson
	double J = 0.,
			 Mpearson = 0.,
			 Mbase = 0.;

   ifstream ifs("exp_data/nuclear_masses/nuclear_mass_exp.txt");
   ofstream ofs("nuclear_mass_dif_original.txt");

	ifs >> Z >> A >> M >> ErrM;

	while(ifs.good()){

		N = A - Z;
		M = M*pow(10.,-6.)*u;
		ErrM = ErrM*pow(10.,-6.)*u;

		J = (double)(N-Z)/(double)A;
		Mpearson =  A* ( par[0]
						+ par[1] * (pow((double)A,-1./3.))
				   	+ Ac * (Z * Z)*(pow((double)A, -4./3.))
						+ ( par[2] + par[3] * pow((double)A,-1./3.) )
						* J*J );
		Mbase = Z * mH + N * mN;
		m = Mpearson + Mbase;// + mass_correction(A, par[4], par[5]);



		//m = CalculateMass_Chung(A, Z);

		h->Fill(Z,A,M-m);
		//gr->SetPoint(i,A,M/A);
		//gr2->SetPoint(i,A,m/A);		
		//gr3->SetPoint(i,A,m2/A);			
	
		ofs << Z << "\t" << A << "\t" << M-m << endl;

		i++;

	   ifs >> Z >> A >> M >> ErrM;

		//delta = 0;
	}

	//h->GetXaxis()->SetTitle("A");
	//h->GetYaxis()->SetTitle("M");

	h->GetXaxis()->SetTitle("Z");
	h->GetYaxis()->SetTitle("A");
	h->GetZaxis()->SetTitle("(MeV)");
	h->GetXaxis()->SetLabelSize(0.03);
	h->GetYaxis()->SetLabelSize(0.03);
	h->GetZaxis()->SetLabelSize(0.03);


//	TMultiGraph *mg = new TMultiGraph();
//	mg->Add(gr, "P");
//	mg->Add(gr2, "L");
//	mg->Add(gr3, "L");

//	gr->SetMarkerStyle(20);
//	gr->SetMarkerSize(1.);
	//gr->Draw("AP");

//	gr2->SetLineColor(2);
//	gr3->SetLineColor(3);

//	mg->GetXaxis()->SetRangeUser(0.,290.);
//	mg->GetYaxis()->SetRangeUser(925.,945.);
//	mg->Draw("a");
	gStyle->SetOptTitle(kFALSE);
	h->Draw("contz");
/*
	TFile f("mass_diference_pearson.root","recreate");
	//gr->Write();
	h->Write();
	f.Close();
*/
	gStyle->SetPalette(1);
	gStyle->SetOptStat(kFALSE);
	//gStyle->SetStatX(0.35);
	//gStyle->SetStatY(0.85);



	//c->Print("Mass_Diference.eps");
}
  
