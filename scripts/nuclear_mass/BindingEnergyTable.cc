/* ================================================================================
 * 
 * 	Copyright 2011 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//================================================================================================================
//	This code calculates the difference between experimental and model nuclear binding energy
//================================================================================================================


#include "TMath.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TPaletteAxis.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

void BindingEnergyTable(){

	TCanvas *c = new TCanvas("c","BindingEnergy",200,100,900,600);
	c->SetFillColor(10);
	c->SetBorderMode(0);
   c->SetGrid();
	TH2D *h = new TH2D("h","BindingEnergy",118,0,118,293,0,293);

	//Todas as grandezas estão em MeV								 
	double m_n = 939.565, 
			 m_p = 938.272, 
			 m_e = 0.511, 	 
			 u  = 931.478;

	double Z, A, B;

   ifstream ifs("data/BindingEnergy/BindingEnergyTableMeV.data");

	while(true){

		ifs >> Z >> A >> B;

		if(!ifs.good()) break;

		h->Fill(Z,A,B);
	}

	h->GetXaxis()->SetTitle("Z");
	h->GetYaxis()->SetTitle("A");
	h->GetZaxis()->SetTitle("(MeV)");
	h->GetXaxis()->SetLabelSize(0.03);
	h->GetYaxis()->SetLabelSize(0.03);
	h->GetZaxis()->SetLabelSize(0.03);


//	TMultiGraph *mg = new TMultiGraph();
//	mg->Add(gr, "P");
//	mg->Add(gr2, "L");
//	mg->Add(gr3, "L");

//	gr->SetMarkerStyle(20);
//	gr->SetMarkerSize(1.);
	//gr->Draw("AP");

//	gr2->SetLineColor(2);
//	gr3->SetLineColor(3);

//	mg->GetXaxis()->SetRangeUser(0.,290.);
//	mg->GetYaxis()->SetRangeUser(925.,945.);
//	mg->Draw("a");
	gStyle->SetOptTitle(kFALSE);
	h->Draw("contz");
/*
	TFile f("mass_diference_pearson.root","recreate");
	//gr->Write();
	h->Write();
	f.Close();
*/
	gStyle->SetPalette(1);
	gStyle->SetOptStat(kFALSE);
	//gStyle->SetStatX(0.35);
	//gStyle->SetStatY(0.85);


	//c->Print("Mass_Diference.eps");
}
  
