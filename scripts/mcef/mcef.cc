/* ================================================================================
 * 
 * 	Copyright 2013, 2015 Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "mcef.hh"

#include "TVectorD.h"

using namespace std;  
  
DataMcefStruct::DataMcefStruct(){
	IsDataOk = false;
	McefNumber = 0;
}
void DataMcefStruct::ReadData(fstream &file){
	while (true) {      
		if (!file.good ()) break;  
		string pdline, Stemp;
		//double Dtemp;
		int Itemp;
		bool Btemp;
		getline(file, pdline);
		char c = pdline.c_str()[0];
		if (c == '$'){
			string text;     
			istringstream istr(pdline.c_str() );      
			istr >> text;
			if ( text == "$NucleusName" ) {	
				istr >> Stemp;   
				nucleusName.push_back(Stemp);      
			}
			if ( text == "$MassIndex" ) {	
				istr >> Itemp; 
				A.push_back(Itemp);      
			}
			if ( text == "$NucleusCharge" ) {	
				istr >> Itemp;   
				Z.push_back(Itemp);      
			}
			if ( text == "$CascadeType" ) {	
				istr >> Stemp;   
				generator.push_back(Stemp);      
			}
			if ( text == "$InitialEnergy" ) {	
				istr >> Itemp;   
				energy.push_back(Itemp);      
			}
			if ( text == "$StepEnergy" ) {	
				istr >> Itemp;   
				step_energy.push_back(Itemp);      
			}
			if ( text == "$EnergiesNumber" ) {	
				istr >> Itemp;   
				number_of_energies.push_back(Itemp);      
			}
			if ( text == "$SimulationCases" ) {	
				istr >> Stemp;   
				nevents.push_back(Stemp);      
			}
			if ( text == "$NumberOfRuns" ) {
				istr >> Itemp;
				runs.push_back(Itemp);
			}
			if ( text == "$WeissWidth" ) {	
				istr >> Stemp;
				if (Stemp == "true") Btemp = true;
				if (Stemp == "false") Btemp = false;
				WeissWidth.push_back(Btemp);      
			}
			if ( text == "$WeissEnergyDist" ) {	
				istr >> Stemp;
				if (Stemp == "true") Btemp = true;
				if (Stemp == "false") Btemp = false;
				WeissEnergyDist.push_back(Btemp);      
			}
			if ( text == "$ModelBarrier" ) {	
				istr >> Stemp;
				modelBarrier.push_back(Stemp);      
			}
			if ( text == "$ModelLevelDens" ) {	
				istr >> Stemp;
				modelLevelDens.push_back(Stemp);      
			}

		}
	}
	if( (nucleusName.size() == generator.size()) && (nucleusName.size() == A.size()) && (nucleusName.size() == Z.size()) && (nucleusName.size() == nevents.size()) && (nucleusName.size() == number_of_energies.size()) && (nucleusName.size() == energy.size()) && (nucleusName.size() == step_energy.size()) && (nucleusName.size() == WeissEnergyDist.size()) && (nucleusName.size() == WeissWidth.size()) && (nucleusName.size() == runs.size()) && (nucleusName.size() == modelBarrier.size()) && (nucleusName.size() == modelLevelDens.size())){
		IsDataOk = true;
		McefNumber = nucleusName.size();
	}
}

void execute_mcef(int a, int z, int runs, TString generator, TString fname_in, TString fname_out, bool WidthOption, bool WeissOption, TString modelBarrier, TString modelLevelDens){
  
	Int_t fissions = 0, 
	      N = 0, 
	      tot_counts = 0,
	      A = 0,
	      Z = 0,
	      Counts = 0;
	Double_t ExEnergy = 0.;
	Double_t Nuc_px = 0, Nuc_py = 0, Nuc_pz=0; 
	Double_t Nuc_lx0 = 0, Nuc_ly0 = 0, Nuc_lz0=0;
	Double_t Nuc_lx = 0, Nuc_ly = 0, Nuc_lz=0;
	Double_t n_mass = CPT::n_mass;//939.565;
	Double_t p_mass = CPT::p_mass; //938.272;
	Double_t a_mass = 3727.378;
 	Double_t NucA_index = 0.;	
	
	TFile *F = new TFile(fname_in.Data());
	TTree *t = (TTree*)F->Get("History");
	int n = t->GetEntries();
	t->SetBranchAddress("finalNuc_A",&A);
	t->SetBranchAddress("finalNuc_Z",&Z);
	t->SetBranchAddress("finalNuc_Ex",&ExEnergy);
	t->SetBranchAddress("initialAttempts",&Counts);
	t->SetBranchAddress("finalNuc_Px",&Nuc_px);
	t->SetBranchAddress("finalNuc_Py",&Nuc_py);
	t->SetBranchAddress("finalNuc_Pz",&Nuc_pz);
	t->SetBranchAddress("finalNuc_Lx",&Nuc_lx0);
	t->SetBranchAddress("finalNuc_Ly",&Nuc_ly0);
	t->SetBranchAddress("finalNuc_Lz",&Nuc_lz0);
	
	TVector3 Nuc_Momentum(0.,0.,0.);
	std::vector<Double_t> Nenergies;
	std::vector<Double_t> Penergies;
	std::vector<Double_t> Aenergies;
	unsigned int jlk_counter_n=0;
	unsigned int jlk_counter_p=0;	
	unsigned int jlk_counter_a=0;	

	Fission fiss;
	Spallation spall;
	Evap_part ejected; // mio

	const int maxZ = 120, maxA = 250;
	Double_t frequencies[maxZ][maxA];
	for( int _z = 0; _z < maxZ; _z++){
		for( int _a = 0; _a < maxA; _a++){
			frequencies[_z][_a] = 0.;
		}
	}
	Double_t A_freq[maxA];
	for(int _a = 0; _a < maxA; _a++){
	  A_freq[_a] = 0.;
	}
	
	Int_t final_a;
	Int_t final_z; //final configuration
	Int_t neutron;
	Int_t proton;
	Int_t alpha; //multiplicities
	Double_t W; //fissility
	Double_t final_E; //final energy
	TVector3 ejected_position(0,0,0);
	TVector3 ejected_p(0,0,0);
	TVector3 ejected_l(0,0,0);
	Double_t lx,ly,lz;
	
	TFile file(fname_out.Data(), "recreate");
	TTree *HistFission = new TTree("history Fission", "MCEF Tree fiss");
	HistFission->Branch("Fission",&fiss.fiss_A,"fiss_A/I:fiss_Z:fiss_E/D:fissility:fiss_neutron/I:fiss_proton:fiss_alpha");
	HistFission->Branch("Fiss_lx",&Nuc_lx,"Nuc_lx/D");
	HistFission->Branch("Fiss_ly",&Nuc_ly,"Nuc_ly/D");
	HistFission->Branch("Fiss_lz",&Nuc_lz,"Nuc_lz/D");
	
	TTree *HistSpall = new TTree("history Spallation", "MCEF Tree spall");
	HistSpall->Branch("Spallation",&spall.spall_A,"spall_A/I:spall_Z:spall_E/D:spall_proton/I:spall_alpha");
	HistSpall->Branch("Spall_lx",&Nuc_lx,"Nuc_lx/D");
	HistSpall->Branch("Spall_ly",&Nuc_ly,"Nuc_ly/D");
	HistSpall->Branch("Spall_lz",&Nuc_lz,"Nuc_lz/D");	

	TTree *neject = new TTree("neutronejected", "MCEF neutron Tree"); // n
	neject->Branch("Neutrons",&ejected.p_energy,"p_energy/D:dir_PX/D:dir_PY/D:dir_PZ/D");
	neject->Branch("N_lx",&lx,"lx/D");
	neject->Branch("N_ly",&ly,"ly/D");
	neject->Branch("N_lz",&lz,"lz/D");
	TTree *peject = new TTree("protonejected", "MCEF protons Tree"); //  p 
	peject->Branch("Protons",&ejected.p_energy,"p_energy/D:dir_PX/D:dir_PY/D:dir_PZ/D");
	peject->Branch("P_lx",&lx,"lx/D");
	peject->Branch("P_ly",&ly,"ly/D");
	peject->Branch("P_lz",&lz,"lz/D");	
	TTree *aeject = new TTree("alphaejected", "MCEF alphas Tree"); //  a 
	aeject->Branch("Alphas",&ejected.p_energy,"p_energy/D:dir_PX/D:dir_PY/D:dir_PZ/D");
	aeject->Branch("A_lx",&lx,"lx/D");
	aeject->Branch("A_ly",&ly,"ly/D");
	aeject->Branch("A_lz",&lz,"lz/D");

	TTree *HistMult = new TTree("history Multi", "MCEF Tree Multi");
	HistMult->Branch("neuMult",&neutron,"neuM/I");
	HistMult->Branch("proMult",&proton,"proM/I");
	HistMult->Branch("alpMult",&alpha,"alpM/I");
	
	Mcef *mcef = new Mcef(modelLevelDens, WidthOption, WeissOption, modelBarrier);		
	std::cout << std::endl << "Running MCEF from " << fname_in.Data() << std::endl;
	for(int i=0; i<n; i++){
		t->GetEntry(i);
				
		if(i%100==0 )
			cout << i << endl;
		if ( ExEnergy > 0. ){
			TVector3 nuc_l=TVector3(Nuc_lx0,Nuc_ly0,Nuc_lz0);	
			Nuc_Momentum.SetXYZ(Nuc_px,Nuc_py,Nuc_pz);
	
			for(int cont = 0; cont < runs; cont++){
					
				Double_t NucA_index = (Double_t) A;	
				N++;
				tot_counts += Counts;// how many projectiles were launched against the target.
				mcef->Generate( A, Z, double(ExEnergy), WidthOption, WeissOption);
				mcef->FinalConfiguration(A, Z, final_a, final_z);
				final_E = mcef->final_E();
				neutron = mcef->NeutronMultiplicity();
				proton = mcef->ProtonMultiplicity();
				alpha = mcef->AlphaMultiplicity();
				W = mcef->Fissility();
				Nenergies=mcef->Nvect(); 
				Penergies=mcef->Pvect(); 
				Aenergies=mcef->Avect(); 

				/// energy analysis	
				TVector3 mv_aux;
				Double_t mass_frac = 1/NucA_index;

				unsigned int jkl; 
				for( jkl=jlk_counter_n; jkl < Nenergies.size(); jkl++ ){ //neutrons
				        ejected_position=xgen(A);
					mv_aux = TMath::Sqrt(2.*n_mass* Nenergies.at(jkl))*sgen();
					ejected.p_energy =  Nenergies.at(jkl);
					ejected.dir_PX = mv_aux.x()+mass_frac*Nuc_Momentum.x(); 
					ejected.dir_PY = mv_aux.y()+mass_frac*Nuc_Momentum.y();
					ejected.dir_PZ = mv_aux.z()+mass_frac*Nuc_Momentum.z();
					ejected_p=TVector3(ejected.dir_PX,ejected.dir_PY,ejected.dir_PZ );
					ejected_l=ejected_position.Cross(ejected_p);
					nuc_l=nuc_l-ejected_l;
					lx=(ejected_l.X())*0.005; 
					ly=ejected_l.Y()*0.005;
					lz=ejected_l.Z()*0.005;
					
					neject->Fill();
					jlk_counter_n++;	
				}
			
				for( jkl=jlk_counter_p; jkl < Penergies.size(); jkl++ ){ // protons
				        ejected_position=xgen(A);
					mv_aux = TMath::Sqrt(2.*p_mass* Penergies.at(jkl))*sgen();
					ejected.p_energy =  Penergies.at(jkl);
					ejected.dir_PX = mv_aux.x()+mass_frac*Nuc_Momentum.x(); 
					ejected.dir_PY = mv_aux.y()+mass_frac*Nuc_Momentum.y();
					ejected.dir_PZ = mv_aux.z()+mass_frac*Nuc_Momentum.z(); 
					ejected_p=TVector3(ejected.dir_PX,ejected.dir_PY,ejected.dir_PZ );
					ejected_l=ejected_position.Cross(ejected_p);
					nuc_l=nuc_l-ejected_l;
					lx=ejected_l.X()*0.005; 
					ly=ejected_l.Y()*0.005;
					lz=ejected_l.Z()*0.005;					
					peject->Fill();
					jlk_counter_p++;
				}
				
				for( jkl=jlk_counter_a; jkl < Aenergies.size(); jkl++ ){ // alfas
				        ejected_position=xgen(A);
					mv_aux = TMath::Sqrt(2.*a_mass* Aenergies.at(jkl))*sgen();
					ejected.p_energy =  Aenergies.at(jkl);
					ejected.dir_PX = mv_aux.x()+4*mass_frac*Nuc_Momentum.x(); 
					ejected.dir_PY = mv_aux.y()+4*mass_frac*Nuc_Momentum.y();
					ejected.dir_PZ = mv_aux.z()+4*mass_frac*Nuc_Momentum.z(); 
					ejected_p=TVector3(ejected.dir_PX,ejected.dir_PY,ejected.dir_PZ );
					ejected_l=ejected_position.Cross(ejected_p);
					nuc_l=nuc_l-ejected_l;
					lx=ejected_l.X()*0.005; 
					ly=ejected_l.Y()*0.005;
					lz=ejected_l.Z()*0.005;					
					aeject->Fill();
					jlk_counter_a++;
				}
				
				if( mcef->NumberFissions() > 0 ){
					fiss.fiss_A = final_a;
					fiss.fiss_Z = final_z;
					fiss.fiss_E = final_E;
					fiss.fissility = W;
					fiss.fiss_neutron = neutron;
					fiss.fiss_proton = proton;
					fiss.fiss_alpha = alpha;
					Nuc_lx=nuc_l.X()*0.005; 
					Nuc_ly=nuc_l.Y()*0.005;
					Nuc_lz=nuc_l.Z()*0.005;
					fissions++;
					HistFission->Fill();
				 }
				 else if( mcef->NumberFissions() == 0 ){
					spall.spall_A = final_a;
					spall.spall_Z = final_z;
					spall.spall_E = final_E;
					spall.spall_neutron = neutron;
					spall.spall_proton = proton;
					spall.spall_alpha = alpha;
					Nuc_lx=nuc_l.X()*0.005;
					Nuc_ly=nuc_l.Y()*0.005;
					Nuc_lz=nuc_l.Z()*0.005;					
					frequencies[final_z][final_a] += 1;
					A_freq[final_a] += 1;
					HistSpall->Fill();
				 }
				 
				 HistMult->Fill();
			}
		}
	}
	HistFission->Write();
	HistSpall->Write();	
	HistMult->Write();
	
	if(WeissOption){
	  	TH1D pK = mcef->ProtonKDist();
		pK.Write();
		TH1D nK = mcef->NeutronKDist();
		nK.Write();
		TH1D aK = mcef->AlphaKDist();
		aK.Write();
		
		neject->Write();
		peject->Write();
		aeject->Write();	
	}
	if(WidthOption){
	  	TGraph pEW = mcef->GammaPDist();
		pEW.Write("Pp");
		TGraph aEW = mcef->GammaADist();
		aEW.Write("Pa");
		TGraph fW = mcef->GammaFDist();
		fW.Write("Pf");
	}
	
	TH1D fP = mcef->fissParamDist();
	fP.Write("fissParam");
	
	/** NOTE: Up to now (December 2015), CRISP cannot make photoabsorption below 40 MeV.
	 That being said, Bremsstrahlung is include here just for completeness of the program.
	 The total fission cross section stored in the TVectorD is necessarily wrong due to the fact
	 that the variable tot_counts DOES NOT represent the reality of the photoabsorption.
	 Below 40 MeV, the primary interaction is not performed and the initial energy is assumed to be
	 the excitation energy of the target.
	**/
	Double_t sf = 0.;
	Double_t rnt = 1.18*pow(double(a),1./3.);
	if(generator.CompareTo("photon") == 0){
		sf = (TMath::Pi() * sqr(rnt) * 10000.)/double(a); // fm^2 -> microbarn
	}
	else if(generator.CompareTo("bremss") == 0 || generator.CompareTo("ultra") == 0){
		sf = TMath::Pi() * sqr(rnt) * 10000.; // fm^2 -> microbarn
	}
	else if(generator.CompareTo("proton") == 0){
		sf = TMath::Pi() * sqr(rnt+1.2) * 10.; // fm^2 -> milibarn
	}
	else if(generator.CompareTo("deuteron") == 0){
		sf = TMath::Pi() * sqr(rnt+2.36) * 10.; // fm^2 -> milibarn
	}
	else{
		sf = (TMath::Pi() * sqr(rnt) * 10000.); // fm^2 -> microbarn
	}
	
	TVectorD v(1);
	v[0] = ( double(fissions) / tot_counts ) * sf;
	v.Write("FissionCrossSection");
	
	TGraph *g = NULL;
	  
	TVectorD x(1);
	for(int _a = 0; _a < maxA; _a++){
		x[0] = x[0] + ( A_freq[_a] / (double)tot_counts ) * sf; 
	}
	x.Write("SpallationCrossSection");
		
	vector<int> vZ(maxZ);
	int cZ = 0;
	for(int _z = 0;_z < maxZ; _z++){
		 for(int _a = 0; _a < maxA; _a++){
			frequencies[_z][_a] = (frequencies[_z][_a] / (double)tot_counts);
			if(frequencies[_z][_a] != 0){
			  vZ[_z] += 1;	//how many A's for each Z
			}
		 }
	}
	for(int j=0; j<maxZ; j++){
	  if(vZ[j] != 0) cZ++;	//how many final Z's
	}
	//cout << "cZ: " << cZ << endl;
		
	g = new TGraph[cZ];
	NameTable nameTable; //table of elements containing name according to charge
	string title;
	int j = 0;
	int k = 0;
	for(int _z = 0; _z < maxZ; _z++){
		 if(vZ[_z] != 0){
			  int npoints = vZ[_z];
			  g[j].Set(npoints);
			  title = Form(("%d" + nameTable.get(_z)).data(), _z);
			  g[j].SetName(title.data());
			  g[j].SetMarkerStyle(20);
			  //cout << "title: " << title.data() << "  npoints: " << g[j].GetN() << "  j: " << j << endl; 
			  for( int _a = 0; _a < maxA; _a++){
					if(frequencies[_z][_a] != 0){
 						 g[j].GetY()[k] = frequencies[_z][_a]*sf;
 						 g[j].GetX()[k] = double(_a);
						 k++;
					} 
			  }
			  g[j].GetXaxis()->SetTitle("A");
			  if(generator.CompareTo("photon") == 0 || generator.CompareTo("bremss") == 0 || generator.CompareTo("ultra") == 0 || generator.CompareTo("hyperon") == 0){
				  g[j].GetYaxis()->SetTitle("#sigma(A) [#mub]");
			  }
			  else if(generator.CompareTo("proton") == 0 || generator.CompareTo("deuteron") == 0){
				  g[j].GetYaxis()->SetTitle("#sigma(A) [mb]");
			  }
			  else{
				  g[j].GetYaxis()->SetTitle("#sigma(A) [#mub]");
			  }
			  g[j].GetYaxis()->SetTitleOffset(1.35);
			  g[j].Write();
			  k = 0;
			  j++;
		}
	}
	
	file.Close();
	delete mcef;
}

TVector3 sgen(Double_t P_E){

 	TVector3 vec_P(0.,0.,0.);
 	TRandom3 rj(0);
	Double_t a,b;
	
	a=360.*rj.Uniform(0., 1.);
	b=180.*rj.Uniform(0., 1.)-90;	

	vec_P.SetXYZ(TMath::Cos(a)*TMath::Cos(b),TMath::Sin(a)*TMath::Cos(b),TMath::Sin(b));
	return vec_P;

}
TVector3 xgen(Double_t A){
  double rnt = CPT::r0 * TMath::Power((Double_t)A, 1./3.);
  double phi = TMath::TwoPi() * gRandom->Uniform();
  double cos_a = 1.0 - 2.0 * gRandom->Uniform();
  double sin_a = TMath::Sqrt(1.0 - TMath::Power(cos_a,2));
  double ri = rnt * TMath::Power(gRandom->Uniform(),1./3.);
  double r_xy = ri * sin_a;       
  TVector3 result ( r_xy * TMath::Cos(phi), r_xy * TMath::Sin(phi), ri  * cos_a );
  return result;
}

