/* ================================================================================
 * 
 * 	Copyright 2013, 2015 Evandro Andrade Segundo, José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include <fstream>
#include <vector>

#include "TString.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TVector3.h"

#include "Mcef.hh"
#include "DataHelper.hh"
#include "NameTable.hh"

using namespace std;  
  
struct DataMcefStruct{
	std::vector<TString> nucleusName, generator, nevents, modelBarrier, modelLevelDens;
	std::vector<Int_t> A, Z, number_of_energies, runs, energy, step_energy;
	std::vector<Bool_t> WeissEnergyDist;
	std::vector<Bool_t> WeissWidth;
	DataMcefStruct();
	Bool_t IsDataOk;
	Int_t McefNumber;
	void ReadData(fstream &file);
};  

void execute_mcef(int a, int z, int runs, TString generator, TString fname_in, TString fname_out, bool WidthOption = false, bool WeissOption = false, TString modelBarrier = "nix", TString modelLevelDens = "dostrovsky");

TVector3 sgen(Double_t P_E = 1.0); // generate a 3D vector isotropicaly in sphere radio 1.
TVector3 xgen(Double_t A);