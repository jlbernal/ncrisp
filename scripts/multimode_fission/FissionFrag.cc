/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "FissionFrag.hh"

DataMultimodalFissionStruct::DataMultimodalFissionStruct(){
	IsDataOk = false;
	MultFissNumber = 0;
}

void DataMultimodalFissionStruct::ReadData(fstream &file){
	while (true) {      
		if (!file.good ()) break;  
		string pdline, Stemp;
		double Dtemp;
		int i, Itemp;
		getline(file, pdline);
		char c = pdline.c_str()[0];
		if (c == '$'){
			string text;     
			istringstream istr(pdline.c_str() );      
			istr >> text;
			if ( text == "$NucleusName" ) {	
				istr >> Stemp;   
				nucleusName.push_back(Stemp);      
			}
			if ( text == "$MassIndex" ) {	
				istr >> Itemp; 
				A.push_back(Itemp);      
			}
			if ( text == "$NucleusCharge" ) {	
				istr >> Itemp;   
				Z.push_back(Itemp);      
			}
			if ( text == "$Generator" ) {	
				istr >> Stemp;   
				generator.push_back(Stemp);      
			}
			if ( text == "$Energy" ) {	
				istr >> Stemp;   
				energy.push_back(Stemp);      
			}
			if ( text == "$SIParameters" ) {
				i = 0;
				Vect_SI = new Double_t[3];
				Vect_SI[0] = 0.;
				Vect_SI[1] = 1.;
				Vect_SI[2] = 0.;
				while (istr >> Dtemp){
					Vect_SI[i] = Dtemp;
					i++;
				}
				SIParams.push_back(Vect_SI);
			}
			if ( text == "$SIIParameters" ) {
				i = 0;
				Vect_SII = new Double_t[3];
				Vect_SII[0] = 0.;
				Vect_SII[1] = 1.;
				Vect_SII[2] = 0.;
				while (istr >> Dtemp){
					Vect_SII[i] = Dtemp;
					i++;
				}
				SIIParams.push_back(Vect_SII);
			}
			if ( text == "$SIIIParameters" ) {
				i = 0;
				Vect_SIII = new Double_t[3];
				Vect_SIII[0] = 0.;
				Vect_SIII[1] = 1.;
				Vect_SIII[2] = 0.;
				while (istr >> Dtemp){
					Vect_SIII[i] = Dtemp;
					i++;
				}
				SIIIParams.push_back(Vect_SIII);
			}
			if ( text == "$SLParameters" ) {
				i = 0;
				Vect_SL = new Double_t[2];
				Vect_SL[0] = 0.;
				Vect_SL[1] = 1.;
				while (istr >> Dtemp){
					Vect_SL[i] = Dtemp;
					i++;
				}
				SLParams.push_back(Vect_SL);
			}
			if ( text == "$mu1" ) {	
				istr >> Dtemp;   
				mu1.push_back(Dtemp);      
			}
			if ( text == "$mu2" ) {	
				istr >> Dtemp;   
				mu2.push_back(Dtemp);      
			}
			if ( text == "$gamma1" ) {	
				istr >> Dtemp;   
				gamma1.push_back(Dtemp);      
			}
			if ( text == "$gamma2" ) {	
				istr >> Dtemp;   
				gamma2.push_back(Dtemp);      
			}
			if ( text == "$norm" ) {	
				istr >> Dtemp;   
				norm.push_back(Dtemp);      
			}
		}
	}
	if( (nucleusName.size() == generator.size()) && (nucleusName.size() == A.size()) && (nucleusName.size() == Z.size()) && (nucleusName.size() == energy.size()) && (nucleusName.size() == SIParams.size()) && (nucleusName.size() == SIIParams.size()) && (nucleusName.size() == SIIIParams.size()) && (nucleusName.size() == SLParams.size()) && (nucleusName.size() == mu1.size()) && (nucleusName.size() == mu2.size()) && (nucleusName.size() == gamma1.size()) && (nucleusName.size() == gamma2.size()) && (nucleusName.size() == norm.size())){
		IsDataOk = true;
		MultFissNumber = nucleusName.size();
	}
}


double CalculateMass_Pearson_Original(int A, int Z)
{
//	Pearson 2001
	double Av = -15.65, Asf = 17.63, Asym = 27.72, Ass = -25.60;
	double mN = 939.566, mH = 938.272;
	int N = A - Z;
	double r0 = 1.233;	//fm
	double e2 = 1.44;	//carga elementar em unidades gaussianas
	double  Ac = (3. * e2)/(5. * r0);
		
	double J = double(N-Z)/(double)A;
	double Mpearson =  A* ( Av
				+ Asf * (pow((double)A,-1./3.))
				+ Ac * (Z * Z)*(pow((double)A, -4./3.))
				+ ( Asym + Ass * pow((double)A,-1./3.) )* J*J );
	double Mbase = Z * mH + N * mN;
	double r = Mpearson + Mbase;
	
	return r;
}

void CalculateT_V(int A, int Z, int A1, int Z1, double E_ex, double &v1, double &v2, double &T1, double &T2){
		
	int A2 = A-A1;
	int Z2 = Z-Z1;
	// useful variables
        double m1 = 0., m2 = 0., M = 0. , T = 0., m12 = 0, v1t = 0., v2t = 0.;		

	M = CalculateMass_Pearson_Original(A,Z);	
	m1 = CalculateMass_Pearson_Original(A1,Z1);
	m2 = CalculateMass_Pearson_Original(A2,Z2);
		
	T =  E_ex + M - m1 - m2; // Kinetic energy of both fragments
	m12  = m1*m2;
	
	v1t = 2 * T * m1 / (m2*m2 + m12);
	v2t = 2 * T * m2 / (m1*m1 + m12);
	
	v1 = TMath::Sqrt(v1t) * 30.; //cm/ns
	v2 = TMath::Sqrt(v2t) * 30.; //cm/ns

	T1 = 0.5 * m1 * v1t;
	T2 = 0.5 * m2 * v2t;
}

void outOfMemory()
{
	std::cout << "======================================================================\n";
	std::cout << " UNABLE TO SARISFY REQUEST FOR MEMORY!\n\n";
	std::cout << " Notice that a large amount of fissioning nucleus will require\n";
	std::cout << " an even larger space in memory for the fragments.\n";
	std::cout << " Try to maintain the number of fissioning nucleus not higher than a\n";
	std::cout << " few hundred thousand and keep in mind the high fissility of\n";
	std::cout << " the nucleus you are working with.\n";
	std::cout << "======================================================================\n\n";

	std::abort();
}


void execute_fragments(MultimodalFission *mf){
	
	 //set the new_handler
	 std::set_new_handler(outOfMemory);
  
	 int rank(0);
	 int size(1);
  
#ifdef CRISP_MPI
  	 MPI_Comm_rank( MPI_COMM_WORLD, &rank );		// rank of the processes
	 MPI_Comm_size( MPI_COMM_WORLD, &size );		// number of processes
#endif
	 
	 if(rank == 0) cout << endl << mf->GetNucleus() << " - " << mf->GetGenerator() << " - " << mf->GetEnergy() << " MeV" << endl << endl;

    //======== Lendo arquivo das massas nucleares e gravando dados em vetores
	 if(rank == 0)  cout << endl << "Saving data in vectors..." << endl << endl;

	 Double_t A_simm = 0.;
	 Double_t Z_simm = 0.;
	 Int_t A_fissao = 0.;
	 Int_t Z_fissao = 0.;
	 Double_t E_fissao = 0.;
	 vector <Double_t> vectA_simm;
	 vector <Double_t> vectZ_simm;
	 vector <Double_t> vectA_fissao;
	 vector <Double_t> vectZ_fissao;
	 vector <Double_t> vectE_fissao;

	 double Amean = 0.;
	 double Zmean = 0.;

	 TString fname_in = "results/mcef/" + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_mcef.root";

	 TFile *F = new TFile(fname_in.Data());
	 TTree *t = (TTree*)F->Get("history Fission");
	 TBranch *Fission = (TBranch*)t->GetBranch("Fission");
	 int n = Fission->GetEntries();
	 TLeaf *l1 = (TLeaf*)Fission->GetLeaf("fiss_A");
	 l1->SetAddress(&A_fissao);
	 TLeaf *l2 = (TLeaf*)Fission->GetLeaf("fiss_Z");
	 l2->SetAddress(&Z_fissao);
	 TLeaf *l3 = (TLeaf*)Fission->GetLeaf("fiss_E");
	 l3->SetAddress(&E_fissao);
	 
	 for(int i=0; i<n; i++){
	   
		  Fission->GetEntry(i);
		  A_simm = A_fissao / 2.;
		  Amean += A_simm;
		  Z_simm = Z_fissao / 2.;
		  Zmean += Z_simm;

		  vectA_simm.push_back(A_simm);
		  vectZ_simm.push_back(Z_simm);
		  vectA_fissao.push_back(double(A_fissao));
		  vectZ_fissao.push_back(double(Z_fissao));
		  vectE_fissao.push_back(E_fissao);
	 }

	 Amean = Amean / vectA_fissao.size();
	 Zmean = Zmean / vectA_fissao.size();

// 	 double Amin = Amean - 6.*(mf->Getsigma_s());
// 	 if(Amin<0.) Amin = 0.;
// 
// 	 double Amax = Amean + 6.*(mf->Getsigma_s());

	 // --- MPI ---------------------------------------------------------------

	 if( gRandom ){
		  delete gRandom;
		  gRandom = new TRandom3( primos[rank] );
	 }

	 int masterNRuns = (int)( vectA_fissao.size() % size);
	 int hostsNRuns = (int)( vectA_fissao.size() / size);

	 int start = hostsNRuns * rank;
	 int end = hostsNRuns * (rank+1);
	 
	 if( rank == 0 ) cout << ": hostsNRuns: " << hostsNRuns << " x " << size << " + masterExtraRuns: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n\n";


	 double *mpi_vectA = new double[vectA_fissao.size() * 14];
	 double *mpi_vectZ = new double[vectA_fissao.size() * 14];
	 double *mpi_vectE = new double[vectA_fissao.size() * 14];
	 double *mpi_vectT = new double[vectA_fissao.size() * 14];
	 double *mpi_vectV = new double[vectA_fissao.size() * 14];
	 int mpi_vect_size = 0;

	 double *finalVectorA = NULL;
	 double *finalVectorZ = NULL;
	 double *finalVectorE = NULL;
	 double *finalVectorV = NULL;
	 double *finalVectorT = NULL;

	 double *extraVectorA = NULL;
	 double *extraVectorZ = NULL;
	 double *extraVectorE = NULL;
	 double *extraVectorT = NULL;
	 double *extraVectorV = NULL;
	 int extra_vect_size = 0;


//======== sorteio dos fragmentos

	 if(rank == 0) cout << endl << "Calculating the fragments..." << endl << endl;

	 int i = 0, l = 0;

	 double a_frag = 0., z_frag = 0., e_frag1 = 0., e_frag2 = 0., v_frag1 = 0., v_frag2 = 0., T_frag1 = 0., T_frag2 = 0.;
			  
	 //correção na energia de excitação dos fragmentos para núcleos leves
	 double deformFactor = 1.; //100% of the energy go to the fragments
	 //os quais gastam mais energia na deformação nuclear precedente à fissão
	
	 try{
		for( i = start; i < end; i++ ){
			
			a_frag = mf->CrossSectionA(0., vectA_simm[i], vectA_fissao[i], l);
			z_frag = mf->CrossSectionZ(a_frag);
			e_frag1 = (a_frag/vectA_fissao[i]) * (deformFactor * vectE_fissao[i]);
			e_frag2 = ((vectA_fissao[i]-a_frag)/vectA_fissao[i]) * (deformFactor * vectE_fissao[i]);
			
			CalculateT_V(vectA_fissao[i], vectZ_fissao[i], a_frag, z_frag, vectE_fissao[i]*(1.-deformFactor), v_frag1, v_frag2,  T_frag1, T_frag2);
			
			for(int k=0; k<l; k++){
				mpi_vectA[mpi_vect_size] = a_frag;
				mpi_vectZ[mpi_vect_size] = z_frag;
				mpi_vectE[mpi_vect_size] = e_frag1;
				mpi_vectT[mpi_vect_size] = T_frag1;
				mpi_vectV[mpi_vect_size] = v_frag1;
				mpi_vectA[mpi_vect_size+1] = vectA_fissao[i] - a_frag;
				mpi_vectZ[mpi_vect_size+1] = vectZ_fissao[i] - z_frag;
				mpi_vectE[mpi_vect_size+1] = e_frag2;
				mpi_vectT[mpi_vect_size+1] = T_frag2;
				mpi_vectV[mpi_vect_size+1] = v_frag2;
				mpi_vect_size += 2;
			}

			l = 0;
		}
	}
	catch(int &contTent){
		cout << "Nao foi possivel sortear um fragmento apos " << contTent << " tentativas\n\n";
		i--;
	}

	int final_size = mpi_vect_size;
#ifdef CRISP_MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Reduce( &mpi_vect_size, &final_size, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD );

	if(rank == 0){
		finalVectorA = new double[final_size];
		finalVectorZ = new double[final_size];
		finalVectorE = new double[final_size];
		finalVectorT = new double[final_size];
		finalVectorV = new double[final_size];

		if( masterNRuns > 0 ){
			extraVectorA = new double[masterNRuns*14];
			extraVectorZ = new double[masterNRuns*14];
			extraVectorE = new double[masterNRuns*14];
			extraVectorT = new double[masterNRuns*14];
			extraVectorV = new double[masterNRuns*14];
		}
	}
	
	int *rcounts = new int[size];
	int *displs = new int[size];

	MPI_Gather( &mpi_vect_size, 1, MPI_INT, rcounts, 1, MPI_INT, 0, MPI_COMM_WORLD);	

	if(rank == 0){
		displs[0] = 0;
		for(i=1; i<size; i++) {
			displs[i] = displs[i-1]+rcounts[i-1];
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Gatherv(mpi_vectA, mpi_vect_size, MPI_DOUBLE, finalVectorA, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectZ, mpi_vect_size, MPI_DOUBLE, finalVectorZ, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectE, mpi_vect_size, MPI_DOUBLE, finalVectorE, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectV, mpi_vect_size, MPI_DOUBLE, finalVectorV, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectT, mpi_vect_size, MPI_DOUBLE, finalVectorT, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	delete[] mpi_vectA;
	delete[] mpi_vectZ;
	delete[] mpi_vectE;
	delete[] mpi_vectT;
	delete[] mpi_vectV;
	delete[] rcounts;
	delete[] displs;
#endif // CRISP_MPI

//masterNRuns

	if(rank == 0){
		
		try{
		  for( i = size*hostsNRuns; i < size*hostsNRuns+masterNRuns; i++ ){
			 
			 a_frag = mf->CrossSectionA(0., vectA_simm[i], vectA_fissao[i], l);
			 z_frag = mf->CrossSectionZ(a_frag);
			 e_frag1 = (a_frag/vectA_fissao[i]) * (deformFactor * vectE_fissao[i]);
			 e_frag2 = ((vectA_fissao[i]-a_frag)/vectA_fissao[i]) * (deformFactor * vectE_fissao[i]);
			 
			CalculateT_V(vectA_fissao[i], vectZ_fissao[i], a_frag, z_frag, vectE_fissao[i]*(1.-deformFactor), v_frag1, v_frag2,  T_frag1, T_frag2);	
				  
			 for(int k=0; k<l; k++){
				  extraVectorA[extra_vect_size] = a_frag;
				  extraVectorZ[extra_vect_size] = z_frag;
				  extraVectorE[extra_vect_size] = e_frag1;
				  extraVectorT[extra_vect_size] = T_frag1;
  				  extraVectorV[extra_vect_size] = v_frag1;
				  extraVectorA[extra_vect_size+1] = vectA_fissao[i] - a_frag;
				  extraVectorZ[extra_vect_size+1] = vectZ_fissao[i] - z_frag;
				  extraVectorE[extra_vect_size+1] = e_frag2;
				  extraVectorT[extra_vect_size+1] = T_frag2;
				  extraVectorV[extra_vect_size+1] = v_frag2;
				  extra_vect_size += 2;
			 }
			 l = 0;
		  }
		}
		catch(int &contTent){
		    cout << "Nao foi possivel sortear um fragmento apos " << contTent << " tentativas\n\n";
			 i--;
		}
	}

	if( rank == 0 ){
	  
		if(deformFactor > 0.){

			cout << endl << "Fragments evaporation..." << endl << endl;
				
			Int_t final_a = 0;
			Int_t final_z = 0;
			Int_t neutron = 0;
			Int_t proton = 0;
			Int_t alpha = 0;
			Double_t A = 0.;
			Double_t Z = 0.;
			Double_t V_frag = 0.;
			Double_t T_frag = 0.;
				
			TString prefix = "results/MultimodalFission/";
			gSystem->mkdir( ( prefix + mf->GetGenerator() + "/" + mf->GetNucleus()).Data() , kTRUE);
			TString fname_out = prefix + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_fragments.root";
				
			TFile file(fname_out.Data(), "recreate");
			TTree *Hist = new TTree("history", "MultimodalFission Tree");
			Hist->Branch("FragA",&final_a,"A/I");
			Hist->Branch("FragZ",&final_z,"Z/I");
			Hist->Branch("neutron",&neutron,"Neu/I");
			Hist->Branch("proton",&proton,"Pro/I");
			Hist->Branch("alpha",&alpha,"Alp/I");
			Hist->Branch("Velocity",&V_frag,"Vel/D");
			Hist->Branch("TEnergy",&T_frag,"TE/D");

			Mcef *mcef = new Mcef();
			
			for( int j=0; j < final_size; j++){
				 #ifdef CRISP_MPI
				 A = cint(finalVectorA[j]);
				 Z = cint(finalVectorZ[j]);
				 mcef->Generate((int)A, (int)Z, finalVectorE[j]);
				 #else // CRISP_MPI
				 A = cint(mpi_vectA[j]);
				 Z = cint(mpi_vectZ[j]);
				 mcef->Generate((int)A, (int)Z, mpi_vectE[j]);
				 #endif
				 if(mcef->NumberFissions() == 0){
					  mcef->FinalConfiguration((int)A, (int)Z, final_a, final_z);
					  if(final_a > 0 && final_z > 0){
							neutron = mcef->NeutronMultiplicity();
							proton = mcef->ProtonMultiplicity();
							alpha = mcef->AlphaMultiplicity();
							#ifdef CRISP_MPI
							T_frag = finalVectorT[j];
							V_frag = finalVectorV[j];
							#else
							T_frag = mpi_vectT[j];
							V_frag = mpi_vectV[j];
							#endif
							Hist->Fill();
					  }
				 }
			}
			
			for(int j=0; j < extra_vect_size; j++){
			  
				 A = cint(extraVectorA[j]);
				 Z = cint(extraVectorZ[j]);
				 mcef->Generate((int)A, (int)Z, extraVectorE[j]);
				 if(mcef->NumberFissions() == 0){
					  mcef->FinalConfiguration((int)A, (int)Z, final_a, final_z);
					  if(final_a > 0 && final_z > 0){
							neutron = mcef->NeutronMultiplicity();
							proton = mcef->ProtonMultiplicity();
							alpha = mcef->AlphaMultiplicity();
							T_frag = extraVectorT[j];
							V_frag = extraVectorV[j];
							Hist->Fill();
					  }
				 }
			}
			Hist->Write();
			TH1D fP = mcef->fissParamDist();
			fP.Write("fissParam");
			file.Close();
			delete mcef;
				//delete Hist;	
		}
		else if (deformFactor == 0.){
		  
		 	cout << endl << "Fragments will not evaporate (deformFactor = 0)" << endl << endl;
				
			Int_t final_a = 0;
			Int_t final_z = 0;
			Int_t neutron = 0;
			Int_t proton = 0;
			Int_t alpha = 0;
				
			TString prefix = "results/MultimodalFission/";
			gSystem->mkdir( ( prefix + mf->GetGenerator() + "/" + mf->GetNucleus()).Data() , kTRUE);
			TString fname_out = prefix + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_fragments.root";
				
			TFile file(fname_out.Data(), "recreate");
			TTree *Hist = new TTree("history", "MultimodalFission Tree");
			Hist->Branch("FragA",&final_a,"A/I");
			Hist->Branch("FragZ",&final_z,"Z/I");
			Hist->Branch("neutron",&neutron,"Neu/I");
			Hist->Branch("proton",&proton,"Pro/I");
			Hist->Branch("alpha",&alpha,"Alp/I");
				
			for( int j=0; j < final_size; j++){
				 #ifdef CRISP_MPI
				 final_a = cint(finalVectorA[j]);
				 final_z = cint(finalVectorZ[j]);
				 neutron = 0;
				 proton = 0;
				 alpha = 0;
				 //mcef->Generate((int)A, (int)Z, finalVectorE[j]);
				 #else // CRISP_MPI
				 final_a = cint(mpi_vectA[j]);
				 final_z = cint(mpi_vectZ[j]);
				 neutron = 0;
				 proton = 0;
				 alpha = 0;
				 //mcef->Generate((int)A, (int)Z, mpi_vectE[j]);
				 #endif
				 Hist->Fill();
			}
				
			for(int j=0; j < extra_vect_size; j++){
			  
				 final_a = cint(extraVectorA[j]);
				 final_z = cint(extraVectorZ[j]);
				 neutron = 0;
				 proton = 0;
				 alpha = 0;
				 Hist->Fill();
			}
				
			Hist->Write();
			file.Close();
		}
		
		//Simulation plot
		TString prefix = "results/MultimodalFission/";
		TString fname_out = prefix + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_fragments.root";
		Int_t Afrag = 0;
		TFile F(fname_out.Data());
		TTree *t = (TTree*)F.Get("history");
		t->SetBranchAddress("FragA",&Afrag);
		int n = t->GetEntries();
		
		Double_t Amin = 0.,
			Amax = 200.;
					
		Double_t Amin2 = mf->GetA(),
			Amax2 = 0.;
		
		TH1D *h = new TH1D("hCalc",(mf->GetNucleus() + " fragments cross section").Data(),Amax-Amin+1,Amin,Amax+1);
  
		for(int i=0; i<n; i++){
			 t->GetEntry(i);
			 h->Fill((double)Afrag);
			 if((double)Afrag < Amin2) {
				  Amin2 = (double)Afrag;
			 }
			 if((double)Afrag > Amax2) {
				  Amax2 = (double)Afrag;
			 }
		}
		
		TString fname_mcef = "results/mcef/" + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_mcef.root";
		TFile F2(fname_mcef.Data());
		TVectorD *CS = (TVectorD*)F2.Get("FissionCrossSection");
		double scale = ( 2. * (*CS)(0) )/h->Integral();
		
		h->Scale(scale);
		
		TGraph *gr = new TGraph(h);
		gr->SetLineStyle(1);

		TCanvas *c1 = new TCanvas("c","Mass Distribution", 200, 10, 600, 400);
		c1->SetFillColor(10);
		c1->SetBorderMode(0);
		
		gPad->SetLogy();
		
		gr->Draw("AL");
		gr->GetXaxis()->SetTitle("A");
		gr->GetXaxis()->SetTitleSize(0.05);
		gr->GetXaxis()->SetLabelSize(0.05);
		gr->GetXaxis()->SetRangeUser(Amin2, Amax2);
		
		if(mf->GetGenerator().CompareTo("bremss") == 0){
		  gr->GetYaxis()->SetTitle("#Y_{A}, mb/eq.q.");
		} else if(mf->GetGenerator().CompareTo("photon") == 0){
		  gr->GetYaxis()->SetTitle("#sigma(A) (#mub)");
		} else if(mf->GetGenerator().CompareTo("proton") == 0 || mf->GetGenerator().CompareTo("deuteron") == 0 || mf->GetGenerator().CompareTo("ultra") == 0){
		  gr->GetYaxis()->SetTitle("#sigma(A) (mb)");
		}
		gr->GetYaxis()->SetTitleSize(0.05);
		gr->GetYaxis()->SetLabelSize(0.05);
		
		TString graph = prefix + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_" + mf->GetGenerator() + ".pdf";
		c1->Print(graph.Data());
		
		delete gr;
		delete h;
		delete c1;
		
	}
	delete[] finalVectorA;
	delete[] finalVectorZ;
	delete[] finalVectorE;
	delete[] extraVectorA;
	delete[] extraVectorZ;
	delete[] extraVectorE;
}
