/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "TMath.h"
#include "TF1.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TVectorT.h"

#include "Mcef.hh"
#include "MultimodalFission.hh"
#include "Util.hh"

#ifdef CRISP_MPI
#include <mpi.h>
#endif // CRISP_MPI
  
#include <vector>
#include <cmath>
#include <fstream>

using namespace std;

struct DataMultimodalFissionStruct{
	std::vector<TString> nucleusName, generator, energy;
	std::vector<Int_t> A, Z;
	std::vector<Double_t> mu1, mu2, gamma1, gamma2, norm;
	Double_t *Vect_SI;
	Double_t *Vect_SII;
	Double_t *Vect_SIII;
	Double_t *Vect_SL;
	std::vector<Double_t*> SIParams;
	std::vector<Double_t*> SIIParams;
	std::vector<Double_t*> SIIIParams;
	std::vector<Double_t*> SLParams;
	DataMultimodalFissionStruct();
	Bool_t IsDataOk;
	Int_t MultFissNumber;
	void ReadData(fstream &file);
};

void outOfMemory();

void execute_fragments(MultimodalFission *mf);
double CalculateMass_Pearson_Original(int A, int Z);
///Calculates kinetic energy and velocity of fragments
void CalculateT_V(int A, int Z, int A1, int Z1, double E_ex , double &v1, double &v2, double &T1, double &T2);
