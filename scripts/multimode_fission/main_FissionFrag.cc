/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "FissionFrag.hh"

int main( int argc, char *argv[] ){

	 fstream file;
	 TString line = "--help";
	 if (argc == 1) {
		  printf ("Syntax Error:  <program> [file]\n");
		  printf ("Type --help for help\n\n");
		  return 1;		
	 }
	 if (argc > 1 && line.CompareTo(argv [1]) == 0) {
		  printf ("Syntax:  <program> [file]\n\n");
		  printf ("file:  multimodal fission input file must have extension *.multFiss \n");
		  return 1;		
	 }	
	 file.open (argv [1], ios::in);
	 if (!file.is_open ()) {
		  printf ("File not found: %s\n", argv [1]);
		  return 1;
	 }
	 
	 DataMultimodalFissionStruct Data;
	 Data.ReadData(file);
	 if (!Data.IsDataOk){
		  std::cout << "Input error\n";
		  return 1;
	 }

	 TStopwatch sw;
	 sw.Start();
	 
#ifdef CRISP_MPI
	 //int rank(0);
	 //int size(1);
	 MPI_Init( &argc, &argv );
	 //MPI_Comm_rank( MPI_COMM_WORLD, &rank );		// rank of the processes
	 //MPI_Comm_size( MPI_COMM_WORLD, &size );		// number of processes
#endif // CRISP_MPI

	 MultimodalFission *mf = new MultimodalFission();
  
	 for(int i=0; i<Data.MultFissNumber; i++){
		
		  mf->SetZ(Data.Z[i]);
		  mf->SetA(Data.A[i]);
		  mf->SetNucleus(Data.nucleusName[i]);
		  mf->SetEnergy(Data.energy[i]);
		  mf->SetGenerator(Data.generator[i]);

		  //standard I
		  mf->SetK1as((Data.SIParams[i])[0]);
		  mf->Setsigma_1as((Data.SIParams[i])[1]);
		  mf->SetD1as((Data.SIParams[i])[2]);

		  //std::cout << "SI\t" << (Data.SIParams[i])[0] << "  " << (Data.SIParams[i])[1] << "  " << (Data.SIParams[i])[2] << std::endl;

		  //standard II  
		  mf->SetK2as((Data.SIIParams[i])[0]);                      
		  mf->Setsigma_2as((Data.SIIParams[i])[1]);
		  mf->SetD2as((Data.SIIParams[i])[2]);  

		  //std::cout  << "SII\t" << (Data.SIIParams[i])[0] << "  " << (Data.SIIParams[i])[1] << "  " << (Data.SIIParams[i])[2] << std::endl;

		  //standard III  
		  mf->SetK3as((Data.SIIIParams[i])[0]);                      
		  mf->Setsigma_3as((Data.SIIIParams[i])[1]);
		  mf->SetD3as((Data.SIIIParams[i])[2]);     

		  //std::cout  << "SIII\t" << (Data.SIIIParams[i])[0] << "  " << (Data.SIIIParams[i])[1] << "  " << (Data.SIIIParams[i])[2] << std::endl;
		  
		  //superlong
		  mf->SetKs((Data.SLParams[i])[0]);  
		  mf->Setsigma_s((Data.SLParams[i])[1]);

		  //std::cout  << "SL\t" << (Data.SLParams[i])[0] << "  " << (Data.SLParams[i])[1] << std::endl;
		  
		  //parametrização do Z mais provável
		  //em função da massa do fragmento
		  mf->Setmu1(Data.mu1[i]);
		  mf->Setmu2(Data.mu2[i]);
		  mf->Setgamma1(Data.gamma1[i]);
		  mf->Setgamma2(Data.gamma2[i]);
		  mf->Setnorm(Data.norm[i]);
		  
		  execute_fragments(mf);
	 }
	 
#ifdef CRISP_MPI
	 MPI_Finalize();
#endif // CRISP_MPI
	 sw.Stop();
	 sw.Print();
	 
	 return 0;
}
