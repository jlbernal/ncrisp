/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef CONSTANT_H
#define CONSTANT_H

#include <math.h>

#define _mp	.9382723 * 1000.		//MeV/c^2 --> proton mass
#define _mn	.9395656 * 1000.		//MeV/c^2--> neutron mass
#define _md  1.875628 * 1000.        //MeV/c^2--> deuteron mass
#define _c	2.99792458E8      //m/s

//#define u	931.502		//Mev/c^2
#define _u	931.49386		//Mev/c^2


#define _e   1.602189E-19    //C

#define _r0  1.2025         //fm

#define _Rp  1.14            //fm
#define _Ra  2.16            //fm

#define _hbar 65.8217E-23      //Mev*sec
#define _hbar_sz 0.658217      //Mev*zsec

#define _zs   1.E-21           //sec --> unidad de tiempo


//convertion factors

/*metro a fermi*/
#define _m2fm 1E15
#define _fm2m 1E-15


/*Mev/c^2*/
#define _MeVcc2MW pow(_c*(1E-8)*(1E2), -2)       //MeV/(c*c) a unidad de masa
#define _MW2MeV   pow(MeVcc2MW, -1.)       //unidad de masa a MeV/(c*c)

static const char _nuc_sym[]="H HeLiBeB C N O F NeNaMgAlSiP S ClArK CaScTiV CrMnFeCoNiCuZnGaGeAsSeBrKrRbSrY ZrNbMoTcRuRhPdAgCdInSnSbTeI XeCsBaLaCePrNdPmSmEuGdTbDyHoErTmYbLuHfTaW ReOsIrPtAuHgTlPbBiPoAtRnFrRaAcThPaU NpPuAmCmBkCfEsFmMdNoLrXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXx";


#endif // CONSTANT_H
