/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef DAMPCOEFF_H
#define DAMPCOEFF_H

#include "constant.hh"

extern double beta_OBD(double _q);
extern double beta_OBD1(double _q);
extern double beta_T(double _T, double _T0, double _T1);
extern double beta_TAprox(double _T);
extern double beta_sps(double _q, double _q_neck, double _qsc);

#endif // DAMPCOEFF_H
