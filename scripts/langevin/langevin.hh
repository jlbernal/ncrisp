/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef LANGEVIN_H
#define LANGEVIN_H

#include <iostream>

#include <fstream>
#include <vector>
#include <sstream>

#include "TString.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TVectorD.h"

#include "DataHelper.hh"

#include "Langevin/work_rootfiles.hh"

#include "Langevin/constant.hh"
#include "Langevin/dynamic.hh"
#include "Langevin/potential.hh"

using namespace std;

/**
 * @brief Struct of input data
 * 
 */
struct DataLangevinStruct{
    std::vector<TString> nucleusName, generator, nevents, observable;
    std::vector<Int_t> A, Z, number_of_energies, runs, energy, step_energy;
    std::vector<double> cf;	
    std::vector<Bool_t> mode;
    DataLangevinStruct();
    Bool_t IsDataOk;
    Int_t LangevinNumber;
    void ReadData(fstream &file);
};

void execute_langevin(int _A, int _Z, int _runs, TString generator, TString observable, TString fname_in, TString fname_out, bool mode =1, double cf_ = 2.);

#endif // LANGEVIN_H
