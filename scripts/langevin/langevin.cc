/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "langevin.hh"


using namespace std;

DataLangevinStruct::DataLangevinStruct(){
    IsDataOk = false;
    LangevinNumber = 0;
}

void DataLangevinStruct::ReadData(fstream &file){
    while (true) {
        if (!file.good ()) break;
        string pdline, Stemp;
        double Dtemp;
        int Itemp;
        bool Btemp;
        getline(file, pdline);
        char _chararc =pdline.c_str()[0];
        if (_chararc == '$'){
            string text;
            istringstream istr(pdline.c_str() );
            istr >> text;
            if ( text == "$NucleusName" ) {
                istr >> Stemp;
                nucleusName.push_back(Stemp);
            }
            if ( text == "$MassIndex" ) {
                istr >> Itemp;
                A.push_back(Itemp);
            }
            if ( text == "$NucleusCharge" ) {
                istr >> Itemp;
                Z.push_back(Itemp);
            }
            if ( text == "$CascadeType" ) {
                istr >> Stemp;
                generator.push_back(Stemp);
            }
            if ( text == "$InitialEnergy" ) {
                istr >> Itemp;
                energy.push_back(Itemp);
            }
            if ( text == "$StepEnergy" ) {
                istr >> Itemp;
                step_energy.push_back(Itemp);
            }
            if ( text == "$EnergiesNumber" ) {
                istr >> Itemp;
                number_of_energies.push_back(Itemp);
            }
            if ( text == "$SimulationCases" ) {
                istr >> Stemp;
                nevents.push_back(Stemp);
            }
            if ( text == "$NumberOfRuns" ) {
                istr >> Itemp;
                runs.push_back(Itemp);
            }
            /**
             * @brief mode:
             *              (0)             --> (default) Langevin && statisticall
             *              (< 0)(nagative) --> only Langevin                   <---- no finished
             *              (> 0)(positive) --> statisticall
             */

            if ( text == "$modeRun" ) {
                istr >> Itemp;
                mode.push_back(Itemp);
            }
            if ( text == "$Observable" ) {
                istr >> Stemp;
                observable.push_back(Stemp);
            }
	    if ( text == "$Friction" ) {
                istr >> Dtemp;
                cf.push_back(Dtemp);
            }
        }
    }
    if( (nucleusName.size() == generator.size()) && (nucleusName.size() == A.size()) && (nucleusName.size() == Z.size())
            && (nucleusName.size() == nevents.size()) && (nucleusName.size() == number_of_energies.size())
            && (nucleusName.size() == energy.size()) && (nucleusName.size() == step_energy.size()) && (nucleusName.size() == mode.size())
            && (nucleusName.size() == runs.size()) && (nucleusName.size() == observable.size()) && (nucleusName.size() == cf.size()) ){
        IsDataOk = true;
        LangevinNumber = nucleusName.size();
    }
}

void execute_langevin(int _A, int _Z, int _runs, TString generator, TString observable, TString fname_in, TString fname_out, bool mode, double cf_){
    int fissions = 0;
    int N = 0;
    int tot_counts = 0;
    int A = 0;
    int Z = 0;
    int Counts = 0;
    double ExEnergy = 0.;
    double lx, ly, lz, L_;	
    lx = ly = lz = 0.;	

    TFile *F = new TFile(fname_in.Data());
    TTree *t = (TTree*)F->Get("History");
    int n = t->GetEntries();

    t->SetBranchAddress("finalNuc_A",&A);
    t->SetBranchAddress("finalNuc_Z",&Z);
    t->SetBranchAddress("finalNuc_Ex",&ExEnergy);
    t->SetBranchAddress("finalNuc_Lx",&lx);
    t->SetBranchAddress("finalNuc_Ly",&ly);
    t->SetBranchAddress("finalNuc_Lz",&lz); 
    //t->SetBranchAddress("_Cnt",&Counts);
	
    /**/
    Fission fiss;
    Spallation spall;
    const int maxZ = 120, maxA = 250;
    Double_t frequencies[maxZ][maxA];
    for( int _z = 0; _z < maxZ; _z++){
        for( int _a = 0; _a < maxA; _a++){
            frequencies[_z][_a] = 0.;
        }
    }

    param_rootFiles param;
    param.fname_out=fname_out;
    param.observable =observable;

    double R0=(_r0*pow(A, 1./3.));
    //double qsci =0.3*R0/2.;
    double qsci =1.2;
    double qneck =0.6;
    //double td =100.0*hbar_sz;      AQUI TAMBIEN REVISAR
    double td =100.0;

    dynamic *objectDynamic =new dynamic(param);
    objectDynamic->Set_Beta_l(cf_); 

    /**
     * Counts the negative excitation energies of cascade
     * 
     */
    int mycounter = 0;   
   std::cout << std::endl << "Running LANGEVIN from " << fname_in.Data() << std::endl;
   
   
    for(int i=0; i<n; i++){ 
        t->GetEntry(i);
	L_  = TMath::Sqrt(lx*lx+ ly*ly + lz*lz);	
        objectDynamic->SetAngMoment(L_);
  	

        if ( ExEnergy > 0. ){
            cout << "-------------------------" << endl;
            cout << "|" << A << "\t \t \t|" << endl;
            cout << "|" << setw(3) << _nuc_sym[2*Z-2] << _nuc_sym[2*Z-1] << " *Ex:= " << ExEnergy << " MeV" << "\t|" << endl;
            cout << "|" << Z ;
            cout << "\t \t \t|" << endl;
            cout << "-------------------------" << endl;
            std::cout << "running... " << i << " of " <<  n << " entry" << std::endl;


        objectDynamic->run(A, Z, ExEnergy, _runs, qsci, qneck, td, mode);
        }else{
	mycounter++;
		}
        cout << " conteos negativos " << mycounter << endl << endl;   
  }
      delete objectDynamic;

} 
