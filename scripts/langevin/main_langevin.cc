/* ================================================================================
 * 
 * 	Copyright 2015 Ubaldo Baños José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include <iostream>

#include "TSystem.h"

#include "langevin.hh"

using namespace std;

int main (int argc, char *argv[])
{

    fstream file;
    TString line = "--help";
    if (argc == 1) {
        printf ("Syntax Error:  <program> [file]\n");
        printf ("Type --help for help\n\n");
        return 1;
    }
    if (argc > 1 && line.CompareTo(argv [1]) == 0) {
        printf ("Syntax:  <program> [file]\n\n");
        printf ("file:  langevin input file must have extension *.lgvn \n");
        return 1;
    }
    file.open (argv [1], ios::in);
    if (!file.is_open ()) {
        printf ("File not found: %s\n", argv [1]);
        return 1;
    }

    DataLangevinStruct Data;
    Data.ReadData(file);
    if (!Data.IsDataOk){
        std::cout << "Input error\n";
        return 1;
    }

    TString fname_in = "";
    TString fname_out = "";

    TStopwatch sw;
    sw.Start();

    TString prefix = "results/langevin/";
    for(int i=0; i<Data.LangevinNumber; i++){

        gSystem->mkdir( ( prefix + Data.generator[i] + "/" + Data.observable[i] + "/" + Data.nucleusName[i]).Data() , kTRUE);

        for(int j=0; j<Data.number_of_energies[i]; j++){
	//	cout << Data.cf[i] << endl;
            Int_t _energy = Data.energy[i] + j * Data.step_energy[i];
            fname_in = Form(("results/mcmc/" + Data.generator[i] + "/" + Data.nucleusName[i] + "/" + Data.nucleusName[i] + "_%dMeV_" + Data.nevents[i] + ".root").Data(), _energy);
            //fname_in = Form((Data.generator[i] + "/" + Data.nucleusName[i] + "/" + Data.nucleusName[i] + "_%d_" + Data.nevents[i] + ".root").Data(), _energy);
            fname_out = Form((prefix + Data.generator[i] + "/" + Data.observable[i] + "/" + Data.nucleusName[i] + "/" + Data.nucleusName[i] + "_%d_langevin.root").Data(), _energy);
            execute_langevin(Data.A[i], Data.Z[i], Data.runs[i], Data.generator[i], Data.observable[i], fname_in, fname_out, Data.mode[i], Data.cf[i] );
        }
    }

    sw.Stop();
    sw.Print();

    return 1;
}



