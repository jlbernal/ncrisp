/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef NUCLEUS_H
#define NUCLEUS_H

#include <stdio.h>


#include "constant.hh"
#include "binding_energy.hh"
#include "fission_const.hh"

struct nucleus{

public:

    nucleus(int A, int Z, int Ex);
    nucleus();

    //    enum partiType {proton, neutron, deuteron, alpha, gamma, fission};

    //    inline int get_A(){
    //        return A;
    //    }

    //    inline int get_Z(){
    //        return Z;
    //    }


    //    inline double get_EB(){
    //        return EB;
    //    }

    //    inline double get_Ex(){
    //        return Ex;
    //    }


    //    /**
    //     * @brief calculate_B //Calcula la energia de separaci\'on para cada particula
    //     * @param A
    //     * @param Z
    //     * @param particle
    //     * @return
    //     */
    //    double calculate_B(int A, int Z, enum partiType particle);

    //    double Coulomb(int A, int Z, partiType particle);

private:

    int A;
    int Z;
    double Ex;

    //    double EB;
    //    double Bf;



    //    double C;

    void initialization();


    //    double level(int A, partiType particle);

    //    void bindingEnergy(int A, int Z){
    //        EB =binding(A,Z);
    //    }

    //    double Ef(int A, int Z, partiType particle){
    //        return Ex - calculate_B(A, Z, particle);
    //    }



    //    /**
    //     *del CRISP
    //*/

    //    double pn1;
    //    double pn2;
    //    double pp1;
    //    double pp2;
    //    double pa1;
    //    double pa2;

public:

    /* Bf */

    double CalculateBf(int A, int Z, double E );
    double GetF(double xo);
    double FissionBarrierCorrection(int A);

    /* af */

    double CalculateAf(int A, int Z, double E);
    double CalculateRf(double y);

    /* an */

    double CalculateAn(int A, int Z, double);

    /* ap */
    double CalculateAp(int A, int Z, double E);

    /* bn */

    double CalculateBn(int A, int Z);

    /* bp */

    double CalculateBp(int A, int Z, double);

    /* ba */

    double CalculateBa(int A, int Z, double);


    /* Va */

    double CalculateVa(int A, int Z, double E);

    /* Vp */

    double CalculateVp(int A, int Z, double E);

    /* Aa */
    double CalculateAa(int A, int Z, double);



};

#endif // NUCLEUS_H
