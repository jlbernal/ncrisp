/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#ifndef __DERIVFUNCTION_H__
#define __DERIVFUNCTION_H__

#include <math.h>
#include "function.hh"


extern void deriv_central (const function *f,
                       double x, double h,
                       double *result, double *abserr);

extern void deriv_backward (const function *f,
                        double x, double h,
                        double *result, double *abserr);

extern void deriv_forward (const function *f,
                       double x, double h,
                       double *result, double *abserr);

#endif /* __DERIVFUNCTION_H__ */
