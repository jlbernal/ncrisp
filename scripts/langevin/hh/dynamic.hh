/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef DYNAMIC_H
#define DYNAMIC_H

#include <math.h>
#include <time.h>
//#include <sys/time.h>

#include <fstream>
#include <iostream>
#include <iomanip>

#include <assert.h>  // Defines the assert function.
#include <stdlib.h>

#include "util.hh"
#include "mcefl.hh"
#include "nucleus.hh"

#include "potential.hh"
#include "constant.hh"

#include "dampcoeff.hh"

#include "TStopwatch.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TVectorD.h"

#include <vector>

#include "DataHelper.hh"

#include "work_rootfiles.hh"




using namespace std;
struct paramFunctionStruct{
    double k;
    double beta;
    double randValue;
    double mass;
    double kT;
    double Exit;
    double A;
    double Z;
};

class dynamic
{
public:
    //dynamic(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, TTree *Hist, TString observable, int mode= 0);
    dynamic(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode= 0);
    dynamic(param_rootFiles param);

    ~dynamic();

    void run();
    void run(int A, int Z, double Ex, int nRun, double qsci, double qneck, double teq, int mode);

    //    int Get_Nres(){ return Nres;}
    //    int Get_Ndin(){ return Ndin;}
    //    int Get_Nfiss(){ return Nfiss;}
    //    int Get_Nstat(){ return nStat;}

    int Get_neutron(){return n_neutron;}
    int Get_proton(){return n_proton;}
    int Get_alpha(){return n_alpha;}
    int Get_fission() { return n_fission; }
    int Get_residues(){ return n_res;}
    double Get_fissility(){return n_fissility;}
    void Set_Beta_l( double beta_ ){beta_l = beta_;}; 

private:
    nucleus *my_nucleus;
    MCEFL *statisticalB;
    NuclearPotentialStruc potStruct;
    paramFunctionStruct paramFunction;

     work_rootFiles *rootFile;

    int mode;

    int A;
    int Z;
    double Ex;
    int nRun;

    double An, Bn;
    double Af, Bf;
    double Ap, Bp, Vp;
    double Aa, Ba, Va;

    int Nfiss;

    double tau;
    double teq;
    double tmax;

    int Nres;

    int Ndin;

    double qsci;
    double qneck;

    bool finish_run;

    int n_proton;
    int n_neutron;
    int n_alpha;
    int n_fission;
    double n_fissility;

    int n_res;

    long idum;

    int nStat;


    /* files */
    ofstream traj_file;
    ofstream out_file;
    ofstream init_file;
    ofstream sc_energy_file;

    ofstream n_energy_file;
    ofstream p_energy_file;
    ofstream a_energy_file;

    ofstream n_time_file;
    ofstream p_time_file;
    ofstream a_time_file;

//    TTree *Hist;
//    TString observable;
    Fission fiss;
    Spallation spall;

    /**/

    void converUnits();

    void Langevin_Statisticall();
    void Langevin();
    bool Statisticall_dynamic(int &A, int &Z, double &Ex, double &time);
    bool Statisticall_dynamic_old(int &A, int &Z, double &Ex);
    void Statisticall_1(int A, int Z, double Ex);

    /*dynamical branch emmision*/

    void dynamicalEmission(int &A, int &Z, double &En);
    void dynamicalEmission1(int &A, int &Z, double &Ex, double time);

    double Gamma_p(int A, int Z, double E);
    double Gamma_a(int A, int Z, double E);
    double Gamma_f(int A, int Z, double E);

    /* energy spectra*/
    double kinetic_energy(double Ex_max, double aj, int A, bool neutron =false);
    double probability_energy_fun(double Ex, double _x_max, double Ex_max, double aj);

    /* to Langevin*/

    double q_eq;
    double step;
    int DeltaStep;
    double beta_l; 


   //  inline double Get_Beta( ){return beta;}; 
    void initial_Condition(double *q, double *pq, void *param);
    double boltzmanFunction(double q, double pq, void *param);

    //    double beta_OBD(double _q);

    //    double boltzmanFunction(double q, double pq, void *param){
    //        double mass = ((struct paramFunction *) param)->mass;
    //        double _A = ((struct paramFunction *) param)->A;

    //        double fact =(pq*pq/2./mass)/param.kT;
    //        double kT = sqrt(Ex/(_A/10.));          //esta es una primera aproximacion, para ver si pincha... corregir densidad de estados
    //        return exp(-fact/kT);
    //    }


};

#endif // DYNAMIC_H
