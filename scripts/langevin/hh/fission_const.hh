/* ================================================================================
 * 
 * 	Copyright 2015 José Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef FISSION_CONST_H
#define FISSION_CONST_H

static const double prf[20] = { 1.29875 ,
                          /* prf2*/  1.25807556963 ,
                          /* prf3*/  1.21625 ,
                          /* prf4*/  1.093768988208,
                          /* prf5*/  1.05125,
                          /* prf6*/  1.043844389675,
                          /* prf7*/  1.006061553298,
                          /* prf8*/  1.004451681981,
                          /* prf9*/  1.00065678667,
                          /* prf10*/  1.04438793896, //
                          /* prf11*/  1.067298739708,
                          /* prf12*/  1.0875,
                          /* prf13*/  1.06875, //
                          /* prf14*/  1.014091758915,        //
                          /* prf15*/  1.0875,
                          /* prf16*/  1.025,
                          /* prf17*/  1.071659386563,
                          /* prf18*/  1.084135262171,
                          /* prf19*/  1.084734928521,
                          /* prf20*/  1.065758659645};

/**
 *del CRISP
*/

static const double pn1 = 18.81302;
static const double pn2 = 1.30001;
static const double pp1 = 18.670295218;
static const double pp2 = 3.83501;
static const double pa1 = 18.67906239024;
static const double pa2 = 25.2201;



#endif // FISSION_CONST_H
