
#ifdef __CINT__
 
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ off nestedclasses;

#pragma link C++ class CrispParticleTable;
#pragma link C++ class Dynamics;
#pragma link C++ class FermiLevels;
#pragma link C++ class Measurement;
#pragma link C++ class MesonsPool;
//#pragma link C++ class LeptonsPool;
#pragma link C++ class NucleusDynamics;
#pragma link C++ class ParticleDynamics;
#pragma link C++ class AbstractChannel;
#pragma link C++ class BBChannel;
#pragma link C++ class CrossSectionChannel;
#pragma link C++ class CrossSectionData;
#pragma link C++ class EventGen;
#pragma link C++ class MesonNucleonChannel;
#pragma link C++ class LeptonNucleonChannel;
#pragma link C++ class PhotonChannel;
#pragma link C++ class PhotonEventGen;
#pragma link C++ class ProtonEventGen;
#pragma link C++ class AbstractCascadeProcess;
#pragma link C++ class BaryonBaryon;
#pragma link C++ class BaryonDecay;
#pragma link C++ class BaryonMeson;
#pragma link C++ class BaryonLepton;
#pragma link C++ class BaryonSurface;
#pragma link C++ class Cascade;
#pragma link C++ class MesonDecay;
#pragma link C++ class MesonSurface;
//#pragma link C++ class LeptonSurface;
#pragma link C++ class SelectedProcess;
#pragma link C++ class TimeOrdering;
#pragma link C++ class Mcef;
#pragma link C++ class Particle;
#pragma link C++ class McefModel;
#pragma link C++ class BsfgModel;
#pragma link C++ class MultimodalFission;
#pragma link C++ class FragmentCrossSectionFCN;
#pragma link C++ class BremsstrahlungEventGen;
#pragma link C++ class UltraEventGen;
#pragma link C++ class HyperonEventGen;
#pragma link C++ class IonEventGen;
#pragma link C++ class DeuteronEventGen;
#pragma link C++ class NeutrinoEventGen;
#pragma link C++ class BindingEnergyTable;

#pragma link C++ class std::vector<Particle> + ;

//#pragma link C++ defined_in "subdir/MyHeader.h";

#endif // __CINT__
