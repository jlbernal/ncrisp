/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */



#include "MultimodalFission.hh"

using namespace std;

//Construtor
MultimodalFission::MultimodalFission()
{
	InitParameters();
}

//Construtor de cópia
MultimodalFission::MultimodalFission(const MultimodalFission& multF)
{
	Z = multF.Z;
	A = multF.A;
	Nucleus = multF.Nucleus;
	Energy = multF.Energy;
	Generator = multF.Generator;

	//standard I
	K1as = multF.K1as;
  	sigma_1as = multF.sigma_1as;        
  	D1as = multF.D1as;

	//standard II  
  	K2as = multF.K2as;                     
  	sigma_2as = multF.sigma_2as;   
  	D2as = multF.D2as;      
 
	//superlong
  	Ks = multF.Ks;  
  	sigma_s = multF.sigma_s;
	
	//super-asymmetric  
	K3as = multF.K3as;                     
  	sigma_3as = multF.sigma_3as;
  	D3as = multF.D3as;
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
	mu1 = multF.mu1;
 	mu2 = multF.mu2;
	gamma1 = multF.gamma1;
  	gamma2 = multF.gamma2;

	//normalização
	norm = multF.norm;
}

//Destrutor
MultimodalFission::~MultimodalFission(){}

void MultimodalFission::InitParameters(){

	A = 241;
	Z = 95;
	Nucleus = "Am241";
	Energy = "660";
	Generator = "proton";

	K1as = 45.0;      
	sigma_1as = 4.2;        
	D1as = 20.;
  
	K2as = 220.5;                     
	sigma_2as = 7.0; 
	D2as = 25.5;
  
	Ks = 2970.0;  
	sigma_s = 15.;
	
 	K3as = 0.;                     
 	sigma_3as = 1.;
 	D3as = 0.;
  
	mu1 = 4.1;
	mu2 = 0.38;
	gamma1 = 0.92;
	gamma2 = 0.003;

	norm = 2161.7;
}

//Inicializa número atômico
void MultimodalFission::SetZ( int z ){	Z = z;}

//Inicializa número de massa
void MultimodalFission::SetA( int a ){	A = a;}

//Inicializa o caso a ser simulado
void MultimodalFission::SetNucleus( TString nu ){Nucleus = nu;}

//Inicializa o caso a ser simulado
void MultimodalFission::SetEnergy( TString en ){Energy = en;}

//Inicializa o caso a ser simulado (Projétil)
void MultimodalFission::SetGenerator( TString pr ){Generator = pr;}

//standard I
void MultimodalFission::SetK1as(double k1){K1as = k1;}
void MultimodalFission::Setsigma_1as(double sig1){	sigma_1as = sig1;}        
void MultimodalFission::SetD1as(double d1){D1as = d1;}

//standard II  
void MultimodalFission::SetK2as(double k2){K2as = k2;}                     
void MultimodalFission::Setsigma_2as(double sig2){sigma_2as = sig2;}
void MultimodalFission::SetD2as(double d2){D2as = d2;}      
  
//superlong
void MultimodalFission::SetKs(double ks){Ks = ks;}  
void MultimodalFission::Setsigma_s(double sigs){sigma_s = sigs;}

//standard III
void MultimodalFission::SetK3as(double k3){K3as = k3;}               
void MultimodalFission::Setsigma_3as(double sig3){sigma_3as = sig3;}
void MultimodalFission::SetD3as(double d3){D3as = d3;} 

//parametrização do Z mais provável
//em função da massa do fragmento
void MultimodalFission::Setmu1(double m1){mu1 = m1;}
void MultimodalFission::Setmu2(double m2){mu2 = m2;}
void MultimodalFission::Setgamma1(double g1){gamma1 = g1;}
void MultimodalFission::Setgamma2(double g2){gamma2 = g2;}
void MultimodalFission::Setnorm(double no){norm = no;}

//função para o sorteio da massa do fragmento de fissão com base no 
//método da rejeição
double MultimodalFission::CrossSectionA(double Amin, double Amean, double Amax, int &count){

	double a_frag = 0.;
	bool passou = false;
	int contTent = 0;
	
	double g1 = 0.,
			 g2 = 0.,
			 gs = 0.,
			 g1l = 0.,
			 g2l = 0.,
 			 g3 = 0.,
 			 g3l = 0.,
			 Ksum = 0.;
			 
	Ksum = K1as + K2as + K3as + Ks + K1as + K2as + K3as;

	while(!passou){
		
		contTent++;

		a_frag = gRandom->Uniform(Amin, Amax);

		g1 = (K1as/Ksum) * TMath::Gaus(a_frag, Amean + D1as, sigma_1as, kTRUE);
		g2 = (K2as/Ksum) * TMath::Gaus(a_frag, Amean + D2as, sigma_2as, kTRUE);
		gs = (Ks/Ksum) * TMath::Gaus(a_frag, Amean, sigma_s, kTRUE);
		g1l = (K1as/Ksum) * TMath::Gaus(a_frag, Amean - D1as, sigma_1as, kTRUE);
		g2l = (K2as/Ksum) * TMath::Gaus(a_frag, Amean - D2as, sigma_2as, kTRUE);
		g3 = (K3as/Ksum) * TMath::Gaus(a_frag, Amean + D3as, sigma_3as, kTRUE);
		g3l = (K3as/Ksum) * TMath::Gaus(a_frag, Amean - D3as, sigma_3as, kTRUE);

		double fmax = 0.;

		if( (K1as/Ksum)*(1./(sigma_1as*sqrt(2.*TMath::Pi()))) <= (K2as/Ksum)*(1./(sigma_2as*sqrt(2.*TMath::Pi()))) ){
			 fmax = (K2as/Ksum)*(1./(sigma_2as*sqrt(2.*TMath::Pi())));
		}
		else{
			 fmax = (K1as/Ksum)*(1./(sigma_1as*sqrt(2.*TMath::Pi())));
		}

		if( fmax <= (Ks/Ksum)*(1./(sigma_s*sqrt(2.*TMath::Pi()))) ){
			 fmax = (Ks/Ksum)*(1./(sigma_s*sqrt(2.*TMath::Pi())));
		}
		  
		if( fmax <= (K3as/Ksum)*(1./(sigma_3as*sqrt(2.*TMath::Pi()))) ){
 			 fmax = (K3as/Ksum)*(1./(sigma_3as*sqrt(2.*TMath::Pi())));
		}
	
		double r = gRandom->Uniform();

		if(r < g1/fmax){
			 passou = true;
			 count++;
		}
		if(r < g2/fmax){
			 passou = true;
			 count++;
		}
		if(r < gs/fmax){
			 passou = true;
			 count++;
		}
		if(r < g1l/fmax){
			 passou = true;
			 count++;
		}
		if(r < g2l/fmax){
			 passou = true;
			 count++;
		}
 		if(r < g3/fmax){
			//std::cout << "g3" << std::endl;
 			 passou = true;
 			 count++;
 		}
 		if(r < g3l/fmax){
			//std::cout << "g3l" << std::endl;
 			 passou = true;
 			 count++;
 		}
		//cout << "contTent: " << contTent << endl;
		if(contTent >= 100) throw contTent;
	}

	return a_frag;
}

//forma gaussina proposta em Karapetyan G S, Balabekyan A R, Demekhina N A and Adam J 2009 Phys. At. Nucl. 72 911–6
//utilizada auxiliarmente nesta classe pelo método CrossSectionZ
double gausZ(double *x, double *par){

	double g = ( 1. / (sqrt( 3.1415 )*par[0] ) ) * exp( - ( (( x[0] - par[1] )*( x[0] - par[1] ))/( par[0]*par[0] ) ) );

	return g;

}

double MultimodalFission::CrossSectionZ(double a_frag){

	double z_frag = 0.;

	double gammaZ = gamma1 + gamma2*a_frag;
	double Zp = mu1 + mu2*a_frag;

	double Zmin = Zp - 3.*gammaZ;
	if(Zmin<0.) Zmin = 0.;
	
	double Zmax = Zp + 3.*gammaZ;

	TF1 *f = new TF1("f",gausZ,0,100,2);
	f->SetParameters(gammaZ, Zp);

	z_frag = f->GetRandom(Zmin, Zmax);
	
	delete f;

	return z_frag;
}


//função para sorteio do número de massa do fragmento
//NOT RECOMMENDED - high computacional cost due to the necessity of integration
// at each step of the the loop and lack of further tests regarding the results
double MultimodalFission::CrossSectionA2(double *x, double *par){

	//double g1 = K1as * TMath::Gaus(x[0], vectA_simm[i] + D1as, sigma_1as, kTRUE);
	//double g2 = K2as * TMath::Gaus(x[0], vectA_simm[i] + D2as, sigma_2as, kTRUE);
	//double gs = Ks * TMath::Gaus(x[0], vectA_simm[i], sigma_s, kTRUE);
	//double g1l = Kl1as * TMath::Gaus(x[0], vectA_simm[i] - D1as, sigmal1as, kTRUE);
	//double g2l = Kl2as * TMath::Gaus(x[0], vectA_simm[i] - D2as, sigmal2as, kTRUE);

	double g1 = par[0] * TMath::Gaus(x[0], par[1], par[2], kTRUE);
	double g2 = par[3] * TMath::Gaus(x[0], par[4], par[5], kTRUE);
	double gs = par[6] * TMath::Gaus(x[0], par[7], par[8], kTRUE);
	double g1l = par[9] * TMath::Gaus(x[0], par[10], par[11], kTRUE);
	double g2l = par[12] * TMath::Gaus(x[0], par[13], par[14], kTRUE);

	double f = g1 + g2 + gs + g1l + g2l;

	return f;
}

//forma gaussina proposta em Karapetyan G S, Balabekyan A R, Demekhina N A and Adam J 2009 Phys. At. Nucl. 72 911–6
//sem uso na atual implementação - equivalente a função auxiliar gausZ
double MultimodalFission::CrossSectionZ2(double *x, double *par){

	double g = ( 1. / (sqrt( 3.1415 )*par[0] ) ) * exp( - ( (( x[0] - par[1] )*( x[0] - par[1] ))/( par[0]*par[0] ) ) );

	return g;
}

/*
PARÂMETROS INICIAIS DE TODOS OS CASOS ESTUDADOS - O Am241 é o caso default da classe

	multF.SetZ(92);
	multF.SetA(238);
	multF.SetNucleus("U238");
	multF.SetEnergy("50");
	multF.SetGenerator("photon");
	multF.SetReactionType("bremsstrahlung");

	//standard I
	multF.SetK1as(7.5);
   multF.SetKl1as(7.5);
   multF.Setsigma_1as(3.54);        
   multF.Setsigmal1as(3.54);
   multF.SetD1as(15.82);

	//standard II  
   multF.SetK2as(120.);              
   multF.SetKl2as(140.);         
   multF.Setsigma_2as(6.);    
   multF.Setsigmal2as(6.);
   multF.SetD2as(20.);      
  
	//superlong
   multF.SetKs(23.75);  
   multF.Setsigma_s(12.);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(4.1);
   multF.Setmu2(0.38);
   multF.Setgamma1(0.92);
   multF.Setgamma2(0.003);
	multF.Setnorm(298.75); //50 MeV
	multF.Setnorm(537.5); //3500 MeV


	multF.SetZ(93);
	multF.SetA(237);
	multF.SetNucleus("Np237");
	multF.SetEnergy("660");
	multF.SetGenerator("proton");
	multF.SetReactionType("monocromatico");

	//standard I
	multF.SetK1as(49.0);
   multF.SetKl1as(49.0);
   multF.Setsigma_1as(4.5);        
   multF.Setsigmal1as(4.5);
   multF.SetD1as(21.3);

	//standard II  
   multF.SetK2as(252.);              
   multF.SetKl2as(252.);         
   multF.Setsigma_2as(6.5);    
   multF.Setsigmal2as(6.5);
   multF.SetD2as(26.3);      
  
	//superlong
   multF.SetKs(2590.0);  
   multF.Setsigma_s(13.7);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(5.0);
   multF.Setmu2(0.37);
   multF.Setgamma1(0.59);
   multF.Setgamma2(0.005);
	multF.Setnorm(2138.62);


	multF.SetZ(92);
	multF.SetA(238);
	multF.SetNucleus("U238");
	multF.SetEnergy("660");
	multF.SetGenerator("proton");
	multF.SetReactionType("monocromatico");

	//standard I
	multF.SetK1as(52.0);
   multF.SetKl1as(52.0);
   multF.Setsigma_1as(3.54);        
   multF.Setsigmal1as(3.54);
   multF.SetD1as(18.5);

	//standard II  
   multF.SetK2as(474.92);              
   multF.SetKl2as(474.92);         
   multF.Setsigma_2as(6.);    
   multF.Setsigmal2as(6.);
   multF.SetD2as(23.5);      
  
	//superlong
   multF.SetKs(1392.65);  
   multF.Setsigma_s(13.);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(4.1);
   multF.Setmu2(0.38);
   multF.Setgamma1(0.92);
   multF.Setgamma2(0.003);
	multF.Setnorm(1911.75);


	multF.SetZ(90);
	multF.SetA(232);
	multF.SetNucleus("Th232");
	multF.SetEnergy("50");
	multF.SetGenerator("photon");
	multF.SetReactionType("bremsstrahlung");

	//standard I
	multF.SetK1as(7.79);
   multF.SetKl1as(7.79);
   multF.Setsigma_1as(8.97);        
   multF.Setsigmal1as(8.97);
   multF.SetD1as(15.82);

	//standard II  
   multF.SetK2as(28.95);              
   multF.SetKl2as(25.91);         
   multF.Setsigma_2as(11.1);    
   multF.Setsigmal2as(11.8);
   multF.SetD2as(23.88);      
  
	//superlong
   multF.SetKs(8.97);  
   multF.Setsigma_s(21.24);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(5.0);
   multF.Setmu2(0.37);
   multF.Setgamma1(0.59);
   multF.Setgamma2(0.005);
	multF.Setnorm(79.41); //50 MeV
	multF.Setnorm(259.18); //3500 MeV


	multF.SetZ(82);
	multF.SetA(208);
	multF.SetNucleus("Pb208");
	multF.SetEnergy("1000");
	multF.SetGenerator("proton");
	multF.SetReactionType("monocromatico");

	//standard I
	multF.SetK1as(0.055);
   multF.SetKl1as(0.055);
   multF.Setsigma_1as(3.97);        
   multF.Setsigmal1as(3.97);
   multF.SetD1as(25.15);

	//standard II  
   multF.SetK2as(0.034);              
   multF.SetKl2as(0.034);         
   multF.Setsigma_2as(5.21);    
   multF.Setsigmal2as(5.21);
   multF.SetD2as(72.4);      
  
	//superlong
   multF.SetKs(75.5);  
   multF.Setsigma_s(14.93);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(0.97);
   multF.Setmu2(0.413);
   multF.Setgamma1(0.5);
   multF.Setgamma2(0.008);
	multF.Setnorm(172.225); //1000 MeV
	multF.Setnorm(87.555); //500 MeV


	multF.SetZ(79);
	multF.SetA(197);
	multF.SetNucleus("Au197");
	multF.SetEnergy("800");
	multF.SetGenerator("proton");
	multF.SetReactionType("monocromatico");

	//standard I
	multF.SetK1as(0.058);
   multF.SetKl1as(0.058);
   multF.Setsigma_1as(3.97);        
   multF.Setsigmal1as(3.97);
   multF.SetD1as(24.46);

	//standard II  
   multF.SetK2as(0.035);              
   multF.SetKl2as(0.035);         
   multF.Setsigma_2as(5.21);    
   multF.Setsigmal2as(5.21);
   multF.SetD2as(72.09);      
  
	//superlong
   multF.SetKs(75.5);  
   multF.Setsigma_s(14.93);
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	multF.Setmu1(1.642);
   multF.Setmu2(0.41193);
   multF.Setgamma1(0.850);
   multF.Setgamma2(0.00395);
	multF.Setnorm(179.401);
*/

