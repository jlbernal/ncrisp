/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef MULTIMODAL_FISSION_pHH
#define MULTIMODAL_FISSION_pHH

#include "TString.h"
#include "TF1.h"
#include "TMath.h"
#include "TRandom3.h"

#include <cmath>

class MultimodalFission{

public:

   MultimodalFission();
	MultimodalFission(const MultimodalFission&);
	virtual ~MultimodalFission();

	void SetZ(int);
	void SetA(int);
	void SetNucleus(TString);
	void SetEnergy(TString);
	void SetGenerator(TString);

	//standard I
	void SetK1as(double);
	void Setsigma_1as(double);        
	void SetD1as(double);

	//standard II  
   void SetK2as(double);                     
   void Setsigma_2as(double);
   void SetD2as(double);      
  
	//superlong
   void SetKs(double);  
   void Setsigma_s(double);
	
 	//super-asymmetric
 	void SetK3as(double);                     
   void Setsigma_3as(double);  
   void SetD3as(double);  
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	void Setmu1(double);
   void Setmu2(double);
   void Setgamma1(double);
   void Setgamma2(double);

	//normalização
	void Setnorm(double);

	inline int 		GetZ() const { return Z; }
	inline int 		GetA() const { return A; }
	inline TString	GetNucleus() const { return Nucleus; }
	inline TString GetEnergy() const { return Energy; }
	inline TString GetGenerator() const { return Generator; }

	//standard I
	inline double GetK1as() const { return K1as; }
   inline double Getsigma_1as() const { return sigma_1as; }        
   inline double GetD1as() const { return D1as; }

	//standard II  
   inline double GetK2as() const { return K2as; }                    
   inline double Getsigma_2as() const { return sigma_2as; }  
   inline double GetD2as() const { return D2as; }
  
	//superlong
   inline double GetKs() const { return Ks; }
   inline double Getsigma_s() const { return sigma_s; }
   
   //super-asymmetric
   inline double GetK3as() const { return K3as; }                      
   inline double Getsigma_3as() const { return sigma_3as; }  
   inline double GetD3as() const { return D3as; }
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	inline double Getmu1() const { return mu1; }
	inline double Getmu2() const { return mu2; }
	inline double Getgamma1() const { return gamma1; }
	inline double Getgamma2() const { return gamma2; }

	inline double Getnorm() const { return norm; }

	void InitParameters();
	double CrossSectionA(double, double, double, int &);
	double CrossSectionZ(double);
	double CrossSectionA2(double *, double *);
	double CrossSectionZ2(double *, double *);

private:

	int Z;
	int A;
	TString Nucleus;
	TString Energy;
	TString Generator;

	//standard I
	double K1as;
   double sigma_1as;        
   double D1as;

	//standard II  
   double K2as;                       
   double sigma_2as; 
   double D2as;      
  
	//superlong
   double Ks;  
   double sigma_s;
	
   //super-asymmetric 
   double K3as;                   
   double sigma_3as;    
   double D3as;
  
	//parametrização do Z mais provável
	//em função da massa do fragmento
  	double mu1;
   double mu2;
   double gamma1;
   double gamma2;

	//normalização
	double norm;

	#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(MultimodalFission, 0);
	#endif // CRISP_SKIP_ROOTDICT
};

#endif
