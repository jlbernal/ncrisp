/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __base_defs_pHH
#define __base_defs_pHH

#include <iostream>
#include "TMath.h"
#include "Api.h"

/// upper numerical limit (infinite)
//_________________________________________________________________________________________________										

const Double_t infty = 1.E10;

/// fast square  
//_________________________________________________________________________________________________										

inline Double_t sqr(Double_t x) { return x*x; }

/// fast square root 
//_________________________________________________________________________________________________										

inline Int_t sqr(Int_t i) { return i*i; }

#endif
