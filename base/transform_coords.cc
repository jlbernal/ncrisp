/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "transform_coords.hh"

//_________________________________________________________________________________________________										

double tcol(double enr, double x, double psq){

	double 	con = 8.636886, 
			e = enr * 0.001, 
			expo = 0.1932952, 
			_tcol  = 1. - 2. * x;
  
	if ( e <= 1.063 ) 
		return _tcol ;
  
	double 	p2 = psq * 1.E-6 ,
			tm = 4.0 * p2 ,
			alfa = con * pow((e - 1.063), expo) ,
			y = 0.5 * alfa * tm ,
			t=0,
			z=0;
	
	if ( y > 70.0 ) {
    
		if( x < 0.5 ) {
		
			z = log ( 0.5 - x ) + y ;
			t = ( - z - 0.693147 ) / alfa + 0.5 * tm ;
		}
    
		if( x == 0.5 ) 
			t = 0.50 * tm ;
    
		if( x > 0.5 ) {
			z = log ( x - 0.50 ) + y ;
			t = ( z + 0.693147 ) / alfa + 0.5 * tm ;
		}
    
		_tcol = 1.0 - 0.5 * t / p2 ;
		
		return _tcol ;
	}
  
	t = TMath::ASinH( 2.0 * ( x - 0.5 ) * sinh ( 0.5 * alfa * tm )) / alfa + 0.5 * tm ;
	
	_tcol = 1.0 - 0.5 * t / p2 ;
	return _tcol ;
}

//_________________________________________________________________________________________________										

TLorentzVector lorentzRotation( const TLorentzVector& pTotal, const TLorentzVector& k ){

	const double cos_theta = cos(pTotal.Theta()), sin_theta = sin(pTotal.Theta());
	const double cos_phi = cos(pTotal.Phi()), sin_phi = sin(pTotal.Phi());
  
	const double beta = pTotal.Vect().Mag()/pTotal.E();
	const double gamma = 1./sqrt( 1. - TMath::Power(beta,2) );
	const double beta_gamma = beta * gamma;
   
	const double a11 = cos_theta * cos_phi;
	const double a12 = cos_theta * sin_phi;
	const double a13 = -sin_theta;
  
	const double a21 = -sin_phi;
	const double a22 = cos_phi;
  
	const double a31 = gamma * sin_theta * cos_phi;
	const double a32 = gamma * sin_theta * sin_phi;
	const double a33 = gamma * cos_theta;
	const double a34 = - beta_gamma;
  
	const double a41 = -beta_gamma * sin_theta * cos_phi;
	const double a42 = -beta_gamma * sin_theta * sin_phi;
	const double a43 = -beta_gamma * cos_theta;
	const double a44 = gamma;
  
	TLorentzVector q( 	a11 * k.Px() + 	a12 * k.Py() + 	a13 * k.Pz(),
						a21 * k.Px() + 	a22 * k.Py(),	
						a31 * k.Px() + 	a32 * k.Py() + 	a33 * k.Pz() + 	a34 * k.E(),
						a41 * k.Px() + 	a42 * k.Py() + 	a43 * k.Pz() + 	a44 * k.E() );
  
	return q; 
}


//_________________________________________________________________________________________________										

TLorentzVector inverseLorentzRotation( const TLorentzVector& pTotal, const TLorentzVector& k ){
  
	const double cos_theta = cos(pTotal.Theta()), sin_theta = sin(pTotal.Theta());
	const double cos_phi = cos(pTotal.Phi()), sin_phi = sin(pTotal.Phi());

	const double beta = pTotal.Vect().Mag()/pTotal.E();
	const double gamma = 1./sqrt( 1. - TMath::Power(beta,2) );
	const double beta_gamma = beta * gamma;

	const double a11 = cos_theta * cos_phi;
	const double a12 = -sin_phi;
	const double a13 = gamma * sin_theta * cos_phi;
	const double a14 = beta_gamma * sin_theta * cos_phi;

	const double a21 = cos_theta * sin_phi;
	const double a22 = cos_phi;
	const double a23 = gamma * sin_theta * sin_phi ;
	const double a24 = beta_gamma * sin_theta * sin_phi;
  
	const double a31 = - sin_theta ;
	const double a33 = gamma * cos_theta ;
	const double a34 = beta_gamma * cos_theta;

	const double a43 = beta_gamma ;
	const double a44 = gamma; 

	TLorentzVector z( 	a11 * k.Px() + 	a12 * k.Py() + 	a13 * k.Pz() + 	a14 * k.E(), 
						a21 * k.Px() + 	a22 * k.Py() + 	a23 * k.Pz() + 	a24 * k.E(),
						a31 * k.Px()    			 +	a33 * k.Pz() + 	a34 * k.E(),
														a43 * k.Pz() + 	a44 * k.E());
  
	return z;
}

//_________________________________________________________________________________________________										

