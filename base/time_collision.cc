/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "time_collision.hh"

//_________________________________________________________________________________________________										
// Retorna o parametro de impacto ao quadrado ( fm^2 ). 
// Esta funcao faz praticamente a mesma coisa que time_collision(...), 
 
double impact_parameter2_2( ParticleDynamics& p1, ParticleDynamics& p2 ){

	// some p1 and p2 variables ...
  
	double 	e1 = p1.Momentum().E(), 
			e2 = p2.Momentum().E();
  
	double 	m1 = p1.GetMass(), 
			m2 = p2.GetMass();
  
	TLorentzVector w = p1.Momentum() + p2.Momentum();
  
	double s = w.M2();  
  
	// the relative velocity and position of p1 and p2
	
	TVector3 v12 = p1.Momentum().Vect() * e1 -  p2.Momentum().Vect() * e2;
	v12 = v12 * (1./(e1 * e2));
  
	TVector3 r12 = p1.Position(); r12 -= p2.Position();

	double v12_dot_r12 = v12.Dot(r12);
  
	if ( v12_dot_r12 > 0 ) 
		return infty;  

	double r12_sqr = r12.Mag2();
	double r12_dot_p1 = r12.Dot( p1.Momentum().Vect() );
	double r12_dot_p2 = r12.Dot( p2.Momentum().Vect() );  
 
	double gs = ( s - sqr(m1 + m2) ) * ( s - sqr(m1 - m2) );
  
	double fs = sqrt(gs) + s - sqr(m1) - sqr(m2);
  
	r12_dot_p2 *= 1./sqr(m2);
	r12_dot_p1 *= 1./sqr(m1);
  
	double c1 = .5 * fs * r12_dot_p2 - r12_dot_p1;
	double c2 = .5 * fs * r12_dot_p1 - r12_dot_p2;

	return ( r12_sqr + 8. * sqr(m1) * sqr(m2) * c1 * c2 /(fs * gs) );  

}

//_________________________________________________________________________________________________										

double impact_parameter2( ParticleDynamics& p1, ParticleDynamics& p2 ){

	// some p1 and p2 variables ...
	
	double e1 = p1.Momentum().E(), e2 = p2.Momentum().E();
  
	TLorentzVector w = p1.Momentum() + p2.Momentum();
 
	// the relative velocity and position of p1 and p2
	TVector3 v12 = p1.Momentum().Vect() * e2 -  p2.Momentum().Vect() * e1;
	v12 = v12 * (1./(e1 * e2));
  
	TVector3 r12 = p1.Position() - p2.Position();

	double v12_dot_r12 = v12.Dot(r12);
  
	if ( v12_dot_r12 > 0 ) 
		return infty;  
  
	Double_t b2 = r12.Mag2() - sqr(v12_dot_r12) / v12.Mag2();
	
	return b2;
}

//_________________________________________________________________________________________________										

double time_collision ( ParticleDynamics& p1, ParticleDynamics& p2, double totalCrossSection ){
  
  
	if ( totalCrossSection < 1.E-7 ) 
		return infty;  
  
	// some p1 and p2 variables ...
	double e1 = p1.Momentum().E(), e2 = p2.Momentum().E();
	
	TVector3 v1 = p1.Momentum().Vect(); 
	TVector3 v2 = p2.Momentum().Vect();
	TVector3 v12 = (p1.Momentum().Vect() * e2) - (p2.Momentum().Vect() * e1);
  
	v12 = v12 * (1./(e1 * e2));

	TVector3 r12 = p1.Position() - p2.Position();
  
	double v12_dot_r12 = v12.Dot(r12);   
	double v12_sqr = v12.Mag2();
	double b2 = impact_parameter2(p1, p2);  
 
	// b2 is im fm^2 totalCrossSection in mbarn. 
	// 10^-1 is the conversion factor from mbarn to fm^2

	if ( TMath::Pi() * b2 > totalCrossSection * .1)
		return infty ;  
  
	double t =  -v12_dot_r12/v12_sqr; // Dtime in fm/c  
	
	return ( t <= 1.E-7 ) ? 1.E-4 : -v12_dot_r12/v12_sqr;
}

//_________________________________________________________________________________________________										

