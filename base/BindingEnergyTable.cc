/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BindingEnergyTable.hh"

Double_t B(const Double_t *x, const Double_t *par){
  
  	//double Av = 15.65, Asf = -17.63, Asym = 27.72, Ass = -25.60; from Pearson
	double Av = par[0], Asf = par[1], Asym = par[2], Ass = par[3];
	
	double Z = x[0], A = x[1];
	double N = x[1] - x[0];
	double r0 = 1.233;
//	double e = sqrt(1.44);	//carga elementar em unidades gaussianas
	double  Ac = (3. * 1.44)/(5. * r0);
		
	double J = (N-Z)/A;
	double Bpearson =  ( Av
			  + Asf * (pow(A,-1./3.))
			  + Ac * (Z * Z)*(pow(A, -4./3.))
			  + ( Asym + Ass * pow(A,-1./3.) )
			    * J*J );

	return Bpearson;
}


BindingEnergyTable::BindingEnergyTable(){
  
	for(int i=0; i<ZMax; i++){
	  for(int j=0; j<AMax; j++){
	    table[i][j] = -1.;
	  }
	}
	std::string file = "data/BindingEnergy/BindingEnergyTableMeV.data";
	ReadTable(file);
}

void BindingEnergyTable::Push( int Z, int A, double bE ){
	//table[ std::pair<int,int>(Z, A) ] = bE;
	table[Z][A] = bE;
}
	
bool BindingEnergyTable::Exist( int Z, int A ){
// 	std::map< std::pair< int, int >, double >::iterator it;
// 	it = table.find( std::pair<int,int>(Z, A) );
// 	if( it == table.end() )
// 		return false;
// 	else
// 		return true;

	if(table[Z][A] == -1.) 
	    return false;
	else
	    return true;
}
	

// Funtion Get(Z, A) reads a file of initialy only experimental bindidng energy in search for a match.
// In case of not finding a nucleus, the Pearson bindidng energy formula is fitted in a region limited in Z centered at
// the nucleus of interest. The new evaluated bindidng energy enters the table in the same file for future use.
// The original experimental only file can be found in folders data/BindingEnergy and exp_data/nuclear_masses 
double BindingEnergyTable::Get( int Z, int A ){
	
	if(Exist(Z,A)){
// 	    std::map< std::pair< int, int >, double >::iterator it;
// 	    it = table.find( std::pair<int,int>(Z, A) );
// 	    return A*(it->second);
	    return A*table[Z][A];
	} else {
	    
	    TGraph2D *exp = new TGraph2D("data/BindingEnergy/BindingEnergyTableMeV.data", "%lg %lg %lg");
	    
		 double Zmin = 0., Zmax = 0.;
		 
		 if(Z<=20){
					  Zmin = double(Z-20),
					  Zmax = double(Z+20);
// 	    double Amin = double(A-50),
// 		     Amax = double(A+50);
		 } else {
					  Zmin = double(Z-10),
					  Zmax = double(Z+10);
// 	    double Amin = double(A-50),
// 		     Amax = double(A+50);
		 }
	    TF2 *fitB = new TF2("fitB",B,Zmin,Zmax,0.,293,4);
	    fitB->SetParameters(15.65, -17.63, 27.72, -25.60);
	    
	    TFitResultPtr r = exp->Fit(fitB,"RS");
	    double p0 = r->Value(0),
		   p1 = r->Value(1),
		   p2 = r->Value(2),
		   p3 = r->Value(3);
	    
	    fitB->SetParameters(p0, p1, p2, p3);
	    double newB = fitB->Eval(double(Z), double(A));
	    
	    std::fstream fs;
	    fs.open("data/BindingEnergy/BindingEnergyTableMeV.data", std::fstream::in | std::fstream::out | std::fstream::app);
	    fs << Z << "\t" << A << "\t" << newB << endl;
	    fs.close();
	    
	    Push(Z, A, newB);
	    
	    delete fitB;
	    delete exp;
	    
	    cout << "nao existe Z: " << Z << "\t A: " << A << "\t B: " << newB << endl;
	    
	    return A*newB;
	}
}
	
void BindingEnergyTable::Dump(){
// 	std::cout << "Z\tA\tbinding energy/A\n";
// 	std::map< std::pair< int, int >, double >::iterator it;
// 	for ( it = table.begin() ; it != table.end(); it++ )
// 		std::cout << (*it).first.first << "\t" <<  (*it).first.second << "\t" << (*it).second << std::endl;
}
	
void BindingEnergyTable::ReadTable( std::string file ){
	TGraph2D data( file.c_str(),"%lg %lg %lg");
	Int_t N = data.GetN();
	Double_t* X = data.GetX();
	Double_t* Y = data.GetY();
	Double_t* Z = data.GetZ();
	for(int i(0); i<N; i++){
		Push( X[i], Y[i], Z[i] );
	}
}
	
//! ler sem usar TGraph
void BindingEnergyTable::ReadTable2( std::string file ){
	std::ifstream ifs( file.c_str() );
	std::string buf;
		
	while(ifs.good()){
		int _Z(0), _A(0);
		double bE(0);
		ifs >> _Z >> _A >> bE;
		Push(_Z,_A,bE);
	}
	ifs.close();
}

