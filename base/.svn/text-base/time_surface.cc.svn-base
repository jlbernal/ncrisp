/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "time_surface.hh"

//_________________________________________________________________________________________________										

double time_surface(ParticleDynamics& p, NucleusDynamics& nuc){

	double R2 = sqr(nuc.GetRadium());
	TVector3 r = p.Position();  
	TVector3 v = p.Momentum().Vect();
	v *= 1./p.Momentum().E();
	//double e1 = nuc.Momentum().E(); double e2 = p.Momentum().E();
	//TVector3 r = nuc.Position() - p.Position();
	//TVector3 v = ( e1 * p.Momentum() + e2 * nuc.Momentum() ) * (1./(e1 * e2));
	double ri = r.Mag();
	if ( ri - nuc.GetRadium() > 0. ) { 
		r = r * (1./ri);
		r = r * nuc.GetRadium();
	}
	double r_dot_v = r.Dot(v);  
	double Delta = sqr(2. * r_dot_v) - 4. * v.Mag2() * ( r.Mag2() - R2 ); 
	if ( Delta < 0 ) {  
		std::vector<ParticleDynamics> v;
		v.push_back(p);
		TString err_msg = Form("time_surface: Delta = %f\n", Delta);
		throw BasicException(err_msg, v);
	}
	double t1 = ( (-2. * r_dot_v + sqrt(Delta) ) / (2. * v.Mag2()) );  
	if ( t1 < 0. ) { 
		// Ok, isto nao esta incorreto, mas esta consistente. Algumas particulas 
		// quando forem atualizar as posicoes, dado um tempo dt, poderao ficar
		// f ora do nucleo por um dx, muito menor que o diametro do nucleo.
		return .001;
	}
	return t1;
}
//_____________________________________________________________________________________________										

/* 
 * WARNING: This function doesn't verify the barrier penetration properly.
 * Another implemenation is provided below.
 */

double penetrability( ParticleDynamics& p, double rnt, int nuc_total_charge, double nuc_total_mass ) {
  
	const double iCoulomb = 1.;
	const double ce = 1.4399784;
	// A vector normal to the nuclear surface in the Lab Frame
	TVector3 vradial = p.Position(); // - nuc.position();
	double r = vradial.Mag();
	// normalize ... 
	vradial *= 1./r;
	// get the particle electrical charge ... 
	int p_charge = (int)p.GetParticleData()->Charge(); //WRONG!!! Charge() here returns multiples of |e|/3. It should be divided by 3 as it is done for the nucleus
	double m = p.GetMass();
	if ( p_charge == 0 ) {
		//neutral charge 
		return 1.;
	} 
	else {    
		if ( p_charge < 0 ) 
			return  -1.;
		else {
			double xmu = (m * nuc_total_mass) / (m + nuc_total_mass);           
			// i think it's not correct, i copied it from the crisp old version...
			double ppx = p.Momentum().Px() * vradial.X();
			double ppy = p.Momentum().Py() * vradial.Y();
			double ppz = p.Momentum().Pz() * vradial.Z();
			// something like radial kinetic energy ...
			double radial_kinetic = (sqr(ppx) + sqr(ppy) + sqr(ppz)) / (2. * xmu);
			double barrier = ce * (double)p_charge * (double)nuc_total_charge / rnt;
			
			//cout << "radial_kinetic: " << radial_kinetic << "  barrier: " << barrier << endl;
			
			if ( radial_kinetic >= barrier ) 
				return iCoulomb * 1.;      
			else {
				double cc12 = ce * nuc_total_charge * p_charge;
				double d = cc12 / radial_kinetic;
				double c_over_d = rnt / d;
				double a2 = acos( sqrt( .5 * ( 1. - (1. - 2. * c_over_d) ) ) );
				double a3 = sqrt( c_over_d - sqr(c_over_d) ) ;  
				double g = a2 - a3 ;	
				double sn = xmu * cc12 * g / sqrt(radial_kinetic);		
				//std::cout << p.getParticleData()->name() << ", sn = " << sn << std::endl;	
				double P = ( sn > 10. )? 0. : iCoulomb * exp(-2. * sn);
				
				//if(P > 0 && P < 1.) cout << "P: " << P << endl;
				
				return P;
			}      
		} 
	} // first else closing brace    
}

void particles_walk_thru(Dynamics& p, double time ){

	double e = p.Momentum().E();  
	TVector3 dr = (p.Momentum().Vect() * (time / e) );
	TVector3 r = p.Position() + dr;
	p.SetPosition(r);  
}
//_____________________________________________________________________________________________										

/* 
 * WARNING: This function doesn't verify the barrier penetration properly.
 * Another implemenation is provided below.
 */

bool it_was_even_so( ParticleDynamics& p1, double nuc_rnt, double total_charge, double total_mass, double potential, double well, double iTime ){
	double y = penetrability( p1, nuc_rnt, (int)total_charge, total_mass);
	y = (y >= 0.)? y : 1.;   
	double x = gRandom->Uniform();
	//cout << "x: " << x << "  y: " << y << endl;
	/*
	 * NOTE: Amount of kinetic energy of p must be larger than the potential, i.e., 
	 * since V = m0 - m*, the kinetic energy must be enough for the particle to regain the mass
	 * and still leave the nucleus, that's why the effective mass is used here.
	 */
	double T = p1.Momentum().E() - p1.GetMass();  
	
	//WRONG!!! The potential here includes the coulomb barrier already. Penetrability is not really being considered
	if ( T > potential && x < y ){
		//cout << "particle " << p1.PdgId() << " tunneled! y: " << setprecision(30) << y << endl;
		return true;  
	}
	else {    
		// posicionando a particula na superficie nuclear
		particles_walk_thru(p1, iTime);     
		TVector3 v = p1.Momentum().Vect();           
		TVector3 x = p1.Position();    
		double ri = x.Mag();
		if ( ri > nuc_rnt ) { 
			x = x * (1./ri);
			x = x * nuc_rnt;
		}
//		if ( fabs(v.Dot(x)) < 1. ) {          
//			TVector3 w = - v.Cross(x);
//			v.Rotate(TMath::Pi() * .5, w );      
			p1.SetMomentum(v);
			p1.SetPosition(-x);      
//		} 
//		else{
//			p1.SetMomentum(-v);
//			p1.SetPosition(x);
//		}    
		return false;
	}
}

/*
 * NOTE: New implemenation of penetrability through the Coulomb barrier follows.
 * This implemenation includes a new probability calculation and a new 
 * it_was_even_so function.
 */

double penetrability(ParticleDynamics& p, double nucRadius, double nucTotalCharge, double well){
	
	double e = 0.0854358; //elementary charge in Natural units "with Gauss" ( c = hbar = ke = 1  =>  e = sqrt(\alpha) , \alpha = 1./137.)
	
	int p_charge = (int)p.GetParticleData()->Charge() / 3; //Charge() returns charge in units of |e|/3
	double m = p.GetMass();
	
	if ( p_charge == 0 || p_charge < 0 ) {
		//neutral or negative charge 
		return 1.;
	} 
	else {
		/*
		 * NOTE: The kinetic energy to compare with Coulomb barrier (Vc) is the amount above the nuclear potential V0.
		 * 	V0 < E - m* < V0 + Vc
		 * 	0 < E - m* - V0 < Vc
		 */
		double T = p.Momentum().E() - p.GetMass() - well;
		double Q = nucTotalCharge * p_charge * e * e; //total charge
		double ratio = (nucRadius*(1./197.)*T) / Q; // factor 1/197 takes fm -> MeV^-1
		
		//cout << "T for comparison with barrier: " << T << "  Tmax: " << Q/(nucRadius*(1./197.)) << endl;
		
		double g = 2.*sqrt(2.*m*T) * ( (Q/T) * ( TMath::ACos(sqrt(ratio)) - sqrt( ratio - pow(ratio,2.) ) ) );
		
		double P = TMath::Exp(-g);
		return P;
	}
}

bool it_was_even_so( ParticleDynamics& p1, double nuc_rnt, double total_charge, double potential, double well, double iTime ){
	/*
	 * NOTE: Amount of kinetic energy of p must be larger than the potential, i.e., 
	 * the kinetic energy must be enough for the particle to regain the mass
	 * and still leave the nucleus, that's why the effective mass is used here.
	 */
	double T = p1.Momentum().E() - p1.GetMass();  
	
	//cout << "mass eff: " << p1.GetMass() << "  mass inv: " << p1.GetInvariantMass() << "  Pot: " << well << "  m0-m*: " << p1.GetInvariantMass() - p1.GetMass() << endl;
	
	if(T > potential){ //no barrier penetration involved. Enough energy to leave
		return true;
	}
	else if ( T > well ){ //barrier penetration is an option. Probability must be calculated

		//cout << "possible penetration. T = " << T << endl;
		
		double y = penetrability(p1, nuc_rnt, total_charge, well); 
		double r = gRandom->Uniform();
		//cout << "P(T) = " << y << "  r = " << r << endl;
		if(r < y){
			//cout << "particle " << p1.PdgId() << " tunneled! y: " << y << endl;
			return true;  
		}
		else{
			//no escaping today, placing the particle at the nuclear surface
			particles_walk_thru(p1, iTime);     
			TVector3 v = p1.Momentum().Vect();           
			TVector3 x = p1.Position();    
			double ri = x.Mag();
			if ( ri > nuc_rnt ) { 
				x = x * (1./ri);
				x = x * nuc_rnt;
			}
   
			p1.SetMomentum(v);
			p1.SetPosition(-x);      
   
			return false;
		}
	}
	else {    
		//no escaping today, placing the particle at the nuclear surface
		particles_walk_thru(p1, iTime);     
		TVector3 v = p1.Momentum().Vect();           
		TVector3 x = p1.Position();    
		double ri = x.Mag();
		if ( ri > nuc_rnt ) { 
			x = x * (1./ri);
			x = x * nuc_rnt;
		}
   
		p1.SetMomentum(v);
		p1.SetPosition(-x);      
   
		return false;
	}
}