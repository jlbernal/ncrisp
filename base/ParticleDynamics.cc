/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "ParticleDynamics.hh"
// Default Constructor
ParticleDynamics::ParticleDynamics ():Dynamics(){  
	pid = 0;
	_isBind = false;
}
//_________________________________________________________________________________________________										
ParticleDynamics::ParticleDynamics ( int id ):Dynamics(){
	pid = id;
	_isBind = false;  
	// to be sure that this particle is in ParticleDataTable
	SetMass( GetInvariantMass() );
	SetMomentum ( TVector3() );
}
// the copy constructor
ParticleDynamics::ParticleDynamics(const ParticleDynamics& u):Dynamics(u){
	pid = u.pid;
	_isBind = u._isBind;
	SetMass( u.GetMass() );  
	SetLifeTime(u.LifeTime());
}
//_________________________________________________________________________________________________										
ParticleDynamics& ParticleDynamics::operator= (const ParticleDynamics& u) {
	// pdata = u.pdata;        // copy the ParticleData
	pid = u.pid;
	_isBind = u._isBind;    // copy isBind flag
	SetMass( u.GetMass() );   
	SetLifeTime(u.LifeTime());
	SetMomentum(u.Momentum());
	SetPosition(u.Position());
	// to be sure that this particle is in ParticleDataTable
	assert ( this->HasValidParticleData() );
	return *this;
}  
//_________________________________________________________________________________________________										
Double_t ParticleDynamics::HalfLife(){
	Double_t  psi = gRandom->Uniform();
	Double_t half_life = - 1. / GetTotalWidth() * log(1. - psi);  
	SetLifeTime(half_life);  
	return half_life;
}
// Return informations of this particle like : invariant mass, spin, isoSpin, etc.
TParticlePDG* ParticleDynamics::GetParticleData(){
	return CPT::Instance()->ParticlePDG(pid); 
}   
//_________________________________________________________________________________________________										
void ParticleDynamics::Print(const Option_t* opt) const{
	std::cout << this->ToString();
}
//_________________________________________________________________________________________________										
void ParticleDynamics::Delete(const Option_t* opt) const{
	std::cout << "Delete()" << std::endl;
}
//_________________________________________________________________________________________________										
void ParticleDynamics::Copy(TObject& obj) const{
	((ParticleDynamics&)obj).pid = pid;
	((ParticleDynamics&)obj)._isBind = _isBind;
	Dynamics::Copy(obj);
}
//_________________________________________________________________________________________________										
TString ParticleDynamics::ToString() const{
	TString str = this->Name() + "\n" + Dynamics::ToString();    
	return str;
}
