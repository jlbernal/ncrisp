/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __MEASUREMENT_HH
#define __MEASUREMENT_HH

#include <map>
#include <vector>
#include "TObject.h"
#include <algorithm>

/// tecnical class manipulate class/values
//_________________________________________________________________________________________________										

class Measurement: public TObject{

public:

	/// default constructor
	//_________________________________________________________________________________________________										

	Measurement ();
	
	/// constructor
	//_________________________________________________________________________________________________										

	Measurement ( double value, double sigma = 0., double stat_err = 0.);
	
	/// constructor
	//_________________________________________________________________________________________________										

	Measurement ( double value, double sigma, std::vector<double> chann_value, std::vector<double> chann_sigma);
	
	/// copy constructor
	//_________________________________________________________________________________________________										

	Measurement ( const Measurement &m );

	/// destructor
	//_________________________________________________________________________________________________										

	~Measurement(){}
  
	/// swap method
	//_________________________________________________________________________________________________										

	void swap( Measurement & other );
	
	/// operator =
	///
	/// @param rhs : reference to object
	//_________________________________________________________________________________________________										

	Measurement& operator = ( Measurement const & rhs );

	/// operator <
	///
	/// @param othe : reference to other object
	//_________________________________________________________________________________________________										

	bool  operator <  ( Measurement const & other ) const;
	
	/// operator ==
	///
	/// @param othe : reference to other object
	//_________________________________________________________________________________________________										

	bool  operator == ( Measurement const & other ) const;

	/// Get value
	///
	/// @return value
	//_________________________________________________________________________________________________										

	inline double   value() { return val; }
	
	/// Get sigma
	///
	/// @return sigma
	//_________________________________________________________________________________________________										

	inline double   sigma() { return sig; }
	
	/// Get err
	///
	/// @return err
	//_________________________________________________________________________________________________										

	inline double  error() { return err; }

	/// Get value vector
	///
	/// @return  value vector
	//_________________________________________________________________________________________________										

	inline std::vector<double>   Ch_value() { return chann_val; }

	/// Get sigma vector
	///
	/// @return sigma vector
	//_________________________________________________________________________________________________										

	inline std::vector<double>   Ch_sigma() { return chann_sig; }

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(Measurement,2);
#endif // CRISP_SKIP_ROOTDICT

private:  

	double val; /// value 
	double sig; /// sigma
	double err; /// statistical uncertainty
	std::vector<double> chann_val; // create a vector holding the measurement values of each channel.
	std::vector<double> chann_sig; // create a vector holding the sigma values of each channel .

};	// Measurement

//_________________________________________________________________________________________________										

inline void swap( Measurement & first, Measurement & second ) { first.swap( second ); }

//_________________________________________________________________________________________________										

inline double NaN(){

	// A byte-order-independent way to return a non-signalling NaN
	struct Dib {
		union {
			double d;
			int i[2];
		} u;
	};
	
	Dib pos3_2;
	Dib posTwo;
	pos3_2.u.d =  1.5;  // Leading bit of mantissa = 1 --> quiet NaN
	posTwo.u.d =  2.0;
	Dib value;
	
	value.u.i[0] = pos3_2.u.i[0] | posTwo.u.i[0];
	value.u.i[1] = pos3_2.u.i[1] | posTwo.u.i[1];

	return value.u.d;
}

#endif
