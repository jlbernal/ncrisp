/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "FermiLevels.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(FermiLevels);
#endif // CRISP_SKIP_ROOTDICT

const Int_t FermiLevels::n_max = 250;
// Number of nucleons in each layer of momentum, starting at 0
const Int_t FermiLevels::max[30] =	{ 6,   6,  12,   6,  18,
					 12,  18,   6,  30,  18,
					 24,  12,  30,  18,  48,
					  6,  36,  30,  36,  18,
					 42,  24,  42,  12,  54,
					 30,  84,  18,  54,  48	};

//____________________________________________________________________________________________										
															
FermiLevels::FermiLevels(Int_t particleId, Int_t A, Int_t Z): TObject(), occuped(){
 
	// the Well label
	_particleID = particleId;  
	this->A = A;  this->Z = Z;
	// the number of nucleons Z, or A - Z 
	nucleons = 0;
	// ... fermi momentum and energy for n nucleons
	_fermiMomentum = 0.; 
	_fermiEnergy = 0.;  
	momentum_dist = new TVector3[n_max];  
	this->FillVector();
} 
//_____________________________________________________________________________________________										

FermiLevels::~FermiLevels(){
	delete []momentum_dist; 
}
//_____________________________________________________________________________________________										

void FermiLevels::DoInitConfig( TVector3* v ) {

	Double_t y = 1.0/3.0;  
	Double_t uu = (297. * TMath::Power(2., y)) / CPT::r0;      

	// calculate the fermi energy for _nucleons
	if ( _particleID == CrispParticleTable::proton_ID ){
		nucleons = Z;
		_fermiEnergy = TMath::Power( uu * TMath::Power( (Double_t)nucleons /(Double_t)A, y), 2)/(2. * CPT::p_mass);
		_fermiMomentum = TMath::Sqrt(_fermiEnergy * ( _fermiEnergy + 2. * CPT::p_mass * CPT::effn ));
	}
	else if ( _particleID == CrispParticleTable::neutron_ID ){
		nucleons = A - Z;
		_fermiEnergy = TMath::Power( uu * TMath::Power( (Double_t)nucleons /(Double_t)A, y), 2)/(2. * CPT::n_mass);
		_fermiMomentum = TMath::Sqrt(_fermiEnergy * ( _fermiEnergy + 2. * CPT::n_mass * CPT::effn));
	}
	occuped.clear();
	Int_t z = nucleons;
	Int_t i = 0;
	while ( z > 0 ) { 
		if  ( z - max[i] >= 0 ) {
			occuped.push_back(max[i]);      
			z -= max[i];
		}
		else 
			if ( z - max[i] < 0 ) {
				occuped.push_back(z);
				last_level = z;
				z = 0;
			}          
			i++;
	}
  
	for ( Int_t j = 0; j < nucleons; j++ )
		v[j] = momentum_dist[j];
  
	module_max = occuped.size();
	cell_size = _fermiMomentum/(Double_t)module_max;
	//std::cout << cell_size << std::endl;
	// last_level : numero de nucleons na ultima camada ... 
	last_level = occuped[module_max - 1 ];
	for ( Int_t k = occuped.size(); k < 30; k++) 
		occuped.push_back(0);
}
//_____________________________________________________________________________________________										

Int_t FermiLevels::GetFreeLevels(Int_t i) const { 

	if ( i >= 0 && i < 30 )
		return max[i] - occuped[i];
	if ( i < 0 ){
		std::cerr << "error in : fermilevels i = " << i <<"\n";
		exit(1);
	}
	else     
		return 2;
}
//_____________________________________________________________________________________________										

void FermiLevels::PrintStatus() const{

	CPT *cpt = CPT::Instance();
	std::cout 	<< cpt->ParticlePDG(_particleID)->GetName() << std::endl
				<< "Fermi energy = " << _fermiEnergy
				<< ", Fermi momentum = " << _fermiMomentum
				<< ", cell_size = " << cell_size << std::endl;
  
	Int_t s = 0;
	for ( Int_t i = 0; i < 29 ; i ++ ) {
		std::cout << occuped[i] << " + ";  
		s += occuped[i];
	}  
	std::cout << occuped[29] << " ";  
	s += occuped[29];
	std::cout << " = " << s << std::endl;
}
//_____________________________________________________________________________________________										

Int_t FermiLevels::GetIntLevelOf(Double_t m) const {

	Double_t cs = GetCellSize();
	Int_t k = int(m) /int (cs) - 1;
	//std::cout<<"m "<<m<<" cs "<<cs<<" m/cs "<<m/cs<<" int "<<int(m) /int( cs)<<std::endl;
	return k;
}
//_____________________________________________________________________________________________										

void FermiLevels::FillVector(){

	Int_t x = 1.; 
	Int_t n = 0;
	
	while ( n < n_max ) { 
		
		Int_t y = x * x;
		for (Int_t i = 0; i <= x ; i++){
			for (Int_t j = 0; j <= x ; j++){
				for (Int_t k = 0; k <= x ; k++){
					if ( y == ( i*i + j*j + k*k ) ) {
						momentum_dist[ n ].SetXYZ( (Double_t)i, (Double_t)j, (Double_t)k );   // spin Up
						momentum_dist[n+1].SetXYZ( (Double_t)i, (Double_t)j, (Double_t)k );   // Spin Down
						n += 2;	
						if ( n == n_max ) 
							return;
					}
				}
			}
		}
		x++;
	}
}
//_____________________________________________________________________________________________										

Int_t FermiLevels::maxLevel(int x){

	Int_t n_state=0;
	Int_t y = x * x;

	for (Int_t i = 0; i <= x ; i++){
		for (Int_t j = 0; j <= x ; j++){
			for (Int_t k = 0; k <= x ; k++){
				Int_t sum2 = i*i + j*j + k*k;
				if ( y == sum2 ) {
					n_state++; //account the contribution per level
				}
			}
		}
	}
	n_state*=2; //account the contribution of spin
	return n_state;
}	    
//_____________________________________________________________________________________________										
