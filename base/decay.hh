/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __decay_pHH
#define __decay_pHH

#include <vector>
#include "TParticlePDG.h"
#include "TDecayChannel.h"
#include "base_defs.hh"
#include "CrispParticleTable.hh"
#include "ParticleDynamics.hh"

///
//_________________________________________________________________________________________________										

bool get_decay_channel( Double_t rnd, TParticlePDG* pdg, std::vector<ParticleDynamics>& pv );

#endif
