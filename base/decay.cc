/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "decay.hh"

bool get_decay_channel( Double_t rnd, TParticlePDG* pdg, std::vector<ParticleDynamics>& pv ){
	if ( rnd < 0. || rnd > 1. ) 
		return false;
	pv.clear();
	TObjArray* decayList = pdg->DecayList();
	if (decayList != NULL){
		Double_t sum = 0.;
		for ( Int_t i = 0; i < decayList->GetEntries(); i++ ) {
			// Get the Branching Ratio of the Decay Channel [i]
			sum += ((TDecayChannel*)((*decayList)[i]))->BranchingRatio();    
			if ( rnd <= sum ) {	 
				Int_t Ndaughters = ((TDecayChannel*)((*decayList)[i]))->NDaughters();
				for ( Int_t j = 0; j < Ndaughters; j++ ) {
						Int_t pdg_id =  ((TDecayChannel*)((*decayList)[i]))->DaughterPdgCode(j);
						pv.push_back(ParticleDynamics (pdg_id));	
						// std::cout << p.getParticleData()->name() << std::endl;	
				}
				return (pv.size() > 0);
			}
		}
		return false;
	}
	return false;
}
