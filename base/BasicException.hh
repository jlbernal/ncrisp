/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __BasicEXCEPTION_pHH
#define __BasicEXCEPTION_pHH

#include <iostream>
#include <vector>
#include "TString.h"
#include "ParticleDynamics.hh"

/// Basic Exception class
//_________________________________________________________________________________________________										


class BasicException{

private:
  
	TString aErrorMsg; /// msn var
	std::vector<ParticleDynamics> pvec; /// particle vec err msn

public:
  
	/// Constructor
	///
	/// @param msn : error msn.
	//_________________________________________________________________________________________________										

	BasicException( const TString & msg );  

	/// Constructor 
	///
	/// @param msn : error msn.
	/// @param v : reference for particle vector (for msn error).
	//_________________________________________________________________________________________________										

	BasicException ( const TString& msg, std::vector<ParticleDynamics>& v);
  
	///	Destructor
	//_________________________________________________________________________________________________										

	virtual ~BasicException(){}
  
	/// set error msn 
	///
	/// @param aNewErrorMsg : new error msn
	//_________________________________________________________________________________________________										

	inline void SetErrorMsg(const TString& aNewErrorMsg ) { 
		aErrorMsg = aNewErrorMsg; 
	}
  
	/// print methoi
	//_________________________________________________________________________________________________										

	virtual void PrintOn();  
  
	/// get error msn
	///
	/// @return error msn
	//_________________________________________________________________________________________________										

	inline TString ErrorMsg() { return aErrorMsg; }   

	// ClassDef(BasicException,1);

};

#endif

