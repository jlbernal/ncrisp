/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Dynamics.hh" 

//#if defined(__CINT__)
//#endif
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(Dynamics);
#endif // CRISP_SKIP_ROOTDICT
Dynamics::Dynamics():TObject(), p(0.,0.,0.,0.), x(0.,0.,0.){
	mass = 0.;
	life_time = infty;  // the particle is stable.
}
Dynamics::Dynamics(const Dynamics& u):TObject(), p(), x(){
	((Dynamics&)u).Copy(*this);
	//mass = u.mass;
	//life_time = u.life_time;
}
//_________________________________________________________________________________________________										
void Dynamics::SetMomentum( const TVector3& _p ){
	double p2 = _p.Mag2();
	double E = TMath::Sqrt( p2 + TMath::Power(GetMass(),2) );
	p.SetVect(_p);
	p.SetE(E);
}
//_________________________________________________________________________________________________										
TString Dynamics::ToString() const{
	TString str = Form("m = %lf, T = %lf, abs(P) = %lf\n", this->GetMass(), this->TKinetic(), this->Momentum().Mag());
	str += 	Form("x = (%f,%f,%f)\np = (%f,%f,%f;%f)\n", 
			this->Position().X(),
			this->Position().Y(), this->Position().Z(), 
			this->Momentum().Px(), this->Momentum().Py(),
			this->Momentum().Pz(), this->Momentum().E() );
	return str;
}
//_________________________________________________________________________________________________										
void Dynamics::Copy(TObject& obj) const{
	((Dynamics&)obj).p = p;
	((Dynamics&)obj).x = x;
	((Dynamics&)obj).mass = mass;
	((Dynamics&)obj).life_time  = life_time;
}
