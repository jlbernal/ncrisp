/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __LeptonsPool_pHH
#define __LeptonsPool_pHH

#include "TObject.h"
#include "ParticleDynamics.hh"
#include <vector>
#include <cassert>
#include <map>
#include "TTree.h"
#include "DataHelper.hh"

//!  Lepton Class. 
/*!
  It store the leptons created in the code.
  So far include neutrinos and muons	
*/

class LeptonsPool: public TObject{ 

public:  

 
/**
 * @brief map for Lepton storage typedef. map an int to a particle. 
 **/
typedef std::map<Int_t , ParticleDynamics> LeptonsMap;  

/// iterator definition for lepton's map
	typedef std::map<Int_t , ParticleDynamics>::iterator iterator; 

 //! Destructor.
/*!
*/	virtual ~LeptonsPool(){} 

//! Constructor.
/*!  Set number of leptons to zero and total energy of the container to zero
*/
	LeptonsPool():TObject(), leptons() { num_leptons = 0; totalEn = 0;}  

//! Include a Lepton.
/*!  Set number of leptons and energy of the container to zero
*/
	  Int_t PutLepton(ParticleDynamics& p);

	
/// Used as patch in event generator	
	inline void rmlepton(){num_leptons--;}; 

	
	/**
	 * @brief Overload of [] Operator
	 * @usage to access a particle 
	 * @param i: position of the desire lepton
	 * @return ParticleDynamics&: Lepton at position i in the container
	 **/
	
	ParticleDynamics& operator[](Int_t i) { return leptons[i]; }  
	
	
	/**
	 * @brief Search for a position in the map
	 *
	 * @param key ... desired position to search 
	 * @return bool: true if founded in the map, else otherwise 
	 **/
	bool find( Int_t key ) { return ( leptons.find(key) != leptons.end() ); }

	/// iterator def return first container position
	LeptonsMap::iterator begin() { return leptons.begin(); } 

	/// iterator def return last container position	
	LeptonsMap::iterator end() { return leptons.end(); } 
	
	
	/**
	 * @brief ... Gives the container size
	 *
	 * @return Int_t:  
	 **/
	
	Int_t size() { return leptons.size(); }
	
	
	void clear() { 
		num_leptons = 0;
		leptons.clear(); 
	}
	
	/**
	 * @brief Sum the mass of all Leptons
	 *
	 * @return double
	 **/	
	
	double TotalMass();
	

	/**
	 * @brief Sum the charge of all Leptons
	 *
	 * @return double
	 **/		
	double TotalCharge();  
	
	double TotalEnergy();
	
	double RetrieveTotalEn() { return totalEn;}
	
	void RemoveLepton(Int_t key) { leptons.erase(key);}   

	void AllKeys( Int_t* arr_keys );
	
	std::vector<int> GetKeys(); ///
	
	TTree* GetLeptonPoolTree();
	
	Lep_Struct GetLeptonsStruct();

private:  
	LeptonsMap leptons;	///< map for leptons
	
	Int_t num_leptons;  	///< leptons counter
	
 	Double_t totalEn;	///< Energy of all leptons in the pool

/*
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef ( LeptonsPool, 2);
#endif // CRISP_SKIP_ROOTDICT
*/

};
#endif
