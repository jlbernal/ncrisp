/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __FermiLevels_pHH
#define __FermiLevels_pHH

#include <math.h>
#include <cmath>
#include <iostream>
#include <vector>

#include "TObject.h"
#include "TVector3.h"
#include "TMath.h"
#include "TLorentzVector.h"

#include "CrispParticleTable.hh"

///
//_________________________________________________________________________________________________										

class FermiLevels: public TObject{

private:     

	Int_t _particleID;  //
	Int_t A, Z;          
	Int_t nucleons;     /// number of nucleons of this well

	Int_t last_level;   /// max. number of occupancy of the last level 
	Int_t module_max;   // 
  
	Double_t	_fermiMomentum, /// the Fermi Momentum for this potential well
				_fermiEnergy;	/// the Fermi Energy for this potential well
				
	Double_t cell_size; 
  
	std::vector<Int_t> occuped;

	static const Int_t max[30]; /// Number of nucleons in each layer of momentum.
	static const Int_t n_max;
	
	TVector3 *momentum_dist;
  
protected:

	///
	//_________________________________________________________________________________________________										

	void FillVector(); 

public:   

	/// Death and Rebirth ...
	//_________________________________________________________________________________________________										

	FermiLevels():TObject(), occuped(), momentum_dist() { }   
	
	///
	//_________________________________________________________________________________________________										

	FermiLevels(Int_t ParticleId, Int_t A, Int_t Z);  
	
	///
	//_________________________________________________________________________________________________										

	virtual ~FermiLevels();  

	///
	//_________________________________________________________________________________________________										
    
	Int_t& operator [] (Int_t i) { return occuped[i]; }  

	///
	//_________________________________________________________________________________________________										

	void DoInitConfig( TVector3 *v );
  
	/// A proton or a neutron was kicked off from nucleus.  We have to 
	/// update the momentum cell size by a factor alpha [0., 1.]
	//_________________________________________________________________________________________________										
  
	inline void ScaleMomentumCell( Double_t alpha ) { 
		cell_size = alpha * cell_size; 
	}  

	///
	//_________________________________________________________________________________________________										

	inline Double_t GetFermiMomentum() const { return _fermiMomentum; } 

	///
	//_________________________________________________________________________________________________										
  
	inline Double_t GetFermiEnergy() const { return _fermiEnergy; } 
  
	/// return the momentum cell size.
	//_________________________________________________________________________________________________										

	inline Double_t GetCellSize() const { return cell_size; }

	///
	//_________________________________________________________________________________________________										

	Int_t GetFreeLevels(Int_t i) const;
  
	/// return the fermi level of m, where m is the module of proton 
	/// three momentum    
	//_________________________________________________________________________________________________										

	Int_t GetIntLevelOf(Double_t m) const;	    

	///
	//_________________________________________________________________________________________________										

	Int_t GetMaxLevel(int i) const{
		return max[i];
	}	    

	///
	//_________________________________________________________________________________________________										

	Int_t maxLevel(int i);	    

	///
	//_________________________________________________________________________________________________										

	void PrintStatus(  ) const;

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(FermiLevels,2);
#endif // CRISP_SKIP_ROOTDICT

}; 

#endif
