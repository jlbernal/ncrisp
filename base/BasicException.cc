/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "BasicException.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
// ClassImp(BasicException);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

BasicException::BasicException( const TString & msg ):aErrorMsg(msg), pvec(){ }

//_________________________________________________________________________________________________										

BasicException::BasicException( const TString& msg, std::vector<ParticleDynamics>& v):aErrorMsg(msg),pvec(v){}

//_________________________________________________________________________________________________										

void BasicException::PrintOn(){
 
	if ( pvec.empty() ) 
		std::cout << aErrorMsg << std::endl;
	else { 
		std::cout << aErrorMsg << std::endl;
		for ( unsigned int i = 0; i < pvec.size(); i++ ) 
			pvec[i].Dump();
	}
}

//_________________________________________________________________________________________________										
