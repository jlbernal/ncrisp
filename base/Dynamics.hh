/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef Dynamics_pHH
#define Dynamics_pHH

#include "TObject.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TVector3.h"
#include "TMath.h"

#include "base_defs.hh"
/// A simple common interface to all Dynamics Particles.

class Dynamics : public TObject{

private:
	TLorentzVector p;	/// the momentum 
	TVector3 l;		/// the angular momentum 
	TVector3 x;       	/// the position 
	Double_t mass;     	/// the mass
	Double_t life_time;	/// the particle Life Time  
public:  
	Dynamics();  
	Dynamics(const Dynamics& u);  
	virtual ~Dynamics() { }  
	virtual Int_t PdgId() const { return 0; }  
	virtual Double_t HalfLife() { return 0.; }
	Double_t GetMass() const { return mass; }
	virtual void SetMass( Double_t m ) { mass = m; }  
	inline Double_t TKinetic() const {  return p.E() - mass; }
	inline TLorentzVector& Momentum() { return p; }
	inline const TLorentzVector Momentum() const { return p; }  
	inline void SetMomentum(const TLorentzVector& momentum) { p = momentum; }  
	void SetMomentum( const TVector3& _p );
	inline TVector3& Position() { return x; }  
	inline const TVector3 Position() const { return x; }
	inline void SetPosition(const TVector3& position) { x = position; }      
	Double_t LifeTime() const { return life_time; } 
	void SetLifeTime(Double_t t) { life_time = t; }   
	virtual TString ToString() const;
	virtual void Copy(TObject& obj) const;
	inline const TVector3 AngularMomentum() const { return l; }  
	inline void SetAngularMomentum(const TVector3& momentum) { l = momentum; }  
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(Dynamics,1);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
