/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef relativisticKinematic_pHH
#define relativisticKinematic_pHH

#include <cassert>

#include "TLorentzVector.h"
#include "TMath.h"
#include "TGenPhaseSpace.h"

#include "transform_coords.hh" 
#include "ParticleDynamics.hh"
#include "BasicException.hh"

/// Make final process of colision 
///
/// @param pTotal : total momentum before process
/// @param p1 : particle #1 after process
/// @param p2 : particle #2 after process
//_________________________________________________________________________________________________										

void twoParticlesOutMomentum( const TLorentzVector& pTotal, ParticleDynamics& p1, ParticleDynamics& p2 );

/// Make final process of colision 
///
/// @param pTotal : total momentum before process
/// @param v : particle vector after process
//_________________________________________________________________________________________________										

bool particlesOutMomentum( const TLorentzVector& pTotal, std::vector<ParticleDynamics>& v );

/// Make elastic colision process  
///
/// @param p1 : particle #1 after process
/// @param p2 : particle #2 after process
//_________________________________________________________________________________________________										

void elasticCollision( ParticleDynamics& p1, ParticleDynamics& p2 );

/// Make inelastic colision process  
///
/// @param pi : particle #1 before process
/// @param qi : particle #2 before process
/// @param pf : particle #1 after process
/// @param qf : particle #2 after process
//_________________________________________________________________________________________________										

void inelasticCollision ( ParticleDynamics& pi, ParticleDynamics& qi, ParticleDynamics& pf, ParticleDynamics& qf );


/// Do decay kinematic: go to CM frame, calculate the momentum for the decay particles, change 
/// their momenta and position. 
///
/// @param rss : initial ressonance particle before process
/// @param pf : particle product #1 after process
/// @param qf : particle product #2 after process
//_________________________________________________________________________________________________										

bool decayKinematic( const ParticleDynamics& rss, ParticleDynamics& p1, ParticleDynamics& p2 );

//

Double_t lamda_malds( Double_t x1,  Double_t x2,  Double_t x3 ); // 

#endif




