/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "relativisticKinematic.hh"

//_________________________________________________________________________________________________										
void twoParticlesOutMomentum( const TLorentzVector& pTotal, ParticleDynamics& p1, ParticleDynamics& p2 ){  
	// rotaciona pTotal para o referencial do centro de massa.
	TLorentzVector pcm = lorentzRotation(pTotal, pTotal);
	double s = pTotal.M2();  // s mandelstan relativistic invariant  
	double m1 = p1.GetMass(), m2 = p2.GetMass();  
	double pm2 = ( ( s - TMath::Power( m1 + m2, 2) ) * ( s - TMath::Power( m1 - m2, 2) ) ) / ( 4. * s );
	if ( pm2 > -1.E-7 && pm2 < 0) pm2=0;
	if ( pm2 < 0. ) {
	        std::cout<<"PM2 "<<pm2<<std::endl;
		std::vector<ParticleDynamics> v;
		v.push_back(p1); v.push_back(p2);
		throw BasicException( "twoParticlesOutMomentum(...): Negative Kinetic Energy\n",v);
	}
	double pm = sqrt(pm2);
	// Angular distribution
	// double x = gRandom->Uniform();
	double ts = 2. * gRandom->Uniform() - 1.;  // tcol(pTotal.E(), x, pm2 );
	if ( fabs(ts) > 1. ) {
		std::cout << "<relativisticKinematic.cc: twoParticlesOutMomentum> Error: " << Form("fabs(ts) > 1. ts = %f", ts) << std::endl;
		ts = 1.;
	}
	double cost = ts;     
	double sint = (cost == 1.)? 0. : sqrt(1.0 - TMath::Power(cost, 2));  
	double phi = 2. * TMath::Pi() * gRandom->Uniform();
	// i will assign k to p1.
	TLorentzVector k1( pm * sint * cos(phi), pm * sint * sin(phi), pm * cost, sqrt( TMath::Power(pm,2) + TMath::Power(p1.GetMass(),2)));
	TLorentzVector k2 = pcm - k1;	
	p1.SetMomentum( inverseLorentzRotation(pTotal, k1) );  
	p2.SetMomentum( inverseLorentzRotation(pTotal, k2) );
}
//_________________________________________________________________________________________________										
bool particlesOutMomentum( const TLorentzVector& pTotal, std::vector<ParticleDynamics>& v ){ 
	const Int_t np = v.size();
	if ( np == 2 ) { 
		twoParticlesOutMomentum(pTotal, v[0], v[1] );
		return true;
	}  
	if ( np > 2 ) {
		// initializing the input for Genbod - N-Body Monte-Carlo Event Generator
		// references at : 
		// F. James, Monte Carlo Phase Space, CERN 68-15 (1968)    
		
		/*
		 * NOTE: TGenPhaseSpace works in GeV.
		 * Every quantity must go to GeV first and then back to MeV after event is generated
		 */
		TGenPhaseSpace genbod;  
		Double_t *mass = new Double_t[np];   
		TLorentzVector P = lorentzRotation(pTotal, pTotal);    
		P = P * (1./1000.); //MeV -> GeV
		for (unsigned int i = 0; i < v.size(); i++)
			mass[i] = v[i].GetMass() / 1000.; //MeV -> GeV
		if ( genbod.SetDecay(P, np, mass) ) {
			genbod.Generate();
			TLorentzVector Wfinal;
			for (unsigned int i = 0; i < v.size(); i++) {
				TLorentzVector q = *(genbod.GetDecay(i));
				q = q * 1000.;  //GeV -> MeV
				v[i].SetMomentum( inverseLorentzRotation (pTotal, q) );	
				Wfinal += v[i].Momentum();
			}      
			double s = pTotal.M2();
			double s2 = Wfinal.M2();
			if ( fabs((s - s2)/s) > 1.E-7 ) { 
				// Energy conservation.
				throw BasicException("Energy conservation failure on particlesOutMomentum\n");
			}
		}    
		else { 
			std::cout << "Warning on relativisticKinematics - no event in the phase space... " << std::endl;
			return false;
		}    
		delete mass;
		return true;
	}
}
//_________________________________________________________________________________________________										
void elasticCollision( ParticleDynamics& p1, ParticleDynamics& p2 ){
	TLorentzVector pcm = p1.Momentum() + p2.Momentum();    
	double s1 = pcm.M2();
	twoParticlesOutMomentum(pcm, p1, p2);
	double s2 = ( p1.Momentum() + p2.Momentum() ).M2();  
	if ( fabs((s1 - s2)/s1) > 1.E-7 ) { 
		// Energy conservation.
		std::vector<ParticleDynamics> v;
		v.push_back(p1); v.push_back(p2);
		throw BasicException("Energy conservation failure on elasticCollision\n", v);
	}	
}
//_________________________________________________________________________________________________										
void inelasticCollision ( ParticleDynamics& pi, ParticleDynamics& qi, ParticleDynamics& pf, ParticleDynamics& qf ){
	TLorentzVector pTotal = pi.Momentum() + qi.Momentum();
	twoParticlesOutMomentum(pTotal, pf, qf);  
	double s2 = ( pf.Momentum() + qf.Momentum() ).M2();  
	if ( fabs((pTotal.M2() - s2)/pTotal.M2()) > 1.E-7 ) { 
		// Energy conservation.
		std::vector<ParticleDynamics> v;
		v.push_back(pf); v.push_back(qf);
		throw BasicException("Energy conservation failure on inelasticCollision\n", v);
	}
}
// Do decay kinematic: go to CM frame, calculate the momentum for the decay particles, change their momenta and position. 
bool decayKinematic( const ParticleDynamics& rss, ParticleDynamics& p1, ParticleDynamics& p2 ) { 
  TLorentzVector pTotal = rss.Momentum();
	double s = pTotal.M2();
	double m1 = p1.GetMass(), m2 = p2.GetMass();
	double T = TMath::Power( TMath::Power(m2, 2) - s - TMath::Power(m1, 2), 2)/(4*s) - TMath::Power(m1,2);
	if  ( T >= 0. ) {       
		twoParticlesOutMomentum(pTotal, p1, p2);  
		double s2 = ( p1.Momentum() + p2.Momentum() ).M2(); 
		if ( fabs((s - s2)/s) > 1.E-7 ) { 
		  // Energy conservation.
		  std::vector<ParticleDynamics> v;
		  v.push_back(p1); v.push_back(p2);
		  throw BasicException("Energy conservation failure on decayKinematic\n", v);
		}		
		return true;
	}    
	return false;
}


// triangular funtion that appears in maldestam formalism

Double_t lamda_malds( Double_t x1,  Double_t x2,  Double_t x3 ) { // 

	return x1*x1+x2*x2+x3*x3 -2.*(x1*x2+x2*x3+x3*x1) ;
}



