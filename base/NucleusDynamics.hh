/* ================================================================================
 * 
 * 	Copyright 2008, 2011, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef NucleusDynamics_pHH
#define NucleusDynamics_pHH

#include <iostream>
#include <algorithm>  //swap
#include <vector>

#include "TVector3.h"
#include "TLorentzVector.h"
#include "TRandom.h"
#include "TTree.h"
#include "TString.h"

#include "Dynamics.hh"
#include "ParticleDynamics.hh"
#include "FermiLevels.hh"
#include "DataHelper.hh"
#include "BindingEnergyTable.hh"

/// Represent the Dynamics of Nuclei.
/// It represents the basic structure of nucleus dynamics configuration, the necessary background 
/// for the understanding of the calculations of this class can be found in : 
///
/// A. Deppman, et. al, Phys. Review C 69, 064611 (2004).
//_________________________________________________________________________________________________										

class NucleusDynamics: public Dynamics{
  
private:   
  
	Int_t A, Z;                                // atomic mass and atomic number

	ParticleDynamics* nucleons;                // vector of nucleons.  
  
	FermiLevels plevels;                       // Fermi Levels for protons
	FermiLevels nlevels;                       // Fermi Levels for neutrons
      
	Double_t rnt;                              // nuclear radium
	
	Double_t totalSysEn;			    // total system energy
	Double_t Rot_Kinetic_Energy;
     Double_t Ex_Energy;			 // initial exitation energy
     
#ifdef THERMA
	double occupationInitial_Proton[30];
	double occupationSysClosed_Proton[30];
	double occupationFinal_Proton[30];
	double occupationInitial_Neutron[30];
	double occupationSysClosed_Neutron[30];
	double occupationFinal_Neutron[30];
#endif

public:
  
  


	/// default constructor							

	NucleusDynamics();
	
	//_________________________________________________________________________________________________	
	
	/// constructor									

	explicit NucleusDynamics(Int_t atomicMass, Int_t atomicNumber);

	//_________________________________________________________________________________________________	
	
	/// destructor			

	virtual ~NucleusDynamics();

	//_________________________________________________________________________________________________	
	
	/// Get nucleus ID
	///
	/// @Return	 Id (pdg) of nucleus

	// i'll implement later
	Int_t PdgId() const { return 0;}  
	
	//_________________________________________________________________________________________________	

	/// Get half life of nucleus 
	///
	/// @Return	 half life of nucleus 

	// i'll implement later
	Double_t HalfLife() { return 0.; }  
	
	//_________________________________________________________________________________________________	
	
	/// Retrieve nucleus total kinetic energy
	///
	/// @return nucleus total kinetic energy
	
	Double_t RetrieveTotalSysEn() const { return totalSysEn; }  

	//_________________________________________________________________________________________________	

	/// Get nucleus mass
	///
	/// @return nucleus mass
	
	// return nucleus total mass.
	Double_t GetMass() const;  

	//_________________________________________________________________________________________________	
	
	/// Get atomic number
	///
	/// @return Z
	
	inline Int_t GetZ() const { return Z; } 

	//_________________________________________________________________________________________________	
	
	/// Get mass number 
	///
	/// @return A									
	
	inline Int_t GetA() const { return A; }

	//_________________________________________________________________________________________________	
	
	/// Get proton fermi level struct
	///
	/// @return reference for proton fermi levels
	
	inline FermiLevels& ProtonFermiLevels() { return plevels; }

	//_________________________________________________________________________________________________	
	
	/// Get neutron fermi level struct
	///
	/// @return reference for neutron fermi levels
				
	inline FermiLevels& NeutronFermiLevels() { return nlevels; }

	//_________________________________________________________________________________________________	
	
	/// Get ocupation of i-th proton level   
	///
	/// @param i : level index
	/// @return ocupation in i-th level

	inline Int_t ProtonFermiLevels(Int_t i) {
		return plevels[i]; 
	}

	//_________________________________________________________________________________________________	
	
	/// Get ocupation of i-th neutron level   
	///
	/// @param i : level index
	/// @return ocupation in i-th level
	
	inline Int_t NeutronFermiLevels(Int_t i) { 
		return nlevels[i]; 
	}

	//_________________________________________________________________________________________________	
	
	/// sharp sphere of radium rnt in fermi
	///
	/// @return radius of the nucleus

	Double_t GetRadium() { return  rnt; }   
	
	//_________________________________________________________________________________________________	
	
	///	get total nucleus charge
	///
	/// @return the nucleus total charge(counting the binded nucleons only).
	
	Double_t TotalCharge();

	//_________________________________________________________________________________________________	
	
	/// Get nuclear potential acting on the particle (includes coulomb barrier if it's the case)
	///
	/// @param particle_id : particle id 
	/// @return the nuclear potential acting on the particle.				
	
	Double_t NuclearPotential( Int_t particle_id );

	//_________________________________________________________________________________________________	
	
	/// Get nuclear potential acting on the particle (it doesn't include coulomb barrier for protons)
	///
	/// @param particle_id : particle id 
	/// @return the nuclear potential acting on the particle without coulomb barrier				
	
	Double_t NuclearPotentialWell( Int_t particle_id );

	//_________________________________________________________________________________________________		
	
	Double_t RotatKineticEnergy();
 
	//_________________________________________________________________________________________________		
	
	///Adds the kinetic energy of all bound particles in the nucleus at a given time
	
	Double_t TotalSystemEnergy();
	
	Double_t GetBasicEnergy();
	
	//_________________________________________________________________________________________________		
	/// get Nucleon by index. 0-based.
	///
	/// @param i : nucleon index
	/// @return i-th nucleon.				
	
	inline ParticleDynamics operator [](Int_t i) const { return nucleons[i]; }

	//_________________________________________________________________________________________________	
	
	/// get reference Nucleon by index. 0-based.
	///
	/// @param i : nucleon index
	/// @return i-th nucleon.
	
	inline ParticleDynamics& operator[](Int_t i) { return nucleons[i]; }  

	//_________________________________________________________________________________________________	
	
	/// get Nucleon by index.
	///
	/// @param i : nucleon index
	/// @return i-th nucleon.

	ParticleDynamics& GetNucleon (Int_t i) { return nucleons[i]; }    

	//_________________________________________________________________________________________________	
	
	/// get integer momentum module
	///
	/// @return the integer momentum module
	
	Int_t GetIntLevelOf( ParticleDynamics& p );

	//_________________________________________________________________________________________________	
	
	TTree* GetNucTree();

	//_________________________________________________________________________________________________

	Nuc_Struct GetNucStruct();
	//_________________________________________________________________________________________________

	inline Double_t Get_InitE(){ return Ex_Energy; } // get the initial Exitation energy 
	//_________________________________________________________________________________________________

	inline  void Set_InitE( Double_t E){Ex_Energy=E; } // Set the initial Exitation energy 
	
	//_________________________________________________________________________________________________
	
#ifdef THERMA

	void TakeProtonLevelsSnapshot(TString);
	
	//_________________________________________________________________________________________________
	
	
	void TakeNeutronLevelsSnapshot(TString);

	//_________________________________________________________________________________________________
	
	
	double GetOccupationProton(int, TString);
	
	//_________________________________________________________________________________________________
	
	
	double GetOccupationNeutron(int, TString);
#endif
	
	//_________________________________________________________________________________________________										
	/// Change the nucleon[index] to the new particle. This method check the Pauli Blocking and update 
	/// the Fermi Levels. Take care of ressonance creation and decay either.
	///
	/// @param index : index of particle to be exchanged
	/// @param newParticle : new particle in index
	/// @return check if change is possible.
	
	bool ChangeNucleon( Int_t index, ParticleDynamics& newParticle);
	
	//_________________________________________________________________________________________________	
	
	/// change a nucleon pair to n1 and n2. If is Pauli permitible return true.
	///
	/// @param nucleon1 : #1 index of particle to be exchanged
	/// @param nucleon2 : #2 index of particle to be exchanged
	/// @param n1 : #1 new particle in index
	/// @param n2 : #2 new particle in index
	/// @return check if change is possible(pauli permitted).									

	bool NucleonPairChange( Int_t nucleon1, Int_t nucleon2, ParticleDynamics& n1, ParticleDynamics& n2 ); 
	bool NucleonPairChange_test( Int_t nucleon1, Int_t nucleon2, ParticleDynamics& n1, ParticleDynamics& n2 );
	
	double Sg(int N_neu, int N_prot  ); 
	inline Double_t GetSn(){ return Sn(this->Z,this->A); };
	inline Double_t GetSp(){ return Sp(this->Z,this->A); };
	inline Double_t GetB(){ return B(this->Z,this->A); };
	
	
private:   
	
	//_________________________________________________________________________________________________										

	bool NucleonPairChange_pp_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pp_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pp_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pp_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										

	bool NucleonPairChange_pn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										

	bool NucleonPairChange_np_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_np_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_np_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_np_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										

	bool NucleonPairChange_nn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_nn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_nn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_nn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										

	bool NucleonPairChange_nn_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_nn_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_pp_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pp_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_pn_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pn_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_np_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_np_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										
	
	bool NucleonPairChange_nd_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pd_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_nd_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pd_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_nd_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pd_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_nd_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pd_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ); 
	
	//_________________________________________________________________________________________________										

	bool NucleonPairChange_nn_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_nn_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_pp_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pp_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_pn_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_pn_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_np_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_np_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	//_________________________________________________________________________________________________										
	
	bool NucleonPairChange_dn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_dp_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_dn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_dp_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_dn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_dp_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );

	bool NucleonPairChange_dn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 );
	bool NucleonPairChange_dp_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ); 

	//_________________________________________________________________________________________________										

	double B(int Z_, int A_);
	double Sn(int Z_, int A_);
	double Sp(int Z_, int A_);
	
	
	//_________________________________________________________________________________________________										
	
public:
	
	/// change the nucleon given by it's index to a ressonance (q).
	///
	///	@param nucleonIndex : nucleon index
	/// @param q : ressonance
	/// @return check if put is possible.
	
	bool PutRessonance( Int_t nucleonIndex, ParticleDynamics& q );
	
	//_________________________________________________________________________________________________

	/// decay the ressonance given by it's index to a proton or a neutron.
	///
	///	@param nucleonIndex : nucleon index
	/// @param q : baryon resulting
	/// @return check if decay is possible.
	
	bool RessonanceDecay(Int_t nucleonIndex, ParticleDynamics& q);

	//_________________________________________________________________________________________________
	
	/// Change the nucleon momentum, you can, of course change it directlly using [] operator, also 
	/// this method update the occupance of Fermi Levels of this particle.
	///
	/// @param nucleonIndex : nucleon index 
	/// @param newMomentum : new quadri-momentum for set
	/// @return check if set is possible.
	

	bool SetNucleonMomentum( Int_t nucleonIndex, TLorentzVector& newMomentum); 
	//_________________________________________________________________________________________________
	
	/// The barion is ejected from the nucleus. It unbinds the particle and removes it 
	/// of the corresponding level				
	
	bool UnBindNucleon(Int_t idx); 
	
	//_________________________________________________________________________________________________
	
	//bool BindNucleon(Int_t idx); 

	//_________________________________________________________________________________________________
	
	/// capture nucleon
	///
	/// @param p : particle to be captured
	/// @return captured particle index								
	
	Int_t CaptureNucleon( ParticleDynamics& p);

	//_________________________________________________________________________________________________
	
	/// init the nucleus energy/momentum configuration 
	
	void DoInitConfig();  

	//_________________________________________________________________________________________________
	
	/// redo the nucleus.
	///
	/// @param A_num : mass number
	/// @param Z_num : Atomic number
	
	void DoInitConfig(Int_t A_num, Int_t Z_num);

	///  separation energy for nucleons	
	static const Double_t separation_Energy;

	/// print method	
	void Print()const{
		nlevels.PrintStatus();
		plevels.PrintStatus();
	} 
	

		//_________________________________________________________________________________________________
	
	/// returns the module of proton three-momentum without the contribution of neutron well.
	///
	/// @param p : proton mamoentum
	/// @return proton mamoentum without the contribution of neutron well
	
	Double_t GetProtonMomentum(TVector3 p);

protected:

	/// set isBind flat to true for all nucleons

	void BindAllNucleons(); 



	//_________________________________________________________________________________________________
	
	/// Get free levels for a particle
	///
	/// @param p : particle.
	/// @return the number of free fermi cells for particle p.
	
	Int_t GetFreeLevelsFor( ParticleDynamics& p );
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(NucleusDynamics,2);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
