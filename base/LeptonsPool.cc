/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "LeptonsPool.hh"
//#if defined(__CINT__)
//#endif
/*
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(LeptonsPool);
#endif // CRISP_SKIP_ROOTDICT
*/

//_________________________________________________________________________________________________										
Int_t LeptonsPool::PutLepton( ParticleDynamics& p ){
	Int_t key = num_leptons;
	// assert(p.hasValidParticleData());  
	// std::pair<int, ParticleDynamics> m(key, p);
	// mesons.insert(m);    
	// std::cout << mesons[key].position() << std::endl;
	leptons[key] = p;
	leptons[key].BindIt(true);
/*	ParticleDynamics &q = mesons[key];  
	assert ( q.HasValidParticleData() );  
	//put the mesons bind mass
	q.SetPosition(p.Position());
	q.SetMomentum(p.Momentum());
	q.SetMass( CPT::effd * q.GetInvariantMass() );   
	q.BindIt(true);  
*/	num_leptons++;
	return key;
}
//_________________________________________________________________________________________________										
Double_t LeptonsPool::TotalMass(){
	Double_t total_mass = 0.;  
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		if ( it->second.IsBind() )
			total_mass += it->second.GetMass();
	}
	return total_mass;
}
//_________________________________________________________________________________________________										
Double_t  LeptonsPool::TotalCharge(){
	Double_t total_charge = 0.;
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		if ( it->second.IsBind() ) 
			total_charge += it->second.GetParticleData()->Charge()/3.; //ROOT gives the charge in units of |e|/3 check
	}
	return total_charge;
}
//_________________________________________________________________________________________________			
Double_t  LeptonsPool::TotalEnergy(){
	Double_t total_energy = 0.;
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		if ( it->second.IsBind() ) 
			total_energy += it->second.Momentum().E();
	}	
	return total_energy;
}
//_________________________________________________________________________________________________	
void  LeptonsPool::AllKeys( Int_t* arr_keys ){
	Int_t i = 0;
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		arr_keys[i] = it->first;
	}
}
//_________________________________________________________________________________________________										
std::vector<int>  LeptonsPool::GetKeys(){
	std::vector<int> v;
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		v.push_back(it->first);
	}
	return v;
}
//_________________________________________________________________________________________________										
TTree*  LeptonsPool::GetLeptonPoolTree(){
	typedef struct {
		Int_t ix,pid; 
		bool Bnd;
	} PrtD;
	PrtD prt;
	TVector3 POS;
	TLorentzVector PL;
	TTree *T = new TTree("Particles","Nucleus Particles");
	T->Branch("Descrp",&prt,"IX/I:PID:Bind/O");
	T->Branch("Pos","TVector3",&POS);
	T->Branch("P","TLorentzVector",&PL);
	for (int i = 0; i < this->num_leptons; i++){
		prt.ix = i;
		prt.pid = this->leptons[i].PdgId();
		prt.Bnd = this->leptons[i].IsBind();
		POS = this->leptons[i].Position();
		PL = this->leptons[i].Momentum();
		T->Fill();
	}
	return T;
}
//_________________________________________________________________________________________________
Lep_Struct  LeptonsPool::GetLeptonsStruct(){
	Lep_Struct LEP;
	LEP.NM = this->leptons.size();
	int i = 0;
	for ( LeptonsMap::iterator it = leptons.begin(); it != leptons.end(); it ++ ) {
		LEP.Lep_KEY[i] = it->first ;
		LEP.Lep_PID[i] = it->second.PdgId();
		LEP.Lep_IsB[i] = it->second.IsBind();
		LEP.Lep_X[i] = it->second.Position().X();
		LEP.Lep_Y[i] = it->second.Position().Y();
		LEP.Lep_Z[i] = it->second.Position().Z();

		LEP.Lep_P[i] = it->second.Momentum().Vect().Mag();
		LEP.Lep_PX[i] = it->second.Momentum().Px();
		LEP.Lep_PY[i] = it->second.Momentum().Py();
		LEP.Lep_PZ[i] = it->second.Momentum().Pz();
		LEP.Lep_E[i] = it->second.Momentum().E();
		LEP.Lep_K[i] = it->second.TKinetic();
		if (it != leptons.end())i++;
	}
	return LEP;

}
