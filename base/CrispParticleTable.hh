/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __CrispParticleTable_pHH
#define __CrispParticleTable_pHH

#include "TDatabasePDG.h"
#include "TObject.h"
#include "TGraphErrors.h"
#include "TParticlePDG.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TMath.h"
#include <iostream>
#include <fstream>
//using namespace std;
/// Class containing the definitions and methods of the implemented particles.
///
/// see 'crisp_table.txt' 
///
/// use form:
/// CPT *cp = CPT::Instance();
/// cp->DataBasePDG()->ReadPDGTable("crisp_table.txt");
//___________________________________________________________________________________________										
class CrispParticleTable: public TObject{
public:
	// IDs
	static const int 	proton_ID,  /// proton id
				neutron_ID; /// neutron id
	static const int photon_ID; /// photon id
	static const int electron_ID; /// electron id
	// pi mesons  
	static const int 	pion_m_ID, /// meson pi⁺ id
				pion_0_ID, /// meson pi⁰ id
				pion_p_ID; /// meson pi⁻ id 
	// k mesons
	static const int 	kaon_p_ID,  /// meson k⁺ id
				kaon_m_ID,  /// meson k⁻ id
				kaon_L0_ID, /// meson kL⁺ (long) id
				kaon_S0_ID; /// meson kS⁺ (short) id
	//vector mesons
	static const int 	rho_0_ID, /// meson rho⁰ id
				rho_p_ID, /// meson rho⁺ id
				rho_m_ID; /// meson rho⁻ id
	static const int 	omega_ID; /// omega meson id
	static const int 	phi_ID; /// phi meson id
	//Charmed mesons
	static const int 	J_Psi_ID; /// J_Psi meson id
	static const int 	D_0_ID, /// meson D⁰ idx
				Dbar_0_ID, /// meson Dbar⁰ idx, antiparticle of D⁰
				D_p_ID, /// meson D⁺ idx
				D_m_ID, /// meson D⁻ idx      , antiparticle of D⁺
				Dast_p_ID, /// meson D*⁺ idx
				Dast_m_ID, /// meson D*⁻ idx  , antiparticle of D*⁺
			 	Dast_0_ID, /// meson D*⁰ idx
				Dastbar_0_ID; /// meson Dbar*⁰bar idx   , antiparticle of D*⁰
	// Delta(1232) P33
	static const int 	p33_m_ID,  ///  id
				p33_0_ID,  /// id
				p33_p_ID,  /// id
				p33_pp_ID; /// id
	// Delta(1700) D33
	static const int 	delta1700_m_ID,  ///  id
				delta1700_0_ID,  /// id
				delta1700_p_ID,  /// id
				delta1700_pp_ID; /// id
	// Delta(1950) F37
	static const int 	f37_m_ID,  /// id
				f37_0_ID,  /// id
				f37_p_ID,  /// id
				f37_pp_ID; /// id
	// N(1440)     P11
	static const int 	p11_0_ID, /// id
				p11_p_ID; /// id
	// N(1520)     D13
	static const int 	d13_0_ID,  /// id
				d13_p_ID ; /// id
	// N(1535)     S11
	static const int 	s11_0_ID, /// id
				s11_p_ID; /// id
	// N(1680)     F15
	static const int 	f15_0_ID, /// id
				f15_p_ID; /// id
	// Lambda 0
	static const int 	lambda_ID, /// id
			 	lambdac_p_ID, /// id
			 	lambdac_m_ID; /// id

// Neutrinos
	static const int	neutrino_m_ID; //id muonic 
//				neutrino_e_ID,	// id electronic
//				neutrino_t_ID;	// id tau note missin antineutrinosso far added when needed

// muons
      static const int	muon_m_ID; // mu- 	
	
//_____________________________________________________________________________________________										

	static inline bool IsNucleon(int id) { return ( (id == proton_ID) || (id == neutron_ID) );}
	static inline bool IsP33(int id){ return ( (id == p33_m_ID) || (id == p33_0_ID) || (id == p33_p_ID) || (id == p33_pp_ID) );}
	static inline bool IsDelta1700(int id){ return ( (id == delta1700_m_ID) || (id == delta1700_0_ID) || (id == delta1700_p_ID) || (id == delta1700_pp_ID) );}
	static inline bool IsF37(int id){ return ( (id == f37_m_ID) || (id == f37_0_ID) || (id == f37_p_ID) || (id == f37_pp_ID) );}  
	static inline bool IsP11(int id){ return ( ( id == p11_0_ID ) || ( id == p11_p_ID ) ); }
	static inline bool IsS11(int id){ return ( ( id == s11_0_ID ) || ( id == s11_p_ID ) ); }
	static inline bool IsD13(int id){ return ( ( id == d13_0_ID ) || ( id == d13_p_ID ) ); }
	static inline bool IsF15(int id){ return ( (id == f15_0_ID) || (id == f15_p_ID) );}
	static inline bool IsNStar(int id){ return ( IsP11(id) || IsD13(id) || IsF15(id) || IsS11(id) );} 
	static inline bool IsPion(int id){ return ( (id == pion_0_ID) || (id == pion_p_ID) || (id == pion_m_ID) );}
	static inline bool IsRho(int id){ return ( (id ==rho_0_ID) || (id == rho_p_ID) || (id == rho_m_ID) );}
	static inline bool IsOmega(int id){ return (id ==omega_ID) ;}
	static inline bool IsPhi(int id){ return (id ==phi_ID) ;}
	static inline bool IsJ_Psi(int id){ return (id ==J_Psi_ID) ;}
	static inline bool IsD(int id){ return ( (id ==D_0_ID)||(id ==Dbar_0_ID) ||(id ==D_p_ID) ||(id ==D_m_ID) );}
	static inline bool IsDast(int id){ return ( (id ==Dast_0_ID)||(id ==Dastbar_0_ID) ||(id ==Dast_p_ID) ||(id ==Dast_m_ID) );}
	static inline bool IsKaon(int id){ return ( (id == kaon_p_ID) || (id == kaon_m_ID) || (id == kaon_L0_ID) || (id == kaon_S0_ID) );}  
	static inline bool IsHyperon(int id){ return ( (id == lambda_ID)||(id == lambdac_m_ID) || ( id == lambdac_p_ID ) );}
	static inline bool IsHyperonLam(int id){ return ( id == lambda_ID);}
	static inline bool IsHyperonLamc(int id){ return ( (id == lambdac_m_ID) || ( id == lambdac_p_ID ) );}
	static inline bool IsImplRessonance( int rss_id ) {
   
		bool is_Delta1700 = IsDelta1700(rss_id);
		bool is_p33 = IsP33(rss_id);
		bool is_f37 = IsF37(rss_id);
		bool is_nstar = IsP11(rss_id) || IsD13(rss_id) ||  IsF15(rss_id) || IsS11(rss_id);       
		return ( is_p33 || is_Delta1700 || is_f37 || is_nstar );
	}     
	
	
	static inline bool IsLepton(int pdgId){
		const Int_t electron = 11;
		const Int_t mu = 13;
		const Int_t mu_p = -13;
		const Int_t nu_mu = 14;
		const Int_t nu_mu_bar = -14;
		return ( pdgId == electron || pdgId == mu || pdgId == mu_p || pdgId == nu_mu || pdgId == nu_mu_bar ); 
	}
	static inline bool IsMeson(int id){
		
		return ( IsPion(id) || IsRho(id) || IsOmega(id) || IsPhi(id) || IsJ_Psi(id) || IsD(id) || IsDast(id) || IsKaon(id) );
	}
	static inline bool IsNeutrino(int id) {
		 return (id == neutrino_m_ID);
	}

static inline bool IsMesonResonance(int id){
		
		return ( IsRho(id) || IsOmega(id) || IsPhi(id) || IsJ_Psi(id) );
	}


//____________________________________________________________________________________________										

	// Effective factor for binded particles ...
	static const double effn;     // effective factor for nucleons
	static const double effd;     // effective factor for delta
	static const double effh;     // for hyperon
	static const double effns;    // for N*
	static const Double_t r0;     // r0 correponds to the nucleon width.
	// the proton and neutron mass ... just a shorthand to improve the access to HepPDT.
	static const double p_mass;
	static const double n_mass;
	static const double pion_m_mass;
	static const double pion_0_mass;
	static const double pion_p_mass;  
	static const double rho_0_mass;
	static const double rho_p_mass;
	static const double rho_m_mass;
	static const double kaon_p_mass;
	static const double kaon_m_mass;
	static const double kaon_0_mass;
	static const double omega_mass ;
	static const double phi_mass;
	static const double J_Psi_mass;
	static const double sigma_mass;
	static const double D_0_mass;
	static const double D_p_mass;
	static const double D_m_mass;
	static const double Dast_0_mass;
	static const double Dast_p_mass;
	static const double Dast_m_mass;
	static const double lambda_mass;
	static const double lambdac_m_mass;
	static const double lambdac_p_mass;

	static CrispParticleTable* Instance();
//_____________________________________________________________________________________________										
  
	static TDatabasePDG* DatabasePDG() { return TDatabasePDG::Instance(); }
//_____________________________________________________________________________________________										

	TParticlePDG* ParticlePDG(int pid) { return pdg->GetParticle(pid); }
//_____________________________________________________________________________________________										

	TParticlePDG* ParticlePDG(char* name) { return pdg->GetParticle(name); }

	inline TNtuple* GetPhotoCS(){return TN_PhotoCS;}
	inline TNtuple* GetNucMesCS(){return TN_NucMesCS;}
	inline TGraph* GetT_rho(){return T_rho;}
	inline TGraph* GetT_omg(){return T_omg;}
	inline TGraph* GetT_phi(){return T_phi;}
	inline TGraph* GetT_J_Psi(){return T_J_Psi;}
	inline TGraph *GetT_PhotonResidN(){return T_PhotonResidN;}
	inline TGraph *GetT_PhotonResidP(){return T_PhotonResidP;}

	inline TGraph* GetT_Npion_Nomega(){return T_MesNuc[0];}
	inline TGraph* GetT_Nomega_Npion(){return T_MesNuc[1];}
	inline TGraph* GetT_Nomega_Nrho(){return T_MesNuc[2];}
	inline TGraph* GetT_Nrho_Nomega(){return T_MesNuc[3];}
	inline TGraph* GetT_Nomega_elast(){return T_MesNuc[4];}
	inline TGraph* GetT_Nomega_N2pion(){return T_MesNuc[5];}
	inline TGraph* GetT_Npion_Nomega1(){return T_MesNuc[6];}
	inline TGraph* GetT_Npion_Nphi(){return T_MesNuc[7];}
	inline TGraph* GetT_Npion_Nrho(){return T_MesNuc[8];}
	inline TGraph* GetT_Nomega_Npion1(){return T_MesNuc[9];}
	inline TGraph* GetT_Nphi_Npion(){return T_MesNuc[10];}
	inline TGraph* GetT_Nrho_Npion(){return T_MesNuc[11];}
	inline TGraph* GetT_NJPsi_ND(){return T_MesNuc[12];}
	inline TGraph* GetT_NJPsi_NDast(){return T_MesNuc[13];}
	inline TGraph* GetT_NJPsi_NDDbar(){return T_MesNuc[14];}
	inline ofstream &Get_Data(){return data_casc;}
//_____________________________________________________________________________________________										
  
	CrispParticleTable();
//_____________________________________________________________________________________________										
	~CrispParticleTable(){
		if(_instance) delete _instance;
		if(TN_PhotoCS) delete TN_PhotoCS;
		if(TN_NucMesCS) delete TN_NucMesCS;
		if(F_PhotoCS) delete F_PhotoCS;
		if(F_NucMesCS) delete F_NucMesCS;
		if(T_rho) delete  T_rho;
		if(T_omg) delete  T_omg;
		if(T_phi) delete  T_phi;
		if(T_J_Psi) delete  T_J_Psi;
		for (int i = 0; i < 15; i++) if (T_MesNuc[i]) delete T_MesNuc[i];
		data_casc.close();
	}
//_____________________________________________________________________________________										  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(CrispParticleTable, 2)  //A track segment
#endif // CRISP_SKIP_ROOTDICT
private:
	static CrispParticleTable* _instance;
	ofstream data_casc;
	TDatabasePDG* pdg;
	TFile* F_PhotoCS;
	TFile* F_NucMesCS;
	TNtuple *TN_PhotoCS;
	TNtuple *TN_NucMesCS;
  // ClassDef(CrispParticleTable,2);
//TGraph
	TGraph *T_rho;
	TGraph *T_omg;
	TGraph *T_phi;
	TGraph *T_J_Psi;
	TGraph *T_PhotonResidN;
	TGraph *T_PhotonResidP;
	TGraph *T_MesNuc[15]; 
};
// a shorthand...
typedef CrispParticleTable CPT;
#endif
