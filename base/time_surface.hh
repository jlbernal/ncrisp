/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __time_surface_pHH
#define __time_surface_pHH

#include <iostream>
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TMath.h"
#include "base_defs.hh"
#include "BasicException.hh"
#include "ParticleDynamics.hh"
#include "NucleusDynamics.hh"

/// colision time with a nucleus surface
///
/// @param p : particle 
/// @param nuc : nucleus 
/// @return colision time with a nucleus surface						

double time_surface(ParticleDynamics& p, NucleusDynamics& nuc);

//_________________________________________________________________________________________________	

/// Calculate penetrability
///
/// @param p : particle colliding with the well
/// @param rnt : nucleus' radius
/// @param nuc_total_charge : nucleus charge
/// @param nuc_total_mass : nucleus mass
/// @return penetrability

double penetrability( ParticleDynamics& p, double rnt, int nuc_total_charge, double nuc_total_mass );

//_________________________________________________________________________________________________	

/// Calculate penetrability
///
/// @param p : particle colliding with the well
/// @param rnt : nucleus' radius
/// @param nuc_total_charge : nucleus charge
/// @return penetrability

double penetrability( ParticleDynamics& p, double rnt, double nuc_total_charge);

//_________________________________________________________________________________________________	

/// updates the particle position
///
/// @param p : particle referance to be updated
/// @param time : time interval							

void particles_walk_thru(Dynamics& p, double time );

//_________________________________________________________________________________________________	

/// method to calculate the possibility of particle escape when colliding with the surface of the nucleus. 
/// if escape return true else update particle position(specular position).
///
/// @param p1 : particle 
/// @param nuc_rnt : nucleus radius
/// @param total_charge : total charge of nucleus
/// @param total_mass : total mass of nucleus 
/// @param potential : nuclear potential 
/// @param iTime : time interval of process 
/// @return check test									

bool it_was_even_so( ParticleDynamics& p1, double nuc_rnt, double total_charge, double total_mass, double potential, double well, double iTime );

//_________________________________________________________________________________________________	

/// method to calculate the possibility of particle escape when colliding with the surface of the nucleus. 
/// if escape return true else update particle position(specular position).
///
/// @param p1 : particle 
/// @param nuc_rnt : nucleus' radius
/// @param total_charge : total charge of nucleus
/// @param potential : nuclear potential 
/// @param iTime : time interval of process 
/// @return check test									

bool it_was_even_so( ParticleDynamics& p1, double nuc_rnt, double total_charge, double potential, double well, double iTime );

#endif
