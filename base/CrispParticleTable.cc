/* ================================================================================
 * 
 * 	Copyright 2008, 2014
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "CrispParticleTable.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(CrispParticleTable);
#endif // CRISP_SKIP_ROOTDICT

CrispParticleTable* CrispParticleTable::_instance = 0;

const int CrispParticleTable::photon_ID   = 22;
const int CrispParticleTable::electron_ID = 11;

const int CrispParticleTable::pion_m_ID  = -211;
const int CrispParticleTable::pion_0_ID  = 111;
const int CrispParticleTable::pion_p_ID  = 211;

const int CrispParticleTable::kaon_p_ID  = 321;
const int CrispParticleTable::kaon_m_ID  = -321;
const int CrispParticleTable::kaon_L0_ID = 130; 
const int CrispParticleTable::kaon_S0_ID = 310;  
const int CrispParticleTable::omega_ID   = 223;
const int CrispParticleTable::phi_ID     = 333;
const int CrispParticleTable::J_Psi_ID   = 443;


const int CrispParticleTable::D_0_ID		= 421;
const int CrispParticleTable::Dbar_0_ID		= -421;
const int CrispParticleTable::D_p_ID		= 411;
const int CrispParticleTable::D_m_ID		= -411;
const int CrispParticleTable::Dast_p_ID		= 413;
const int CrispParticleTable::Dast_m_ID		= -413;
const int CrispParticleTable::Dast_0_ID		= 423;
const int CrispParticleTable::Dastbar_0_ID	= -423;


const int CrispParticleTable::rho_0_ID   = 113;
const int CrispParticleTable::rho_p_ID   = 213;
const int CrispParticleTable::rho_m_ID   = -213;

const int CrispParticleTable::proton_ID  = 2212;
const int CrispParticleTable::neutron_ID = 2112;

//Delta(1232) P33
const int CrispParticleTable::p33_m_ID = 1114;
const int CrispParticleTable::p33_0_ID = 2114;  
const int CrispParticleTable::p33_p_ID = 2214;
const int CrispParticleTable::p33_pp_ID = 2224;

//Delta(1700) D33
const int CrispParticleTable::delta1700_m_ID = 121114;
const int CrispParticleTable::delta1700_0_ID = 122114;  
const int CrispParticleTable::delta1700_p_ID = 122214;
const int CrispParticleTable::delta1700_pp_ID = 122224;

//Delta(1950) f37
const int CrispParticleTable::f37_m_ID = 1118;
const int CrispParticleTable::f37_0_ID = 2118;  
const int CrispParticleTable::f37_p_ID = 2218;
const int CrispParticleTable::f37_pp_ID = 2228;

//N(1440) P11
const int CrispParticleTable::p11_0_ID = 12112;
const int CrispParticleTable::p11_p_ID = 12212;

//N(1535) S11
const int CrispParticleTable::s11_0_ID = 22112;
const int CrispParticleTable::s11_p_ID = 22212;
  
// N(1520) D13
const int CrispParticleTable::d13_0_ID = 1214;
const int CrispParticleTable::d13_p_ID = 2124;
  
// N(1680) F15
const int CrispParticleTable::f15_0_ID = 12116;
const int CrispParticleTable::f15_p_ID = 12216;

// Hyperon 
const int CrispParticleTable::lambda_ID = 3122;  
const int CrispParticleTable::lambdac_p_ID = 4122;  
const int CrispParticleTable::lambdac_m_ID = -4122;  

//Neutrinos 
const int CrispParticleTable::neutrino_m_ID = 14; 

// Muons
const int CrispParticleTable::muon_m_ID = 13;  	 


const Double_t CrispParticleTable::r0 = 1.18;  // nucleon width in Fm.


const double CrispParticleTable::effn =  .950; // effective factor for nucleons
const double CrispParticleTable::effd =  .950; // effective factor for delta
const double CrispParticleTable::effh =  .950; // for hyperon
const double CrispParticleTable::effns = .950; // for N*

const double CrispParticleTable::p_mass = 938.2723;
const double CrispParticleTable::n_mass = 939.5656;
const double CrispParticleTable::pion_m_mass = 139.57018;
const double CrispParticleTable::pion_0_mass = 134.9766;
const double CrispParticleTable::pion_p_mass = 139.57018;  
const double CrispParticleTable::rho_0_mass = 775.49;
const double CrispParticleTable::rho_p_mass = 770.4;
const double CrispParticleTable::rho_m_mass = 775.4;
const double CrispParticleTable::kaon_p_mass = 493.667;
const double CrispParticleTable::kaon_m_mass = 493.667;
const double CrispParticleTable::kaon_0_mass = 497.648;
const double CrispParticleTable::omega_mass = 782.65;
const double CrispParticleTable::phi_mass = 1019.445;
const double CrispParticleTable::J_Psi_mass = 3096.916;
const double CrispParticleTable::sigma_mass = 550.;
const double CrispParticleTable::D_0_mass = 1864.6;
const double CrispParticleTable::D_p_mass = 1869.4;
const double CrispParticleTable::D_m_mass = 1869.4;
const double CrispParticleTable::Dast_0_mass = 2006.7;
const double CrispParticleTable::Dast_p_mass = 2010;
const double CrispParticleTable::Dast_m_mass = 2010;
const double CrispParticleTable::lambda_mass =  1115.68;
const double CrispParticleTable::lambdac_m_mass = 2286.46;
const double CrispParticleTable::lambdac_p_mass = 2286.46;

CrispParticleTable* CrispParticleTable::Instance(){
  
	if ( _instance == 0 ) {
		_instance = new CrispParticleTable();
		std::cout << " instance was created" << std::endl;
	}  
	return _instance;
}

CrispParticleTable::CrispParticleTable(){
	if ( _instance == 0 )
		_instance = this;
	else { 
		delete _instance;
		_instance = this;
	}
	pdg   = TDatabasePDG::Instance();
	F_PhotoCS = new TFile("data/VM_TotalCS_less_2000GeV_NW67000_Nt500.root");
// 	F_PhotoCS = new TFile("data/VM_TotalCS_less_150GeV.root");
	F_NucMesCS = new TFile("data/TotalCS_medium_less_100GeV.root");
	TN_PhotoCS = (TNtuple*)F_PhotoCS->Get("TN");
	TN_NucMesCS = (TNtuple*)F_NucMesCS->Get("TN");
	TN_NucMesCS->SetName("TN_N");
	//data_casc.open("cascade.out");
	Float_t Q_2, W, rho, omg, phi, jpsi; //Q_2:W:Rho:Omega:Phi:J_Psi
	TN_PhotoCS->SetBranchAddress("Rho",&rho);
	TN_PhotoCS->SetBranchAddress("Omega",&omg);
	TN_PhotoCS->SetBranchAddress("Phi",&phi);
	TN_PhotoCS->SetBranchAddress("J_Psi",&jpsi);

	const int Npoints = 67000;
	const double PhDQ = 0.5;
	const double PhDW = 1999./Npoints;
	Q_2 = 0;
	double Q = (int)floor(Q_2/PhDQ + 0.5);
	//Total cross section calculation parameters
	const Double_t bp1 = 91., bp2 = 71.4; 
	const Double_t bn1 = 87., bn2 = 65.;
	T_rho =  new TGraph();
	T_omg =  new TGraph();
	T_phi =  new TGraph();
	T_J_Psi =  new TGraph();
	T_PhotonResidP =  new TGraph();
	T_PhotonResidN =  new TGraph();
	for (int i = 0; i < Npoints; i++){
		W = 1 + i*PhDW;
		double NW = (int)floor((W - 1)/PhDW); 
		int idx = Q*5000 + NW;
		TN_PhotoCS->GetEntry(idx);
		if (W < 1.745){	rho = 0; omg = 0; }		
		if (W < 2.0132) phi = 0;
		if (W < 4.0396) jpsi = 0;
		T_rho->SetPoint(i,W,(double)rho); //milibar
		T_omg->SetPoint(i,W,(double)omg); //milibar
		T_phi->SetPoint(i,W,(double)phi); //milibar
		T_J_Psi->SetPoint(i,W,(double)jpsi); //milibar

		Double_t Ep = W*W/(.9382723*.95)/2 - (.9382723*.95)/2;
		Double_t En = W*W/(.9395656*.95)/2 - (.9395656*.95)/2;
		Double_t xp = -2. * ( Ep - 0.139 );      
		Double_t xn = -2. * ( En - 0.139 );      
		Double_t TotalCS_N = ( bn1 + bn2 / TMath::Sqrt(En) ) * ( 1. - exp(xn) );  //total cross section for neutrons  //microbar
		Double_t TotalCS_P = ( bp1 + bp2 / TMath::Sqrt(Ep) ) * ( 1. - exp(xp) );  //total cross section for protons   //microbar
		T_PhotonResidN->SetPoint(i,W, TotalCS_N - (rho + omg + phi + jpsi)*1e3 );   	//total cross section for neutrons minus all vector mesons cross section
		T_PhotonResidP->SetPoint(i,W, TotalCS_P - (rho + omg + phi + jpsi)*1e3 );   	//total cross section for protons minus all vector mesons cross section
	}

	for (int i = 0; i < 15; i++) T_MesNuc[i] = new TGraph();
	Float_t s,Npion_Nomega, Nomega_Npion, Nomega_Nrho, Nrho_Nomega, Nomega_elast, Nomega_N2pion, Npion_Nomega1, Npion_Nphi, Npion_Nrho, Nomega_Npion1, Nphi_Npion, Nrho_Npion, NJPsi_ND, NJPsi_NDast, NJPsi_NDDbar; 
	TN_NucMesCS->SetBranchAddress("Npion_Nomega",&Npion_Nomega);
	TN_NucMesCS->SetBranchAddress("Nomega_Npion",&Nomega_Npion);
	TN_NucMesCS->SetBranchAddress("Nomega_Nrho",&Nomega_Nrho);
	TN_NucMesCS->SetBranchAddress("Nrho_Nomega",&Nrho_Nomega);
	TN_NucMesCS->SetBranchAddress("Nomega_elast",&Nomega_elast);
	TN_NucMesCS->SetBranchAddress("Nomega_N2pion",&Nomega_N2pion);
	TN_NucMesCS->SetBranchAddress("Npion_Nomega1",&Npion_Nomega1);
	TN_NucMesCS->SetBranchAddress("Npion_Nphi",&Npion_Nphi);
	TN_NucMesCS->SetBranchAddress("Npion_Nrho",&Npion_Nrho);
	TN_NucMesCS->SetBranchAddress("Nomega_Npion1",&Nomega_Npion1);
	TN_NucMesCS->SetBranchAddress("Nphi_Npion",&Nphi_Npion);
	TN_NucMesCS->SetBranchAddress("Nrho_Npion",&Nrho_Npion);
	TN_NucMesCS->SetBranchAddress("NJPsi_ND",&NJPsi_ND);
	TN_NucMesCS->SetBranchAddress("NJPsi_NDast",&NJPsi_NDast);
	TN_NucMesCS->SetBranchAddress("NJPsi_NDDbar",&NJPsi_NDDbar);
// meson nucleon cross section filling
	const double PhDs = 100./3000.;
	for (int i = 0; i < 3000; i++){
		s = i*PhDs;
		double Ns = (int)floor((s)/PhDs); 
		TN_NucMesCS->GetEntry(Ns);
		if (s < 2.97345) Npion_Nomega = 0;
		if (s < 2.96603) Nomega_Npion = 0;
		if (s < 2.96902) Nomega_Nrho = 0;
		if (s < 2.96843) Nrho_Nomega = 0;
		if (s < 2.96603) Nomega_elast = 0;
		if (s < 2.96615) Nomega_N2pion = 0;
		if (s < 2.96603) Npion_Nomega1 = 0;
		if (s < 3.8434)  Npion_Nphi = 0;
		if (s < 2.94142) Npion_Nrho = 0;
		if (s < 2.96624) Nomega_Npion1 = 0;
		if (s < 2.8372)  Nphi_Npion = 0;
		if (s < 2.94163) Nrho_Npion = 0;
		if (s < 17.3) NJPsi_ND = 0;
		if (s < 18.5) NJPsi_NDast = 0;
		if (s < 18.5) NJPsi_NDDbar = 0;

		T_MesNuc[0]->SetPoint(i,s,Npion_Nomega   );
		T_MesNuc[1]->SetPoint(i,s,Nomega_Npion   );
		T_MesNuc[2]->SetPoint(i,s,Nomega_Nrho    );
		T_MesNuc[3]->SetPoint(i,s,Nrho_Nomega    );
		T_MesNuc[4]->SetPoint(i,s,Nomega_elast   );
		T_MesNuc[5]->SetPoint(i,s,Nomega_N2pion  );
		T_MesNuc[6]->SetPoint(i,s,Npion_Nomega1  );
		T_MesNuc[7]->SetPoint(i,s,Npion_Nphi     );
		T_MesNuc[8]->SetPoint(i,s,Npion_Nrho     );
		T_MesNuc[9]->SetPoint(i,s,Nomega_Npion1  );
		T_MesNuc[10]->SetPoint(i,s,Nphi_Npion    );
		T_MesNuc[11]->SetPoint(i,s,Nrho_Npion    );
		T_MesNuc[12]->SetPoint(i,s,NJPsi_ND	 );
		T_MesNuc[13]->SetPoint(i,s,NJPsi_NDast   );
		T_MesNuc[14]->SetPoint(i,s,NJPsi_NDDbar  );
	}
}
