/* ================================================================================
 * 
 * 	Copyright 2008, 2011, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "NucleusDynamics.hh"

BindingEnergyTable tableB;

//to use table of binding energies instead of mass formula, change here
//NOTE!!!: the table contains also results of fitting, not only experimental data
bool table(false);

// internal methods

void initNucleonsMomentum( NucleusDynamics* nuc, Int_t particle_id){
	Int_t 	A = nuc->GetA(), Z = nuc->GetZ();
	Int_t 	start_idx = 0, N = Z;
	Double_t 	dp = 0;
	TVector3 	*v3 = 0;
  
	if ( particle_id == CPT::proton_ID ) {
		start_idx = 0; 
		N = Z;
		v3 = new TVector3[Z];
		nuc->ProtonFermiLevels().DoInitConfig(v3);
		dp = nuc->ProtonFermiLevels().GetCellSize();
	} 
	else if ( particle_id == CPT::neutron_ID) {
		start_idx = Z; 
		N = A - Z;
		v3 = new TVector3[A - Z];
		nuc->NeutronFermiLevels().DoInitConfig(v3);
		dp = nuc->NeutronFermiLevels().GetCellSize();    
	}
	TVector3 p3;  
	for ( Int_t i =0 ; i < N; i++ ){
		p3.SetX( ( gRandom->Uniform() > .5 ) ? dp * v3[i].X() : -( dp * v3[i].X() ) );
		p3.SetY( ( gRandom->Uniform() > .5 ) ? dp * v3[i].Y() : -( dp * v3[i].Y() ) );
		p3.SetZ( ( gRandom->Uniform() > .5 ) ? dp * v3[i].Z() : -( dp * v3[i].Z() ) );
		(*nuc)[start_idx + i].SetMomentum(p3);
	}  
	delete []v3;
}
//_________________________________________________________________________________________________				

void randomizeVector( ParticleDynamics* vec, Int_t A ) { 
	for ( Int_t i = 0; i < A; i++ ) {
		Int_t a = (Int_t)(gRandom->Uniform() * (double)(A - 1));
		Int_t b = (Int_t)(gRandom->Uniform() * (double)(A - 1));
		if ( a != b)
			std::swap(vec[a], vec[b]);
	} 
}
//_________________________________________________________________________________________________					

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(NucleusDynamics);
#endif // CRISP_SKIP_ROOTDICT
//_________________________________________________________________________________________________

NucleusDynamics::~NucleusDynamics(){
	delete []nucleons;
}
//_________________________________________________________________________________________________										

NucleusDynamics::NucleusDynamics(): Dynamics(), plevels(CPT::proton_ID, 2, 1), nlevels(CPT::neutron_ID, 2, 1){
	A = 2; Z = 1;  
	nucleons = 0;
	Rot_Kinetic_Energy = 0.;
	Set_InitE(0.);
	DoInitConfig();
}
//_________________________________________________________________________________________________  

NucleusDynamics::NucleusDynamics(Int_t atomicMass, Int_t atomicNumber) :  // The constructor
									Dynamics(), 
									plevels(CPT::proton_ID, atomicMass, atomicNumber), 
									nlevels(CPT::neutron_ID, atomicMass, atomicNumber){
	A = atomicMass; 
	Z = atomicNumber;   
	nucleons = 0;
	Rot_Kinetic_Energy = 0.;
	Set_InitE(0.);
	DoInitConfig(); 
}
//_________________________________________________________________________________________________	

void NucleusDynamics::BindAllNucleons(){
	for ( Int_t i = 0; i < A; i++) { 
		nucleons[i].SetMass( nucleons[i].GetInvariantMass() * CPT::effn );
		nucleons[i].BindIt(true);
	}
}
//_________________________________________________________________________________________________ 

/*
 * NOTE:  
 * The plevels object keeps the protons in a regular C++ vector (index starting at 0, 
 * no matter the position of the proton well relative to neutrons).
 * 
 * The proton momentum must be taken back to its original value (before the well adjustment)
 * in order to allow its location.
 */
double NucleusDynamics::GetProtonMomentum(TVector3 p){
 
	double pf = p.Mag();
	double kfn = nlevels.GetFermiMomentum();
	double kfp = plevels.GetFermiMomentum();

	if ( fabs ( pf - ( kfn - kfp ) ) < 1.E-7 )
		return 0.;
	else { 
		double p2 = TMath::Power(pf,2);
		double x = TMath::Sqrt( p2 + TMath::Power( kfn - kfp, 2 ) + 2. * ( kfn - kfp ) * pf ) / pf;
		TVector3 v( p.X()/x, p.Y()/x, p.Z()/x);
		return v.Mag();//abs(pf - abs( kfn - kfp ));
	} 
}
//_________________________________________________________________________________________________ 	

void NucleusDynamics::DoInitConfig(){ // init the nucleus energy/momentum configuration 
 
	rnt = CPT::r0 * TMath::Power((Double_t)A, 1./3.);  
	const Int_t max_nucleons = 400;
	if ( nucleons != 0 ) 
		delete []nucleons;
  
	nucleons = new ParticleDynamics[max_nucleons];
	for ( Int_t i = 0; i < Z; i++ ) { 
		nucleons[i] = ParticleDynamics(CPT::proton_ID);    
		nucleons[i].SetMass( CPT::p_mass * CPT::effn );
		nucleons[i].BindIt(true);    
	}
	for ( Int_t i = Z; i < A ; i++ ) {
		nucleons[i] = ParticleDynamics(CPT::neutron_ID);  
		nucleons[i].SetMass( CPT::n_mass * CPT::effn );
		nucleons[i].BindIt(true);      
	} 
	initNucleonsMomentum(this, CPT::proton_ID);
	initNucleonsMomentum(this, CPT::neutron_ID);
	for (Int_t i = 0; i < A; i++) {   
		// Configuration for the target nucleus
		double phi = TMath::TwoPi() * gRandom->Uniform();
		double cos_a = 1.0 - 2.0 * gRandom->Uniform();
		double sin_a = TMath::Sqrt(1.0 - TMath::Power(cos_a,2));
		double ri = rnt * TMath::Power(gRandom->Uniform(),1./3.);
		double r_xy = ri * sin_a;       
		TVector3 r ( r_xy * TMath::Cos(phi), r_xy * TMath::Sin(phi), ri  * cos_a );       
		nucleons[i].SetPosition(r);      
	}     
	// taking by reference the neutron well to adjust the
	// minimal energy for the proton well.
	double kfn = nlevels.GetFermiMomentum();
	double kfp = plevels.GetFermiMomentum();  
	for ( Int_t i = 0; i < Z; i++) {
		double px = nucleons[i].Momentum().Px();
		double py = nucleons[i].Momentum().Py();
		double pz = nucleons[i].Momentum().Pz(); 
		double m2 = nucleons[i].Momentum().Vect().Mag2();    
		double m = TMath::Sqrt(m2);
		double f = ( m + kfn - kfp) / m;    
    
		TVector3 k( px * f, py * f, pz * f ); 
		nucleons[i].SetMomentum(k);          
	}  
	randomizeVector(nucleons, A); 
	
	// setting nucleus momentum and angular momentum
	TLorentzVector p;
	for ( Int_t i = 0; i < A; i ++ ) 
		p += nucleons[i].Momentum();
	SetMomentum(p);  
	
	SetAngularMomentum( TVector3(0,0,0));
	
	totalSysEn = TotalSystemEnergy();
}
//_________________________________________________________________________________________________

void NucleusDynamics::DoInitConfig(Int_t A_num, Int_t Z_num){
	this->A = A_num;
	this->Z = Z_num;
	this->DoInitConfig();
}
//_________________________________________________________________________________________________

double NucleusDynamics::GetMass() const{

	double total_mass = 0.;
	for ( Int_t i = 0; i < A; i++ )
		if ( nucleons[i].IsBind() )
			total_mass += nucleons[i].GetMass();
	return total_mass;
}
//_________________________________________________________________________________________________

double NucleusDynamics::TotalCharge(){

	double total_charge = 0.;  
	for ( Int_t i = 0; i < A; i++ )
		if ( nucleons[i].IsBind() )
			total_charge += nucleons[i].GetParticleData()->Charge()/3.; //ROOT gives the charge in units of |e|/3
  
	return total_charge;
}
//_________________________________________________________________________________________________


double NucleusDynamics::TotalSystemEnergy(){

	double totalEn = 0.;
 	for ( Int_t i = 0; i < A; i++ ){
		if ( nucleons[i].IsBind() )
			totalEn += nucleons[i].Momentum().E();
	}	
	return totalEn;
}

//_________________________________________________________________________________________________


double NucleusDynamics::GetBasicEnergy(){
  for(int i=0;i<A;i++){
    
  }
  return 0;
}


//_________________________________________________________________________________________________


const double NucleusDynamics::separation_Energy =  8.00;
//_________________________________________________________________________________________________

/* 
 * A fixed separation_Energy is used in order to consider nuclear separation energy only
 * and include the coulomb barrier explicitly
 */
double NucleusDynamics::NuclearPotential( Int_t particle_id ){
 
	CPT* cpt = CPT::Instance();
	
	if ( CPT::neutron_ID == particle_id ){
		return nlevels.GetFermiEnergy() + separation_Energy;
	}
	else if ( CPT::proton_ID == particle_id ) {
		
		/* NOTE: CRISP initially constructs two independent Fermi gases, one for neutrons and the other
		 * for the protons. While independent, each one has different Fermi energies and momenta but as soon
		 * as they are put together all protons have their momenta adjusted so that the last proton has the same
		 * Fermi energy and momentum as the last neutron. This adjustment is done by method NucleusDynamics::DoInitConfig(). 
		 * Buy doing this, the protons in the bottom of the well have a minimum momentum which is higher than the 
		 * minimum momentum the neutrons in the bottom can have, as it should be.
		 * Regarding the nuclear potential well, it is now defined by the neutron well.
		 */
		double proton_barrier = nlevels.GetFermiEnergy() + separation_Energy;
		//Coulomb potential = (Ke^2/r0)*[Z/A^(1/3)]
		//using K=9 X 10^{9} N.m^{2}/(C^{2}); e=1.6 X 10^{-19} C; r=r0 A^{1/3} fm,
		//with r0=1.2 and 1eV=1.6 X 10^{-19}, follows that
		//(Ke^2/r0) = 1.19
		proton_barrier += 1.19 * this->Z / TMath::Power(this->A,(1./3.));
		//cout<<"Proton barrier "<< 1.19 * this->Z / TMath::Power(this->A,(1./3.))<<endl;
		return (proton_barrier);  
	} 
	else if ( cpt->IsHyperon(particle_id) || cpt->IsImplRessonance(particle_id) || cpt->IsMeson(particle_id) ){
			
		TParticlePDG *part = cpt->ParticlePDG(particle_id);
		double mass = part->Mass()*1000.; //GeV -> MeV
		double well = mass - mass*CPT::effn; //approximating the potential well by the mass difference
		
		double e = 0.0854358; //elementary charge in Natural units "with Gauss" 
					//( c = hbar = ke = 1  =>  e = sqrt(\alpha) , \alpha = 1./137.)
		double barrier = 0.;
		double p_charge = part->Charge() / 3.; //Charge() returns charge in units of |e|/3
		if(p_charge > 0){
			barrier = (this->Z * p_charge * e * e) / (CPT::r0*TMath::Power(this->A,1./3.)*(1./197.)); // factor 1/197 takes fm -> MeV^-1
		}
		//cout << "ID: " << particle_id << "  potential: " << well << "  barrier: " << barrier << endl;
		return well + barrier;
	}

	return 0.;
}


double NucleusDynamics::NuclearPotentialWell( Int_t particle_id ){
 
	CPT* cpt = CPT::Instance();
	
	if ( CPT::neutron_ID == particle_id ){
		return nlevels.GetFermiEnergy() + separation_Energy;
	}
	else if ( CPT::proton_ID == particle_id ) {
		/* NOTE: CRISP initially constructs two independent Fermi gases, one for neutrons and the other
		 * for the protons. While independent, each one has different Fermi energies and momenta but as soon
		 * as they are put together all protons have their momenta adjusted so that the last proton has the same
		 * Fermi energy and momentum as the last neutron. This adjustment is done by method NucleusDynamics::DoInitConfig(). 
		 * Buy doing this, the protons in the bottom of the well have a minimum momentum which is higher than the 
		 * minimum momentum the neutrons in the bottom can have, as it should be.
		 * Regarding the nuclear potential well, it is now defined by the neutron well.
		 */
		return nlevels.GetFermiEnergy() + separation_Energy;
	}
	else if ( cpt->IsHyperon(particle_id) || cpt->IsImplRessonance(particle_id) || cpt->IsMeson(particle_id) ){
		
		TParticlePDG *part = cpt->ParticlePDG(particle_id);
		double mass = part->Mass()*1000.; //GeV -> MeV
		double well = mass - mass*CPT::effn; //approximating the potential well by the mass difference
		return well;
	}

	return 0.;
}


Double_t NucleusDynamics::RotatKineticEnergy(){

	double I = (2./5.)*GetMass()*pow(GetRadium(), 2.); //moment of inertia
	
	double lx2 = AngularMomentum().X()*AngularMomentum().X();
	double ly2 = AngularMomentum().Y()*AngularMomentum().Y();
	double lz2 = AngularMomentum().Z()*AngularMomentum().Z();
	
	return ( 1./( 2.*I ) ) * ( lx2 + ly2 + lz2 ); 
}



//_________________________________________________________________________________________________

Int_t NucleusDynamics::GetIntLevelOf( ParticleDynamics& p ){

	Int_t id = p.PdgId();
	if ( id == CPT::proton_ID ) {
		double pm = GetProtonMomentum( p.Momentum().Vect() );
		return plevels.GetIntLevelOf(pm);    
	}
	if ( id == CPT::neutron_ID ){
		double nm = p.Momentum().Vect().Mag();
		return nlevels.GetIntLevelOf(nm);
	}  
	// something wrong ...
	return -1;
}
//_________________________________________________________________________________________________	

Int_t NucleusDynamics::GetFreeLevelsFor( ParticleDynamics& p ){ // Return the number of free fermi cells for particle p.

	Int_t id = p.PdgId(); 
	Int_t l =  GetIntLevelOf( p );
	if ( id == CPT::neutron_ID ) 
		return nlevels.GetFreeLevels(l);
  
	else if ( id == CPT::proton_ID )
		return plevels.GetFreeLevels(l);
  
	return 0;
}
//_____________________________________________________________________________________										

bool NucleusDynamics::ChangeNucleon( int index, ParticleDynamics& newParticle){

	TLorentzVector newMomentum = newParticle.Momentum();
	Int_t id	= nucleons[index].PdgId();
	Int_t new_id	= newParticle.PdgId();
	
	int k = 0;
	if(new_id == CPT::proton_ID){
		k = plevels.GetIntLevelOf( GetProtonMomentum( newMomentum.Vect() ) );
		if(k<0) return false;
	}
	else if(new_id == CPT::neutron_ID){
		k = nlevels.GetIntLevelOf( newMomentum.Vect().Mag() );
		if(k<0) return false;
	}
	
	
	if ( id == new_id ){
		if ( (id == CPT::proton_ID) || (id == CPT::neutron_ID) ) {    
			return SetNucleonMomentum(index, newMomentum);      
		}
	}
	else {
		CPT *cpt = CPT::Instance();
		ParticleDynamics q = nucleons[index];
		if ( ( id == CPT::proton_ID ) && ( new_id == CPT::neutron_ID ) ){
			// p changes to n.
			double pnew = newParticle.Momentum().Vect().Mag();
			// get the indexes of the levels where the n and p are located
			Int_t nl = nlevels.GetIntLevelOf(pnew);	
			TVector3 v = nucleons[index].Momentum().Vect();		
			/*
			 * NOTE: v is the proton momentum in a nucleus where proton and neutron 
			 * Fermi energies match, as it should be. This is garanteed in DoInitConfig() by adjusting
			 * the all proton momenta after all nucleons have already been placed from bottom up according to
			 * their momenta as multiples of the momentum cell size dp. 
			 * 
			 * Nevertheless, the plevels object keeps the protons in a regular C++ vector (index starting at 0, 
			 * no matter the position of the proton well relative to neutrons).
			 * 
			 * The proton momentum must be taken back to its original value (before the well adjustment)
			 * in order to allow its location. Therefore, the method GetIntLevelOf() doesn't account for 
			 * the physical Fermi level but for the index that locates the proton in the C++ vector.
			 */
			Int_t pl = plevels.GetIntLevelOf( GetProtonMomentum(v) );
			bool flag_pass=false;
			if ( newParticle.TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ) {
				if (nlevels.GetFreeLevels(nl)>0){
					nlevels[nl]++;
					flag_pass = true;
				}
				else
					return false;
			}
			if ( nucleons[index].TKinetic() <= NuclearPotential( CPT::proton_ID ) ) {
				if (plevels[pl]>0)
					plevels[pl]--;
				else{
					if (flag_pass==true)
						nlevels[nl]--;
					return false;
				}
			}
			nucleons[index] = newParticle;
			nucleons[index].SetPosition( q.Position() );
			nucleons[index].BindIt(true);
			return true;
		}
		if ( ( id == CPT::neutron_ID ) && ( new_id == CPT::proton_ID ) ){      
			// n changes to p
			double pnew = GetProtonMomentum(newMomentum.Vect());
			Int_t pl = plevels.GetIntLevelOf(pnew);	
			double pold = nucleons[index].Momentum().Vect().Mag();
			Int_t nl = nlevels.GetIntLevelOf(pold);
			bool flag_pass = false;
			if ( newParticle.TKinetic() <= NuclearPotential( CPT::proton_ID ) ) {
				if (plevels.GetFreeLevels(pl)>0){
					plevels[pl]++;
					flag_pass=true;
				}
				else
					return false;
			}
			if ( nucleons[index].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
				if (nlevels[nl]>0)
					nlevels[nl]--; 
				else{
					
					if (flag_pass == true)
						plevels[pl]--;
					return false;
				}
			}
			nucleons[index] = newParticle;
			nucleons[index].SetPosition( q.Position() );
			nucleons[index].BindIt(true);
			return true;
		}
		if ( cpt->IsImplRessonance(id) ){
			// ressonance decay
			return RessonanceDecay(index, newParticle);
		}
		if ( CPT::IsHyperon( id ) ){ 
			// std::cout << id << std::endl;
			return RessonanceDecay(index, newParticle); 
		}   
		if ( cpt->IsImplRessonance(new_id) ){
			// ressonance creation 	
			return PutRessonance(index, newParticle);      
		}
	} // closing brace of else
	return false;    
}
//_________________________________________________________________________________________________

bool NucleusDynamics::NucleonPairChange( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	CPT *cpt = CPT::Instance();
	Int_t	new_particle_id_1 = n1.PdgId(), 
		new_particle_id_2 = n2.PdgId();
	Int_t	particle_id_1 = nucleons[idx1].PdgId(),
		particle_id_2 = nucleons[idx2].PdgId();
		
	int k = 0;
	if(new_particle_id_1 == CPT::proton_ID){
		k = plevels.GetIntLevelOf( GetProtonMomentum( n1.Momentum().Vect() ) );
		if(k<0) return false;
	}
	else if(new_particle_id_1 == CPT::neutron_ID){
		k = nlevels.GetIntLevelOf( n1.Momentum().Vect().Mag() );
		if(k<0) return false;
	}
	if(new_particle_id_2 == CPT::proton_ID){
		k = plevels.GetIntLevelOf( GetProtonMomentum( n2.Momentum().Vect() ) );
		if(k<0) return false;
	}
	else if(new_particle_id_2 == CPT::neutron_ID){
		k = nlevels.GetIntLevelOf( n2.Momentum().Vect().Mag() );
		if(k<0) return false;
	}

		
//	std::cout<< nucleons[idx1].Name() <<" + "<< nucleons[idx2].Name() <<" -> "<<n1.Name()<<" + "<<n2.Name()<<"\n" ; 
	if ( (particle_id_1 == CPT::proton_ID)&&(particle_id_2 == CPT::proton_ID) ){
		// p p ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_pp_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_pp_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_pp_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_pp_nn( idx1, idx2, n1, n2 );
		}

		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> d n
			return NucleonPairChange_pp_dn( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> d p
			return NucleonPairChange_pp_dp( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::neutron_ID) ){
			// -> n d
			return NucleonPairChange_pp_nd( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::proton_ID) ){
			// -> p d
			return NucleonPairChange_pp_pd( idx1, idx2, n1, n2 );
		}
	}
	else if ( (particle_id_1 == CPT::proton_ID)&&(particle_id_2 == CPT::neutron_ID) ){
		// p n ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_pn_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_pn_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_pn_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_pn_nn( idx1, idx2, n1, n2 );
		}

		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> d n
			return NucleonPairChange_pn_dn( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> d p
			return NucleonPairChange_pn_dp( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::neutron_ID) ){
			// -> n d
			return NucleonPairChange_pn_nd( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::proton_ID) ){
			// -> p d
			return NucleonPairChange_pn_pd( idx1, idx2, n1, n2 );
		}
	}
	else if ( (particle_id_1 == CPT::neutron_ID)&&(particle_id_2 == CPT::proton_ID) ){
		// n p ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_np_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_np_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_np_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_np_nn( idx1, idx2, n1, n2 );
		}

		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> d n
			return NucleonPairChange_np_dn( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> d p
			return NucleonPairChange_np_dp( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::neutron_ID) ){
			// -> n d
			return NucleonPairChange_np_nd( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::proton_ID) ){
			// -> p d
			return NucleonPairChange_np_pd( idx1, idx2, n1, n2 );
		}
	}
	else if ( (particle_id_1 == CPT::neutron_ID)&&(particle_id_2 == CPT::neutron_ID) ){
		// n n ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_nn_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_nn_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_nn_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_nn_nn( idx1, idx2, n1, n2 );
		}

		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> d n
			return NucleonPairChange_nn_dn( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon(particle_id_1) )&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> d p
			return NucleonPairChange_nn_dp( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::neutron_ID) ){
			// -> n d
			return NucleonPairChange_nn_nd( idx1, idx2, n1, n2 );
		}
		else if ( ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon(particle_id_2) )&&(new_particle_id_1 == CPT::proton_ID) ){
			// -> p d
			return NucleonPairChange_nn_pd( idx1, idx2, n1, n2 );
		}
	}
	else if ( ( cpt->IsImplRessonance(particle_id_1) || cpt->IsHyperon(particle_id_1) )&&(particle_id_2 == CPT::neutron_ID) ){
		// d n ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_dn_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_dn_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_dn_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_dn_nn( idx1, idx2, n1, n2 );
		}
		// exist processo -> d *
	}
	else if ( ( cpt->IsImplRessonance(particle_id_1) || cpt->IsHyperon(particle_id_1) )&&(particle_id_2 == CPT::proton_ID) ){
		// d p ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_dp_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_dp_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_dp_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_dp_nn( idx1, idx2, n1, n2 );
		}
		// exist processo -> d *
	}
	else if ( ( cpt->IsImplRessonance(particle_id_2) || cpt->IsHyperon(particle_id_2) )&&(particle_id_1 == CPT::neutron_ID) ){
		// n d ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_nd_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_nd_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_nd_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_nd_nn( idx1, idx2, n1, n2 );
		}
		// exist processo -> d *
	}
	else if ( ( cpt->IsImplRessonance(particle_id_2) || cpt->IsHyperon(particle_id_2) )&&(particle_id_1 == CPT::proton_ID) ){
		// p d ->
		if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> p p
			return NucleonPairChange_pd_pp( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::proton_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> p n
			return NucleonPairChange_pd_pn( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::proton_ID) ){
			// -> n p
			return NucleonPairChange_pd_np( idx1, idx2, n1, n2 );
		}
		else if ( (new_particle_id_1 == CPT::neutron_ID)&&(new_particle_id_2 == CPT::neutron_ID) ){
			// -> n n
			return NucleonPairChange_pd_nn( idx1, idx2, n1, n2 );
		}
		// exist processo -> d *
	}
//-----------------------
/*	else{
	// new ressonance ...
 		if ( cpt->IsImplRessonance( new_particle_id_1 ) || cpt->IsHyperon( new_particle_id_1 ) ) {
			if (PutRessonance(idx1, n1) && ChangeNucleon(idx2, n2))
				return true;
			return false; 
		}
		else if ( cpt->IsImplRessonance( new_particle_id_2 ) || cpt->IsHyperon( new_particle_id_2 ) ){
			if (PutRessonance(idx2, n2) && ChangeNucleon(idx1, n1))
				return true;

			return false; 
		}
	}
*/
	return false;
}


//_________________________________________________________________________________________________

/* 
 * NOTE: This method is under testing
 */

bool NucleusDynamics::NucleonPairChange_test( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	
	Int_t old_Level_1 = 0;
	Int_t old_Level_2 = 0;
	
	bool old_pass_1 = false,
	     old_pass_2 = false;
	     
///identifying the old particles, obtaining their levels and trying to extract them
	
	///old particle 1
	if(nucleons[idx1].PdgId() == CPT::proton_ID){
	
		old_Level_1 = plevels.GetIntLevelOf( GetProtonMomentum( nucleons[idx1].Momentum().Vect() ) );
		if ( (nucleons[idx1].Momentum().E() - nucleons[idx1].GetMass()) <= NuclearPotential( nucleons[idx1].PdgId() ) ){
			if (plevels[old_Level_1]>0){
				plevels[old_Level_1]--;
				old_pass_1 = true;
			}
			else
				return false;
		}
	}
	else if( nucleons[idx1].PdgId() == CPT::neutron_ID ){
	
		old_Level_1 = nlevels.GetIntLevelOf( nucleons[idx1].Momentum().Vect().Mag() );
		if ( (nucleons[idx1].Momentum().E() - nucleons[idx1].GetMass()) <= NuclearPotential( nucleons[idx1].PdgId() ) ){
			if (nlevels[old_Level_1]>0){
				nlevels[old_Level_1]--;
				old_pass_1 = true;
			}
			else
				return false;
		}
	}
	     
	///old particle 2
	if(nucleons[idx2].PdgId() == CPT::proton_ID){
	
		old_Level_2 = plevels.GetIntLevelOf( GetProtonMomentum( nucleons[idx2].Momentum().Vect() ) );
		if ( (nucleons[idx2].Momentum().E() - nucleons[idx2].GetMass()) <= NuclearPotential( nucleons[idx2].PdgId() ) ){
			if (plevels[old_Level_2]>0){
				plevels[old_Level_2]--;
				old_pass_2 = true;
			}
			else{
				if(nucleons[idx1].PdgId() == CPT::proton_ID) plevels[old_Level_1]++;
				if(nucleons[idx1].PdgId() == CPT::neutron_ID) nlevels[old_Level_1]++;
				return false;
			}
		}
	}
	else if( nucleons[idx2].PdgId() == CPT::neutron_ID ){
		
		old_Level_2 = nlevels.GetIntLevelOf( nucleons[idx2].Momentum().Vect().Mag() );
		if ( (nucleons[idx2].Momentum().E() - nucleons[idx2].GetMass()) <= NuclearPotential( nucleons[idx2].PdgId() ) ){
			if (nlevels[old_Level_2]>0){
				nlevels[old_Level_2]--;
				old_pass_2 = true;
			}
			else{
				if(nucleons[idx1].PdgId() == CPT::proton_ID) plevels[old_Level_1]++;
				if(nucleons[idx1].PdgId() == CPT::neutron_ID) nlevels[old_Level_1]++;
				return false;
			}
		}
	}
	
	Int_t new_Level_1 = 0;
	Int_t new_Level_2 = 0;
	
	/*
	 * NOTE: variable refusedNew is important because resonances don't pass Pauli blocking
	 * but they are also not refused, so it is more general checking for refusal than
	 * for permission 
	 */
	bool refusedNew = false,
	     new_pass_1 = false,
	     new_pass_2 = false;
	
///identifying the new particles, obtaining their levels and trying the change
	
	///new particle 1
	if(n1.PdgId() == CPT::proton_ID){
	
		new_Level_1 = plevels.GetIntLevelOf( GetProtonMomentum( n1.Momentum().Vect() ) );
		if ( (n1.Momentum().E() - n1.GetMass()) <= NuclearPotential( n1.PdgId() ) ){
			if (plevels.GetFreeLevels(new_Level_1)>0){
				plevels[new_Level_1]++;
				new_pass_1 = true;
			}
			else
				refusedNew = true;
		}
	}
	else if( n1.PdgId() == CPT::neutron_ID ){
	
		new_Level_1 = nlevels.GetIntLevelOf( n1.Momentum().Vect().Mag() );
		if ( (n1.Momentum().E() - n1.GetMass()) <= NuclearPotential( n1.PdgId() ) ){
			if (nlevels.GetFreeLevels(new_Level_1)>0){
				nlevels[new_Level_1]++;
				new_pass_1 = true;
			}
			else
				refusedNew = true;
		}
	}
	
	if(!refusedNew){ ///first new particle passed Pauli blocking
	
		///new particle 2
		if(n2.PdgId() == CPT::proton_ID){
			
			new_Level_2 = plevels.GetIntLevelOf( GetProtonMomentum( n2.Momentum().Vect() ) );
			if ( (n2.Momentum().E() - n2.GetMass()) <= NuclearPotential( n2.PdgId() ) ){
				if (plevels.GetFreeLevels(new_Level_2)>0){
					plevels[new_Level_2]++;
					new_pass_2 = true;
				}
				else
					refusedNew = true;
			}
		}
		else if( n2.PdgId() == CPT::neutron_ID ){
			
			new_Level_2 = nlevels.GetIntLevelOf( n2.Momentum().Vect().Mag() );
			if ( (n2.Momentum().E() - n2.GetMass()) <= NuclearPotential( n2.PdgId() ) ){
				if (nlevels.GetFreeLevels(new_Level_2)>0){
					nlevels[new_Level_2]++;
					new_pass_2 = true;
				}
				else
					refusedNew = true;
			}
		}
		
		if(!refusedNew){ ///if we got here everything went well, binding new particles to the nucleus


			/*
			 * NOTE: Tracking changes in levels at each collision
			 */
// 			if( (nucleons[idx1].PdgId() == CPT::proton_ID || nucleons[idx1].PdgId() == CPT::neutron_ID) &&
// 			    (nucleons[idx2].PdgId() == CPT::proton_ID || nucleons[idx2].PdgId() == CPT::neutron_ID) &&
// 			    (n1.PdgId() == CPT::proton_ID || n1.PdgId() == CPT::neutron_ID) &&
// 			    (n2.PdgId() == CPT::proton_ID || n2.PdgId() == CPT::neutron_ID) ){
// 		
// 				
// 				cout << "1: " << nucleons[idx1].PdgId() << " level " << old_Level_1 << 
// 					", Kinetic Energy: " << nucleons[idx1].Momentum().E() - nucleons[idx1].GetMass() << " ---> " << 
// 					n1.PdgId() << " level " << new_Level_1 << ", Kinetic Energy: " << n1.Momentum().E() - n1.GetMass();
// 						
// 				if( (n1.Momentum().E() - n1.GetMass()) > NuclearPotentialWell( n1.PdgId() ) ){
// 					cout << ", I got above potential!" << endl;
// 				}
// 				else cout << endl;
// 					
// 				cout << "2: " << nucleons[idx2].PdgId() << " level " << old_Level_2 << 
// 					", Kinetic Energy: " << nucleons[idx2].Momentum().E() - nucleons[idx2].GetMass() << " ---> " << 
// 					n2.PdgId() << " level " << new_Level_2 << ", Kinetic Energy: " << n2.Momentum().E() - n2.GetMass();
// 						
// 				if( (n2.Momentum().E() - n2.GetMass()) > NuclearPotentialWell( n2.PdgId() ) ){
// 					cout << ", I got above potential!" << endl << endl;
// 				}
// 				else cout << endl << endl;
// 
// 			}

			
			n1.SetPosition(nucleons[idx1].Position());
			nucleons[idx1] = n1;
			nucleons[idx1].BindIt(true);

			n2.SetPosition(nucleons[idx2].Position());
			nucleons[idx2] = n2;
			nucleons[idx2].BindIt(true);

			return true;
	
		}
		else{ ///second new particle denied by Pauli blocking. Undoing old particles extractions and particle 1 inclusion


			if(nucleons[idx1].PdgId() == CPT::proton_ID) plevels[old_Level_1]++;
			else if(nucleons[idx1].PdgId() == CPT::neutron_ID) nlevels[old_Level_1]++;
			
			if(nucleons[idx2].PdgId() == CPT::proton_ID) plevels[old_Level_2]++;
			else if(nucleons[idx2].PdgId() == CPT::neutron_ID) nlevels[old_Level_2]++;
	
			if(n1.PdgId() == CPT::proton_ID) plevels[new_Level_1]--;
			else if(n1.PdgId() == CPT::neutron_ID) nlevels[new_Level_1]--;
			
			return false;
		}
	}
	else{

		if(nucleons[idx1].PdgId() == CPT::proton_ID) plevels[old_Level_1]++;
		else if(nucleons[idx1].PdgId() == CPT::neutron_ID) nlevels[old_Level_1]++;

		if(nucleons[idx2].PdgId() == CPT::proton_ID) plevels[old_Level_2]++;
		else if(nucleons[idx2].PdgId() == CPT::neutron_ID) nlevels[old_Level_2]++;
		
		return false;
	}
}

//_________________________________________________________________________________________________

bool NucleusDynamics::NucleonPairChange_pp_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TVector3 v__1 = (nucleons[idx1].Momentum().Vect());
	TVector3 v__2 = (nucleons[idx2].Momentum().Vect());
	Int_t level_particle_id_1 = plevels.GetIntLevelOf( GetProtonMomentum(v__1) );
	Int_t level_particle_id_2 = plevels.GetIntLevelOf( GetProtonMomentum(v__2) );
	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//p
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-proton
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p
	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p
	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
	double 	p_mass = CPT::p_mass * CPT::effn;
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}
	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
	}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	n1.SetPosition(nucleons[idx1].Position());
	nucleons[idx1] = n1;//p
	nucleons[idx1].BindIt(true);

	n2.SetPosition(nucleons[idx2].Position());
	nucleons[idx2] = n2;//p
	nucleons[idx2].BindIt(true);

	return true;
}

bool NucleusDynamics::NucleonPairChange_pp_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum(),//p
			v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
		p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//p
		flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
		level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics q_1 = nucleons[idx1],//p
			  q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
		level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_pp_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum(),//p
			v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
		p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//p
		flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
		level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
				q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
		level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
		flag_pass_1	= false,
		flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);

	return true;

}
bool NucleusDynamics::NucleonPairChange_pp_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum(),//p
			v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
		p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//p
		flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
		level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
	
}

//_________________________________________________________________________________________________										

bool NucleusDynamics::NucleonPairChange_pn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}

	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}
bool NucleusDynamics::NucleonPairChange_pn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_pn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_pn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

//_________________________________________________________________________________________________										

bool NucleusDynamics::NucleonPairChange_np_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(), //n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}
bool NucleusDynamics::NucleonPairChange_np_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p


	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 );
	Int_t 	level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else{
			return false;
		}
	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p


	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else{
			flag_pass = true;
		}
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else{
			flag_pass = true;
		}
	}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_np_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
	
}
bool NucleusDynamics::NucleonPairChange_np_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

//_________________________________________________________________________________________________										

bool NucleusDynamics::NucleonPairChange_nn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_nn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_nn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}
bool NucleusDynamics::NucleonPairChange_nn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

/*****************************************************************************************************/
/*****************************************************************************************************/

//_________________________________________________________________________________________________										

bool NucleusDynamics::NucleonPairChange_nn_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;
			
	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_nn_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	
	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	
	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_pp_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum(),//p
					v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 );//n
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

bool NucleusDynamics::NucleonPairChange_pp_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum(),//p
					v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 );//p
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;
			
	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);

	return true;
}

bool NucleusDynamics::NucleonPairChange_pn_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 );//n
			
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_pn_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect()),//p
			p_particle_id_2 = nucleons[idx2].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false,//p
			flag_particle_id_2 = false;//n
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 ),//p
			level_particle_id_2 = nlevels.GetIntLevelOf( p_particle_id_2 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//n
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 
		if (nlevels[level_particle_id_2]>0){
			nlevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				plevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}
		if (flag_particle_id_2 == true){
			nlevels[level_particle_id_2]++;//n
		}
		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_np_nd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else{
			return false;
		}
	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> neutron-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 );//n
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else{
			flag_pass = true;
		}
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_np_pd( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_2 = nucleons[idx2].Momentum();//p

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(),//n
			p_particle_id_2 = GetProtonMomentum(v_particle_id_2.Vect());//p

	bool 	flag_particle_id_1 = false,//n
			flag_particle_id_2 = false;//p
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 ),//n
			level_particle_id_2 = plevels.GetIntLevelOf( p_particle_id_2 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//p
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	if ( nucleons[idx2].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 
		if (plevels[level_particle_id_2]>0){
			plevels[level_particle_id_2]--;
			flag_particle_id_2 = true;
		}
		else{
			if (flag_particle_id_1 == true)
				nlevels[level_particle_id_1]++;
			return false;
		}
	}
		
	// caso -> proton-delta
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 );//p
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}
		if (flag_particle_id_2 == true){
			plevels[level_particle_id_2]++;//p
		}
		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//d
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

//_________________________________________________________________________________________________										
	
bool NucleusDynamics::NucleonPairChange_nd_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;
	}
		
	// caso -> neutron-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}

		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

bool NucleusDynamics::NucleonPairChange_pd_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect());//p

	bool 	flag_particle_id_1 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}

	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}
		
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}

		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_nd_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else{
			return false;
		}
	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else{
			flag_pass = true;
		}
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else{
			flag_pass = true;
		}
	}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		
		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}

		return false;
	}
		
	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_pd_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect());//p

	bool 	flag_particle_id_1 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
		
	// caso -> neutron-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//n
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = newMomentum_1.Vect().Mag();//n
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = nlevels.GetIntLevelOf( p_new_1 ),//n
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_1)>0){
			nlevels[level_new_1]++; //n
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			nlevels[level_new_1]--;//n
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}

		return false;
	}

	nucleons[idx1] = n1;//n
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_nd_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag();//n

	bool 	flag_particle_id_1 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
	
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}

		return false;
	}
		
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_pd_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect());//p

	bool 	flag_particle_id_1 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
		
	// caso -> proton-neutron
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//n

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = newMomentum_2.Vect().Mag();//n

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = nlevels.GetIntLevelOf( p_new_2 );//n
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
	double 	n_mass = CPT::n_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
		if (nlevels.GetFreeLevels(level_new_2)>0){
			nlevels[level_new_2]++; //n
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			nlevels[level_new_2]--;//n
		}

		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}

		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//n
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}

bool NucleusDynamics::NucleonPairChange_nd_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	double 	p_particle_id_1 = nucleons[idx1].Momentum().Vect().Mag(); //n

	bool 	flag_particle_id_1 = false;//n
			
	Int_t 	level_particle_id_1 = nlevels.GetIntLevelOf( p_particle_id_1 );//n

	ParticleDynamics 	q_1 = nucleons[idx1],//n
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){ 

		if (nlevels[level_particle_id_1]>0){
			nlevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
		
	// caso -> proton-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}

		if (flag_particle_id_1 == true){
			nlevels[level_particle_id_1]++;//n
		}

		return false;
	}
	
	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;
}

bool NucleusDynamics::NucleonPairChange_pd_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){

	TLorentzVector 	v_particle_id_1 = nucleons[idx1].Momentum();//p

	double 	p_particle_id_1 = GetProtonMomentum(v_particle_id_1.Vect());//p

	bool 	flag_particle_id_1 = false;//p
			
	Int_t 	level_particle_id_1 = plevels.GetIntLevelOf( p_particle_id_1 );//p

	ParticleDynamics 	q_1 = nucleons[idx1],//p
						q_2 = nucleons[idx2];//d
		
	if ( nucleons[idx1].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){ 

		if (plevels[level_particle_id_1]>0){
			plevels[level_particle_id_1]--;
			flag_particle_id_1 = true;
		}
		else
			return false;

	}
		
	// caso -> proton-proton
	
	TLorentzVector newMomentum_1 = n1.Momentum();//p
	TLorentzVector newMomentum_2 = n2.Momentum();//p

	double p_new_1 = GetProtonMomentum(newMomentum_1.Vect());//p
	double p_new_2 = GetProtonMomentum(newMomentum_2.Vect());//p

	Int_t 	level_new_1 = plevels.GetIntLevelOf( p_new_1 ),//p
			level_new_2 = plevels.GetIntLevelOf( p_new_2 );//p
				
				
	double 	p_mass = CPT::p_mass * CPT::effn;
			
	bool	flag_pass 	= false,
			flag_pass_1	= false,
			flag_pass_2	= false;

	if ( (newMomentum_1.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_1)>0){
			plevels[level_new_1]++; //p
			flag_pass_1 = true;
		}
		else
			flag_pass = true;
	}

	if ( (newMomentum_2.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
		if (plevels.GetFreeLevels(level_new_2)>0){
			plevels[level_new_2]++; //p
			flag_pass_2 = true;
		}
		else
			flag_pass = true;
		}
			
	if (flag_pass == true){
	
		if (flag_pass_1 == true){
			plevels[level_new_1]--;//p
		}
		if (flag_pass_2 == true){
			plevels[level_new_2]--;//p
		}
		
		if (flag_particle_id_1 == true){
			plevels[level_particle_id_1]++;//p
		}

		return false;
	}

	nucleons[idx1] = n1;//p
	nucleons[idx1].SetPosition( q_1.Position() );
	nucleons[idx1].BindIt(true);
	
	nucleons[idx2] = n2;//p
	nucleons[idx2].SetPosition( q_2.Position() );
	nucleons[idx2].BindIt(true);
		
	return true;

}
	
//_________________________________________________________________________________________________										

bool NucleusDynamics::NucleonPairChange_nn_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_nn_nd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_nn_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_nn_pd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_pp_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pp_nd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_pp_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pp_pd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_pn_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pn_nd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_pn_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pn_pd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_np_dn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_np_nd( idx1, idx2, n2, n1 );
}

bool NucleusDynamics::NucleonPairChange_np_dp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_np_pd( idx1, idx2, n2, n1 );
}

//_________________________________________________________________________________________________										
	
bool NucleusDynamics::NucleonPairChange_dn_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return NucleonPairChange_nd_nn( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dp_nn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pd_nn( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dn_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_nd_np( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dp_np( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pd_np( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dn_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_nd_pn( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dp_pn( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pd_pn( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dn_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_nd_pp( idx2, idx1, n1, n2 );
}

bool NucleusDynamics::NucleonPairChange_dp_pp( Int_t idx1, Int_t idx2, ParticleDynamics& n1, ParticleDynamics& n2 ){
	return  NucleonPairChange_pd_pp( idx2, idx1, n1, n2 );
} 

/*****************************************************************************************************/
/*****************************************************************************************************/
bool NucleusDynamics::PutRessonance( Int_t nucleonIndex, ParticleDynamics& q ){
  
	TVector3 p = nucleons[nucleonIndex].Momentum().Vect();
	Int_t nucId = nucleons[nucleonIndex].PdgId();  
	if ( nucId == CPT::proton_ID ){
		double k = GetProtonMomentum(p);
		Int_t i = plevels.GetIntLevelOf( k );
		if ( nucleons[nucleonIndex].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){
			if (plevels[i]>0)
				plevels[i]--;
			else 
				return false;
		}
		q.SetPosition( nucleons[nucleonIndex].Position() );
		q.BindIt(true);
		nucleons[nucleonIndex] = q;
		return true;
	}
	if ( nucId == CPT::neutron_ID ) {
		double k = p.Mag();
		Int_t i = nlevels.GetIntLevelOf( k );    
		if ( nucleons[nucleonIndex].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){
			if (nlevels[i]>0)
				nlevels[i]--;
			else 
				return false;
		}

		q.SetPosition( nucleons[nucleonIndex].Position() );
		q.BindIt(true);
		
		nucleons[nucleonIndex] = q;

		return true;
	}
  
	q.SetPosition( nucleons[nucleonIndex].Position() );
	q.BindIt(true);
	nucleons[nucleonIndex] = q;
  
	return true;
}

//_________________________________________________________________________________________________										

bool NucleusDynamics::RessonanceDecay( Int_t nucleonIndex, ParticleDynamics& q ){

	Int_t new_id = q.PdgId();
	if ( new_id == CPT::neutron_ID ) {
		ParticleDynamics n( CPT::neutron_ID );
		double k = q.Momentum().Vect().Mag();    
		// Increment the neutron level l
		Int_t l = nlevels.GetIntLevelOf( k );
		if(l < 0)
			return false;
		
		if ( q.TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){
			if (nlevels.GetFreeLevels(l)>0)
				nlevels[l]++;
			else 
				return false;
		}
		q.SetPosition( nucleons[nucleonIndex].Position() ); 
		q.BindIt(true);
		q.SetMass( n.GetInvariantMass() * CPT::effn );
		nucleons[nucleonIndex] = q;      
		return true;
	}
	if ( new_id == CPT::proton_ID ) {
		double p = GetProtonMomentum(q.Momentum().Vect());
		// increment the proton level j.
		Int_t j = plevels.GetIntLevelOf(p);
		if(j < 0)
			return false;
		
		if ( q.TKinetic() <= NuclearPotential( CPT::proton_ID ) ){
			if (plevels.GetFreeLevels(j)>0)
				plevels[j]++;
			else 
				return false;
		}
		q.SetPosition( nucleons[nucleonIndex].Position() ); 
		q.BindIt(true);
		q.SetMass( CPT::p_mass * CPT::effn );
		nucleons[nucleonIndex] = q;
		return true;	
	}
	q.SetPosition( nucleons[nucleonIndex].Position() ); 
	q.BindIt(true);
	nucleons[nucleonIndex] = q;
	return true;  
}
//_________________________________________________________________________________________________

bool NucleusDynamics::SetNucleonMomentum( Int_t nucleonIndex, TLorentzVector& newMomentum){
 
	Int_t id = nucleons[nucleonIndex].PdgId();
	if ( id == CPT::neutron_ID ){
		double p_old = nucleons[nucleonIndex].Momentum().Vect().Mag();    
		double p_new = newMomentum.Vect().Mag();
		Int_t l_new = nlevels.GetIntLevelOf( p_new );
		
		if(l_new < 0) return false;
		
		Int_t l_old = nlevels.GetIntLevelOf( p_old );      
		if ( l_new != l_old ){      
			double n_mass = CPT::n_mass * CPT::effn;
			bool flag_pass=false;
			if ( nucleons[nucleonIndex].TKinetic() <= (NuclearPotential( CPT::neutron_ID )) ){
				if (nlevels[l_old]>0){
					nlevels[l_old]--;      
					flag_pass=true;
				}
				else{
					return false;
				}
			}
			if ( (newMomentum.E() - n_mass) <= (NuclearPotential( CPT::neutron_ID )) ){
				if (nlevels.GetFreeLevels(l_new)>0){
					nlevels[l_new]++;       
				}
				else {
					if (flag_pass==true){
						nlevels[l_old]++;
					}
					return false;
				}
			}
			nucleons[nucleonIndex].SetMomentum(newMomentum);
			return true;
		}
		else if(l_new == l_old){
			nucleons[nucleonIndex].SetMomentum(newMomentum);
			return true;
		}
	}
	else if ( id == CPT::proton_ID ){
		// k2 eh o modulo do momento do proton menos o incremento devido
		// ao poco de neutrons.
		TLorentzVector v = nucleons[nucleonIndex].Momentum();
		double p_old = GetProtonMomentum(v.Vect());
		double p_new = GetProtonMomentum(newMomentum.Vect());
		Int_t l_old = plevels.GetIntLevelOf( p_old );
		Int_t l_new = plevels.GetIntLevelOf( p_new );
		
		if(l_new < 0) return false;
		
		if ( l_new != l_old ) {      
			double p_mass = CPT::p_mass * CPT::effn;
			bool flag_pass = false;
			if ( nucleons[nucleonIndex].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){
				if (plevels[l_old]>0){
					plevels[l_old]--;
					flag_pass = true;
				}
				else{ 	
					return false;
				}
			}
			if ( (newMomentum.E() - p_mass) <= NuclearPotential( CPT::proton_ID ) ){
				if (plevels.GetFreeLevels(l_new)>0){
					plevels[l_new]++; 
				}
				else{
					if (flag_pass == true)
						plevels[l_old]++; 
					return false;
				}
			}
			nucleons[nucleonIndex].SetMomentum(newMomentum);
			return true;
		}
		else if (l_new == l_old){
			nucleons[nucleonIndex].SetMomentum(newMomentum);
			return true;
		}
	}
	return false;      
}
//_________________________________________________________________________________________________

Int_t NucleusDynamics::CaptureNucleon( ParticleDynamics& p ){

	if ( p.PdgId() == CPT::proton_ID ) {     
		A++; Z++;
	}  
	else if ( p.PdgId() == CPT::neutron_ID ) 
		A++;  
  
	nucleons[A-1] = p;
	return (A - 1);
}
//_________________________________________________________________________________________________

bool NucleusDynamics::UnBindNucleon(Int_t idx){

	/* NOTE: The management of the Fermi levels regarding the escaping particle is also done
	 * by the method NucleusDynamics::SetNucleonMomentum() every time the particle gets either under
	 * or above the nuclear potential unless it's a proton
	 * 
	 * NucleusDynamics keeps track of the Fermi levels up to the Coulomb barrier.
	 * If a proton leaves when it is above the barrier there is no need to look at the level,
	 * the code just unbinds the proton.
	 * If the proton leaves by tunneling, that means it was in the region where we keep track of the levels.
	 * In this case, we must check the occupation and then decrement it here.
	 */
	if ( nucleons[idx].PdgId() == CPT::proton_ID ){
		
		if( nucleons[idx].TKinetic() > NuclearPotential( CPT::proton_ID ) ){
			nucleons[idx].BindIt(false);
			return true;
		}
		else{
			Int_t l = plevels.GetIntLevelOf( GetProtonMomentum( nucleons[idx].Momentum().Vect() ) );
			if (plevels[l]>0){
				plevels[l]--;
			}
			nucleons[idx].BindIt(false);
			return true;
		}
	}
	if ( nucleons[idx].PdgId() == CPT::neutron_ID ){
		nucleons[idx].BindIt(false);
		return true;
	}
	if ( CPT::IsImplRessonance(nucleons[idx].PdgId()) ){
		nucleons[idx].BindIt(false);
		return true;
	}
	if ( CPT::IsHyperon(nucleons[idx].PdgId()) ){
		nucleons[idx].BindIt(false);
		return true;
	}

	cout << "WARNING!! Particle not identified. Unable to unbind it. Function responsible: NucleusDynamics::UnBindNucleon(Int_t idx)" << endl;
	nucleons[idx].BindIt(true);
	return false;
}
//_________________________________________________________________________________________________

double NucleusDynamics::B(int Z_, int A_){
	
	if(!table){
		/* ==============================================================================
		//Bethe-Weizsacker
		double a_v = 15.853,
			a_s = 18.330,
		 	a_c = 0.714,
		 	a_l = 23.200,
		 	delta = 11.200;
		 	
		double b = a_v * double(A_) 
			 - a_s * TMath::Power( double(A_) ,(2./3.)) 
		 	 - a_c * double(Z_) * (double(Z_) -1) / TMath::Power(double(A_),(1./3.)) 
		 	 - a_l * sqr(double(A_)-2*double(Z_)) / double(A_);
		 
		if ( (A_%2) == 0 ){
			if((((A_-Z_)%2)==0)&&((Z_%2)==0)){
				b = b + delta;
			}
			else if ((((A_-Z_)%2)!=0)&&((Z_%2)!=0)){
				b = b - delta;
			}
		}
		===================================================================================
		*/
		//Pearson 2001
		double Av = 15.65, Asf = -17.63, Asym = 27.72, Ass = -25.60;
		double N = A_ - Z_;
		double r0 = 1.233;
		double e2 = 1.44;
		double  Ac = (3. * e2)/(5. * r0);
			
		double J = (N-Z_)/A_;
		double b =  ( Av
				+ Asf * (pow(A_,-1./3.))
				+ Ac * (Z * Z)*(pow(A_, -4./3.))
				+ ( Asym + Ass * pow(A_,-1./3.) )
				* J*J );
		//=================================================================================
		return b;
		
	} else {
		return tableB.Get(Z_, A_);
	}

}

double NucleusDynamics::Sn(int Z_, int A_){

	return B(Z_,A_) - B(Z_,(A_-1)); 
}

double NucleusDynamics::Sp(int Z_, int A_){

	return B(Z_,A_) - B((Z_-1),(A_-1)); 
}

double NucleusDynamics::Sg( int N_neu, int N_prot){
	int Z_  =  this->Z;
	int A_ = this->A;
	if(A_==N_neu+N_prot)
	    return B(Z_,A_);
//	cout << Z_ << " " << A_ << " test funtion " << endl;
//	cout << B(Z_,A_) << " " <<   B((Z_-N_prot),(A_-N_neu-N_prot)) << " " << Sp(Z_,A_)  <<  endl;
	return B(Z_,A_) - B((Z_-N_prot),(A_-N_neu-N_prot)); 
}

//_________________________________________________________________________________________________
/*
bool NucleusDynamics::BindNucleon(Int_t idx){

	Int_t l = GetIntLevelOf(nucleons[idx]);
	if (nucleons[idx].PdgId() == CPT::proton_ID ){
		if ( nucleons[idx].TKinetic() <= NuclearPotential( CPT::proton_ID ) ){
			if (plevels[l]>0){
				plevels[l]++;
				nucleons[idx].BindIt(true);
				return true;
			}
			else 
				return false;
		}
	}
	else if (nucleons[idx].PdgId() == CPT::neutron_ID ){
		if ( nucleons[idx].TKinetic() <= NuclearPotential( CPT::neutron_ID ) ){
			if (nlevels[l]>0){
				nlevels[l]++;
				nucleons[idx].BindIt(true);
				return true;
			}
			else 
				return false;
		}
	}
	if ( CPT::IsImplRessonance(nucleons[idx].PdgId()) ){
		nucleons[idx].BindIt(true);
		return true;
	}
	if ( CPT::IsHyperon(nucleons[idx].PdgId()) ){
		nucleons[idx].BindIt(true);
		return true;
	}
	if ((nucleons[idx].PdgId() == CPT::proton_ID)||(nucleons[idx].PdgId() == CPT::neutron_ID ) ){
		nucleons[idx].BindIt(true);
		return true;
	}
	nucleons[idx].BindIt(true);
	return false;
}
*/
Nuc_Struct NucleusDynamics::GetNucStruct(){
	Nuc_Struct NS;
	NS.A = this->A;
	NS.Z = this->Z;
	for (int i = 0; i < 30; i++){
		NS.PLevels[i] = this->plevels[i];
		NS.NLevels[i] = this->nlevels[i];
//		std::cout << " Proton " << this->plevels[i] << std::endl;
//		std::cout << " Neutron " << this->nlevels[i] << std::endl;
	}
	for (int i = 0; i < this->A; i++){
		NS.Nuc_IDX[i] = i;
		NS.Nuc_PID[i] = this->nucleons[i].PdgId();
		NS.Nuc_IsB[i] = this->nucleons[i].IsBind();
		NS.Nuc_X[i] = this->nucleons[i].Position().X();
		NS.Nuc_Y[i] = this->nucleons[i].Position().Y();
		NS.Nuc_Z[i] = this->nucleons[i].Position().Z();
		NS.Nuc_P[i] = this->nucleons[i].Momentum().Vect().Mag();
		NS.Nuc_PX[i] = this->nucleons[i].Momentum().Px();
		NS.Nuc_PY[i] = this->nucleons[i].Momentum().Py();
		NS.Nuc_PZ[i] = this->nucleons[i].Momentum().Pz();
		NS.Nuc_E[i] = this->nucleons[i].Momentum().E();
		NS.Nuc_K[i] = this->nucleons[i].TKinetic();
	}
	return NS;
}

TTree* NucleusDynamics::GetNucTree(){
	typedef struct {Int_t p,n;} Level;
	TTree *L = new TTree("Process","Process Tree");
	static Level l;
	L->Branch("level",&l,"p:n");
	for (int i = 0; i < 30; i++){
		l.p = this->plevels[i];
		l.n = this->nlevels[i];
		L->Fill();
	}
	typedef struct {
		Int_t ix,pid; 
		bool Bnd;
	} PrtD;
	PrtD prt;
	TVector3 POS;
	TLorentzVector PL;
	TTree *T = new TTree("Particles","Nucleus Particles");
	T->Branch("Descrp",&prt,"IX/I:PID:Bind/O");
	T->Branch("Pos","TVector3",&POS);
	T->Branch("P","TLorentzVector",&PL);
	for (int i = 0; i < this->A; i++){
		prt.ix = i;
		prt.pid = this->nucleons[i].PdgId();
		prt.Bnd = this->nucleons[i].IsBind();
		POS = this->nucleons[i].Position();
		PL = this->nucleons[i].Momentum();
		T->Fill();
	}
	TTree *N = new TTree("Nucleus","Nucleus Tree");
	N->Branch("Particles","TTree",&T);
	N->Branch("Levels","TTree",&L);
	N->Fill();
	delete T;
	delete L;
	return N;
}

#ifdef THERMA



void NucleusDynamics::TakeProtonLevelsSnapshot(TString moment){

	if(moment.CompareTo("initial") == 0){
		for(int i=0; i<30; i++){
			occupationInitial_Proton[i] = ProtonFermiLevels(i) / (double)ProtonFermiLevels().GetMaxLevel(i);
		}
	}
	else if(moment.CompareTo("sys_closed") == 0){
		for(int i=0; i<30; i++){
			occupationSysClosed_Proton[i] = ProtonFermiLevels(i) / (double)ProtonFermiLevels().GetMaxLevel(i);
		}
	}
	else if(moment.CompareTo("final") == 0){
		for(int i=0; i<30; i++){
			occupationFinal_Proton[i] = ProtonFermiLevels(i) / (double)ProtonFermiLevels().GetMaxLevel(i);
		}
	}	
}

void NucleusDynamics::TakeNeutronLevelsSnapshot(TString moment){

	if(moment.CompareTo("initial") == 0){
		for(int i=0; i<30; i++){
			occupationInitial_Neutron[i] = NeutronFermiLevels(i) / (double)NeutronFermiLevels().GetMaxLevel(i);
		}
	}
	else if(moment.CompareTo("sys_closed") == 0){
		for(int i=0; i<30; i++){
			occupationSysClosed_Neutron[i] = NeutronFermiLevels(i) / (double)NeutronFermiLevels().GetMaxLevel(i);
		}
	}
	else if(moment.CompareTo("final") == 0){
		for(int i=0; i<30; i++){
			occupationFinal_Neutron[i] = NeutronFermiLevels(i) / (double)NeutronFermiLevels().GetMaxLevel(i);
		}
	}	
}

double NucleusDynamics::GetOccupationProton(int i, TString moment){
	
	if(moment.CompareTo("initial") == 0){
		return occupationInitial_Proton[i];
	}
	else if(moment.CompareTo("sys_closed") == 0){
		return occupationSysClosed_Proton[i];
	}
	else if(moment.CompareTo("final") == 0){
		return occupationFinal_Proton[i];
	}
}	

double NucleusDynamics::GetOccupationNeutron(int i, TString moment){
	
	if(moment.CompareTo("initial") == 0){
		return occupationInitial_Neutron[i];
	}
	else if(moment.CompareTo("sys_closed") == 0){
		return occupationSysClosed_Neutron[i];
	}
	else if(moment.CompareTo("final") == 0){
		return occupationFinal_Neutron[i];
	}
}


#endif
