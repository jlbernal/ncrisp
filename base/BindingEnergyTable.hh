/* ================================================================================
 * 
 * 	Copyright 2012 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef BindingEnergyTable_HH
#define BindingEnergyTable_HH

#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TGraph2D.h"
#include "TF2.h"

#include <string>
#include <map>
#include <iostream>
#include <fstream>

#define ZMax 118
#define AMax 293

using namespace std;

class BindingEnergyTable {
  
public:
  
	double table[ZMax][AMax];
	//std::map< std::pair< int, int >, double > table;	

	BindingEnergyTable();
	void Push( int Z, int A, double bE);
	bool Exist( int Z, int A );
	
	// Funtion Get(Z, A) reads a file of initialy only experimental bindidng energy in search for a match.
	// In case of not finding a nucleus, the Pearson bindidng energy formula is fitted in a region limited in Z centered at
	// the nucleus of interest. The new evaluated bindidng energy enters the table in the same file for future use.
	// The original experimental only file can be found in folders data/BindingEnergy and exp_data/nuclear_masses 
	double Get( int Z, int A );
	void Dump();
	void ReadTable( std::string file );
	//! ler sem usar TGraph
	void ReadTable2( std::string file );
	
	#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BindingEnergyTable,0);
	#endif // CRISP_SKIP_ROOTDICT
};

#endif