/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __MesonsPool_pHH
#define __MesonsPool_pHH

#include "TObject.h"
#include "ParticleDynamics.hh"
#include <vector>
#include <cassert>
#include <map>
#include "TTree.h"
#include "DataHelper.hh"

class MesonsPool: public TObject{ ///	class used to store and manipulate mesons. 

public:  
	typedef std::map<Int_t , ParticleDynamics> MesonsMap;  /// map for Meson storage 
	typedef std::map<Int_t , ParticleDynamics>::iterator iterator; /// iterator definition for meson's map
	virtual ~MesonsPool(){} 
	MesonsPool():TObject(), mesons() { num_mesons = 0; totalEn = 0;}  
	Int_t PutMeson(ParticleDynamics& p);
	ParticleDynamics& operator[](Int_t i) { return mesons[i]; }  
	bool find( Int_t key ) { return ( mesons.find(key) != mesons.end() ); }
	MesonsMap::iterator begin() { return mesons.begin(); } 
	MesonsMap::iterator end() { return mesons.end(); } 
	Int_t size() { return mesons.size(); }
	void clear() { 
		num_mesons = 0;
		mesons.clear(); 
	}
	double TotalMass();
	double TotalCharge();  
	double TotalEnergy();
	double RetrieveTotalEn() { return totalEn;}
	void RemoveMeson(Int_t key) { mesons.erase(key);}   
	void AllKeys( Int_t* arr_keys );
	std::vector<int> GetKeys(); ///
	TTree* GetMesonPoolTree();
	Mes_Struct GetMesonStruct();

private:  
	MesonsMap mesons;	/// map for mesons
	Int_t num_mesons;  	/// mesons counter
	Double_t totalEn;		/// total energy

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef ( MesonsPool, 2);
#endif // CRISP_SKIP_ROOTDICT
};
#endif
