/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "MesonsPool.hh"
//#if defined(__CINT__)
//#endif
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(MesonsPool);
#endif // CRISP_SKIP_ROOTDICT
//_________________________________________________________________________________________________										
Int_t MesonsPool::PutMeson( ParticleDynamics& p ){
	Int_t key = num_mesons;
	// assert(p.hasValidParticleData());  
	// std::pair<int, ParticleDynamics> m(key, p);
	// mesons.insert(m);    
	// std::cout << mesons[key].position() << std::endl;
	mesons[key] = p;
	mesons[key].BindIt(true);;
/*	ParticleDynamics &q = mesons[key];  
	assert ( q.HasValidParticleData() );  
	//put the mesons bind mass
	q.SetPosition(p.Position());
	q.SetMomentum(p.Momentum());
	q.SetMass( CPT::effd * q.GetInvariantMass() );   
	q.BindIt(true);  
*/	num_mesons++;
	return key;
}
//_________________________________________________________________________________________________										
Double_t MesonsPool::TotalMass(){
	Double_t total_mass = 0.;  
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		if ( it->second.IsBind() )
			total_mass += it->second.GetMass();
	}
	return total_mass;
}
//_________________________________________________________________________________________________										
Double_t MesonsPool::TotalCharge(){
	Double_t total_charge = 0.;
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		if ( it->second.IsBind() ) 
			total_charge += it->second.GetParticleData()->Charge()/3.; //ROOT gives the charge in units of |e|/3
	}
	return total_charge;
}
//_________________________________________________________________________________________________			
Double_t MesonsPool::TotalEnergy(){
	Double_t total_energy = 0.;
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		if ( it->second.IsBind() ) 
			total_energy += it->second.Momentum().E();
	}	
	return total_energy;
}
//_________________________________________________________________________________________________	
void MesonsPool::AllKeys( Int_t* arr_keys ){
	Int_t i = 0;
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		arr_keys[i] = it->first;
	}
}
//_________________________________________________________________________________________________										
std::vector<int> MesonsPool::GetKeys(){
	std::vector<int> v;
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		v.push_back(it->first);
	}
	return v;
}
//_________________________________________________________________________________________________										
TTree* MesonsPool::GetMesonPoolTree(){
	typedef struct {
		Int_t ix,pid; 
		bool Bnd;
	} PrtD;
	PrtD prt;
	TVector3 POS;
	TLorentzVector PL;
	TTree *T = new TTree("Particles","Nucleus Particles");
	T->Branch("Descrp",&prt,"IX/I:PID:Bind/O");
	T->Branch("Pos","TVector3",&POS);
	T->Branch("P","TLorentzVector",&PL);
	for (int i = 0; i < this->num_mesons; i++){
		prt.ix = i;
		prt.pid = this->mesons[i].PdgId();
		prt.Bnd = this->mesons[i].IsBind();
		POS = this->mesons[i].Position();
		PL = this->mesons[i].Momentum();
		T->Fill();
	}
	return T;
}
//_________________________________________________________________________________________________
Mes_Struct MesonsPool::GetMesonStruct(){
	Mes_Struct MS;
	MS.NM = this->mesons.size();
	int i = 0;
	for ( MesonsMap::iterator it = mesons.begin(); it != mesons.end(); it ++ ) {
		MS.Mes_KEY[i] = it->first ;
		MS.Mes_PID[i] = it->second.PdgId();
		MS.Mes_IsB[i] = it->second.IsBind();
		MS.Mes_X[i] = it->second.Position().X();
		MS.Mes_Y[i] = it->second.Position().Y();
		MS.Mes_Z[i] = it->second.Position().Z();

		MS.Mes_P[i] = it->second.Momentum().Vect().Mag();
		MS.Mes_PX[i] = it->second.Momentum().Px();
		MS.Mes_PY[i] = it->second.Momentum().Py();
		MS.Mes_PZ[i] = it->second.Momentum().Pz();
		MS.Mes_E[i] = it->second.Momentum().E();
		MS.Mes_K[i] = it->second.TKinetic();
		if (it != mesons.end())i++;
	}
	return MS;

}
