/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef ParticleDynamics_pHH
#define ParticleDynamics_pHH

#include <iostream>
#include <string>
#include "TString.h"
#include "TParticlePDG.h"
#include "TRandom.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include "Dynamics.hh"
#include "CrispParticleTable.hh"

/// Contains the basic interface of particles dynamics, the energy, momentum, kinetics Energy, etc.
class ParticleDynamics: public Dynamics{
private:    
	bool _isBind;  
	int pid;              /// store the particle id
public:
	virtual ~ParticleDynamics() { }
	ParticleDynamics(); 
	ParticleDynamics (int id);
	ParticleDynamics (const ParticleDynamics& u);  
	ParticleDynamics& operator= (const ParticleDynamics& u);
	int PdgId() const { return pid; }   
	double HalfLife();
 	inline bool IsBind() const;
	inline void BindIt(bool b = true);  
	TParticlePDG* GetParticleData();
	inline bool HasValidParticleData() const;
	inline std::string Name() const;
	inline double GetInvariantMass() const;
	inline double GetTotalWidth() const;  
	virtual void Print(const Option_t* opt) const;  
	virtual void Delete(const Option_t* opt) const; 
	virtual void Copy(TObject& obj) const;
	TString ToString() const;
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(ParticleDynamics,2);
#endif // CRISP_SKIP_ROOTDICT
};
#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(ParticleDynamics);
#endif // CRISP_SKIP_ROOTDICT
bool ParticleDynamics::IsBind() const{return _isBind;}
void ParticleDynamics::BindIt(bool b){_isBind = b;}  
std::string ParticleDynamics::Name()const{return std::string(CPT::Instance()->ParticlePDG(pid)->GetName());}
bool  ParticleDynamics::HasValidParticleData() const{return ( CPT::Instance()->ParticlePDG(pid) != 0 );}
double ParticleDynamics::GetInvariantMass() const{ return CPT::Instance()->ParticlePDG(pid)->Mass() * 1000.; }
double ParticleDynamics::GetTotalWidth() const{return CPT::Instance()->ParticlePDG(pid)->Width() * 1000.; }
#endif
