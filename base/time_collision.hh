/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __time_collision_pHH
#define __time_collision_pHH

#include "TVector3.h"
#include "TLorentzVector.h"
#include "TMath.h"

#include "base_defs.hh"
#include "ParticleDynamics.hh"

/// Return the squared impact parameter (fm^2).
/// This function does pretty much the same thing time_collision (...)
///
/// @param p1 a Particle
/// @param p2 a Particle
//_________________________________________________________________________________________________										

double impact_parameter2( ParticleDynamics& p1, ParticleDynamics& p2 );

/// Return the squared impact parameter (fm^2).
///
/// @param p1 a Particle
/// @param p2 a Particle
//_________________________________________________________________________________________________										

double impact_parameter2_2( ParticleDynamics& p1, ParticleDynamics& p2 );

/// calculate the time collision of two particles.
///
/// @param p1 a Particle
/// @param p2 a Particle
/// @param totalCrossSection the total cross section in mbarn
//_________________________________________________________________________________________________										

double time_collision( ParticleDynamics& p1, ParticleDynamics& p2, double totalCrossSection );

#endif
