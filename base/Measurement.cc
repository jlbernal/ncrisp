/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Measurement.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(Measurement); 
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

Measurement::Measurement(double value, double sigma, double stat_err):TObject(), val(value), sig(sigma), err(stat_err){}

//_________________________________________________________________________________________________										

Measurement::Measurement(double value, double sigma, std::vector<double> chann_value, std::vector<double> chann_sigma):TObject(), val(value), sig(sigma), chann_val(chann_value), chann_sig(chann_sigma){}

//_________________________________________________________________________________________________										

Measurement::Measurement():TObject(), val(NaN()),sig(NaN()){}

//_________________________________________________________________________________________________										

Measurement::Measurement ( const Measurement &m ):TObject(), val( m.val ), sig( m.sig ), chann_val( m.chann_val ), chann_sig( m.chann_sig ){}

//_________________________________________________________________________________________________										

void Measurement::swap( Measurement & other ){

	std::swap( val, other.val );
	std::swap( sig, other.sig );
	std::swap( chann_val, other.chann_val );
	std::swap( chann_sig, other.chann_sig );
}

//_________________________________________________________________________________________________										

Measurement& Measurement::operator = ( Measurement const & rhs ){

	Measurement temp( rhs );
	swap( temp );
  
	return *this;
}

//_________________________________________________________________________________________________										

bool  Measurement::operator <  ( Measurement const & other ) const{

	return val < other.val;
}

//_________________________________________________________________________________________________										

bool  Measurement::operator == ( Measurement const & other ) const{

	return val == other.val;
}

//_________________________________________________________________________________________________										

