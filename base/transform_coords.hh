/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __transform_coords_pHH
#define __transform_coords_pHH

#include <cmath>
#include "TLorentzVector.h"
#include "TMath.h"
#include "ParticleDynamics.hh"

/// obscure method .... 
//_________________________________________________________________________________________________										

double tcol(double enr, double x, double psq);

/// rotate a vector in referecial the center of mass for the laboratory referencial.
///
/// @param Ptotal : the sum of the moments (p_i + q_i)
/// @param k : the vector to be rotate
/// @return rotate vector
//_________________________________________________________________________________________________										

TLorentzVector lorentzRotation( const TLorentzVector& pTotal, const TLorentzVector& k );


/// unrotate a vector in referecial the center of mass for the laboratory referencial.
///
/// @param Ptotal : the sum of the moments (p_i + q_i)
/// @param k : the vector to be unrotate
/// @return unrotate vector
//_________________________________________________________________________________________________										

TLorentzVector inverseLorentzRotation( const TLorentzVector& pTotal, const TLorentzVector& k );

#endif
