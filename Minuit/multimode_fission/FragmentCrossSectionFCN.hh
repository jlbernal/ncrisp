//====================================================================================================================
//Classe para ajuste dos parâmetros do modelo multimodal de fissão com evaporação incluída com Minuit2
//
//	Autor: Evandro A. Segundo					e-mail: evandro.segundo@gmail.com
//====================================================================================================================

#ifndef FRAGMENTCROSSSECTIONFCN_HH
#define FRAGMENTCROSSSECTIONFCN_HH

#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/FCNBase.h"
#include "Minuit2/MnScan.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnStrategy.h"

#include "TGraph.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"

#include "MultimodalFission.hh"
#include "Mcef.hh"
#include "Util.hh"

#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream> 
#include <exception>

#ifdef CRISP_MPI
#include "mpi.h"
#endif // CRISP_MPI

using namespace std;

struct DataMultimodalFissionStructMINUIT{
	std::vector<TString> nucleusName, generator, energy;
	std::vector<Int_t> A, Z;
	std::vector<Double_t> mu1, mu2, gamma1, gamma2, norm;
	Double_t *Vect_SI;
	Double_t *Vect_SII;
	Double_t *Vect_SIII;
	Double_t *Vect_SL;
	std::vector<Double_t*> SIParams;
	std::vector<Double_t*> SIIParams;
	std::vector<Double_t*> SIIIParams;
	std::vector<Double_t*> SLParams;
	DataMultimodalFissionStructMINUIT();
	Bool_t IsDataOk;
	Int_t MultFissNumber;
	void ReadData(fstream &file);
};


class FragmentCrossSectionFCN : public ROOT::Minuit2::FCNBase {

public:

	FragmentCrossSectionFCN(MultimodalFission&, long, const std::vector<double>& , const std::vector<double>&, 
				const std::vector<double>& );

	virtual ~FragmentCrossSectionFCN();

	virtual double operator()(const std::vector<double>&) const;
	virtual double Up() const { return err; }

	//! Atualiza os parametros calculados pelo MINUIT
	void UpdateParams(const std::vector<double>& v) const;
	//! Exibe os parametros
	void PrintParams(std::ostream&) const;

	void GenerateFrags() const;
	//void EvapFragmentos() const;
	TGraph HistoFragmentos() const;

private:

	MultimodalFission *mf;
	std::vector<double> mass;
	std::vector<double> csection;
	std::vector<double> inc;

	// MPI
	int rank;	//!< Rank do processo MPI
	int size;	//!< Numero de processos MPI

	//! Semente para numeros aleatorios
	long Seed;
	double err;	
	
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(FragmentCrossSectionFCN,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
