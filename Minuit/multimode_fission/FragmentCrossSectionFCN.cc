#include "FragmentCrossSectionFCN.hh"

DataMultimodalFissionStructMINUIT::DataMultimodalFissionStructMINUIT(){
	IsDataOk = false;
	MultFissNumber = 0;
}
void DataMultimodalFissionStructMINUIT::ReadData(fstream &file){
	while (true) {      
		if (!file.good ()) break;  
		string pdline, Stemp;
		double Dtemp;
		int i, Itemp;
		getline(file, pdline);
		char c = pdline.c_str()[0];
		if (c == '$'){
			string text;     
			istringstream istr(pdline.c_str() );      
			istr >> text;
			if ( text == "$NucleusName" ) {	
				istr >> Stemp;   
				nucleusName.push_back(Stemp);      
			}
			if ( text == "$MassIndex" ) {	
				istr >> Itemp; 
				A.push_back(Itemp);      
			}
			if ( text == "$NucleusCharge" ) {	
				istr >> Itemp;   
				Z.push_back(Itemp);      
			}
			if ( text == "$Generator" ) {	
				istr >> Stemp;   
				generator.push_back(Stemp);      
			}
			if ( text == "$Energy" ) {	
				istr >> Stemp;   
				energy.push_back(Stemp);      
			}
			if ( text == "$SIParameters" ) {
				i = 0;
				Vect_SI = new Double_t[3];
				Vect_SI[0] = 0.;
				Vect_SI[1] = 1.;
				Vect_SI[2] = 0.;
				while (istr >> Dtemp){
					Vect_SI[i] = Dtemp;
					i++;
				}
				SIParams.push_back(Vect_SI);
			}
			if ( text == "$SIIParameters" ) {
				i = 0;
				Vect_SII = new Double_t[3];
				Vect_SII[0] = 0.;
				Vect_SII[1] = 1.;
				Vect_SII[2] = 0.;
				while (istr >> Dtemp){
					Vect_SII[i] = Dtemp;
					i++;
				}
				SIIParams.push_back(Vect_SII);
			}
			if ( text == "$SIIIParameters" ) {
				i = 0;
				Vect_SIII = new Double_t[3];
				Vect_SIII[0] = 0.;
				Vect_SIII[1] = 1.;
				Vect_SIII[2] = 0.;
				while (istr >> Dtemp){
					Vect_SIII[i] = Dtemp;
					i++;
				}
				SIIIParams.push_back(Vect_SIII);
			}
			if ( text == "$SLParameters" ) {
				i = 0;
				Vect_SL = new Double_t[2];
				Vect_SL[0] = 0.;
				Vect_SL[1] = 1.;
				while (istr >> Dtemp){
					Vect_SL[i] = Dtemp;
					i++;
				}
				SLParams.push_back(Vect_SL);
			}
			if ( text == "$mu1" ) {	
				istr >> Dtemp;   
				mu1.push_back(Dtemp);      
			}
			if ( text == "$mu2" ) {	
				istr >> Dtemp;   
				mu2.push_back(Dtemp);      
			}
			if ( text == "$gamma1" ) {	
				istr >> Dtemp;   
				gamma1.push_back(Dtemp);      
			}
			if ( text == "$gamma2" ) {	
				istr >> Dtemp;   
				gamma2.push_back(Dtemp);      
			}
			if ( text == "$norm" ) {	
				istr >> Dtemp;   
				norm.push_back(Dtemp);      
			}
		}
	}
	if( (nucleusName.size() == generator.size()) && (nucleusName.size() == A.size()) && (nucleusName.size() == Z.size()) && (nucleusName.size() == energy.size()) && (nucleusName.size() == SIParams.size()) && (nucleusName.size() == SIIParams.size()) && (nucleusName.size() == SIIIParams.size()) && (nucleusName.size() == SLParams.size()) && (nucleusName.size() == mu1.size()) && (nucleusName.size() == mu2.size()) && (nucleusName.size() == gamma1.size()) && (nucleusName.size() == gamma2.size()) && (nucleusName.size() == norm.size())){
		IsDataOk = true;
		MultFissNumber = nucleusName.size();
	}
}

FragmentCrossSectionFCN::FragmentCrossSectionFCN(MultimodalFission& multF,
						 long Seed,
						 const std::vector<double>& m, 
						 const std::vector<double>& cs, 
						 const std::vector<double>& er) : mass(m),
																									 		 csection(cs),
																									 		 inc(er) 
{
	this->mf = new MultimodalFission(multF);
	this->Seed = Seed;
	this->err = 1.;

#ifdef CRISP_MPI
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
#else
	rank = 0;
	size = 1;
#endif // CRISP_MPI
}

FragmentCrossSectionFCN::~FragmentCrossSectionFCN(){

	delete mf;
}

int NPar(1); //declarado global porque "FragmentCrossSectionFCN::operator() const" não aceita acesso a
				 //atributos da classe
double BestParcialChi = -1.0;
int NAjustes = 0;	// Numero de ajustes feitos

double FragmentCrossSectionFCN::operator()(const std::vector<double>& par) const {

	for(uint i(0); i < par.size(); i++){
		if( isNaN(par[i]) ){	
			std::cerr << "Erro no parametro " << i << " seu valor esta indefinido\n*Erro Fatal!\n";	
			return infty; 
		}
	}

	NPar = par.size();
	
#ifdef CRISP_MPI
	if( rank == 0 )
	{
		// manda a quantidade de parametros para outros hosts
		MPI_Bcast( &NPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
		std::vector< double > par2;
		par2 = par;
		MPI_Bcast( &par2[0], par2.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
	}
#endif // CRISP_MPI

	// Atualiza os parametros
	UpdateParams(par);

	if(rank == 0)
	{
		PrintParams(std::cout);
		std::cout << std::endl << std::endl;
	}

	//Re-seta Seed do Random para que a aletoriedade do Monte Carlo nao atrapalhe o MINUIT
	gRandom->SetSeed( this->Seed );

//	MPI_Barrier( MPI_COMM_WORLD );

	try{
		GenerateFrags();
	}
	catch(int &contTent){

		std::cout << setprecision(5) << "Ajuste: " << NAjustes << "\t\t\t\t\t Melhor Chi2 parcial: " << BestParcialChi << std::endl;
		//if(rank == 0){
			std::cerr << "\n\n ================================================================ \n\n";
			std::cerr << "\tErro Fatal no rank " << rank << "! Nao foi possivel sortear uma massa \n";
			std::cerr << "\tcom este conjunto de parametros, apos " << contTent << " tentativas. \n";
			std::cerr << "\n ================================================================ \n\n";
		//}
		return infty;		
	}
	catch(double &z_frag){

		std::cout << setprecision(5) << "Ajuste: " << NAjustes << "\t\t\t\t\t Melhor Chi2 parcial: " << BestParcialChi << std::endl;
		//if(rank == 0){
			std::cerr << "\n\n =============================================================== \n\n";
			std::cerr << "\tErro Fatal no rank " << rank << "! Nao eh possivel sortear um fragmento \n";
			std::cerr << "\tcom este conjunto de parametros. \n";
			std::cerr << "\n =============================================================== \n\n";
		//}
		return infty;
	}
	catch(char &error_string){

		if(rank == 0) std::cout << setprecision(5) << "Ajuste: " << NAjustes << "\t\t\t\t\t Melhor Chi2 parcial: " << BestParcialChi << std::endl;
		if(rank == 0){
			std::cerr << "\n\n ============================================================= \n\n";
			std::cerr << "\tErro Fatal! \n";
			std::cerr << "\t" << error_string << "\n";
			std::cerr << "\n ============================================================= \n\n";
		}
		return infty;

	}
	catch(exception &Ex){

		std::cerr << Ex.what() << endl;
	}

	//EvapFragmentos();

	TGraph fcsGraph = HistoFragmentos();

	double chi2 = 0.;

	//usando erro estatístico 1./sqrt(número de frequência)
	for(unsigned int n = 0; n < mass.size(); n++) {
		chi2 += ( ((fcsGraph.Eval(mass[n]) - csection[n])/(1./sqrt(csection[n]))) * 
				    ((fcsGraph.Eval(mass[n]) - csection[n])/(1./sqrt(csection[n]))) );
	}

	chi2 = chi2 / ( mass.size()-par.size() );

	if(BestParcialChi < 0.0)
		BestParcialChi = chi2;
	else
		if(BestParcialChi > chi2)
			BestParcialChi = chi2;

	NAjustes++;

	if(rank == 0){
		std::cout << setprecision(5) << "\t\t\t\t\t\t\t\tChi2: " << chi2 << std::endl;
		std::cout << setprecision(5) << "Ajuste: " << NAjustes << "\t\t\t\t\t Melhor Chi2 parcial: " << BestParcialChi << std::endl;
		std::cout << "====================================================================================" << std::endl << std::endl;
	}

	return chi2;
}


void FragmentCrossSectionFCN::GenerateFrags() const{

    //======== Lendo arquivo das massas nucleares e gravando dados em vetores
	 if(rank == 0)  cout << endl << "Saving data in vectors..." << endl << endl;

	 std::ifstream ifs;
	 Double_t A_simm = 0.;
	 Double_t Z_simm = 0.;
	 Int_t A_fissao = 0.;
	 Int_t Z_fissao = 0.;
	 Double_t E_fissao = 0.;
	 vector <Double_t> vectA_simm;
	 vector <Double_t> vectZ_simm;
	 vector <Double_t> vectA_fissao;
	 vector <Double_t> vectZ_fissao;
	 vector <Double_t> vectE_fissao;

	 double Amean = 0.;
	 double Zmean = 0.;

	 TString fname_in = "results/mcef/" + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_mcef.root";

	 TFile *F = new TFile(fname_in.Data());
	 TTree *t = (TTree*)F->Get("history Fission");
	 TBranch *Fission = (TBranch*)t->GetBranch("Fission");
	 int n = Fission->GetEntries();
	 TLeaf *l1 = (TLeaf*)Fission->GetLeaf("fiss_A");
	 l1->SetAddress(&A_fissao);
	 TLeaf *l2 = (TLeaf*)Fission->GetLeaf("fiss_Z");
	 l2->SetAddress(&Z_fissao);
	 TLeaf *l3 = (TLeaf*)Fission->GetLeaf("fiss_E");
	 l3->SetAddress(&E_fissao);
	 
	 for(int i=0; i<n; i++){
	   
		  Fission->GetEntry(i);
		  A_simm = A_fissao / 2.;
		  Amean += A_simm;
		  Z_simm = Z_fissao / 2.;
		  Zmean += Z_simm;

		  vectA_simm.push_back(A_simm);
		  vectZ_simm.push_back(Z_simm);
		  vectA_fissao.push_back(double(A_fissao));
		  vectZ_fissao.push_back(double(Z_fissao));
		  vectE_fissao.push_back(E_fissao);
	 }

	 Amean = Amean / vectA_fissao.size();
	 Zmean = Zmean / vectA_fissao.size();

	 double Amin = Amean - 6.*(mf->Getsigma_s());
	 if(Amin<0.) Amin = 0.;

	 double Amax = Amean + 6.*(mf->Getsigma_s());

	 // --- MPI ---------------------------------------------------------------


	 int masterNRuns = (int)( vectA_fissao.size() % size);
	 int hostsNRuns = (int)( vectA_fissao.size() / size);

	 int start = hostsNRuns * rank;
	 int end = hostsNRuns * (rank+1);

	 if( rank == 0 ) cout << ": hostsNRuns: " << hostsNRuns << " x " << size << " + masterExtraRuns: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n\n";


	 double *mpi_vectA = new double[vectA_fissao.size() * 10];
	 double *mpi_vectZ = new double[vectA_fissao.size() * 10];
	 double *mpi_vectE = new double[vectA_fissao.size() * 10];
	 int mpi_vect_size = 0;

	 double *finalVectorA = NULL;
	 double *finalVectorZ = NULL;
	 double *finalVectorE = NULL;

	 double *extraVectorA = NULL;
	 double *extraVectorZ = NULL;
	 double *extraVectorE = NULL;
	 int extra_vect_size = 0;


//======== sorteio dos fragmentos

	 if(rank == 0) cout << endl << "Calculating the fragments..." << endl << endl;

	 if( gRandom ){
		  delete gRandom;
		  gRandom = new TRandom3( primos[rank] );
	 }

	 int i = 0,
		  l = 0;

	 double a_frag = 0.,
			  z_frag = 0.,
			  e_frag1 = 0.,
			  e_frag2 = 0.;
	
	 try{
		for( i = start; i < end; i++ ){
		
		  a_frag = mf->CrossSectionA(Amin, vectA_simm[i], Amax, l);
		  z_frag = mf->CrossSectionZ(a_frag);
		  e_frag1 = (a_frag/vectA_fissao[i]) * (vectE_fissao[i]);
		  e_frag2 = ((vectA_fissao[i]-a_frag)/vectA_fissao[i]) * (vectE_fissao[i]);
		  
		  for(int k=0; k<l; k++){
			 mpi_vectA[mpi_vect_size] = a_frag;
			 mpi_vectZ[mpi_vect_size] = z_frag;
			 mpi_vectE[mpi_vect_size] = e_frag1;
			 mpi_vectA[mpi_vect_size+1] = vectA_fissao[i] - a_frag;
			 mpi_vectZ[mpi_vect_size+1] = vectZ_fissao[i] - z_frag;
			 mpi_vectE[mpi_vect_size+1] = e_frag2;
			 mpi_vect_size += 2;
		  }

		  l = 0;
	  }
	}
	catch(int &contTent){
		throw contTent;
	}
	catch(exception &Ex){
		throw Ex;
	}

	int final_size = mpi_vect_size;
#ifdef CRISP_MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Reduce( &mpi_vect_size, &final_size, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD );

	if(rank == 0){
		finalVectorA = new double[final_size];
		finalVectorZ = new double[final_size];
		finalVectorE = new double[final_size];

		if( masterNRuns > 0 ){
			extraVectorA = new double[masterNRuns*10];
			extraVectorZ = new double[masterNRuns*10];
			extraVectorE = new double[masterNRuns*10];
		}
	}

	int *rcounts = new int[size];
	int *displs = new int[size];

	MPI_Gather( &mpi_vect_size, 1, MPI_INT, rcounts, 1, MPI_INT, 0, MPI_COMM_WORLD);	

	if(rank == 0){
		displs[0] = 0;
   	for(i=1; i<size; i++) {
   		displs[i] = displs[i-1]+rcounts[i-1];
   	}
	}

	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Gatherv(mpi_vectA, mpi_vect_size, MPI_DOUBLE, finalVectorA, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectZ, mpi_vect_size, MPI_DOUBLE, finalVectorZ, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Gatherv(mpi_vectE, mpi_vect_size, MPI_DOUBLE, finalVectorE, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	delete[] mpi_vectA;
	delete[] mpi_vectZ;
	delete[] mpi_vectE;
	delete[] rcounts;
	delete[] displs;
#endif // CRISP_MPI

//masterNRuns

	if(rank == 0){
		
		try{
		  for( i = size*hostsNRuns; i < size*hostsNRuns+masterNRuns; i++ ){
			 
			 a_frag = mf->CrossSectionA(Amin, vectA_simm[i], Amax, l);
			 z_frag = mf->CrossSectionZ(a_frag);
			 e_frag1 = (a_frag/vectA_fissao[i]) * (vectE_fissao[i]);
			 e_frag2 = ((vectA_fissao[i]-a_frag)/vectA_fissao[i]) * (vectE_fissao[i]);
				  
			 for(int k=0; k<l; k++){
				  extraVectorA[extra_vect_size] = a_frag;
				  extraVectorZ[extra_vect_size] = z_frag;
				  extraVectorE[extra_vect_size] = e_frag1;
				  extraVectorA[extra_vect_size+1] = vectA_fissao[i] - a_frag;
				  extraVectorZ[extra_vect_size+1] = vectZ_fissao[i] - z_frag;
				  extraVectorE[extra_vect_size+1] = e_frag2;
				  extra_vect_size += 2;
			 }
			 l = 0;
		  }
		}
		catch(int &contTent){
			throw contTent;
		}
		catch(exception &Ex){
			throw Ex;
		}
	}

#ifdef CRISP_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif // CRISP_MPI

	if( rank == 0 ){

		cout << endl << "Fragments evaporation..." << endl << endl;
		
		Int_t final_a = 0;
		Int_t final_z = 0;
		Int_t neutron = 0;
		Int_t proton = 0;
		Int_t alpha = 0;
		Double_t A = 0.;
		Double_t Z = 0.;
		
		TString prefix = "results/MultimodalFission/";
		gSystem->mkdir( ( prefix + mf->GetGenerator() + "/" + mf->GetNucleus()).Data() , kTRUE);
		TString fname_out = prefix + mf->GetGenerator() + "/" + mf->GetNucleus() + "/" + mf->GetNucleus() + "_" + mf->GetEnergy() + "_fragmentsMIN.root";
		
		TFile file(fname_out.Data(), "recreate");
		TTree *Hist = new TTree("history", "MultimodalFission MINUIT Tree");
		Hist->Branch("FragA",&final_a,"A/I");
		Hist->Branch("FragZ",&final_z,"Z/I");
		Hist->Branch("neutron",&neutron,"Neu/I");
		Hist->Branch("proton",&proton,"Pro/I");
		Hist->Branch("alpha",&alpha,"Alp/I");

		Mcef *mcef = new Mcef();

		for(int j=0; j < final_size; j++){
			#ifdef CRISP_MPI
			A = cint(finalVectorA[j]);
			Z = cint(finalVectorZ[j]);
			//cout << "pegou fragmentos - A: " << A << " Z: " << Z << " E: " << finalVectorE[j] << endl;
			mcef->Generate((int)A, (int)Z, finalVectorE[j]);
			//cout << "gerou evaporacao" << endl;
			#else // CRISP_MPI
			A = cint(mpi_vectA[j]);
			Z = cint(mpi_vectZ[j]);
			mcef->Generate((int)A, (int)Z, mpi_vectE[j]);
			#endif
			if(mcef->NumberFissions() == 0){
				//cout << "evaporou fragmentos" << endl;
				mcef->FinalConfiguration((int)A, (int)Z, final_a, final_z);
				//cout << "configuracao final" << endl;
				if(final_a > 0 && final_z > 0){
					//cout << "configuracao final valida" << endl;
					neutron = mcef->NeutronMultiplicity();
					proton = mcef->ProtonMultiplicity();
					alpha = mcef->AlphaMultiplicity();
					Hist->Fill();
					//cout << "preencheu historico" << endl;
				}
			}
		}

		for(int j=0; j < extra_vect_size; j++){

		    A = cint(extraVectorA[j]);
		    Z = cint(extraVectorZ[j]);
		    mcef->Generate((int)A, (int)Z, extraVectorE[j]);
		    if(mcef->NumberFissions() == 0){
				  mcef->FinalConfiguration((int)A, (int)Z, final_a, final_z);
				  if(final_a > 0 && final_z > 0){
						neutron = mcef->NeutronMultiplicity();
						proton = mcef->ProtonMultiplicity();
						alpha = mcef->AlphaMultiplicity();
						Hist->Fill();
				  }
		    }
		}
		Hist->Write();
		file.Close();	
		delete mcef;
		delete Hist;
	}
	
	delete[] finalVectorA;
	delete[] finalVectorZ;
	delete[] finalVectorE;
	delete[] extraVectorA;
	delete[] extraVectorZ;
	delete[] extraVectorE;
}


TGraph FragmentCrossSectionFCN::HistoFragmentos() const{

	if(rank == 0) std::cout << "Creating Graph (Interpolation)..." << std::endl << std::endl;

	TString exp_file = "exp_data/data_Multimode/" + (this->mf)->GetGenerator() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + ".data";
	TGraphErrors *exp = new TGraphErrors(exp_file.Data(), "%lg %lg %lg");
	
	TString prefix = "results/MultimodalFission/";
	TString fname_in = prefix + (this->mf)->GetGenerator() + "/" + (this->mf)->GetNucleus() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + "_fragmentsMIN.root";

	Int_t A = 0, Z = 0;
	TFile F(fname_in.Data());
	TTree *t = (TTree*)F.Get("history");
	t->SetBranchAddress("FragA",&A);
	t->SetBranchAddress("FragZ",&Z);
	int n = t->GetEntries();

	Double_t Amin = (this->mf)->GetA(),
			   Amax = 0;
	Double_t Atemp = 0.;

	for(int i=0; i<exp->GetN(); i++){
		Atemp = exp->GetX()[i];
		if(Atemp < Amin) {
		  Amin = Atemp;
		}
		if(Atemp > Amax) {
		  Amax = Atemp;
		}		
	}
	Amin = (int)Amin-10;
	Amax = (int)Amax+10;
	TH1D *h = new TH1D("hCalc",((this->mf)->GetNucleus()).Data(),Amax-Amin,Amin,Amax);
  
	for(int i=0; i<n; i++){
		t->GetEntry(i);
		if(A > Z){
		  h->Fill(A);
		} else if(A <= Z){
		  if(rank == 0) cout << "Wrong entry on fragments file: A = " << A << ", Z = " << Z << endl << endl;
		}
	}
  
	Double_t scale = ((this->mf)->Getnorm())/(h->Integral());
	h->Scale(scale);
  
	TGraph *gr = new TGraph(h);

	delete h;
	delete exp;
	delete t;

	return *gr;
}



void FragmentCrossSectionFCN::UpdateParams(const std::vector<double>& v) const
{
	//standard I
	(this->mf)->SetK1as(v[0]);
   (this->mf)->Setsigma_1as(v[1]);        
   (this->mf)->SetD1as(v[2]);

	//standard II  
   (this->mf)->SetK2as(v[3]);                    
   (this->mf)->Setsigma_2as(v[4]);    
   (this->mf)->SetD2as(v[5]);      

	//standard III  
   (this->mf)->SetK3as(v[6]);                    
   (this->mf)->Setsigma_3as(v[7]);    
   (this->mf)->SetD3as(v[8]);  
  
	//superlong
   (this->mf)->SetKs(v[9]);  
   (this->mf)->Setsigma_s(v[10]);

	//normalização
   (this->mf)->Setnorm(v[11]);
}

void FragmentCrossSectionFCN::PrintParams(std::ostream& os) const
{
// 	os << std::endl;
// 	os << std::setprecision(5);
// 	os << "K1as = [" << std::setw(13) << (this->mf)->GetK1as() << "]";
// 	os << "\t\tK2as = [" << std::setw(13) << (this->mf)->GetK2as() << "]";
// 	os << "\t\tKs = [" << std::setw(12) << (this->mf)->GetKs() << "]";
// 	os << std::endl;
// 	os << "Kl1as = [" << std::setw(12) << (this->mf)->GetKl1as() << "]";
// 	os << "\t\tKl2as = [" << std::setw(12) << (this->mf)->GetKl2as() << "]";
// 	os << "\t\tsigma_s = [" << std::setw(7) << (this->mf)->Getsigma_s() << "]";
// 	os << std::endl;
// 	os << "sigma_1as = [" << std::setw(8) << (this->mf)->Getsigma_1as() << "]";
// 	os << "\t\tsigma_2as = [" << std::setw(8) << (this->mf)->Getsigma_2as() << "]";
// 	os << std::endl;
// 	os << "sigmal1as = [" << std::setw(8) << (this->mf)->Getsigmal1as() << "]";
// 	os << "\t\tsigmal2as = [" << std::setw(8) << (this->mf)->Getsigmal2as() << "]";
// 	os << std::endl;
// 	os << "D1as = [" << std::setw(13) << (this->mf)->GetD1as() << "]";
// 	os << "\t\tD2as = [" << std::setw(13) << (this->mf)->GetD2as() << "]";
// 	os << "\t\tNorm = [" << std::setw(10) << (this->mf)->Getnorm() << "]";
// 	os << std::endl;
	
		os << std::endl;
		os << std::setprecision(5);
		os << "K1as = [" << std::setw(13) << (this->mf)->GetK1as() << "]";
		os << "\t\tK2as = [" << std::setw(13) << (this->mf)->GetK2as() << "]";
		os << "\t\tK3as = [" << std::setw(13) << (this->mf)->GetK3as() << "]";
		os << "\t\tKs = [" << std::setw(12) << (this->mf)->GetKs() << "]";
		os << std::endl;
		os << "sigma_1as = [" << std::setw(8) << (this->mf)->Getsigma_1as() << "]";
		os << "\t\tsigma_2as = [" << std::setw(8) << (this->mf)->Getsigma_2as() << "]";
		os << "\t\tsigma_3as = [" << std::setw(8) << (this->mf)->Getsigma_3as() << "]";
		os << "\t\tsigma_s = [" << std::setw(7) << (this->mf)->Getsigma_s() << "]";
		os << std::endl;
		os << "D1as = [" << std::setw(13) << (this->mf)->GetD1as() << "]";
		os << "\t\tD2as = [" << std::setw(13) << (this->mf)->GetD2as() << "]";
		os << "\t\tD3as = [" << std::setw(13) << (this->mf)->GetD3as() << "]";
		os << "\t\tNorm = [" << std::setw(10) << (this->mf)->Getnorm() << "]";
		os << std::endl;
}


#if 0
/*
 * THIS PART WAS DEACTIVATED - EVAPORATION OF FRAGMENTS NOW RUN RIGHT AFTER THEIR GENERATION IN FUNTION GenerateFrags()
 */

void FragmentCrossSectionFCN::EvapFragmentos() const{

	TString frag = "results/" + (this->mf)->GetProjectile() + "/" + (this->mf)->GetNucleus() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + "_fragmentos_min.txt";
	TString fragEvapGen = "results/" + (this->mf)->GetProjectile() + "/" + (this->mf)->GetNucleus() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + "_evap_rank%d.mcef";
	TString fragEvapRank = Form(fragEvapGen.Data(), rank);

  	std::ifstream fragFile(frag.Data());
  	std::ofstream *ofs = new std::ofstream(fragEvapRank.Data());
	
	Mcef *mcef = new Mcef();
  	if(rank == 0) std::cout << "Running Evaporation of Fragments... " << std::endl << std::endl;

	float A = 0., Z = 0., energy = 0.; //TNtuple só funciona com float

	TNtuple *tuple = new TNtuple("AZEtupla","","A:Z:energy");
	int sizeTuple = 0;
	while ( true ) {
		if(!fragFile.good()) break;
		fragFile >> A >> Z >> energy;
		A = cint(A);
		Z = cint(Z);
		if ( energy > 0. ){
			tuple->Fill(A, Z, energy);   
			sizeTuple++;
		}
	}

	tuple->SetBranchAddress("A",&A);
	tuple->SetBranchAddress("Z",&Z);
	tuple->SetBranchAddress("energy",&energy);

	int masterNRuns = (int)( sizeTuple % size);
	int hostsNRuns = (int)( sizeTuple / size);
	int start = hostsNRuns * rank;
	int end = hostsNRuns * (rank+1);

	if( rank == 0 ) cout << ": hostsNRuns: " << hostsNRuns << " x " << size << " + masterExtraRuns: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n\n";
	for( int i = start; i < end; i++ ) {    
		tuple->GetEntry(i);
		mcef->Generate_evap( (int)A, (int)Z, energy, ofs); 
	}
	if(rank == 0){
		TString fragEvapExtra = "results/" + (this->mf)->GetProjectile() + "/" + (this->mf)->GetNucleus() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + "_evap_extra.mcef";
		std::ofstream *outExtra = new std::ofstream(fragEvapExtra.Data());
		for( int i = size*hostsNRuns; i < size*hostsNRuns+masterNRuns; i++ ){
			tuple->GetEntry(i);
			mcef->Generate_evap( (int)A, (int)Z, energy, outExtra); 
		}

		TString fragEvap = "results/" + (this->mf)->GetProjectile() + "/" + (this->mf)->GetNucleus() + "/" + (this->mf)->GetNucleus() + "_" + (this->mf)->GetEnergy() + "_evap_min.mcef";
		TString com = "cat ";
		for(int rankCount = 0; rankCount < size; rankCount++){
			com = com + Form(fragEvapGen.Data(), rankCount) + " ";
		}
		com = com + fragEvapExtra + " > " + fragEvap;
		system(com.Data());
		TString com2 = "rm results/" + (this->mf)->GetProjectile() + "/" + (this->mf)->GetNucleus() + "/*rank* " + fragEvapExtra;
		system(com2.Data());
		delete outExtra;
	}
	delete ofs;
	delete tuple;
	delete mcef;
}
#endif




