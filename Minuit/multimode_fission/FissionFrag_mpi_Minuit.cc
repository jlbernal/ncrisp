#include "TStopwatch.h"

#include "MultimodalFission.hh"
#include "FragmentCrossSectionFCN.hh"

#include <vector>
#include <cmath>
#include <fstream>

using namespace std;

void minuitClient( FragmentCrossSectionFCN & );

int main( int argc, char *argv[] ){

  	fstream file;
	TString line = "--help";
	if (argc == 1) {
	  printf ("Syntax Error:  <program> [file]\n");
	  printf ("Type --help for help\n\n");
	  return 1;		
	}
	if (argc > 1 && line.CompareTo(argv [1]) == 0) {
	  printf ("Syntax:  <program> [file]\n\n");
	  printf ("file:  multimodal fission input file for MINUIT must have extension *.multFissMIN \n");
	  return 1;		
	}	
	file.open (argv [1], ios::in);
	if (!file.is_open ()) {
		  printf ("File not found: %s\n", argv [1]);
		  return 1;
	}
	 
	DataMultimodalFissionStructMINUIT Data;
	Data.ReadData(file);
	if (!Data.IsDataOk){
	  std::cout << "Input error\n";
	  return 1;
	}
	 
	TStopwatch sw;
	sw.Start();
	
	int rank(0);
	int size(1);

#ifdef CRISP_MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );		// rank of the processes
	MPI_Comm_size( MPI_COMM_WORLD, &size );		// number of processes
#endif // CRISP_MPI

	if( gRandom )
		delete gRandom;
	gRandom = new TRandom3( primos[rank] );

// 	CrispParticleTable *cpt = CrispParticleTable::Instance();
// 
// 	if( rank == 0 )
// 		std::cout << std::endl << "Loading PDG Particle Data ..." << std::endl;
// 		
// 	cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");

	if( rank == 0 )
		std::cout << "Ready." << std::endl;
	
#ifdef CRISP_MPI
	MPI_Barrier( MPI_COMM_WORLD );
#endif // CRISP_MPI

	MultimodalFission multF;
	
	for(int i=0; i<Data.MultFissNumber; i++){
		
		  multF.SetZ(Data.Z[i]);
		  multF.SetA(Data.A[i]);
		  multF.SetNucleus(Data.nucleusName[i]);
		  multF.SetEnergy(Data.energy[i]);
		  multF.SetGenerator(Data.generator[i]);

		  //standard I
		  multF.SetK1as((Data.SIParams[i])[0]);
		  multF.Setsigma_1as((Data.SIParams[i])[1]);
		  multF.SetD1as((Data.SIParams[i])[2]);

		  //standard II  
		  multF.SetK2as((Data.SIIParams[i])[0]);                      
		  multF.Setsigma_2as((Data.SIIParams[i])[1]);
		  multF.SetD2as((Data.SIIParams[i])[2]);  

		  //standard III  
		  multF.SetK3as((Data.SIIIParams[i])[0]);                      
		  multF.Setsigma_3as((Data.SIIIParams[i])[1]);
		  multF.SetD3as((Data.SIIIParams[i])[2]);     
		  
		  //superlong
		  multF.SetKs((Data.SLParams[i])[0]);  
		  multF.Setsigma_s((Data.SLParams[i])[1]);
		  
		  //parametrização do Z mais provável
		  //em função da massa do fragmento
		  multF.Setmu1(Data.mu1[i]);
		  multF.Setmu2(Data.mu2[i]);
		  multF.Setgamma1(Data.gamma1[i]);
		  multF.Setgamma2(Data.gamma2[i]);
		  multF.Setnorm(Data.norm[i]);

		  TString exp_file = "exp_data/data_Multimode/" + multF.GetGenerator() + "/" + multF.GetNucleus() + "_" + multF.GetEnergy() + ".data";

		  std::ifstream ifs;
		  Double_t A = 0.;
		  Double_t cs = 0.;
		  Double_t er = 0.;
		  vector <Double_t> vectA;
		  vector <Double_t> vectCs;
		  vector <Double_t> vectEr;

		  ifs.open(exp_file.Data(), ifstream::in);

		  while(true){

				if(!ifs.good()) break;

				ifs >> A >> cs >> er;

				vectA.push_back(A);
				vectCs.push_back(cs);
				vectEr.push_back(er);
		  }
		  ifs.close();

		  FragmentCrossSectionFCN FcsFCN(multF, primos[rank], vectA, vectCs, vectEr);

		  //ROOT::Minuit2::MnUserParameters upar;

		  if( rank != 0 )
		  {
				minuitClient(FcsFCN);
		  }
		  else
		  {
				double step1 = 2.;
				double step2 = 1.;
				ROOT::Minuit2::MnUserParameters upar;

				//standard I  
				upar.Add("K1as", multF.GetK1as(), step1);
				upar.Add("sigma_1as", multF.Getsigma_1as(), step2);      
				upar.Add("D1as", multF.GetD1as(), step1);   
				//standard II  
				upar.Add("K2as", multF.GetK2as(), step1);
				upar.Add("sigma_2as", multF.Getsigma_2as(), step2);      
				upar.Add("D2as", multF.GetD2as(), step1);   
				//standard III
				upar.Add("K3as", multF.GetK3as(), step1);
				upar.Add("sigma_3as", multF.Getsigma_3as(), step2);      
				upar.Add("D3as", multF.GetD3as(), step1);  
				//superlong
				upar.Add("Ks", multF.GetKs(), step1);
				upar.Add("sigma_s", multF.Getsigma_s(), step2);
				//normalização
				upar.Add("Norm", multF.Getnorm(), step1);

				//standard I
				upar.SetLimits("K1as", 0., 600.);
				upar.SetLimits("sigma_1as", 1., 60.);      
				upar.SetLimits("D1as", 1., 150.);   
				//standard II  
				upar.SetLimits("K2as", 0., 600.);
				upar.SetLimits("sigma_2as", 1., 60.);     
				upar.SetLimits("D2as", 1., 150.);   
				//standard III
				if(multF.GetK3as() == 0.){
					upar.Fix("K3as");
					upar.Fix("sigma_3as");
					upar.Fix("D3as");
				} else {
					upar.SetLimits("K3as", 0., 600.);
					upar.SetLimits("sigma_3as", 1., 60.);     
					upar.SetLimits("D3as", 1., 200.); 
				}
				//superlong
				upar.SetLimits("Ks", 0., 5000.);
				upar.SetLimits("sigma_s", 1., 60.);
				//normalização
				upar.SetLimits("Norm", 1., 10000.);

				if( rank == 0 ) std::cout << "\nExecutando [" << "SCAN" << "]: " << std::endl;
				ROOT::Minuit2::MnScan scan(FcsFCN, upar);
				ROOT::Minuit2::FunctionMinimum min = scan();
				if( rank == 0 ) std::cout << "\nMinimo [" << "SCAN" << "]: " << min << std::endl;

				upar = scan.Parameters();

				if( rank == 0 ) std::cout << "\nExecutando ["<< "MINIMIZE]" << std::endl;
				ROOT::Minuit2::MnMinimize minimize(FcsFCN, upar);
				ROOT::Minuit2::FunctionMinimum min2 = minimize();
				if( rank == 0 ) std::cout << "\nMinimo [" << "MINIMIZE]: " << min2 << std::endl;

				upar = minimize.Parameters();

#ifdef CRISP_MPI	
				// rank0 avisa aos outros hosts q o calculo do MINUIT acabou (valor 0)
				int nPar = 0;
				// manda a quantidade de parametros == 0 para dizer q terminou
				MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
#endif // CRISP_MPI
		  }
		/*
		  std::vector< double > v;
		  v = upar.Params();

		  // atualiza + 1 vez os valores dos parametros		
		  int nPar = upar.Params().size();		
		  if( rank == 0 )
		  {
				MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
				MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );	
		  }
		  else
		  {
				MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
				v.resize( nPar );
				MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );	
		  }

		  if(rank == 0){
				FcsFCN.UpdateParams(v);
				std::stringstream ss; ss << "results/Ajuste_fissao_multimodal.txt";
				std::ofstream ofs( ss.str().c_str() );
				ofs << upar;	
		  }
		*/
		  std::cout << "Rank[" << rank << "]" << " Terminando.\n";
		  #ifdef CRISP_MPI
		  MPI_Finalize();
		  #endif // CRISP_MPI
	}

  	sw.Stop();
	sw.Print(); 
}

void minuitClient( FragmentCrossSectionFCN &FcsFCN )
{
#ifdef CRISP_MPI
	int rank(0);
	int size(1);
	

	// MPI ----------------------------------------------------------------------
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // size dos processos
	// End MPI ------------------------------------------------------------------

	
	int nPar = 0;
	std::vector< double > v;
	
	while(true)
	{
		// Espera o Rank0 mandar o numero de parametros
		// int MPI_Recv(void*, int, ompi_datatype_t*, int, int, ompi_communicator_t*, MPI_Status*)
//		MPI_Recv( &nPar, 1, MPI_INT, 0,	MPI_COMM_WORLD );
		MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
		
		//std::cout << "Rank["<<rank<<"]" << " Recebeu nPar == " << nPar <<"\n";
		
		// termina quando receber 0
		if( nPar == 0 )
			break;
		
		v.resize( nPar );
		
		// atualiza parametros
		MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
		
		// executa uma iteração de calculo do chisqr
		FcsFCN(v);
	}
	#endif // CRISP_MPI
}
