
#ifndef stdafx_h
#define stdafx_h

#include <algorithm>
#include <cassert>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

#include "TVector3.h"
#include "Api.h"
#include "TAxis.h"
#include "TBuffer.h"
#include "TDatabasePDG.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TGenPhaseSpace.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TMath.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TMethodCall.h"
#include "TMultiGraph.h"
#include "TNamed.h"
#include "TNtuple.h"
#include "TParticlePDG.h"
#include "TPaveText.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TString.h"
#include "TSystem.h"
#include "TStyle.h"



#ifdef CRISP_MINUIT
#include "Minuit2/FCNBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnFumiliMinimize.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnUserParameters.h"
#endif // CRISP_MINUIT

#ifdef CRISP_MPI
#include "mpi.h"
#endif // CRISP_MPI

// primos entre 1 e 1000 que podem ser utilizados como sementes (estao desordenados)
#define NPRIMOS 168
const int primos[] = { 233, 61, 149, 239, 347, 443, 563, 659, 773, 887, 3, 67, 151, 241, 349, 449, 569, 661, 787, 907, 5, 71, 157, 251, 353, 457, 571, 673, 797, 911, 7, 73, 163, 257, 359, 461, 577, 677, 809, 919, 11, 79, 167, 263, 367, 463, 587, 683, 811, 929, 13, 83, 173, 269, 373, 467, 593, 691, 821, 937, 17, 89, 179, 271, 379, 479, 599, 701, 823, 941, 19, 97, 181, 277, 383, 487, 601, 709, 827, 947, 23, 101, 191, 281, 387, 491, 607, 719, 829, 953, 29, 103, 193, 283, 397, 499, 613, 727, 839, 967, 31, 107, 197, 293, 401, 503, 617, 733, 853, 971, 37, 109, 199, 307, 409, 509, 619, 739, 857, 977, 41, 113, 211, 311, 419, 521, 631, 743, 859, 983, 43, 127, 223, 313, 421, 523, 641, 751, 863, 991, 47, 131, 227, 317, 431, 541, 643, 757, 877, 997, 53, 137, 229, 331, 433, 547, 647, 761, 881, 59, 139, 2, 337, 439, 557, 653, 769, 883 };

typedef unsigned int uint;

#endif // stdafx_h
