
#include "Data.hh"

CascadeData::CascadeData( std::string Type, int A, int Z, int N, std::vector< double > Energies, std::string ExpData, double ExpMin, double ExpMax, std::string Results, std::string Graphs )
{
	NameTable n;
	this->NucleusName = n.get(Z);
	
	this->Type = Type;
	this->ExpData = ExpData;
	this->ExpMin = ExpMin;
	this->ExpMax = ExpMax;
	
	this->Energies = Energies;
	this->A = A;
	this->Z = Z;
	this->N = N;
	
	this->Results = Results;
	this->Graphs = Graphs;	
}



FissionData::FissionData() : NucleusName(""), A(0), Z(0), ExpData(""), ExpMin(0.0), ExpMax(0.0), Cascades(""), Results(""), Graphs(""), BarreiraFissao(""), DensidadeNiveis("")
{}

FissionData::FissionData( int A, int Z, std::vector< double > Energies, std::string ExpData, double ExpMin, double ExpMax, std::string Cascades, std::string Results, std::string Graphs, std::string BarreiraFissao, std::string DensidadeNiveis )
{
	NameTable n;
	this->NucleusName = n.get(Z);
	
	this->ExpData = ExpData;
	this->ExpMin = ExpMin;
	this->ExpMax = ExpMax;

	this->Energies = Energies;
	this->A = A;
	this->Z = Z;
	
	this->Cascades = Cascades;
	this->Results = Results;
	this->Graphs = Graphs;
	this->BarreiraFissao = BarreiraFissao;
	this->DensidadeNiveis = DensidadeNiveis;
}


SpallationData::SpallationData() : NucleusName(""), A(0), Z(0), N(0), ExpData(""), ExpMin(0.0), ExpMax(0.0), Cascades(""), Results(""), Graphs(""), BarreiraFissao(""), DensidadeNiveis("")
{}

		
SpallationData::SpallationData( int A, int Z, int N, double Energy, std::string ExpData, double ExpMin, double ExpMax, std::string Cascades, std::string Results, std::string Graphs, std::string BarreiraFissao, std::string DensidadeNiveis )
{
	NameTable n;
	this->NucleusName = n.get(Z);
	
	this->ExpData = ExpData;
	this->ExpMin = ExpMin;
	this->ExpMax = ExpMax;

	this->Z = Z;
	this->A = A;
	this->N = N;
	this->Energy = Energy;
	
	this->Cascades = Cascades;
	this->Results = Results;
	this->Graphs = Graphs;
	this->BarreiraFissao = BarreiraFissao;
	this->DensidadeNiveis = DensidadeNiveis;
	
	for(int i(11); i >= 0; i--)
		this->Z_final.push_back(Z-i);
}



TaskInput::TaskInput( std::string Type, std::vector< double > Energies, std::string Cascades, std::string ExpData, double ExpMin, double ExpMax, int A, int Z, int N, std::string Results, std::string Graphs, std::string DN, std::string BF )
{
	this->Type = Type;
	this->Energies = Energies;
	
	this->Results = Results;
	this->Graphs = Graphs;
	
	this->Cascades = Cascades;
	
	this->ExpData = ExpData;
	this->ExpMin = ExpMin;
	this->ExpMax = ExpMax;
	
	this->A = A;
	this->Z = Z;
	this->N = N;
	
	this->DN = DN;
	this->BF = BF;
	
	this->A_defined = false;
	this->Z_defined = false;
	this->Type_valid = true;
}


TaskInput::TaskInput()
{
	this->Type = "";

	this->Results = "";
	this->Graphs = "";

	this->Cascades = "";

	this->ExpData = "";
	this->ExpMin = std::numeric_limits<double>::min();
	this->ExpMax = std::numeric_limits<double>::max();

	this->A = 0;
	this->Z = 0;
	this->N = 0;
	
	this->DN = "comum";
	this->BF = "comum";
	
	this->A_defined = false;
	this->Z_defined = false;
	this->Type_valid = false;
	this->DN_valid = true;
	this->BF_valid = true;
}


void TaskInput::fromXML( TiXmlElement *elem )
{
	this->Type = elem->Value();
	if( Type == "photon" || Type == "proton" || Type == "photofission" || Type == "spallation" )
		Type_valid = true;
	else
		Type_valid = false;
		
		
	TiXmlAttribute* pAttrib = NULL;
	for ( pAttrib = elem->FirstAttribute(); pAttrib != 0; pAttrib = pAttrib->Next()) 
	{
		if( std::string(pAttrib->Name()) == "a" )
		{
			elem->Attribute( "a", &A );
			A_defined = true;
		}
		
		else if( std::string(pAttrib->Name()) == "z" )
		{
			elem->Attribute( "z", &Z );
			Z_defined = true;
		}
		
		else if( std::string(pAttrib->Name()) == "h" )
			elem->Attribute( "h", &N );
		
		else if( std::string(pAttrib->Name()) == "resultados" )
			Results = elem->Attribute( "resultados" );
		
		else if( std::string(pAttrib->Name()) == "graficos" )
			Graphs = elem->Attribute( "graficos" );
		
		else if( std::string(pAttrib->Name()) == "cascades" )
			Cascades = elem->Attribute( "cascades" );
		
		else if( std::string(pAttrib->Name()) == "dn" )
		{
			std::string temp = elem->Attribute( "dn" );
			std::transform(temp.begin(), temp.end(), temp.begin(), (int(*)(int))std::tolower);
			DN = temp;

			if( DN == "comum" || DN == "ramamurth" || DN == "ignatyvk" || DN == "goriety" || DN == "bsfg" || DN == "bsfg-eg" || DN == "bsfg-ct" )
			{	DN_valid = true;	}
			else
			{	DN_valid = false;	}
		}
		
		else if( std::string(pAttrib->Name()) == "bf" )
		{
			std::string temp = elem->Attribute( "bf" );
			std::transform(temp.begin(), temp.end(), temp.begin(), (int(*)(int))std::tolower);
			BF = temp;
			
			if( BF == "comum" || BF == "microscopico" )
			{	BF_valid = true;	}
			else
			{	BF_valid = false;	}
		}
	}
	
	TiXmlElement *child = NULL;
	child = elem->FirstChildElement( "exp" );
	if( child )
	{
		this->ExpData = child->GetText();
		
		const char* pAttrib = NULL;
		pAttrib = child->Attribute("min");
		if(pAttrib)
			this->ExpMin = atof( pAttrib );
		
		pAttrib = child->Attribute("max");
		if(pAttrib)
			this->ExpMax = atof( pAttrib );
	}
	
	for ( child = elem->FirstChildElement( "energia" ); child != 0; child = child->NextSiblingElement( "energia" ))
	{
		Energies.push_back( atof(child->GetText()) );
		if(Type == "spallation") break;
	}
	
}


std::string TaskInput::toString(){
	std::stringstream ss;

	ss << Type << " {\n";
	if( !this->Type_valid )
		ss << "--> Erro: Tipo inválido \"" << Type << "\". <--\n";
	
	ss << "\ta: \"" << A << "\"\n";
	if( !this->A_defined )
		ss << "--> Erro: Massa não definida: \"" << A << "\".<--\n";
	if( this->A < 1 )
		ss << "--> Erro: Massa menor que 1: \"" << A << "\" <--\n";

	ss << "\tz: \"" << Z << "\"\n";
	if( !this->Z_defined )
		ss << "--> Erro: Numero atômico não definido: \"" << Z << "\" <--\n";
	if( this->Z < 0 )
		ss << "--> Erro: Massa menor que 0: \"" << Z << "\" <--\n";

	if(Type != "photofission")
		ss << "\thistorias: \"" << N << "\"\n";
	
	if( Type != "photofission" && N <= 0 )
		ss << "--> Erro: Numero de histórias deve ser maior que 0 para cascata e spallation. <--\n";
		
	ss << "\tresultados: \"" << (Results!="" ? Results : "Nao gerar") << "\"\n";
	ss << "\tgraficos: \"" << (Graphs!="" ? Graphs : "Nao gerar") << "\"\n";
	
	if(Type == "photofission" || Type == "spallation")
	{
		ss << "\tDensidade: \"" << DN << "\"\n";
		if( !DN_valid )
			ss << "--> Erro: Tipo de Densidade de níveis inválida: \"" << DN << "\"<--\n";
	}
	
	if(Type == "photofission" || Type == "spallation")
	{
		ss << "\tBarreira: \"" << BF << "\"\n";
		if( !BF_valid )
			ss << "--> Erro: Tipo de barreira de fissão inválida: \"" << BF << "\"<--\n";
	}
	
	ss << "\tEnergies: " << "{";
	if( Energies.size() > 0 )
		ss << Energies[0];
	for(unsigned int i(1); i < Energies.size(); i++)
		ss << ", " << Energies[i];
	ss << "}\n";
	
	ss	<< "\texp: \"" << (ExpData!=""?ExpData:"Nao gerar") << "\" ["
		<< (ExpMin!=std::numeric_limits<double>::min() ? ::toString(ExpMin) : "") << ", "
		<< (ExpMax!=std::numeric_limits<double>::max() ? ::toString(ExpMax) : "") << "]" << "\n";

	ss << "}\n";
	
	return ss.str();
}

bool TaskInput::hasError()
{
	return !this->A_defined || !this->Z_defined || !this->Type_valid || /*(Type == "photofission" && N >= 0) ||*/ !DN_valid || !BF_valid;
}

CRISP_XML_Parser::CRISP_XML_Parser() : logLevel(4) {}	

CRISP_XML_Parser::CRISP_XML_Parser( std::string file ) : logLevel(4)
{
	load( file );
}

void CRISP_XML_Parser::load( std::string file )
{
	this->file = file;
	doc.LoadFile( file );
	if ( !doc.LoadFile() )
	{
		std::cout << "Erro ao abrir arquivo de entrada \"" << file << "\"\n";
		
		std::cout << "\n\n";
		std::cout << "Descrição do erro: " << doc.ErrorDesc() << "\n";
		std::cout << "Linha: " << doc.ErrorRow() << "\n";
		std::cout << "Coluna: " << doc.ErrorCol() << "\n";
		
//		std::string s = doc.ErrorDesc();
//		std::cout << "\n";
//		std::cout << '*' << std::setfill('*') << std::setw(s.size()+4) << "*\n";
//		std::cout << '*' << std::setfill(' ') << std::left << std::setw(s.size()+1) << doc.ErrorDesc() << " *\n";
//		std::cout << '*' << std::setfill(' ') << std::left << std::setw(s.size()+1) << std::left << "Linha: " << std::left << doc.ErrorRow() << " *\n";
//		std::cout << '*' << std::setfill(' ') << std::left << std::setw(s.size()+1) << std::left << "Coluna: " << std::left << doc.ErrorCol() << " *\n";
//		std::cout << '*' << std::setfill('*') << std::setw(s.size()+4) << "*\n";
		exit(1);
	}
	parser();
}

std::vector< CascadeData > CRISP_XML_Parser::getCascades()
{
	std::vector< CascadeData > result;
	
	for(unsigned int i(0); i < tasks.size(); i++)
	{
		if(tasks[i].Type == "photon" || tasks[i].Type == "proton")
		{
			CascadeData temp(	tasks[i].Type,
								tasks[i].A,
								tasks[i].Z,
								tasks[i].N,
								tasks[i].Energies,
								tasks[i].ExpData,
								tasks[i].ExpMin,
								tasks[i].ExpMax,
								tasks[i].Results,
								tasks[i].Graphs );
			result.push_back(temp);
		}
	}
	return result;
}


std::vector< FissionData > CRISP_XML_Parser::getPhotofission()
{
	std::vector< FissionData > result;
	
	for(unsigned int i(0); i < tasks.size(); i++)
	{
		if(tasks[i].Type == "photofission" )
		{
			FissionData temp(	tasks[i].A,
								tasks[i].Z,
								tasks[i].Energies,
								tasks[i].ExpData,
								tasks[i].ExpMin,
								tasks[i].ExpMax,
								tasks[i].Cascades,
								tasks[i].Results,
								tasks[i].Graphs,
								tasks[i].BF,
								tasks[i].DN );
			result.push_back(temp);
		}
	}
	return result;
}

std::vector< SpallationData > CRISP_XML_Parser::getSpallations()
{
	std::vector< SpallationData > result;
	
	for(unsigned int i(0); i < tasks.size(); i++)
	{
		if(tasks[i].Type == "spallation" )
		{
			SpallationData temp(	tasks[i].A,
								tasks[i].Z,
								tasks[i].N,
								tasks[i].Energies[0],
								tasks[i].ExpData,
								tasks[i].ExpMin,
								tasks[i].ExpMax,
								tasks[i].Cascades,
								tasks[i].Results,
								tasks[i].Graphs,
								tasks[i].BF,
								tasks[i].DN );
			result.push_back(temp);
		}
	}
	return result;
}



void CRISP_XML_Parser::parser()
{
	TiXmlElement* pElem = doc.FirstChildElement();
	
	pElem->Attribute( "log", &logLevel );
		
	const char* temp = pElem->Attribute( "pastaBase" );
	if( temp )
		pastaBase = temp;
		
	TiXmlElement* pChild = NULL;

	for ( 	pChild = pElem->FirstChildElement(); 
			pChild != 0; 
			pChild = pChild->NextSiblingElement()) 
	{
		TaskInput t;
		t.fromXML( pChild );
//		t.Results = pastaBase + "/" + t.Results;
//		t.Graphs = pastaBase + "/" + t.Graphs;
		tasks.push_back(t);
	}
	
	for( unsigned int i(0); i < tasks.size(); i++ )
	{
		std::cout << tasks[i].toString();
	}
}

int CRISP_XML_Parser::getLogLevel()
{
	return this->logLevel;
}

std::string CRISP_XML_Parser::getPastaBase()
{
	return this->pastaBase;
}
	
	
bool CRISP_XML_Parser::hasErrors()
{
	for( unsigned int i(0); i < tasks.size(); i++ )
	{
		if( tasks[i].hasError() )
			return true;
	}
	return false;
	
}
/*
std::string CRISP_XML_Parser::showErrors()
{
	std::stringstream ss;
	for( unsigned int i(0); i < tasks.size(); i++ )
		if( tasks[i].hasError() )
			ss << tasks[i].toString();
	return ss.str();
}
*/
std::string CRISP_XML_Parser::showAll()
{
	std::stringstream ss;
	for( unsigned int i(0); i < tasks.size(); i++ )
		ss << tasks[i].toString();
	return ss.str();
}




