#ifndef Data_pHH
#define Data_pHH

#include "NameTable.hh"
#include "tinyxml/tinyxml.h"
#include "tinyxml/tinystr.h"
template<typename T>
std::string toString( T t ){
	std::stringstream ss;
	ss << t;
	return ss.str();
}

const std::string  DensidadeNiveis[] = {
	"comum",
	"ramamurth",
	"ignatyvk",
	"goriety",
	"bsfg",
	"bsfg-eg",
	"bsfg-ct"
};

const std::string BarreiraFissao[] = {
	"comum",
	"microscopico"
};

struct CascadeData{
	std::string	NucleusName;
	std::string Type;				// tipo da cascata (photon ou proton)
	int A;							// massa
	int Z;							// numero de protons
	int N;							// numero de historias
	std::vector< double > Energies;	// energias
	std::string ExpData;			// caminho para arquivo de dados experimentais
	double ExpMin;					// valor minimo para pegar do arquivo experimental
	double ExpMax;					// valor maximo para pegar do arquivo experimental
	std::string Results;			// pasta dos arquivos de saída com resultados
	std::string Graphs;				// pasta dos arquivos de saída para graficos
	CascadeData( std::string Type, int A, int Z, int N, std::vector< double > Energies, std::string ExpData, double ExpMin, double ExpMax, std::string Results, std::string Graphs );
};

struct FissionData{
	std::string	NucleusName;
	int A;
	int Z;
	std::vector< double > Energies;
	std::string ExpData;
	double ExpMin;					// valor minimo para pegar do arquivo experimental
	double ExpMax;					// valor maximo para pegar do arquivo experimental
	std::string Cascades;			// pasta dos arquivos de saída da cascata
	std::string Results;			// pasta dos arquivos de saída com resultados
	std::string Graphs;				// pasta dos arquivos de saída para graficos
	std::string BarreiraFissao;
	std::string DensidadeNiveis;
	FissionData();
	FissionData( int A, int Z, std::vector< double > Energies, std::string ExpData, double ExpMin, double ExpMax, std::string Cascades, std::string Results, std::string Graphs, std::string BarreiraFissao, std::string DensidadeNiveis );
};

struct SpallationData{
	std::string	NucleusName;
	int A, N, Z;
	double Energy, ExpMin, ExpMax;
	std::string ExpData;
	std::string Cascades;			// pasta dos arquivos de saída da cascata
	std::string Results;			// pasta dos arquivos de saída com resultados
	std::string Graphs;				// pasta dos arquivos de saída para graficos
	std::string BarreiraFissao;
	std::string DensidadeNiveis;
	std::vector< int > Z_final;
	SpallationData();
	SpallationData( int A, int Z, int N, double Energy, std::string ExpData, double ExpMin, double ExpMax, std::string Cascades, std::string Results, std::string Graphs, std::string BarreiraFissao, std::string DensidadeNiveis );
};

struct TaskInput{
	int A;							// massa
	int Z;							// numero de protons
	int N;							// numero de historias
	std::string Type;				// tipo da cascata (photon, proton, photofission, spallation)
	std::string ExpData;			// arquivos de dados experimentais
	std::string Cascades;			// pasta dos arquivos de saída da cascata
	double ExpMin;					// energia minima para pegar do arquivo esperimental
	double ExpMax;					// energia maxima para pegar do arquivo esperimental
	std::vector< double > Energies;	// energias (spallation somente uma)
	std::string Results;			// pasta dos arquivos de saída com resultados
	std::string Graphs;				// pasta dos arquivos de saída para graficos
	
	std::string DN;				// tipo da densidade de niveis (somente MCEF)
	std::string BF;				// tipo da barreira de fissão (somente MCEF)
	TaskInput( std::string Type, std::vector< double > Energies, std::string Cascades, std::string ExpData, double ExpMin, double ExpMax, int A, int Z, int N, std::string Results, std::string Graphs, std::string DN, std::string BF );
	TaskInput();
	void fromXML( TiXmlElement *elem );
	std::string toString();
	bool hasError();
private:	
	bool A_defined;
	bool Z_defined;
	bool Type_valid;
	bool DN_valid;
	bool BF_valid;
};

class CRISP_XML_Parser{
	std::string file;
	TiXmlDocument doc;
	std::string pastaBase;
	int logLevel;
	
	std::vector< TaskInput > tasks;

public:
	CRISP_XML_Parser();
	CRISP_XML_Parser( std::string file );
	void load( std::string file );
	std::vector< CascadeData > getCascades();
	std::vector< FissionData > getPhotofission();
	std::vector< SpallationData > getSpallations();
	bool hasErrors();
//	std::string showErrors();
	std::string showAll();
	int getLogLevel();
	std::string getPastaBase();
private:
	void parser();
};
#endif // Data_pHH

