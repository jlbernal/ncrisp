
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>

#include "TAxis.h"
#include "TGraph.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraphErrors.h"
#include "TRandom3.h"

#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnScan.h"

#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "PhotonEventGen.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "McefAdjust.hh"


#include "Timer.hh"
#include "Data.hh"

#ifdef WIN32
#pragma comment (lib, "WSock32.Lib")
#endif

const std::string defaultInputFile("input/mcef.xml");
std::string parserArguments(int argc, char *argv[]);

void minuitClient( McefAdjust &Adj );
void mcef_minuit(std::string inputFile );


int main( int argc, char *argv[] )
{
	std::string inputFile = parserArguments(argc, argv);
	
	int rank(0);
	int size(1);

#ifdef CRISP_MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
#endif // CRISP_MPI

	if( rank == 0 )
		std::cout << "rank: " << rank << " -- " << "size: " << size << "\n\n";

	// Gerando numeros aleatórios diferentes para cada host
	// TRandom3 gera numeros aleatórios muito melhores que o default TRandom
	// Eh dado um numero primo diferente para cada noh como seed
	if( gRandom )
		delete gRandom;
	gRandom = new TRandom3( primos[rank] );

	CrispParticleTable *cpt = CrispParticleTable::Instance();

	if( rank == 0 )
		std::cout << std::endl << "Loading PDG Particle Data ..." << std::endl;
		
	cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");

	if( rank == 0 )
		std::cout << "Ready." << std::endl;

#ifdef CRISP_MPI
	MPI_Barrier( MPI_COMM_WORLD );
#endif // CRISP_MPI

#ifdef _DEBUG
	char hostname[256];
	gethostname(hostname, sizeof(hostname));
#ifndef WIN32
	std::cout <<"Rank " << rank << " : PID " << getpid() << " on " << hostname << " ready for attach\n";
	sleep(5);
#else
	std::cout <<"Rank " << rank << " : PID " << GetCurrentProcessId() << " on " << hostname << " ready for attach\n";
	Sleep(5000);
#endif // WIN32
	fflush(stdout);
#endif // _DEBUG



	double TempoInicial(0.0), TempoFinal(0.0), TempoTotal(0.0);

	if( rank == 0 )
		TempoInicial = CRISPGetTime()*0.001;

	mcef_minuit(inputFile);

	if( rank == 0 )
	{
		TempoFinal = CRISPGetTime()*0.001;
		TempoTotal = TempoFinal - TempoInicial;
		std::cout << "Tempo total de execucao do programa: ";
		FormTime( TempoTotal, std::cout );
		std::cout << std::endl << std::endl << std::endl;
	}

#ifdef CRISP_MPI
	std::cout << "Rank[" << rank << "]" << " Terminando.\n";
	MPI_Finalize();
#endif // CRISP_MPI

	return true;
}

void mcef_minuit( std::string inputFile)
{
	int rank(0);
	int size(1);
	
#ifdef CRISP_MPI
	// MPI ----------------------------------------------------------------------
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // size dos processos
	// End MPI ------------------------------------------------------------------
#endif // CRISP_MPI

	//	Como trabalhar com o MINUIT MCEF

	//	O vetor "Int_t e[]" contem a lista de energias que a photofission vai trabalhar,
	//		eh necessario ter um arquivo de cascata para cada uma dessa energias
	//	O vetor "std::vector< Int_t > energies" eh somente uma conversao do vetor estilo C "Int_t e[]" para estilo C++

	double e[] = { 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0 };
	std::vector< double > energies(e, e + sizeof(e)/sizeof(double));

	//	O vetor "std::vector< FissionData > Photonfissions" contem as informacoes sobre as Photofissions que devem ser testadas
	//	Cada "FissionData" contem uma Photofission
	//	
	//	Ex.: FissionData("minuit/quiquadrado/chumbo_ce.dat", "Pb208", energies, 208, 82)
	//		1. par: string com o diretorio e nome do arquivo experimental
	//		2. par: string com o nome do nucleo
	//		3. par: vetor com a lista de energias a seres testadas
	//		4. par: numero de massa do elemento
	//		5. par: numero atomico do elemento

	std::vector< FissionData > Photonfissions;
//	Photonfissions.push_back( FissionData(208, 82, energies, "exp_data/mcef/photofission/chumbo_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Pb208", "results/mcef/photofission/Pb208" ) );
//	Photonfissions.push_back( FissionData(232, 90, energies, "exp_data/mcef/photofission/torio_ce.dat",		0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Th232", "results/photofission/mcef/Th232" ) );
//	Photonfissions.push_back( FissionData(235, 92, energies, "exp_data/mcef/photofission/uranio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/U235", "results/photofission/mcef/U235" ) );
//	Photonfissions.push_back( FissionData(237, 93, energies, "exp_data/mcef/photofission/neptunio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Np237", "results/photofission/mcef/Np237" ) );
//	Photonfissions.push_back( FissionData(238, 92, energies, "exp_data/mcef/photofission/uranio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/U238", "results/photofission/mcef/U238" ) );
	
	//	O vetor "std::vector< SpallationData > Spallations" contem as informacoes sobre as Spallations que devem ser testadas
	//	Cada "SpallationData" contem um Spallation
	//	
	//	Ex.: SpallationData("minuit/quiquadrado", "Pb", "Pb208", 1000, Pb_Z_final, Pb_NucleusFinal, 150)
	//		1º par: string com o diretorio onde estao os arquivos experimentais (sao varios)
	//		2. par: string com o nome do nucleo
	//		3. par: numero atomico
	//		4. par: massa
	//		4. par: energia que vai ser feita o teste
	//		5. par: vetor com os valores finais de numero atomico
	//		6. par: vetor com os nomes do elementos quimicos finais
	//		7. par: numero de vezes que o Spallation deve ser rodado

	std::vector< SpallationData > Spallations;
//	Spallations.push_back( SpallationData(82, 208, 50, 1000, "exp_data/mcef/spallation/Pb208",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/Pb208", "results/mcef/spallation/Pb208") );
//	Spallations.push_back( SpallationData(79, 197, 50,  800, "exp_data/mcef/spallation/Au197",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/Au197", "results/mcef/spallation/Au197") );
//	Spallations.push_back( SpallationData(92, 238, 50, 1000, "exp_data/mcef/spallation/U238",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/U238", "results/mcef/spallation/U238") );


	//	McefAdjust eh a classe que calcula o qui-quadrado para o MCEF
	//
	//	Ex. de uso: McefAdjust(nuc, "paralelo/results", Photonfissions, Spallations);
	//		1. par: instancia da classe NucleusDynamics
	//		2. par: string com a pasta onde se encontram os arquivos da cascata
	//		3. par: Lista de Photonfissions a seres feitas
	//		4. par: Lista de Spallations a seres feitas
	//		5. par: (Parametro opcional) define o nivel de informacao a ser exibida durante a minimizacao
	//			valor minimo eh 0 (para nao mostrar nada na tela) e maximo eh 4 (para mostrar todos qui-quadrados parciais)
//	McefAdjust Full_Adj(nuc, Photonfissions, Spallations, primos[rank], 4);
	
	CRISP_XML_Parser c("input/mcef.xml");
	if( rank == 0 ) c.showAll();

	if( rank == 0 ) if( c.hasErrors() )
	{
		std::cout << "\n\n";
		std::cout << "###########################################################\n";
		std::cout << "#-Houveram erros no arquivo de entrada                    #\n";
		std::cout << "#-Leia o log acima e corrija-os.                          #\n";
		std::cout << "#-Abortando execucao.                                     #\n";
		std::cout << "###########################################################\n\n";
		exit(1);
	}

	Photonfissions = c.getPhotofission();
	Spallations = c.getSpallations();
	
	
	if( (Photonfissions.size() == 0) && (Spallations.size() == 0) )
	{
		if( rank == 0 ) std::cout << "Nenhuma Fotofissao ou Spallation para executar, o arquivo de entrada está errado ou vazio.\n";
		exit(1);
	}


	{
		
		McefAdjust Adj( Photonfissions, Spallations, primos[rank], c.getLogLevel());

		McefAdjust::ResetParcialChi();
		
		ROOT::Minuit2::MnUserParameters upar;
		
		if( rank != 0 )
		{
			minuitClient(Adj);
		}
		else
		{
			double erro = 0.01;

			ROOT::Minuit2::MnUserParameters upar;

			for(int i(0); i < 5; i++)
			{
				std::stringstream ss;
				ss << "prf" << i;
				upar.Add( ss.str().c_str(),	McefModel::Get_prf(i),	erro);
			}

			//upar.Add("pp1",		McefModel::Get_pp1(),	erro); // erro_med);
			//upar.Add("pp2",		McefModel::Get_pp2(),	erro); // erro_med);
			//upar.Add("pa1",		McefModel::Get_pa1(),	erro); // erro_med);
			//upar.Add("pa2",		McefModel::Get_pa2(),	erro); // erro_med);
			//upar.Add("pn1",		McefModel::Get_pn1(),	erro); // erro_med);
			//upar.Add("pn2",		McefModel::Get_pn2(),	erro); // erro_med);
			upar.Add("pbf1",	McefModel::Get_pbf1(),	erro); // erro_med);
			
			for(int i(0); i < 5; i++)
			{
				std::stringstream ss;
				ss << "prf" << i;
//				upar.SetLimits(ss.str().c_str(),	1.0,	1.2);
			}
	
			//upar.SetLimits("prf1",	-10.0,	10.0);
			//upar.SetLimits("prf2",	-10.0,	10.0);
			//upar.SetLimits("prf3",	1.0,	1.2);
			//upar.SetLimits("prf4",	1.0,	1.2);
			//upar.SetLimits("prf5",	1.0,	1.2);
			//upar.SetLimits("prf6",	1.0,	1.2);
			//upar.SetLimits("prf7",	1.0,	1.2);

			//upar.SetLimits("pp1",	15,		25);
			//upar.SetLimits("pp2",	0.5,	5.0);
			//upar.SetLimits("pa1",	15.,	25.);
			//upar.SetLimits("pa2",	1.,		10.);
			//upar.SetLimits("pn1",	15,		25);
			//upar.SetLimits("pn2",	0.5,	5);
			upar.SetLimits("pbf1", -5,		0 );
			
	//		upar.Fix("prf1");
	//		upar.Fix("prf2");
	//		upar.Fix("prf3");
	//		upar.Fix("prf4");
	//		upar.Fix("prf5");
	//		upar.Fix("prf6");
	//		upar.Fix("prf7");
			//upar.Fix("pp1");
			//upar.Fix("pp2");
			//upar.Fix("pa1");
			//upar.Fix("pa2");
			//upar.Fix("pn1");
			//upar.Fix("pn2");
			//upar.Fix("pbf1");
		
/*
			for(int i(3); i < 4; i++)
			{
				double divisor = 10.0/pow(10.0,i);
				erro = 1.0 * divisor;

				for(int a(0); a < 25; a++)
				{
					std::stringstream ss;
					ss << "prf" << a;
					upar.SetError(ss.str().c_str(),	erro);
				}
				
				//upar.SetError("prf1", erro);
				//upar.SetError("prf2", erro);
				//upar.SetError("prf3", erro);
				//upar.SetError("prf4", erro);
				//upar.SetError("prf5", erro);
				//upar.SetError("prf6", erro);
				//upar.SetError("prf7", erro);
				//upar.SetError("pp1", erro);
				//upar.SetError("pp2", erro);
				//upar.SetError("pa1", erro);
				//upar.SetError("pa2", erro);
				//upar.SetError("pn1", erro);
				//upar.SetError("pn2", erro);
				upar.SetError("pbf1", erro);

				if( rank == 0 )	std::cout << "Erros: " << erro << "\n";

				if( rank == 0 )	std::cout << "Executando [" << Tag2 << "_SCAN" <<  i << "]: " << std::endl;
				ROOT::Minuit2::MnScan scan(*adj, upar);
				ROOT::Minuit2::FunctionMinimum min = scan();
				if( rank == 0 ) std::cout << "Minimo [" << Tag2 << "_SCAN" <<  i << "]: " << min << std::endl;

				upar = scan.Parameters();

			}*/
			
			if( rank == 0 )	std::cout << "Executando [" << "SCAN" << "]: " << std::endl;
			ROOT::Minuit2::MnScan scan(Adj, upar);
			ROOT::Minuit2::FunctionMinimum min = scan();
			if( rank == 0 ) std::cout << "Minimo [" << "SCAN" << "]: " << min << std::endl;

			upar = scan.Parameters();

			if( rank == 0 )	std::cout << "Executando ["<< "MINIMIZE]" << std::endl;
			ROOT::Minuit2::MnMinimize minimize(Adj, upar);
			ROOT::Minuit2::FunctionMinimum min2 = minimize();
			if( rank == 0 ) std::cout << "Minimo [" << "MINIMIZE]: " << min2 << std::endl;

			upar = minimize.Parameters();


#ifdef CRISP_MPI			
			// rank0 mavisa aos outros hosts q o calculo do MINUIT acabou (valor 0)
			int nPar = 0;
			// manda a quantidade de parametros == 0 para dizer q terminou
			MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
#endif // CRISP_MPI
		}
		
		std::vector< double > v;
		v = upar.Params();
		
#ifdef CRISP_MPI
		// atualiza + 1 vez os valores dos parametros		
		int nPar = upar.Params().size();		
		if( rank == 0 )
		{
			MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
			MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
			
		}
		else
		{
			MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
			v.resize( nPar );
			MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
			
		}
#endif // CRISP_MPI

		Adj.AdjustParams( v );

		Adj.GeraResultados( c.getPastaBase() );
		Adj.GeraGraficos( c.getPastaBase() );
		if( rank == 0 ) Adj.GeraGraficoRF(c.getPastaBase().c_str());
		
		std::stringstream ss; ss << c.getPastaBase() << "/" << "Ajuste.txt";
		std::ofstream ofs( ss.str().c_str() );
		ofs << upar;
	}

}



std::string parserArguments(int argc, char *argv[])
{
	if( argc > 0 )
	for(int i(0); i < argc; i++)
	{
		std::string cmd(argv[i]);
		if(cmd == "-i")
		{
			// arquivo de entrada
			if( i+1 < argc )
			{
				std::string path(argv[i+1]);
				return path;
			} else {
				std::cout << "arquivo de entrada não definido!\n";
				exit(1);
			}
		}
	}
	return defaultInputFile;
}




void minuitClient( McefAdjust &Adj )
{
#ifdef CRISP_MPI
	int rank(0);
	int size(1);
	

	// MPI ----------------------------------------------------------------------
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // size dos processos
	// End MPI ------------------------------------------------------------------

	
	int nPar = 0;
	std::vector< double > v;
	
	while(true)
	{
		// Espera o Rank0 mandar o numero de parametros
		// int MPI_Recv(void*, int, ompi_datatype_t*, int, int, ompi_communicator_t*, MPI_Status*)
//		MPI_Recv( &nPar, 1, MPI_INT, 0,	MPI_COMM_WORLD );
		MPI_Bcast( &nPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
		
//		std::cout << "Rank["<<rank<<"]" << " Recebeu nPar == " << nPar <<"\n";
		
		// termina quando receber 0
		if( nPar == 0 )
			break;
		
		v.resize( nPar );
		
		// atualiza parametros
		MPI_Bcast( &v[0], v.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
		
		// executa uma iteração de calculo do chisqr
		Adj(v);
	}
#endif // CRISP_MPI
}

