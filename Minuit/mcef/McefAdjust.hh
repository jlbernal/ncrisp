
#ifndef McefAdjust_h
#define McefAdjust_h

#include <iostream>
#include <sstream> 
#include <iomanip>
#include <vector>
#include <map>
#include <fstream>

#include "TAxis.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TImage.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TSystem.h"
#include "Math/Interpolator.h"

#ifdef CRISP_MINUIT
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnMinimize.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/FCNBase.h"
#endif // CRISP_MINUIT



#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "PhotonEventGen.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "Mcef.hh"
#include "Models.hh"


#ifdef CRISP_MPI
#include "mpi.h"
#endif // CRISP_MPI


#include "Timer.hh"
#include "Data.hh"
#include "Util.hh"


typedef CrispParticleTable CPT;


/*! Estrutura que guarda todo um resultado da cascata
 serve para evitar q a Spallation fique acessando o disco
 usado como um buffer para accelerar o calculo do mcef
*/
struct CascadeFileBuffer {
	
	std::string in_file;
	std::vector< double > 	Energy_array;
	std::vector< int > 		A_array;
	std::vector< int > 		Z_array;
	std::vector< int > 		Counts_array;
	std::vector< double > 	ExEnergy_array;
	
	CascadeFileBuffer( int N = 5000 )
	{
		Energy_array.reserve(N);
		A_array.reserve(N);
		Z_array.reserve(N);
		Counts_array.reserve(N);
		ExEnergy_array.reserve(N);
	}
};





Measurement PhotoFission(const CascadeFileBuffer &buffer, NucleusDynamics* nuc, Mcef* mcef, TTree *mcef_t = NULL);

int LoadSpallationData( int z, std::vector< Int_t > &MassResult, std::vector< Double_t > &SigmaResults );
void SpallationCS( const CascadeFileBuffer &buffer, int nTimes, Mcef* mcef);	// Spallation
void SpallationCS_MPI( const CascadeFileBuffer &buffer, int nTimes, Mcef* mcef);	// Spallation Paralelo

/*!
 *	Classe que executa a Fotofissão e Spallation.
 *	Capaz de executar várias Fotofissões/Spallations de uma só vez.
 *	Pode gerar arquivos com resultados e gráficos auomaticamente.
*/
class McefAdjust
#ifdef CRISP_MINUIT
	: public ROOT::Minuit2::FCNBase
#endif // CRISP_MINUIT
{
private:
	// MPI
	int rank;	//!< Rank do processo MPI
	int size;	//!< Numero de processos MPI
	// End MPI
	
	int NTotalPontos[10];
	
	/*!
		Define o nivel de informacao que deve ser exibido durante a minimizacao (default 2).
		
		0 == nao mostra nada.<br />
		1 == mostra variaveis e qui-quadrado total.<br />
		2 == mostra qui-quadrado parcial 1.<br />
		3 == mostra qui-quadrado parcial 2.<br />
		4 == mostra todas as informacoes possiveis (muito lixo na tela).
	 */
	int verbose;
	
	//! Marca se deve gerar uma TTree dos resultados (atualmente não utilizado)
	bool generateTTree;
	
	//! Semente para numeros aleatorios
	long Seed;	

	//! Vetor que guarda os dados experimentais para Photofission
	std::vector< TGraphErrors > FissionExpGraphs;

	//! Vetor que guarda os dados experimentais para Spallation
	std::vector< std::vector< TGraphErrors > > SpallationExpGraphs;

	NucleusDynamics* nuc;
	MesonsPool* mpool;
	Double_t err;

	std::vector< FissionData > Fissions;										//!< Vetor de Photofissions a serem realizadas
	std::vector< SpallationData > Spallations;									//!< Vetor de Spallations a serem realizadas
	std::vector< CascadeFileBuffer > SpallationCascadeBuffer;					//!< Vetor com os buffers de dados da cascata usado pelo Spallation
	std::vector< std::vector< CascadeFileBuffer > > PhotofissionCascadeBuffer;	//!< Matriz com os buffers de dados da cascata usado pelo Spallation

public:

	//! Construtor
	/*!
		\param Fissions Vetor com tarefas a serem realizadas de Fotofissões.
		\param Spallations Vetor com tarefas a serem realizadas de Spallations.
		\param Seed Semente para números pseudo-aleatórios.
		\param verbose valor da verbose.
	*/
	McefAdjust( std::vector< FissionData > Fissions,
				std::vector< SpallationData > Spallations, long Seed, int verbose = 2 );

	//! Destrutor
	virtual ~McefAdjust();

#ifdef CRISP_MINUIT
	// Metodos do que seram chamados pelo MINUIT
	virtual double operator() (const std::vector<double>& v) const;
	virtual double Up() const { return err; } 
	void SetErrorDef(double v) { err = v; }

	//! Exibe os parametros
	void PrintParams(std::ostream& os) const;
	//! Atualiza os parametros pelos calculados pelo MINUIT
	void AdjustParams(const std::vector<double>& v) const;
	


	// Metodos para calcular o quiquadrado
	//! Calcula o Qui-quadrado para uma fotofissão
	/*!
		\param Graph Gráfico pré-carregado com dados experimentais
		\param Fission Objeto com os dados sobre a Fotofissao a ser realizada
		\param buffer objeto com os dados do arquivo de saída da cascata
		\return Valor do qui-quadrado calculado para determinada fotofissão
	*/
	double ChiSqrPhotofission( TGraphErrors Graph, FissionData Fission, const std::vector< CascadeFileBuffer > &buffer ) const;
	
	//! Calcula o Qui-quadrado para uma spallation
	/*!
		\param Graphs Gráficos pré-carregados com dados experimentais
		\param Spallation Objeto com os dados sobre a Spallation a ser realizada
		\param buffer objeto com os dados do arquivo de saída da cascata
		\param NTotalPontos Valor de retorno que contem quantos pontos foram gerados no gráfico da spallation
		\return Valor do qui-quadrado calculado para determinada spallation
	*/
	double ChiSqrSpallation( std::vector< TGraphErrors > Graphs, SpallationData Spallation, const CascadeFileBuffer &buffer, int &NTotalPontos ) const;
	
	//! Calcula o Qui-quadrado para todas fotofissões
	/*!
		\return Valor do qui-quadrado calculado para todas fotoffisões
	*/
	double ChiSqrPhotofissionAll() const;
	
	//! Calcula o Qui-quadrado para todas spallations	
	/*!
		\return Valor do qui-quadrado calculado para todas spallations
	*/
	double ChiSqrSpallationAll() const;
	
	//! Reinicia o valor do melhor valor calculado para o Qui-quadrado (utilizado parar gerar gráficos parciais, não utilizado atualmente)
	static void ResetParcialChi();

#endif // CRISP_MINUIT

	//! Metodo que testa a existência de todos os arquivos necessarios para realizar o MINUIT
	void TestaArquivos();

	//! Gera arquivos de resultados com os parametros atualizados pelo MINUIT
	/*!
		\param pastaBase Caminho para a pasta onde os resultados devem ser criado
	*/
	void GeraResultados( std::string pastaBase ) const;

	//! Gera graficos dos dados experimentias, calculados pelos CRISP e um grafico comparativo entre os dois
	/*!
		\param pastaBase Caminho para a pasta onde os gráficos devem ser criado
	*/
	void GeraGraficos( std::string pastaBase ) const;

	//! Carrega os dados experimentais em Graficos TGraph e carrega os arquivos da cascata para memopria para acccesso rapido
	void LoadGraphs();

	//! Gera um gráfico com o valor atual do RF
	/*!
		\param pasta Caminho para a pasta onde o gráfico deve ser criado
	*/
	void GeraGraficoRF(std::string pasta) const;

	//! Instancia uma classe Mcef com determinada Densidade de Níveis e Barreira de Fissão
	/*!
		\param DensidadeNiveis String que determina a densidade de níveis que deve ser utilizada
		\param BarreiraFissao String que determina a barreira de fissão que deve ser utilizada
		\return Um objeto Mcef criado com os determinados que deve ser deletado depois de utilizado!
	*/
	Mcef* buildMcef(std::string DensidadeNiveis, std::string BarreiraFissao) const;

	//! Muda a vebose da execução (Ver atrivuto "int verbose")
	/*!
		\param _v Determina o novo valor da verbose.
		\sa verbose
	*/
	void SetVerbose( int _v ) { this->verbose = _v; }
};



#endif // McefAdjust_h
