
#include "McefAdjust.hh"

extern std::ostream *McefModel_os;
bool extraSpallation = false;


// Versao 5.14 do ROOT a interpolacoa linear eh definida como "LINEAR" e na 5.20 "kLINEAR"
// esse define resolve a imcompatibilidade entre as duas versoes
#if (ROOT_VERSION_CODE <= ROOT_VERSION(5,14,0))
	#define kLINEAR LINEAR
#endif



//	Matrix de frequencia do spallation
static const int __MAX_A = 250;
static const int __MAX_Z = 120;
double frequencias[__MAX_Z][__MAX_A];
double X[__MAX_A];

// versao local da matriz acima (usadas na paralelizacao com MPI)
double local_frequencias[__MAX_Z][__MAX_A];



Measurement PhotoFission(const CascadeFileBuffer &buffer, NucleusDynamics* nuc, Mcef* mcef, TTree *tree){
	// Calcular a area da interacao.
	Double_t rnt = nuc->GetRadium();
	Int_t a = nuc->GetA();
	Double_t sf = (TMath::Pi() * sqr(rnt) * 10000.) / (Double_t)a;

	Int_t fissoes = 0, N = 0, tot_counts = 0;

 	for(uint i(0); i < buffer.ExEnergy_array.size(); i++){ // while ( ifs.good() )
//		Double_t Energy = buffer.Energy_array[i];
		Int_t A = buffer.A_array[i];
		Int_t Z = buffer.Z_array[i];
		Int_t Counts = buffer.Counts_array[i];
		Double_t ExEnergy = buffer.ExEnergy_array[i];
		if ( ExEnergy > 0. )		{
			std::ostringstream branch_name;
			TBranch *branch = NULL;
			if( tree )			{
				branch_name << "Mcef" << "." << i;
				//TBranch *branch = tree->Branch( branch_name.str().c_str(), "Mcef", &mcef, 128000, 2 );
				branch = tree->Bronch( branch_name.str().c_str(), "Mcef", &mcef, 16800, 0);
			}
			N++;
			// contar quantos fotons foram lancados contra o nucleo.
			tot_counts += Counts;
			try{
				mcef->Generate( A, Z, ExEnergy, branch);
			}catch(std::exception &e){throw e;}
			if ( mcef->NumberFissions() > 0 ){
				// Nucleo fissionou.
				fissoes++;
			}
		}
		if(tree) if((i+1)%500==0) tree->AutoSave("FlushBaskets");	// auto-salva a ttree no disco e libera memoria a cada 500 iteracoes
	} // 'while' closing brace.
//	std::cout << tot_counts << " | " << fissoes << " | " << N << "\n";
	Double_t photo_abs_cs = (Double_t)N / (Double_t) tot_counts * sf;
	// std::cout << photo_abs_cs << std::endl;
	Double_t fission_ratio = (Double_t)fissoes / (Double_t)N;
	std::vector<double> Va, Vb;
	return Measurement( photo_abs_cs * fission_ratio, 0., Va, Vb);
}

int LoadSpallationData( int z, std::vector< Int_t > &MassResult, std::vector< Double_t > &SigmaResults )
{
	Int_t i = 0;
	for( int a = 150; a < __MAX_A; a++)
	{

		double r = 1.2 * pow(a, 1./3.);
		double S = r * r * TMath::Pi();
		double f = frequencias[z][a];
		double sigma = f * S * 10.;
		if  ( f > 0. )
		{
			MassResult.push_back(a);
			SigmaResults.push_back(sigma);
//			SpallResults[a] = sigma;
//			vect_A[i] = (Double_t)a;
//			vect_sigma[i] = sigma;
			i++;
		}

	}
	return i;
}
/*
void SpallationCS( const CascadeFileBuffer &buffer, int nTimes, Mcef* mcef)
{
	std::vector< Double_t > SpallResults;

	// Zera a matriz de frequencias
	for( int z = 1; z < __MAX_Z ; z++)
	{
		for( int a = 1; a < __MAX_A ; a++)
		{
			X[a] = a;
			frequencias[z][a] = 0;
			
			// MPI
			local_frequencias[z][a] = 0;
			// end MPI
		}
	}

	Int_t N = 0;
	Int_t not_fission = 0;
	
	
 	for(int i(0); i < buffer.ExEnergy_array.size(); i++) // while ( ifs.good() )
	{
		Double_t Energy = buffer.Energy_array[i];
		Int_t A = buffer.A_array[i];
		Int_t Z = buffer.Z_array[i];
		Int_t Counts = buffer.Counts_array[i];
		Double_t ExEnergy = buffer.ExEnergy_array[i];

		Int_t a_final(0), z_final(0);
		if ( ExEnergy > 0. )
		{		
			// versao serial -----------------------------------------------------------------------
			for ( Int_t i = 0; i < nTimes; i++ )
			{
				mcef->Generate( A, Z, ExEnergy  );
				a_final = 0; z_final = 0;
				if ( mcef->NumberFissions() == 0 )
				{
					mcef->FinalConfiguration(A, Z, a_final, z_final);
					frequencias[z_final][a_final] += 1;
					not_fission++;
				}
			}
			// end versao serial -----------------------------------------------------------------------
		
			N++;
		}

	}
	

	Int_t nEventos =  N * nTimes;

	for( int z = 1; z < __MAX_Z ; z++)
		for( int a = 1; a < __MAX_A ; a++)
			frequencias[z][a] = (frequencias[z][a] / (double)nEventos);
}
*/
void SpallationCS_MPI( const CascadeFileBuffer &buffer, int nTimes, Mcef* mcef)
{
	// Zera a matriz de frequencias
	for( int z = 1; z < __MAX_Z ; z++)
	{
		for( int a = 1; a < __MAX_A ; a++)
		{
			X[a] = a;
			frequencias[z][a] = 0;
			
			// MPI
			local_frequencias[z][a] = 0;
			// end MPI
		}
	}

	Int_t N = 0;
	Int_t not_fission = 0;
	
#ifndef CRISP_MPI
	std::ofstream ofs;	
	if( extraSpallation )
	{
		ofs.open("test.txt");
		ofs	<< std::setw(5) << "Z"
			<< std::setw(5) << "A"
			<< std::setw(20) << "ProtonMultiplicity"
			<< std::setw(20) << "NeutronMultiplicity"
			<< std::setw(20) << "AlphaMultiplicity"
			<< std::setw(10) << "E_final" << "\n";
	}
#endif // !CRISP_MPI
	
	// MPI
	Int_t local_not_fission = 0;
	// end MPI
	
 	for(unsigned int i(0); i < buffer.ExEnergy_array.size(); i++) // while ( ifs.good() )
	{
//		Double_t Energy = buffer.Energy_array[i];
		Int_t A = buffer.A_array[i];
		Int_t Z = buffer.Z_array[i];
//		Int_t Counts = buffer.Counts_array[i];
		Double_t ExEnergy = buffer.ExEnergy_array[i];

//		ReadCascadeLine( ifs, Energy, A, Z, Counts, ExEnergy );

		Int_t a_final(0), z_final(0);
		
#ifdef CRISP_MPI		
			// versao paralela --------------------------------------------------------------------------
			// MPI ----------------------------------------------------------------------
			int rank(0); MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
			int size(0); MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos

			int masterNRuns((int)(nTimes % size));
			int hostsNRuns((int)(nTimes / size));

			int start(hostsNRuns * rank);
			int end(hostsNRuns * (rank+1));

//			if( rank == 0 )	std::cout << "Rank: " << rank << " N: " << nTimes << " | hosts_N_Runs: " << hostsNRuns << " x " << size << " + master_Extra_Runs: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n";
			// End MPI ------------------------------------------------------------------
			
			for ( Int_t a = start; a < end; a++ )
			{
				try{	mcef->Generate( A, Z, ExEnergy);	}catch(std::exception &e){throw e;}
				a_final = 0; z_final = 0;
				if ( mcef->NumberFissions() == 0 )
				{
					mcef->FinalConfiguration(A, Z, a_final, z_final);
					local_frequencias[z_final][a_final] += 1;
					local_not_fission++;
				}				
//				std::cout << "Rank: " << rank << " local_frequencias[" << z_final << "][" << a_final << "]: " << local_frequencias[z_final][a_final] << "\n";
			}
			
			// extra Runs
			if(rank == 0)
			for ( Int_t i = (hostsNRuns * size); i < (hostsNRuns * size) + masterNRuns; i++ )
			{
				try{	mcef->Generate( A, Z, ExEnergy);	}catch(std::exception &e){throw e;}
				a_final = 0; z_final = 0;
				if ( mcef->NumberFissions() == 0 )
				{
					mcef->FinalConfiguration(A, Z, a_final, z_final);
					local_frequencias[z_final][a_final] += 1;
					local_not_fission++;
				}
//				std::cout << "Rank: " << rank << " local_frequencias[" << z_final << "][" << a_final << "]: " << local_frequencias[z_final][a_final] << "\n";
			}		
			// end versao paralela -----------------------------------------------------------------------
#else
			// versao serial -----------------------------------------------------------------------
			for ( Int_t i = 0; i < nTimes; i++ )
			{
				try{	mcef->Generate( A, Z, ExEnergy);	}catch(std::exception &e){throw e;}
				a_final = 0; z_final = 0;
				if ( mcef->NumberFissions() == 0 )
				{
					mcef->FinalConfiguration(A, Z, a_final, z_final);
					frequencias[z_final][a_final] += 1;
					not_fission++;
					
					if( extraSpallation )
					{
						ofs	<< std::setw(5) << z_final
							<< std::setw(5) << a_final
							<< std::setw(20) << mcef->ProtonMultiplicity()
							<< std::setw(20) << mcef->NeutronMultiplicity()
							<< std::setw(20) << mcef->AlphaMultiplicity()
							<< std::setw(10) << mcef->final_E() << "\n";
					}
				}
			}
			// end versao serial -----------------------------------------------------------------------
#endif // CRISP_MPI
			N++;
	}
	
#ifndef CRISP_MPI	
	if( extraSpallation ) extraSpallation = false;
#endif // !CRISP_MPI

#ifdef CRISP_MPI
	// MPI ------------------------------------------------------------------------------
//	for(int i(0); i < __MAX_Z; i++)
//		MPI_Allreduce( &local_frequencias[i], &frequencias[i], __MAX_A, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
	MPI_Allreduce( local_frequencias, frequencias, __MAX_A*__MAX_Z, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
	
	MPI_Allreduce( &local_not_fission, &not_fission, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
	// end MPI ---------------------------------------------------------------------------
#endif // CRISP_MPI

	Int_t nEventos =  N * nTimes;

	for( int z = 1; z < __MAX_Z ; z++)
		for( int a = 1; a < __MAX_A ; a++)
			frequencias[z][a] = (frequencias[z][a] / (double)nEventos);
}








McefAdjust::McefAdjust( //NucleusDynamics* nuc,
						std::vector< FissionData > Fissions,
						std::vector< SpallationData > Spallations, long Seed, int verbose )
{
	this->Seed = Seed;
	this->verbose = verbose;
	this->err = 1.;
	this->nuc = new NucleusDynamics(); //nuc;
	this->mpool = new MesonsPool();
//	this->mcef = mcef;
	this->Fissions = Fissions;
	this->Spallations = Spallations;
	
#ifdef CRISP_MINUIT		// nao gerar ttree quando executar usando o MINUIT
	this->generateTTree = false;
#else
	this->generateTTree = false;
#endif // CRISP_MINUIT	
	
	for(int i(0);i<10;i++)
		this->NTotalPontos[i]=0;

#ifdef CRISP_MPI
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
#else
	rank = 0;
	size = 1;
#endif // CRISP_MPI
	
	LoadGraphs();
}

McefAdjust::~McefAdjust()
{
	delete nuc;
	delete mpool;
}

void McefAdjust::TestaArquivos()
{
	if(rank != 0) return;	// somente um processo testa os arkivos
	
	bool OK(true);
	std::vector< int > FissionsToRemove;
	std::vector< int > SpallsToRemove;
	if(rank == 0) if(this->verbose >= 1) std::cout << "Verificando a existencia de todos arquivos necessarios para execucao do MCEF..." << std::endl;

	// Verifica a existencia de todos os arquivos necessários para as foto-fissoes e spallations
	std::ifstream ifs;

	if(rank == 0) if(this->verbose >= 4) std::cout << "\tVerificando dados para photofission" << std::endl;

	// Verificando dados experimentais para photofission
	for(uint a(0); a < Fissions.size(); a++)
	{
	
#ifndef CRISP_MINUIT		// graficos experimentais obrigatorios para minuit
		if(Fissions[a].ExpData == "")	// nao tem arquivo experiental
			continue;
#endif // CRISP_MINUIT

		if(rank == 0) if(this->verbose >= 4) std::cout << "\tVerificando dados experimentais \"" << Fissions[a].ExpData << "\"... ";
		ifs.open( Fissions[a].ExpData.c_str() );		
		if( !ifs.is_open() || !ifs.good() )
		{
			if(rank == 0) if(this->verbose >= 1) std::cout << "\n\t\tErro ao tentar abrir: " << Fissions[a].ExpData << std::endl;
			if(rank == 0) if(this->verbose >= 1) std::cout << "\t\tRemovendo Photofission de " << Fissions[a].NucleusName << Fissions[a].A << " por falta de arquivo." << std::endl;
			FissionsToRemove.push_back(a);
			OK = false;
			continue;
		}	// erro
		else if(rank == 0) if(this->verbose >= 4) std::cout << "OK.\n";
		ifs.close();

		// Arquivos da cascata
		std::stringstream fname;
		fname << Fissions[a].Cascades << "/photon/" << Fissions[a].NucleusName << Fissions[a].A << "/" << Fissions[a].NucleusName << Fissions[a].A << "_%d.cascade";

		// Procura todos os arquivos de cascata
		for ( uint b = 0; b < Fissions[a].Energies.size(); b++ )
		{
			TString inputFileName = Form(fname.str().c_str(), (int)Fissions[a].Energies[b]);
			if(rank == 0) if(this->verbose >= 4) std::cout << "\t\tVerificando \"" << inputFileName.Data() << "\"... ";

			ifs.open( inputFileName.Data() );
			if( !ifs.is_open() || !ifs.good() )
			{
				if(rank == 0) if(this->verbose >= 1) std::cout << "\n\t\tErro ao tentar abrir: " << inputFileName.Data() << std::endl;
				if(rank == 0) if(this->verbose >= 1) std::cout << "\t\tRemovendo Photofission para energy \"" << Fissions[a].Energies[b] << "\" de " << Fissions[a].NucleusName.c_str() << Fissions[a].A << " por falta de arquivo da cascata." << std::endl;
//				Fissions[a].Energies.erase(
				FissionsToRemove.push_back(a);
				OK = false;
			}	// erro
			else if(rank == 0) if(this->verbose >= 4) std::cout << "OK.\n";
			ifs.close();
		}
	}

	if(this->verbose >= 4) std::cout << "\tVerificando dados para spallation" << std::endl;

	// Verificando dados experimentais para spallation
	for(uint b(0); b < Spallations.size(); b++)	// Para cada Spallation
	{
		for(uint c(0); c < Spallations[b].Z_final.size(); c++)	// Para cada numero atomico final
		{
#ifndef CRISP_MINUIT		// graficos experimentais obrigatorios para minuit
			if(Spallations[b].ExpData == "")	// nao tem arquivo experiental
				continue;
#endif // CRISP_MINUIT
				
			std::ostringstream oss;
			oss <<  Spallations[b].ExpData << "/e" << Spallations[b].NucleusName << Spallations[b].Energy << "Z" << Spallations[b].Z_final[c] << ".dat";
			
			if(this->verbose >= 4) std::cout << "\t\tVerificando dados experimentais  \"" << oss.str().c_str() << "\"... ";
			
			ifs.open( oss.str().c_str() );
			if( !ifs.is_open() || !ifs.good() )
			{
				if(rank == 0) if(this->verbose >= 1) std::cout << "\n\t\tErro ao tentar abrir: " << oss.str().c_str() << std::endl;
				if(rank == 0) if(this->verbose >= 1) std::cout << "\t\tRemovendo Spallations de " << Spallations[b].NucleusName << Spallations[b].A << " por falta de arquivo." << std::endl;
				SpallsToRemove.push_back(b);
				OK = false;
				continue;
			}	// erro
			else if(rank == 0) if(this->verbose >= 4) std::cout << "OK.\n";
			ifs.close();
		}

		// Arquivo da cascata
		std::stringstream fname;
		fname << Spallations[b].Cascades << "/proton/" << Spallations[b].NucleusName << Spallations[b].A << "/" << Spallations[b].NucleusName << Spallations[b].A << "_%d.cascade";
		TString inputFileName = Form(fname.str().c_str(), (int)Spallations[b].Energy);
		
		if(this->verbose >= 4) std::cout << "\t\tVerificando \"" << inputFileName.Data() << "\"... ";
		
		ifs.open( inputFileName.Data() );
		if( !ifs.is_open() || !ifs.good() )
		{
			if(rank == 0) if(this->verbose >= 1) std::cout << "\n\t\tErro ao tentar abrir: " << inputFileName.Data() << std::endl;
			if(rank == 0) if(this->verbose >= 1) std::cout << "\t\tRemovendo Spallations de " << Spallations[b].NucleusName << Spallations[b].A << " por falta de arquivo." << std::endl;
			SpallsToRemove.push_back(b);
			OK = false;
		}	// erro
		else if(rank == 0) if(this->verbose >= 4) std::cout << "OK.\n";
		ifs.close();
	}

	if( !OK )
	{
		if(rank == 0) if(this->verbose >= 1) std::cout << "Um ou mais photofissions/spallation possuem arquivos experimetais ou cascata faltando, mude o valor do log para \"4\" e leia a descricao do erro acima." << std::endl;
	
		// retirando fotofissoes q tiverem arquivos experimentais faltando
//		for(uint a = FissionsToRemove.size() - 1; a >= 0; a--)
//			Fissions.erase(Fissions.begin() + FissionsToRemove[a]);

		// retirando spallations q tiverem arquivos experimentais faltando
//		for(uint b = SpallsToRemove.size() - 1; b >= 0; b--)
//			Spallations.erase(Spallations.begin() + SpallsToRemove[b]);

		exit(1);
	}
	else
	{
		if(rank == 0) if(this->verbose >= 1) std::cout << "Todos arquivos para execucao estao OK" << std::endl;
	}
}

void McefAdjust::GeraResultados( std::string pastaBase ) const
{
//	if(rank != 0) return;	// somente um processo gera os resultados
	
	if(rank == 0) if(this->verbose >= 1) std::cout << "\nGerando Resultados..\n";

//	TString outPrefix = "minuit/quiquadrado";

	// gera resultados para phtofissions

	if( this->Fissions.size() > 0 )
		if(rank == 0) if(this->verbose >= 2) std::cout << "\tGerando Resultados Fissoes\n";

	for( uint a(0); a < this->Fissions.size(); a++ )
	{
		FissionData Fission = this->Fissions[a];
		
		if( Fission.Results == "" )
			continue;
		
		// Arquivo da cascata
		std::stringstream fname;
		fname << Fission.Cascades << "/photon/" << Fission.NucleusName << Fission.A << "/" << Fission.NucleusName << Fission.A << "_%d.cascade";

		std::vector< double > SigmaResults;
		std::vector< double > EnergyResults;
		

#ifdef CRISP_MPI
	// versao paralela --------------------------------------------------------------------------
		// MPI ----------------------------------------------------------------------
		int masterNRuns((int)(Fission.Energies.size() % size));
		int hostsNRuns((int)(Fission.Energies.size() / size));

		int start(hostsNRuns * rank);
		int end(hostsNRuns * (rank+1));

		// if( rank == 0 ) std::cout << "Rank: " << rank << " N: " << Fission.Energies.size() << " | hosts_N_Runs: " << hostsNRuns << " x " << size << " + master_Extra_Runs: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n";
		// End MPI ------------------------------------------------------------------

		for ( uint b = 0; b < Fission.Energies.size(); b++ )
		{
			EnergyResults.push_back(Fission.Energies[b]);
		}
		
		double *Sigma = new double[hostsNRuns];
		double *SigmaTotal = new double[Fission.Energies.size()];
		
		Mcef *mcef = buildMcef(Fission.DensidadeNiveis, Fission.BarreiraFissao);


		try{
		for ( Int_t b = start; b < end; b++ )
		{
			TString inputFileName = Form(fname.str().c_str(), (int)Fission.Energies[b]);

			// Calcula photofission
			nuc->DoInitConfig(Fission.A, Fission.Z);
			Measurement m = PhotoFission(PhotofissionCascadeBuffer[a][b], nuc, mcef);
			Sigma[b-start] = m.value();
					
	//		std::cout << "Rank: " << rank << " Sigma[" << a << "]: " << Sigma[a-start] << "\n";
		}
		}catch(std::exception &e){throw e;}

		MPI_Allgather( Sigma, hostsNRuns, MPI_DOUBLE, SigmaTotal, hostsNRuns, MPI_DOUBLE, MPI_COMM_WORLD);
		
		// extra Runs
		for ( Int_t i = (hostsNRuns * size); i < (hostsNRuns * size) + masterNRuns; i++ )
		{
			// TString inputFileName = Form(buffer[i], (int)Fission.Energies[i]);

			// Calcula photofission
			Measurement m = PhotoFission(PhotofissionCascadeBuffer[a][i], nuc, mcef);
			SigmaTotal[i] = m.value();
	//		std::cout << "Rank: " << rank << " Sigma[" << i << "]: " << Sigma[i] << "\n";
		}
		
		delete mcef; mcef = 0;
		
		for ( uint b = 0; b < Fission.Energies.size(); b++ )
			SigmaResults.push_back(SigmaTotal[b]);
	//	SigmaResults = std::vector< double >(SigmaTotal, SigmaTotal + sizeof(SigmaTotal)/sizeof(double));
		
		delete[] Sigma;
		delete[] SigmaTotal;

	// end versao paralela -----------------------------------------------------------------------

#else

	// versao serial --------------------------------------------------------------------------	
		
		// Para cada energia executa Foto-fissao
		for ( uint b = 0; b < Fission.Energies.size(); b++ )
		{
			TFile *hfile = 0;
			TTree *tree = 0;
			
			if( this->generateTTree )
			{
				std::ostringstream tree_name; tree_name << "Photofission_" << Fission.NucleusName << Fission.A << "_" << Fission.Energies[b];
				hfile = new TFile( (tree_name.str()+".root").c_str(),"RECREATE", tree_name.str().c_str() );
				tree = new TTree(tree_name.str().c_str(),tree_name.str().c_str());
			}

//			tree.SetAutoSave(1000000);	// auto save every 1 Mbytes of data collected
//			tree.SetMaxVirtualSize(100000000);

			//std::ostringstream branch_name; branch_name << "Mcef";// << ".";
			//TBranch *mcef_t = tree.Branch( branch_name.str().c_str(), "Mcef", &mcef, 128000, 2 );
			//TBranch *mcef_t = tree.Bronch(branch_name.str().c_str(), "Mcef", &mcef, 128000, 1);

			TString inputFileName = Form(fname.str().c_str(), (int)Fission.Energies[b]);
			// std::cout << "Reading cascade file: " << inputFileName.Data() << std::endl;
			
			// Calcula photofission
			nuc->DoInitConfig(Fission.A, Fission.Z);
			
			Mcef *mcef = buildMcef(Fission.DensidadeNiveis, Fission.BarreiraFissao);

			
			Measurement m;
			if( this->generateTTree )
				m = PhotoFission( PhotofissionCascadeBuffer[a][b], nuc, mcef, tree );
			else
				m = PhotoFission( PhotofissionCascadeBuffer[a][b], nuc, mcef );
				
			delete mcef; mcef = 0;
				
			SigmaResults.push_back(m.value());
			EnergyResults.push_back(Fission.Energies[b]);
			
			//tree.OptimizeBaskets(10000000, 1.1, "d");
//			tree.Print();
			
			if( this->generateTTree )
			{
				// Save all objects in this file
				hfile->Write();
				// Close the file. Note that this is automatically done when you leave the application.
				hfile->Close();
				delete tree;
				delete hfile;
			}
		}
		
	// end versao serial -----------------------------------------------------------------------
#endif // CRISP_MPI

/*

		// código para ler o arquivo .root com os dados da TTree para o Mcef
		TFile *MyFile = new TFile("Photofission_Pb208_1000.root","READ");
		TTree *MyTree = (TTree *) MyFile->Get("Photofission_Pb208_1000");
		Mcef *MyObjA = 0;

		MyTree->SetBranchAddress("Mcef.2", &MyObjA);
		TBranch *branch = MyTree->GetBranch("Mcef.2");
//		branch->SetAddress(MyObjA);
//		branch->SetFile("File2.root");

		double a(0.0);
		long b_(0);
		long c_(0);

		Mcef MyObj1;
		Mcef MyObj2;
		Mcef MyObj3;

		b_ = MyTree->GetNbranches();
		c_ = branch->GetNleaves();
		
		Int_t nevent = branch->GetEntries();
		for (Int_t i=0;i<nevent;i++)
		{
			a = branch->GetEvent(i);
			std::cout << "Dbg1: " << MyObjA->final_E() << "\n";
		}
		MyFile->Close();
//		delete MyObjA;
//		delete MyTree;
		delete MyFile;
*/
		if(rank == 0)
		{
			// escrevendo arquivo de saída
			std::ostringstream f_out_name;
			f_out_name << pastaBase << "/" << Fission.Results << "/" << Fission.NucleusName << Fission.A << ".photofission";
			
			// cria a pasta de saida, caso nao exista
			gSystem->mkdir(((pastaBase + "/") + Fission.Results).c_str(), kTRUE);			
			
			if(rank == 0) if(this->verbose >= 2) std::cout << "\t\tCriando \"" << f_out_name.str().c_str() << "\"...\n";
			std::ofstream ofs( f_out_name.str().c_str() );

			// Para cada energia executa Foto-fissao
			for ( uint b = 0; b < Fission.Energies.size(); b++ )
			{
				ofs << EnergyResults[b] << "\t" << SigmaResults[b] << std::endl;
				// SigmaResults.push_back(m.value());
				// EnergyResults.push_back(Fissions[a].Energies[b]);

				
				
			}
			ofs.close();
		}
	}




	// gera resultados para spallation
	if( this->Spallations.size() > 0 )
		if(rank == 0) if(this->verbose >= 2) std::cout << "\n\tGerando Resultados Spallation\n";

	for( uint a(0); a < this->Spallations.size(); a++ )
	{
		if( Spallations[a].Results == "" )
			continue;
	
	
		std::stringstream fname;
		fname << Spallations[a].Cascades << "/proton/" << Spallations[a].NucleusName << Spallations[a].A << "/" << Spallations[a].NucleusName << Spallations[a].A << "_%d.cascade";
		TString inputFileName = Form(fname.str().c_str(), (int)Spallations[a].Energy);
		
//		TString f_out_name = Prefix + "/Photofission/" + Fissions[a].NucleusName + ".photofission";
		std::ostringstream f_out_name;
//		f_out_name << outPrefix << "/proton/" << Spallations[a].NucleusName << "/spallation/c" << Spallations[a].NucleusName << Spallations[a].Energy << "Z%d.dat";
		f_out_name << pastaBase << "/" << Spallations[a].Results << "/c" << Spallations[a].NucleusName << Spallations[a].Energy << "Z%d.dat";
		
		// cria a pasta de saida, caso nao exista
		gSystem->mkdir(((pastaBase + "/") + Spallations[a].Results).c_str(), kTRUE);			

		
		std::vector< std::vector< Int_t > > MassResult;
		std::vector< std::vector< Double_t > > SigmaResults;
		
		MassResult.resize(Spallations[a].Z_final.size());
		SigmaResults.resize(Spallations[a].Z_final.size());
		
		
		
		Mcef *mcef = buildMcef(Spallations[a].DensidadeNiveis, Spallations[a].BarreiraFissao);
		
		SpallationCS_MPI( SpallationCascadeBuffer[a], Spallations[a].N, mcef ); // std::ofstream(outFile.Data()) );
		
		delete mcef; mcef = 0;
		
		if(rank == 0)
		{
			for( uint b(0); b < Spallations[a].Z_final.size(); b++ )
			{
				TString calc_data_fname = Form(f_out_name.str().c_str(), Spallations[a].Z_final[b]);

				LoadSpallationData( Spallations[a].Z_final[b], MassResult[b], SigmaResults[b] );

				if(rank == 0) if(this->verbose >= 2) std::cout << "\t\tCriando \"" << calc_data_fname.Data() << "\"...\n";

				std::ofstream ofs( calc_data_fname.Data() );
				for( uint c(0); c < MassResult[b].size(); c++ )
				{
					ofs << MassResult[b][c] << "\t" << SigmaResults[b][c] << std::endl;	
				}
				ofs.close();
			}
		}
	}

	if(rank == 0) if(this->verbose >= 1) std::cout << "\nTerminou de gerar resultados." << std::endl;

}

void McefAdjust::GeraGraficos( std::string pastaBase ) const
{
	if(rank != 0) return;	// somente um processo gera os graficos
	
	if(rank == 0) if(this->verbose >= 1) std::cout << "\nGerando Graficos...\n";

	if( this->Fissions.size() > 0 )
		if(rank == 0) if(this->verbose >= 2) std::cout << "\n\tGerando Graficos Fissao\n";

	// criando graficos automaticos para Fissao
	for(unsigned int a(0); a < Fissions.size(); a++)
	{
		if( Fissions[a].Graphs == "" )
			continue;
		
		// Carregando dados experimentais
		TGraphErrors tge0;
		
		if( Fissions[a].ExpData != "" )
			tge0 = this->FissionExpGraphs[a];
		
		// carregando dados gerados pelo CRISP
		std::stringstream ss;
		ss << pastaBase << "/" << Fissions[a].Results << "/" << Fissions[a].NucleusName << Fissions[a].A << ".photofission";	
		
//		if(rank == 0) if(this->verbose >= 2) std::cout << "\t\tCarregando \""<< ss.str() <<"\" ...\n";
		
		TGraph tg0( ss.str().c_str(),"%lg %lg");
	
		std::stringstream ss2;
		ss2 << pastaBase << "/" << Fissions[a].Graphs << "/" << Fissions[a].NucleusName << Fissions[a].A;

		// cria a pasta de saida, caso nao exista
		gSystem->mkdir(((pastaBase + "/") + Fissions[a].Graphs).c_str(), kTRUE);			
		
		if(rank == 0) if(this->verbose >= 2)
		{
			std::cout << "\t\tGerando \"" << ss2.str().c_str() << ".png\"...\n";
		}



		
		// Criando graficos
		tge0.SetMarkerStyle(24);
		tge0.SetMarkerColor(38);
		tge0.SetMarkerSize(0.32);
//		tge0.GetXaxis()->SetTitle("\\omega [MeV]");
//		tge0.GetYaxis()->SetTitle("\\sigma(\\gamma p)[\\mub]");
		std::stringstream titleName; titleName << "Fotofissao Experimental " << Fissions[a].NucleusName << Fissions[a].A;
		tge0.SetTitle( titleName.str().c_str() );
		
		tg0.SetMarkerStyle(24);
		tg0.SetMarkerColor(1);
//		tg0.GetXaxis()->SetTitle("\\omega [MeV]");
//		tg0.GetYaxis()->SetTitle("\\sigma(\\gamma p)[\\mub]"); 
		std::stringstream titleName2; titleName2 << "Fotofissao MCEF " << Fissions[a].NucleusName << Fissions[a].A;
		tg0.SetTitle( titleName2.str().c_str() );
		
		
//		SaveGraph(&tg0, (ss2.str() + "_c").c_str(), "AC*");
//		SaveGraph(&tge0, (ss2.str() + "_e").c_str(), "AP");

		std::stringstream titleName3; titleName3 << "Fotofissao " << Fissions[a].NucleusName << Fissions[a].A;
		TMultiGraph mg0( "fotomg", titleName3.str().c_str() );
		mg0.Add(&tg0, "L*");
		mg0.Add(&tge0, "P");

		TCanvas *c = new TCanvas("c","Photofission Results",0,0, 1924,1108);
		gStyle->Reset();
		gStyle->SetOptStat("");

		TPaveText pt(0.72,0.91,0.97,0.99, "TR");
		pt.SetTextAlign(12);
		pt.AddText("#color[38]{#bullet} Experimental");
		pt.AddText("#bullet Calculation Model");
		pt.Draw();

		mg0.Draw( "A" );
				
		mg0.GetXaxis()->SetTitle("#omega [MeV]");
		mg0.GetYaxis()->SetTitle("#sigma(#gamma p)[#mub]");
//		mg0.SetTitle(("Fotofissão " + Fissions[a].NucleusName + Fissions[a].Z).c_str());

//		c->Print( (ss2.str()+".ps").c_str(), "ps" );
//		c->Print( (ss2.str()+".png").c_str(), "png" );
//		std::cout << "Dbg1: " << ss2.str() << "\n";
		c->SaveAs( (ss2.str() + ".png").c_str() );
		c->SaveAs( (ss2.str() + ".cxx").c_str() );

		delete c;
	}
	
	if( this->Spallations.size() > 0 )
		if(rank == 0) if(this->verbose >= 2) std::cout << "\n\tGerando Graficos Spallation\n";

	// criando graficos automaticos para Spallation
	for( uint a(0); a < Spallations.size(); a++ )
	{
		if( Spallations[a].Graphs == "" )
			continue;

		gStyle->Reset();
		
		TCanvas *c1 = new TCanvas( "c1", "titulo_1", 1924, 1108 );
		c1->Divide(1,1);
		c1->cd(1);



		TPad *subpad = new TPad("subpad","Titulo",.04,.04,.9,.9);
		gStyle->SetOptStat("");
		
		subpad->Draw();


		
		TPaveText pt1(0.4,0.005,0.6,0.04);
		pt1.SetBorderSize(0);
		pt1.AddText("mass number (A)");	// TText *t1=

		TPaveText pt2(0.005,0.35,0.04,0.65);
		pt2.SetBorderSize(0);
		
		TText *t2 = pt2.AddText("cross section (mb)");
		t2->SetTextAngle(90);
		t2->SetTextSize(0.03);
		
		// Titulo canvas
		std::ostringstream osst;
		if( Spallations[a].Energy >= 1000 )
			osst << "^{" << Spallations[a].A << "}" << Spallations[a].NucleusName << " + " << Spallations[a].Energy/1000.0 << "GeV p";
		else
			osst << "^{" << Spallations[a].A << "}" << Spallations[a].NucleusName << " + " << Spallations[a].Energy << "MeV p";
			
		TPaveText pt3(0.30,0.92,0.70,0.99);
		TText *t3=pt3.AddText(osst.str().c_str());
		t3->SetTextSize(0.05);

		TPaveText pt4(0.77,0.92,0.99,0.99);
		pt4.SetTextAlign(12);
		pt4.AddText("#color[38]{#bullet} Experimental");	// TText *t4=
		pt4.AddText("#times Calculation Model");	// TText *t5=

		pt1.Draw();
		pt2.Draw();
		pt3.Draw();
		pt4.Draw();
		
		subpad->Divide(3,4);
		
		TMultiGraph *mg = new TMultiGraph[Spallations[a].Z_final.size()];

//		TObjArray pbGraphs;

		NameTable nameTable;

		for( uint b(0); b < Spallations[a].Z_final.size(); b++ )
		{
			// carregando dados experimentais
			//std::ostringstream oss;
			//oss <<  Spallations[a].ExpData << "/e" << Spallations[a].NucleusName << Spallations[a].Energy << "Z" << Spallations[a].Z_final[b] << ".dat";
			TGraphErrors tge0;
			
			if( Spallations[a].ExpData != "" )	// nao carrega grafico experimental
				tge0 = this->SpallationExpGraphs[a][b]; //( oss.str().c_str(),"%lg %lg" );

			// carregando dados calculados pelo crisp
			std::ostringstream f_out_name;
			f_out_name << pastaBase << "/" << Spallations[a].Results << "/c" << Spallations[a].NucleusName << Spallations[a].Energy << "Z" << Spallations[a].Z_final[b] << ".dat";
			
//			if(rank == 0) if(this->verbose >= 2) std::cout << "\t\tCarregando \""<< f_out_name.str() <<"\" ...\n";
			
			// Titulo
			std::ostringstream ossn; ossn << Spallations[a].Z_final[b] << nameTable.get(Spallations[a].Z_final[b]); // Spallations[a].NucleusFinal[b]
			
//			TGraph tg0( f_out_name.str().c_str(),"%lg %lg");
			
			
//		 	TMultiGraph mg;
			TGraphErrors *tg_exp = new TGraphErrors(tge0);
			TGraph *tg1 = new TGraph(f_out_name.str().c_str(), "%lg %lg");
			
			subpad->cd(b+1);
			subpad->cd(b+1)->SetLogy();
//			gPad->SetLogy();
			
			subpad->SetBorderMode(0);
			subpad->cd(b+1)->SetBorderMode(0);
			
			tg_exp->SetMarkerColor(38);	
			tg_exp->SetMarkerStyle(8);
			tg_exp->SetMarkerSize(0.32);
//			tg_exp->SetMarkerStyle(20);
//			tg_exp->SetMarkerSize(0.64);

			tg1->SetMarkerColor(1);			
			tg1->SetMarkerStyle(5);
			tg1->SetMarkerSize(0.48);
//			tg1->SetMarkerStyle(4);
//			tg1->SetMarkerSize(0.64);

			if( tg_exp->GetN() > 0 )
				mg[b].Add(tg_exp, "P");
			if( tg1->GetN() > 0 )
				mg[b].Add(tg1, "P");

			//Aqui alteram-se os títulos que irão nos gráficos
			mg[b].SetTitle(ossn.str().c_str());
			gStyle->SetLabelSize(0.085,"X");
			gStyle->SetLabelSize(0.085,"Y");

			gStyle->SetTitleH(0.12);
			gStyle->SetTitleW(0.20);

			mg[b].Draw("A");
			mg[b].GetXaxis()->SetNdivisions(504);			
//			pbGraphs.Add(mg);
		}
	
		std::ostringstream ossc;
		ossc << pastaBase << "/" << Spallations[a].Graphs << "/" << Spallations[a].NucleusName << Spallations[a].Energy;
		// cria a pasta de saida, caso nao exista
		gSystem->mkdir(((pastaBase + "/") + Spallations[a].Graphs).c_str(), kTRUE);			
		
		if(rank == 0) if(this->verbose >= 2)
			std::cout << "\t\tGerando \"" << ossc.str().c_str() << ".png\"...\n";
		
//		c1->Print( (ossc.str()+".ps").c_str(), "ps" );
//		c1->Print( (ossc.str()+".png").c_str(), "png" );
		c1->SaveAs( (ossc.str()+".png").c_str() );
		c1->SaveAs( (ossc.str()+".cxx").c_str() );

		delete[] mg;
		delete subpad;
		delete c1;

	}

	if(rank == 0) if(this->verbose >= 1) std::cout << "\nTerminou de gerar graficos." << std::endl;
}



void McefAdjust::GeraGraficoRF(std::string pasta) const
{
	TCanvas *c1 = new TCanvas("c1","Grafico do RF", 1924,1108);

	Double_t x[500], y[500];
	Int_t start = 15;
	Int_t end = 45;
	int step = 5;
	Int_t n = end*step - start*step;
	
	McefModel model;

	for (int i=start*step; i<end*step; i++)
	{
		x[(int)(i-start*step)] = (double)i/(double)step;
		y[(int)(i-start*step)] = model.CalculateRf( (double)i/(double)step );
	}
	gStyle->Reset();
	gStyle->SetOptStat("");
	
	gStyle->SetLabelSize(0.034,"X");
	gStyle->SetLabelSize(0.034,"Y");
	
	TGraph gr1(n,x,y);
	gr1.SetTitle("Funcao de RF");
	gr1.GetXaxis()->SetTitle("Z^{2}/A");
	gr1.GetYaxis()->SetTitle("RF");
	
	gr1.Draw("AL");

	TImage *img = TImage::Create();
	img->FromPad(c1);
	img->WriteImage( ( (pasta + "/RF")+".png").c_str() );
	delete c1;
	delete img;

}


void McefAdjust::LoadGraphs()
{
	TestaArquivos();

	PhotofissionCascadeBuffer.resize( Fissions.size() );
	FissionExpGraphs.resize( Fissions.size() );
	
	
	// Para cada fissao
	for(uint a(0); a < Fissions.size(); a++)
	{
//		FissionExpGraphs.push_back( TGraphErrors(Fissions[a].ExpData.c_str(),"%lg %lg %lg") );
		// Somente pega os dados experimentais que estejam entre as energias calculas pelo CRISP
		// Exemplo: {100, 150, 200, 250, 300} -> So interessam as energias experimentais entre 100 e 300
		// Os valores serão emparelhados com as energias dos dados experimentais atraves de interpolacao
		if(Fissions[a].ExpData != "")	// grafco experimental eh opiciona
			FissionExpGraphs[a] = MultFillTGraphYError( Fissions[a].ExpData.c_str(), Fissions[a].Energies[0], Fissions[a].Energies[Fissions[a].Energies.size()-1] );
//		SaveGraph( &FissionExpGraphs[a], (Fissions[a].NucleusName +"_exp") );


		// Arquivos da cascata
		std::stringstream fname;
		fname << Fissions[a].Cascades << "/photon/" << Fissions[a].NucleusName << Fissions[a].A << "/" << Fissions[a].NucleusName << Fissions[a].A << "_%d.cascade";

		PhotofissionCascadeBuffer[a].resize( Fissions[a].Energies.size() );

		// para cada energia da fissão
		for ( uint b = 0; b < Fissions[a].Energies.size(); b++ )
		{
			TString inputFileName = Form(fname.str().c_str(), (int)Fissions[a].Energies[b]);

			PhotofissionCascadeBuffer[a][b].in_file = inputFileName;

			std::ifstream ifs(inputFileName.Data());
			while ( ifs.good() )
			{
				Double_t Energy(0.0);
				Int_t A(0), Z(0);
				Int_t Counts(0);
				Double_t ExEnergy(0);
				
				ReadCascadeLine( ifs, Energy, A, Z, Counts, ExEnergy );
				
				if ( ExEnergy > 0. )
				{
					PhotofissionCascadeBuffer[a][b].Energy_array.push_back(Energy);
					PhotofissionCascadeBuffer[a][b].A_array.push_back(A);
					PhotofissionCascadeBuffer[a][b].Z_array.push_back(Z);
					PhotofissionCascadeBuffer[a][b].Counts_array.push_back(Counts);
					PhotofissionCascadeBuffer[a][b].ExEnergy_array.push_back(ExEnergy);
				}
			}	
			ifs.close();
		}
	}

	SpallationCascadeBuffer.resize(Spallations.size());
	SpallationExpGraphs.resize(Spallations.size());

	for(uint b(0); b < Spallations.size(); b++)	// Para cada Spallation
	{
		// Poe os dados do arquivo da cascata no buffer
		std::stringstream fname; fname << Spallations[b].Cascades << "/proton/" << Spallations[b].NucleusName << Spallations[b].A << "/" + Spallations[b].NucleusName << Spallations[b].A << "_%d.cascade";
		TString inputFileName = Form(fname.str().c_str(), (int)Spallations[b].Energy);
		
		std::ifstream ifs(inputFileName.Data());
		while ( ifs.good() )
		{
			Double_t Energy(0.0);
			Int_t A(0), Z(0);
			Int_t Counts(0);
			Double_t ExEnergy(0);
			
			ReadCascadeLine( ifs, Energy, A, Z, Counts, ExEnergy );
			
			if ( ExEnergy > 0. )
			{
				SpallationCascadeBuffer[b].Energy_array.push_back(Energy);
				SpallationCascadeBuffer[b].A_array.push_back(A);
				SpallationCascadeBuffer[b].Z_array.push_back(Z);
				SpallationCascadeBuffer[b].Counts_array.push_back(Counts);
				SpallationCascadeBuffer[b].ExEnergy_array.push_back(ExEnergy);
			}
		}	
		ifs.close();
		 
			
		if(Spallations[b].ExpData != "")	// grafco experimental eh opiciona
			for(uint c(0); c < Spallations[b].Z_final.size(); c++)	// Para cada numero atomico final
			{
				std::ostringstream oss;
				oss <<  Spallations[b].ExpData << "/e" << Spallations[b].NucleusName << Spallations[b].Energy << "Z" << Spallations[b].Z_final[c] << ".dat";
				SpallationExpGraphs[b].push_back( TGraphErrors( oss.str().c_str(),"%lg %lg") );
			}
	}
}


Mcef* McefAdjust::buildMcef(std::string DensidadeNiveis, std::string BarreiraFissao) const
{
	Mcef *mcef;
	if( DensidadeNiveis == "comum" || DensidadeNiveis == "ramamurth" || DensidadeNiveis == "ignatyvk" || DensidadeNiveis == "goriety" )
	{
		mcef = new Mcef(new McefModel);
	} else 	if( DensidadeNiveis == "bsfg" || DensidadeNiveis == "bsfg-eg" || DensidadeNiveis == "bsfg-ct" )
	{
		mcef = new Mcef(new BsfgModel);
	} else
	{
		mcef = new Mcef(new McefModel);
	}
	
	if( BarreiraFissao == "comum" )
	{
		McefModel::SetNovoBf( false );
	} else if( BarreiraFissao == "microscopico" )
	{
		McefModel::SetNovoBf( true );
	} else
	{
		McefModel::SetNovoBf( false );
	}
	return mcef;
}


#ifdef CRISP_MINUIT

// ugly... foi declarado global porque "McefAdjust::operator() const" nao aceita acesso a atributos da classe
int NPar(1);		// Numero de parametros: usado para calcular o grau de liberdade
double BestParcialChi = -1.0;
int NAjustes = 0;	// Numero de ajustes feitos

void McefAdjust::ResetParcialChi()
{
	BestParcialChi = -1.0;
//	NAjustes = 0;
}


double McefAdjust::operator()(const std::vector<double>& v) const
{

	// -1.#IND000000000000
	for(uint i(0); i < v.size(); i++)
		if( isNaN(v[i]) )
		{	std::cerr << "Erro no parametro " << i << " seu valor esta indefinido\n*Erro Fatal!\n";	return infty; }

	NPar = v.size();
	
#ifdef CRISP_MPI
//	std::cout << "Rank["<<rank<<"]" << " - Aki\n";
//	MPI_Barrier( MPI_COMM_WORLD );

	// rank0 manda a quantidade de parametros para os outros hosts	
	if( rank == 0 )
	{
		// manda a quantidade de parametros para outros hosts
		MPI_Bcast( &NPar, 1, MPI_INT, 0, MPI_COMM_WORLD );
		std::vector< double > v2;
		v2 = v;
		MPI_Bcast( &v2[0], v2.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
	}

//	std::cout << "Rank["<<rank<<"]" << " - Aki2\n";
//	MPI_Barrier( MPI_COMM_WORLD );
#endif // CRISP_MPI
	
	// Atualiza os parametros
	AdjustParams(v);
	
	
	if(rank == 0) if(this->verbose >= 1)
	{
//		std::cout << "Rank["<<rank<<"]:" << std::endl;
		PrintParams(std::cout);
		std::cout << std::endl << std::endl;
	}
	



	
	// Re-seta Seed do Random para que a aletoriedade do Monte Carlo nao atrapalhe o MINUIT
//	std::ostringstream oss; oss << "random_mcef_" << rank;
//	gRandom->WriteRandom( oss.str().c_str() );	
	gRandom->SetSeed( this->Seed );

	const double peso1(1.0/1.0);		// Peso dado aos resultados da photofission
	const double peso2(1.0/1.0);		// Peso dado aos resultados da spallation

	double ChiSqr(0.0);
	
//	std::cout << std::setprecision(10);
	try{
	// Calcula o qui-quadrado final usando a media ponderada do resultados das fissions e spallations
	ChiSqr = peso1*ChiSqrPhotofissionAll() + peso2*ChiSqrSpallationAll();
	}catch(std::exception &e)
	{
//		gRandom->ReadRandom( oss.str().c_str() );
		if(rank == 0) if(this->verbose >= 1) std::cout << "Melhor Chi^2 = [" << std::setw(25) << BestParcialChi << "]\t\t" << getTimeStamp() << std::endl << std::endl;
		if(rank == 0) std::cerr << "Erro Fatal! " << e.what() << "\n"; return infty;
	}

//	gRandom->ReadRandom( oss.str().c_str() );

	if(rank == 0) if(this->verbose >= 1)
	{
		std::cout << std::endl;
//		std::cout << std::setprecision(20);
		std::cout << "ChiSqr Final = [" << std::setw(25) << ChiSqr << "]" << std::endl;
	}
	
	if(BestParcialChi < 0.0)
		BestParcialChi = ChiSqr;
	else
		if(BestParcialChi > ChiSqr)
			BestParcialChi = ChiSqr;	
	NAjustes++;

/*
	if( ::NAjustes%50 == 0 )
	{
		std::stringstream pasta;
		std::stringstream comando;
		std::stringstream arquivo;

		pasta << "results/minuit-mcef/parcial" << "/" << ::NAjustes;
		comando << "mkdir " << pasta.str();
		arquivo << pasta.str() << "/ajuste.txt";

		if(rank == 0)
		{
			std::cout << "\n\n---------------------------------------------------------------------------------------------------------------\n";
			std::cout << "Criando resultado parcial em " << pasta.str() << "...\n";
			gSystem->Exec( comando.str().c_str() );
		}
	    
		GeraResultados( pasta.str().c_str() );
		GeraGraficos( pasta.str().c_str(), pasta.str().c_str() );
		if(rank == 0) GeraGraficoRF( pasta.str().c_str(), "RF");

		if(rank == 0) 
		{
			std::ofstream ajuste( arquivo.str().c_str() );
			PrintParams( ajuste );
			ajuste << "\n\n"  << "Chi^2 Parcial = [" << std::setw(25) << ChiSqr			<< "]\t\t" << getTimeStamp() << std::endl;
			ajuste << "\n\n"  << "Melhor Chi^2  = [" << std::setw(25) << BestParcialChi	<< "]\t\t" << std::endl << std::endl;
			std::cout << "\n\n---------------------------------------------------------------------------------------------------------------\n";
		}
	}
*/
	
	if(rank == 0) if(this->verbose >= 1) std::cout << "Melhor Chi^2 = [" << std::setw(25) << BestParcialChi << "]\t\t" << getTimeStamp() << std::endl << std::endl;

	return ChiSqr;
}



double McefAdjust::ChiSqrSpallation( std::vector< TGraphErrors > Graphs, SpallationData Spallation, const CascadeFileBuffer &buffer, int &NTotalPontos ) const
{
	double ChiSqr(0.0);

//	TString fname = Prefix + "/proton/" + Spallation.NucleusName + "/" + Spallation.NucleusName + "_%d.cascade";
//	TString inputFileName = Form(fname.Data(), (int)Spallation.Energy);
//	TString outFile = prefix + "/" + nucleusName + energy + ".fissility";
	

//	std::vector< std::map< Int_t, Double_t > > SpallResults;
//	std::vector< std::map< Int_t, Double_t > > ExpResults;
	std::vector< std::vector< Int_t > > MassResult;
	std::vector< std::vector< Double_t > > SigmaResults;
	MassResult.resize(Spallation.Z_final.size());
	SigmaResults.resize(Spallation.Z_final.size());

//	SpallationCS( inputFileName.Data(), Spallation.NumberTimes, mcef ); // std::ofstream(outFile.Data()) );
	
	
	Mcef *mcef = buildMcef(Spallation.DensidadeNiveis, Spallation.BarreiraFissao);

	try{	SpallationCS_MPI( buffer, Spallation.N, mcef );	}catch(std::exception &e){throw e;}
	
	delete mcef; mcef=0;

	for( uint a(0); a < Spallation.Z_final.size(); a++ )
		LoadSpallationData( Spallation.Z_final[a], MassResult[a], SigmaResults[a] );

//	for( int b(0); b < Spallation.Z_final.size(); b++ )
//		LoadExperimentalData( Spallation.ExpData.c_str(), ExpResults[b], 100 );

	for( uint a(0); a < SigmaResults.size(); a++ )
		NTotalPontos += SigmaResults[a].size();

	// dados experimentais
//	Int_t N = Graph.GetN();
//	Double_t* X = Graph.GetX();
//	Double_t* Y = Graph.GetY();
	std::vector< std::vector< Double_t > > MassExp;
	std::vector< std::vector< Double_t > > SigmaExp;

	for( uint a(0); a < Graphs.size(); a++ )
	{
		Int_t N = Graphs[a].GetN();
		Double_t* X = Graphs[a].GetX();
		Double_t* Y = Graphs[a].GetY();
		std::vector< Double_t > Mass(X, X + N);
		std::vector< Double_t > Sigma(Y, Y + N);
		MassExp.push_back(Mass);
		SigmaExp.push_back(Sigma);
	}

	// Algoritmo: Emparelha os dados experimentais com os calculados pelo CRISP
	// Para cada numero de massa do vetor experimental procura se existe um com mesmo
	// numero de massa no vetor calculado pelo CRISP
	// para os encontrados calcula o Qui-quadrado

	// Existe um vetor para cada numero atomico final i.e. eAu800Z68, eAu800Z69, eAu800Z70, etc.
	// cada um deses vetores possui uma lista de numero de massa final e uma lista de sigma final

	if(rank == 0) if(this->verbose >= 4) std::cout << "\tCalculando CriSqr Spallation para " << Spallation.NucleusName << Spallation.A << "..." << std::endl;
	
	NameTable nameTable;
	
	// contador de dados emparelhados (usado para dividir o ChiSqr no final)
	int NCS(0);
	// para cada vetor Experimantal de numero atomico...
	for( uint a(0); a < MassExp.size(); a++ )
	{
		double ChiSqrP1(0.0);	// QuiQuadrado Parcial 1
		int NCS1(0);			// Contador do ChiSqrP1
		
		
		
		if(rank == 0) if(this->verbose >= 4) std::cout << "\t\tNumero Atomico = " << Spallation.Z_final[a] << nameTable.get(Spallation.Z_final[a]) << std::endl;

		// Para cada massa Experimantal de um determinado numero atomico final...
		for( uint b(0); b < MassExp[a].size(); b++ )
		{
			// Tenta encontar nos dados calculados pelo CRISP massa igual
//			std::vector< Int_t >::iterator it = find( MassResult[a].begin(), MassResult[a].end(), MassExp[a][b] );
			int p = LinearVectorFind( MassResult[a], (int)MassExp[a][b] );

			if( p != -1 )
			{
				double tempchi = sqr( (log10(SigmaExp[a][b]) - log10(SigmaResults[a][p])) );///SigmaExp[a][b] );
			
				if(rank == 0) if(this->verbose >= 4)
					std::cout	<< "\t\t\tMass = "		<< std::setw(5) << (int)MassExp[a][b]
								<< ", CRISP = "			<< std::setw(15) << SigmaResults[a][p]
								<< ", Exp = "			<< std::setw(10) << SigmaExp[a][b]
								<< ", ChiSqr = "		<< std::setw(15) << tempchi
								<< std::endl;
				
				// Encontrou um -> calcula ChiSqr nos sigmas
				ChiSqrP1 += tempchi;
				NCS1++;
			} else {
				// se não gerou ponto na determinada posição precisa retornar um chisqr bem alto
//				double tempchi = 10000000.0;
				double tempchi = sqr( (log10(SigmaExp[a][b]) - log10(1.0/15000000.0)) ); ///SigmaExp[a][b]);
				
				if(rank == 0) if(this->verbose >= 4)
					std::cout	<< "\t\t\tMass = "		<< std::setw(5) << (int)MassExp[a][b]
								<< ", CRISP = "			<< std::setw(15) << 0.0
								<< ", Exp = "			<< std::setw(10) << SigmaExp[a][b]
								<< ", ChiSqr = "		<< std::setw(15) << tempchi
								<< std::endl;
				
				// não encontrou ponto, entao chisqr tem q ser grande
				ChiSqrP1 += tempchi;
				NCS1++;
			}
		}
		// Impede divisao por zero
//		if( NCS1 > 0 )
//		{
//			ChiSqr += ChiSqrP1 / (double)NCS1;
			ChiSqr += ChiSqrP1;
			NCS++;
//		}
	}

//	// Impede divisao por zero
//	if( NCS > 0 )
//	{
//		if(this->verbose >= 3) std::cout << "\t\tCriSqr Spallation para " << Spallation.NucleusName << " = " << ChiSqr/(double)NCS << std::endl;
		if(rank == 0) if(this->verbose >= 3) std::cout << "\t\tCriSqr Spallation para " << Spallation.NucleusName << Spallation.A << " = " << ChiSqr << std::endl;
//		return ChiSqr/(double)NCS;
//	}
//	else
//	{
//		return ChiSqr/double(NCS - NPar);
//	}
	
//	if(NCS <= NPar)
		return ChiSqr;							// Qui-quadrado normal
//	else
//		return ChiSqr/double(NCS - NPar);		// Qui-quadrado reduzido
}

double McefAdjust::ChiSqrPhotofission( TGraphErrors Graph, FissionData Fission, const std::vector< CascadeFileBuffer > &buffer ) const
{

	double ChiSqr(0.0);

	Int_t N = Graph.GetN();
	Double_t* X = Graph.GetX();
	Double_t* Y = Graph.GetY();
	Double_t* Y_err = Graph.GetEY();
	
	// Se nao houve algum problema nao destruir tudo dividindo ChiSqr por zero no final
	if( N == 0 )
		return ChiSqr;

#ifdef CRISP_MPI
	// versao paralela --------------------------------------------------------------------------
	// MPI ----------------------------------------------------------------------
	int masterNRuns((int)(Fission.Energies.size() % size));
	int hostsNRuns((int)(Fission.Energies.size() / size));

	int start(hostsNRuns * rank);
	int end(hostsNRuns * (rank+1));

	// if( rank == 0 ) std::cout << "Rank: " << rank << " N: " << Fission.Energies.size() << " | hosts_N_Runs: " << hostsNRuns << " x " << size << " + master_Extra_Runs: " << masterNRuns << " = Total: " << masterNRuns+(hostsNRuns*size) << "\n";
	// End MPI ------------------------------------------------------------------
	// Arquivo da cascata
	std::stringstream fname;
	fname << Fission.Cascades << "/photon/" << Fission.NucleusName << Fission.A << "/" << Fission.NucleusName << Fission.A << "_%d.cascade";

	std::vector< double > SigmaResults;
	std::vector< double > EnergyResults;
	
	for ( uint a = 0; a < Fission.Energies.size(); a++ )
	{
		EnergyResults.push_back(Fission.Energies[a]);
	}
	
	double *Sigma = new double[hostsNRuns];
	double *SigmaTotal = new double[Fission.Energies.size()];
	
	Mcef *mcef = buildMcef(Fission.DensidadeNiveis, Fission.BarreiraFissao);

	try{
	for ( Int_t a = start; a < end; a++ )
	{
		TString inputFileName = Form(fname.str().c_str(), (int)Fission.Energies[a]);

		// Calcula photofission
		nuc->DoInitConfig(Fission.A, Fission.Z);
		Measurement m = PhotoFission(buffer[a], nuc, mcef);
		Sigma[a-start] = m.value();
				
//		std::cout << "Rank: " << rank << " Sigma[" << a << "]: " << Sigma[a-start] << "\n";
	}
	}catch(std::exception &e){throw e;}

	MPI_Allgather( Sigma, hostsNRuns, MPI_DOUBLE, SigmaTotal, hostsNRuns, MPI_DOUBLE, MPI_COMM_WORLD);
	
	// extra Runs
	for ( Int_t i = (hostsNRuns * size); i < (hostsNRuns * size) + masterNRuns; i++ )
	{
		// TString inputFileName = Form(buffer[i], (int)Fission.Energies[i]);

		// Calcula photofission
		nuc->DoInitConfig(Fission.A, Fission.Z);
		Measurement m = PhotoFission(buffer[i], nuc, mcef);
		SigmaTotal[i] = m.value();
//		std::cout << "Rank: " << rank << " Sigma[" << i << "]: " << Sigma[i] << "\n";
	}	
	
	delete mcef; mcef = 0;
	
	for ( uint a = 0; a < Fission.Energies.size(); a++ )
		SigmaResults.push_back(SigmaTotal[a]);
//	SigmaResults = std::vector< double >(SigmaTotal, SigmaTotal + sizeof(SigmaTotal)/sizeof(double));
	
	delete[] Sigma;
	delete[] SigmaTotal;

	// end versao paralela -----------------------------------------------------------------------

#else

	// versao serial --------------------------------------------------------------------------	
	// Arquivo da cascata
	std::stringstream fname;
	fname << Fission.Cascades << "/photon/" << Fission.NucleusName << Fission.A << "/" << Fission.NucleusName << Fission.A << "_%d.cascade";

	std::vector< double > SigmaResults;
	std::vector< double > EnergyResults;
	
	Mcef *mcef = buildMcef(Fission.DensidadeNiveis, Fission.BarreiraFissao);
	
	// Para cada energia executa Foto-fissao
	for ( uint a = 0; a < Fission.Energies.size(); a++ )
	{
		TString inputFileName = Form(fname.str().c_str(), (int)Fission.Energies[a]);
		// std::cout << "Reading cascade file: " << inputFileName.Data() << std::endl;

		// Calcula photofission
		nuc->DoInitConfig(Fission.A, Fission.Z);
		Measurement m = PhotoFission( buffer[a], nuc, mcef);
		SigmaResults.push_back(m.value());
		EnergyResults.push_back(Fission.Energies[a]);
	}
	
	delete  mcef; mcef=0;
	
	// end versao serial -----------------------------------------------------------------------
#endif // CRISP_MPI
	

	// Transforma o vetor de dados esperimentais em vetor tipo C++
//	std::vector<Double_t> xvector(X, X + N );
//	std::vector<Double_t> yvector(Y, Y + N );
	// Prepara os dados experimentais para serem interpolados
//	ROOT::Math::Interpolator ExpDataInterpolation(xvector, yvector, ROOT::Math::Interpolation::kLINEAR);

	// Prepara os dados calculados pelo MCEF para serem interpolados
	ROOT::Math::Interpolator FissionDataInterpolation(EnergyResults, SigmaResults, ROOT::Math::Interpolation::kLINEAR);

	if(rank == 0) if(this->verbose >= 4) std::cout << "\tCalculando ChiSqr para Phtofission " << Fission.NucleusName << Fission.A << "...  N = " << N << std::endl;
	// Calcula a soma do Qui-quadrado para todas as energias
	for ( int b = 0; b < N; b++ )
	{
//		std::cout << "X[" << b << "]: " << X[b] << "\tY[" << b << "]: " << Y[b] << "\tYe[" << b << "]: " << Y_err[b] << "\n";
//		std::cout << "" << ChiSqr << " += sqr(" << FissionDataInterpolation.Eval(X[b]) << " - " << Y[b] << ") / sqr(" << Y_err[b] << ");\n";
		if(rank == 0) if(this->verbose >= 4)
			std::cout	<< "\t\tEnergia: "		<< std::setw(5)  << X[b]
						<< ", CRISP: "			<< std::setw(15) << FissionDataInterpolation.Eval(X[b])
						<< ", Exp: "			<< std::setw(10) << Y[b]
						<< ", ErroExp: "		<< std::setw(10) << Y_err[b]
						<< ", ChiSqr = "		<< std::setw(10) << sqr(FissionDataInterpolation.Eval(X[b]) - Y[b]) / sqr(Y_err[b])
						<< std::endl;
						
		ChiSqr += sqr(FissionDataInterpolation.Eval(X[b]) - Y[b]) / sqr(Y_err[b]);
	}
//	if(this->verbose >= 3) std::cout << "\t\tCriSqr Photofision para " << Fission.NucleusName << " = " << ChiSqr/N << std::endl;
	if(rank == 0) if(this->verbose >= 3) std::cout << "\tCriSqr Photofision para " << Fission.NucleusName << Fission.A << " = " << ChiSqr << std::endl;

	// Calcula a soma do Qui-quadrado para todas as energias
//	for ( Int_t b = 0; b < EnergyResults.size(); b++ )
//	{
//		std::cout << "X[" << b << "]: " << X[b] << "\tY[" << b << "]: " << Y[b] << "\tYe[" << b << "]: " << Y_err[b] << "\n";
//		std::cout << "\t" << ChiSqr << " += sqr(" << ExpDataInterpolation.Eval(EnergyResults[b]) << " - " << SigmaResults[b] << ") / sqr(" << SigmaResults[b] << ");\n";
//		ChiSqr += sqr(ExpDataInterpolation.Eval(EnergyResults[b]) - SigmaResults[b]) / ExpDataInterpolation.Eval(EnergyResults[b]);
//	}

//	return ChiSqr/N;
//	if(N <= NPar)
//		return ChiSqr;						// Qui-quadrado normal
//	else
		return ChiSqr/double(N - NPar);		// Qui-quadrado reduzido
}

double McefAdjust::ChiSqrPhotofissionAll() const
{
	double ChiSqr(0.0);
	double TempoInicial = CRISPGetTime()*0.001;

	try{

	if( Fissions.size() > 0 )
	{
		for(uint a(0); a < Fissions.size(); a++)
			ChiSqr += ChiSqrPhotofission( FissionExpGraphs[a], Fissions[a], PhotofissionCascadeBuffer[a] );

//		if(this->verbose >= 2) std::cout << "\tCriSqr Total Photofissions(" << Fissions.size() << ") = " << ChiSqr/(double)Fissions.size() << std::endl;
		if(rank == 0) if(this->verbose >= 2) std::cout << "\tCriSqr Total Photofissions(" << Fissions.size() << ") = " << ChiSqr << ". Demorou: " << CRISPGetTime()*0.001 - TempoInicial << " segundos" << std::endl;

//		return ChiSqr/(double)Fissions.size();
		return ChiSqr;
	}
	if(rank == 0) if(this->verbose >= 2) std::cout << "\tCriSqr Total Photofissions(0) = " << ChiSqr << std::endl;
	
	}catch(std::exception &e){throw e;}

	return ChiSqr;
}



double McefAdjust::ChiSqrSpallationAll() const
{
	double ChiSqr(0.0);
	double TempoInicial = CRISPGetTime()*0.001;

	try{
	
	if( Spallations.size() > 0 )
	{
		for(uint a(0); a < Spallations.size(); a++)
		{
			int n(0);
			double chi(0.0);
			chi = ChiSqrSpallation( SpallationExpGraphs[a], Spallations[a], SpallationCascadeBuffer[a], n );
//			if( n > NTotalPontos[a] )
				ChiSqr += chi;
//			else
//				ChiSqr += 10e10;
		}

//		if(this->verbose >= 2) std::cout << "\tCriSqr Total Spallations(" << Spallations.size() << ") = " << ChiSqr/(double)Spallations.size() << std::endl;
		if(rank == 0) if(this->verbose >= 2) std::cout << "\tCriSqr Total Spallations(" << Spallations.size() << ") = " << ChiSqr << ". Demorou: " << CRISPGetTime()*0.001 - TempoInicial << " segundos" << std::endl;

//		return ChiSqr/(double)Spallations.size();
		return ChiSqr;

	}
	if(rank == 0) if(this->verbose >= 2) std::cout << "\tCriSqr Total Spallations(0) = " << ChiSqr << std::endl;
	
	}catch(std::exception &e){throw e;}
	
	return ChiSqr;
}




void McefAdjust::PrintParams(std::ostream& os) const //, const std::vector<double>& v) const
{
	for(int i(0); i < 5; i++)
	{
		os << "# prf" << std::setw(2) << i << " = [" << std::setw(10) << McefModel::Get_prf(i) << "]";
		if((i+1)%5 == 0)
			os << "\n";
		else
			os << "\t";
	}
/*
//	os << std::setprecision(20);
	os << "# prf1" << " = [" << std::setw(15) << McefModel::Get_prf1() << "]";
	os << "\t# prf2" << " = [" << std::setw(15) << McefModel::Get_prf2() << "]";

	os << "\t# prf3" << " = [" << std::setw(15) << McefModel::Get_prf3() << "]";
	os << "\t# prf4" << " = [" << std::setw(15) << McefModel::Get_prf4() << "]";
	os << std::endl;
	os << "# prf5" << " = [" << std::setw(15) << McefModel::Get_prf5() << "]";
	os << "\t# prf6" << " = [" << std::setw(15) << McefModel::Get_prf6() << "]";
	os << "\t# prf7" << " = [" << std::setw(15) << McefModel::Get_prf7() << "]";

//	os << std::endl;
	os << "# pp1  " << " = [" << std::setw(10) << McefModel::Get_pp1() << "]";
	os << "\t# pp2  " << " = [" << std::setw(10) << McefModel::Get_pp2() << "]";
	os << "\t# pa1  " << " = [" << std::setw(10) << McefModel::Get_pa1() << "]";
	os << "\t# pa2  " << " = [" << std::setw(10) << McefModel::Get_pa2() << "]";
	os << "\t# pn1  " << " = [" << std::setw(10) << McefModel::Get_pn1() << "]";
	os << std::endl;
	os << "# pn2   " << "= [" << std::setw(10) << McefModel::Get_pn2() << "]";*/
//	os << "# pbf1  " << "= [" << std::setw(15) << McefModel::Get_pbf1() << "]\n";

}

void McefAdjust::AdjustParams(const std::vector<double>& v) const
{
	for(int i(0); i < 5; i++)
		McefModel::Set_prf( i, v[i] );

/*
//	double prf1 = McefModel::Get_prf1();
	McefModel::Set_prf1( v[0] );

//	double prf2 = McefModel::Get_prf2();
	McefModel::Set_prf2( v[1] );

//	double prf3 = McefModel::Get_prf3();
	McefModel::Set_prf3( v[2] );

//	double prf4 = McefModel::Get_prf4();
	McefModel::Set_prf4( v[3] );

//	double prf5 = McefModel::Get_prf5();
	McefModel::Set_prf5( v[4] );

//	double prf5 = McefModel::Get_prf6();
	McefModel::Set_prf6( v[5] );

//	double prf5 = McefModel::Get_prf7();
	McefModel::Set_prf7( v[6] );


//	double pp6 = McefModel::Get_pp1();
	McefModel::Set_pp1( v[25] );

//	double pp7 = McefModel::Get_pp2();
	McefModel::Set_pp2( v[26] );

//	double pa8 = McefModel::Get_pa1();
	McefModel::Set_pa1( v[27] );

//	double pa9 = McefModel::Get_pa2();
	McefModel::Set_pa2( v[28] );

//	double pbf10 = McefModel::Get_pn1();
	McefModel::Set_pn1( v[29] );

//	double pbf10 = McefModel::Get_pn2();
	McefModel::Set_pn2( v[30] );
*/
//	double pbf10 = McefModel::Get_pbf1();
//	McefModel::Set_pbf1( v[5] );

}
#endif // CRISP_MINUIT

