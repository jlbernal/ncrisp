
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>

#include "TAxis.h"
#include "TGraph.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraphErrors.h"
#include "TRandom3.h"
#include "TSystem.h"

#include "CrispParticleTable.hh"
#include "NucleusDynamics.hh"
#include "PhotonEventGen.hh"
#include "photon_actions.hh"
#include "gn_cross_sections.hh"
#include "McefAdjust.hh"

#include "Timer.hh"
#include "Data.hh"

#ifdef WIN32
#pragma comment (lib, "WSock32.Lib")
#endif

const std::string defaultInputFile("input/mcef.xml");
std::string parserArguments(int argc, char *argv[]);

void mcef(std::string inputFile  );


int main( int argc, char *argv[] )
{
	std::string inputFile = parserArguments(argc, argv);
	
	int rank(0);
	int size(1);

#ifdef CRISP_MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
#endif // CRISP_MPI

	if( rank == 0 )
		std::cout << "rank: " << rank << " -- " << "size: " << size << "\n\n";

	// Gerando numeros aleat�rios diferentes para cada host
	// TRandom3 gera numeros aleat�rios muito melhores que o default TRandom
	// Eh dado um numero primo diferente para cada noh como seed
	if( gRandom )
		delete gRandom;
	gRandom = new TRandom3( primos[rank] );

	CrispParticleTable *cpt = CrispParticleTable::Instance();

	if( rank == 0 )
		std::cout << std::endl << "Loading PDG Particle Data ..." << std::endl;
		
	cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");

	if( rank == 0 )
		std::cout << "Ready." << std::endl;

#ifdef CRISP_MPI
	MPI_Barrier( MPI_COMM_WORLD );
#endif // CRISP_MPI

#ifdef _DEBUG
	char hostname[256];
	gethostname(hostname, sizeof(hostname));
#ifndef WIN32
	std::cout <<"Rank " << rank << " : PID " << getpid() << " on " << hostname << " ready for attach\n";
//	sleep(10);
#else
	std::cout <<"Rank " << rank << " : PID " << GetCurrentProcessId() << " on " << hostname << " ready for attach\n";
//	Sleep(10000);
#endif // WIN32
	fflush(stdout);
#endif // _DEBUG



	double TempoInicial(0.0), TempoFinal(0.0), TempoTotal(0.0);

	if( rank == 0 )
		TempoInicial = CRISPGetTime()*0.001;


	mcef(inputFile);

	if( rank == 0 )
	{
		TempoFinal = CRISPGetTime()*0.001;
		TempoTotal = TempoFinal - TempoInicial;
		std::cout << "Tempo total de execucao do programa: ";
		FormTime( TempoTotal, std::cout );
		std::cout << std::endl << std::endl << std::endl;
	}

	
#ifdef CRISP_MPI
	MPI_Finalize();
#endif // CRISP_MPI



	return 0;
}







void mcef( std::string inputFile )
{
	int rank(0);
	int size(1);
	
#ifdef CRISP_MPI
	MPI_Comm_rank( MPI_COMM_WORLD, &rank ); // rank dos processos
	MPI_Comm_size( MPI_COMM_WORLD, &size ); // Numero de processos
#endif // CRISP_MPI

	//	Como trabalhar com o MCEF

	//	O vetor "Int_t e[]" contem a lista de energias que a photofission vai trabalhar,
	//		eh necessario ter um arquivo de cascata para cada uma dessa energias
	//	O vetor "std::vector< Int_t > energies" eh somente uma conversao do vetor estilo C "Int_t e[]" para estilo C++

	double e[] = { 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0 };
	std::vector< double > energies(e, e + sizeof(e)/sizeof(double));

	//	O vetor "std::vector< FissionData > Photonfissions" contem as informacoes sobre as Photofissions que devem ser testadas
	//	Cada "FissionData" contem uma Photofission
	//	
	//	Ex.: FissionData("minuit/quiquadrado/chumbo_ce.dat", "Pb208", energies, 208, 82)
	//		1. par: string com o diretorio e nome do arquivo experimental
	//		2. par: string com o nome do nucleo
	//		3. par: vetor com a lista de energias a seres testadas
	//		4. par: numero de massa do elemento
	//		5. par: numero atomico do elemento

	std::vector< FissionData > Photonfissions;
//	Photonfissions.push_back( FissionData(208, 82, energies, "exp_data/mcef/photofission/chumbo_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Pb208", "results/mcef/photofission/Pb208" ) );
//	Photonfissions.push_back( FissionData(232, 90, energies, "exp_data/mcef/photofission/torio_ce.dat",		0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Th232", "results/photofission/mcef/Th232" ) );
//	Photonfissions.push_back( FissionData(235, 92, energies, "exp_data/mcef/photofission/uranio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/U235", "results/photofission/mcef/U235" ) );
//	Photonfissions.push_back( FissionData(237, 93, energies, "exp_data/mcef/photofission/neptunio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/Np237", "results/photofission/mcef/Np237" ) );
//	Photonfissions.push_back( FissionData(238, 92, energies, "exp_data/mcef/photofission/uranio_ce.dat",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/photofission/U238", "results/photofission/mcef/U238" ) );
	
	//	O vetor "std::vector< SpallationData > Spallations" contem as informacoes sobre as Spallations que devem ser testadas
	//	Cada "SpallationData" contem um Spallation
	//	
	//	Ex.: SpallationData("minuit/quiquadrado", "Pb", "Pb208", 1000, Pb_Z_final, Pb_NucleusFinal, 150)
	//		1� par: string com o diretorio onde estao os arquivos experimentais (sao varios)
	//		2. par: string com o nome do nucleo
	//		3. par: numero atomico
	//		4. par: massa
	//		4. par: energia que vai ser feita o teste
	//		5. par: vetor com os valores finais de numero atomico
	//		6. par: vetor com os nomes do elementos quimicos finais
	//		7. par: numero de vezes que o Spallation deve ser rodado

	std::vector< SpallationData > Spallations;
//	Spallations.push_back( SpallationData(82, 208, 50, 1000, "exp_data/mcef/spallation/Pb208",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/Pb208", "results/mcef/spallation/Pb208") );
//	Spallations.push_back( SpallationData(79, 197, 50,  800, "exp_data/mcef/spallation/Au197",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/Au197", "results/mcef/spallation/Au197") );
//	Spallations.push_back( SpallationData(92, 238, 50, 1000, "exp_data/mcef/spallation/U238",	0.0, 5000.0, "exp_data/mcef/mcmc", "results/mcef/spallation/U238", "results/mcef/spallation/U238") );


	//	McefAdjust eh a classe que realiza todos os calculos do MCEF
	//  ela gera os resultados e grafica automaticamentes
	//
	//	Ex. de uso: McefAdjust(nuc, "paralelo/results", Photonfissions, Spallations);
	//		1. par: instancia da classe NucleusDynamics
	//		2. par: string com a pasta onde se encontram os arquivos da cascata
	//		3. par: Lista de Photonfissions a seres feitas
	//		4. par: Lista de Spallations a seres feitas
	//		5. par: (Parametro opcional) define o nivel de informacao a ser exibida durante a minimizacao
	//			valor minimo eh 0 (para nao mostrar o minimo possivel na tela) e maximo eh 4 (para mostrar todas as mensagens do programa)
//	McefAdjust Full_Adj(nuc, Photonfissions, Spallations, primos[rank], 4);
	
	CRISP_XML_Parser c("input/mcef.xml");
	
	if( rank == 0 ) c.showAll();
	if( rank == 0 ) if( c.hasErrors() )
	{
		std::cout << "\n\n";
		std::cout << "###########################################################\n";
		std::cout << "#-Houveram erros no arquivo de entrada                    #\n";
		std::cout << "#-Leia o log acima e corrija-os.                          #\n";
		std::cout << "#-Abortando execucao.                                     #\n";
		std::cout << "###########################################################\n\n";
		exit(1);
	}
	
	Photonfissions = c.getPhotofission();
	Spallations = c.getSpallations();
	
	
	if( (Photonfissions.size() == 0) && (Spallations.size() == 0) )
	{
		if( rank == 0 ) std::cout << "Nenhuma Fotofissao ou Spallation para executar, o arquivo de entrada est� errado ou vazio.\n";

	} else {
		
		McefAdjust Adj(Photonfissions, Spallations, primos[rank], c.getLogLevel());
		if( rank == 0 ) std::cout << "Gerando resultados\n";
		Adj.GeraResultados( c.getPastaBase() );
		if( rank == 0 ) std::cout << "Gerando graficos\n";
		Adj.GeraGraficos( c.getPastaBase() );
	}
}



std::string parserArguments(int argc, char *argv[])
{
	if( argc > 0 )
	for(int i(0); i < argc; i++)
	{
		std::string cmd(argv[i]);
		if(cmd == "-i")
		{
			// arquivo de entrada
			if( i+1 < argc )
			{
				std::string path(argv[i+1]);
				return path;
			} else {
				std::cout << "arquivo de entrada n�o definido!\n";
				exit(1);
			}
		}
	}
	return defaultInputFile;
}


