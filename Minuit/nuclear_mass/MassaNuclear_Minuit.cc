//================================================================================================================
//	Este código faz o ajuste de parâmetros da fórmula semi-empírica de massa utilizando o Minuit 
// como implementado na classe TMinuit.h
//
// Não depende das bibliotecas do CRISP. Deve ser executado no prompt do root (interpretado)
//	
// Para lista de comandos do Minuit ver: http://wwwasdoc.web.cern.ch/wwwasdoc/minuit/node18.html
//
//	Autor: Evandro A. Segundo	 email:evandro.segundo@gmail.com
//
//================================================================================================================

#include "TMinuit.h"
#include "TMath.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>



using namespace std;

int countCall = 0;

double mass_correction(int a=0, double c1=0., double c2=0.){

	return c1*a + c2;
}

void ChiSquare(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){

	++countCall;

	//Todas as grandezas estão em MeV								 
	double m_n = 939.565, 
			 m_p = 938.272, 
			 m_e = 0.511, 	 
			 u  = 931.478,  
			 delta = 0.,
			 m = 0.;

	long double ChiSqr = 0,
					chisquare = 0;

	long double delt = 0;

	int i = 0;
	int Z = 0, A = 0, N = 0;
	double M = 0, ErrM = 0;

	//parâmetros fixos Pearson
	double r0 = (1.233*pow(10.,-15))/(1.97*pow(10.,-13)),
			 e  = 0.08542,	
			 Ac = (3. * e*e)/(5. * r0),
			 mN = 939.566, 
			 mH = 938.272;
	//variáveis úteis Pearson
	double J = 0.,
			 Mpearson = 0.,
			 Mbase = 0.;

   ifstream ifs("exp_data/nuclear_masses/nuclear_mass_exp.txt");

	ifs >> Z >> A >> M >> ErrM;

	while(ifs.good()){

		if(A > 50){

			N = A - Z;
			M = M*pow(10.,-6.)*u;
			ErrM = ErrM*pow(10.,-6.)*u;	

			J = (double)(N-Z)/(double)A;
			Mpearson =  A* ( par[0]
							+ par[1] * (pow((double)A,-1./3.))
					   	+ Ac * (Z * Z)*(pow((double)A, -4./3.))
							+ ( par[2] + par[3] * pow((double)A,-1./3.) )
							* J*J );
			Mbase = Z * mH + N * mN;
			m = Mpearson + Mbase + mass_correction(A, par[4], par[5]);

/*
		if( Z%2 == 0 ){
			if( N%2 == 0 ){

				delta = -par[4]*pow((double)A,-3./4.);
			}
		}
		else{
			if( N%2 != 0 ){

				delta = par[4]*pow((double)A,-3./4.);
			}
		}

		m = Z*m_p + N*m_n + Z*m_e
			 - par[0]*A 												//volume
			 + par[1]*pow(A,2./3.) 									//superfície
			 + par[2]*( (Z*(Z-1.))/pow(A,1./3.) ) 				//coulombiano
			 + par[3]*( ((double)(N-Z)*(N-Z))/(double)A ) 	//simetria
			 + delta
			 + mass_correction(Z, A, par[5], par[6], par[7], par[8], par[9], par[10], par[11], par[12]);
*/


			delt = (M - m) / ErrM;

			chisquare += (delt*delt);

			i++;
		}
	
		ifs >> Z >> A >> M >> ErrM;

		//delta = 0;
		
	}

	ChiSqr = chisquare / (double)( i - npar );

	f = ChiSqr;
}



int MassaNuclear_Minuit(){

	TMinuit min(15);

	min.SetPrintLevel(3);
	//  select verbose level:
  	//    default :     (58 lines in this test)
  	//    -1 : minimum  ( 4 lines in this test)
  	//     0 : low      (31 lines)
  	//     1 : medium   (61 lines)
  	//     2 : high     (89 lines)
  	//     3 : maximum (199 lines in this test)

	min.SetFCN(ChiSquare);

	static const int arg = 2;
	double arglist[arg];

	int ierflg = 0;

	//Definição do parâmetro de erro UP
  	arglist[0] = 1; //UP=1 para chisquare
  	min.mnexcm("SET ERR", arglist ,1 , ierflg);

  	//Configurando parâmetros, starts, steps e limites
	static const int nPar = 6;

//CONJUNTO DE PARÂMETROS
//________________________________________________________________

//	PARÂMETROS PARA A FÓRMULA DE MASSA CHUNG + CORREÇÃO

// a_v = 15.68, a_s = 18.56, a_c = 0.717, a_sim = 28.1, par do delta = 34. MEYER
/*
	double vstart[nPar] = {15.68,		   //a_v
						  		  18.56, 		//a_s
						 		  0.717, 		//a_c
						  		  28.1, 	   	//a_sim
						  		  34.,			//par do delta

						  		  1.055,			//c1_corr
						  		  0.0175,		//c2_corr
							     0.01,		//c3_corr
								  1.055,		   //c4_corr
						  		  0.98,			//c5_corr
						  		  0.88,		   //c6_corr
							     0.95,		   //c7_corr
								  0.95};		//c8_corr
*/

//	PARÂMETROS PARA A FÓRMULA DE MASSA PEARSON
// Av = -15.65, Asf = 17.63, Asym = 27.72, Ass = -25.60;


	double vstart[nPar] = {   -1.62487e+01 ,
    							    1.95867e+01 ,
      							  1.04119e+01 ,
     								   1.28867e+02, 
										0.,
										0.};

/*
	double vstart[nPar] = {-15.60,	   //Av
						  		   17.69, 		//Asf
						 		   27.78, 		//Asym
						  		  -25.55,		//Ass
						 		    1., 		//c1_corr
						  		    1.};		//c2_corr

*/

//_______________________________________________________________________________________


/*
  	double step[nPar] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
  	min.mnparm(0, "a_v", vstart[0], step[0], 0.01, 0, ierflg); 
  	min.mnparm(1, "a_s", vstart[1], step[1], 0.01, 0, ierflg);  
  	min.mnparm(2, "a_c", vstart[2], step[2], 0.01, 0, ierflg);  
  	min.mnparm(3, "a_sim", vstart[3], step[3], 0.01, 0, ierflg);  
	min.mnparm(4, "a_delt", vstart[4], step[4], 0.01, 0, ierflg);

	//parâmetros da correção
  	min.mnparm(5, "c1_corr", vstart[5], step[5], 0, 0, ierflg);  
  	min.mnparm(6, "c2_corr", vstart[6], step[6], 0, 0, ierflg);   
  	min.mnparm(7, "c3_corr", vstart[7], step[7], 0, 0, ierflg);   
  	min.mnparm(8, "c4_corr", vstart[8], step[8], 0, 0, ierflg);
  	min.mnparm(9, "c5_corr", vstart[9], step[9], 0, 0, ierflg);  
  	min.mnparm(10, "c6_corr", vstart[10], step[10], 0, 0, ierflg);   
  	min.mnparm(11, "c7_corr", vstart[11], step[11], 0, 0, ierflg);   
  	min.mnparm(12, "c8_corr", vstart[12], step[12], 0, 0, ierflg);


	min.FixParameter(0);
	min.FixParameter(1);         
	min.FixParameter(2);
	min.FixParameter(3);
	min.FixParameter(4);

//	min.FixParameter(5);
//	min.FixParameter(6);
//	min.FixParameter(7);
//	min.FixParameter(8);
//	min.FixParameter(9);
//	min.FixParameter(10);
//	min.FixParameter(11);
//	min.FixParameter(12);        
*/

  	double step[nPar] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
  	min.mnparm(0, "Av", vstart[0], step[0], 0, 0, ierflg); 
  	min.mnparm(1, "Asf", vstart[1], step[1], 0, 0, ierflg);  
  	min.mnparm(2, "Asym", vstart[2], step[2], 0, 0, ierflg);  
  	min.mnparm(3, "Ass", vstart[3], step[3], 0, 0, ierflg);  
  	min.mnparm(4, "c1_corr", vstart[4], step[4], 0, 0, ierflg);  
  	min.mnparm(5, "c2_corr", vstart[5], step[5], 0, 0, ierflg); 

//	min.FixParameter(0);
//	min.FixParameter(1);         
//	min.FixParameter(2);
//	min.FixParameter(3);
	min.FixParameter(4);
	min.FixParameter(5);

	cout << "\n Minimizacao..." << endl << endl;

  	//Minimização
  	arglist[0] = 3; //level
	min.mnexcm("SET PRIntout", arglist, 1, ierflg);

	//min.mnexcm("SCAN", 0, 0, ierflg);

	//Configuração do MINImize. Se MIGrad não converge, muda para SIMplex 
	//0.001*[tolerance]*UP
	//UP=1 para chisquare (comando SET ERR)
  	arglist[0] = 2000; //máximo de chamadas
  	arglist[1] = 0.001;  //tolerance
  	//min.mnexcm("MINI", arglist, 2, ierflg);
	//min.mnexcm("SIMplex", arglist, 2, ierflg);
	min.mnexcm("MIGrad", arglist, 2, ierflg);

	//Busca por mínimo local adicional - Comando IMProve
	//arglist[0] = 100; //máximo de chamadas
  	//min->mnexcm("IMP", arglist ,1 , ierflg);

	return 0;
}
  
