/* ================================================================================
 * 
 * 	Copyright 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __LeptonSurface_pHH
#define __LeptonSurface_pHH

#include <map>
#include "TObject.h"
#include "AbstractCascadeProcess.hh"
#include "BasicException.hh"
#include "time_surface.hh"

//#ifndef CRISP_DEBUG_WRITE
//#define CRISP_DEBUG_WRITE
//#endif

class LeptonSurface: public AbstractCascadeProcess{

private:
	
	std::map<int, double> lepton_surface; 

protected:

public:

	///
	//_________________________________________________________________________________________________										

	LeptonSurface(LeptonsPool& lpool);

	///
	//_________________________________________________________________________________________________										

	virtual ~LeptonSurface();
  
	///
	//_________________________________________________________________________________________________										

	Double_t FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin);
  
	///
	//_________________________________________________________________________________________________										

	bool Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add);
  
	///
	//_________________________________________________________________________________________________										

	void Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);

	///
	//_________________________________________________________________________________________________										

	void Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, Double_t t);

	///
	//_________________________________________________________________________________________________										

	void AddLeptonSurfaceProcess( int key, LeptonsPool& lpool, NucleusDynamics& nuc, double t );
	
	void ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);


/*	
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(LeptonSurface, 0);
#endif // CRISP_SKIP_ROOTDICT
*/

  
};


#endif
