/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "BaryonSurface.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(BaryonSurface);
#endif // CRISP_SKIP_ROOTDICT

BaryonSurface::BaryonSurface(NucleusDynamics& nuc):AbstractCascadeProcess(), bs(), bs_counter(){

	last_count = 0;  
	this->Initialize(nuc); 
}
//_________________________________________________________________________________________________										
BaryonSurface::~BaryonSurface(){

	bs.clear();
	bs_counter.clear();
}
//_________________________________________________________________________________________________										
void BaryonSurface::Initialize(NucleusDynamics& nuc){
	for ( int i = 0; i < nuc.GetA(); i++ ) {
		try { 
			double t = time_surface ( nuc[i], nuc );
			bs.push_back(t);
			bs_counter.push_back(0);
		} 
		catch (BasicException Ex) {       
			TString msg = Ex.ErrorMsg() +
			Form("\nOn BaryonSurface::Initialize(...): nucleon idx = %d ", i);     
			Ex.SetErrorMsg(msg);
			throw(Ex);
		}
	}  
	last_count = -1;
}
//_________________________________________________________________________________________________										
void BaryonSurface::Update( Int_t , Int_t , NucleusDynamics& , MesonsPool&, LeptonsPool& , Double_t){
	// Should not implement
}
//_________________________________________________________________________________________________										
Double_t BaryonSurface::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){
	int k = -1; 
	double t = tmin;
	for ( UInt_t i = 0; i < bs.size() ; i++ ) {
		if ( bs[i] < t ) { 
			k = i; 
			t = bs[i]; 
		}      
	}  
	idx1 = k;
	return t;
}
//_________________________________________________________________________________________________										
bool BaryonSurface::Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool,  std::vector<ParticleDynamics>& vec_add){
	int idx = i;	
	//double t_mass = nuc.GetMass() + mpool.TotalMass();
	double t_charge = nuc.TotalCharge() + mpool.TotalCharge();  
	double v0 = nuc.NuclearPotential( nuc[idx].PdgId() ); //this one includes coulomb barrier for protons
	double well = nuc.NuclearPotentialWell( nuc[idx].PdgId() ); //this one refers only to potential due to the well
	   
	//NOTE: Traditional WKB aproximation is used to determine the barrier penetration
	if ( it_was_even_so( nuc[idx], nuc.GetRadium(), t_charge, v0, well, bs[idx] ) ) {  
		if (nuc.UnBindNucleon(idx)){
			
	//		modifies the momentum and mass of the particle to exit the nucleus fulfilling the energy conservation law
			TVector3 p_in = nuc[idx].Momentum().Vect();
			Double_t m_out	= nuc[idx].GetInvariantMass();
			Double_t m_in = nuc[idx].GetMass();

			Double_t p2_out	= TMath::Abs(p_in.Mag2() + sqr(m_in) - sqr(m_out));
			TVector3 p_out = p_in.Unit()*TMath::Sqrt(p2_out);

			nuc[idx].SetMass(m_out);
			nuc[idx].SetMomentum(p_out);

			//taking away emitted particle's momentum from nucleus
			Double_t eTemp = sqrt( nuc[idx].GetInvariantMass()*nuc[idx].GetInvariantMass() + p_out.Mag2());
			TLorentzVector pTemp(p_out, eTemp);
			nuc.SetMomentum(nuc.Momentum() - pTemp);
			//taking away emitted particle's angulars momentum from nucleus
			TVector3 lTemp = nuc[idx].Position().Cross(p_out);
			nuc.SetAngularMomentum(nuc.AngularMomentum() - lTemp);
	//		std::cout << "Kinetic energy 2 = " << nuc[idx].TKinetic() << ", key = " << idx << std::endl;
	//		p_in.Print();
	//		p_out.Print();
			return true;
		}
	}
	return true;
}
//_________________________________________________________________________________________________										
void BaryonSurface::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){
	try { 
		double ti = ( !nuc[k].IsBind() )? infty: time_surface( nuc[k], nuc );
		if ( TMath::Abs( ti < 1E-7 ) ) 
			ti = .00001;
		bs[k] = t + ti;
		++(bs_counter[k]);  
	}
	catch (BasicException ex) {
		throw ex;
	}
}


void BaryonSurface::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

	//std::cout << "inside BaryonSurface::ForceResonanceDecay" << std::endl;
	//Forcing decay of baryonic resonance
	if( CPT::IsImplRessonance( nuc[idx1].PdgId()) ){
		std::cout << "Resonance leaving... forcing decay" << std::endl;
		std::vector<ParticleDynamics> daughters;
		bool accepted = false;
		int count = 0;
		while(!accepted){
			//std::cout << "looking for a decay channel" << std::endl;
			double x = gRandom->Uniform();
			if( get_decay_channel(x, nuc[idx1].GetParticleData(), daughters) ) {
				//cout << "found a channel, testing it, number of daughters: " << daughters.size() << endl;
				accepted = ForcedDecayFinalState(idx1, nuc, mpool, daughters, bs[idx1]);
				//if(!accepted) cout << "found but not permitted" << endl;
				count++;
				if(count > 1000000 && !accepted){
					std::cout << "Forcing a decay without kinematics. count = " << count << std::endl;
					accepted = ForcedDecayNoKinematics(idx1, nuc, mpool, daughters, bs[idx1]);
				}
			}
		}
		std::cout<< "Final state for forced decay accepted" << endl;
	}
}

bool BaryonSurface::ForcedDecayFinalState( int idx, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<ParticleDynamics> daughters, Double_t current_time){

	ParticleDynamics baryon;    
	ParticleDynamics parent = nuc[idx];
	
	for ( unsigned int i = 0; i < daughters.size(); i++ ) {    
		if ( CPT::IsImplRessonance( daughters[i].PdgId() ) || CPT::IsNucleon( daughters[i].PdgId() ) ){      
			baryon = daughters[i];      
			// remove the baryon from the daughters list.
			daughters.erase( daughters.begin() + i );
		}
	}
	
	if ( daughters.size() == 1 ) {    
		
		if ( CPT::IsNucleon( baryon.PdgId() ) ){ 
			baryon.SetMass( baryon.GetInvariantMass() );
			
			if ( CPT::IsMesonResonance( daughters[0].PdgId() ) ) {      
				//std::cout << "resonance decay: nucleon + meson resonance" << std::endl;
				double upper_cut = daughters[0].GetMass() + daughters[0].GetTotalWidth();
				upper_cut = ( upper_cut > parent.Momentum().E() -  baryon.GetMass() ) ? parent.Momentum().E() -  baryon.GetMass(): upper_cut;
				double rssMass = resonance_mass_outside( upper_cut, daughters[0].PdgId() );
				daughters[0].SetMass ( rssMass );
				// daughters[0].SetLifeTime( bd_time[idx] + daughters[0].HalfLife() );
				daughters[0].SetLifeTime( current_time + daughters[0].HalfLife() );
			}
			else{
				//std::cout << "resonance decay: nucleon + meson" << std::endl;
				daughters[0].SetMass( daughters[0].GetInvariantMass() );
			}
		}
		else { 
			//std::cout << "resonance decay: nucleon resonance + meson" << std::endl;
			//if the baryon is a resonance, the other decay product is a meson pi (no width)
			daughters[0].SetMass( daughters[0].GetInvariantMass() );
			
			double upper_cut = baryon.GetMass() + baryon.GetTotalWidth();
			upper_cut = ( upper_cut > parent.Momentum().E() -  daughters[0].GetMass() ) ? parent.Momentum().E() -  daughters[0].GetMass(): upper_cut;
			double rssMass = resonance_mass_outside( upper_cut, baryon.PdgId() );
			baryon.SetMass( rssMass );
			baryon.SetLifeTime ( current_time + baryon.HalfLife() );
		}        
		
		// calculate the momenta for baryon and the meson in daughters.
		if ( !decayKinematic( parent, baryon, daughters[0] ) ) {
			daughters.clear();   
			return false;    
		}  

		double m_f = daughters[0].GetMass() + baryon.GetMass();    

		// check pauli blocking to result baryon
		if ( parent.GetMass() >= m_f ) {
			
			particles_walk_thru(parent, time_surface( parent, nuc ));
			// Setting baryon position
			baryon.SetPosition( parent.Position() );
			// Setting meson position ... 
			daughters[0].SetPosition( parent.Position()); 
			
			baryon.BindIt(false);
			nuc[idx] = baryon;
			daughters[0].BindIt(false);
			mpool.PutMeson(daughters[0]);
			return true;
		}
		else{
			daughters.clear();
			return false;
		}
	}
	else if(daughters.size() > 1){
		//std::cout << "resonance decay: nucleon + 2 mesons" << std::endl;
		std::vector<ParticleDynamics> v;
		baryon.SetMass( baryon.GetInvariantMass() );
		v.push_back( baryon );
		
		for( unsigned int i = 0; i < daughters.size(); i++ ){
 			daughters[i].SetMass( daughters[i].GetInvariantMass() );
			v.push_back( daughters[i] );
 		}
 		
 		double m_f = baryon.GetMass();
		for( unsigned int i = 0; i < daughters.size(); i++ ){  
			m_f = m_f + daughters[i].GetMass();
		}
 		
 		if ( parent.GetMass() < m_f ) {      
// 			std::cout << "resonance decay: mass of daughters higher than mass of parent" << std::endl;
// 			std::cout << "parent: " << parent.PdgId() << "  mass: " << parent.GetMass() << std::endl;
// 			std::cout << "baryon: " << baryon.PdgId() << "  mass: " << baryon.GetMass() << std::endl;
// 			for( unsigned int i = 0; i < daughters.size(); i++ ){
// 				std::cout << "daughters[" << i << "]: " << daughters[i].PdgId() << "  mass: " << daughters[i].GetMass() << std::endl;
// 			}
			daughters.clear();
			return false;
		}
 		
 		if(!particlesOutMomentum(parent.Momentum(), v)){
			daughters.clear();      
			return false;
		}
		
		particles_walk_thru(parent, time_surface( parent, nuc ));
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		baryon.BindIt(false);
		nuc[idx] = baryon;

		// Setting meson position ... 
		for( unsigned int i = 0; i < daughters.size(); i++ ){
			daughters[i].SetPosition( parent.Position()); 
			daughters[i].BindIt(false);
			mpool.PutMeson(daughters[i]);
			return true;
		}
		
		//std::cout << "resonance decay: Pauli blocked" << std::endl;
	}
	std::cout << "1 WARNING! Something wrong with the decay! Less than 2 particles in the final state vector." << std::endl;
	daughters.clear();
	return false;
}


bool BaryonSurface::ForcedDecayNoKinematics( int idx, NucleusDynamics& nuc, MesonsPool& mpool, std::vector<ParticleDynamics> daughters, Double_t current_time){

	ParticleDynamics baryon;    
	ParticleDynamics parent = nuc[idx];
	
	for ( unsigned int i = 0; i < daughters.size(); i++ ) {    
		if ( CPT::IsImplRessonance( daughters[i].PdgId() ) || CPT::IsNucleon( daughters[i].PdgId() ) ){      
			baryon = daughters[i];      
			// remove the baryon from the daughters list.
			daughters.erase( daughters.begin() + i );
		}
	}
	
	if ( daughters.size() == 1 ) {    
		
		if ( CPT::IsNucleon( baryon.PdgId() ) ){ 
			baryon.SetMass( baryon.GetInvariantMass() );
			
			if ( CPT::IsMesonResonance( daughters[0].PdgId() ) ) {      
				//std::cout << "resonance decay: nucleon + meson resonance" << std::endl;
				double upper_cut = daughters[0].GetMass() + daughters[0].GetTotalWidth();
				upper_cut = ( upper_cut > parent.Momentum().E() -  baryon.GetMass() ) ? parent.Momentum().E() -  baryon.GetMass(): upper_cut;
				double rssMass = resonance_mass_outside( upper_cut, daughters[0].PdgId() );
				daughters[0].SetMass ( rssMass );
				// daughters[0].SetLifeTime( bd_time[idx] + daughters[0].HalfLife() );
				daughters[0].SetLifeTime( current_time + daughters[0].HalfLife() );
			}
			else{
				//std::cout << "resonance decay: nucleon + meson" << std::endl;
				daughters[0].SetMass( daughters[0].GetInvariantMass() );
			}
		}
		else { 
			//std::cout << "resonance decay: nucleon resonance + meson" << std::endl;
			//if the baryon is a resonance, the other decay product is a meson pi (no width)
			daughters[0].SetMass( daughters[0].GetInvariantMass() );
			
			double upper_cut = baryon.GetMass() + baryon.GetTotalWidth();
			upper_cut = ( upper_cut > parent.Momentum().E() -  daughters[0].GetMass() ) ? parent.Momentum().E() -  daughters[0].GetMass(): upper_cut;
			double rssMass = resonance_mass_outside( upper_cut, baryon.PdgId() );
			baryon.SetMass( rssMass );
			baryon.SetLifeTime ( current_time + baryon.HalfLife() );
		}        
		TVector3 pNull(0,0,0);
		baryon.SetMomentum( pNull );
		daughters[0].SetMomentum( pNull );
		
		particles_walk_thru(parent, time_surface( parent, nuc ));
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		// Setting meson position ... 
		daughters[0].SetPosition( parent.Position());     

		baryon.BindIt(false);
		nuc[idx] = baryon;
		daughters[0].BindIt(false);
		mpool.PutMeson(daughters[0]);
		return true;
	}
	else if(daughters.size() > 1){
		//std::cout << "resonance decay: nucleon + 2 mesons" << std::endl;
		std::vector<ParticleDynamics> v;
		baryon.SetMass( baryon.GetInvariantMass() );
		v.push_back( baryon );
		
		for( unsigned int i = 0; i < daughters.size(); i++ ){
 			daughters[i].SetMass( daughters[i].GetInvariantMass() );
			v.push_back( daughters[i] );
 		}
 		
		TVector3 pNull(0,0,0);
		baryon.SetMomentum( pNull );
 		
		for( unsigned int i = 0; i < daughters.size(); i++ ){  
			daughters[i].SetMomentum( pNull );
		}
 		
		particles_walk_thru(parent, time_surface( parent, nuc ));
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		baryon.BindIt(false);
		nuc[idx] = baryon;

		// Setting meson position ... 
		for( unsigned int i = 0; i < daughters.size(); i++ ){
			daughters[i].SetPosition( parent.Position()); 
			daughters[i].BindIt(false);
			mpool.PutMeson(daughters[i]);
		}
		return true;
	}
	std::cout << "2 WARNING! Something wrong with the decay! Less than 2 particles in the final state vector." << std::endl;
	daughters.clear();
	return false;
}

