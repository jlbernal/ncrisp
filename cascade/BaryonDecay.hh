/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef BaryonBaryon_pHH
#define BaryonBaryon_pHH

#include <vector>
#include "AbstractCascadeProcess.hh"
#include "resonance_mass.hh"
#include "time_surface.hh"
#include "decay.hh"
#include "relativisticKinematic.hh"

///
//_________________________________________________________________________________________________										

class BaryonDecay: public AbstractCascadeProcess{

private:
  
	std::vector<double> bd_time;

protected:


public:
  
	///
	//_________________________________________________________________________________________________										

	BaryonDecay(NucleusDynamics& nuc);

	///
	//_________________________________________________________________________________________________										

	virtual ~BaryonDecay();

	///
	//_________________________________________________________________________________________________										

	Double_t FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin);
  
	///
	//_________________________________________________________________________________________________										

	bool Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add);  

	///
	//_________________________________________________________________________________________________										

	// Should not implement.
	void Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
  
	///
	//_________________________________________________________________________________________________										

	void Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
  
	///
	//_________________________________________________________________________________________________										

	void FindResonanceForDecay(NucleusDynamics& nuc);

	///
	//_________________________________________________________________________________________________										

	static bool DoDistMomentum( int idx, NucleusDynamics& nuc, std::vector<ParticleDynamics>& daughters, Double_t current_time);
	
	//_________________________________________________________________________________________________	
	
	void ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BaryonDecay, 0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
