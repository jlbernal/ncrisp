/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __TimeOrdering_pHH
#define __TimeOrdering_pHH

#include "TObject.h"
#include "NucleusDynamics.hh"
#include "BaryonBaryon.hh"
#include "BaryonMeson.hh"
#include "BaryonSurface.hh"
#include "BaryonDecay.hh"
#include "MesonDecay.hh"
#include "MesonSurface.hh"
#include "SelectedProcess.hh"
#include "LeptonSurface.hh"

/// TimeOrdering class 
/// The intranuclear cascade time controler. 
//_________________________________________________________________________________________________										
 
class TimeOrdering: public TObject{

private:    
  
	double totalTime;     
  
	BaryonBaryon  *bbProcess;
	BaryonSurface *bsProcess;
	BaryonDecay   *bdProcess;
	BaryonMeson   *bmProcess;
	MesonSurface  *msProcess;
	MesonDecay    *mdProcess;
	LeptonSurface *lsProcess;
	
  
protected:
  		

	void Reset();
  
public:  


	TimeOrdering(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool);  
    
	virtual ~TimeOrdering();

	SelectedProcess* GetFastestProcess();
	
	void UpdateAll(SelectedProcess& p,NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool,  std::vector<ParticleDynamics>& vec_add);

	void NewMesonProcess( Int_t key, Int_t i, NucleusDynamics& nuc, MesonsPool& mpool, Double_t t);
	
	void NewLeptonProcess(Int_t key, Int_t i, NucleusDynamics& nuc, LeptonsPool& lpool, Double_t t );


	inline const Double_t TotalTime() const { return totalTime; }
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(TimeOrdering,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
