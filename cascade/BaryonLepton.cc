/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BaryonLepton.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
// ClassImp(BaryonLepton);
#endif // CRISP_SKIP_ROOTDICT

BaryonLepton::BaryonLepton(NucleusDynamics& nuc):AbstractCascadeProcess(), bl(){
	blChannels = new TObjArray();
	BaryonLeptonIntialize(nuc);
}
//_________________________________________________________________________________________________

BaryonLepton::~BaryonLepton(){
  
	bl.clear();
	blChannels->Delete();
	delete blChannels;
}
//_________________________________________________________________________________________________

void BaryonLepton::BaryonLeptonIntialize(NucleusDynamics& nuc){
  
#ifndef __CINT__
		blChannels->Add(new LeptonNucleonChannel("CCqe_n", nun_mump_cross_section, 0, nun_mump_ccqe_action));  
		// Elastic CrossSection for rho
/*		blChannels->Add( new MesonNucleonChannel( "rho elastic"  , rho_elastic_cross_section  , 0, rhoN_elastic_action));
		blChannels->Add( new MesonNucleonChannel( "kaon elastic" , kaon_elastic_cross_section , 0, meson_elastic_scattering_action));
		blChannels->Add( new MesonNucleonChannel( "omega elastic", omega_elastic_cross_section, 0, omegaN_elastic_action));
		blChannels->Add( new MesonNucleonChannel( "phi elastic"  , phi_elastic_cross_section  , 0, phiN_elastic_action));
		blChannels->Add( new MesonNucleonChannel( "JPsi elastic"  , JPsi_elastic_cross_sectionVDM  , 0, JPsiN_elastic_action));
		blChannels->Add( new MesonNucleonChannel( "PionN OmegaN"  , PionN_OmegaN_cross_section  , 0, PionN_OmegaN_action));
		blChannels->Add( new MesonNucleonChannel( "OmegaN PionN"  , OmegaN_PionN_cross_section  , 0, OmegaN_PionN_action));
		blChannels->Add( new MesonNucleonChannel( "OmegaN RhoN"  , OmegaN_RhoN_cross_section     , 0, OmegaN_RhoN_action));
		blChannels->Add( new MesonNucleonChannel( "RhoN OmegaN"  , RhoN_OmegaN_cross_section     , 0, RhoN_OmegaN_action));
		blChannels->Add( new MesonNucleonChannel( "OmegaN 2PionN"  , OmegaN_2PionN_cross_section , 0, OmegaN_2PionN_action));
		blChannels->Add( new MesonNucleonChannel( "PionN PhiN"  , PionN_PhiN_cross_section  , 0, PionN_OmegaN_action));
		blChannels->Add( new MesonNucleonChannel( "PionN RhoN"  , PionN_PhiN_cross_section  , 0, PionN_OmegaN_action));
		blChannels->Add( new MesonNucleonChannel( "PhiN PionN"  , PhiN_PionN_cross_section  , 0, PhiN_PionN_action));
		blChannels->Add( new MesonNucleonChannel( "RhoN PionN"  , RhoN_PionN_cross_section  , 0, RhoN_PionN_action));
		blChannels->Add( new MesonNucleonChannel( "RhoN mPionN"  , RhoN_mPionN_cross_section  , 0, RhoN_mPionN_action));
		
		blChannels->Add( new MesonNucleonChannel( "NJPsi LambdaD"  , NJPsi_ND_cross_section  , 0, NJPsi_ND_action));
		blChannels->Add( new MesonNucleonChannel( "NJPsi LambdaDast"  , NJPsi_NDast_cross_section  , 0, NJPsi_NDast_action));
		blChannels->Add( new MesonNucleonChannel( "NJPsi NDDbar"  , NJPsi_NDDbar_cross_section  , 0, NJPsi_NDDbar_action));
*/

#else

		blChannels->Add(new LeptonNucleonChannel("givename", nun_mump_cross_section, 0, nun_mump_ccqe_action));  
		

#endif

//	((MesonNucleonChannel*)(*blChannels)[0])->CrossSection().SetLowerEnergyCut(pion_abs_lc);
}
//_________________________________________________________________________________________________										
Double_t BaryonLepton::TotalCrossSection(Int_t idx1, Int_t key, NucleusDynamics& nuc, LeptonsPool& lpool){
	Double_t cs = 0.;
	for ( Int_t i = 0; i < blChannels->GetEntries(); i++ ) {
		double cs_temp = ((LeptonNucleonChannel*)(*blChannels)[i])->CrossSection().Calculate(&lpool[key], &nuc[idx1]);
		if (cs_temp > 0) cs = cs + cs_temp; 
	}
	return cs;
}
//_________________________________________________________________________________________________										
 Double_t BaryonLepton::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){
	if ( bl.empty() ) {
		idx1 = -1;
		idx2 = -1;
		return infty;
	}
	double t = tmin;
	int key = -1, idx = -1;
	for ( BLTimeMap::iterator it = bl.begin(); it != bl.end(); it++ ) { 
		for ( unsigned int i = 0; i < it->second.size(); i++ ) {
			if ( (it->second)[i] < t ){
				t = (it->second)[i];
				key =it->first;
				idx = i;
			}
		}
	}
	idx1 = idx;
	idx2 = key;
	return t;
}
//_________________________________________________________________________________________________										
bool BaryonLepton::Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add){
	//std::cout<<"BaryonMeson::Execute \n";
	int idx1 = i, key = j;
	if ( !nuc[idx1].IsBind() || !lpool[key].IsBind() ) {
		(bl[key])[idx1] = infty;
		return false;
	}
	Double_t tcs = TotalCrossSection( idx1, key, nuc, lpool );
	if ( fabs(tcs) < 1.E-7 ){
		(bl[key])[idx1] = infty;
		return false;
  	}
	Double_t sum = 0.;
	Double_t x = gRandom->Uniform();
	for ( Int_t m = 0; m < blChannels->GetEntries(); m++ ) {
		Double_t CrossSec = ((LeptonNucleonChannel*)(*blChannels)[m])->CrossSection().Value();
//		std::cout << "barion[" << idx1 <<"] and meson [" << key << "]" << ", CrossSection = " << CrossSec << std::endl;
		sum += CrossSec/ tcs;
		if ( x <= sum ) { 		//					     
			bool action = ((LeptonNucleonChannel*)(*blChannels)[m])->DoAction(&lpool, key, idx1, &nuc, (bl[key])[idx1], &mpool, &vec_add);
			if ( !action ) {
//				std::cout << "bl - blocked" << std::endl;	  
				(bl[key])[idx1] = infty;
				return false;
			} 
			else{
				return true;	
			}
		}    
	}
	(bl[key])[idx1] = infty;
	return false;
}
//_________________________________________________________________________________________________										
void BaryonLepton::Update( Int_t idx1, Int_t, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){  
	if ( lpool.size() != 0 ) {
		for ( BLTimeMap::iterator it = bl.begin();  it != bl.end(); it++ ) {   
			int key = it->first;
			if ( lpool.find(key) ) {      	
				if ( ! lpool[key].IsBind() ) {	
					bl.erase(key);	
				} 	
				else {
					if ( nuc[idx1].IsBind() ) {
						//double tcs = calculate_TotalCrossSection( key, mpool, idx, nuc, p);
						double tcs = TotalCrossSection(idx1, key, nuc, lpool);
						double ti = time_collision( lpool[key], nuc[idx1], tcs );
						(it->second)[idx1] = ti + t;
					} 
					else { 
						//std::cout << "turn off bl interaction with nuc[" << idx << "]" << std::endl;
						(it->second)[idx1] = infty;
					}
				}
			}
			else 
				bl.erase(key);   // consistence check ...       
		} // for (...) closing brace
	}   
}
//_________________________________________________________________________________________________										
void BaryonLepton::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){
	Int_t key = k;
	if ( !lpool.find(key) ){
		bl.erase(key);
	}else{
		ParticleDynamics &lepton = lpool[key];        
		if ( !lepton.IsBind() ) { 
			bl.erase(key);
			return;
		}
		for ( int i = 0; i < nuc.GetA(); i++ ) {      
			if ( !nuc[i].IsBind() ){ 
				(bl[key])[i] = infty;    
			}
			else {	
				double tcs = TotalCrossSection(i, key, nuc, lpool );
				double ti = time_collision( nuc[i], lepton, tcs );
				(bl[key])[i] = t + ti;
			}
		}
	}
}
//_________________________________________________________________________________________________										
void BaryonLepton::AddBaryonLeptonProcess(int key, int j, LeptonsPool& lpool, NucleusDynamics& nuc, Double_t t){
	if ( !lpool.find(key) ) 
		return;  
//	Int_t pdg_id = mpool[key].PdgId();
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() ) {
			Double_t tcs = TotalCrossSection( i, key, nuc, lpool);
			Double_t ti = time_collision(nuc[i], lpool[key], tcs );
			if (j == -1) (bl[key]).push_back(t + ti);
			else {
				if (i == j) (bl[key]).push_back(infty); //so meson can not interact with the same nucleon again at least in the next process
				else (bl[key]).push_back(t + ti);
				// std::cout << t + ti << std::endl;
			}
		}
		else 
			(bl[key]).push_back(infty);
	}
}


void BaryonLepton::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

}