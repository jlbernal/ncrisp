/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BaryonMeson.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
// ClassImp(BaryonMeson);
#endif // CRISP_SKIP_ROOTDICT

BaryonMeson::BaryonMeson(NucleusDynamics& nuc):AbstractCascadeProcess(), bm(){
	bmChannels = new TObjArray();
	BaryonMesonIntialize(nuc);
}
//_________________________________________________________________________________________________

BaryonMeson::~BaryonMeson(){
  
	bm.clear();
	bmChannels->Delete();
	delete bmChannels;
}
//_________________________________________________________________________________________________

void BaryonMeson::BaryonMesonIntialize(NucleusDynamics& nuc){
  
#ifndef __CINT__
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - Delta1232 formation", piN_Delta1232, 0, pion_absorption_Delta1232 ));
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - Delta1700 formation", piN_Delta1700, 0, pion_absorption_Delta1700 ));  
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - Delta1950 formation", piN_Delta1950, 0, pion_absorption_Delta1950 ));  
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - N1440 formation", piN_N1440, 0, pion_absorption_N1440 ));  
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - N1520 formation", piN_N1520, 0, pion_absorption_N1520 ));  
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - N1535 formation", piN_N1535, 0, pion_absorption_N1535 ));  
		bmChannels->Add( new MesonNucleonChannel( "pion absortion - N1680 formation", piN_N1680, 0, pion_absorption_N1680 ));

		bmChannels->Add( new MesonNucleonChannel( "rho elastic"  , rho_elastic_cross_section  , 0, rhoN_elastic_action));
		bmChannels->Add( new MesonNucleonChannel( "kaon elastic" , kaon_elastic_cross_section , 0, meson_elastic_scattering_action));
		bmChannels->Add( new MesonNucleonChannel( "omega elastic", omega_elastic_cross_section, 0, omegaN_elastic_action));
		bmChannels->Add( new MesonNucleonChannel( "phi elastic"  , phi_elastic_cross_section  , 0, phiN_elastic_action));
		bmChannels->Add( new MesonNucleonChannel( "JPsi elastic"  , JPsi_elastic_cross_sectionVDM  , 0, JPsiN_elastic_action));
		bmChannels->Add( new MesonNucleonChannel( "PionN OmegaN"  , PionN_OmegaN_cross_section  , 0, PionN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel( "OmegaN PionN"  , OmegaN_PionN_cross_section  , 0, OmegaN_PionN_action));
		bmChannels->Add( new MesonNucleonChannel( "OmegaN RhoN"  , OmegaN_RhoN_cross_section     , 0, OmegaN_RhoN_action));
		bmChannels->Add( new MesonNucleonChannel( "RhoN OmegaN"  , RhoN_OmegaN_cross_section     , 0, RhoN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel( "OmegaN 2PionN"  , OmegaN_2PionN_cross_section , 0, OmegaN_2PionN_action));
		bmChannels->Add( new MesonNucleonChannel( "PionN PhiN"  , PionN_PhiN_cross_section  , 0, PionN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel( "PionN RhoN"  , PionN_PhiN_cross_section  , 0, PionN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel( "PhiN PionN"  , PhiN_PionN_cross_section  , 0, PhiN_PionN_action));
		bmChannels->Add( new MesonNucleonChannel( "RhoN PionN"  , RhoN_PionN_cross_section  , 0, RhoN_PionN_action));
		bmChannels->Add( new MesonNucleonChannel( "RhoN mPionN"  , RhoN_mPionN_cross_section  , 0, RhoN_mPionN_action));
		
		bmChannels->Add( new MesonNucleonChannel( "NJPsi LambdaD"  , NJPsi_ND_cross_section  , 0, NJPsi_ND_action));
		bmChannels->Add( new MesonNucleonChannel( "NJPsi LambdaDast"  , NJPsi_NDast_cross_section  , 0, NJPsi_NDast_action));
		bmChannels->Add( new MesonNucleonChannel( "NJPsi NDDbar"  , NJPsi_NDDbar_cross_section  , 0, NJPsi_NDDbar_action));


#else
		CrossSectionChannel pion_Delta1232("pion absortion - Delta1232 formation", piN_Delta1232);
		CrossSectionChannel pion_Delta1700("pion absortion - Delta1700 formation", piN_Delta1700);
		CrossSectionChannel pion_Delta1950("pion absortion - Delta1950 formation", piN_Delta1950);
		CrossSectionChannel pion_N1440("pion absortion - N1440 formation", piN_N1440);
		CrossSectionChannel pion_N1520("pion absortion - N1520 formation", piN_N1520);
		CrossSectionChannel pion_N1535("pion absortion - N1535 formation", piN_N1535);
		CrossSectionChannel pion_N1680("pion absortion - N1680 formation", piN_N1680);
		
		CrossSectionChannel rho_el(  "rho_elastic", rho_elastic_cross_section);  
		CrossSectionChannel kaon_el( "kaon_elastic", kaon_elastic_cross_section);  
		CrossSectionChannel omega_el("omega_elastic", omega_elastic_cross_section);  
		CrossSectionChannel phi_el(  "phi_elastic", phi_elastic_cross_section);  
		CrossSectionChannel JPsi_el(  "JPsi_elastic", JPsi_elastic_cross_sectionVDM);  

		CrossSectionChannel PionN_OmegaN ("PionN OmegaN", PionN_OmegaN_cross_section );
		CrossSectionChannel OmegaN_PionN ("OmegaN PionN", OmegaN_PionN_cross_section );
		CrossSectionChannel OmegaN_RhoN  ("OmegaN RhoN",  OmegaN_RhoN_cross_section   );
		CrossSectionChannel RhoN_OmegaN  ("RhoN OmegaN",  RhoN_OmegaN_cross_section   );
		CrossSectionChannel OmegaN_2Pion ("OmegaN 2PionN",OmegaN_2PionN_cross_sectionN);
		CrossSectionChannel PionN_PhiN   ("PionN PhiN",   PionN_PhiN_cross_section    );
		CrossSectionChannel PionN_RhoN   ("PionN RhoN",   PionN_PhiN_cross_section    );
		CrossSectionChannel PhiN_PionN   ("PhiN PionN",   PhiN_PionN_cross_section    );
 		CrossSectionChannel RhoN_PionN   ("RhoN PionN",   RhoN_PionN_cross_section    );
		CrossSectionChannel RhoN_mPionN   ("RhoN mPionN",   RhoN_mPionN_cross_section    );

 		CrossSectionChannel NJPsi_LambdaD     ("NJPsi LambdaD"  , NJPsi_ND_cross_section);
 		CrossSectionChannel NJPsi_LambdaDast  ("NJPsi LambdaDast"  , NJPsi_NDast_cross_section);
 		CrossSectionChannel NJPsi_NDDbar      ("NJPsi NDDbar"  , NJPsi_NDDbar_cross_section);

  
		bmChannels->Add( new MesonNucleonChannel( pion_Delta1232, pion_absorption_Delta1232 ));
		bmChannels->Add( new MesonNucleonChannel( pion_Delta1700, pion_absorption_Delta1700 ));
		bmChannels->Add( new MesonNucleonChannel( pion_Delta1950, pion_absorption_Delta1950 ));
		bmChannels->Add( new MesonNucleonChannel( pion_N1440, pion_absorption_N1440 ));
		bmChannels->Add( new MesonNucleonChannel( pion_N1520, pion_absorption_N1520 ));
		bmChannels->Add( new MesonNucleonChannel( pion_N1535, pion_absorption_N1535 ));
		bmChannels->Add( new MesonNucleonChannel( pion_N1680, pion_absorption_N1680 ));
		
		bmChannels->Add( new MesonNucleonChannel( rho_el, rhoN_elastic_action));   
		bmChannels->Add( new MesonNucleonChannel( kaon_el, meson_elastic_scattering_action));
		bmChannels->Add( new MesonNucleonChannel( omega_el, omegaN_elastic_action));  
		bmChannels->Add( new MesonNucleonChannel( phi_el, phiN_elastic_action));  
		bmChannels->Add( new MesonNucleonChannel( JPsi_el, JPsiN_elastic_action));  

		bmChannels->Add( new MesonNucleonChannel(PionN_OmegaN, PionN_OmegaN_action)); 
		bmChannels->Add( new MesonNucleonChannel(OmegaN_PionN, OmegaN_PionN_action));  
		bmChannels->Add( new MesonNucleonChannel(OmegaN_RhoN , OmegaN_RhoN_action));  
		bmChannels->Add( new MesonNucleonChannel(RhoN_OmegaN , RhoN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel(OmegaN_2Pion, OmegaN_2PionN_action));
		bmChannels->Add( new MesonNucleonChannel(PionN_PhiN  , PionN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel(PionN_RhoN  , PionN_OmegaN_action));
		bmChannels->Add( new MesonNucleonChannel(PhiN_PionN  , PhiN_PionN_action));
		bmChannels->Add( new MesonNucleonChannel(RhoN_PionN  , RhoN_PionN_action));
		bmChannels->Add( new MesonNucleonChannel(RhoN_mPionN  , RhoN_mPionN_action));

		bmChannels->Add( new MesonNucleonChannel(NJPsi_LambdaD  , NJPsi_ND_action));
		bmChannels->Add( new MesonNucleonChannel(NJPsi_LambdaDast  , NJPsi_NDast_action));
		bmChannels->Add( new MesonNucleonChannel(NJPsi_NDD  , NJPsi_NDDbar_action));

#endif

	// p33_threshold leva em conta o tamanho da celula de momento de 
	// Fermi. A energia minima necessaria para a formacao de uma 
	// ressonancia Delta(P33) eh : 
	//  \[
	//  E_{min} = M_n + M_{\pi} + \Delta_{Fermi} 
	//  \]
	// onde $M_n$ eh a massa reduzida do nucleon, $M_{\pi} a massa do pion
	// e $\Delta_{Fermi} eh a incerteza na energia cinetica devido ao bloqueio
	// de Pauli.

	const double cellSize = nuc.NeutronFermiLevels().GetCellSize();  
	const double pion_abs_lc = CPT::p_mass * CPT::effn + cellSize + 160.;
	((MesonNucleonChannel*)(*bmChannels)[0])->CrossSection().SetLowerEnergyCut(pion_abs_lc);
}
//_________________________________________________________________________________________________										
Double_t BaryonMeson::TotalCrossSection(Int_t idx1, Int_t key, NucleusDynamics& nuc, MesonsPool& mpool){
	Double_t cs = 0.;
	for ( Int_t i = 0; i < bmChannels->GetEntries(); i++ ) {
		double cs_temp = ((MesonNucleonChannel*)(*bmChannels)[i])->CrossSection().Calculate(&mpool[key], &nuc[idx1]);
		if (cs_temp > 0) cs = cs + cs_temp; 
	}
	return cs;
}
//_________________________________________________________________________________________________										
 Double_t BaryonMeson::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){
	if ( bm.empty() ) {
		idx1 = -1;
		idx2 = -1;
		return infty;
	}
	double t = tmin;
	int key = -1, idx = -1;
	for ( BMTimeMap::iterator it = bm.begin(); it != bm.end(); it++ ) { 
		for ( unsigned int i = 0; i < it->second.size(); i++ ) {
			if ( (it->second)[i] < t ){
				t = (it->second)[i];
				key =it->first;
				idx = i;
			}
		}
	}
	idx1 = idx;
	idx2 = key;
	return t;
}
//_________________________________________________________________________________________________										
bool BaryonMeson::Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add){
	//std::cout<<"BaryonMeson::Execute \n";
	int idx1 = i, key = j;
	if ( !nuc[idx1].IsBind() || !mpool[key].IsBind() ) {
		(bm[key])[idx1] = infty;
		return false;
	}
	Double_t tcs = TotalCrossSection( idx1, key, nuc, mpool );
	if ( fabs(tcs) < 1.E-7 ){
		(bm[key])[idx1] = infty;
		return false;
  	}
	Double_t sum = 0.;
	Double_t x = gRandom->Uniform();
	for ( Int_t m = 0; m < bmChannels->GetEntries(); m++ ) {
		Double_t CrossSec = ((MesonNucleonChannel*)(*bmChannels)[m])->CrossSection().Value();
//		std::cout << "barion[" << idx1 <<"] and meson [" << key << "]" << ", CrossSection = " << CrossSec << std::endl;
		sum += CrossSec/ tcs;
		if ( x <= sum ) { 
			bool action = ((MesonNucleonChannel*)(*bmChannels)[m])->DoAction(key, idx1, &nuc, (bm[key])[idx1], &mpool, &vec_add);
			if ( !action ) {
//				std::cout << "bm - blocked" << std::endl;	  
				(bm[key])[idx1] = infty;
				return false;
			} 
			else{
				return true;	
			}
		}    
	}
	(bm[key])[idx1] = infty;
	return false;
}
//_________________________________________________________________________________________________										
void BaryonMeson::Update( Int_t idx1, Int_t, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){  
	if ( mpool.size() != 0 ) {
		for ( BMTimeMap::iterator it = bm.begin();  it != bm.end(); it++ ) {   
			int key = it->first;
			if ( mpool.find(key) ) {      	
				if ( ! mpool[key].IsBind() ) {	
					bm.erase(key);	
				} 	
				else {
					if ( nuc[idx1].IsBind() ) {
						//double tcs = calculate_TotalCrossSection( key, mpool, idx, nuc, p);
						double tcs = TotalCrossSection(idx1, key, nuc, mpool);
						double ti = time_collision( mpool[key], nuc[idx1], tcs );
						(it->second)[idx1] = ti + t;
					} 
					else { 
						//std::cout << "turn off bm interaction with nuc[" << idx << "]" << std::endl;
						(it->second)[idx1] = infty;
					}
				}
			}
			else 
				bm.erase(key);   // consistence check ...       
		} // for (...) closing brace
	}   
}
//_________________________________________________________________________________________________										
void BaryonMeson::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){
	Int_t key = k;
	if ( !mpool.find(key) ){
		bm.erase(key);
	}else{
		ParticleDynamics &meson = mpool[key];        
		if ( !meson.IsBind() ) { 
			bm.erase(key);
			return;
		}
		for ( int i = 0; i < nuc.GetA(); i++ ) {      
			if ( !nuc[i].IsBind() ){ 
				(bm[key])[i] = infty;    
			}
			else {	
				double tcs = TotalCrossSection(i, key, nuc, mpool );
				double ti = time_collision( nuc[i], meson, tcs );
				(bm[key])[i] = t + ti;
			}
		}
	}
}
//_________________________________________________________________________________________________										
void BaryonMeson::AddBaryonMesonProcess(int key, int j, MesonsPool& mpool, NucleusDynamics& nuc, Double_t t){
	if ( !mpool.find(key) ) 
		return;  
//	Int_t pdg_id = mpool[key].PdgId();
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() ) {
			Double_t tcs = TotalCrossSection( i, key, nuc, mpool);
			Double_t ti = time_collision(nuc[i], mpool[key], tcs );
			if (j == -1) (bm[key]).push_back(t + ti);
			else {
				if (i == j) (bm[key]).push_back(infty); //so meson can not interact with the same nucleon again at least in the next process
				else (bm[key]).push_back(t + ti);
				// std::cout << t + ti << std::endl;
			}
		}
		else 
			(bm[key]).push_back(infty);
	}
}

void BaryonMeson::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

}
