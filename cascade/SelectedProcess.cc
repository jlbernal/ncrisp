/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "SelectedProcess.hh"

bool SelectedProcess::ExecuteProcess(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& new_particles){
	switch(_type) {
		case barion_barion:
			p1 = nuc[_idx1];
			p2 = nuc[_idx2];
		break;
		case barion_meson:
			p1 = nuc[_idx1];
			p2 = mpool[_idx2];
		break;
		case barion_surface:
			p1 = nuc[_idx1];
		break;
		case meson_surface:
			p1 = mpool[_idx1];
		break;
		case barion_decay:
			p1 = nuc[_idx1];
		break;
		case meson_decay:
			p1 = mpool[_idx1];
		break; 
		case lepton_surface:
			p1 = lpool[_idx1];
		break;
	}
	// std::cout << this->GetName() << Form(": [%d, %d]", _idx1, _idx2) << Form(" t = %f", _time) << std::endl;
	return _process->Execute(_idx1, _idx2, nuc, mpool, lpool, new_particles);
}

SelectedProcess::SelectedProcess( Int_t type, char* name, Int_t idx1, Int_t idx2, AbstractCascadeProcess* p, Double_t t): TNamed(name, name), p1(), p2(){
	_type = type;
	_idx1 = idx1;
	_idx2 = idx2;
	_time = t;
	_process = p;
}

SelectedProcess::SelectedProcess(SelectedProcess& u):TNamed(u.GetName(), u.GetName()), p1(u.p1), p2(u.p2){
	_type = u._type;
	_idx1 = u._idx1;
	_idx2 = u._idx2;
	_time = u._time;
	_process = u._process;
}

SelectedProcess::~SelectedProcess(){}

Double_t SelectedProcess::GetEnergyOut(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool ){
	if ( _type == barion_surface ) { 
		ParticleDynamics &p = nuc[_idx1];
		if ( p.IsBind() ) 
			return 0.;
		else {
			/* 
			 * NOTE: GetMass() here returns rest mass 
			 * (change from effective to the rest mass has been already made in BaryonSurface.cc after unbinding)
			 */
			double well = nuc.NuclearPotentialWell( p.PdgId() ); 
			double E_out = p.Momentum().E() - p.GetMass()*CPT::effn - well; 
int aa=0,zz=0;			
for(int i=0;i<nuc.GetA();i++){
  if(nuc.GetNucleon(i).IsBind())
    aa++;
}						
			if(E_out > 0.){
				//cout << "particle " << p.PdgId() << ", index: " << _idx1 << " leaving and taking " << E_out << " MeV, kinetic energy inside nucleus: " << p.Momentum().E() - p.GetMass()*CPT::effn << "  Nuclear Well: " << well << "  mass diff: " << p.GetMass() - p.GetMass()*CPT::effn << endl;
				return E_out;
			}
			else{
				cout << "ERROR!!: particle " << p.PdgId() << " has negative kinetic energy outside nucleus. It should not be leaving!" << endl;
				return E_out;
			}
		}
	}
	if ( _type == meson_surface ) { 
		if (  mpool.find(_idx1) ) { 
			ParticleDynamics &m = mpool[_idx1];
			double E_out_meson = ( m.IsBind() ) ? 0. : m.Momentum().E();
			//cout << "particle " << m.PdgId() << " leaving and taking " << E_out_meson << " MeV" << endl;
			return E_out_meson;
		}
	} 
	
	
	
	if ( _type == lepton_surface ) { 
		if (  lpool.find(_idx1) ) { 
			ParticleDynamics &m = lpool[_idx1];
			double E_out_meson = ( m.IsBind() ) ? 0. : m.Momentum().E();
			//cout << "particle " << m.PdgId() << " leaving and taking " << E_out_meson << " MeV" << endl;
			return E_out_meson;
		}
	}
	return 0.; 
}

void SelectedProcess::ProcessStatus( std::ostream& ofs, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, std::vector<ParticleDynamics>& vec) {  
	switch(_type) {
		case barion_barion:
			ofs << "---------------------------------------------------" << std::endl;  
			ofs << this->GetName() << Form(":[%d, %d]: t = %f",_idx1, _idx2, _time) << std::endl;
			ofs << "- start -" << std::endl;
			ofs << p1.ToString();
			ofs << p2.ToString();
			ofs << "- end -" << std::endl;
			ofs << nuc[_idx1].ToString();
			ofs << nuc[_idx2].ToString();
			ofs << "---------------------------------------------------" << std::endl;  
		break;
		case barion_meson:
		{
			ofs << "---------------------------------------------------" << std::endl;  
			ofs << this->GetName()<< Form(":[%d, %d]: t = %f",_idx1, _idx2, _time) << std::endl;
			ofs << "- start -" << std::endl;
			ofs << p1.ToString();
			ofs << p2.ToString();
			ofs << "- end -" << std::endl;
			ofs << Form ("decay at t = %f", nuc[_idx1].LifeTime()) << std::endl;	     
			ofs << nuc[_idx1].ToString();      
			if ( mpool.find(_idx2) ) {
				ofs << mpool[_idx2].ToString();
			}
			ofs << "---------------------------------------------------" << std::endl;  
		}
		break;
		case barion_surface:    
			if ( !nuc[_idx1].IsBind() ) {           
			ofs << "---------------------------------------------------" << std::endl;  
				ofs << this->GetName() << Form(":[%d]: t = %f", _idx1, _time) << std::endl;
				ofs << "t = " << _time << std::endl;
				ofs << nuc[_idx1].Name() << " go out!" << " T = " << nuc[_idx1].TKinetic() << std::endl;
				ofs << nuc[_idx1].ToString();      
			ofs << "---------------------------------------------------" << std::endl;  
			}
		break;
		case meson_surface:    
			if ( !mpool[_idx1].IsBind() ) {
			ofs << "---------------------------------------------------" << std::endl;  
				ofs << this->GetName() << Form(":[%d]: t = %f", _idx1, _time) << std::endl;   
				double v0 = nuc.NuclearPotential( p1.PdgId() );
				ofs << " t = " << _time << std::endl;
				ofs << p1.Name() << " go out!" << " E_out = " << p1.Momentum().E() - v0 << std::endl;
				ofs << nuc[_idx1].ToString();      
			ofs << "---------------------------------------------------" << std::endl;  
			}	
		break;
		case barion_decay: 
			ofs << "---------------------------------------------------" << std::endl;  
			ofs << this->GetName() << Form(":[%d]: t = %f", _idx1, _time) << std::endl;   
			ofs << "- start -" << std::endl;
			ofs << p1.ToString();
			ofs << "- end -" << std::endl;
			ofs << nuc[_idx1].ToString() << vec[0].ToString() << std::endl;    
			ofs << "---------------------------------------------------" << std::endl;  
		break;
		case meson_decay:
			ofs << "---------------------------------------------------" << std::endl;  
			ofs << this->GetName() << Form(":[%d]: t = %f", _idx1, _time) << std::endl;   
			ofs << p1.ToString();
			ofs << "---------------------------------------------------" << std::endl;  
		break;
		
		case lepton_surface:    
		break;
	}   
}

TTree* SelectedProcess::GetProcessTree(NucleusDynamics& nuc, MesonsPool& mpool, std::vector<ParticleDynamics>& vec){
	TTree *T = new TTree("Process","Process Tree");
	TTree *M = new TTree("Init Part"," Initial Particles Tree");
	TTree *MF = new TTree("Final Part"," Final Particles Tree");
	typedef struct {
			const char* Name;
			Int_t i1,i2;
			Double_t t;
	} ProDcrp;
	TString Comm;	
	static ProDcrp point;
	T->Branch("point",&point,"Name/C:i1/I:i2:t/D");
	T->Branch("Particles","TTree",&M);
	T->Branch("Comment","TString",&Comm);

	TVector3 POS;
	TLorentzVector P;
	typedef struct {
		Int_t pid; 
		bool Bnd;
	} PrtD;
	PrtD _PrtD;
	M->Branch("PID",&_PrtD,"PID/I:Bind/O");
	M->Branch("Pos","TVector3",&POS);
	M->Branch("P","TLorentzVector",&P);

	M->Branch("PID",&_PrtD,"PID/I:Bind/O");
	MF->Branch("Pos","TVector3",&POS);
	MF->Branch("P","TLorentzVector",&P);

	switch(_type) {
		case barion_barion:
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = _idx2;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			_PrtD.pid =  p2.PdgId(); _PrtD.Bnd = p2.IsBind(); POS = p2.Position(); P = p2.Momentum();
			M->Fill();
			_PrtD.pid =  nuc[_idx1].PdgId(); _PrtD.Bnd = nuc[_idx1].IsBind(); 
			POS = nuc[_idx1].Position(); P = nuc[_idx1].Momentum();
			MF->Fill();
			_PrtD.pid =  nuc[_idx2].PdgId(); _PrtD.Bnd = nuc[_idx2].IsBind();
			POS = nuc[_idx2].Position(); P = nuc[_idx2].Momentum();
			MF->Fill();
			Comm = " ";
		break;
		case barion_meson:
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = _idx2;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			_PrtD.pid =  p2.PdgId(); _PrtD.Bnd = p2.IsBind(); POS = p2.Position(); P = p2.Momentum();
			M->Fill();
			_PrtD.pid =  nuc[_idx1].PdgId(); _PrtD.Bnd = nuc[_idx1].IsBind(); 
			POS = nuc[_idx1].Position(); P = nuc[_idx1].Momentum();
			MF->Fill();
			if ( mpool.find(_idx2) ) {
				_PrtD.pid =  mpool[_idx2].PdgId(); _PrtD.Bnd = mpool[_idx2].IsBind();
				POS = mpool[_idx2].Position(); P = mpool[_idx2].Momentum();
				MF->Fill();
			}
			Comm = Form ("decay at t = %f", nuc[_idx1].LifeTime());	
		break;
		case barion_surface:    
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = -1;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			_PrtD.pid =  nuc[_idx1].PdgId(); _PrtD.Bnd = nuc[_idx1].IsBind(); 
			POS = nuc[_idx1].Position(); P = nuc[_idx1].Momentum();
			MF->Fill();
			if ( !nuc[_idx1].IsBind() ) 
				Comm = "Barion " + nuc[_idx1].Name() + " go out!";
			else	Comm = "Barion " + nuc[_idx1].Name() + " can't go out!";
		break;
		case meson_surface:    
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = -1;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			_PrtD.pid =  mpool[_idx1].PdgId(); _PrtD.Bnd = mpool[_idx1].IsBind(); 
			POS = mpool[_idx1].Position(); P = mpool[_idx1].Momentum();
			MF->Fill();
			if ( !mpool[_idx1].IsBind() )
				Comm = "Meson " + mpool[_idx1].Name() + " go out!";
			else	Comm = "Meson " + mpool[_idx1].Name() + " can't go out!";
		break;
		case barion_decay: 
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = -1;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			for ( int i = 0; i < (int) vec.size(); i ++ ) {
				_PrtD.pid =  vec[i].PdgId(); _PrtD.Bnd = vec[i].IsBind(); 
				POS = vec[i].Position(); P = vec[i].Momentum();
				MF->Fill();
			}
			Comm = " ";
		break;
		case meson_decay:
			point.Name = this->GetName();
			point.i1 = _idx1;
			point.i2 = -1;
			point.t = _time;
			_PrtD.pid =  p1.PdgId(); _PrtD.Bnd = p1.IsBind(); POS = p1.Position(); P = p1.Momentum();
			M->Fill();
			Comm = " ";
		break;
		case lepton_surface:
		  break;
	}
	T->Fill();
	return T;
}

Proc_Struct SelectedProcess::GetProcessStruct(NucleusDynamics& nuc, MesonsPool& mpool,  std::vector<ParticleDynamics>& vec){
	Proc_Struct PS;
	switch(_type) {
		case barion_barion:
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = _idx2;
			PS.TIME = _time;
			PS.NPartIn = 2;
			PS.NTotal = 4 + (int) vec.size();

			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();

			PS.Proc_PID[1] = p2.PdgId();
			PS.Proc_IsB[1] = p2.IsBind();
			PS.Proc_IsF[1] = false;
			PS.Proc_X[1] = p2.Position().X();
			PS.Proc_Y[1] = p2.Position().Y();
			PS.Proc_Z[1] = p2.Position().Z();
			PS.Proc_P[1] = p2.Momentum().Vect().Mag();
			PS.Proc_PX[1] = p2.Momentum().Px();
			PS.Proc_PY[1] = p2.Momentum().Py();
			PS.Proc_PZ[1] = p2.Momentum().Pz();
			PS.Proc_E[1] = p2.Momentum().E();
			PS.Proc_K[1] = p2.TKinetic();

			PS.Proc_PID[2] = nuc[_idx1].PdgId();
			PS.Proc_IsB[2] = nuc[_idx1].IsBind();
			PS.Proc_IsF[2] = true;
			PS.Proc_X[2] = nuc[_idx1].Position().X();
			PS.Proc_Y[2] = nuc[_idx1].Position().Y();
			PS.Proc_Z[2] = nuc[_idx1].Position().Z();
			PS.Proc_P[2] = nuc[_idx1].Momentum().Vect().Mag();
			PS.Proc_PX[2] = nuc[_idx1].Momentum().Px();
			PS.Proc_PY[2] = nuc[_idx1].Momentum().Py();
			PS.Proc_PZ[2] = nuc[_idx1].Momentum().Pz();
			PS.Proc_E[2] = nuc[_idx1].Momentum().E();
			PS.Proc_K[2] = nuc[_idx1].TKinetic();

			PS.Proc_PID[3] = nuc[_idx2].PdgId();
			PS.Proc_IsB[3] = nuc[_idx2].IsBind();
			PS.Proc_IsF[3] = true;
			PS.Proc_X[3] = nuc[_idx2].Position().X();
			PS.Proc_Y[3] = nuc[_idx2].Position().Y();
			PS.Proc_Z[3] = nuc[_idx2].Position().Z();
			PS.Proc_P[3] = nuc[_idx2].Momentum().Vect().Mag();
			PS.Proc_PX[3] = nuc[_idx2].Momentum().Px();
			PS.Proc_PY[3] = nuc[_idx2].Momentum().Py();
			PS.Proc_PZ[3] = nuc[_idx2].Momentum().Pz();
			PS.Proc_E[3] = nuc[_idx2].Momentum().E();
			PS.Proc_K[3] = nuc[_idx2].TKinetic();

		break;
		case barion_meson:
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = _idx2;
			PS.TIME = _time;
			PS.NPartIn = 2;
			PS.NTotal = 2 + (int) vec.size();

			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();

			PS.Proc_PID[1] = p2.PdgId();
			PS.Proc_IsB[1] = p2.IsBind();
			PS.Proc_IsF[1] = false;
			PS.Proc_X[1] = p2.Position().X();
			PS.Proc_Y[1] = p2.Position().Y();
			PS.Proc_Z[1] = p2.Position().Z();
			PS.Proc_P[1] = p2.Momentum().Vect().Mag();
			PS.Proc_PX[1] = p2.Momentum().Px();
			PS.Proc_PY[1] = p2.Momentum().Py();
			PS.Proc_PZ[1] = p2.Momentum().Pz();
			PS.Proc_E[1] = p2.Momentum().E();
			PS.Proc_K[1] = p2.TKinetic();

			for ( int i = 0; i < (int) vec.size(); i ++ ) {
				PS.Proc_PID[i + 2] = vec[i].PdgId();
				PS.Proc_IsB[i + 2] = vec[i].IsBind();
				PS.Proc_IsF[i + 2] = true;
				PS.Proc_X[i + 2] = vec[i].Position().X();
				PS.Proc_Y[i + 2] = vec[i].Position().Y();
				PS.Proc_Z[i + 2] = vec[i].Position().Z();
				PS.Proc_P[i + 2] = vec[i].Momentum().Vect().Mag();
				PS.Proc_PX[i + 2] = vec[i].Momentum().Px();
				PS.Proc_PY[i + 2] = vec[i].Momentum().Py();
				PS.Proc_PZ[i + 2] = vec[i].Momentum().Pz();
				PS.Proc_E[i + 2] = vec[i].Momentum().E();
				PS.Proc_K[i + 2] = vec[i].TKinetic();
			}
		break;
		case barion_surface:    
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = -1;
			PS.TIME = _time;
			PS.NPartIn = 1;
			PS.NTotal = 2;

			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();

			PS.Proc_PID[1] = nuc[_idx1].PdgId();
			PS.Proc_IsB[1] = nuc[_idx1].IsBind();
			PS.Proc_IsF[1] = true;
			PS.Proc_X[1] = nuc[_idx1].Position().X();
			PS.Proc_Y[1] = nuc[_idx1].Position().Y();
			PS.Proc_Z[1] = nuc[_idx1].Position().Z();
			PS.Proc_P[1] = nuc[_idx1].Momentum().Vect().Mag();
			PS.Proc_PX[1] = nuc[_idx1].Momentum().Px();
			PS.Proc_PY[1] = nuc[_idx1].Momentum().Py();
			PS.Proc_PZ[1] = nuc[_idx1].Momentum().Pz();
			PS.Proc_E[1] = nuc[_idx1].Momentum().E();
			PS.Proc_K[1] = nuc[_idx1].TKinetic();
		break;
		case meson_surface:    
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = -1;
			PS.TIME = _time;
			PS.NPartIn = 1;
			PS.NTotal = 2;
			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();

			PS.Proc_PID[1] = mpool[_idx1].PdgId();
			PS.Proc_IsB[1] = mpool[_idx1].IsBind();
			PS.Proc_IsF[1] = true;
			PS.Proc_X[1] = mpool[_idx1].Position().X();
			PS.Proc_Y[1] = mpool[_idx1].Position().Y();
			PS.Proc_Z[1] = mpool[_idx1].Position().Z();
			PS.Proc_P[1] = mpool[_idx1].Momentum().Vect().Mag();
			PS.Proc_PX[1] = mpool[_idx1].Momentum().Px();
			PS.Proc_PY[1] = mpool[_idx1].Momentum().Py();
			PS.Proc_PZ[1] = mpool[_idx1].Momentum().Pz();
			PS.Proc_E[1] = mpool[_idx1].Momentum().E();
			PS.Proc_K[1] = mpool[_idx1].TKinetic();
		break;
		case barion_decay: 
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = -1;
			PS.TIME = _time;
			PS.NPartIn = 1;
			PS.NTotal =1 + (int)vec.size();
			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();

			for ( int i = 0; i < (int) vec.size(); i ++ ) {
				PS.Proc_PID[i+1] = vec[i].PdgId();
				PS.Proc_IsB[i+1] = vec[i].IsBind();
				PS.Proc_IsF[i+1] = true;
				PS.Proc_X[i+1] = vec[i].Position().X();
				PS.Proc_Y[i+1] = vec[i].Position().Y();
				PS.Proc_Z[i+1] = vec[i].Position().Z();
				PS.Proc_P[i+1] = vec[i].Momentum().Vect().Mag();
				PS.Proc_PX[i+1] = vec[i].Momentum().Px();
				PS.Proc_PY[i+1] = vec[i].Momentum().Py();
				PS.Proc_PZ[i+1] = vec[i].Momentum().Pz();
				PS.Proc_E[i+1] = vec[i].Momentum().E();
				PS.Proc_K[i+1] = vec[i].TKinetic();
			}
		break;
		case meson_decay: //esto hay que arreglarlo cuando los mesones decaigan realmente
			PS.ProN = this->GetType();
			PS.I1 = _idx1;
			PS.I2 = -1;
			PS.TIME = _time;
			PS.NPartIn = 1;
			PS.NTotal = 1;
			PS.Proc_PID[0] = p1.PdgId();
			PS.Proc_IsB[0] = p1.IsBind();
			PS.Proc_IsF[0] = false;
			PS.Proc_X[0] = p1.Position().X();
			PS.Proc_Y[0] = p1.Position().Y();
			PS.Proc_Z[0] = p1.Position().Z();
			PS.Proc_P[0] = p1.Momentum().Vect().Mag();
			PS.Proc_PX[0] = p1.Momentum().Px();
			PS.Proc_PY[0] = p1.Momentum().Py();
			PS.Proc_PZ[0] = p1.Momentum().Pz();
			PS.Proc_E[0] = p1.Momentum().E();
			PS.Proc_K[0] = p1.TKinetic();
		break;
		case lepton_surface:
		  break;
	}
	return PS;
}


bool SelectedProcess::ManageFinalProcessDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

	if(_type == barion_surface && CPT::IsImplRessonance( nuc[idx1].PdgId() )){
		_process->ManageFinalDetails(nuc, mpool, idx1);
		return true;
	}
	return false;
}