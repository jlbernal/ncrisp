/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __BaryonLepton_pHH
#define __BaryonLepton_pHH

#include <map>
#include "TObjArray.h"
#include "AbstractCascadeProcess.hh"
#include "LeptonNucleonChannel.hh"
#include "Lepton_n_cross_sections.hh"
#include "Lepton_n_actions.hh"
#include "time_collision.hh"
#include "DataHelper.hh"
typedef std::map<int, std::vector<double> > BLTimeMap;


class BaryonLepton: public AbstractCascadeProcess{
private:
	TObjArray* blChannels;
	BLTimeMap bl;   // Time map for baryon-meson process 
protected:
	void BaryonLeptonIntialize(NucleusDynamics& nuc);
	Double_t TotalCrossSection(Int_t idx1, Int_t key, NucleusDynamics& nuc, LeptonsPool& lpool);
public:
	BaryonLepton(NucleusDynamics& nuc);
	virtual ~BaryonLepton();
	Double_t FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin);
	bool Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add);
	void Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
	void Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
	void AddBaryonLeptonProcess(int key, int j, LeptonsPool& lpool, NucleusDynamics& nuc, Double_t t);
	void ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BaryonLepton, 0);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
