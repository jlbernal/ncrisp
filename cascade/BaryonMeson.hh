/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __BaryonMeson_pHH
#define __BaryonMeson_pHH

#include <map>
#include "TObjArray.h"
#include "AbstractCascadeProcess.hh"
#include "MesonNucleonChannel.hh"
#include "Meson_n_cross_sections.hh"
#include "Meson_n_actions.hh"
#include "kaon_n_cross_sections.hh"
#include "time_collision.hh"
#include "DataHelper.hh"
typedef std::map<int, std::vector<double> > BMTimeMap;

class BaryonMeson: public AbstractCascadeProcess{
private:
	TObjArray* bmChannels;
	BMTimeMap bm;   // Time map for baryon-meson process 
protected:
	void BaryonMesonIntialize(NucleusDynamics& nuc);
	Double_t TotalCrossSection(Int_t idx1, Int_t key, NucleusDynamics& nuc, MesonsPool& mpool);
public:
	BaryonMeson(NucleusDynamics& nuc);
	virtual ~BaryonMeson();
	Double_t FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin);
	bool Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add);
	void Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
	void Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);
	void AddBaryonMesonProcess(int key, int j, MesonsPool& mpool, NucleusDynamics& nuc, Double_t t);
	void ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BaryonMeson, 0);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
