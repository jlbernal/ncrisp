/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include  "LeptonSurface.hh"

//#if !defined(CRISP_SKIP_ROOTDICT)
//ClassImp(LeptonSurface);
//#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

LeptonSurface::LeptonSurface(LeptonsPool&):AbstractCascadeProcess(), lepton_surface(){
}

//_________________________________________________________________________________________________										

LeptonSurface::~LeptonSurface(){
	lepton_surface.clear();
}

//_________________________________________________________________________________________________										

Double_t LeptonSurface::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){

	idx2 = -1;
	if ( lepton_surface.empty() ) { 
		idx1 = -1;
		return infty;
	}    
	
	int k = -1;
	double t = tmin;
	
	for ( std::map<int,double>::iterator it = lepton_surface.begin(); 
		it != lepton_surface.end(); it++ ) {
		
		if ( it->second < t ) {
			t = it->second;
			k = it->first;
		}      
	}
  idx1 = k;
  return t;
}

//_________________________________________________________________________________________________										

bool LeptonSurface::Execute ( Int_t& i, Int_t& , NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, std::vector<ParticleDynamics>&) {

		
	int key = i;
		
	lpool[key].BindIt(false); 
	//taking away emitted particle's momentum from nucleus

	nuc.SetMomentum(nuc.Momentum() -  lpool[key].Momentum());
	//taking away emitted particle's angulars momentum from nucleus

	TVector3 lTemp = lpool[key].Position().Cross(lpool[key].Momentum().Vect());
	nuc.SetAngularMomentum(nuc.AngularMomentum() - lTemp);
	return true;  

}

//_________________________________________________________________________________________________										

void LeptonSurface::Update( Int_t, Int_t, NucleusDynamics&, MesonsPool& ,LeptonsPool& , Double_t ){

	// should not implement
}

//_________________________________________________________________________________________________										

void LeptonSurface::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, Double_t t) {

	const int key = k;
  
	if ( !lpool.find(key) ) 
		lepton_surface.erase(key); // lepton was absorbed !
	else {    
		if ( !lpool[key].IsBind() )
			lepton_surface[key] = infty;
		else { 
			double ti = time_surface(lpool[key], nuc);
			lepton_surface[key] = t + ti;
		}
	}
}

//_________________________________________________________________________________________________										

void LeptonSurface::AddLeptonSurfaceProcess( int key, LeptonsPool& lpool, NucleusDynamics& nuc, double t ){

	if ( lpool.find(key) ) {
		double ti = time_surface(lpool[key], nuc);
		lepton_surface[key] = t + ti;
	}
}

//_________________________________________________________________________________________________										

void LeptonSurface::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){
  
  
}


