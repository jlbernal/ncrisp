/* ================================================================================
 * 
 * 	Copyright 2008 2016 Jose Luis Bernal Castillo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Cascade.hh"

using namespace std;
const Double_t Cascade::delta_t_max = 100.;

Cascade::Cascade(NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, Double_t start_energy, bool PrintCasc) :TObject() {  
	timeOrdering = 0;
	cont = 0;
	countAllowed = 0;
	blocked = 0;
	cascade_max_time = 2000.;
	elapsedTime = 0.;
	ex_energy = start_energy;
	flag_started = false;
	this->Restart(nuc, mpool,lpool, start_energy); 
	proc_result = false;
	casc_TerForc = false;
	PrintHist =  PrintCasc;
#ifdef THERMA	
	pass = 0;
#endif
	if (PrintHist){
		History = new TTree("Cascade History Tree","History");
//		History->ResetBranchAddresses();
//		History->Reset();
/*		History->Branch("Nuc_A",&NUC.A,"A/I");
		History->Branch("Nuc_Z",&NUC.Z,"Z/I");
		History->Branch("Nuc_Ex",&ex_energy,"Ex/D");
		History->Branch("Blk",&blocked,"Blk/I");
		History->Branch("Cont",&cont,"Cont/I");
		History->Branch("time",&elapsedTime,"Time/D");
		History->Branch("Pro_Res",&proc_result,"Pro_Res/O");
		History->Branch("Casc_TerForc",&casc_TerForc,"Casc_TerForc/O");

		History->Branch("Plevels",NUC.PLevels,"Nuc_PLevels[30]/I");
		History->Branch("Nlevels",NUC.NLevels,"Nuc_NLevels[30]/I");
	
		History->Branch("Nuc_Inx",NUC.Nuc_IDX, "Nuc_IND[A]/I");
		History->Branch("Nuc_Pid",NUC.Nuc_PID, "Nuc_PID[A]/I");
		History->Branch("Nuc_IsBind",NUC.Nuc_IsB, "Nuc_IsB[A]/O");
		History->Branch("Nuc_X",NUC.Nuc_X, "Nuc_X[A]/D");
		History->Branch("Nuc_Y",NUC.Nuc_Y, "Nuc_Y[A]/D");
		History->Branch("Nuc_Z",NUC.Nuc_Z, "Nuc_Z[A]/D");
		History->Branch("Nuc_P",NUC.Nuc_P, "Nuc_P[A]/D");
		History->Branch("Nuc_PX",NUC.Nuc_PX, "Nuc_PX[A]/D");
		History->Branch("Nuc_PY",NUC.Nuc_PY, "Nuc_PY[A]/D");
		History->Branch("Nuc_PZ",NUC.Nuc_PZ, "Nuc_PZ[A]/D");
		History->Branch("Nuc_E",NUC.Nuc_E, "Nuc_E[A]/D");
		History->Branch("Nuc_K",NUC.Nuc_K, "Nuc_K[A]/D");
	
*/		History->Branch("Mes_NM",&MPOOL.NM,"NM/I");
		//History->Branch("Mes_Inx",MPOOL.Mes_KEY, "Mes_IND[NM]/I");
		History->Branch("Mes_Pid",MPOOL.Mes_PID, "Mes_PID[NM]/I");
		History->Branch("Mes_IsBind",MPOOL.Mes_IsB, "Mes_IsB[NM]/O");

		History->Branch("Mes_PX",MPOOL.Mes_PX, "Mes_PX[NM]/D");
		History->Branch("Mes_PY",MPOOL.Mes_PY, "Mes_PY[NM]/D");
		History->Branch("Mes_PZ",MPOOL.Mes_PZ, "Mes_PZ[NM]/D");
		History->Branch("Mes_E",MPOOL.Mes_E, "Mes_E[NM]/D");
		//History->Branch("Mes_K",MPOOL.Mes_K, "Mes_K[NM]/D");
		History->Branch("Mes_X",MPOOL.Mes_X, "Mes_X[NM]/D");
		History->Branch("Mes_Y",MPOOL.Mes_Y, "Mes_Y[NM]/D");
		History->Branch("Mes_Z",MPOOL.Mes_Z, "Mes_Z[NM]/D");
/*		History->Branch("Mes_P",MPOOL.Mes_P, "Mes_P[NM]/D");
	
		History->Branch("Pro_Num",&PRO.ProN,"NAME/C");
		History->Branch("Pro_Time",&PRO.TIME, "TIME/D");
		History->Branch("Pro_Inx1",&PRO.I1, "I1/I");
		History->Branch("Pro_Inx2",&PRO.I2, "I2/I");
		History->Branch("Pro_NParIn",&PRO.NPartIn, "NPartIn/I");
		History->Branch("Pro_NTotal",&PRO.NTotal, "NTotal/I");
		History->Branch("Pro_Pid",PRO.Proc_PID, "Proc_PID[NTotal]/I");
		History->Branch("Pro_IsFinal",PRO.Proc_IsF, "Proc_IsF[NTotal]/O");
		History->Branch("Pro_IsBind",PRO.Proc_IsB, "Proc_IsB[NTotal]/O");
		History->Branch("Pro_X",PRO.Proc_X, "Proc_X[NTotal]/D");
		History->Branch("Pro_Y",PRO.Proc_Y, "Proc_Y[NTotal]/D");
		History->Branch("Pro_Z",PRO.Proc_Z, "Proc_Z[NTotal]/D");
		History->Branch("Pro_P",PRO.Proc_P, "Proc_P[NTotal]/D");
		History->Branch("Pro_PX",PRO.Proc_PX, "Proc_PX[NTotal]/D");
		History->Branch("Pro_PY",PRO.Proc_PX, "Proc_PY[NTotal]/D");
		History->Branch("Pro_PZ",PRO.Proc_PX, "Proc_PZ[NTotal]/D");
		History->Branch("Pro_E",PRO.Proc_E, "Proc_E[NTotal]/D");
		History->Branch("Pro_K",PRO.Proc_K, "Proc_K[NTotal]/D");
*/	}	
}
//_____________________________________________________________________________________________										
//_________________________________________________________________________________________________										
Cascade::~Cascade(){
	if ( timeOrdering ) delete timeOrdering;
	if ( PrintHist ) delete History;
//	if (CPT::Instance()->Get_Data().is_open()) CPT::Instance()->Get_Data().close();

}
//_____________________________________________________________________________________________										
Double_t Cascade::TerminateForced(NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool){
	Double_t ex = 0.;
	
	for ( MesonsPool::iterator mit = mpool.begin(); mit != mpool.end(); mit++ ) {
		if ( mit->second.IsBind() ){
			std::cout << "Meson being forced out" << std::endl;
			ex += mit->second.Momentum().E();
			cout << "meson "  << mit->second.PdgId() << " " << endl;
			mit->second.BindIt(false); //All mesons are set true for binding when created
		}
	}
	
	for ( LeptonsPool::iterator mit = lpool.begin(); mit != lpool.end(); mit++ ) {
	  /*
		 * NOTE: Leptons are not bound to nucleus but flag is keep for compatibility. mejorar esta nota  
		 */ 
	  
		if ( mit->second.IsBind() ){
			ex += mit->second.Momentum().E();
				cout << "lepton " << endl;
			mit->second.BindIt(false); //All leptons are set true for binding when created
		}
	}
	
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) { 
		/*
		 * NOTE: This method being called means very likely that a Delta resonance remained in nucleus without 
		 * decaying due to Pauli blocking. Since there is no Delta nucleus in nature, this resonance is removed by force
		 * and its entire kinetic energy must be taken out
		 */ 
		if ( CPT::IsImplRessonance(nuc[i].PdgId()) && nuc[i].IsBind() ) {
			std::cout << "Resonance being forced out" << std::endl;
			nuc[i].BindIt(false);
			cout << "Deltas " << nuc[i].PdgId()  <<  endl;
			ex += nuc[i].Momentum().E() - nuc[i].GetMass();
		}
	}
	std::cout << "[warning] Termination forced. Energy taken out = " << ex << std::endl;
	return ex;
}
//_____________________________________________________________________________________________										
Double_t Cascade::Execute( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool ){
	try {    
		blocked = 0;
		t_changed = 0.;
		while ( this->NextProcess(nuc, mpool, lpool, ex_energy) ) { 
			if (PrintHist) History->Fill();
		}    
		if (PrintHist){ // Print the final input after finishing the cascade
			NUC = nuc.GetNucStruct();
			MPOOL = mpool.GetMesonStruct();
			LPOOL = lpool.GetLeptonsStruct();
			History->Fill();
		}
#ifdef THERMA
 		//std::cout << std::endl;
 		//cout << "countAllowed final: " << countAllowed << endl;
// 		nuc.ProtonFermiLevels().PrintStatus();
// 		nuc.NeutronFermiLevels().PrintStatus();
		nuc.TakeProtonLevelsSnapshot("final");
		nuc.TakeNeutronLevelsSnapshot("final");
#endif
		return ex_energy;
	} 
	catch (BasicException e) {
		throw;
	}  
}
//_________________________________________________________________________________________________										
bool Cascade::Execute_Partial_Cascade( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, int n_col){
	if (flag_started==false){
		flag_started	= true;
		blocked   		= 0;
		t_changed 		= 0.;
		exc_energy 		= start_energy;
	}
	int	count_n_col =0;
	while ( this->NextProcess(nuc, mpool,lpool, exc_energy) ) { 
		count_n_col++;
		if ( count_n_col >= n_col)
			return true;
	}    
	return false;
}
//_________________________________________________________________________________________________										
void Cascade::UpdateParticlesPosition( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t time ) {
	const Double_t t_min = .0001;
	Double_t ti = ( fabs(time) < 1.E-7 ) ? t_min: time;

	for ( int i = 0; i < nuc.GetA(); i++ )
		if ( nuc[i].IsBind() ) 
			particles_walk_thru(nuc[i],ti);
//	std::cout << "Updating meson ParticlesPosition size = " << mpool.size()<< std::endl;
//	int i = 0;
//	MPOOL = mpool.GetMesonStruct();
//	MPOOL.Print();
	for ( MesonsPool::iterator mit = mpool.begin(); mit != mpool.end(); mit++ ){
//	std::cout << "Updating meson ParticlesPosition i = " << ++i << std::endl;
		if  ( mit->second.IsBind() ) {
			particles_walk_thru( mit->second, ti);
		}
	}
	
	for ( LeptonsPool::iterator mit = lpool.begin(); mit != lpool.end(); mit++ ){
//	std::cout << "Updating leptons ParticlesPosition i = " << ++i << std::endl;
		particles_walk_thru( mit->second, ti);
	}
}
//_________________________________________________________________________________________________										
bool Cascade::IsCascadeFinished( NucleusDynamics& nuc, MesonsPool& mpool){
	Int_t   canEscape = 0,
		canEscapem = 0,
		bindNucleon = 0, 
		ressonances = 0;
  
	for ( int i = 0; i < nuc.GetA(); i++ ) {	
		if ( nuc[i].IsBind() ) {    	  	        
			bindNucleon++;	  
			int pid = nuc[i].PdgId();	
			/*
			 * NOTE: Protons may tunnel according to WKB approximation
			 * Kinetic energy must be compared onty with the well
			 */
			double v0 = nuc.NuclearPotentialWell( pid );
			double T = nuc[i].Momentum().E() - nuc[i].GetMass(); 
			
			if ( T > v0 ) {
			  //cout<<T<<" "<<nuc[i].Position().Mag()<<endl;
				canEscape++;
			}
			if ( !CPT::IsNucleon( pid ) ) 
				ressonances++;
		}
	}
	for ( MesonsPool::iterator mit = mpool.begin(); mit != mpool.end(); mit++ ) {    
		if  ( mit->second.IsBind() ) {
			canEscape++;
			canEscapem++;        
		}
	} 
	//cout<<"Can scape barion "<<canEscape<<endl;
	return ( canEscape == 0 && ressonances == 0 );
}
//_________________________________________________________________________________________________
bool Cascade::NextProcess( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool,  Double_t& ex_energy ){
	
#ifdef THERMA
	bool system_closed(false);
#endif
	

	if ( IsCascadeFinished( nuc, mpool) ){
#ifdef THERMA
		system_closed = true;
		pass++;
#else
		return false;
#endif
	}
	
	if(cont==0)
	  mytest = false;

	while(true) {
		proc_result = false;
		try {   
			//std::cout << std::endl;
			//nuc.ProtonFermiLevels().PrintStatus();
			//nuc.NeutronFermiLevels().PrintStatus();
#ifdef THERMA
			if(countAllowed == 0){
 				//std::cout << std::endl;
 				//cout << "countAllowed initial: " << countAllowed << endl;
// 				nuc.ProtonFermiLevels().PrintStatus();
// 				nuc.NeutronFermiLevels().PrintStatus();
				nuc.TakeProtonLevelsSnapshot("initial");
				nuc.TakeNeutronLevelsSnapshot("initial");
			}

			if(system_closed){
				if(pass == 1){
 					//std::cout << std::endl;
 					//cout << "countAllowed system closed: " << countAllowed << endl;
// 					nuc.ProtonFermiLevels().PrintStatus();
// 					nuc.NeutronFermiLevels().PrintStatus();
					nuc.TakeProtonLevelsSnapshot("sys_closed");
					nuc.TakeNeutronLevelsSnapshot("sys_closed");
				}
			}
#endif
			cont++;
// 			if(mytest){
// 			  cout << curr_proc->GetType() << endl;
// 			}
			if (PrintHist){
				NUC = nuc.GetNucStruct();
				MPOOL = mpool.GetMesonStruct();
				LPOOL = lpool.GetLeptonsStruct();
			}
	
			std::vector<ParticleDynamics> vec_add;
			
			curr_proc = timeOrdering->GetFastestProcess();
		
// 			if(mytest){
// 			  cout << curr_proc->GetType() << endl;
// 			  mytest = false;   
// 			}
			proc_result = curr_proc->ExecuteProcess( nuc, mpool,lpool, vec_add );
			//cout <<"time "<< elapsedTime <<" Process: " << curr_proc->GetType() <<  endl;
			if  (PrintHist) PRO = curr_proc->GetProcessStruct( nuc, mpool, vec_add);
			if ( ( curr_proc->GetTime() - t_changed ) >= delta_t_max || curr_proc->GetTime() > cascade_max_time ) {
				//std::cout << "[warning] Termination forced." << std::endl;
				ex_energy -= TerminateForced(nuc, mpool,lpool);
				elapsedTime = curr_proc->GetTime();
				casc_TerForc = true;
				delete curr_proc;
				return false;
			}     
			if ( proc_result ) { // Process allowed by Pauli blocking

				countAllowed++;
				Double_t energy_out = curr_proc->GetEnergyOut(nuc, mpool,lpool);
				if (energy_out  < 0  ) std::cout << "ERROR!!: Negative kinetic energy of the leaving particle - process involved: " << curr_proc->GetType() << ", current energy = " << ex_energy << ", energy being taken = " << energy_out <<  std::endl; 
// 				if (ex_energy - energy_out < 0  ){
// 				   cout << "Process: " << curr_proc->GetType() <<  endl;
// 				   std::cout << "ERROR!!: Negative excitation energy - process involved: " << curr_proc->GetType() << ", current energy = " << ex_energy << ", energy being taken = " << energy_out <<  std::endl;				  
// 				   delete curr_proc;
// 				   blocked++;
// 				   continue;
// 				}
				ex_energy -= energy_out;		
 				
				Int_t ptype = curr_proc->GetType();
				Double_t ti = curr_proc->GetTime();
				Double_t dt =  ti - t0;
				t0 = ti;
				elapsedTime = ti; // Cascade total elapsed time.
				
				/* NOTE: Portion below verifies the existence of leptons in vec_add,
				 * takes the leptons out and their energy
				 * Not useful right now. Cascade is not producing leptons.
				 * The selected process puts all current products (baryons and mesons) in vec_add
				 * and erases them at end because method above GetEnergyOut already takes care of emitted particles
				 * This portion may be useful someday
				 */
				Double_t leptons_en = 0.;
				for ( std::vector<ParticleDynamics>::iterator it = vec_add.begin(); it < vec_add.end(); it++ ) {
					if ( CPT::IsLepton(it->PdgId()) ) {
						leptons_en += it->Momentum().E();	  
						vec_add.erase(it);
					}
				}
				
				
				if ( !( ( ptype == SelectedProcess::barion_surface || ptype == SelectedProcess::meson_surface|| ptype == SelectedProcess::lepton_surface )  && energy_out == 0. ) ) {
					t_changed = ti;	  
				}
				
		// esta linea de test		curr_proc->ManageFinalProcessDetails(nuc, mpool, curr_proc->GetIdx1());
				
				if ((dt > 1.E-7)){
					// Do Update particles position
					UpdateParticlesPosition( nuc, mpool,lpool, dt);	
				
				}
				// Do Update Time... 
				timeOrdering->UpdateAll( *curr_proc, nuc, mpool, lpool, vec_add );
// 				if ( ptype == SelectedProcess::lepton_surface)
// 				  cout << "antes " << endl;

				delete curr_proc;

// 				if ( ptype == SelectedProcess::lepton_surface){
// 				  cout << "despues  " << endl;
// 				  mytest = true;
// 				}

				
				return true;	
			} 
			else {	
				blocked++;	
			}	
			delete curr_proc;      
		} 
		catch (BasicException ex) {
			TString msg = ex.ErrorMsg();
			msg += "\nError on process: ";
			msg += curr_proc->GetName();
			ex.SetErrorMsg(msg);
			delete curr_proc;
			throw;	      
		}
	}  
	return false;
}
//_____________________________________________________________________________________________										
void Cascade::Restart(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t energy) {
	if ( timeOrdering ) 
		delete timeOrdering;
	try { 
		this->start_energy = energy;  
		this->t0 = 0.;
		timeOrdering = new TimeOrdering(nuc, mpool, lpool);
	}
	catch (BasicException ex) {
		TString msg = "[Error] intra-nuclear cascade initializing error\n" +
		ex.ErrorMsg();
		throw BasicException(msg);
	}
}
//_____________________________________________________________________________________________										
void Cascade::FinalNucleus( Int_t& A, Int_t &Z, NucleusDynamics& nuc){
	A = 0; 
	Z = 0;
	for ( Int_t i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() ) {
			if ( nuc[i].PdgId() == CPT::neutron_ID ) 
				A++; 
			if ( nuc[i].PdgId() == CPT::proton_ID ) {
				A++; 
				Z++;
			}
		}
	}
}
