/* ================================================================================
 * 
 * 	Copyright 2008 2016 Jose Luis Bernal Castillo Evandro Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "TimeOrdering.hh"

//_________________________________________________________________________________________________										

TimeOrdering::TimeOrdering(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool ):TObject(){
	bbProcess = 0;
	bsProcess = 0;
	bdProcess = 0;
	bmProcess = 0;
	msProcess = 0;
	mdProcess = 0;
	lsProcess = 0;
	
	try {    
		bbProcess = new BaryonBaryon(nuc);
		bsProcess = new BaryonSurface(nuc);
		bdProcess = new BaryonDecay(nuc);
		bmProcess = new BaryonMeson(nuc);
		msProcess = new MesonSurface(mpool);
		mdProcess = new MesonDecay(mpool);
		lsProcess = new LeptonSurface(lpool);
	
		
		for ( MesonsPool::iterator it = mpool.begin(); it != mpool.end(); it++ )
			NewMesonProcess(it->first, -1, nuc, mpool, 0.); 
		
		for ( LeptonsPool::iterator it = lpool.begin(); it != lpool.end(); it++ ){
			NewLeptonProcess(it->first, -1, nuc, lpool, 0.);
		}


	}
	catch (BasicException ex) {
		Reset();
		throw;
	}
}
//_________________________________________________________________________________________________										
TimeOrdering::~TimeOrdering(){
	Reset();
}
//_________________________________________________________________________________________________										
void TimeOrdering::Reset(){
	if ( bbProcess ) 
		delete bbProcess;
	if ( bsProcess ) 
		delete bsProcess;
	if ( bdProcess ) 
		delete bdProcess;
	if ( bmProcess ) 
		delete bmProcess;
	if ( msProcess ) 
		delete msProcess;
	if ( mdProcess ) 
		delete mdProcess;
	if (lsProcess)
		delete lsProcess;
}
//_________________________________________________________________________________________________										
SelectedProcess* TimeOrdering::GetFastestProcess(){
	Int_t 	idx1 = -1,  idx2 = -1;
	Int_t 	temp_idx1 = 0, temp_idx2 = 0;  
	Int_t type = SelectedProcess::defp;
	char* name = "none";
	AbstractCascadeProcess *x = 0;
	Double_t t = infty;
	Double_t ti = t;
	// Baryon-Surface fastest process time
	if ( ( ti = bsProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "baryon-surface";
		type = SelectedProcess::barion_surface;
		x = bsProcess;
		t = ti;
	}
	// Meson-Surface fastest process time
	if ( ( ti = msProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "meson-surface";
		type = SelectedProcess::meson_surface;
		x = msProcess;
		t = ti;
	}
	// Baryon-Baryon fastes process time
	if ( ( ti = bbProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;    
		name = "baryon-baryon";
		type = SelectedProcess::barion_barion;
		x = bbProcess;
		t = ti;
	}
	// Meson-Decay fastes process time
	if ( ( ti = mdProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "meson-decay";
		type = SelectedProcess::meson_decay;
		x = mdProcess;
		t = ti;
	}
	// Baryon-Meson fastes process time
	if ( ( ti = bmProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "baryon-meson";
		type = SelectedProcess::barion_meson;
		x = bmProcess;
		t = ti;
	}
	// Baryon-Decay fastes process time
	if ( ( ti = bdProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "baryon decay";
		type = SelectedProcess::barion_decay;
		x = bdProcess;
		t = ti;
	}
	
	if ( ( ti = lsProcess->FastestProcess(temp_idx1, temp_idx2, t) ) < t ) {
		idx1 = temp_idx1;
		idx2 = temp_idx2;
		name = "lepton-surface";
		type = SelectedProcess::lepton_surface;
		x = lsProcess;
		t = ti;
	}
	
	// std::cout << name << " " << idx1 << " " << idx2 << " " << t << std::endl;
	return new SelectedProcess(type, name, idx1, idx2, x, t);

}
//_________________________________________________________________________________________________										
void TimeOrdering::UpdateAll(SelectedProcess& p, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add){
	Double_t t = p.GetTime();  
	Int_t ptype = p.GetType();
	Int_t idx1 = p.GetIdx1(), idx2 = p.GetIdx2();
	switch ( ptype ){
		case SelectedProcess::barion_barion:
			bbProcess->Update(idx1, idx2, nuc, mpool, lpool, t);
			bmProcess->Update(idx1, 0, nuc, mpool, lpool, t);
			bmProcess->Update(idx2, 0, nuc, mpool, lpool, t);
			bsProcess->Update(idx1, nuc, mpool, lpool, t);
			bsProcess->Update(idx2, nuc, mpool, lpool, t); 
		break;
		case SelectedProcess::barion_meson:
			bbProcess->Update(idx1, nuc, mpool, lpool, t);
			bsProcess->Update(idx1, nuc, mpool, lpool, t);
			bmProcess->Update(idx2, nuc, mpool, lpool, t);
			msProcess->Update(idx2, nuc, mpool, lpool, t);
		break;
		case SelectedProcess::barion_decay:
			bbProcess->Update(idx1, nuc, mpool, lpool, t);
			bsProcess->Update(idx1, nuc, mpool, lpool, t);
			bmProcess->Update(idx1, nuc, mpool, lpool, t);
		break;
		case SelectedProcess::meson_decay:
			bmProcess->Update(idx1, nuc, mpool, lpool, t);
			msProcess->Update(idx1, nuc, mpool, lpool, t);
		break;
		case SelectedProcess::barion_surface:
			bbProcess->Update(idx1, nuc, mpool, lpool, t);
			bsProcess->Update(idx1, nuc, mpool, lpool, t);
			bmProcess->Update(idx1, 0, nuc, mpool, lpool, t);      
		break;
		case SelectedProcess::meson_surface:
			bmProcess->Update(idx1, nuc, mpool, lpool, t);
			msProcess->Update(idx1, nuc, mpool, lpool, t);
		break;
		case SelectedProcess::lepton_surface:
			lsProcess->Update(idx1, nuc, mpool, lpool, t);
		break;
	}  
// 	 std::cout << "updated mesonpool " << std::endl;  
	if ( vec_add.size() > 0 ) {
		for ( unsigned int i = 0; i < vec_add.size(); i++ ) {
		     if(CPT::IsMeson(vec_add[i].PdgId())) {
			  int key = mpool.PutMeson(vec_add[i]);
			  NewMesonProcess(key, idx1, nuc, mpool, t);
		     }
		     
		     if(CPT::IsLepton(vec_add[i].PdgId())) {
			int key = lpool.PutLepton(vec_add[i]);
			  NewLeptonProcess(key, idx1, nuc, lpool, t);
			  cout << "a lepton here" << endl<< endl<< endl ;
		    }
		     
		}
		vec_add.clear();
	}  
	bdProcess->Update(idx1, nuc, mpool, lpool, t);
	mdProcess->Update(idx1, nuc, mpool, lpool, t);  
	totalTime = t;  
}
//_________________________________________________________________________________________________										
void TimeOrdering::NewMesonProcess( Int_t key, Int_t i, NucleusDynamics& nuc, MesonsPool& mpool,Double_t t ){
	if ( mpool.find(key) ) { 
		msProcess->AddMesonSurfaceProcess(key, mpool, nuc, t);
		bmProcess->AddBaryonMesonProcess(key, i, mpool, nuc, t);
	}
}

//_________________________________________________________________________________________________										
void TimeOrdering::NewLeptonProcess( Int_t key, Int_t i, NucleusDynamics& nuc, LeptonsPool& lpool,Double_t t ){
	if ( lpool.find(key) ) { 
		lsProcess->AddLeptonSurfaceProcess(key, lpool, nuc, t);
	}
}
