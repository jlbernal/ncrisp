/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __BaryonBaryon_pHH
#define __BaryonBaryon_pHH

#include <iostream>
#include <map>
#include "TObject.h"
#include "TObjArray.h"
#include "AbstractCascadeProcess.hh"
#include "BBChannel.hh"
#include "bb_actions.hh"
#include "bb_cross_sections.hh"
#include "time_collision.hh"
#include "DataHelper.hh"
///
//_________________________________________________________________________________________________										

class BaryonBaryon: public AbstractCascadeProcess{

private:
  
	Int_t __A;
	Int_t **bb_counter;
	Double_t **bb;  

	TObjArray *bb_ch;
	std::vector< std::pair<Int_t,Int_t> > candidates;
	std::vector<Int_t> c_counts;
  
protected:
  
	///
	//_________________________________________________________________________________________________										

	inline void UpdateBBCollisionTime( Int_t i, Int_t j, Double_t time, bool clear_candidates_flag = true );
  
	///
	//_________________________________________________________________________________________________										

	void ClearCandidates();
  
	///
	//_________________________________________________________________________________________________										

	void InitializeVectors(NucleusDynamics& nuc);     

public:
  
	///
	//_________________________________________________________________________________________________										

	BaryonBaryon(NucleusDynamics& nuc); 
  
	///
	//_________________________________________________________________________________________________										

	virtual ~BaryonBaryon();
  
	///
	//_________________________________________________________________________________________________										

	bool Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add);

	///
	//_________________________________________________________________________________________________										

	Double_t CalcTotalCrossSection( Int_t i, Int_t j, NucleusDynamics& nuc);

	///
	//_________________________________________________________________________________________________										

	Double_t FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin);

	///
	//_________________________________________________________________________________________________										

	void Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);

	///
	//_________________________________________________________________________________________________										

	void Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t);

	///
	//_________________________________________________________________________________________________										
	void ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);

	inline TObjArray* GetBBChannels() { return bb_ch; } 

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BaryonBaryon,0);
#endif // CRISP_SKIP_ROOTDICT
};

#endif
