/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __SelectedProcess_pHH
#define __SelectedProcess_pHH

#include <iostream>
#include "TNamed.h"
#include "AbstractCascadeProcess.hh"
#include "TString.h"
#include "TTree.h"
#include "DataHelper.hh"
///
//_________________________________________________________________________________________________										

class SelectedProcess: public TNamed {

private:

	Int_t _type;
	Int_t _idx1;
	Int_t _idx2;
	ParticleDynamics p1, p2;  
	Double_t _time;
	AbstractCascadeProcess* _process;

public:
	enum nprocess { defp = 0, barion_barion, barion_meson, barion_decay, meson_decay, barion_surface, meson_surface, lepton_surface }; 
	
	SelectedProcess( Int_t type, char* name, Int_t idx1, Int_t idx2, AbstractCascadeProcess* p, Double_t t);
	SelectedProcess(SelectedProcess& u);
	virtual ~SelectedProcess();
	inline Int_t GetIdx1() const { return _idx1; }
	inline Int_t  GetIdx2() const { return _idx2; }
	inline Double_t GetTime() const { return _time; }
	inline Int_t GetType() const { return _type; } 
	Double_t GetEnergyOut(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool );

	
	/**
	 * @brief Execute the selected procces in cascade
	 *
	 * @param nuc the nuclei
	 * @param mpool pool of mesons
	 * @param lpool pool of leptons
	 * @param new_particles vector that store the particles generated
	 * @return bool return if the process was successful
	 **/

	bool ExecuteProcess(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& new_particles);
	bool ManageFinalProcessDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1);	
	void ProcessStatus( std::ostream& ofs, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, std::vector<ParticleDynamics>& vec);
	TTree* GetProcessTree(NucleusDynamics& nuc, MesonsPool& mpool, std::vector<ParticleDynamics>& vec);
	Proc_Struct GetProcessStruct(NucleusDynamics& nuc, MesonsPool& mpool, std::vector<ParticleDynamics>& vec);

#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(SelectedProcess,0);
#endif // CRISP_SKIP_ROOTDICT
};
#endif
