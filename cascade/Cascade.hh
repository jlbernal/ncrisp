/* ================================================================================
 * 
 * 	Copyright 2008 2016 Jose Luis Bernal Castillo Evandro Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#ifndef __Cascade_PHH
#define __Cascade_PHH

#include <iostream>
#include <fstream>
#include "TObject.h"
#include "base_defs.hh"
#include "CrispParticleTable.hh"
#include "TimeOrdering.hh"
#include "time_surface.hh"
#include "time_collision.hh"
#include "TTree.h"
#include "DataHelper.hh"
class Cascade: public TObject{

private:
  
  /**
   * @brief class that manage time for each process inside cascade 
   **/
  TimeOrdering *timeOrdering;
  
  
	Double_t cascade_max_time;
	Double_t start_energy;
	Double_t t0; 
	Double_t elapsedTime;
	Int_t blocked, cont, countAllowed;
	Double_t t_changed;
	static const Double_t delta_t_max;
	bool flag_started; // flag for the partial cascade situation
	bool ProR; // flag to mark the success of current process
	Double_t exc_energy; //stores the excitation energy for partial cascade situation
	Double_t ex_energy; 
	SelectedProcess *curr_proc;
	bool PrintHist;
	TTree* History;
	Nuc_Struct NUC;
	Mes_Struct MPOOL;
	Lep_Struct LPOOL;
	Proc_Struct PRO;
	bool proc_result;
	bool casc_TerForc;
#ifdef THERMA	
	int pass;
#endif
	bool mytest; // use for debugin 
protected:
  
	/// Terminate intra-nuclear cascade - time limit exceeded.
	Double_t TerminateForced(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool);
public: 
	Cascade(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t start_energy, bool PrintCasc = false);
	virtual ~Cascade();
	/// Do the intra-nuclear cascade, return the final excitation energy and the final nucleus configuration
	Double_t Execute( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool);
	bool Execute_Partial_Cascade( NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, int n_col);
	void UpdateParticlesPosition( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t time );
	bool IsCascadeFinished( NucleusDynamics& nuc, MesonsPool& mpool);    
	/// Execute the cascade step-by-step ... 
	bool NextProcess( NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t& ex_energy);
	void Restart(NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t energy); 
	void FinalNucleus( Int_t& A, Int_t &Z, NucleusDynamics& nuc);
	inline Double_t ElapsedTime() const { return elapsedTime; }
	inline TTree* GetHistory() const { return History; }
	inline Int_t TotalBlocked() const { return blocked; }
	inline Int_t TotalCount() const { return cont; }
	inline Double_t BlockingIndex() const { return blocked/((Double_t)cont); }
	Double_t GetExcitationEnergy(){return exc_energy;}
	
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(Cascade,0);
#endif // CRISP_SKIP_ROOTDICT
  
};

#endif
