/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include  "MesonSurface.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(MesonSurface);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										

MesonSurface::MesonSurface(MesonsPool&):AbstractCascadeProcess(), meson_surface(){
}

//_________________________________________________________________________________________________										

MesonSurface::~MesonSurface(){
	meson_surface.clear();
}

//_________________________________________________________________________________________________										

Double_t MesonSurface::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){

	idx2 = -1;
	if ( meson_surface.empty() ) { 
		idx1 = -1;
		return infty;
	}    
	
	int k = -1;
	double t = tmin;
	
	for ( std::map<int,double>::iterator it = meson_surface.begin(); 
		it != meson_surface.end(); it++ ) {
		
		if ( it->second < t ) {
			t = it->second;
			k = it->first;
		}      
	}
  idx1 = k;
  return t;
}

//_________________________________________________________________________________________________										

bool MesonSurface::Execute ( Int_t& i, Int_t& , NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, std::vector<ParticleDynamics>&) {

	// compute the total charge and total mass of the nucleus
	
	int key = i;
	double t_charge = nuc.TotalCharge() + mpool.TotalCharge();  
	
	double v0 = nuc.NuclearPotential( mpool[key].PdgId() );   //this one includes coulomb barrier for charged particles
	double well = nuc.NuclearPotentialWell( mpool[key].PdgId() ); //this one refers only to potential due to the well

	if ( it_was_even_so( mpool[key], nuc.GetRadium(), t_charge, v0, well, meson_surface[key] ) ) {
//			std::cout << "meson:" << mpool[key].Name()  << " go out!" << " E_out = " << mpool[key].Momentum().E() << std::endl;
			mpool[key].BindIt(false); 
			//taking away emitted particle's momentum from nucleus
			nuc.SetMomentum(nuc.Momentum() -  mpool[key].Momentum());
			//taking away emitted particle's angulars momentum from nucleus
			TVector3 lTemp = mpool[key].Position().Cross(mpool[key].Momentum().Vect());
			nuc.SetAngularMomentum(nuc.AngularMomentum() - lTemp);
		return true;  
	} 
	return true;  
}

//_________________________________________________________________________________________________										

void MesonSurface::Update( Int_t, Int_t, NucleusDynamics&, MesonsPool& ,LeptonsPool& , Double_t ){

	// should not implement
}

//_________________________________________________________________________________________________										

void MesonSurface::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, Double_t t) {

	const int key = k;
  
	if ( !mpool.find(key) ) 
		meson_surface.erase(key); // meson was absorbed !
	else {    
		if ( !mpool[key].IsBind() )
			meson_surface[key] = infty;
		else { 
			double ti = time_surface(mpool[key], nuc);
			meson_surface[key] = t + ti;
		}
	}
}

//_________________________________________________________________________________________________										

void MesonSurface::AddMesonSurfaceProcess( int key, MesonsPool& mpool, NucleusDynamics& nuc, double t ){

	if ( mpool.find(key) ) {
		double ti = time_surface(mpool[key], nuc);
		meson_surface[key] = t + ti;
	}
}

//_________________________________________________________________________________________________										
void MesonSurface::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

}