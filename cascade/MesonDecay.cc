/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "MesonDecay.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(MesonDecay);
#endif // CRISP_SKIP_ROOTDICT

//_________________________________________________________________________________________________										
MesonDecay::MesonDecay(MesonsPool& mpool):AbstractCascadeProcess(), meson_decay(){
	FindNewMesonResonance(mpool); 
}
//_________________________________________________________________________________________________										
MesonDecay::~MesonDecay(){
	meson_decay.clear();
}
//_________________________________________________________________________________________________										
Double_t MesonDecay::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){
	double ti = tmin;
	idx1 = -1; 
	idx2 = -1;
	for ( std::map<int,double>::iterator it = meson_decay.begin(); it != meson_decay.end(); it++ ) {   
		if ( it->second < ti ) {
			idx1 = it->first;
			ti = it->second;
		}
	}  
	return ti;
}
//_________________________________________________________________________________________________										
bool MesonDecay::Execute ( Int_t& idx1, Int_t&, NucleusDynamics&, MesonsPool& mpool,LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add){
	ParticleDynamics &parent = mpool[idx1]; 
	double x = gRandom->Uniform();
	double tmin = meson_decay[idx1];  
	if ( get_decay_channel( x, mpool[idx1].GetParticleData(), vec_add) ) {
		//std::cout << "EXECUTING MESON DECAY ....Meson parent id = " << mpool[idx1].PdgId() << std::endl;
		// CM energy ... 
		double M = parent.Momentum().M();
		double mi = M;
		for ( unsigned int i = 0; i < vec_add.size(); i++ ) { 	
			if ( vec_add[i].GetParticleData()->NDecayChannels() > 0) {
				//std::cout << "Meson daugther [" << i << "]id = " << vec_add[i].PdgId() << std::endl;
				double mass = resonance_mass( mi, vec_add[i].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
				mi -= mass;	
				vec_add[i].SetMass ( mass );
				vec_add[i].SetLifeTime( tmin + vec_add[i].HalfLife() );	
			}
		}    
		if ( mi < 0. ) {
			Double_t tm = 0.;
			for ( unsigned int i = 0; i < vec_add.size(); i++ ) {
				std::cout << Form("m[%d] = %E", i, vec_add[i].GetMass()) << std::endl;
				tm += vec_add[i].GetMass();	
			}
			std::cout << Form("[warning] mt < E ( %e < %e)", mi, M ) << std::endl;      
			return false;
		}
		else { 
			particlesOutMomentum( parent.Momentum(), vec_add );
			for ( unsigned int i = 0; i < vec_add.size(); i++ ) {
				//std::cout << "Meson daugther [" << i << "]id = " << vec_add[i].PdgId() << std::endl;
				//vec_add[i].Momentum().Print();
				vec_add[i].SetPosition(parent.Position());
			}
			mpool.RemoveMeson(idx1); // remove parent from meson pool.
			return true;
		}
	}
	return false;
}
//_________________________________________________________________________________________________										
void MesonDecay::Update( Int_t idx1, Int_t, NucleusDynamics& nuc, MesonsPool& mpool,LeptonsPool& lpool, Double_t t){
	this->Update(idx1, nuc, mpool,lpool, t);  
}
//_________________________________________________________________________________________________										
void MesonDecay::Update( Int_t, NucleusDynamics&, MesonsPool& mpool,LeptonsPool& lpool, Double_t){
	meson_decay.clear();
	for ( MesonsPool::iterator it = mpool.begin(); it != mpool.end(); it++ ) {    
		if ( it->second.IsBind() && it->second.GetParticleData()->NDecayChannels() > 0 )
			//meson_decay[it->first] = infty;    // for no one decay ************************************************************
			meson_decay[it->first] = it->second.LifeTime();    
		}
}
//_________________________________________________________________________________________________										
void MesonDecay::FindNewMesonResonance( MesonsPool& mpool ){
	for ( MesonsPool::iterator it = mpool.begin(); it != mpool.end(); it++ ) {
		if (  it->second.GetParticleData()->NDecayChannels() > 0 ) 
			//meson_decay[it->first] = infty;    // for no one decay ************************************************************
			meson_decay[it->first] = it->second.LifeTime();    
	}

}

void MesonDecay::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){
}