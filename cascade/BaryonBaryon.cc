/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BaryonBaryon.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(BaryonBaryon);
#endif // CRISP_SKIP_ROOTDICT

BaryonBaryon::BaryonBaryon(NucleusDynamics& nuc):AbstractCascadeProcess(), candidates(), c_counts(){
	bb = 0;
	bb_ch = 0;
	this->InitializeVectors(nuc);
}

BaryonBaryon::~BaryonBaryon(){
	ClearCandidates();
	if( bb_ch ) bb_ch->Delete();
	if( bb_ch ) {
		delete bb_ch;
		bb_ch = 0;
	}
	if( bb ){
		for (int i(0); i < __A-1; i++)
			delete[] bb[i];
		delete[] bb;
		bb = 0;
	}
}

void BaryonBaryon::InitializeVectors(NucleusDynamics& nuc){
	// Pega o numero de massa do nucleo
	__A = nuc.GetA();  
	//const Int_t n = ( __A * ( __A - 1 ) ) / 2;  	// Numero de colisões entre todos os nucleons
	// alocação matrix esparsa
	//
	// nucleo de A=4 -> [0, 1, 2, 3]
	// [0] -> [1, 2, 3]
	// [1] -> [2, 3]
	// [2] -> [3]
	//
	//
	// numero de linhas = A - 1
	// numero de colunas = A - 1 - i
	// allocate space to time-collision
	bb = new Double_t*[__A - 1];
	for ( Int_t i = 0; i < __A - 1; i++ ){  
		bb[i] = new Double_t[__A  -1 - i];
	} 
	bb_ch = new TObjArray();
	bb_ch->SetOwner();
#ifndef __CINT__
	bb_ch->Add(new BBChannel( "elastic", bb_elastic_cross_section, 0, bb_elastic_np));
	bb_ch->Add(new BBChannel( "inelastic", bb_one_pion_cross_section, 0, bb_inelastic));  
#else
	bb_ch->Add(new BBChannel( CrossSectionChannel( "elastic", bb_elastic_cross_section), bb_elastic_np ));
	bb_ch->Add(new BBChannel( CrossSectionChannel( "inelastic", bb_one_pion_cross_section), bb_inelastic));  
#endif
	Double_t sum;
	for ( Int_t i = 0; i < __A - 1; i++ )
	{
		for ( Int_t j = i + 1; j < __A; j++ )
		{        
			// j1 = i; j2 = j;
			sum = CalcTotalCrossSection(i, j, nuc);
			bb[i][j-i-1] = time_collision( nuc[i], nuc[j], sum);      
		}
	}
}

Double_t BaryonBaryon::CalcTotalCrossSection( Int_t i, Int_t j, NucleusDynamics& nuc){
	Double_t cs = 0.;  
	for ( Int_t k = 0; k < (Int_t)bb_ch->GetEntriesFast(); k++ ) {
		cs += ((BBChannel*)(*bb_ch)[k])->CrossSection().Calculate(&nuc[i], &nuc[j]);
	}
	return cs;
}

void BaryonBaryon::ClearCandidates(){
	candidates.clear();
	c_counts.clear();
}

Double_t BaryonBaryon::FastestProcess( Int_t& idx1, Int_t& idx2, Double_t tmin){
	Double_t t = tmin;    
	if ( !candidates.empty() ){
		Int_t m = 0, n = 0;
		for ( Int_t k = 0; k < (Int_t)candidates.size(); k++ ){
			// pega os numeros dos dois precessos
			Int_t i = candidates[k].first;
			Int_t j = candidates[k].second;
			if ( i > j ) std::swap(i, j);	// devido a alocação da tabela, j sempre deve ser maior que i
			if ( bb[i][j-i-1] < t ){ 
				m = i; n = j; t = bb[i][j-i-1];
			}
		}  
		idx1 = m; idx2 = n;
		return t;
	}
	Int_t m = 1, n = 0;
	for ( Int_t i = 0; i < __A -1; i++ ){
		for ( Int_t j = i + 1; j < __A; j++ ){
			Double_t ti = bb[i][j-i-1];
			if ( ti < tmin ){ 	
				candidates.push_back( std::pair<Int_t,Int_t> (i,j) );
				if ( ti < t ){
					m = i; n = j; 		  
					t = ti;
				}
			}
		}
	}
	idx1 = m; idx2 = n;     
	return t;
}

void  BaryonBaryon::Update( Int_t idx1, Int_t idx2, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool,  Double_t t){
	Int_t j = idx1; Int_t k = idx2;  
	Double_t tcs, ti;
	if  ( nuc[j].IsBind() && nuc[k].IsBind() ){   
		tcs = CalcTotalCrossSection (j, k, nuc);
		ti = time_collision ( nuc[j], nuc[k], tcs );            
		UpdateBBCollisionTime(j, k, ti + t);    
	} else 
		UpdateBBCollisionTime(j, k, infty, true);    

	for ( Int_t i = 0; i < __A; i++) {   
		if ( i != k && i != j ) {       
			if ( nuc[i].IsBind() ) {      
				tcs = CalcTotalCrossSection (i, k, nuc);
				ti = time_collision ( nuc[i], nuc[k], tcs );            
				UpdateBBCollisionTime(i, k, ti + t);
				tcs = CalcTotalCrossSection (i, j, nuc);
				ti = time_collision ( nuc[i], nuc[j], tcs );            
				UpdateBBCollisionTime(i, j, ti + t);
			} 
			else {
				UpdateBBCollisionTime(i, j, infty, true);
				UpdateBBCollisionTime(i, k, infty, true);
			}      
		}
	} 
}

void BaryonBaryon::Update( Int_t k, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, Double_t t){
	if ( !nuc[k].IsBind() ){
		for ( Int_t i = 0; i < __A; i++ )
			if ( i != k )
				UpdateBBCollisionTime(i, k, infty, true );
	} 
	else {
		for ( Int_t i = 0; i < __A; i++) 
			if ( i != k ) {	
				if ( nuc[i].IsBind() ){
					Double_t tcs = CalcTotalCrossSection (i, k, nuc);
					Double_t ti = time_collision ( nuc[i], nuc[k], tcs);
					UpdateBBCollisionTime(i, k, ti + t, true);      
				}  
				else
					UpdateBBCollisionTime(i, k, infty, true);
			}
	}
}

bool BaryonBaryon::Execute ( Int_t& i, Int_t& j, NucleusDynamics& nuc, MesonsPool& mpool, LeptonsPool& lpool, std::vector<ParticleDynamics>& vec_add){
	Int_t m = i, n = j;
	if ( m > n ) std::swap(m, n);
	if ( !nuc[m].IsBind() || !nuc[n].IsBind() ){
		this->UpdateBBCollisionTime(m, n, infty, false);
		return false;
	}
	Double_t tcs = this->CalcTotalCrossSection(m, n, nuc); 
	if ( tcs < 1.E-7 ){
		this->UpdateBBCollisionTime(m, n, infty, false);
		return false;
	}
	Double_t sum = 0. ;
	Double_t x = gRandom->Uniform();  
	for ( Int_t k = 0; k < bb_ch->GetEntriesFast(); k++ ){
		sum += ((BBChannel*)(*bb_ch)[k])->CrossSection().Value() / tcs;
		if ( x < sum ){
			if ( !((BBChannel*)(*bb_ch)[k])->DoAction( m, n, &nuc, bb[m][n-m-1], &mpool, &vec_add ) ){
//					std::cout << ((BBChannel*)(*bb_ch)[k])->CrossSection().GetName() ;
					this->UpdateBBCollisionTime(m, n, infty, false);
					return false;
			}else	{
//				std::cout << "nucleon i = " << i << ", nucleon j = " << j << std::endl;
				return true;
			}
		}
	}
	this->UpdateBBCollisionTime(m, n, infty, false);
	return false;
}

void BaryonBaryon::UpdateBBCollisionTime( Int_t i, Int_t j, Double_t time, bool clear_candidates_flag ) {
//	Int_t m = i, n = j;
//	if ( n > m ) std::swap(m, n);
//	bb[m][n] = time;  
	if ( i > j ) std::swap(i, j);	// devido a alocação da tabela, j sempre deve ser maior que i
	bb[i][j-i-1] = time;
//	++(bb_counter[m][n]);
	if ( clear_candidates_flag ) ClearCandidates(); 
}


void BaryonBaryon::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

}