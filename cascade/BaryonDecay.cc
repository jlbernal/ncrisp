/* ================================================================================
 * 
 * 	Copyright 2008
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "BaryonDecay.hh"

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(BaryonDecay);
#endif // CRISP_SKIP_ROOTDICT

BaryonDecay::BaryonDecay(NucleusDynamics& nuc):AbstractCascadeProcess(), bd_time(){
	FindResonanceForDecay(nuc);
}
//_____________________________________________________________________________________________										

BaryonDecay::~BaryonDecay(){
	bd_time.clear();
}
//_____________________________________________________________________________________________										

Double_t BaryonDecay::FastestProcess( Int_t& idx1, Int_t&, Double_t tmin){

	double t = tmin;
	int k = -1;  
	for ( unsigned int i = 0; i < bd_time.size(); i++ ) { 
		if ( bd_time[i] < tmin ) { 
			t = bd_time[i];
			k = i;
		}      
	}
	idx1 = k;
	return t;
}
//_____________________________________________________________________________________________										

bool BaryonDecay::Execute ( Int_t& i, Int_t&, NucleusDynamics& nuc, MesonsPool&, LeptonsPool& , std::vector<ParticleDynamics>& vec_add){

	double x = gRandom->Uniform();    
	
	if ( get_decay_channel( x, nuc[i].GetParticleData(), vec_add) ) {
		if ( DoDistMomentum(i , nuc, vec_add, bd_time[i]) ) {             
			return true;
		}
	}
	vec_add.clear();
	bd_time[i] = infty;
  
  return false;
}

//_____________________________________________________________________________________________										

void BaryonDecay::Update( Int_t, Int_t, NucleusDynamics&, MesonsPool&, LeptonsPool&, Double_t){
  // Should not implement
}
//_____________________________________________________________________________________________										

void BaryonDecay::Update ( Int_t, NucleusDynamics& nuc, MesonsPool&, LeptonsPool& , double t ){

	bd_time.clear();
	const double dt = .001;  
	
	for (int i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() && !CPT::IsNucleon( nuc[i].PdgId() ) ) {
			double tau = nuc[i].HalfLife();
			if ( tau < t ) 
				tau = t + dt;      
			bd_time.push_back( tau );
		}
		else 
		bd_time.push_back(infty);
	}  
}
//_____________________________________________________________________________________________										

void BaryonDecay::FindResonanceForDecay(NucleusDynamics& nuc){
  
	bd_time.clear();
	for (int i = 0; i < nuc.GetA(); i++ ) {
		if ( nuc[i].IsBind() && !CPT::IsNucleon( nuc[i].PdgId() ) ) {      

			bd_time.push_back(nuc[i].LifeTime());
		} 	
		else 
			bd_time.push_back(infty);
	} 
}
//_____________________________________________________________________________________________										
/*
bool BaryonDecay::DoDistMomentum( int idx, NucleusDynamics& nuc, std::vector<ParticleDynamics>& daughters, Double_t current_time){

	ParticleDynamics baryon;    
	ParticleDynamics parent = nuc[idx];
	for ( unsigned int i = 0; i < daughters.size(); i++ ) {    
		if ( CPT::IsImplRessonance( daughters[i].PdgId() ) || CPT::IsNucleon( daughters[i].PdgId() ) ){      
			baryon = daughters[i];      
			// remove the baryon from the daughters list.
			daughters.erase( daughters.begin() + i );
		}
	}    
	if ( daughters.size() == 1 ) {        
		//parent.SetPosition( nuc[idx].Position() );    
		Double_t tau = parent.HalfLife();
        
		// need to make a more general way to deal with mesons decays,
		// i will not change this now...
		if ( CPT::IsRho( daughters[0].PdgId() ) ) {      
			// a long line code to get the rho upperCutoff !
			// 1000. is due a GeV -> MeV convertion.
			double upper_cut = daughters[0].GetMass() + daughters[0].GetTotalWidth();
			double rssMass = resonance_mass( upper_cut, daughters[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
			daughters[0].SetMass ( rssMass );
			// daughters[0].SetLifeTime( bd_time[idx] + daughters[0].HalfLife() );
			daughters[0].SetLifeTime( current_time + daughters[0].HalfLife() );
		} 
		if ( CPT::IsNucleon( baryon.PdgId() ) ){ 
			baryon.SetMass( baryon.GetInvariantMass() * CPT::effn );      
		}
		else { 
			// is another ressonance 
			double upper_cut = parent.Momentum().E() - daughters[0].GetMass();
			double rssMass = resonance_mass( upper_cut, baryon.PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
			baryon.SetMass( rssMass );
			double t = baryon.HalfLife();
      
			baryon.SetLifeTime ( current_time + t );
		}        
		// calculate the momenta for baryon and the meson in daughters.
		if ( !decayKinematic( parent, baryon, daughters[0] ) ) {
			daughters.clear();      
			return false;    
		}
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		particles_walk_thru(baryon, tau);
		// Setting meson position ... 
		daughters[0].SetPosition( parent.Position());        
		particles_walk_thru(daughters[0], tau);    

		double m_f = daughters[0].GetMass() + baryon.GetMass();    

		// check pauli blocking to result baryon
		if ( parent.GetMass() >= m_f ) {
			if ( nuc.RessonanceDecay(idx, baryon)  ) {
				return true;
			}
			// std::cout << "decay forbidden: Pauli Blocked" << std::endl;
		}
	}
	daughters.clear();
	return false;
}
*/

bool BaryonDecay::DoDistMomentum( int idx, NucleusDynamics& nuc, std::vector<ParticleDynamics>& daughters, Double_t current_time){

	/*
	 * NOTE: Mesons created after a baryon decay are recorded in the mesons pool by the
	 * class TimeOrdering in the method UpdateAll.
	 */
	
	ParticleDynamics baryon;    
	ParticleDynamics parent = nuc[idx];
	//std::cout<<" Resonance_decay_ID "<<parent.PdgId()<<" mass "<<parent.GetMass()<<std::endl;
	Double_t tau = parent.HalfLife();
	
	for ( unsigned int i = 0; i < daughters.size(); i++ ) {    
		if ( CPT::IsImplRessonance( daughters[i].PdgId() ) || CPT::IsNucleon( daughters[i].PdgId() ) ){      
			baryon = daughters[i];      
			// remove the baryon from the daughters list.
			daughters.erase( daughters.begin() + i );
		}
	}    
	if ( daughters.size() == 1 ) {    
		
		if ( CPT::IsNucleon( baryon.PdgId() ) ){ 
			baryon.SetMass( baryon.GetInvariantMass() * CPT::effn );
			
			if ( CPT::IsMesonResonance( daughters[0].PdgId() ) ) {      
				//std::cout << "resonance decay: nucleon + meson resonance" << std::endl;
				double upper_cut = daughters[0].GetMass() + daughters[0].GetTotalWidth();
				upper_cut = ( upper_cut > parent.Momentum().E() -  baryon.GetMass() ) ? parent.Momentum().E() -  baryon.GetMass(): upper_cut;
				double rssMass = resonance_mass( upper_cut, daughters[0].PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
				daughters[0].SetMass ( rssMass );
				// daughters[0].SetLifeTime( bd_time[idx] + daughters[0].HalfLife() );
				daughters[0].SetLifeTime( current_time + daughters[0].HalfLife() );
			}
			else{
				//std::cout << "resonance decay: nucleon + meson" << std::endl;
				daughters[0].SetMass( daughters[0].GetInvariantMass() * CPT::effn );
			}
		}
		else { 
			//std::cout << "resonance decay: nucleon resonance + meson" << std::endl;
			//if the baryon is a resonance, the other decay product is a meson pi (no width)
			daughters[0].SetMass( daughters[0].GetInvariantMass() * CPT::effn );
			
			double upper_cut = baryon.GetMass() + baryon.GetTotalWidth();
			upper_cut = ( upper_cut > parent.Momentum().E() -  daughters[0].GetMass() ) ? parent.Momentum().E() -  daughters[0].GetMass(): upper_cut;
			double rssMass = resonance_mass( upper_cut, baryon.PdgId() ); //NOTE: resonance_mass routine already takes care of effective mass
			baryon.SetMass( rssMass );
			baryon.SetLifeTime ( current_time + baryon.HalfLife() );
		}        
		
		// calculate the momenta for baryon and the meson in daughters.
		if ( !decayKinematic( parent, baryon, daughters[0] ) ) {
			daughters.clear();      
			return false;    
		}
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		particles_walk_thru(baryon, tau);
		// Setting meson position ... 
		daughters[0].SetPosition( parent.Position());        
		particles_walk_thru(daughters[0], tau);    

		double m_f = daughters[0].GetMass() + baryon.GetMass();    

		// check pauli blocking to result baryon
		//if ( parent.GetMass() >= m_f ) {
			if ( nuc.RessonanceDecay(idx, baryon)  ) {
				//std::cout << "resonance decay: Pauli permitted" << std::endl;
				return true;
			}
			//std::cout << "resonance decay: Pauli blocked" << std::endl;
		//}
	}
	else if(daughters.size() > 1){
		//std::cout << "resonance decay: nucleon + 2 mesons" << std::endl;
		std::vector<ParticleDynamics> v;
		baryon.SetMass( baryon.GetInvariantMass() * CPT::effn );
		v.push_back( baryon );
		
		for( unsigned int i = 0; i < daughters.size(); i++ ){
 			daughters[i].SetMass( daughters[i].GetInvariantMass() * CPT::effn );
			v.push_back( daughters[i] );
 		}
 		
 		double m_f = baryon.GetMass();
		for( unsigned int i = 0; i < daughters.size(); i++ ){  
			m_f = m_f + daughters[i].GetMass();
		}
 		
 		if ( parent.GetMass() < m_f ) {      
// 			std::cout << "resonance decay: mass of daughters higher than mass of parent" << std::endl;
// 			std::cout << "parent: " << parent.PdgId() << "  mass: " << parent.GetMass() << std::endl;
// 			std::cout << "baryon: " << baryon.PdgId() << "  mass: " << baryon.GetMass() << std::endl;
// 			for( unsigned int i = 0; i < daughters.size(); i++ ){
// 				std::cout << "daughters[" << i << "]: " << daughters[i].PdgId() << "  mass: " << daughters[i].GetMass() << std::endl;
// 			}
			daughters.clear();
			return false;
		}
 		
 		if(!particlesOutMomentum(parent.Momentum(), v)){
			daughters.clear();      
			return false;
		}
		
		// Setting baryon position
		baryon.SetPosition( parent.Position() );
		particles_walk_thru(baryon, tau);
		// Setting meson position ... 
		for( unsigned int i = 0; i < daughters.size(); i++ ){
			daughters[i].SetPosition( parent.Position());        
			particles_walk_thru(daughters[i], tau);   
		}
		
		// check pauli blocking to result baryon
		if ( nuc.RessonanceDecay(idx, baryon)  ) {
			//std::cout << "resonance decay: Pauli permitted" << std::endl;
			return true;
		}
		//std::cout << "resonance decay: Pauli blocked" << std::endl;
	}
	daughters.clear();
	return false;
}


void BaryonDecay::ManageFinalDetails(NucleusDynamics& nuc, MesonsPool& mpool, Int_t idx1){

}