/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Implementation file VectorMeson_Ampl_Helper.cpp
 ************************************************************************
 * Description:
 * Helper functions for Vector Meson amplitude calculation
   data on crisp_table.txt file
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#include "VectorMeson_Ampl_Helper.hh"

double Trg_Fun(double x, double y, double z){
	return x*x + y*y + z*z - 2*x*y -2*y*z -2*x*z;
}
double Mand_Sigma(double m1, double m2, double m3, double m4){
	return m1*m1 + m2*m2 + m3*m3 + m4*m4;
}
double Mand_t(double cos_theta, double s, double m1, double m2, double m3, double m4){
	double m1_2 = m1*m1;
	double m2_2 = m2*m2;
	double m3_2 = m3*m3;
	double m4_2 = m4*m4;
	double t = sqrt(Trg_Fun(s, m1_2, m2_2)) * sqrt(Trg_Fun(s, m3_2, m4_2)) * cos_theta/2/s  - s*s/2/s - (m1_2 - m2_2)*(m3_2 - m4_2)/2/s + Mand_Sigma(m1,m2,m3,m4)/2;
	return t;
}
double Mand_cos_theta_s(double s, double t, double m1, double m2, double m3, double m4){
	double m1_2 = m1*m1;
	double m2_2 = m2*m2;
	double m3_2 = m3*m3;
	double m4_2 = m4*m4;
	double lambda = sqrt(Trg_Fun(s, m1_2, m2_2)*Trg_Fun(s, m3_2, m4_2));
	double cos = s*s/lambda + s*2*t/lambda - s*Mand_Sigma(m1,m2,m3,m4)/lambda + (m1_2 - m2_2)*(m3_2 - m4_2)/lambda ;
	return cos;
}
double Mand_cos_theta_t(double s, double t, double m1, double m2, double m3, double m4){
	double m1_2 = m1*m1;
	double m2_2 = m2*m2;
	double m3_2 = m3*m3;
	double m4_2 = m4*m4;
	double lambda = sqrt(Trg_Fun(t, m1_2, m3_2)*Trg_Fun(t, m2_2, m4_2));
	double cos = t*t/lambda + t*2*s/lambda - t*Mand_Sigma(m1,m2,m3,m4)/lambda + (m1_2 - m3_2)*(m2_2 - m4_2)/lambda;
	return cos;
}
double VM_Mand_z(double s, double Q_2, double t, double Mass_N, double Mass_VM){  //Eq (2) M_N include the future posibility of neutron
	double Mp_2 = Mass_N*Mass_N;
	double Mv_2 = Mass_VM*Mass_VM;
	double lambda = sqrt(TMath::Power(t + Q_2 - Mv_2,2) + 4*Mv_2*Q_2);//*sqrt(t*t - 4*Mp_2*t);
	double z =  t/lambda + 2*s/lambda - 2*Mp_2/lambda +  Q_2/lambda - Mv_2/lambda;// *t
	return z;
}
double VM_Mand_t(double s, double Q_2, double cos, double Mass_N, double Mass_VM){
	double MV_2 = Mass_VM * Mass_VM;
	double MN_2 = Mass_N  * Mass_N;
	double lambda = sqrt( Trg_Fun(s,-Q_2,MN_2)*Trg_Fun(s,MV_2,MN_2) );
	return 0.5*(lambda*cos/s - (s + Q_2 - MV_2- 2*MN_2 ) + (Q_2 + MN_2)*(MV_2 - MN_2)/s);
}
double VM_Mand_CosTheta(double s, double Q_2, double t, double Mass_N, double Mass_VM){  //Eq (2) M_N include the future posibility of neutron
	double MV_2 = Mass_VM * Mass_VM;
	double MN_2 = Mass_N  * Mass_N;
	double lambda = sqrt( Trg_Fun(s,-Q_2,MN_2)*Trg_Fun(s,MV_2,MN_2) );
	return s*(2*t + s + Q_2 - MV_2 - 2*MN_2)/lambda - (Q_2 + MN_2)*( MV_2 - MN_2)/lambda;
}
double VM_Pom_bi(double t, double Qbar_2, double Mass_pion, double bi0, double bi1, double Qb_2){ // Eq (20)
	double pion_mass_2 = Mass_pion*Mass_pion; //Evaluating the possibility of ussing different pion mass (pion+-, pion0)
	double Temp_bi = (bi0 + bi1/(1 + Qbar_2/Qb_2))*(sqrt(4*pion_mass_2) - sqrt(4*pion_mass_2 - t));
	return Temp_bi;
}
double VM_Pom_gi(double t, double Mass_pion, double Qbar_2, double gi, double Qi_2, double bi0, double bi1, double Qb_2){ //Eq (19)
	double Temp_gi = gi/( Qi_2 + Qbar_2)* exp(VM_Pom_bi(t, Qbar_2, Mass_pion, bi0, bi1, Qb_2));
	return Temp_gi;
}
double VM_Regg_br(double t, double Qbar_2, double br, double Qb_2){ // Eq (22)
	double Temp_br = br*t/(1 + (Qbar_2/Qb_2));
	return Temp_br;
}
double VM_Regg_gr(double t, double Mass_N, double Qbar_2, double gr, double Qr_2, double br, double Qb_2){ //Eq (21)
	double Temp_gr = gr*Mass_N*Mass_N/( (Qr_2 + Qbar_2)*Qbar_2)* exp(VM_Regg_br(t,Qbar_2,br, Qb_2));
	return Temp_gr;
}
