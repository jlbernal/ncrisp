/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Implementation file VectorMeson_Ampl_Terms.cpp
 ************************************************************************
 * Description:
 * Vector Meson amplitude Terms calculation
   data on crisp_table.txt file
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#include "VectorMeson_Ampl_Terms.hh"
/*Parameters obtained by fitting Experimental Data (Table 1) */

const double _gamma = 0.053853;
const double _g1  =  0.010435;
const double _g0  = -0.032901;
const double _gf  =  0.083371;
const double _gpi = 0.60011;
const double _Q0_2 = 0.0;
const double _Q1_2 = 0.41908;
const double _Qr_2 = 0.0;
const double _Qb_2 = 3.9724;
const double _b10  = 2.1251;
const double _b11  = 2.5979; 
const double _b00  = 2.6967;
const double _b01  = 6.7897;
const double _br   = 4.5741;
const double _ReggTraj_alpha_0_f  = 0.8;
const double _ReggTraj_alpha_0_p_f  = 0.85;
const double _ReggTraj_alpha_0_pi = 0.0;
const double _ReggTraj_alpha_0_p_pi = 0.85;
//For Virtuality Q² diferent from 0
/*Parameters obtained by fitting Experimental virtual Data (Table 2) */
// We used Choise 2 
const double _c   = 1.69;
const double _n1  = 0.84596;
const double _c1  = 0.55469;


/**************************************************/
TComplex Pomeron(double z, double t, double Q_2, double MV, double Mass_pion){ //Eq (9)
	double Qbar_2 = Q_2 + MV*MV; // Eq (8)
	TComplex Pom_Temp0 = TComplex(0,1); // i
	Pom_Temp0 *= VM_Pom_gi(t, Mass_pion, Qbar_2, _g0, _Q0_2, _b00, _b01, _Qb_2); //i*g0
	TComplex t0 = TComplex(0,-1*z); // (-iz)
	double alpha_Pom = 1 + _gamma*(sqrt(4*Mass_pion*Mass_pion) - sqrt(4*Mass_pion*Mass_pion - t)); // alpha_Pom(t)
	t0 = TComplex::Power(t0, alpha_Pom - 1); // (-iz)^(alpha_Pom(t) - 1)
	Pom_Temp0 *= t0; // i*g0 * (-iz)^(alpha_Pom(t) - 1)

	TComplex Pom_Temp1 = TComplex(0,1); // i
	Pom_Temp1 *= VM_Pom_gi(t, Mass_pion, Qbar_2, _g1, _Q1_2, _b10, _b11, _Qb_2); //i*g1
	Pom_Temp1 *= t0; //i*g1 * (-iz)^(aplha - 1)
	t0 = TComplex(0,-1*z); // (-iz)
	t0 = TComplex::Log(t0); // ln(-iz)
	Pom_Temp1 *= t0; //i*g1 * ln(-iz) * (-iz)^(aplha - 1)
	TComplex Pom_Term = Pom_Temp0 + Pom_Temp1;
	Pom_Term *= TMath::Power(MV*MV/Qbar_2, 0.25); //including virtuality Eq (24) ussing choise 2
	return Pom_Term;
}

TComplex Reggeon_f(double z, double t, double Q_2, double MV, double Mass_Nuc){ //Eq (10) for f trayectory
	double Qbar_2 = Q_2 + MV*MV; // Eq (8)
	TComplex Regg_Temp0 = TComplex(0,1); // i
	Regg_Temp0 *= VM_Regg_gr(t, Mass_Nuc, Qbar_2, _gf, _Qr_2, _br, _Qb_2); //i*gf
	TComplex temp = Regg_Temp0;
	TComplex t0 = TComplex(0,-1*z); // (-iz)
	double alpha_Regg = _ReggTraj_alpha_0_f + _ReggTraj_alpha_0_p_f*t; //alpha_Regg(t)
	t0 = TComplex::Power(t0, alpha_Regg - 1); // (-iz)^(alpha_Regg(t) - 1)
	Regg_Temp0 = Regg_Temp0 * t0;// i*gr * (-iz)^(alpha_Regg(t) - 1)
	Regg_Temp0 *= TMath::Power(_c1*MV*MV/(_c1*MV*MV + Q_2), -0.5); //including virtuality Eq (29) ussing choise 2
	return Regg_Temp0; 
}

TComplex Reggeon_pi(double z, double t, double Q_2, double MV, double Mass_Nuc){ //Eq (10) for pi trayectory
	double Qbar_2 = Q_2 + MV*MV; // Eq (8)
	TComplex Regg_Temp0 = TComplex(0,1); // i
	Regg_Temp0 *= VM_Regg_gr(t, Mass_Nuc, Qbar_2, _gpi, _Qr_2, _br, _Qb_2); //i*gf
	TComplex temp = Regg_Temp0;
	TComplex t0 = TComplex(0,-1*z); // (-iz)
	double alpha_Regg = _ReggTraj_alpha_0_pi + _ReggTraj_alpha_0_p_pi*t; //alpha_Regg(t)
	t0 = TComplex::Power(t0, alpha_Regg - 1); // (-iz)^(alpha_Regg(t) - 1)
	Regg_Temp0 = Regg_Temp0 * t0;// i*gr * (-iz)^(alpha_Regg(t) - 1)
	Regg_Temp0 *= TMath::Power(_c1*MV*MV/(_c1*MV*MV + Q_2), -0.5); //including virtuality Eq (29) ussing choise 2
	return Regg_Temp0; 
}
