/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/************************************
OBSERVACIONES
************************************/
void All_TotalCS_Nuc_meson_Generator(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".include base");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();


	TFile file("TotalCS_medium_less_100GeV.root","recreate");
	TNtuple *TN = new TNtuple("TN","Inter meson nucleon","s:Npion_Nomega:Nomega_Npion:Nomega_Nrho:Nrho_Nomega:Nomega_elast:Nomega_N2pion:Npion_Nomega1:Npion_Nphi:Npion_Nrho:Nomega_Npion1:Nphi_Npion:Nrho_Npion:NJPsi_ND:NJPsi_NDast:NJPsi_NDDbar");
/*
	*data << "##################################################################" << endl;
	*data << "# Vector Meson less 150 GeV energies Total Cross Section Database#" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#               CRISP (Colaboração Río-São Paulo)                #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#                 ROOT CINT (C/C++ interpreter)                  #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "# Implementation file: All_TotalCS_ medium Data_less_150GeV.cpp  #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#                      Descripction:                             #" << endl;
	*data << "# Database for interpolation of all vector-meson total amplitude #" << endl;
	*data << "# whit energies less than 150 GeV                                #" << endl;
	*data << "# Database store on file VM_TotalCS_less_150GeV.out              #" << endl;
	*data << "##################################################################" << endl;
*/

	const double _GeV2_mbar = 0.3894;	// Collins pag 3 1GeV⁻² = 0.3894 mb
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double Mphi = CPT::phi_mass/1000;
	const double Msigma = CPT::sigma_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	const double Mphi_2 = Mphi*Mphi;
	const double Msigma_2 = Msigma*Msigma;

	double s_i = 0;
	double s_f = 100.;
	const int Ns = 3000;
	const int Nt = 10000;
	double s_pass = (s_f - s_i)/Ns;
	double s[Ns];

// Npion_Nomega
	double TotalCS_Npion_Nomega [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t, DiffCSinv;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Npion_Nomega[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s[i],t,MN,Mpion_n,MN,Momega);
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PionNeut__ProtOmega(s[i],t,z);
			TotalCS_Npion_Nomega[i] =  TotalCS_Npion_Nomega[i] + DiffCS * t_pass;

		}
	}
// Nomega_Npion
	double TotalCS_Nomega_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_Npion[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s[i],t,MN,Momega,MN,Mpion_n);
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_ProtOmega__PionNeut(s[i],t,z);
			TotalCS_Nomega_Npion[i] =  TotalCS_Nomega_Npion[i] + DiffCS * t_pass;
		}
	}

// Nomega_Nrho
	double TotalCS_Nomega_Nrho [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_Nrho[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NOmega__NRho(s[i],t);
			TotalCS_Nomega_Nrho[i] =  TotalCS_Nomega_Nrho[i] + DiffCS * t_pass;
		}
	}

// Nrho_Nomega
	double TotalCS_Nrho_Nomega [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nrho_Nomega[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NRho__NOmega(s[i],t);
			TotalCS_Nrho_Nomega[i] =  TotalCS_Nrho_Nomega[i] + DiffCS * t_pass;
		}
	}
// Nomega_elastic
	double TotalCS_Nomega_elastic [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Momega_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Momega_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Momega,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Momega_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Momega_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Momega,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_elastic[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NOmega_elastic(s[i],t);
			TotalCS_Nomega_elastic[i] =  TotalCS_Nomega_elastic[i] + DiffCS * t_pass;
		}
	}

// Nomega_N2pion
	double TotalCS_Nomega_N2pion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nomega_N2pion[i] = VM_Total_CrossSection_NOmega_2pionN(s[i],Ns);
	}

// Npion_Nomega
	double TotalCS_Npion_Nomega1 [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nomega1[i] = VM_Total_CrossSection_Npion_Nomega(s[i]);
	}

// Npion_phi
	double TotalCS_Npion_Nphi [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nphi[i] = VM_Total_CrossSection_Npion_Nphi(s[i]);
	}

// Npion_rho
	double TotalCS_Npion_Nrho [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nrho[i] = VM_Total_CrossSection_Npion_Nrho(s[i]);
	}

// Nomega_Npion
	double TotalCS_Nomega_Npion1 [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nomega_Npion1[i] = VM_Total_CrossSection_Nomega_Npion(s[i]);
	}

// Nphi_Npion
	double TotalCS_Nphi_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nphi_Npion[i] = VM_Total_CrossSection_Nphi_Npion(s[i]);
	}

// Nrho_Npion
	double TotalCS_Nrho_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nrho_Npion[i] = VM_Total_CrossSection_Nrho_Npion(s[i]);
	}

	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ncos = 1000;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;

// NJPsi_ND
	double TotalCS_NJPsi_ND[Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypD = 0.;
		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			if (s[i] > 17.3) tcsHypD = tcsHypD + VM_Diff_CrossSection_NJPsi__HypD(s[i], cosTheta)*cos_delta;
		}
		TotalCS_NJPsi_ND[i] = tcsHypD*_GeV2_mbar;
	}

// NJPsi_NDast
	double TotalCS_NJPsi_NDast [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypDast = 0.;
		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			if (s[i] > 18.5) tcsHypDast = tcsHypDast + VM_Diff_CrossSection_NJPsi__HypDast(s[i], cosTheta)*cos_delta;
		}
		TotalCS_NJPsi_NDast[i] = tcsHypDast;
	}

	const Int_t Nt = 100;
	const Int_t Ns1 = 100;
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
// NJPsi_NDDbar
	double TotalCS_NJPsi_NDDbar[Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypDDbar = 0.;
		Double_t s1_max = TMath::Power( TMath::Sqrt(s[i]) - mD, 2);
		Double_t s1_min = TMath::Power(mN + mD,2);
		if (s1_max > s1_min){
			Double_t s1_delta = (s1_max - s1_min)/Ns1;
			for (int j = 0; j < Ns1; j ++){
				double s1 = s1_min + j*s1_delta;
				double tempIntT = 0;
				double t_max = 0.5*(sqrt( Trg_Fun(s[i],mj*mj,mN*mN)*Trg_Fun(s[i],mD*mD,s1) )/s[i] - (mj*mj - mN*mN)*(mD*mD - s1)/s[i] + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s[i] );
				double t_min = 0.5*(-1*sqrt( Trg_Fun(s[i],mj*mj,mN*mN)*Trg_Fun(s[i],mD*mD,s1) )/s[i] - (mj*mj - mN*mN)*(mD*mD - s1)/s[i] + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s[i] );
				if (t_max <= t_min) cout << " t something wrong\n";
				Double_t t_delta = (t_max - t_min)/Nt;
				for (int k = 0; k < Nt; k ++){
					double t = t_min + k*t_delta;
					Double_t temp = VM_Diff_CrossSection_NJPsi__HypDDbar(s[i], t, s1)*t_delta;
					tempIntT = tempIntT + temp;
				}
				tcsHypDDbar = tcsHypDDbar + tempIntT*s1_delta;
			}
		}		
		TotalCS_NJPsi_NDDbar[i] = tcsHypDDbar/_GeV2_mbar;
	}


	for (int i = 0; i < Ns; i++){ //filling database file
		Float_t DATA[16];
		DATA[0] = (Float_t)s[i] ; 
		DATA[1] = (Float_t)TotalCS_Npion_Nomega [i]; 
		DATA[2] = (Float_t)TotalCS_Nomega_Npion[i]; 
		DATA[3] = (Float_t)TotalCS_Nomega_Nrho[i]; 
		DATA[4] = (Float_t)TotalCS_Nrho_Nomega[i]; 
		DATA[5] = (Float_t)TotalCS_Nomega_elastic[i]; 
		DATA[6] = (Float_t)TotalCS_Nomega_N2pion[i]; 
		DATA[7] = (Float_t)TotalCS_Npion_Nomega1[i]; 
		DATA[8] = (Float_t)TotalCS_Npion_Nphi[i];
		DATA[9] = (Float_t)TotalCS_Npion_Nrho[i]; 
		DATA[10] = (Float_t) TotalCS_Nomega_Npion1[i]; 
		DATA[11] = (Float_t)TotalCS_Nphi_Npion [i]; 
		DATA[12] = (Float_t)TotalCS_Nrho_Npion [i];
		DATA[13] = (Float_t)TotalCS_NJPsi_ND [i];
		DATA[14] = (Float_t)TotalCS_NJPsi_NDast [i];
		DATA[15] = (Float_t)TotalCS_NJPsi_NDDbar [i];
		TN->Fill(DATA);
	}
	file.Write();
	file.Close();
}
