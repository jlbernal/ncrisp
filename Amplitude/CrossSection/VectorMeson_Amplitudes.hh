/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Header file VectorMeson_Amplitudes.hh
 ************************************************************************
 * Description:
 * Vector Meson specific elastic process amplitudes calculation
   
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#ifndef __VectorMeson_Amplitudes_pHH
#define __VectorMeson_Amplitudes_pHH

#include "TMath.h"
#include "TComplex.h"
#include "VectorMeson_Ampl_Terms.hh"
#include "CrispParticleTable.hh"
#include "iostream"

TComplex VM_Ampl_ProtonRho(double z, double t, double Q_2);//Eq (10), rho meson case
TComplex VM_Ampl_ProtonPhi(double z, double t, double Q_2);//Eq (10), phi meson case
TComplex VM_Ampl_ProtonOmega(double z, double t, double Q_2);//Eq (10), omega meson case
TComplex VM_Ampl_ProtonJ_Psi(double z, double t, double Q_2);//Eq (10), J/Psi meson case
#endif
