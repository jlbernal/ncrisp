/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Implementation file VectorMeson_Amplitudes.cpp
 ************************************************************************
 * Description:
 * Vector Meson specific elastic process amplitudes calculation
   
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#include "VectorMeson_Amplitudes.hh"
// Energy must be in GeV
TComplex VM_Ampl_ProtonRho(double z, double t, double Q_2){//Eq (10), rho meson case
	double MP = CPT::p_mass/1000;
	double MV = CPT::rho_0_mass/1000;
	double MPion = CPT::pion_0_mass/1000;
	TComplex PomAmpl = Pomeron(z,t,Q_2, MV, MPion);
	TComplex ReggAmpl = Reggeon_f(z,t,Q_2, MV, MP); 
//	cout << "Pomeron: " << PomAmpl.Re() << " i*" << PomAmpl.Im() << endl;
//	cout << "Reggeon: " << ReggAmpl.Re() << " i*" << ReggAmpl.Im() << endl;
	TComplex Ampl = PomAmpl + ReggAmpl;
return Ampl;
}

TComplex VM_Ampl_ProtonPhi(double z, double t, double Q_2){//Eq (10), phi meson case
	double MP = CPT::p_mass/1000;
	double MV = CPT::phi_mass/1000;
	double MPion = CPT::pion_0_mass/1000;
	TComplex Ampl = Pomeron(z,t,Q_2, MV, MPion) + Reggeon_f(z,t,Q_2, MV, MP);
return Ampl;
}

TComplex VM_Ampl_ProtonOmega(double z, double t, double Q_2){//Eq (10), omega meson case
	double MP = CPT::p_mass/1000;
	double MV = CPT::omega_mass/1000;
	double MPion = CPT::pion_0_mass/1000;
	TComplex Ampl = Pomeron(z,t,Q_2, MV,MPion) + Reggeon_f(z,t,Q_2, MV, MP) + Reggeon_pi(z,t,Q_2, MV, MP);
return Ampl;
}

TComplex VM_Ampl_ProtonJ_Psi(double z, double t, double Q_2){//Eq (10), J/Psi meson case
	double MV = CPT::J_Psi_mass/1000;
	double MPion = CPT::pion_0_mass/1000;
	TComplex Ampl = Pomeron(z,t,Q_2, MV ,MPion);
return Ampl;
}

