/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Header file VectorMeson_Diff_Cross_Sections.hh
 ************************************************************************
 * Description:
 * Vector Meson specific process cross sections calculation
   
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#ifndef __VectorMeson_Diff_Cross_Sections_pHH
#define __VectorMeson_Diff_Cross_Sections_pHH

#include "TMath.h"
#include "TLorentzVector.h"
#include "TComplex.h"
#include "VectorMeson_Amplitudes.hh"
#include "CrispParticleTable.hh"
#include "iostream"
double VM_Diff_CrossSection_PhoProt__ProtRho(double z, double t, double Q_2);
double VM_Diff_CrossSection_PhoProt__ProtPhi(double z, double t, double Q_2);
double VM_Diff_CrossSection_PhoProt__ProtOmega(double z, double t, double Q_2);
double VM_Diff_CrossSection_PhoProt__ProtJ_Psi(double z, double t, double Q_2);
double Mott_Diff_CrossSection_Sig(int Z, TLorentzVector p, double theta);

// * Article: Eur. Phys. J. A 6, 71–81 (1999)
double VM_Diff_CrossSection_PionNeut__ProtOmega(double s, double t, double Costheta);
double VM_Diff_CrossSection_ProtOmega__PionNeut(double s, double t, double Costheta);
double VM_Diff_CrossSection_NOmega__NRho(double s, double t);
double VM_Diff_CrossSection_NRho__NOmega(double s, double t);
double VM_Diff_CrossSection_NOmega_elastic(double s, double t);
double VM_Total_CrossSection_NOmega_2pionN(double s, int N);
double VM_Total_CrossSection_Nrho_Npion(double s);
double VM_Total_CrossSection_Nomega_Npion(double s);
double VM_Total_CrossSection_Nphi_Npion(double s);
double VM_Total_CrossSection_Npion_Nrho(double s);
double VM_Total_CrossSection_Npion_Nomega(double s);
double VM_Total_CrossSection_Npion_Nphi(double s);

// * Article: Physical Review C 63 044906
double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos);
double VM_Diff_CrossSection_NJPsi__HypDb(double s, double cos);

double VM_Diff_CrossSection_NJPsi__HypD(double s, double cos); // Total diff Cross Section. HypDa + HypDb
double VM_Diff_CrossSection_NJPsi__HypDast(double s, double cos);
double VM_Diff_CrossSection_NJPsi__HypDDbar(Double_t s, Double_t t, Double_t s1);
#endif
