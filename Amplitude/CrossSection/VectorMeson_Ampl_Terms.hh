/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Header file VectorMeson_Ampl_Terms.hh
 ************************************************************************
 * Description:
 * Vector Meson amplitude Terms calculation
   data on crisp_table.txt file
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#ifndef __VectorMeson_Ampl_Terms_pHH
#define __VectorMeson_Ampl_Terms_pHH

#include "TMath.h"
#include "TComplex.h"
#include "VectorMeson_Ampl_Helper.hh"
#include "iostream"

TComplex Pomeron(double z, double t, double Q_2, double MV, double Mass_pion); //Eq (9)
TComplex Reggeon_f(double z, double t, double Q_2, double MV, double Mass_Nuc); //Eq (10)
TComplex Reggeon_pi(double z, double t, double Q_2, double MV, double Mass_Nuc); //Eq (10)
#endif
