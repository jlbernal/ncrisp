/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Implementation file VectorMeson_Diff_Cross_Sections.cpp
 ************************************************************************
 * Description:
 * Vector Meson specific process cross sections calculation
   
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#include "VectorMeson_Diff_Cross_Sections.hh"
const double _NC = 3;
const double _NRho = 1./sqrt(2.);
const double _NOmega = 1./3./sqrt(2.);
const double _NPhi = 1./3.;
const double _NJ_Psi = 2./3.;
double  VM_Diff_CrossSection_PhoProt__ProtRho(double  z, double  t, double  Q_2){
	TComplex Amp = _NC*_NRho*VM_Ampl_ProtonRho(z,t,Q_2);
//	std::cout << "t: " << t << ", z: " << z << ". Amplitud:   "<< Amp.Re() << " +   i "<< Amp.Im() << std::endl;
	double  CS = 4*TMath::Pi()*Amp.Rho2();
	return CS;
}

double  VM_Diff_CrossSection_PhoProt__ProtPhi(double  z, double  t, double  Q_2){
	TComplex Amp = _NC*_NPhi*VM_Ampl_ProtonPhi(z,t,Q_2);
	double  CS = 4*TMath::Pi()*TMath::Power(TComplex::Abs(Amp),2);
	return CS;
}

double  VM_Diff_CrossSection_PhoProt__ProtOmega(double  z, double  t, double  Q_2){
	TComplex Amp = _NC*_NOmega*VM_Ampl_ProtonOmega(z,t,Q_2);
	double  CS = 4*TMath::Pi()*TMath::Power(TComplex::Abs(Amp),2);
	return CS;
}

double  VM_Diff_CrossSection_PhoProt__ProtJ_Psi(double  z, double  t, double  Q_2){
	TComplex Amp = _NC*_NJ_Psi*VM_Ampl_ProtonJ_Psi(z,t,Q_2);
	double  CS = 4*TMath::Pi()*TMath::Power(TComplex::Abs(Amp),2);
	return CS;
}
double Mott_Diff_CrossSection_Sig(int Z, TLorentzVector p, double theta){
	const double alpha = 1./137.035999679;
	double sigma = TMath::Power(alpha,2)/4*Z*Z*(p.E()*p.E() - TMath::Power(p.Vect().Mag()*TMath::Sin(theta/2),2))/TMath::Power(p.Vect().Mag()*TMath::Sin(theta/2),4);
	return sigma;
}
double VM_Diff_CrossSection_PionNeut__ProtOmega(double s, double t, double Costheta){
	const double g_wpp = 11.79;
	const double g_pNN = 3.24;
	const double k = 6.1;
	const double Lamnda = 2.7;
	const double alpha = 0.16;
	const double betha = 2.3;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;

	double f_pNN = k * g_pNN;
	double Fwpp = (Lamnda*Lamnda - Mrho*Mrho)/(Lamnda*Lamnda - t);
	double FpNN = TMath::Exp(betha*t)* TMath::Exp(-1*alpha*s);
	double Sintheta_2 = (1 - Costheta*Costheta);
	double CS = g_wpp*g_wpp/Momega_2/16/Trg_Fun(s,MN_2,Mpion_n_2) * TMath::Power(Fwpp*FpNN/(t - Mrho_2),2);
	CS = CS*( -1*TMath::Power((g_pNN + f_pNN)*Momega,2)*t*Trg_Fun(t,Momega_2,Mpion_n_2)/(4*Momega_2) + (g_pNN*g_pNN - f_pNN*f_pNN*t/(4*MN_2) )*Sintheta_2/8/s*Trg_Fun(s,MN_2,Mpion_n_2)*Trg_Fun(s,MN_2,Momega_2) );
	return CS;
}
double VM_Diff_CrossSection_ProtOmega__PionNeut(double s, double t, double Costheta){
	const double g_wpp = 11.79;
	const double g_pNN = 3.24;
	const double k = 6.1;
	const double Lamnda = 2.7;
	const double alpha = 0.16;
	const double betha = 2.3;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;

	double f_pNN = k * g_pNN;
	double Fwpp = (Lamnda*Lamnda - Mrho*Mrho)/(Lamnda*Lamnda - t);
	double FpNN = TMath::Exp(betha*t)* TMath::Exp(-1*alpha*s);
	double Sintheta_2 = (1 - Costheta*Costheta);

	double CS1 = g_wpp*g_wpp/Momega_2/4/TMath::Pi()/TMath::Pi()/Trg_Fun(s,MN_2,Momega_2);
	double CS2 = TMath::Power(Fwpp*FpNN/(t - Mrho_2),2);
	double CS3 = -1*TMath::Power((g_pNN + f_pNN)*Momega,2)*t*Trg_Fun(t,Momega_2,Mpion_n_2)/(4*Momega_2);
	double CS4 = g_pNN*g_pNN - f_pNN*f_pNN*t/(4*MN_2) ;
	double CS5 = Sintheta_2/8/s*Trg_Fun(s,MN_2,Mpion_n_2)*Trg_Fun(s,MN_2,Momega_2);
	double CS = CS1*CS2*(CS3 + CS4*CS5);
	return CS;
}
double VM_Diff_CrossSection_NOmega__NRho(double s, double t){
	const double g_wpp = 11.79;
	const double g_pNN = 13.59;
	const double Lamnda = 2.7;
	const double Lamnda1 = 1.05;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	double Fwpp = (Lamnda*Lamnda - Mpion_n_2)/(Lamnda*Lamnda - t);
	double FpNN = (Lamnda1*Lamnda1 - Mpion_n_2)/(Lamnda1*Lamnda1 - t);
	double CS = -1*g_pNN*g_pNN*g_wpp*g_wpp *t /Momega_2 * ( TMath::Power(t - Momega_2 - Mrho_2,2) - 4*Momega_2*Mrho_2)/(96*TMath::Pi()*Trg_Fun(s,Momega_2,MN_2) )*TMath::Power(Fwpp*FpNN/(t - Mpion_n_2),2);
	return CS;
}
double VM_Diff_CrossSection_NRho__NOmega(double s, double t){
	const double g_wpp = 11.79;
	const double g_pNN = 13.59;
	const double Lamnda = 2.7;
	const double Lamnda1 = 1.05;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	double Fwpp = (Lamnda*Lamnda - Mpion_n_2)/(Lamnda*Lamnda - t);
	double FpNN = (Lamnda1*Lamnda1 - Mpion_n_2)/(Lamnda1*Lamnda1 - t);
	// detailed balance from NOmega__NRho cross section
	//g_SI_omega/g_SI_rho   = 1/3
	double CS = -1*g_pNN*g_pNN*g_wpp*g_wpp *t /Momega_2/3 * ( TMath::Power(t - Momega_2 - Mrho_2,2) - 4*Momega_2*Mrho_2)/(96*TMath::Pi()*Trg_Fun(s,Mrho_2,MN_2) )*TMath::Power(Fwpp*FpNN/(t - Mpion_n_2),2);
	return CS;
}
double VM_Diff_CrossSection_NOmega_elastic(double s, double t){
	const double g_wsw = 1.76;
	const double g_pNN = 10.54;
	const double Lamnda = 2;
	const double MN = CPT::n_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Msigma = CPT::sigma_mass/1000;
	const double MN_2 = MN*MN;
	const double Momega_2 = Momega*Momega;
	const double Msigma_2 = Msigma*Msigma;
	double Fwsw = (Lamnda*Lamnda - Msigma_2)/(Lamnda*Lamnda - t);
	double FpNN = Fwsw;
	double CS = g_pNN*g_pNN*g_wsw*g_wsw*(4*MN_2 - t )/16/TMath::Pi()/Momega_2/Trg_Fun(s,Momega_2,MN_2)*TMath::Power(Fwsw*FpNN/(t - Msigma_2),2)*(Momega_2*Momega_2 - Momega_2*t/3 + t*t/12);
	return CS;

}
double VM_Total_CrossSection_Nrho_Npion1(double s){   // para evitar a singularidad de VM_Total_CrossSection_NOmega_2pionN del termino "/sqrt(Trg_Fun(s, Mrho_2, MN_2))"
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double a = 0.413;
	const double M = 1.809;
	double CS = TMath::Pi()*TMath::Pi()/3*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 );
	return CS;
}
double VM_Total_CrossSection_NOmega_2pionN(double s, int N){
	const double g_wpp = 11.79;
	const double Lamnda = 2.7;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	double s1_p = TMath::Power(sqrt(s) - Mpion_n,2);
	double s1_m = TMath::Power(MN + Mpion_n,2);
	double s1_pass = (s1_p - s1_m)/N;
	double CS = 0;	
	for (int i = 0; i < N; i++){
		double s1 = s1_m + i* s1_pass;
		double t_p = Momega_2 + Mpion_n_2 - 0.5/s*( (s + Momega_2 - MN_2)*(s + Mpion_n_2 - s1) - sqrt( Trg_Fun(s, Momega_2, MN_2)*Trg_Fun(s, Mpion_n_2, s1) ) );
		double t_m = Momega_2 + Mpion_n_2 - 0.5/s*( (s + Momega_2 - MN_2)*(s + Mpion_n_2 - s1) + sqrt( Trg_Fun(s, Momega_2, MN_2)*Trg_Fun(s, Mpion_n_2, s1) ) );
		double t_pass  = (t_p - t_m)/N;
		double CST = 0;
		for (int j = 0; j < 1000; j++){
			double t = t_m + j* t_pass;
			double Fwpp = (Lamnda*Lamnda - Mrho_2)/(Lamnda*Lamnda - t);
			double temp = TMath::Power(g_wpp*Fwpp/Momega/(t - Mrho_2) ,2)*( TMath::Power(t + Momega_2 - Mpion_n_2,2) - 4*Momega_2*t );
			CST = CST + temp*t_pass;
		}
		double temp1 =  VM_Total_CrossSection_Nrho_Npion1(s1)*CST/32/TMath::Pi()/TMath::Pi()/Trg_Fun(s, Momega_2, MN_2)*s1_pass;
		CS = temp1 + CS;
	}
	return CS;
}
double VM_Total_CrossSection_Nrho_Npion(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double a = 0.413;
	const double M = 1.809;
	double CS = TMath::Pi()*TMath::Pi()/3*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )/sqrt(Trg_Fun(s, Mrho_2, MN_2));
	return CS;
}

double VM_Total_CrossSection_Nomega_Npion(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double a = 0.302;
	const double M = 1.809;
	const double MN = CPT::n_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double MN_2 = MN*MN;
	const double Momega_2 = Momega*Momega;
	double CS = TMath::Pi()*TMath::Pi()*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )/sqrt(Trg_Fun(s, Momega_2, MN_2));
	return CS;
}
double VM_Total_CrossSection_Nphi_Npion(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double a = 5.88e-3;
	const double M = 1.8;
	const double MN = CPT::n_mass/1000;
	const double Mphi = CPT::phi_mass/1000;
	const double MN_2 = MN*MN;
	const double Mphi_2 = Mphi*Mphi;
	double CS = TMath::Pi()*TMath::Pi()*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )/sqrt(Trg_Fun(s, Mphi_2, MN_2));
	return CS;
}
double VM_Total_CrossSection_Npion_Nrho(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double a = 0.413;
	const double M = 1.809;
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	double CS = TMath::Pi()*TMath::Pi()*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )*sqrt(Trg_Fun(s, Mrho_2, MN_2) )/Trg_Fun(s, Mpion_n_2, MN_2);
	return CS;
}
double VM_Total_CrossSection_Npion_Nomega(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double MN = CPT::n_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	const double a = 0.302;
	const double M = 1.809;
	double CS = TMath::Pi()*TMath::Pi()*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )*sqrt(Trg_Fun(s, Momega_2, MN_2))/Trg_Fun(s, Mpion_n_2, MN_2);;
	return CS;
}
double VM_Total_CrossSection_Npion_Nphi(double s){
	const double ganma = 0.99;
	const double ganma_2 = ganma*ganma;
	const double a = 5.88e-3;
	const double M = 1.8;
	const double MN = CPT::n_mass/1000;
	const double Mphi = CPT::phi_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double MN_2 = MN*MN;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	const double Mphi_2 = Mphi*Mphi;
	double CS = TMath::Pi()*TMath::Pi()*a*ganma_2/( TMath::Power(sqrt(s) - M,2) + ganma_2/4 )*sqrt(Trg_Fun(s, Mphi_2, MN_2))/Trg_Fun(s, Mpion_n_2, MN_2);;
	return CS;
}

double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos){
	const Double_t gJDD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_p_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );
	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t Ma_2_0 = 8*gJDD*gJDD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Ma_2_1 = 1./(t - mD*mD) + 1./(mD*mD + mj*mj - t); //intercambio
	Double_t Ma_2_2 = (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Ma_2_3 = TMath::Power( (mj*mj + mD*mD - t)/2.,2 )- mj*mj*mD*mD;
	Double_t Ma_2 = Ma_2_0*TMath::Power(Ma_2_1, 2)*Ma_2_2*Ma_2_3;
	Double_t fluxo_2 = ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s );
	if ( fluxo_2 > 0)
		Ma_2 = Ma_2 * TMath::Sqrt( fluxo_2 );
	else Ma_2 = 0;
	Double_t result =  2*Ma_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
	return result;
}
double VM_Diff_CrossSection_NJPsi__HypDb(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDastNLam =-19.;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );
	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t pj_p = (2*s + t - mj*mj - 2*mN*mN - mD*mD)/2.;
	Double_t pj_q = (mj*mj - mD*mD + t)/2.;
	Double_t p_2 = 2*(mN*mN + mlambda*mlambda )- t;
	Double_t Mb_2_0 = gJDastD*gJDastD*gDastNLam*gDastNLam/(3*mj*mj); 
	Double_t Mb_2_1 = 1./(t - mDast*mDast)/(t - mDast*mDast); //intercambio
	Double_t Mb_2_2 = mj*mj*(p_2*t - TMath::Power( mlambda*mlambda - mN*mN,2) );
	Double_t Mb_2_3 = 2*pj_p*pj_q*( mlambda*mlambda - mN*mN);
	Double_t Mb_2_4 = p_2*pj_q*pj_q + t*pj_p*pj_p;
	Double_t Mb_2_5 = 4*( (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda)* ( mj*mj*t - pj_q*pj_q) ;
	Double_t Mb_2 = Mb_2_0*Mb_2_1 * (Mb_2_2 + Mb_2_3 - Mb_2_4 - Mb_2_5 );
	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Mb_2 = Mb_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mb_2 = 0;
	return 2*Mb_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
}

double VM_Diff_CrossSection_NJPsi__HypD(double s, double cos){
	double diffDa = VM_Diff_CrossSection_NJPsi__HypDa(s,cos);
	double diffDb = VM_Diff_CrossSection_NJPsi__HypDb(s,cos);
	return diffDa + diffDb;
}


double VM_Diff_CrossSection_NJPsi__HypDast(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDNLam = 15.8;
//	const Double_t gDNLam = 14.8;   //These is the real value but result is shorter than the one in the article but same shape.... still doubt
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mDast*mDast,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mDast*mDast - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mDast,mlambda) - s );

	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t Mc_2_0 = 4*gJDastD*gJDastD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Mc_2_1 = 1./(t - mD*mD)/(t - mD*mD); //intercambio
	Double_t Mc_2_2 =(mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Mc_2_3 = TMath::Power( (mj*mj + mDast*mDast - t)/2. ,2) - mj*mj*mDast*mDast;
	Double_t Mc_2 = Mc_2_0 * Mc_2_1 * Mc_2_2 * Mc_2_3;
	if ((( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ))  >0 )
		Mc_2 = Mc_2 * TMath::Sqrt( ( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mc_2 = 0;
	return 2*Mc_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
}


double VM_Diff_CrossSection_NJPsi__HypDDbar(Double_t s, Double_t t, Double_t s1){
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s ) * ( TMath::Power( mN - mj,2) - s )/4/s;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t)/(t - mD*mD) ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t Md_2 = Md_2_0* Md_2_1*Md_2_2;
	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
//	cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Md_2_0 = " << Md_2_0  << ", Md_2_1 = " << Md_2_1  << ", Md_2_2 = " << Md_2_2 << ", sigma = " << SigmaDN << ", Diff CS = " << Md_2 << endl;
	return Md_2*SigmaDN;
}
