/* ================================================================================
 * 
 * 	Copyright 2011 Israel Gonzalez
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

/***********************************************************************
 * g++ (C/C++ interpreter)
 ************************************************************************
 * Header file VectorMeson_Ampl_Helper.hh
 ************************************************************************
 * Description:
 * Helper functions for Vector Meson amplitude calculation
   data on crisp_table.txt file
 ************************************************************************
 * Author                  CRISP Israel Gonzalez
 * Copyright(c) 2002~2011  CRISP
 * Last Modification Date  16/07/2011
 * For the licensing terms see the file README.TXT
 * Article: Physical Review D 67, 074023 (2003)
 * "Photoproduction of vector mesons in the soft dipole Pomeron model" 
 ************************************************************************/
#ifndef __VectorMeson_Ampl_Helper_pHH
#define __VectorMeson_Ampl_Helper_pHH

#include "TMath.h"
#include "iostream"

double Trg_Fun(double x, double y, double z);
double Mand_Sigma(double m1, double m2, double m3, double m4);
double Mand_t(double cos_theta, double s, double m1, double m2, double m3, double m4);
double Mand_cos_theta_s(double s, double t, double m1, double m2, double m3, double m4);
double Mand_cos_theta_t(double s, double t, double m1, double m2, double m3, double m4);
double VM_Mand_z(double s, double Q_2, double t, double Mass_N, double Mass_VM);  //Eq (2) M_N include the future posibility of neutron
double VM_Mand_t(double s, double Q_2, double cos, double Mass_N, double Mass_VM);  //Eq (13) M_N include the future posibility of neutron
double VM_Mand_CosTheta(double s, double Q_2, double t, double Mass_N, double Mass_VM);  //Eq (13) isolando o CosTheta, M_N include the future posibility of neutron
double VM_Pom_bi(double t, double Qbar_2, double Mass_pion, double bi0, double bi1, double Qb_2); // Eq (20)
double VM_Pom_gi(double t, double Mass_pion, double Qbar_2, double gi, double Qi_2, double bi0, double bi1, double Qb_2); //Eq (19)
double VM_Regg_br(double t, double Qbar_2, double br, double Qb_2); // Eq (22)
double VM_Regg_gr(double t, double Mass_N, double Qbar_2, double gr, double Qr_2, double br, double Qb_2); //Eq (21)
#endif
