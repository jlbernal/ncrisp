
Double_t onePionCrossSectionN(Double_t W){
	// in microbarn (GEV)^1/2  
	const Double_t bn1 = 87., bn2 = 65.;
	// the energy in GeV
	Double_t E = W*W/2/(CPT::n_mass/1000) - CPT::n_mass/1000/2;  
	Double_t x = -2. * ( E - 0.139 );      
	return  ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
}
//_____________________________________________________________________________________________										
Double_t onePionCrossSectionP(Double_t W){
	// in microbarn (GEV)^1/2  
	const Double_t bp1 = 91., bp2 = 71.4; 
	// the energy in GeV
	Double_t E = W*W/2/(CPT::p_mass/1000) - CPT::p_mass/1000/2;
	Double_t x = -2. * ( E - 0.139 );
	return ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );  
}
//_____________________________________________________________________________________________										
Double_t residualN_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_PhotonResidN()->Eval(W);
	else{
		const Double_t bn1 = 87., bn2 = 65.;
		Double_t E = W*W/2/(CPT::n_mass/1000) - CPT::n_mass/1000/2;  
		Double_t x = -2. * ( E - 0.139 );      
		CS = ( bn1 + bn2 / TMath::Sqrt(E) ) * ( 1. - exp(x) ); 
		CS = CS - 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) );
		CS = CS - 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) );
		CS = CS - 2.36765 * ( 0.113964*pow(W,0.328556 ) );
		CS = CS - 2.55277 * ( 0.000957289*pow(W,0.753558 ) );
	}
	return  CS; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t residualP_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_PhotonResidP()->Eval(W);
	else{
		const Double_t bp1 = 91., bp2 = 71.4; 
		Double_t E = W*W/2/(CPT::p_mass/1000) - CPT::p_mass/1000/2;  
		Double_t x = -2. * ( E - 0.139 );      
		CS = ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );
		CS = CS - 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) );
		CS = CS - 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) );
		CS = CS - 2.36765 * ( 0.113964*pow(W,0.328556 ) );
		CS = CS - 2.55277 * ( 0.000957289*pow(W,0.753558 ) );
	}
	return  CS; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t rho_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_rho()->Eval(W);
	else 
		CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
	return  CS*1000; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t omega_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_omg()->Eval(W);
	else 
		CS = 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
	return  CS*1000; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t phi_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_phi()->Eval(W);
	else
		CS = 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
	return  CS*1000; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t J_Psi_PhotonCrossSection(Double_t W){
	Double_t CS;
	if (W < 100 )
		CS = CPT::Instance()->GetT_J_Psi()->Eval(W);
	else
		CS = 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
	return  CS*1000; //milibar -> microbar
}
//_____________________________________________________________________________________________										
int AdjTotalUltraHighEnergyTestCPT(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Int_t Num = 9;
	TGraph *T[Num];
	Double_t Wmax = 7; Int_t N = 1000;
	Double_t pass = (Wmax - 0.1)/N;

	for(int i = 0; i < Num; i++)
		T[i] = new TGraph();

	for(int i = 0; i <= N; i++){
		Double_t W0 = 1.15 + i*pass;
		Double_t rho_Photon = rho_PhotonCrossSection(W0);
		T[0]->SetPoint(i, W0, rho_Photon);
		Double_t omega_Photon = omega_PhotonCrossSection(W0);
		T[1]->SetPoint(i, W0, omega_Photon);
		Double_t phi_Photon = phi_PhotonCrossSection(W0);
		T[2]->SetPoint(i, W0, phi_Photon);
		Double_t J_Psi_Photon = J_Psi_PhotonCrossSection(W0);
		T[3]->SetPoint(i, W0, J_Psi_Photon);

		Double_t residualN_Photon = residualN_PhotonCrossSection(W0);
		T[4]->SetPoint(i, W0, residualN_Photon);

		Double_t residualP_Photon = residualP_PhotonCrossSection(W0);
		T[5]->SetPoint(i, W0, residualP_Photon);

		T[6]->SetPoint(i, W0, onePionCrossSectionN(W0));
		T[7]->SetPoint(i, W0, onePionCrossSectionP(W0));
		T[8]->SetPoint(i, W0, rho_Photon + omega_Photon + phi_Photon + J_Psi_Photon + residualN_Photon);
//		cout << "residualN_Photon = " << residualN_Photon  << ", residualP_Photon = " << residualP_Photon << endl;
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();

	TString name[Num] = {"#rho","#omega","#phi","J/#Psi", "residualN", "residualP", "totalN", "totalP", "total"};
	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("Vector Meson's Total Cross Section.");
	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	leg->SetNColumns(2);
	for(int i = 0; i < Num; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(3);
		T[i]->SetLineColor(i+1);
		T[i]->SetLineStyle(1 + i);
		mg->Add(T[i],"L");
		TString nameTemp;
		if (i<4) nameTemp = "#gamma N -> "  + name[i] + " N ";
		else nameTemp = name[i];
		leg->AddEntry(T[i], nameTemp.Data(),"L");
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("W(GeV)");				
	mg->GetYaxis()->SetTitle("#sigma (#mu bar)");				
	mg->GetYaxis()->SetRangeUser(1e-5,2.5e2);
//	mg->GetYaxis()->SetRangeUser(1e-3,1.5e2);
	mg->GetXaxis()->SetLimits(1,7.1);
	leg->Draw("same");
	c->SaveAs("AdjTotalUltraHighEnergyTestCPT.pdf");
//	delete c;
	return 0;
}
