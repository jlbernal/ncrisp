{
	TFile *f = new TFile("Amplitude/Results/Data/VM_TotalCS_less_150GeV.root");
	TTree *t1 = (TTree*)f->Get("TN");
	Float_t Q_2, W, rho, omg, phi, jpsi; //Q_2:W:Rho:Omega:Phi:J_Psi
	t1->SetBranchAddress("Q_2",&Q_2);
	t1->SetBranchAddress("W",&W);
	t1->SetBranchAddress("Rho",&rho);
	t1->SetBranchAddress("Omega",&omg);
	t1->SetBranchAddress("Phi",&phi);
	t1->SetBranchAddress("J_Psi",&jpsi);
	int idx;
	const double PhDQ = 0.5;
	const double PhDW = 149./5000.;
	cout << "entre ooooeee  la virtualidad Q^2 " << endl;
	cin >> Q_2;
	double Q = (int)floor(Q_2/PhDQ + 0.5);
	TGraph *gr =  new TGraph();
	TGraph *gr1 =  new TGraph();
	TGraph *gr2 =  new TGraph();
	TGraph *gr3 =  new TGraph();
	for (int i = 0; i < 5000; i++){
		W = 1 + i*PhDW;
		double NW = (int)floor((W - 1)/PhDW); 
		int idx = Q*5000 + NW;
		t1->GetEntry(idx);
		if (W < 1.745){	rho = 0; omg = 0; }		
		if (W < 2.0132) phi = 0;
		if (W < 4.0396) phi = 0;
		gr->SetPoint(i,W,(double)rho);
		gr1->SetPoint(i,W,(double)omg);
		gr2->SetPoint(i,W,(double)phi);
		gr3->SetPoint(i,W,(double)jpsi);
//		cout <<  Q_2 <<  ",  " << W <<  ",  " << rho <<  ",  " << omg <<  ",  " << phi <<  ",  " << jpsi << endl; //Q_2:W:Rho:Omega:Phi:J_Psi
	}
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->GetYaxis()->SetNdivisions(5);
//	gr->GetYaxis()->SetRangeUser(1,13);
//	gr->GetXaxis()->SetRangeUser(0.1,4);
	gr->GetYaxis()->SetTitle("Photo Cros Section (m bar)");
	gr->GetXaxis()->SetTitle("W (GeV/c)");
	gr->Draw("AC");
	gr1->Draw("same");
	gr2->Draw("same");
	gr3->Draw("same");
	c1->Update();
	gPad->SetLogy();
	gPad->SetLogx();
}
