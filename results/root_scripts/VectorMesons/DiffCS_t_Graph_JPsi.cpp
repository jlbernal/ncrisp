
double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos){
	const Double_t gJDD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );

	
	Double_t Ma_2_0 = 8*gJDD*gJDD*gDNLam*gDNLam/(3*mj*mj);
	
	Double_t Ma_2_1 = 1./(t - mD*mD) + 1./(mD*mD + mj*mj - t); //intercambio
	Double_t Ma_2_2 = (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Ma_2_3 = TMath::Power( (mj*mj + mD*mD - t)/2.,2 )- mj*mj*mD*mD;
	Double_t Ma_2 = Ma_2_0*TMath::Power(Ma_2_1, 2)*Ma_2_2*Ma_2_3;
	Double_t fluxo_2 = ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s );
	if ( fluxo_2 > 0)
		Ma_2 = Ma_2 * TMath::Sqrt( fluxo_2 );
	else Ma_2 = 0;
	cout << "Ma_2_0 = " << Ma_2_0 << ", Ma_2_1 = " << Ma_2_1 << ", Ma_2_2 = " << Ma_2_2 << ", Ma_2_3 = " << Ma_2_3 << ", fluxo " << TMath::Sqrt( fluxo_2) << endl;
	return 2*Ma_2/64/TMath::Pi()/s;
}
double VM_Diff_CrossSection_NJPsi__HypDb(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDastNLam =-19.;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );

	Double_t pj_p = (2*s + t - mj*mj - 2*mN*mN - mD*mD)/2.;
	Double_t pj_q = (mj*mj - mD*mD + t)/2.;
	Double_t p_2 = 2*(mN*mN + mlambda*mlambda )- t;

	Double_t Mb_2_0 = gJDastD*gJDastD*gDastNLam*gDastNLam/(3*mj*mj); 
	Double_t Mb_2_1 = 1./(t - mDast*mDast)/(t - mDast*mDast); //intercambio
	Double_t Mb_2_2 = mj*mj*(p_2*t - TMath::Power( mlambda*mlambda - mN*mN,2) );
	Double_t Mb_2_3 = 2*pj_p*pj_q*( mlambda*mlambda - mN*mN);
	Double_t Mb_2_4 = p_2*pj_q*pj_q + t*pj_p*pj_p;
	Double_t Mb_2_5 = 4*( (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda)* ( mj*mj*t - pj_q*pj_q) ;
	Double_t Mb_2 = Mb_2_0*Mb_2_1 * (Mb_2_2 + Mb_2_3 - Mb_2_4 - Mb_2_5 );
	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Mb_2 = Mb_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mb_2 = 0;
	return 2*Mb_2/64/TMath::Pi()/s;
}
double VM_Diff_CrossSection_NJPsi__HypDast(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mDast*mDast,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mDast*mDast - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mDast,mlambda) - s );

	Double_t Mc_2_0 = 4*gJDastD*gJDastD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Mc_2_1 = 1./(t - mD*mD)/(t - mD*mD); //intercambio
	Double_t Mc_2_2 =(mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Mc_2_3 = TMath::Power( (mj*mj + mDast*mDast - t)/2. ,2) - mj*mj*mDast*mDast;

	Double_t Mc_2 = Mc_2_0 * Mc_2_1 * Mc_2_2 * Mc_2_3;
	if ( ( ( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ))  >0 )
		Mc_2 = Mc_2 * TMath::Sqrt( ( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mc_2 = 0;
	return 2*Mc_2/64/TMath::Pi()/s;
}


double VM_Diff_CrossSection_NJPsi__HypDDbar_s1(Double_t s, Double_t t, Double_t s1){
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;

	Double_t qj_2 = t; // ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
//	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s1 ) * ( TMath::Power( mN - mj,2) - s1 )/4/s1;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
//	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t) ,2)/TMath::Power( t - mD ,2);
	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t) ,2)/TMath::Power( t - mD*mD ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t SigmaDN;
	if ( (( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
	return Md_2_2*SigmaDN;
}
double VM_Diff_CrossSection_NJPsi__HypDDbar(double s, double t){
	const Double_t s1_max = 100;
	const Double_t s1_min = 1;
	const Int_t N = 1000;
	Double_t s1_delta = (s1_max - s1_min)/(Double_t)N;
	Double_t intFunc = 0.;
	for (int i = 0; i < N; i ++){
		double s1 = s1_min + i*s1_delta;
//		cout << "value[" << i << "] (s1 = "<< s1 << ") = " << VM_Diff_CrossSection_NJPsi__HypDDbar_s1(s,t,s1) << endl;
		intFunc = intFunc +  VM_Diff_CrossSection_NJPsi__HypDDbar_s1(s,t,s1)*s1_delta;
	}
	return intFunc;
}

void DiffCS_t_Graph_JPsi(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	const Double_t s = 300;
	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ncos = 100;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;
	double tPoint[Ncos], csHypDa[Ncos], csHypDb[Ncos], csHypDast[Ncos], csHypDDbar[Ncos];
	for (int i = 0; i < Ncos; i ++){
		double t = cos_min + i*cos_delta;
		double csTemp = VM_Diff_CrossSection_NJPsi__HypDa(s, t);
		cout << "CosTheta Cross Section_"<< s << " (cos = " << t <<  ") = " << csTemp << endl;
		tPoint[i] = t;
		csHypDa[i] = csTemp;
/*		csHypDb[i] = VM_Diff_CrossSection_NJPsi__HypDb(s, t);
		csHypDast[i] = VM_Diff_CrossSection_NJPsi__HypDast(s, t);
		csHypDDbar[i] = VM_Diff_CrossSection_NJPsi__HypDDbar(s, t);
*/	}
	TGraph *gHypDa = new TGraph(Ncos,tPoint,csHypDa);
//	TGraph *gHypDb = new TGraph(Ncos,tPoint,csHypDb);
//	TGraph *gHypDast = new TGraph(Ncos,tPoint,csHypDast);
//	TGraph *gHypDDbar = new TGraph(Ncos,tPoint,csHypDDbar);

	TCanvas *c1 = new TCanvas("c1","All Mesons Fit",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gHypDa->Draw("ALP");

/*	TCanvas *c2 = new TCanvas("c2","All Mesons Fit",600,800);
	c2->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c2->SetFrameFillColor(10);
	c2->SetFillColor(10);
	gHypDb->Draw("ALP");

	TCanvas *c3 = new TCanvas("c3","All Mesons Fit",600,800);
	c3->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c3->SetFrameFillColor(10);
	c3->SetFillColor(10);
	gHypDast->Draw("ALP");

	TCanvas *c4 = new TCanvas("c4","All Mesons Fit",600,800);
	c4->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c4->SetFrameFillColor(10);
	c4->SetFillColor(10);
	gHypDDbar->Draw("ALP");
*/}
