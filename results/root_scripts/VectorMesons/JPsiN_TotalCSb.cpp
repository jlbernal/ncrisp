
//double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos){
double VM_Diff_CrossSection_NJPsi__HypDb(double* x, double* par){
	double cos = x[0];
	double s = par[0];
	const Double_t gJDastD = 7.64;
	const Double_t gDastNLam =-19.;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );

	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);

	Double_t pj_p = (2*s + t - mj*mj - 2*mN*mN - mD*mD)/2.;
	Double_t pj_q = (mj*mj - mD*mD + t)/2.;
	Double_t p_2 = 2*(mN*mN + mlambda*mlambda )- t;
	Double_t Mb_2_0 = gJDastD*gJDastD*gDastNLam*gDastNLam/(3*mj*mj); 
	Double_t Mb_2_1 = 1./(t - mDast*mDast)/(t - mDast*mDast); //intercambio
	Double_t Mb_2_2 = mj*mj*(p_2*t - TMath::Power( mlambda*mlambda - mN*mN,2) );
	Double_t Mb_2_3 = 2*pj_p*pj_q*( mlambda*mlambda - mN*mN);
	Double_t Mb_2_4 = p_2*pj_q*pj_q + t*pj_p*pj_p;
	Double_t Mb_2_5 = 4*( (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda)* ( mj*mj*t - pj_q*pj_q) ;
	Double_t Mb_2 = Mb_2_0*Mb_2_1 * (Mb_2_2 + Mb_2_3 - Mb_2_4 - Mb_2_5 );
	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Mb_2 = Mb_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mb_2 = 0;
	return 2*Mb_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
}
void JPsiN_TotalCSb(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Double_t s_max = 10000;
	const Double_t s_min = 17.3;
	const Int_t Ns = 100;
	const Double_t s_delta = (s_max - s_min)/Ns;
	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ncos = 10000;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3

	double sPoint[Ns], csHypDb[Ns];
	for (int i = 0; i < Ns; i ++){
		double s = s_min + i*s_delta;
//		double tcsHypDb = 0.;
		TF1 *f1 = new TF1("f1", VM_Diff_CrossSection_NJPsi__HypDb,-1,1,1);
		f1->SetParameter(0,s);
/*		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			Double_t csTemp = VM_Diff_CrossSection_NJPsi__HypDa(s, cosTheta);
			tcsHypDa = tcsHypDa + csTemp*cos_delta;
		}
*/		sPoint[i] = TMath::Sqrt(s);
		csHypDb[i] = f1->Integral(-1,1)*_GeV2_mbar;
	}
	TGraph *gHypDb = new TGraph(Ns,sPoint,csHypDb);

	TCanvas *c1 = new TCanvas("c1","HypDb",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gHypDb->Draw("ALP");
}
