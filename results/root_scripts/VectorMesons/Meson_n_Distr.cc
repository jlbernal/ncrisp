#include "Meson_n_Distr.hh"

Double_t OmegaN_PionN_AngDist(Double_t *x, Double_t *par){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	Double_t cos = x[0];
	Double_t s = par[0];
	double MVP = CPT::pion_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MN_2 = TMath::Power(MN,2);
	
	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MN,MVO) - s ); //Eq (13)
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_ProtOmega__PionNeut(s,t,cos)*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000; //mbar->micro bar   in a distribution it is not important
}

