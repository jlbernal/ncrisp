/************************************
OBSERVACIONES
1. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	double MVR = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVR_2 = TMath::Power(MVR,2);
	double MN_2 = TMath::Power(MN,2);
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int Ns = 3000;
	double Pomega[Ns], TCS[Ns];
	double Pomega_0 = 0.01; 
	double Pomega_1 = 4;
	double Pomega_pass = (Pomega_1 - Pomega_0)/Ns; 
	for (int i = 0; i < Ns; i++){
		Pomega[i] = Pomega_0 + i* Pomega_pass;
		double s = TMath::Power(MN + sqrt(Pomega[i]*Pomega[i] + MVO_2) ,2) - Pomega[i]*Pomega[i];
		TCS[i] = VM_Total_CrossSection_NOmega_2pionN(s,Ns);
		cout << "s = " << s << ", Pomega[" << i << "] = " << Pomega[i] << ", TCS[" << i << "] = " << TCS[i] << endl;
	}
	// create graph
	TGraph *gr = new TGraph(Ns,Pomega,TCS);
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->GetYaxis()->SetNdivisions(5);
//	gr->GetYaxis()->SetRangeUser(1,13);
//	gr->GetXaxis()->SetRangeUser(0.1,4);
	gr->GetYaxis()->SetTitle("Total Cros Section (m bar)");
	gr->GetXaxis()->SetTitle("P_{#omega} (GeV/c)");
	gr->Draw("AC");
	c1->Update();
	gPad->SetLogy();
	gPad->SetLogx();
	c1->Print("Amplitude/Results/DiffCS/Total_NOmega__2pionN.pdf");
}

