/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado y nunca puede ser menor que -4*m_pion^2 o produce una singularidad en el termino Reggeon;
************************************/
{

	double MV, MP, MV_2, MP_2; // masa en Gev (VERY IMPORTANT)

	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	double Q_2[11]  = {0,0.24,0.47,2,3.5,5.69,7,10.7,13,19.7,27};
	double W_i = 1;
	double W_f = 500.;
	const int NW = 1000;
	const int Nt = 10000;

	double W_pass = (W_f - W_i)/NW;
	double W[NW];

	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);

// Rho Meson
	double TotalCS_Rho  [NW];
	double tempCS;
	MV = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	MP = CPT::p_mass/1000.;
	MV_2 = TMath::Power(MV,2);
	MP_2 = TMath::Power(MP,2);
	for (int m = 0; m < 11; m++){
		for (int i = 0; i < NW; i++){
			W[i] = W_i + i*W_pass;
			double W_2 = TMath::Power(W[i],2);
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2[m],MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2[m] - MV_2- 2*MP_2 ) + (Q_2[m] + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2[m],MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2[m] - MV_2- 2*MP_2 ) + (Q_2[m] + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double z_lim_p = VM_Mand_z(W_2,Q_2[m],t_lim_p,MP,MV);
			double z_lim_m = VM_Mand_z(W_2,Q_2[m],t_lim_m,MP,MV);
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double t_pass = (t_lim_p -t_lim_m)/Nt;
			TotalCS_Rho[i] = 0;
			double DiffCS;			
			double _t;			
			for (int j = 0; j < Nt; j++){
				_t = t_lim_m + j*t_pass;
				double z = VM_Mand_z(W_2,Q_2[m],_t,MP,MV);
				DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,_t,Q_2[m]);
				TotalCS_Rho[i] += DiffCS*t_pass; 
				}
			if (i == (int)NW/2) tempCS = TotalCS_Rho[i];
		}
		gPad->SetLogy();
		gPad->SetLogx();
		TGraph *gr = new TGraph(NW,W,TotalCS_Rho);
		gr->GetYaxis()->SetNdivisions(5);
		gr->GetYaxis()->SetRangeUser(1e-7,0.1);
		gr->GetXaxis()->SetRangeUser(1,400.);
		gr->GetYaxis()->SetTitle("Total Cros Section (mb)");
		gr->GetXaxis()->SetTitle("W (GeV)");
		if (m ==0) gr->Draw("AC");
		else gr->Draw("Csame");
	
		c1->Update();
		TLatex n;
		n.SetTextSizePixels(10);
		TString VirtStr = "%g";
		VirtStr = Form(VirtStr.Data(),Q_2[m]);
		n.DrawLatex(W[(int)NW/2],tempCS,"Q^{2} = " + VirtStr);
	}	
	c1->Print("Amplitude/Results/TotalCS/TotalCS_Rho_Virt_Ex.pdf");
}


