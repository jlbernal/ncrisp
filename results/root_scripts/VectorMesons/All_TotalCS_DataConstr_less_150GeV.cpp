/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado y nunca puede ser menor que -4*m_pion^2 o produce una singularidad en el termino Reggeon;
************************************/
{
 	ofstream* data = new ofstream ("Amplitude/Results/Data/VM_TotalCS_less_150GeV.out");
/*
	*data << "##################################################################" << endl;
	*data << "# Vector Meson less 10 GeV energies Total Cross Section Database #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#               CRISP (Colaboração Río-São Paulo)                #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#                 ROOT CINT (C/C++ interpreter)                  #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#   Implementation file: All_TotalCS_ DataConst_less_10GeV.cpp   #" << endl;
	*data << "#########                                                #########" << endl;
	*data << "#                      Descripction:                             #" << endl;
	*data << "# Database for interpolation of all vector-meson total amplitude #" << endl;
	*data << "# whit energies less than 10 GeV and virtuality Q² range         #" << endl;
	*data << "# between 0 and 35 GeV.                                          #" << endl;
	*data << "# Database store on file VM_TotalCS_less_10GeV.out               #" << endl;
	*data << "##################################################################" << endl;
*/
	const int NQ = 70;
	const int NW = 5000;
	*data << 4 << "   " <<  NQ + 1 <<  "   " <<  NW << endl;
	*data << "#Q_2       W         Rho      Omega      Phi      J_Psi" << endl;


	double MV, MP, MV_2, MP_2; // masa en Gev (VERY IMPORTANT)
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	double Q_2_i  = 0;
	double Q_2_f  = 35;
	double Q_2_pass = (Q_2_f - Q_2_i)/NQ;
	double W_i = 1;
	double W_f = 150.;
	double W_pass = (W_f - W_i)/NW;
	const int Nt = 10000; //for integration

	for (int l = 0; l <= NQ; l++){
		double Q_2 = Q_2_i + l* Q_2_pass;
		double W[NW];
		// Rho Meson
		double TotalCS_Rho  [NW];
		MV = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
		MP = CPT::p_mass/1000.;
		MV_2 = TMath::Power(MV,2);
		MP_2 = TMath::Power(MP,2);
		for (int i = 0; i < NW; i++){
			W[i] = W_i + i*W_pass;
			double W_2 = TMath::Power(W[i],2);
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double z_lim_p = VM_Mand_z(W_2,Q_2,t_lim_p,MP,MV);
			double z_lim_m = VM_Mand_z(W_2,Q_2,t_lim_m,MP,MV);
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double t_pass = (t_lim_p -t_lim_m)/Nt;
			TotalCS_Rho[i] = 0;
			double DiffCS;			
			double _t;			
			for (int j = 0; j < Nt; j++){
				_t = t_lim_m + j*t_pass;
				double z = VM_Mand_z(W_2,Q_2,_t,MP,MV);
				DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,_t,Q_2);
				TotalCS_Rho[i] += DiffCS*t_pass; 
				}
		}
	
	// Omega Meson
	
		double TotalCS_Omega [NW];
		MV = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
		MP = CPT::p_mass/1000.;
		MV_2 = TMath::Power(MV,2);
		MP_2 = TMath::Power(MP,2);
		for (int i = 0; i < NW; i++){
			W[i] = W_i + i*W_pass;
			double W_2 = TMath::Power(W[i],2);
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double z_lim_p = VM_Mand_z(W_2,Q_2,t_lim_p,MP,MV);
			double z_lim_m = VM_Mand_z(W_2,Q_2,t_lim_m,MP,MV);
			double t_pass = (t_lim_p -t_lim_m)/Nt;
			TotalCS_Omega[i] = 0;
			double DiffCS;			
			double _t;			
			for (int j = 0; j < Nt; j++){
				_t = t_lim_m + j*t_pass;
				double z = VM_Mand_z(W_2,Q_2,_t,MP,MV);
				DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z,_t,Q_2);
				TotalCS_Omega[i] += DiffCS*t_pass; 
				}
		}
	
	// Phi Meson
	
		double TotalCS_Phi  [NW];
		MV = CPT::phi_mass/1000.; // masa en Gev (VERY IMPORTANT)
		MP = CPT::p_mass/1000.;
		MV_2 = TMath::Power(MV,2);
		MP_2 = TMath::Power(MP,2);
		for (int i = 0; i < NW; i++){
			W[i] = W_i + i*W_pass;
			double W_2 = TMath::Power(W[i],2);
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq 	(13)
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double z_lim_p = VM_Mand_z(W_2,Q_2,t_lim_p,MP,MV);
			double z_lim_m = VM_Mand_z(W_2,Q_2,t_lim_m,MP,MV);
			double t_pass = (t_lim_p -t_lim_m)/Nt;
			TotalCS_Phi[i] = 0;
			double DiffCS;			
			double _t;			
			for (int j = 0; j < Nt; j++){
				_t = t_lim_m + j*t_pass;
	//			double z = -1*Mand_cos_theta_t(W_2,_t[j],MP,0,MP,MV);
				double z = VM_Mand_z(W_2,Q_2,_t,MP,MV);
				DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtPhi(z,_t,Q_2);
				TotalCS_Phi[i] += DiffCS*t_pass; 
				}
		}
	
	
	// J_Psi Meson
	
		double TotalCS_J_Psi  [NW];
		MV = CPT::J_Psi_mass/1000.; // masa en Gev (VERY IMPORTANT)
		MP = CPT::p_mass/1000.;
		MV_2 = TMath::Power(MV,2);
		MP_2 = TMath::Power(MP,2);
		for (int i = 0; i < NW; i++){
			W[i] = W_i + i*W_pass;
			double W_2 = TMath::Power(W[i],2);
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double z_lim_p = VM_Mand_z(W_2,Q_2,t_lim_p,MP,MV);
			double z_lim_m = VM_Mand_z(W_2,Q_2,t_lim_m,MP,MV);
			double t_pass = (t_lim_p -t_lim_m)/Nt;
			TotalCS_J_Psi[i] = 0;
			double DiffCS;			
			double _t;			
			for (int j = 0; j < Nt; j++){
				_t = t_lim_m + j*t_pass;
				double z = VM_Mand_z(W_2,Q_2,_t,MP,MV);
				DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtJ_Psi(z,_t,Q_2);
				TotalCS_J_Psi[i] += DiffCS*t_pass; 
			}
		}
		for (int i = 0; i < NW; i++){ //filling database
			*data << Q_2 <<   "   " << W[i] << "   " << TotalCS_Rho[i] << "   " << TotalCS_Omega[i] << "   " << TotalCS_Phi[i] << "   " << TotalCS_J_Psi[i] << endl;
		}
	}	
}


