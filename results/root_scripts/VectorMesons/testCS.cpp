Double_t totalCrossSectionN(Double_t W){
	Double_t CS;
	CS = CS + 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
	CS = CS + 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
	CS = CS + 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
	CS = CS + 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
	return CS*1e3;
}

Double_t totalCrossSectionP(Double_t W){
	Double_t CS;
	CS = CS + 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
	CS = CS + 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
	CS = CS + 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
	CS = CS + 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
	return CS*1e3;
}

Double_t onePionCrossSectionN(Double_t W){
	// in microbarn (GEV)^1/2  
	const Double_t bn1 = 87., bn2 = 65.;
	// the energy in GeV
	Double_t omega = W;  
	Double_t x = -2. * ( omega - 0.139 );      
	return  ( bn1 + bn2 / TMath::Sqrt(omega) ) * ( 1. - exp(x) ); 
}
//_____________________________________________________________________________________________										
Double_t onePionCrossSectionP(Double_t W){
	// in microbarn (GEV)^1/2  
	const Double_t bp1 = 91., bp2 = 71.4; 
	// the energy in GeV
	Double_t omega = W;  
	Double_t x = -2. * ( omega - 0.139 );      
	return ( bp1 + bp2 / TMath::Sqrt(omega) ) * ( 1. - exp(x) );  
}
//_____________________________________________________________________________________________										
Double_t residualN_PhotonCrossSection(Double_t W){
	Double_t CS;
	CS = onePionCrossSectionN(W) - totalCrossSectionN(W);
	return  CS; //milibar -> microbar
}
//_____________________________________________________________________________________________										
Double_t residualP_PhotonCrossSection(Double_t W){
	Double_t CS;
	cout << "onePionCrossSectionP(W) = " << onePionCrossSectionP(W) << ", totalCrossSectionP(W) = " << totalCrossSectionP(W) << endl;
	CS = onePionCrossSectionP(W) - totalCrossSectionP(W);
	CS = 100;
	return  CS; //milibar -> microbar
}
void testCS(){
	const Int_t Num = 4;
	TGraph *T[Num];
	for(int i = 0; i < Num; i++){
		T[i] = new TGraph();
	}
	Double_t Emax = 7000; Int_t N = 100000;
	Double_t pass = (Emax - 0.1)/N;
	for(int i = 0; i <= N; i++){
		Double_t E = 1 + i*pass;
/*		Double_t residualN_Photon = residualN_PhotonCrossSection(E);
		T[0]->SetPoint(i, E, residualN_Photon);
		Double_t residualP_Photon = residualP_PhotonCrossSection(E);
		T[1]->SetPoint(i, E, residualP_Photon);
*/
		Double_t residualN_Photon = residualN_PhotonCrossSection(E);
		T[0]->SetPoint(i, E, residualN_Photon);
		Double_t residualP_Photon = residualP_PhotonCrossSection(E);
		T[1]->SetPoint(i, E, residualP_Photon);
		T[2]->SetPoint(i, E, onePionCrossSectionN(E));
		T[3]->SetPoint(i, E, onePionCrossSectionP(E));
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TString name[Num] = {"residualN", "residualP", "totalN", "totalP"};
	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("Vector Meson's Total Cross Section.");
	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	leg->SetNColumns(2);
	for(int i = 0; i < Num; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(3);
		T[i]->SetLineColor(i + 1);
		T[i]->SetLineStyle(1 + i);
		mg->Add(T[i],"L");
		TString  nameTemp = name[i];
		leg->AddEntry(T[i], nameTemp.Data(),"L");
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("W(GeV)");				
	mg->GetYaxis()->SetTitle("#sigma (#mu bar)");				
	mg->GetYaxis()->SetRangeUser(0.5e-2,3e2);
//	mg->GetYaxis()->SetRangeUser(1e-3,1.5e2);
	mg->GetXaxis()->SetRangeUser(0,7000);
	leg->Draw("same");
	c->SaveAs("AdjTotalUltraHighEnergyTestCPT.pdf");

}
