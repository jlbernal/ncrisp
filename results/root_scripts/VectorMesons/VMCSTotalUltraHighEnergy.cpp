{
	const Double_t b_v[4] 		= {11, 		10, 	7, 	4};
	const Double_t X[4]   		= {5, 		0.55, 	0.34, 	0.0015};
	const Double_t epsilon[4]   	= {0.22, 	0.22, 	0.22, 	0.80};
	const Double_t Y[4]  		= {26, 		18, 	0, 	0};
	const Double_t eta[4] 		= {1.23, 	1.92, 	0, 	0};
	const Double_t f_v[4] 		= {2.02, 	23.1, 	13.7, 	10.4};
	TF1 *f = new TF1("f","[0] * ( [1]*pow(x,[2] ) + [3]*pow(x,-1*[4] ) )/1000 ",0,3000);
	TF1 *f1 = new TF1("f","[0] * ( [1]*pow(x,[2] ) )/1000 ",0,3000);
	TGraph **T = new TGraph*[4]; 

	for(int i = 0; i < 2; i++){
		f->SetParameters(b_v[i], X[i], epsilon[i], Y[i], eta[i]);
		T[i] = new TGraph(f);
	}
	for(int i = 2; i < 4; i++){
		f1->SetParameters(b_v[i], X[i], epsilon[i]);
		T[i] = new TGraph(f1);
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
//	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("Vector Meson's Total Cross Section.");
	for(int i = 0; i < 4; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		mg->Add(T[i]);
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("W(GeV)");				
	mg->GetYaxis()->SetTitle("#sigma (mbar)");				
	mg->GetYaxis()->SetRangeUser(1e-3,1);
	mg->GetXaxis()->SetRangeUser(-50,3000);

	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	leg->AddEntry(T[0],"#rho","p");
	leg->AddEntry(T[1],"#omega","p");
	leg->AddEntry(T[2],"#phi","p");
	leg->AddEntry(T[3],"J/#Psi","p");
	leg->Draw("same");
}
