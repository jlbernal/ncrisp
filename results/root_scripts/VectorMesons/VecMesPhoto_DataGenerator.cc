{
	TGraphErrors *PhotoProdRho = new TGraphErrors(127);
	PhotoProdRho->SetName("TGE_Rho");

	PhotoProdRho->SetPoint(0 , 180.50   , 14.800E-03  ) ; PhotoProdRho->SetPointError(0 , 13.50 , 5.700E-03  ) ;     //    DERRICK 94   ZP C63, 391
	PhotoProdRho->SetPoint(1 ,  70.00   , 14.700E-03  ) ; PhotoProdRho->SetPointError(1 , 10.00 , 0.400E-03  ) ;     //    DERRICK 95   ZP C69, 39
	PhotoProdRho->SetPoint(2 , 187.00   , 13.600E-03  ) ; PhotoProdRho->SetPointError(2 ,  0.00 , 0.800E-03  ) ;     //    AID       96   NP B463,3
	PhotoProdRho->SetPoint(3 ,  55.00   ,  9.100E-03  ) ; PhotoProdRho->SetPointError(3 ,  0.00 , 0.900E-03  ) ;     //    AID       96   NP B463,3
	PhotoProdRho->SetPoint(4 ,  55.00   , 10.900E-03  ) ; PhotoProdRho->SetPointError(4 , 10.00 , 0.200E-03  ) ;     //    BREITWEG 99   EPJ C6,603
	PhotoProdRho->SetPoint(5 ,  65.00   , 10.800E-03  ) ; PhotoProdRho->SetPointError(5 , 10.00 , 0.200E-03  ) ;     //    BREITWEG 99   EPJ C6,603
	PhotoProdRho->SetPoint(6 ,  75.00   , 11.400E-03  ) ; PhotoProdRho->SetPointError(6 , 10.00 , 0.300E-03  ) ;     //    BREITWEG 99   EPJ C6,603
	PhotoProdRho->SetPoint(7 ,  90.00   , 11.700E-03  ) ; PhotoProdRho->SetPointError(7 , 10.00 , 0.300E-03  ) ;     //    BREITWEG 99   EPJ C6,603
	PhotoProdRho->SetPoint(8 ,   3.387  , 12.200E-03  ) ; PhotoProdRho->SetPointError(8 ,  0.26 , 0.900E-03  ) ;     //    BODENKAMP 85 NP B255,717
	PhotoProdRho->SetPoint(9 ,   8.158  ,  8.840E-03  ) ; PhotoProdRho->SetPointError(9 ,  0.60 , 0.440E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(10,   8.927  , 10.680E-03  ) ; PhotoProdRho->SetPointError(10,  1.27 , 0.670E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(11,   9.438  ,  9.900E-03  ) ; PhotoProdRho->SetPointError(11,  0.72 , 0.490E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(12,  10.017  ,  9.500E-03  ) ; PhotoProdRho->SetPointError(12,  0.68 , 0.560E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(13,  11.581  ,  9.820E-03  ) ; PhotoProdRho->SetPointError(13,  0.93 , 0.560E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(14,  12.212  ,  8.240E-03  ) ; PhotoProdRho->SetPointError(14,  0.88 , 0.470E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(15,  14.135  ,  9.220E-03  ) ; PhotoProdRho->SetPointError(15,  1.10 , 0.520E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(16,  14.847  ,  8.590E-03  ) ; PhotoProdRho->SetPointError(16,  1.12 , 0.490E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(17,  17.190  ,  9.750E-03  ) ; PhotoProdRho->SetPointError(17,  1.31 , 0.560E-03  ) ;     //    EGLOFF  79   PRL 43, 657
	PhotoProdRho->SetPoint(18,   5.807  , 10.900E-03  ) ; PhotoProdRho->SetPointError(18,  0.42 , 1.100E-03  ) ;     //    ALEKSANDROV 80YF 32, 651
	PhotoProdRho->SetPoint(19,   6.565  , 11.000E-03  ) ; PhotoProdRho->SetPointError(19,  0.37 , 1.100E-03  ) ;     //    ALEKSANDROV 80YF 32, 651
	PhotoProdRho->SetPoint(20,   7.245  , 10.200E-03  ) ; PhotoProdRho->SetPointError(20,  0.33 , 1.100E-03  ) ;     //    ALEKSANDROV 80YF 32, 651
	PhotoProdRho->SetPoint(21,   3.247  , 18.100E-03  ) ; PhotoProdRho->SetPointError(21,  0.32 , 1.500E-03  ) ;     //    STRUCZIN72   NP B47, 436
	PhotoProdRho->SetPoint(22,   3.247  , 15.200E-03  ) ; PhotoProdRho->SetPointError(22,  0.32 , 1.400E-03  ) ;     //    STRUCZIN72   NP B47, 436
	PhotoProdRho->SetPoint(23,   3.247  , 17.000E-03  ) ; PhotoProdRho->SetPointError(23,  0.32 , 2.000E-03  ) ;     //    STRUCZIN72   NP B47, 436
	PhotoProdRho->SetPoint(24,   2.041  , 21.800E-03  ) ; PhotoProdRho->SetPointError(24,  0.07 , 1.400E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(25,   2.153  , 22.100E-03  ) ; PhotoProdRho->SetPointError(25,  0.04 , 1.700E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(26,   2.238  , 22.200E-03  ) ; PhotoProdRho->SetPointError(26,  0.04 , 1.600E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(27,   2.340  , 19.700E-03  ) ; PhotoProdRho->SetPointError(27,  0.06 , 1.100E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(28,   2.524  , 18.500E-03  ) ; PhotoProdRho->SetPointError(28,  0.12 , 1.100E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(29,   2.772  , 17.700E-03  ) ; PhotoProdRho->SetPointError(29,  0.13 , 1.000E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(30,   3.054  , 18.900E-03  ) ; PhotoProdRho->SetPointError(30,  0.16 , 2.000E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(31,   3.204  , 15.200E-03  ) ; PhotoProdRho->SetPointError(31,  0.31 , 1.400E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(32,   3.389  , 17.400E-03  ) ; PhotoProdRho->SetPointError(32,  0.19 , 2.000E-03  ) ;     //    STRUCZIN75   NP B108, 45
	PhotoProdRho->SetPoint(33,   1.796  , 12.100E-03  ) ; PhotoProdRho->SetPointError(33,  0.08 , 1.000E-03  ) ;     //    CROUCH 64    PRL 13, 640
	PhotoProdRho->SetPoint(34,   1.970  , 17.700E-03  ) ; PhotoProdRho->SetPointError(34,  0.10 , 1.000E-03  ) ;     //    CROUCH 64    PRL 13, 640
	PhotoProdRho->SetPoint(35,   2.659  , 19.600E-03  ) ; PhotoProdRho->SetPointError(35,  0.60 , 1.000E-03  ) ;     //    CROUCH 64    PRL 13, 640
	PhotoProdRho->SetPoint(36,   2.944  , 17.000E-03  ) ; PhotoProdRho->SetPointError(36,  0.58 , 0.700E-03  ) ;     //    ERBE 66      NC A46, 795
	PhotoProdRho->SetPoint(37,   1.770  , 21.100E-03  ) ; PhotoProdRho->SetPointError(37,  0.00 , 5.000E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(38,   1.873  , 29.800E-03  ) ; PhotoProdRho->SetPointError(38,  0.00 , 7.100E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(39,   1.994  , 37.500E-03  ) ; PhotoProdRho->SetPointError(39,  0.00 , 5.000E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(40,   2.217  , 33.200E-03  ) ; PhotoProdRho->SetPointError(40,  0.00 , 4.100E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(41,   2.551  , 22.400E-03  ) ; PhotoProdRho->SetPointError(41,  0.00 , 3.400E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(42,   3.130  , 20.900E-03  ) ; PhotoProdRho->SetPointError(42,  0.00 , 2.500E-03  ) ;     //    CROUCH 66    PR 146, 994
	PhotoProdRho->SetPoint(43,   2.912  , 14.600E-03  ) ; PhotoProdRho->SetPointError(43,  0.29 , 1.800E-03  ) ;     //    BLECHSCH67  NC A52, 1348
	PhotoProdRho->SetPoint(44,   2.551  , 17.800E-03  ) ; PhotoProdRho->SetPointError(44,  0.20 , 2.000E-03  ) ;     //    ERBE 68       PL 27B, 54
	PhotoProdRho->SetPoint(45,   2.896  , 16.400E-03  ) ; PhotoProdRho->SetPointError(45,  0.17 , 2.000E-03  ) ;     //    ERBE 68       PL 27B, 54
	PhotoProdRho->SetPoint(46,   3.247  , 16.000E-03  ) ; PhotoProdRho->SetPointError(46,  0.19 , 2.000E-03  ) ;     //    ERBE 68       PL 27B, 54
	PhotoProdRho->SetPoint(47,   1.743  , 11.200E-03  ) ; PhotoProdRho->SetPointError(47,  0.00 , 2.200E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(48,   1.796  , 14.400E-03  ) ; PhotoProdRho->SetPointError(48,  0.00 , 2.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(49,   1.848  , 18.300E-03  ) ; PhotoProdRho->SetPointError(49,  0.00 , 2.700E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(50,   1.922  , 22.000E-03  ) ; PhotoProdRho->SetPointError(50,  0.00 , 4.700E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(51,   1.970  , 17.900E-03  ) ; PhotoProdRho->SetPointError(51,  0.00 , 4.400E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(52,   2.018  , 25.100E-03  ) ; PhotoProdRho->SetPointError(52,  0.00 , 4.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(53,   2.109  , 21.500E-03  ) ; PhotoProdRho->SetPointError(53,  0.00 , 3.000E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(54,   2.196  , 18.000E-03  ) ; PhotoProdRho->SetPointError(54,  0.00 , 2.700E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(55,   2.217  , 20.600E-03  ) ; PhotoProdRho->SetPointError(55,  0.00 , 3.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(56,   2.280  , 15.500E-03  ) ; PhotoProdRho->SetPointError(56,  0.00 , 2.000E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(57,   2.360  , 16.800E-03  ) ; PhotoProdRho->SetPointError(57,  0.00 , 2.000E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(58,   2.439  , 19.600E-03  ) ; PhotoProdRho->SetPointError(58,  0.00 , 2.100E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(59,   2.514  , 18.400E-03  ) ; PhotoProdRho->SetPointError(59,  0.00 , 2.000E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(60,   2.551  , 19.500E-03  ) ; PhotoProdRho->SetPointError(60,  0.00 , 1.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(61,   2.642  , 16.500E-03  ) ; PhotoProdRho->SetPointError(61,  0.00 , 1.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(62,   2.814  , 16.600E-03  ) ; PhotoProdRho->SetPointError(62,  0.00 , 1.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(63,   2.896  , 17.500E-03  ) ; PhotoProdRho->SetPointError(63,  0.00 , 3.700E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(64,   2.976  , 16.000E-03  ) ; PhotoProdRho->SetPointError(64,  0.00 , 1.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(65,   3.130  , 16.900E-03  ) ; PhotoProdRho->SetPointError(65,  0.00 , 1.400E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(66,   3.247  , 15.900E-03  ) ; PhotoProdRho->SetPointError(66,  0.00 , 3.300E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(67,   3.319  , 15.000E-03  ) ; PhotoProdRho->SetPointError(67,  0.00 , 2.200E-03  ) ;     //    ERBE 68     PR 175, 1669
	PhotoProdRho->SetPoint(68,   3.262  , 16.000E-03  ) ; PhotoProdRho->SetPointError(68,  0.09 , 2.500E-03  ) ;     //    BALLAM 68   PRL 21, 1541
	PhotoProdRho->SetPoint(69,   3.867  , 14.000E-03  ) ; PhotoProdRho->SetPointError(69,  0.10 , 2.500E-03  ) ;     //    BALLAM 68   PRL 21, 1541
	PhotoProdRho->SetPoint(70,   2.551  , 20.100E-03  ) ; PhotoProdRho->SetPointError(70,  0.40 , 2.300E-03  ) ;     //    DAVIER 68    PRL 21, 841
	PhotoProdRho->SetPoint(71,   3.484  , 18.900E-03  ) ; PhotoProdRho->SetPointError(71,  0.59 , 2.400E-03  ) ;     //    DAVIER 68    PRL 21, 841
	PhotoProdRho->SetPoint(72,   4.837  , 13.800E-03  ) ; PhotoProdRho->SetPointError(72,  0.85 , 5.200E-03  ) ;     //    DAVIER 68    PRL 21, 841
	PhotoProdRho->SetPoint(73,   2.551  , 19.200E-03  ) ; PhotoProdRho->SetPointError(73,  0.40 , 2.300E-03  ) ;     //    DAVIER 69    PRL D1, 790
	PhotoProdRho->SetPoint(74,   3.484  , 15.600E-03  ) ; PhotoProdRho->SetPointError(74,  0.59 , 1.700E-03  ) ;     //    DAVIER 69    PRL D1, 790
	PhotoProdRho->SetPoint(75,   4.837  , 13.400E-03  ) ; PhotoProdRho->SetPointError(75,  0.85 , 3.600E-03  ) ;     //    DAVIER 69    PRL D1, 790
	PhotoProdRho->SetPoint(76,   2.992  , 19.200E-03  ) ; PhotoProdRho->SetPointError(76,  0.00 , 1.200E-03  ) ;     //    EISENBERG 69 PRL 22, 669
	PhotoProdRho->SetPoint(77,   3.347  , 19.800E-03  ) ; PhotoProdRho->SetPointError(77,  0.00 , 2.200E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(78,   3.484  , 18.000E-03  ) ; PhotoProdRho->SetPointError(78,  0.00 , 1.800E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(79,   3.616  , 16.300E-03  ) ; PhotoProdRho->SetPointError(79,  0.00 , 1.600E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(80,   4.739  , 13.400E-03  ) ; PhotoProdRho->SetPointError(80,  0.00 , 0.800E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(81,   4.885  , 13.200E-03  ) ; PhotoProdRho->SetPointError(81,  0.00 , 0.800E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(82,   5.027  , 12.900E-03  ) ; PhotoProdRho->SetPointError(82,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(83,   5.166  , 13.000E-03  ) ; PhotoProdRho->SetPointError(83,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(84,   5.300  , 12.000E-03  ) ; PhotoProdRho->SetPointError(84,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(85,   5.431  , 12.500E-03  ) ; PhotoProdRho->SetPointError(85,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(86,   5.559  , 12.200E-03  ) ; PhotoProdRho->SetPointError(86,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(87,   5.709  , 12.500E-03  ) ; PhotoProdRho->SetPointError(87,  0.00 , 0.700E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(88,   5.855  , 11.900E-03  ) ; PhotoProdRho->SetPointError(88,  0.00 , 0.600E-03  ) ;     //    ANDERSON 70    PR D1, 27
	PhotoProdRho->SetPoint(89,   2.477  , 16.400E-03  ) ; PhotoProdRho->SetPointError(89,  0.00 , 1.000E-03  ) ;     //    BINGHAM 70   PRL 24, 955
	PhotoProdRho->SetPoint(90,   3.114  , 16.400E-03  ) ; PhotoProdRho->SetPointError(90,  0.00 , 1.000E-03  ) ;     //    BINGHAM 70   PRL 24, 955
	PhotoProdRho->SetPoint(91,   3.551  , 12.300E-03  ) ; PhotoProdRho->SetPointError(91,  0.20 , 0.900E-03  ) ;     //    PARK 72      NP B36, 404
	PhotoProdRho->SetPoint(92,   3.744  , 12.100E-03  ) ; PhotoProdRho->SetPointError(92,  0.54 , 1.100E-03  ) ;     //    PARK 72      NP B36, 404
	PhotoProdRho->SetPoint(93,   4.537  , 11.700E-03  ) ; PhotoProdRho->SetPointError(93,  0.32 , 1.000E-03  ) ;     //    PARK 72      NP B36, 404
	PhotoProdRho->SetPoint(94,   5.388  , 11.600E-03  ) ; PhotoProdRho->SetPointError(94,  0.55 , 1.000E-03  ) ;     //    PARK 72      NP B36, 404
	PhotoProdRho->SetPoint(95,   2.477  , 21.000E-03  ) ; PhotoProdRho->SetPointError(95,  0.00 , 1.000E-03  ) ;     //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(96,   3.115  , 16.200E-03  ) ; PhotoProdRho->SetPointError(96,  0.00 , 0.700E-03  ) ;     //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(97,   2.477  , 18.600E-03  ) ; PhotoProdRho->SetPointError(97,  0.00 , 1.100E-03  ) ;     //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(98,   3.115  , 14.500E-03  ) ; PhotoProdRho->SetPointError(98,  0.00 , 1.000E-03  ) ;     //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(99,   2.477  , 18.600E-03  ) ; PhotoProdRho->SetPointError(99,  0.00 , 1.000E-03  ) ;   //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(100,   3.115  , 15.900E-03  ) ; PhotoProdRho->SetPointError(100,  0.00 , 0.700E-03  ) ;   //    BALLAM   72   PR D5, 545
	PhotoProdRho->SetPoint(101,   2.477  , 21.000E-03  ) ; PhotoProdRho->SetPointError(101,  0.00 , 1.000E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(102,   3.115  , 16.200E-03  ) ; PhotoProdRho->SetPointError(102,  0.00 , 0.700E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(103,   4.281  , 13.300E-03  ) ; PhotoProdRho->SetPointError(103,  0.00 , 0.500E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(104,   2.477  , 23.500E-03  ) ; PhotoProdRho->SetPointError(104,  0.00 , 2.400E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(105,   3.115  , 18.200E-03  ) ; PhotoProdRho->SetPointError(105,  0.00 , 1.600E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(106,   4.281  , 14.000E-03  ) ; PhotoProdRho->SetPointError(106,  0.00 , 0.900E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(107,   2.477  , 18.600E-03  ) ; PhotoProdRho->SetPointError(107,  0.00 , 1.100E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(108,   3.115  , 14.500E-03  ) ; PhotoProdRho->SetPointError(108,  0.00 , 1.000E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(109,   4.281  , 11.800E-03  ) ; PhotoProdRho->SetPointError(109,  0.00 , 0.500E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(110,   2.477  , 18.600E-03  ) ; PhotoProdRho->SetPointError(110,  0.00 , 1.000E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(111,   3.115  , 15.900E-03  ) ; PhotoProdRho->SetPointError(111,  0.00 , 0.700E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(112,   4.281  , 13.500E-03  ) ; PhotoProdRho->SetPointError(112,  0.00 , 0.500E-03  ) ;   //    BALLAM 73    PR D7, 3150
	PhotoProdRho->SetPoint(113,   2.992  , 15.900E-03  ) ; PhotoProdRho->SetPointError(113,  0.26 , 1.9000E-03 ) ;   //    EISENBERG 72 NP B42, 349
	PhotoProdRho->SetPoint(114,   2.259  , 22.100E-03  ) ; PhotoProdRho->SetPointError(114,  0.11 , 1.4000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(115,   2.458  , 21.400E-03  ) ; PhotoProdRho->SetPointError(115,  0.10 , 1.6000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(116,   2.677  , 18.700E-03  ) ; PhotoProdRho->SetPointError(116,  0.12 , 1.6000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(117,   2.960  , 16.200E-03  ) ; PhotoProdRho->SetPointError(117,  0.16 , 1.7000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(118,   3.276  , 15.400E-03  ) ; PhotoProdRho->SetPointError(118,  0.16 , 1.4000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(119,   3.867  , 13.700E-03  ) ; PhotoProdRho->SetPointError(119,  0.17 , 1.3000E-03 ) ;   //   EISENBERG 72   PR D5, 15
	PhotoProdRho->SetPoint(120,   9.237  ,  9.400E-03  ) ; PhotoProdRho->SetPointError(120,  3.04 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(121,   6.913  ,  8.500E-03  ) ; PhotoProdRho->SetPointError(121,  0.72 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(122,   8.441  ,  9.700E-03  ) ; PhotoProdRho->SetPointError(122,  0.88 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(123,   9.237  ,  9.200E-03  ) ; PhotoProdRho->SetPointError(123,  3.04 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(124,   6.913  ,  8.800E-03  ) ; PhotoProdRho->SetPointError(124,  0.72 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(125,   8.441  ,  9.600E-03  ) ; PhotoProdRho->SetPointError(125,  0.88 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56
	PhotoProdRho->SetPoint(126,  10.430  ,  7.500E-03  ) ; PhotoProdRho->SetPointError(126,  1.19 , 0.100E-03  ) ;   //   ASTON 82     NP B209, 56

	TGraphErrors *PhotoProdOmg = new TGraphErrors(57);
	PhotoProdOmg->SetName("TGE_Omg");
	PhotoProdOmg->SetPoint(0  ,  80.00    , 1.210E-03  ) ; PhotoProdOmg->SetPointError(0 , 10.00   , 0.120E-03  ) ;     // BREITWEG 00  DESY-00-084 
	PhotoProdOmg->SetPoint(1  ,  11.702   , 0.910E-03  ) ; PhotoProdOmg->SetPointError(1 ,  1.05   , 0.240E-03  ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdOmg->SetPoint(2  ,  13.559   , 0.950E-03  ) ; PhotoProdOmg->SetPointError(2 ,  0.90   , 0.220E-03  ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdOmg->SetPoint(3  ,  17.754   , 0.940E-03  ) ; PhotoProdOmg->SetPointError(3 ,  3.36   , 0.190E-03  ) ;     // BUSENITZ 89    PR D40, 1
	PhotoProdOmg->SetPoint(4  ,   2.606   , 4.200E-03  ) ; PhotoProdOmg->SetPointError(4 ,  0.13   , 0.300E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(5  ,   2.847   , 2.500E-03  ) ; PhotoProdOmg->SetPointError(5 ,  0.12   , 0.100E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(6  ,   3.054   , 2.200E-03  ) ; PhotoProdOmg->SetPointError(6 ,  0.10   , 0.100E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(7  ,   2.606   , 4.730E-03  ) ; PhotoProdOmg->SetPointError(7 ,  0.13   , 0.338E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(8  ,   2.847   , 2.820E-03  ) ; PhotoProdOmg->SetPointError(8 ,  0.12   , 0.113E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(9  ,   3.054   , 2.480E-03  ) ; PhotoProdOmg->SetPointError(9 ,  0.10   , 0.113E-03  ) ;     // BARBER 84     ZP C26,343 
	PhotoProdOmg->SetPoint(10 ,   2.944   , 3.800E-03  ) ; PhotoProdOmg->SetPointError(10,  0.58   , 0.500E-03  ) ;     // ERBE 66      NC A46, 795
	PhotoProdOmg->SetPoint(11 ,   1.825   , 7.300E-03  ) ; PhotoProdOmg->SetPointError(11,  0.11   , 1.600E-03  ) ;     // CROUCH 67   PR 155, 1468
	PhotoProdOmg->SetPoint(12 ,   1.994   , 6.300E-03  ) ; PhotoProdOmg->SetPointError(12,  0.08   , 1.900E-03  ) ;     // CROUCH 67   PR 155, 1468
	PhotoProdOmg->SetPoint(13 ,   2.217   , 5.500E-03  ) ; PhotoProdOmg->SetPointError(13,  0.15   , 1.000E-03  ) ;     // CROUCH 67   PR 155, 1468
	PhotoProdOmg->SetPoint(14 ,   2.976   , 3.200E-03  ) ; PhotoProdOmg->SetPointError(14,  0.62   , 0.600E-03  ) ;     // CROUCH 67   PR 155, 1468
	PhotoProdOmg->SetPoint(15 ,   8.606   , 1.010E-03  ) ; PhotoProdOmg->SetPointError(15,  2.41   , 0.015E-03  ) ;     // ATKINSON 84  NP B231, 15 
	PhotoProdOmg->SetPoint(16 ,   7.866   , 1.160E-03  ) ; PhotoProdOmg->SetPointError(16,  1.67   , 0.113E-03  ) ;     // ASTON 82     NP B209, 56
	PhotoProdOmg->SetPoint(17 ,  10.017   , 1.170E-03  ) ; PhotoProdOmg->SetPointError(17,  0.68   , 0.180E-03  ) ;     // EGLOFF 79   PRL 43, 1545 
	PhotoProdOmg->SetPoint(18 ,  11.581   , 1.040E-03  ) ; PhotoProdOmg->SetPointError(18,  0.93   , 0.160E-03  ) ;     // EGLOFF 79   PRL 43, 1545 
	PhotoProdOmg->SetPoint(19 ,  13.385   , 1.110E-03  ) ; PhotoProdOmg->SetPointError(19,  2.05   , 0.130E-03  ) ;     // EGLOFF 79   PRL 43, 1545 
	PhotoProdOmg->SetPoint(20 ,  16.236   , 1.080E-03  ) ; PhotoProdOmg->SetPointError(20,  2.50   , 0.180E-03  ) ;     // EGLOFF 79   PRL 43, 1545 
	PhotoProdOmg->SetPoint(21 ,   1.850   , 7.000E-03  ) ; PhotoProdOmg->SetPointError(21,  0.15   , 0.800E-03  ) ;     // JOOS 77     NP B122, 365
	PhotoProdOmg->SetPoint(22 ,   2.090   , 7.400E-03  ) ; PhotoProdOmg->SetPointError(22,  0.09   , 1.000E-03  ) ;     // JOOS 77     NP B122, 365
	PhotoProdOmg->SetPoint(23 ,   2.430   , 5.400E-03  ) ; PhotoProdOmg->SetPointError(23,  0.23   , 0.600E-03  ) ;     // JOOS 77     NP B122, 365
	PhotoProdOmg->SetPoint(24 ,   2.086   , 7.600E-03  ) ; PhotoProdOmg->SetPointError(24,  0.12   , 1.500E-03  ) ;     // STRUCZIN75   NP B108, 45 
	PhotoProdOmg->SetPoint(25 ,   2.300   , 5.300E-03  ) ; PhotoProdOmg->SetPointError(25,  0.10   , 0.500E-03  ) ;     // STRUCZIN75   NP B108, 45 
	PhotoProdOmg->SetPoint(26 ,   2.524   , 3.900E-03  ) ; PhotoProdOmg->SetPointError(26,  0.12   , 0.300E-03  ) ;     // STRUCZIN75   NP B108, 45 
	PhotoProdOmg->SetPoint(27 ,   3.054   , 2.700E-03  ) ; PhotoProdOmg->SetPointError(27,  0.16   , 0.500E-03  ) ;     // STRUCZIN75   NP B108, 45 
	PhotoProdOmg->SetPoint(28 ,   3.389   , 2.000E-03  ) ; PhotoProdOmg->SetPointError(28,  0.19   , 0.500E-03  ) ;     // STRUCZIN75   NP B108, 45
	PhotoProdOmg->SetPoint(29 ,   2.944   , 3.800E-03  ) ; PhotoProdOmg->SetPointError(29,  0.58   , 0.500E-03  ) ;     // ERBE 66      NC A46, 795
	PhotoProdOmg->SetPoint(30 ,   2.551   , 5.410E-03  ) ; PhotoProdOmg->SetPointError(30,  0.19   , 0.901E-03  ) ;     // ERBE 68       PL 27B, 54
	PhotoProdOmg->SetPoint(31 ,   2.896   , 4.050E-03  ) ; PhotoProdOmg->SetPointError(31,  0.17   , 0.676E-03  ) ;     // ERBE 68       PL 27B, 54
	PhotoProdOmg->SetPoint(32 ,   3.247   , 3.490E-03  ) ; PhotoProdOmg->SetPointError(32,  0.19   , 0.788E-03  ) ;     // ERBE 68       PL 27B, 54
	PhotoProdOmg->SetPoint(33 ,   1.799   , 6.790E-03  ) ; PhotoProdOmg->SetPointError(33,  0.00   , 1.110E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(34 ,   1.970   , 7.600E-03  ) ; PhotoProdOmg->SetPointError(34,  0.00   , 1.010E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(35 ,   2.131   , 7.500E-03  ) ; PhotoProdOmg->SetPointError(35,  0.00   , 1.220E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(36 ,   2.217   , 5.980E-03  ) ; PhotoProdOmg->SetPointError(36,  0.00   , 2.430E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(37 ,   2.280   , 6.890E-03  ) ; PhotoProdOmg->SetPointError(37,  0.00   , 1.010E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(38 ,   2.458   , 5.570E-03  ) ; PhotoProdOmg->SetPointError(38,  0.00   , 1.010E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(39 ,   2.642   , 4.260E-03  ) ; PhotoProdOmg->SetPointError(39,  0.00   , 0.912E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(40 ,   2.896   , 3.650E-03  ) ; PhotoProdOmg->SetPointError(40,  0.00   , 0.608E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(41 ,   2.944   , 3.550E-03  ) ; PhotoProdOmg->SetPointError(41,  0.00   , 1.220E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(42 ,   3.247   , 3.240E-03  ) ; PhotoProdOmg->SetPointError(42,  0.00   , 0.709E-03  ) ;     // ERBE 68     PR 175, 1669
	PhotoProdOmg->SetPoint(43 ,   3.276   , 2.000E-03  ) ; PhotoProdOmg->SetPointError(43,  0.00   , 0.500E-03  ) ;     // BALLAM 69    PL 30B, 421
	PhotoProdOmg->SetPoint(44 ,   2.729   , 3.950E-03  ) ; PhotoProdOmg->SetPointError(44,  0.58   , 0.912E-03  ) ;     // DAVIER 69     PR D1, 790
	PhotoProdOmg->SetPoint(45 ,   4.537   , 2.640E-03  ) ; PhotoProdOmg->SetPointError(45,  1.33   , 0.709E-03  ) ;     // DAVIER 69     PR D1, 790
	PhotoProdOmg->SetPoint(46 ,   2.992   , 2.840E-03  ) ; PhotoProdOmg->SetPointError(46,  0.00   , 0.507E-03  ) ;     // EISENBERG 69 PRL 22, 669
	PhotoProdOmg->SetPoint(47 ,   2.992   , 2.900E-03  ) ; PhotoProdOmg->SetPointError(47,  0.20   , 0.400E-03  ) ;     // EISENBERG 72   PR D5, 15
	PhotoProdOmg->SetPoint(48 ,   3.276   , 2.300E-03  ) ; PhotoProdOmg->SetPointError(48,  0.16   , 0.400E-03  ) ;     // EISENBERG 72   PR D5, 15
	PhotoProdOmg->SetPoint(49 ,   3.867   , 2.000E-03  ) ; PhotoProdOmg->SetPointError(49,  0.17   , 0.300E-03  ) ;     // EISENBERG 72   PR D5, 15
	PhotoProdOmg->SetPoint(50 ,   2.477   , 5.800E-03  ) ; PhotoProdOmg->SetPointError(50,  0.00   , 0.500E-03  ) ;     // BALLAM 70   PRL 24, 1364
	PhotoProdOmg->SetPoint(51 ,   3.114   , 3.200E-03  ) ; PhotoProdOmg->SetPointError(51,  0.00   , 0.300E-03  ) ;     // BALLAM 70   PRL 24, 1364
	PhotoProdOmg->SetPoint(52 ,   2.477   , 5.310E-03  ) ; PhotoProdOmg->SetPointError(52,  0.06   , 0.501E-03  ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdOmg->SetPoint(53 ,   3.114   , 3.010E-03  ) ; PhotoProdOmg->SetPointError(53,  0.14   , 0.301E-03  ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdOmg->SetPoint(54 ,   4.282   , 1.900E-03  ) ; PhotoProdOmg->SetPointError(54,  0.13   , 0.301E-03  ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdOmg->SetPoint(55 ,   7.866   , 1.160E-03  ) ; PhotoProdOmg->SetPointError(55,  1.67   , 0.113E-03  ) ;     // ASTON 82     NP B209, 56
	PhotoProdOmg->SetPoint(56 ,  70.00    , 1.210E-03  ) ; PhotoProdOmg->SetPointError(56,  0.00   , 0.120E-03  ) ;     // BREITWEG 00  DESY-00-084

	TGraphErrors *PhotoProdPhi = new TGraphErrors(39);
	PhotoProdPhi->SetName("TGE_Phi");
	PhotoProdPhi->SetPoint(0  ,   8.158    ,  0.506E-03  ) ; PhotoProdPhi->SetPointError(0 ,  0.60   , 0.090E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(1  ,   8.927    ,  0.568E-03  ) ; PhotoProdPhi->SetPointError(1 ,  1.36   , 0.091E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(2  ,   9.438    ,  0.546E-03  ) ; PhotoProdPhi->SetPointError(2 ,  0.72   , 0.089E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(3  ,  10.017    ,  0.625E-03  ) ; PhotoProdPhi->SetPointError(3 ,  0.68   , 0.063E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(4  ,  11.581    ,  0.646E-03  ) ; PhotoProdPhi->SetPointError(4 ,  0.93   , 0.065E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(5  ,  12.212    ,  0.648E-03  ) ; PhotoProdPhi->SetPointError(5 ,  0.88   , 0.052E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(6  ,  14.135    ,  0.661E-03  ) ; PhotoProdPhi->SetPointError(6 ,  1.11   , 0.053E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(7  ,  14.847    ,  0.630E-03  ) ; PhotoProdPhi->SetPointError(7 ,  1.12   , 0.101E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(8  ,  17.190    ,  0.740E-03  ) ; PhotoProdPhi->SetPointError(8 ,  1.30   , 0.092E-03   ) ;     // EGLOFF  79   PRL 43, 657 
	PhotoProdPhi->SetPoint(9  ,  70.00     ,  0.960E-03  ) ; PhotoProdPhi->SetPointError(9 , 10.00   , 0.190E-03   ) ;     // DERRICK 96  PL 377B, 259 
	PhotoProdPhi->SetPoint(10 ,   8.714    ,  0.524E-03  ) ; PhotoProdPhi->SetPointError(11,  0.56   , 0.101E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(11 ,   9.732    ,  0.446E-03  ) ; PhotoProdPhi->SetPointError(12,  0.50   , 0.056E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(12 ,  10.652    ,  0.417E-03  ) ; PhotoProdPhi->SetPointError(13,  0.45   , 0.063E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(13 ,  11.499    ,  0.545E-03  ) ; PhotoProdPhi->SetPointError(14,  0.42   , 0.074E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(14 ,  12.288    ,  0.656E-03  ) ; PhotoProdPhi->SetPointError(15,  0.39   , 0.093E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(15 ,  13.030    ,  0.526E-03  ) ; PhotoProdPhi->SetPointError(16,  0.37   , 0.084E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(16 ,  13.731    ,  0.635E-03  ) ; PhotoProdPhi->SetPointError(17,  0.35   , 0.120E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(17 ,  14.720    ,  0.608E-03  ) ; PhotoProdPhi->SetPointError(18,  0.66   , 0.116E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(18 ,  16.522    ,  0.594E-03  ) ; PhotoProdPhi->SetPointError(19,  1.18   , 0.131E-03   ) ;     // BUSENITZ 89    PR D40, 1 
	PhotoProdPhi->SetPoint(19 ,   7.562    ,  0.456E-03  ) ; PhotoProdPhi->SetPointError(20,  1.36   , 0.014E-03   ) ;     // ATKINSON 85  ZP C27, 233 
	PhotoProdPhi->SetPoint(20 ,   8.606    ,  0.610E-03  ) ; PhotoProdPhi->SetPointError(21,  2.41   , 0.035E-03   ) ;     // ATKINSON 84  NP B231, 15 
	PhotoProdPhi->SetPoint(21 ,   2.477    ,  0.400E-03  ) ; PhotoProdPhi->SetPointError(22,  0.06   , 0.100E-03   ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdPhi->SetPoint(22 ,   3.114    ,  0.410E-03  ) ; PhotoProdPhi->SetPointError(23,  0.14   , 0.090E-03   ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdPhi->SetPoint(23 ,   4.282    ,  0.550E-03  ) ; PhotoProdPhi->SetPointError(24,  0.13   , 0.070E-03   ) ;     // BALLAM 73    PR D7, 3150 
	PhotoProdPhi->SetPoint(24 ,   2.551    ,  0.493E-03  ) ; PhotoProdPhi->SetPointError(25,  0.19   , 0.168E-03   ) ;     // ERBE 68       PL 27B, 54 
	PhotoProdPhi->SetPoint(25 ,   3.099    ,  0.541E-03  ) ; PhotoProdPhi->SetPointError(26,  0.37   , 0.156E-03   ) ;     // ERBE 68       PL 27B, 54 
	PhotoProdPhi->SetPoint(26 ,   2.944    ,  0.490E-03  ) ; PhotoProdPhi->SetPointError(27,  0.58   , 0.014E-03   ) ;     // ERBE 66      NC A46, 795 
	PhotoProdPhi->SetPoint(27 ,   2.380    ,  0.500E-03  ) ; PhotoProdPhi->SetPointError(28,  0.41   , 0.200E-03   ) ;     // CROUCH 67   PR 156, 1426 
	PhotoProdPhi->SetPoint(28 ,   2.170    ,  0.289E-03  ) ; PhotoProdPhi->SetPointError(29,  0.00   , 0.096E-03   ) ;     // ERBE 68     PR 175, 1669 
	PhotoProdPhi->SetPoint(29 ,   2.551    ,  0.385E-03  ) ; PhotoProdPhi->SetPointError(30,  0.00   , 0.135E-03   ) ;     // ERBE 68     PR 175, 1669 
	PhotoProdPhi->SetPoint(30 ,   2.944    ,  0.385E-03  ) ; PhotoProdPhi->SetPointError(31,  0.00   , 0.250E-03   ) ;     // ERBE 68     PR 175, 1669 
	PhotoProdPhi->SetPoint(31 ,   3.099    ,  0.434E-03  ) ; PhotoProdPhi->SetPointError(32,  0.00   , 0.125E-03   ) ;     // ERBE 68     PR 175, 1669 
	PhotoProdPhi->SetPoint(32 ,   4.215    ,  0.526E-03  ) ; PhotoProdPhi->SetPointError(33,  2.06   , 0.191E-03   ) ;     // DAVIER 69     PR D1, 790 
	PhotoProdPhi->SetPoint(33 ,   3.389    ,  0.509E-03  ) ; PhotoProdPhi->SetPointError(34,  0.30   , 0.040E-03   ) ;     // FRIES 78    NP B143, 408 
	PhotoProdPhi->SetPoint(34 ,   7.309    ,  0.489E-03  ) ; PhotoProdPhi->SetPointError(35,  1.11   , 0.012E-03   ) ;     // ASTON 80      NP B172, 1 
	PhotoProdPhi->SetPoint(35 ,   2.659    ,  0.465E-03  ) ; PhotoProdPhi->SetPointError(36,  0.18   , 0.029E-03   ) ;     // BARBER 82      ZP C12, 1 
	PhotoProdPhi->SetPoint(36 ,   2.992    ,  0.416E-03  ) ; PhotoProdPhi->SetPointError(37,  0.16   , 0.019E-03   ) ;     // BARBER 82      ZP C12, 1 
	PhotoProdPhi->SetPoint(37 ,   2.830    ,  0.445E-03  ) ; PhotoProdPhi->SetPointError(38,  0.35   , 0.019E-03   ) ;     // BARBER 82      ZP C12, 1 
	PhotoProdPhi->SetPoint(38 ,   4.9      ,  0.667E-03  ) ; PhotoProdPhi->SetPointError(39,  0.0    , 0.196E-03   ) ;     // BORISSOV 01  NPB 99A 156 

	TGraphErrors *PhotoProdJPsi = new TGraphErrors(48);
	PhotoProdJPsi->SetName("TGE_JPsi");
	PhotoProdJPsi->SetPoint(0  ,     26.50   ,   0.326E-04  ) ; PhotoProdJPsi->SetPointError(0 ,   5.00  ,  0.540E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(1  ,     40.00   ,   0.415E-04  ) ; PhotoProdJPsi->SetPointError(1 ,  10.00  ,  0.108E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(2  ,     60.00   ,   0.558E-04  ) ; PhotoProdJPsi->SetPointError(2 ,  10.00  ,  0.151E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(3  ,     80.00   ,   0.666E-04  ) ; PhotoProdJPsi->SetPointError(3 ,  10.00  ,  0.197E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(4  ,    100.00   ,   0.734E-04  ) ; PhotoProdJPsi->SetPointError(4 ,  10.00  ,  0.231E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(5  ,    120.00   ,   0.867E-04  ) ; PhotoProdJPsi->SetPointError(5 ,  10.00  ,  0.320E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(6  ,    140.00   ,  10.470E-05  ) ; PhotoProdJPsi->SetPointError(6 ,  10.00  ,  0.490E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(7  ,    160.00   ,  11.000E-05  ) ; PhotoProdJPsi->SetPointError(7 ,  10.00  ,  0.106E-04   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(8  ,     27.50   ,  33.630E-06  ) ; PhotoProdJPsi->SetPointError(8 ,   7.50  ,  0.159E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(9  ,     42.50   ,  43.760E-06  ) ; PhotoProdJPsi->SetPointError(9 ,   7.50  ,  0.199E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(10 ,     55.00   ,  57.240E-06  ) ; PhotoProdJPsi->SetPointError(10,   5.00  ,  0.184E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(11 ,     65.00   ,  62.530E-06  ) ; PhotoProdJPsi->SetPointError(11,   5.00  ,  0.230E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(12 ,     75.00   ,  68.890E-06  ) ; PhotoProdJPsi->SetPointError(12,   5.00  ,  0.255E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(13 ,     85.00   ,  72.080E-06  ) ; PhotoProdJPsi->SetPointError(13,   5.00  ,  0.293E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(14 ,    100.00   ,  81.930E-06  ) ; PhotoProdJPsi->SetPointError(14,  10.00  ,  0.233E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(15 ,    117.50   ,  95.680E-06  ) ; PhotoProdJPsi->SetPointError(15,   7.50  ,  0.319E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(16 ,    132.50   ,  10.395E-05  ) ; PhotoProdJPsi->SetPointError(16,   7.50  ,  0.362E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(17 ,    155.00   ,  11.500E-05  ) ; PhotoProdJPsi->SetPointError(17,  15.00  ,  0.332E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(18 ,    185.00   ,  12.905E-05  ) ; PhotoProdJPsi->SetPointError(18,  15.00  ,  0.468E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(19 ,    215.00   ,  14.174E-05  ) ; PhotoProdJPsi->SetPointError(19,  15.00  ,  0.611E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(20 ,    245.00   ,  14.026E-05  ) ; PhotoProdJPsi->SetPointError(20,  15.00  ,  0.743E-05   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(21 ,    275.00   ,  18.947E-05  ) ; PhotoProdJPsi->SetPointError(21,  15.00  ,  0.134E-04   ) ;     // BREITWEG 02 HEP-EX/0201043
	PhotoProdJPsi->SetPoint(22 ,     31.00   ,  19.800E-06  ) ; PhotoProdJPsi->SetPointError(22,   5.00  ,  3.700E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(23 ,     46.20   ,  29.800E-06  ) ; PhotoProdJPsi->SetPointError(23,   6.20  ,  3.700E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(24 ,     56.10   ,  41.700E-06  ) ; PhotoProdJPsi->SetPointError(24,   4.00  ,  5.200E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(25 ,     64.00   ,  49.500E-06  ) ; PhotoProdJPsi->SetPointError(25,   4.00  ,  6.600E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(26 ,     72.50   ,  51.800E-06  ) ; PhotoProdJPsi->SetPointError(26,   4.50  ,  6.900E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(27 ,     81.50   ,  62.400E-06  ) ; PhotoProdJPsi->SetPointError(27,   4.50  ,  8.500E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(28 ,     92.00   ,  67.600E-06  ) ; PhotoProdJPsi->SetPointError(28,   5.00  ,  8.800E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(29 ,    105.20   ,  64.600E-06  ) ; PhotoProdJPsi->SetPointError(29,   6.50  ,  9.600E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(30 ,    133.40   ,  89.000E-06  ) ; PhotoProdJPsi->SetPointError(30,  20.40  , 13.200E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(31 ,    147.30   ,  80.600E-06  ) ; PhotoProdJPsi->SetPointError(31,  12.30  ,  7.900E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(32 ,    172.20   ,  10.400E-05  ) ; PhotoProdJPsi->SetPointError(32,  12.20  ,  7.900E-06   ) ;     // ADLOFF 00   PL 483B,23 
	PhotoProdJPsi->SetPoint(33 ,    197.10   ,  11.300E-05  ) ; PhotoProdJPsi->SetPointError(33,  12.10  , 12.200E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(34 ,    222.40   ,  11.600E-05  ) ; PhotoProdJPsi->SetPointError(34,  12.40  , 14.800E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(35 ,    247.40   ,  11.890E-05  ) ; PhotoProdJPsi->SetPointError(35,  12.40  , 15.600E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(36 ,    272.40   ,  15.690E-05  ) ; PhotoProdJPsi->SetPointError(36,  12.40  , 20.300E-06   ) ;     // ADLOFF 00   PL 483B,23
	PhotoProdJPsi->SetPoint(37 ,     42.00   ,  36.800E-06  ) ; PhotoProdJPsi->SetPointError(37,  12.00  ,  3.900E-06   ) ;     // AID 96   NP B472,3  
	PhotoProdJPsi->SetPoint(38 ,     72.00   ,  50.600E-06  ) ; PhotoProdJPsi->SetPointError(38,  12.00  ,  4.800E-06   ) ;     // AID 96   NP B472,3 
	PhotoProdJPsi->SetPoint(39 ,    102.00   ,  70.600E-06  ) ; PhotoProdJPsi->SetPointError(39,  12.00  ,  7.000E-06   ) ;     // AID 96   NP B472,3 
	PhotoProdJPsi->SetPoint(40 ,    132.00   ,  68.000E-06  ) ; PhotoProdJPsi->SetPointError(40,  12.00  , 10.600E-06   ) ;     // AID 96   NP B472,3 
	PhotoProdJPsi->SetPoint(41 ,     16.804  ,  18.000E-06  ) ; PhotoProdJPsi->SetPointError(41,   5.15  ,  2.000E-06   ) ;     // BINKLEY 82    PRL 48, 73   
	PhotoProdJPsi->SetPoint(42 ,      6.044  ,   3.720E-06  ) ; PhotoProdJPsi->SetPointError(42,   0.00  ,  0.380E-06   ) ;     // GAMERINI 75 PRL 35,8 483 
	PhotoProdJPsi->SetPoint(43 ,     11.9    ,  11.900E-06  ) ; PhotoProdJPsi->SetPointError(43,   0.00  ,  1.800E-06   ) ;     // AB*HOLMES E401=?BINKLEY 82 PRL 48, 73
	PhotoProdJPsi->SetPoint(44 ,     15.344  ,  14.400E-06  ) ; PhotoProdJPsi->SetPointError(44,   0.00  ,  1.800E-06   ) ;     // AB*HOLMES E401=?BINKLEY 82
	PhotoProdJPsi->SetPoint(45 ,     18.146  ,  18.400E-06  ) ; PhotoProdJPsi->SetPointError(45,   0.00  ,  3.400E-06   ) ;     // AB*HOLMES E401=?BINKLEY 82
	PhotoProdJPsi->SetPoint(46 ,     20.569  ,  23.600E-06  ) ; PhotoProdJPsi->SetPointError(46,   0.00  ,  3.400E-06   ) ;     // AB*HOLMES E401=?BINKLEY 82
	PhotoProdJPsi->SetPoint(47 ,     14.068  ,   9.800E-06  ) ; PhotoProdJPsi->SetPointError(47,   0.00  ,  2.100E-06   ) ;     // DENBY 84   PRL 52,795

//Phys. Rev Letter 23 1256(1969)
	float DataA_8000[5]  = {1, 2, 13, 64, 208};
	float DataAErr_8000[5]  = {0, 0, 0, 0, 0};
	float DataCS_8000[5] = {118.8, 233.6, 1234., 5252., 14320.};
	float DataCSErr_8000[5] = {2.6, 5.6, 25., 296., 1340.};
	TGraphErrors* TGECS_A_8000 = new TGraphErrors(5, DataA_8000, DataCS_8000, DataAErr_8000, DataCSErr_8000 );
	TGECS_A_8000->SetName("TGECS_A_8000");
	
	float DataA_13500[5]  = {1, 2, 12, 64, 208};
	float DataAErr_13500[5]  = {0, 0, 0, 0, 0};
	float DataCS_13500[5] = {114, 218.9, 1153., 5373, 12940};
	float DataCSErr_13500[5] = {2.8, 4.3, 34, 291, 1430};
	TGraphErrors* TGECS_A_13500 = new TGraphErrors(5, DataA_13500, DataCS_13500, DataAErr_13500, DataCSErr_13500 );
	TGECS_A_13500->SetName("TGECS_A_13500");

	float DataA_16400[5]  = {1, 2, 12, 64, 208};
	float DataAErr_16400[5]  = {0, 0, 0, 0, 0};
	float DataCS_16400[5] = {113, 216.3, 1181, 5324, 15700};
	float DataCSErr_16400[5] = {2.5, 4.7, 25, 268, 1370};
	TGraphErrors* TGECS_A_16400 = new TGraphErrors(5, DataA_16400, DataCS_16400, DataAErr_16400, DataCSErr_16400 );
	TGECS_A_16400->SetName("TGECS_A_164000");

//Phys. Rev Letter 22 490 (1969)
	float DataA_9000[6]  = {9, 12, 27, 64, 108, 208};
	float DataAErr_9000[6]  = {0, 0, 0, 0, 0, 0};
	float DataCS_9000[6] = {3.48, 6.85, 26.7, 90.4, 147., 525.};
	float DataCSErr_9000[6] = {.27, .74, 2.7, 9.2, 29., 69.};
	TGraphErrors* TGECS_A_9000 = new TGraphErrors(6, DataA_9000, DataCS_9000, DataAErr_9000, DataCSErr_9000 );
	TGECS_A_9000->SetName("TGECS_A_9000");

	float Datat_Be_9000[19] = {.003, .005, .007, .009, .012, .016, .020, .024, .028, .032, .036, .040, .044, .050, .058, .066, .078, .102, .50};
	float DataDiffCS_Be_9000[19] = {2.95, 2.99, 3.16, 2.40, 2.23, 1.59, 1.56, 1.09, 1.25, .59, .91, .71, .77, .37, .40, .34, .35, .09, .10};
	float DatatErr_Be_9000[19] = {.0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_Be_9000[19] = {.45, .43, .46, .40, .29, .24, .25, .21, .23, .16, .20, .19, .20, .10, .13, .13, .09, .04, .05};
	TGraphErrors* TGEDiffCS_t_Be_9000 = new TGraphErrors(19, Datat_Be_9000, DataDiffCS_Be_9000, DatatErr_Be_9000, DataDiffCSErr_Be_9000 );
	TGEDiffCS_t_Be_9000->SetName("TGEDiffCS_t_Be_9000");

	float Datat_C12_9000[11] = {.003,.005,.008,.012,.016,.020,.026,.034,.042,.054,.094};
	float DataDiffCS_C12_9000[11] = {6.78,7.54,3.54,3.68,3.59,2.05,1.87,.84,.79,.82,.18};
	float DatatErr_C12_9000[11] = {.0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_C12_9000[11] = {1.29,1.40,.68,.70,.69,.54,.40,.29,.29,.21,.07};
	TGraphErrors* TGEDiffCS_t_C12_9000 = new TGraphErrors(11, Datat_C12_9000, DataDiffCS_C12_9000, DatatErr_C12_9000, DataDiffCSErr_C12_9000 );
	TGEDiffCS_t_C12_9000->SetName("TGEDiffCS_t_C12_9000");
	TGEDiffCS_t_C12_9000->Print();

	float Datat_Al27_9000[13] = {.002,.004,.006,.008,.010,.012,.014,.016,.019,.023,.029,.041,.057};
	float DataDiffCS_Al27_9000[13] = {24.1,22.8,13.8,16.3,9.8,10.5,10.4,8.6,3.4,3.6,2.2,0.8,0.9};
	float DatatErr_Al27_9000[13] = {.0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_Al27_9000[13] = {3.4,3.4,2.6,2.8,2.1,2.3,2.5,2.3,1.0,1.1,0.6,0.3,0.3};
	TGraphErrors* TGEDiffCS_t_Al27_9000 = new TGraphErrors(13, Datat_Al27_9000, DataDiffCS_Al27_9000, DatatErr_Al27_9000, DataDiffCSErr_Al27_9000 );
	TGEDiffCS_t_Al27_9000->SetName("TGEDiffCS_t_Al27_9000");
	TGEDiffCS_t_Al27_9000->Print();

	float Datat_Cu63_9000[10] = {.002,.004,.006,.008,.011,.015,.021,.033,.049,.065};
	float DataDiffCS_Cu63_9000[10] = {82.1,61.0,52.4,34.8,19.6,12.7,5.7,1.5,0.7,1.4};
	float DatatErr_Cu63_9000[10] = {.0, .0, .0, .0, .0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_Cu63_9000[10] = {10.2,8.9,8.4,7.1,3.6,3.0,1.4,0.6,0.4,0.7};
	TGraphErrors* TGEDiffCS_t_Cu63_9000 = new TGraphErrors(10, Datat_Cu63_9000, DataDiffCS_Cu63_9000, DatatErr_Cu63_9000, DataDiffCSErr_Cu63_9000 );
	TGEDiffCS_t_Cu63_9000->SetName("TGEDiffCS_t_Cu63_9000");
	TGEDiffCS_t_Cu63_9000->Print();

	float Datat_Ag107_9000[6] = {.002,.004,.006,.009,.019,.043};
	float DataDiffCS_Ag107_9000[6] = {118.,101.,82.,38.,5.,2.5};
	float DatatErr_Ag107_9000[6] = {.0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_Ag107_9000[6] = {25.,25.,22.,11.,2.,1.4};
	TGraphErrors* TGEDiffCS_t_Ag107_9000 = new TGraphErrors(6, Datat_Ag107_9000, DataDiffCS_Ag107_9000, DatatErr_Ag107_9000, DataDiffCSErr_Ag107_9000 );
	TGEDiffCS_t_Ag107_9000->SetName("TGEDiffCS_t_Ag107_9000");
	TGEDiffCS_t_Ag107_9000->Print();

	float Datat_Pb208_9000[9] = {.0015,.0025,.0035,.0045,.006,.008,.011,.017,.029};
	float DataDiffCS_Pb208_9000[9] = {287.,370.,301.,216.,131.,62.,42.,12.,5.};
	float DatatErr_Pb208_9000[9] = {.0, .0, .0, .0, .0, .0, .0, .0, .0};
	float DataDiffCSErr_Pb208_9000[9] = {60.,70.,67.,52.,30.,23.,14.,5.,3.};
	TGraphErrors* TGEDiffCS_t_Pb208_9000 = new TGraphErrors(9, Datat_Pb208_9000, DataDiffCS_Pb208_9000, DatatErr_Pb208_9000, DataDiffCSErr_Pb208_9000 );
	TGEDiffCS_t_Pb208_9000->SetName("TGEDiffCS_t_Pb208_9000");
	TGEDiffCS_t_Pb208_9000->Print();

//Phys. Rev. Lett. 24, 336–340 (1970) 
	float PRL24_7_DataA_8000[7]  = {9, 12, 27, 64, 120, 180, 208};
	float PRL24_7_DataAErr_8000[7]  = {0, 0, 0, 0, 0, 0, 0};
	float PRL24_7_DataCS_8000[7] = {5.27, 8.15, 35.3, 125, 348, 610, 675};
	float PRL24_7_DataCSErr_8000[7] = {.30, .41, 2.6, 8, 25, 51, 44};
	TGraphErrors* PRL24_7_TGECS_A_8000 = new TGraphErrors(7, PRL24_7_DataA_8000, PRL24_7_DataCS_8000, PRL24_7_DataAErr_8000, PRL24_7_DataCSErr_8000 );
	PRL24_7_TGECS_A_8000->SetName("PRL24_7_TGECS_A_8000");

/*Phys. Rev. Lett. 99, 262302 (2007) 	R. Nasseripour, M. Wood, C. Djalali, D. Weygand, C. Tur, U. Mosel, P. Muehlich 	Search for Medium Modifications of the ρ Meson*/
	TGraphErrors *RhoInvMass_Fe56 = new TGraphErrors(44);
	RhoInvMass_Fe56->SetName("RhoInvMass_Fe56");
	RhoInvMass_Fe56->SetPoint(0  ,   0.156  ,  -0.00  ) ; RhoInvMass_Fe56->SetPointError(0 ,  0  , 0   	) ;     
	RhoInvMass_Fe56->SetPoint(1  ,   0.18  	,  -0.04  ) ; RhoInvMass_Fe56->SetPointError(1 ,  0  , 0   	) ;     
	RhoInvMass_Fe56->SetPoint(2  ,   0.204  ,  0.839  ) ; RhoInvMass_Fe56->SetPointError(2 ,  0  , 1.265   	) ;     
	RhoInvMass_Fe56->SetPoint(3  ,   0.228  ,  -1.69  ) ; RhoInvMass_Fe56->SetPointError(3 ,  0  , 0   	) ;     
	RhoInvMass_Fe56->SetPoint(4  ,   0.252  ,  1.338  ) ; RhoInvMass_Fe56->SetPointError(4 ,  0  , 1.897   	) ;     
	RhoInvMass_Fe56->SetPoint(5  ,   0.276  ,  -2.68  ) ; RhoInvMass_Fe56->SetPointError(5 ,  0  , 0   	) ;     
	RhoInvMass_Fe56->SetPoint(6  ,   0.3  	,   -3.33 ) ; RhoInvMass_Fe56->SetPointError(6 ,  0 ,  0  	) ;     
	RhoInvMass_Fe56->SetPoint(7  ,   0.324  ,  2.393  ) ; RhoInvMass_Fe56->SetPointError(7 ,  0  , 2.408   	) ;     
	RhoInvMass_Fe56->SetPoint(8  ,   0.348  ,  0.612  ) ; RhoInvMass_Fe56->SetPointError(8 ,  0  , 2   	) ;     
	RhoInvMass_Fe56->SetPoint(9  ,   0.372  ,  -3.74  ) ; RhoInvMass_Fe56->SetPointError(9 ,  0  , 0   	) ;     
	RhoInvMass_Fe56->SetPoint(10 ,   0.396  ,  4.595  ) ; RhoInvMass_Fe56->SetPointError(10,  0  , 2.966   	) ;     
	RhoInvMass_Fe56->SetPoint(11 ,   0.42  	,  2.765  ) ; RhoInvMass_Fe56->SetPointError(11,  0  , 2.757   	) ;     
	RhoInvMass_Fe56->SetPoint(12 ,   0.444  ,  15.43  ) ; RhoInvMass_Fe56->SetPointError(12,  0  , 4.561   	) ;     
	RhoInvMass_Fe56->SetPoint(13 ,   0.468  ,  2.257  ) ; RhoInvMass_Fe56->SetPointError(13,  0  , 2.864   	) ;     
	RhoInvMass_Fe56->SetPoint(14 ,   0.492  ,  22.50  ) ; RhoInvMass_Fe56->SetPointError(14,  0  , 5.422   	) ;     
	RhoInvMass_Fe56->SetPoint(15 ,   0.516  ,  30.58  ) ; RhoInvMass_Fe56->SetPointError(15,  0  , 6.132   	) ;     
	RhoInvMass_Fe56->SetPoint(16 ,   0.54  	,  23.42  ) ; RhoInvMass_Fe56->SetPointError(16,  0  , 5.568   	) ;     
	RhoInvMass_Fe56->SetPoint(17 ,   0.564  ,  17.49  ) ; RhoInvMass_Fe56->SetPointError(17,  0  , 5.02   	) ;     
	RhoInvMass_Fe56->SetPoint(18 ,   0.588  ,  38.29  ) ; RhoInvMass_Fe56->SetPointError(18,  0  , 6.797   	) ;     
	RhoInvMass_Fe56->SetPoint(19 ,   0.612  ,  40.44  ) ; RhoInvMass_Fe56->SetPointError(19,  0  , 6.87   	) ;     
	RhoInvMass_Fe56->SetPoint(20 ,   0.636  ,  36.66  ) ; RhoInvMass_Fe56->SetPointError(20,  0  , 6.496   	) ;     
	RhoInvMass_Fe56->SetPoint(21 ,   0.66  	,  41.27  ) ; RhoInvMass_Fe56->SetPointError(21,  0  , 6.753   	) ;     
	RhoInvMass_Fe56->SetPoint(22 ,   0.684  ,  56.39  ) ; RhoInvMass_Fe56->SetPointError(22,  0  , 7.81   	) ;     
	RhoInvMass_Fe56->SetPoint(23 ,   0.708  ,  55.96  ) ; RhoInvMass_Fe56->SetPointError(23,  0  , 7.874   	) ;     
	RhoInvMass_Fe56->SetPoint(24 ,   0.732  ,  83.46  ) ; RhoInvMass_Fe56->SetPointError(24,  0  , 9.644   	) ;     
	RhoInvMass_Fe56->SetPoint(25 ,   0.756  ,  61.35  ) ; RhoInvMass_Fe56->SetPointError(25,  0  , 9.165   	) ;     
	RhoInvMass_Fe56->SetPoint(26 ,   0.78  	,  76.14  ) ; RhoInvMass_Fe56->SetPointError(26,  0  , 13.446   ) ;     
	RhoInvMass_Fe56->SetPoint(27 ,   0.804  ,  40.19  ) ; RhoInvMass_Fe56->SetPointError(27,  0  , 7.616   	) ;     
	RhoInvMass_Fe56->SetPoint(28 ,   0.828  ,  31.24  ) ; RhoInvMass_Fe56->SetPointError(28,  0  , 5.865   	) ;     
	RhoInvMass_Fe56->SetPoint(29 ,   0.852  ,  25.93  ) ; RhoInvMass_Fe56->SetPointError(29,  0  , 5.235   	) ;     
	RhoInvMass_Fe56->SetPoint(30 ,   0.876  ,  25.18  ) ; RhoInvMass_Fe56->SetPointError(30,  0  , 5.119   	) ;     
	RhoInvMass_Fe56->SetPoint(31 ,   0.9  	,   10.53 ) ; RhoInvMass_Fe56->SetPointError(31,  0 ,   3.376 	) ;     
	RhoInvMass_Fe56->SetPoint(32 ,   0.924  ,  18.88  ) ; RhoInvMass_Fe56->SetPointError(32,  0  , 4.45   	) ;     
	RhoInvMass_Fe56->SetPoint(33 ,   0.948  ,  13.82  ) ; RhoInvMass_Fe56->SetPointError(33,  0  , 3.847   	) ;     
	RhoInvMass_Fe56->SetPoint(34 ,   0.972  ,  4.339  ) ; RhoInvMass_Fe56->SetPointError(34,  0  , 2.408   	) ;     
	RhoInvMass_Fe56->SetPoint(35 ,   0.996  ,  19.86  ) ; RhoInvMass_Fe56->SetPointError(35,  0  , 4.858   	) ;     
	RhoInvMass_Fe56->SetPoint(36 ,   1.02  	,  -0.30  ) ; RhoInvMass_Fe56->SetPointError(36,  0  , 4.899   	) ;     
	RhoInvMass_Fe56->SetPoint(37 ,   1.044  ,  7.822  ) ; RhoInvMass_Fe56->SetPointError(37,  0  , 3.286   	) ;     
	RhoInvMass_Fe56->SetPoint(38 ,   1.068  ,  10.90  ) ; RhoInvMass_Fe56->SetPointError(38,  0  , 3.376   	) ;     
	RhoInvMass_Fe56->SetPoint(39 ,   1.092  ,  2.949  ) ; RhoInvMass_Fe56->SetPointError(39,  0  , 1.789   	) ;     
	RhoInvMass_Fe56->SetPoint(40 ,   1.116  ,  3.904  ) ; RhoInvMass_Fe56->SetPointError(40,  0  , 2   	) ;     
	RhoInvMass_Fe56->SetPoint(41 ,   1.14  	,  4.531  ) ; RhoInvMass_Fe56->SetPointError(41,  0  , 2.145   	) ;     
	RhoInvMass_Fe56->SetPoint(42 ,   1.164  ,  2.55  ) ; RhoInvMass_Fe56->SetPointError(42,   0 ,  1.612  	) ;     
	RhoInvMass_Fe56->SetPoint(43 ,   1.188  ,  4.172  ) ; RhoInvMass_Fe56->SetPointError(43,  0  , 2.049   	) ;     

	TGraphErrors *RhoInvMass_C12 = new TGraphErrors(45);
	RhoInvMass_C12->SetName("RhoInvMass_C12");
	RhoInvMass_C12->SetPoint(0  ,   0.132	 ,1	  ) ; RhoInvMass_C12->SetPointError(0 ,  0  , 	1	) ;     
	RhoInvMass_C12->SetPoint(1  ,   0.156	 ,-0.024  ) ; RhoInvMass_C12->SetPointError(1 ,  0  , 	0	) ;     
	RhoInvMass_C12->SetPoint(2  ,   0.18	 , 1.146  ) ; RhoInvMass_C12->SetPointError(2 ,  0  ,1.095  	) ;     
	RhoInvMass_C12->SetPoint(3  ,   0.204	 ,2.021	  ) ; RhoInvMass_C12->SetPointError(3 ,  0  ,1.949  	) ;     
	RhoInvMass_C12->SetPoint(4  ,   0.228	 ,-0.384  ) ; RhoInvMass_C12->SetPointError(4 ,  0  ,1.612   	) ;     
	RhoInvMass_C12->SetPoint(5  ,   0.252	 ,-5.195  ) ; RhoInvMass_C12->SetPointError(5 ,  0  ,0.001  	) ;     
	RhoInvMass_C12->SetPoint(6  ,   0.276	, -0.953  ) ; RhoInvMass_C12->SetPointError(6 ,   0 ,2  	) ;     
	RhoInvMass_C12->SetPoint(7  ,   0.3	 ,  -5.357) ; RhoInvMass_C12->SetPointError(7 ,  0  ,0.632  	) ;     
	RhoInvMass_C12->SetPoint(8  ,   0.324	 ,-0.174  ) ; RhoInvMass_C12->SetPointError(8 ,  0  ,2.49  	) ;     
	RhoInvMass_C12->SetPoint(9  ,   0.348	 ,16.818  ) ; RhoInvMass_C12->SetPointError(9 ,  0  ,4.817  	) ;     
	RhoInvMass_C12->SetPoint(10 ,   0.372	 ,11.758  ) ; RhoInvMass_C12->SetPointError(10,  0  ,4.336   	) ;     
	RhoInvMass_C12->SetPoint(11 ,   0.396	, 7.539	  ) ; RhoInvMass_C12->SetPointError(11,  0  ,3.899   	) ;     
	RhoInvMass_C12->SetPoint(12 ,   0.42	 , 3.94	  ) ; RhoInvMass_C12->SetPointError(12,  0  ,3.633  	) ;     
	RhoInvMass_C12->SetPoint(13 ,   0.444	 ,26.468  ) ; RhoInvMass_C12->SetPointError(13,  0  ,6.017   	) ;     
	RhoInvMass_C12->SetPoint(14 ,   0.468	 ,18.618  ) ; RhoInvMass_C12->SetPointError(14,  0  ,5.459   	) ;     
	RhoInvMass_C12->SetPoint(15 ,   0.492	 ,8.576	  ) ; RhoInvMass_C12->SetPointError(15,  0  ,4.539   	) ;     
	RhoInvMass_C12->SetPoint(16 ,   0.516	, 24.038  ) ; RhoInvMass_C12->SetPointError(16,  0  ,6.116   	) ;     
	RhoInvMass_C12->SetPoint(17 ,   0.54	 , 23.619 ) ; RhoInvMass_C12->SetPointError(17,  0  ,6.083 	) ;     
	RhoInvMass_C12->SetPoint(18 ,   0.564	 ,33.185  ) ; RhoInvMass_C12->SetPointError(18,  0  ,6.914   	) ;     
	RhoInvMass_C12->SetPoint(19 ,   0.588	 ,34.502  ) ; RhoInvMass_C12->SetPointError(19,  0  ,6.986  	) ;     
	RhoInvMass_C12->SetPoint(20 ,   0.612	 ,60.71   ) ; RhoInvMass_C12->SetPointError(20,  0  ,8.532   	) ;     
	RhoInvMass_C12->SetPoint(21 ,   0.636	, 69.978  ) ; RhoInvMass_C12->SetPointError(21,  0  ,8.877   	) ;     
	RhoInvMass_C12->SetPoint(22 ,   0.66	 , 56.128 ) ; RhoInvMass_C12->SetPointError(22,  0  ,7.899 	) ;     
	RhoInvMass_C12->SetPoint(23 ,   0.684	 ,78.287  ) ; RhoInvMass_C12->SetPointError(23,  0  ,9.22   	) ;     
	RhoInvMass_C12->SetPoint(24 ,   0.708	 ,93.221  ) ; RhoInvMass_C12->SetPointError(24,  0  ,10.1   	) ;     
	RhoInvMass_C12->SetPoint(25 ,   0.732	 ,112.701 ) ; RhoInvMass_C12->SetPointError(25,  0  ,11.269   	) ;     
	RhoInvMass_C12->SetPoint(26 ,   0.756	, 96.548  ) ; RhoInvMass_C12->SetPointError(26,  0  ,11.576    	) ;     
	RhoInvMass_C12->SetPoint(27 ,   0.78	 , 92.555 ) ; RhoInvMass_C12->SetPointError(27,  0  ,17.268  	) ;     
	RhoInvMass_C12->SetPoint(28 ,   0.804	 ,61.284  ) ; RhoInvMass_C12->SetPointError(28,  0  ,9.798   	) ;     
	RhoInvMass_C12->SetPoint(29 ,   0.828	 ,39.887  ) ; RhoInvMass_C12->SetPointError(29,  0  ,6.753   	) ;     
	RhoInvMass_C12->SetPoint(30 ,   0.852	 ,28.177  ) ; RhoInvMass_C12->SetPointError(30,  0  ,5.532   	) ;     
	RhoInvMass_C12->SetPoint(31 ,   0.876	, 21.327  ) ; RhoInvMass_C12->SetPointError(31,  0  ,4.775  	) ;     
	RhoInvMass_C12->SetPoint(32 ,   0.9	 ,19.446  ) ; RhoInvMass_C12->SetPointError(32,  0  ,4.539 	) ;     
	RhoInvMass_C12->SetPoint(33 ,   0.924	 ,7.167	  ) ; RhoInvMass_C12->SetPointError(33,  0  ,2.864   	) ;     
	RhoInvMass_C12->SetPoint(34 ,   0.948	 ,10.909  ) ; RhoInvMass_C12->SetPointError(34,  0  ,3.493   	) ;     
	RhoInvMass_C12->SetPoint(35 ,   0.972	 ,7.507	  ) ; RhoInvMass_C12->SetPointError(35,  0  ,3.033   	) ;     
	RhoInvMass_C12->SetPoint(36 ,   0.996	, 20.188  ) ; RhoInvMass_C12->SetPointError(36,  0  ,5.04   	) ;     
	RhoInvMass_C12->SetPoint(37 ,   1.02	 , 1.213  ) ; RhoInvMass_C12->SetPointError(37,  0  ,6.782  	) ;     
	RhoInvMass_C12->SetPoint(38 ,   1.044	 ,6.713	  ) ; RhoInvMass_C12->SetPointError(38,  0  ,3.493   	) ;     
	RhoInvMass_C12->SetPoint(39 ,   1.068	 ,4.797	  ) ; RhoInvMass_C12->SetPointError(39,  0  ,2.408   	) ;     
	RhoInvMass_C12->SetPoint(40 ,   1.092	 ,1.444	  ) ; RhoInvMass_C12->SetPointError(40,  0  ,1.342  	) ;     
	RhoInvMass_C12->SetPoint(41 ,   1.116	, 2.77	  ) ; RhoInvMass_C12->SetPointError(41,  0  ,1.732  	) ;     
	RhoInvMass_C12->SetPoint(42 ,   1.14	 , 4.269  ) ; RhoInvMass_C12->SetPointError(42,  0  ,2.098  	) ;     
	RhoInvMass_C12->SetPoint(43 ,   1.164	 ,4.291	  ) ; RhoInvMass_C12->SetPointError(43,  0  ,2.098 	) ;     
	RhoInvMass_C12->SetPoint(44 ,   1.188	 ,0.936	  ) ; RhoInvMass_C12->SetPointError(44,  0  ,1    	) ;     


	TGraph *Giesser60 = new TGraph(10);
	Giesser60->SetName("Giesser60");
	Giesser60->SetPoint(0, 12.872638366869595,0.9861382344111573);
	Giesser60->SetPoint(1, 15.0002727308914,0.9504646116248324);
	Giesser60->SetPoint(2, 18.62130501703815,0.9067701230546806);
	Giesser60->SetPoint(3, 25.55278338505485,0.8475677767007445);
	Giesser60->SetPoint(4, 33.263465009781484,0.7987307011372329);
	Giesser60->SetPoint(5, 47.86583249689563,0.7329349975740241);
	Giesser60->SetPoint(6, 71.84913613239826,0.661620004117);
	Giesser60->SetPoint(7, 107.2835335779714,0.5960153882229435);
	Giesser60->SetPoint(8, 151.96576550706337,0.5413211848035535);
	Giesser60->SetPoint(9, 205.2769279635414,0.4977231998900537);

	TGraph *Giesser105 = new TGraph(10);
	Giesser105->SetName("Giesser105");
	Giesser105->SetPoint(0, 11.95638361361463,1.0024147775451635);
	Giesser105->SetPoint(1, 14.688224537321256,0.9388021981989012);
	Giesser105->SetPoint(2, 17.854611675373647,0.884654100353332);
	Giesser105->SetPoint(3, 23.86558313631465,0.8117269982108051);
	Giesser105->SetPoint(4, 35.07826722177202,0.723754546940692);
	Giesser105->SetPoint(5, 56.39908374347487,0.6232159396040222);
	Giesser105->SetPoint(6, 90.20149576745891,0.5377445766078071);
	Giesser105->SetPoint(7, 119.30717144992919,0.4913872564475994);
	Giesser105->SetPoint(8, 157.80449135875543,0.44902625949718067);
	Giesser105->SetPoint(9, 207.62700979435297,0.41031514582003775);

	TGraph *Giesser149 = new TGraph(11);
	Giesser149->SetName("Giesser149");
	Giesser149->SetPoint(0, 12.019656769824918,1.0003620806083418);
	Giesser149->SetPoint(1, 13.428298701047,0.9621588069057052);
	Giesser149->SetPoint(2, 15.322118697525466,0.9178576686781222);
	Giesser149->SetPoint(3, 18.23688045201539,0.8631261794137652);
	Giesser149->SetPoint(4, 23.869446046766885,0.7822560519824107);
	Giesser149->SetPoint(5, 31.241381066104665,0.710421105656139);
	Giesser149->SetPoint(6, 44.72786133654147,0.625652629201852);
	Giesser149->SetPoint(7, 69.6795850319141,0.5321261281044697);
	Giesser149->SetPoint(8, 109.12131325593852,0.45538086619248397);
	Giesser149->SetPoint(9, 156.22890818126902,0.40022104027281297);
	Giesser149->SetPoint(10, 204.48110654615704,0.3627225524508418);

	TGraph *Giesser193 = new TGraph(10);
	Giesser193->SetName("Giesser193");
	Giesser193->SetPoint(0, 11.76908471337598,0.9982901195327896);
	Giesser193->SetPoint(1, 14.382409187877585,0.9311010282091078);
	Giesser193->SetPoint(2, 17.209772121959183,0.8648566124575884);
	Giesser193->SetPoint(3, 24.125351583497793,0.760083107344895);
	Giesser193->SetPoint(4, 37.19118811715233,0.6411640781170627);
	Giesser193->SetPoint(5, 56.73069421085761,0.5441893573771824);
	Giesser193->SetPoint(6, 79.9489491827483,0.4763042910359595);
	Giesser193->SetPoint(7, 113.26398498504454,0.417746930021615);
	Giesser193->SetPoint(8, 148.24621928930605,0.37860636370505313);
	Giesser193->SetPoint(9, 207.81191421522558,0.33479712729166705);

	TGraph *Giesser210 = new TGraph(10);
	Giesser210->SetName("Giesser210");
	Giesser210->SetPoint(0, 11.707130603555521,1.000338564900869);
	Giesser210->SetPoint(1, 15.323772023308152,0.8955049030263792);
	Giesser210->SetPoint(2, 20.16437853568354,0.7951001614337155);
	Giesser210->SetPoint(3, 25.706955611016447,0.71468908696016);
	Giesser210->SetPoint(4, 34.91557957856178,0.6280899869774952);
	Giesser210->SetPoint(5, 52.42520641423298,0.5308991669009134);
	Giesser210->SetPoint(6, 73.88334099146572,0.46181671810214586);
	Giesser210->SetPoint(7, 104.1244937202421,0.4017235181656301);
	Giesser210->SetPoint(8, 141.41457599636757,0.35816071155858054);
	Giesser210->SetPoint(9, 206.79232523357533,0.3090158242430401);

	TGraph *Valencia30 = new TGraph(11);
	Valencia30->SetName("Valencia30");
	Valencia30->SetPoint(0, 12.014824773745394,0.9979440432106602);
	Valencia30->SetPoint(1, 17.545091659768026,0.9696005489103676);
	Valencia30->SetPoint(2, 24.439523219837106,0.9190810195464224);
	Valencia30->SetPoint(3, 32.46821815130951,0.8820183101733804);
	Valencia30->SetPoint(4, 50.50982609465298,0.8207185644823977);
	Valencia30->SetPoint(5, 66.75590935371271,0.774760698798503);
	Valencia30->SetPoint(6, 85.93778018322993,0.7328831166793833);
	Valencia30->SetPoint(7, 114.77224198319063,0.7018829791610252);
	Valencia30->SetPoint(8, 150.09861773189192,0.6653124576038467);
	Valencia30->SetPoint(9, 174.84494674057888,0.6424375075194692);
	Valencia30->SetPoint(10, 227.46658791507105,0.6064628653478298);

	TGraph *Valencia50 = new TGraph(10);
	Valencia50->SetName("Valencia50");
	Valencia50->SetPoint(0, 11.951828049514601,0.9979440432106602);
	Valencia50->SetPoint(1, 19.3910346985051,0.9362634846413612);
	Valencia50->SetPoint(2, 29.231755398392874,0.8552054376931044);
	Valencia50->SetPoint(3, 44.532689156299305,0.7795590391803995);
	Valencia50->SetPoint(4, 61.066878541389656,0.7238888013726869);
	Valencia50->SetPoint(5, 89.67823228951822,0.6464163281281503);
	Valencia50->SetPoint(6, 123.62011368996183,0.6027299662206788);
	Valencia50->SetPoint(7, 149.41228017968163,0.5701510374040725);
	Valencia50->SetPoint(8, 181.5359535427792,0.5404442038957908);
	Valencia50->SetPoint(9, 224.07439150776912,0.5112319625359147);

	TGraph *Valencia90 = new TGraph(11);
	Valencia90->SetName("Valencia90");
	Valencia90->SetPoint(0, 12.07815354653372,0.9979440432106605);
	Valencia90->SetPoint(1, 16.38711854124549,0.9557321244597872);
	Valencia90->SetPoint(2, 19.70228718712037,0.9040726241934289);
	Valencia90->SetPoint(3, 24.065108032077212,0.8516925217590341);
	Valencia90->SetPoint(4, 32.484269598222475,0.7876224032239667);
	Valencia90->SetPoint(5, 43.391813144744575,0.722400517276778);
	Valencia90->SetPoint(6, 68.96075970366931,0.6229077599314855);
	Valencia90->SetPoint(7, 98.64867267092734,0.5494160498086547);
	Valencia90->SetPoint(8, 131.08310961048582,0.5028836545547397);
	Valencia90->SetPoint(9, 171.45916917909247,0.45840155041776337);
	Valencia90->SetPoint(10, 227.83482466141166,0.4187149290719034);

	TGraph *Valencia150 = new TGraph(11);
	Valencia150->SetName("Valencia150");
	Valencia150->SetPoint(0, 12.01471680421757,1);
	Valencia150->SetPoint(1, 17.003372380855943,0.9305005973414168);
	Valencia150->SetPoint(2, 20.444313525325644,0.8694025950601678);
	Valencia150->SetPoint(3, 26.882497689015942,0.7925004002479986);
	Valencia150->SetPoint(4, 37.65261850086539,0.7106038722194711);
	Valencia150->SetPoint(5, 51.63792912208502,0.6437610524259165);
	Valencia150->SetPoint(6, 66.83934689492467,0.5820067440202218);
	Valencia150->SetPoint(7, 91.68031147182035,0.508085228488765);
	Valencia150->SetPoint(8, 129.77740293763748,0.44814047465571644);
	Valencia150->SetPoint(9, 168.87033698221762,0.4034872993900354);
	Valencia150->SetPoint(10, 220.88954221256594,0.3662864113795119);


	TGraph *Valencia210 = new TGraph(13);
	Valencia210->SetName("Valencia210");
	Valencia210->SetPoint(0, 11.951828049514601,0.9979440432106602);
	Valencia210->SetPoint(1, 15.385277540627337,0.9557321244597872);
	Valencia210->SetPoint(2, 19.7061827298195,0.8640512497863206);
	Valencia210->SetPoint(3, 24.07267827597666,0.7925004002479986);
	Valencia210->SetPoint(4, 32.840006278041955,0.722400517276778);
	Valencia210->SetPoint(5, 43.868575672432804,0.6571474038797614);
	Valencia210->SetPoint(6, 55.59574627181317,0.6077122955678675);
	Valencia210->SetPoint(7, 72.73073688772479,0.5360133720083564);
	Valencia210->SetPoint(8, 96.65931795133358,0.4727736086091461);
	Valencia210->SetPoint(9, 127.78001755140107,0.4221761402214038);
	Valencia210->SetPoint(10, 165.4039351571889,0.3777704249354468);
	Valencia210->SetPoint(11, 207.44863815337487,0.3415318994140561);
	Valencia210->SetPoint(12, 236.60893671402653,0.33388688885104734);

	TObjArray Hlist (0);  // Creating an array of histograms

	Hlist.Add(PhotoProdRho);   // Adding TGraph to the list 
	Hlist.Add(PhotoProdOmg);
	Hlist.Add(PhotoProdPhi);
	Hlist.Add(PhotoProdJPsi);
	Hlist.Add(TGECS_A_8000);
	Hlist.Add(TGECS_A_13500);
	Hlist.Add(TGECS_A_16400);
	Hlist.Add(TGECS_A_9000);
	Hlist.Add(TGEDiffCS_t_C12_9000);
	Hlist.Add(TGEDiffCS_t_Al27_9000);
	Hlist.Add(TGEDiffCS_t_Cu63_9000);
	Hlist.Add(TGEDiffCS_t_Ag107_9000);
	Hlist.Add(TGEDiffCS_t_Pb208_9000);
	Hlist.Add(RhoInvMass_Fe56);
	Hlist.Add(RhoInvMass_C12);
	Hlist.Add(PRL24_7_TGECS_A_8000);
	Hlist.Add(Giesser60);
	Hlist.Add(Giesser105);
	Hlist.Add(Giesser149);
	Hlist.Add(Giesser193);
	Hlist.Add(Giesser210);
	Hlist.Add(Valencia30);
	Hlist.Add(Valencia50);
	Hlist.Add(Valencia90);
	Hlist.Add(Valencia150);
	Hlist.Add(Valencia210);

	TFile f( "results/root_scripts/VectorMesons/PhotoProduction_VectorMeson_Exp.root","recreate");
	Hlist->Write();

	TCanvas *c1 = new TCanvas("c1","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gPad->SetLogy();
	gPad->SetLogx();

	PhotoProdRho->GetXaxis()->SetTitle("W (GeV)");				
	PhotoProdRho->GetYaxis()->SetTitle("#sigma (mbar)");				
	PhotoProdRho->GetYaxis()->SetRangeUser(1e-6,0.1);
	PhotoProdRho->GetXaxis()->SetRangeUser(1,1000.);


	PhotoProdRho->SetMarkerStyle(20);
	PhotoProdRho->SetMarkerSize(1);
	PhotoProdRho->SetMarkerColor(1);
	PhotoProdOmg->SetMarkerStyle(21);
	PhotoProdOmg->SetMarkerSize(1);
	PhotoProdOmg->SetMarkerColor(2);
	PhotoProdPhi->SetMarkerStyle(22);
	PhotoProdPhi->SetMarkerSize(1);
	PhotoProdPhi->SetMarkerColor(3);
	PhotoProdJPsi->SetMarkerStyle(23);
	PhotoProdJPsi->SetMarkerSize(1);
	PhotoProdJPsi->SetMarkerColor(4);

	PhotoProdRho->Draw("AP");
	PhotoProdOmg->Draw("Psame");
	PhotoProdPhi->Draw("Psame");
	PhotoProdJPsi->Draw("Psame");

	c1->Print("results/root_scripts/VectorMesons/PhotoProduction_VectorMeson_Exp.eps");	
	f.Close();
}
