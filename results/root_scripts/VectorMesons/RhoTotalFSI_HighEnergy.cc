/* ================================================================================
 * 
 * 	Copyright 2015 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "TF1.h"
#include "TCanvas.h"

///NOTE: All units in GeV

const double mRho = 775.49/1000.;
const double mProton = 938.2723/1000.; //proton
const double pion_pm_mass = 139.57018/1000.;

double pion_Proton_ReggeFit(double *x, double *p){
	
	TLorentzVector pion(0,0,x[0],sqrt(x[0]*x[0] + pion_pm_mass*pion_pm_mass));
	TLorentzVector proton(0,0,0,mProton);
	
	double w = (pion+proton).Mag();
	
	return p[0]*pow(w,p[1]) + p[2]*pow(w,p[3]);
}

double RhoN_Total_CS_HighEn(double *x, double *p){

	double thrld = mRho+mProton;	
	double s = pow(x[0] + thrld,2.);
	
	double W = sqrt(s);

	if(W >= 2.){
		
		double CS_pipP = 42.6816*pow(W,-1.29223) + 14.0899*pow(W,0.166105); //milibarn 
		double CS_pimP = 34.5152*pow(W,-0.540496) + 7.02817*pow(W,0.306528); //milibarn
		
		return (1./2.)*(CS_pipP+CS_pimP);
	}
	else return 0.;
}

double RhoNTotalVDM_ReggeFit(double *x, double *p){
	
	double w = x[0];
	return p[0]*pow(w,p[1]) + p[2]*pow(w,p[3]);
}

void RhoTotalFSI_HighEnergy(){

	bool fit1(false);
	bool draw1(false);
	bool fit2(true);
	bool draw2(false);
	
	if(fit1){
		
	/// Pi+ + P --> X
		TGraphErrors *pion_p_Proton = new TGraphErrors("results/root_scripts/VectorMesons/pi_plus_proton_xsection.txt","%lg %lg %lg");
		pion_p_Proton->SetMarkerStyle(20);
		pion_p_Proton->SetMarkerSize(0.8);
		pion_p_Proton->SetMarkerColor(1);
		
		TCanvas *c1 = new TCanvas("c1","#pi^{+} P Cross Section",10,20,900,600);
		c1->SetFillColor(10);
		c1->SetBorderMode(0);
		
		TF1 *pionpP = new TF1("pionpP",pion_Proton_ReggeFit,1.8,500,4);
		pionpP->SetParameters(10,1,10,1);
		pionpP->SetParLimits(0,0,100);
		pionpP->SetParLimits(1,-5,5);
		pionpP->SetParLimits(2,0,100);
		pionpP->SetParLimits(3,0,5);
		
		pion_p_Proton->Fit(pionpP,"R");
		pion_p_Proton->Draw("AP");
		
		pion_p_Proton->GetYaxis()->SetTitle("#sigma [mb]");
		pion_p_Proton->GetXaxis()->SetTitle("p_{lab} [GeV/c]");
		
		gPad->SetLogx();
		gPad->SetLogy();
		gStyle->SetOptTitle(kFALSE);
		
		TLegend leg(0.56,0.7,0.9,0.9);
		leg.SetFillColor(10);
		leg.AddEntry(pion_p_Proton, "Total #pi^{+}P cross section (Exp)", "P");
		leg.AddEntry(pionpP, "Regge fit - Power law", "L");
		leg.Draw();
		
		c1->Print("results/root_scripts/VectorMesons/pi_plus_proton_xsection_ReggeFit.pdf");
		
	/// Pi- + P --> X	
		TGraphErrors *pion_m_Proton = new TGraphErrors("results/root_scripts/VectorMesons/pi_minus_proton_xsection.txt","%lg %lg %lg");
		pion_m_Proton->SetMarkerStyle(20);
		pion_m_Proton->SetMarkerSize(0.8);
		pion_m_Proton->SetMarkerColor(1);
		
		TCanvas *c2 = new TCanvas("c2","#pi^{-} P Cross Section",10,20,900,600);
		c2->SetFillColor(10);
		c2->SetBorderMode(0);
		
		TF1 *pionmP = new TF1("piompP",pion_Proton_ReggeFit,6.0,700,4);
		pionmP->SetParameters(10,1,10,1);
		pionmP->SetParLimits(0,0,100);
		pionmP->SetParLimits(1,-5,5);
		pionmP->SetParLimits(2,0,100);
		pionmP->SetParLimits(3,0,5);
		
		pion_m_Proton->Fit(pionmP,"R");
		pion_m_Proton->Draw("AP");
		
		pion_m_Proton->GetYaxis()->SetTitle("#sigma [mb]");
		pion_m_Proton->GetXaxis()->SetTitle("p_{lab} [GeV/c]");
		pion_m_Proton->SetMinimum(5);
		pion_m_Proton->SetMaximum(150);
		
		gPad->SetLogx();
		gPad->SetLogy();
		gStyle->SetOptTitle(kFALSE);
		
		TLegend leg2(0.56,0.7,0.9,0.9);
		leg2.SetFillColor(10);
		leg2.AddEntry(pion_m_Proton, "Total #pi^{-}P cross section (Exp)", "P");
		leg2.AddEntry(pionmP, "Regge fit - Power law", "L");
		leg2.Draw();
		
		c2->Print("results/root_scripts/VectorMesons/pi_minus_proton_xsection_ReggeFit.pdf");
	}
	
	if(draw1){
	
		TF1* RhoN_total_HighEn = new TF1("RhoN_CS_HiEn",RhoN_Total_CS_HighEn,0.3,100,1);
		RhoN_total_HighEn->SetParameter(0,10);
		RhoN_total_HighEn->SetNpx(1000);
		
		TCanvas *c3 = new TCanvas("c3","RhoN Total Cross Section",10,20,700,500);
		c3->SetFillColor(10);
		c3->SetBorderMode(0);
		
		RhoN_total_HighEn->Draw();
		
		gPad->SetLogx();
		
		RhoN_total_HighEn->GetXaxis()->SetTitle("#sqrt{s} - #sqrt{s_{0}} [GeV]");
		RhoN_total_HighEn->GetYaxis()->SetTitle("#sigma [mb]");
		RhoN_total_HighEn->GetXaxis()->SetRangeUser(0.,100.);
		
		c3->Print("results/Figures/RhoN_Total_Cross_Section_HighEnergy.eps");
		c3->Print("results/Figures/RhoN_Total_Cross_Section_HighEnergy.pdf");
		
	} 
	
	if(fit2){
	
		TFile F("data/PhotoProduction_VectorMeson_Exp.root");
		TGraphErrors *RhoPhotoCS = (TGraphErrors*)F.Get("TGE_Rho");
		
		TGraphErrors *RhoNTotalVDM_Exp2 = new TGraphErrors();
		RhoNTotalVDM_Exp2->SetMarkerSize(0.8);
		RhoNTotalVDM_Exp2->SetMarkerStyle(20);
		
		double couplingConst = 0.61; //gamma_rho^2/4pi
		double alpha = 1./137.; //fine structure constant
		
		for(int i=0; i<RhoPhotoCS->GetN(); i++){
			
			double gN_rhoN_Forward = 10.9 * RhoPhotoCS->GetY()[i]; // slope parameter for rho elastic b = 10.9  GeV^-2 H1Collaboration1996
			double CS = sqrt( couplingConst * (64.*TMath::Pi()/alpha) * gN_rhoN_Forward );
				
			double ErrX = RhoPhotoCS->GetEX()[i];
			double ErrY = (1./2.)*sqrt(couplingConst*(64.*TMath::Pi()/alpha)*10.9)
						*pow(RhoPhotoCS->GetY()[i],-1./2.)*RhoPhotoCS->GetEY()[i];
			
			RhoNTotalVDM_Exp2->SetPoint(i,RhoPhotoCS->GetX()[i],CS);
			RhoNTotalVDM_Exp2->SetPointError(i,ErrX,ErrY);
		}
	
		TCanvas *c4 = new TCanvas("c4","#rhoN Cross Section",10,20,900,600);
		c4->SetFillColor(10);
		c4->SetBorderMode(0);
		
		gPad->SetLogx();
		//gPad->SetLogy();
		gStyle->SetOptTitle(kFALSE);
		
		TF1 *rhoNTotal = new TF1("rhoNTotal",RhoNTotalVDM_ReggeFit,2,40000,4);
		rhoNTotal->SetParameters(10,1,10,1);
		rhoNTotal->SetParLimits(0,0,100);
		rhoNTotal->SetParLimits(1,-5,5);
		rhoNTotal->SetParLimits(2,0,100);
		rhoNTotal->SetParLimits(3,0,5);
		
		RhoNTotalVDM_Exp2->Fit(rhoNTotal,"R");
		RhoNTotalVDM_Exp2->Draw("AP");
		
		RhoNTotalVDM_Exp2->GetYaxis()->SetTitle("#sigma [mb]");
		RhoNTotalVDM_Exp2->GetXaxis()->SetTitle("W [GeV]");
		
		TLegend leg4(0.56,0.7,0.9,0.9);
		leg4.SetFillColor(10);
		leg4.AddEntry(RhoNTotalVDM_Exp2, "Total #rhoN cross section (Exp)", "P");
		leg4.AddEntry(rhoNTotal, "Regge fit - Power law", "L");
		leg4.Draw();
		
		c4->Print("results/root_scripts/VectorMesons/rhoN_total_xsection_ReggeFit.pdf");
	
	}
}

















