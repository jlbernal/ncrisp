/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int Nt = 500;
	const int NW = 500;
	double W_i = 2;
	double W_f = 800.;
	const int NComp = 5;
	double theta_0 = TMath::TwoPi() * 1e-2;

	TString EqStr = "[0]+ [1]*exp([2] + [3]*x + [4]*x*x)";
	TString EqStrLtx = "a_{0}+ a_{1}exp(a_{2} + a_{3}x + a_{4}x^{2})"; //for figures
	const int PAR = 5;

	double Q_2  = 0.0;

	double MV = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MP = CPT::p_mass/1000.;
	double MV_2 = TMath::Power(MV,2);
	double MP_2 = TMath::Power(MP,2);
	double W_pass = (W_f - W_i)/NW;
	double par[PAR + 1][NW];
	double parErr[PAR][NW];
	double _W[NW];
	bool spc = false;
	for (int i = 0 ; i < NW ; i ++){
		double W = W_i + i*W_pass ;
		double W_2 = W*W;
		double t_max = VM_Mand_t (W_2, Q_2,1,MP,MV);
		double z_max = VM_Mand_z (W_2,Q_2,t_max,MP,MV);
		double DiffCScos_max = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z_max,t_max,Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
		double t = VM_Mand_t (W_2, Q_2,TMath::Cos(theta_0),MP,MV);
		double z = VM_Mand_z (W_2,Q_2,t,MP,MV);
		double DiffCScos = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t,Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
//		cout << " W["<< i << "] =  " << W << ", t = " << t <<  ", DiffCS_max(cos = 1) = " <<  DiffCScos_max <<  ", DiffCS(cos) = " <<  DiffCScos <<  endl;
		if (DiffCScos > 0.01) spc = true; // beyond threshold on photo vector meson production valid energy range
		if ((DiffCScos_max > 1e3*DiffCScos) && (spc == true)){
			cout << "la energia de corte es W = " << W << endl << endl;
			W_f = W; 
			break;
		}
	}
	W_pass = (W_f - W_i)/NW;
	TGraph *gr;
	TF1 *f1 = new TF1("f1",EqStr,0,1);
	TCanvas *cComp[NComp];
	for (int i = 0 ; i < NW ; i ++){
		_W[i] = W_i + i*W_pass ;
		double W_2 = _W[i]*_W[i];
		double DiffCS[Nt];
		double t[Nt];
		double Cos[Nt];
		double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
		if (t_lim_m < -2.5) t_lim_m = -2.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		for (int j = 0; j < Nt; j++){
			t[j] = t_lim_m + j* t_pass;
			double z = VM_Mand_z(W_2,Q_2,t[j],MP,MV);
			DiffCS[j] = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t[j],Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
			Cos[j] = (W_2*W_2 + W_2*(2*t[j] - Mand_Sigma(0,MP,MV,MP)) + (0 - MP_2)*(MV_2))/sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) ) ;
		}
		// fitting parameters
		gr = new TGraph(Nt,Cos,DiffCS);// create graph
		gr->Fit("f1","QR");
		for (int n1 = 0; n1 <= NComp; n1++)
			if (i == n1*(int)((double)NW/NComp)){
				TString WComp = "%f";
				WComp = Form(WComp.Data(),_W[i]);
				cComp[n1] = new TCanvas("cComp" + WComp,"",200,10,600,400);
				gr->GetYaxis()->SetNdivisions(5);
				gr->GetYaxis()->SetTitle("d#sigma/cos #theta (mb/rad)");
				gr->GetXaxis()->SetTitle("cos #theta (rad)");
				gr->Draw("AC");
				cComp[n1]->Update();
				TLatex nComp;
				double X = (cComp[n1]->GetUxmax() - cComp[n1]->GetUxmin())/10;
				double Y = (cComp[n1]->GetUymax() - cComp[n1]->GetUymin())/10;
				TString Chi = "%f";
				double CvPass = 0.8;
				Chi = Form(Chi.Data(),f1->GetChisquare()/(Nt - PAR));
				nComp.DrawLatex(cComp[n1]->GetUxmin() + X, cComp[n1]->GetUymax() - CvPass*Y, EqStrLtx);
				nComp.DrawLatex(cComp[n1]->GetUxmin() + X, cComp[n1]->GetUymax() - 2*CvPass*Y, "W = " + WComp);
				for (int n2 = 0; n2 < PAR; n2++){
					TString PARstr = "%f";
					PARstr = Form(PARstr.Data(),f1->GetParameter(n2));
					TString NPar = "%d";
					NPar = Form(NPar.Data(),n2);
					nComp.DrawLatex(cComp[n1]->GetUxmin() + X, cComp[n1]->GetUymax() - (3*CvPass + CvPass*n2)*Y, "Par[" + NPar + "] = " + PARstr);
				}
				nComp.DrawLatex(cComp[n1]->GetUxmin() + X, cComp[n1]->GetUymax() - (3*CvPass + CvPass*PAR)*Y, "Chi-Square = " + Chi);
				cComp[n1]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Rho_FittComp_" + WComp + ".pdf");
			}
		double _par[PAR];
		f1->GetParameters(&_par[0]);
		par[PAR][i] = f1->GetChisquare()/(Nt - PAR);
		for (int k = 0; k < PAR; k++){
			par[k][i] = _par[k];
			parErr[k][i] = f1->GetParError(k)/_par[k];
			parErr[k][i] = sqrt(parErr[k][i]*parErr[k][i]);
//			cout << " W = " << _W[i] << ", par[" << k << "][" << i << "] = " << par[k][i] << ", ChiSqr = " << par[PAR][i] << endl;
		}
//		cout << " W = " << _W[i] << ", ChiSqr = " << par[PAR][i] << endl;
	}
		// drawing
	TString EfStr = "%d";
	EfStr = Form(EfStr.Data(),(int)W_f);
	TLatex n;
	Double_t cpass, cmax, cmin, y1, y2;
	TCanvas *c[PAR + 1];
	TGraph *GR[PAR + 1]; //beside Chi square
	for (int i = 0; i <= PAR; i++){
		GR[i] = new TGraph(NW,_W,par[i]);
		TString NCv = "%d";
		NCv = Form(NCv.Data(),i);
		c[i] = new TCanvas("c" + NCv,"",200,10,600,400);
		GR[i]->GetYaxis()->SetNdivisions(5);
		if (i == PAR) GR[i]->GetYaxis()->SetTitle("Chi-Square");
			else GR[i]->GetYaxis()->SetTitle("Parameter " + NCv);
		GR[i]->GetXaxis()->SetTitle("W (GeV)");
		GR[i]->Draw("AC");
		c[i]->Update();
		cmax  = c[i]->GetUymax();
		cmin  = c[i]->GetUymin();
		cpass = TMath::Abs(cmax - cmin)/10.;
		y1 = cmax -     cpass;
		y2 = cmax - 2 * cpass;
		n.SetTextSizePixels(20);
		double X = (W_f - W_i)/10;
		if (i == PAR) n.DrawLatex(X, y1 , "Chi-Square");
			else  n.DrawLatex(X, y1 , "Parameter " + NCv);
		n.DrawLatex(X, y2, EqStrLtx);
		if (i == PAR) c[i]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Rho_Par_ChiSq.pdf");
			else c[i]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Rho_Par_" + NCv + "_" + EfStr + ".pdf");
	}
}
