/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	gROOT->ProcessLine(".L base/CrispParticleTable.cc");
     CrispParticleTable *cpt = CrispParticleTable::Instance();
     cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc");
     gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc");
	
	double MV = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MP = CPT::p_mass/1000.;
	double MV_2 = TMath::Power(MV,2);
	double MP_2 = TMath::Power(MP,2);
	double Q_2  = 0.0;
	double W = 55;
	double W_2 = W*W;
	double cos = -1;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 1000;
	double DiffCS[N];
	double t[N];
	double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
	double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
	t_lim_m = -0.5;
	double t_pass = (t_lim_p - t_lim_m)/N;
	for (int j = 0; j < N; j++){
		t[j] = t_lim_m + j* t_pass;
		double z = VM_Mand_z(W*W,Q_2,t[j],MP,MV);
		DiffCS[j] = 1e-2*_GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t[j],Q_2);
		t[j] = TMath::Sqrt(t[j]*t[j]);
	}

	// create graph
	TGraph *gr = new TGraph(N,t,DiffCS);
	gr->GetYaxis()->SetNdivisions(52);
	gr->GetYaxis()->SetRangeUser(1e-6,1.1);
	gr->GetYaxis()->SetTitle("Diff Cros Section (mb)");
	gr->GetXaxis()->SetTitle("|t| (GeV^{2})");
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->Draw("AC");
	gPad->SetLogy();
	cout << W << endl;
	TString EneStr = "%d";
	EneStr = Form(EneStr.Data(),(int)W);
	c1->Print("results/root_scripts/VectorMesons/DiffCS_Rho_"+ EneStr + ".pdf");
}

