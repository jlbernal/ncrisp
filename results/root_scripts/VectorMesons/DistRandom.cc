{
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".include base");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc+");
	gROOT->ProcessLine(".include Amplitude/CrossSection");
	gROOT->ProcessLine(".L Amplitude/Meson_n_Distr.cc+");
	cout << std::endl;
	cout << "Loading PDG Particle Data ..." << endl;
	CrispParticleTable *cpt = CrispParticleTable::Instance();  
	cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");
	cout << "Ready." << endl;

	TCanvas *c1 = new TCanvas("c1","c1",600,800);
	c1->SetFillColor(42);
	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gPad->SetLogy();
	gPad->SetLogx();

	Double_t s = 2.9566 * 1e6;
	std::cout <<  "RhoN_PionN_AngDist 1 , s = " << s/1000./1000.  << std::endl;
	OmegaN_PionN_AngDist;
	TF1 *f9 = new TF1("RhoN_PionN_AngDist", OmegaN_PionN_AngDist, -1, 1, 1); // due to the simmilar mass of rho and omega meson we use the same angular distribution
	Double_t s1;
	s1 = (s < 2967000)? 2967000:s;
	std::cout << "s1 = " << s1 << std::endl;
	f9->SetParameter(0,s1/1000/1000); //MeV^2 -> GeV^2
	f9->Draw();
/*	f9->SetNpx(1000);
	for (int i = 0; i < 100; i++){
		cosTheta = f9->GetRandom();
		std::cout <<  "RhoN_PionN_AngDist 2 , cosTheta = " <<  cosTheta  << std::endl;
	}
*	delete f9;
*/}

