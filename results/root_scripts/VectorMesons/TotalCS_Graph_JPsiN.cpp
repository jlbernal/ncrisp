
double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos){
	const Double_t gJDD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_p_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );
	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t Ma_2_0 = 8*gJDD*gJDD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Ma_2_1 = 1./(t - mD*mD) + 1./(mD*mD + mj*mj - t); //intercambio
	Double_t Ma_2_2 = (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Ma_2_3 = TMath::Power( (mj*mj + mD*mD - t)/2.,2 )- mj*mj*mD*mD;
	Double_t Ma_2 = Ma_2_0*TMath::Power(Ma_2_1, 2)*Ma_2_2*Ma_2_3;
	Double_t fluxo_2 = ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s );
	if ( fluxo_2 > 0)
		Ma_2 = Ma_2 * TMath::Sqrt( fluxo_2 );
	else Ma_2 = 0;
	Double_t result =  2*Ma_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
	return result;
}
double VM_Diff_CrossSection_NJPsi__HypDb(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDastNLam =-19.;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );
	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t pj_p = (2*s + t - mj*mj - 2*mN*mN - mD*mD)/2.;
	Double_t pj_q = (mj*mj - mD*mD + t)/2.;
	Double_t p_2 = 2*(mN*mN + mlambda*mlambda )- t;
	Double_t Mb_2_0 = gJDastD*gJDastD*gDastNLam*gDastNLam/(3*mj*mj); 
	Double_t Mb_2_1 = 1./(t - mDast*mDast)/(t - mDast*mDast); //intercambio
	Double_t Mb_2_2 = mj*mj*(p_2*t - TMath::Power( mlambda*mlambda - mN*mN,2) );
	Double_t Mb_2_3 = 2*pj_p*pj_q*( mlambda*mlambda - mN*mN);
	Double_t Mb_2_4 = p_2*pj_q*pj_q + t*pj_p*pj_p;
	Double_t Mb_2_5 = 4*( (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda)* ( mj*mj*t - pj_q*pj_q) ;
	Double_t Mb_2 = Mb_2_0*Mb_2_1 * (Mb_2_2 + Mb_2_3 - Mb_2_4 - Mb_2_5 );
	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Mb_2 = Mb_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mb_2 = 0;
	return 2*Mb_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
}
double VM_Diff_CrossSection_NJPsi__HypDast(double s, double cos){
	const Double_t gJDastD = 7.64;
	const Double_t gDNLam = 15.8;
//	const Double_t gDNLam = 14.8;   //These is the real value but result is shorter than the one in the article but same shape.... still doubt
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mDast*mDast,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mDast*mDast - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mDast,mlambda) - s );

	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);
	Double_t Mc_2_0 = 4*gJDastD*gJDastD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Mc_2_1 = 1./(t - mD*mD)/(t - mD*mD); //intercambio
	Double_t Mc_2_2 =(mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Mc_2_3 = TMath::Power( (mj*mj + mDast*mDast - t)/2. ,2) - mj*mj*mDast*mDast;
	Double_t Mc_2 = Mc_2_0 * Mc_2_1 * Mc_2_2 * Mc_2_3;
	if ((( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ))  >0 )
		Mc_2 = Mc_2 * TMath::Sqrt( ( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mc_2 = 0;
	return 2*Mc_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
}


double VM_Diff_CrossSection_NJPsi__HypDDbarFF(Double_t s, Double_t t, Double_t s1){
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s ) * ( TMath::Power( mN - mj,2) - s )/4/s;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t)/(t - mD*mD) ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t Md_2 = Md_2_0* Md_2_1*Md_2_2;
	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
//	cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Md_2_0 = " << Md_2_0  << ", Md_2_1 = " << Md_2_1  << ", Md_2_2 = " << Md_2_2 << ", sigma = " << SigmaDN << ", Diff CS = " << Md_2 << endl;
	return Md_2*SigmaDN;
}

double VM_Diff_CrossSection_NJPsi__HypDDbar(Double_t s, Double_t t, Double_t s1){
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s ) * ( TMath::Power( mN - mj,2) - s )/4/s;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
	Double_t Md_2_1 = TMath::Power( 1/(t - mD*mD) ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t Md_2 = Md_2_0* Md_2_1*Md_2_2;
	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
//	cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Md_2_0 = " << Md_2_0  << ", Md_2_1 = " << Md_2_1  << ", Md_2_2 = " << Md_2_2 << ", sigma = " << SigmaDN << ", Diff CS = " << Md_2 << endl;
	return Md_2*SigmaDN;
}

void TotalCS_Graph_JPsiN(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Double_t s_max = 200;
	const Double_t s_min = 17.3;
	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ns = 1000;
	const Int_t Ncos = 1000;
	const Int_t Nt = 100;
	const Int_t Ns1 = 100;
	const Double_t s_delta = (s_max - s_min)/Ns;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3

	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;

	double sPoint[Ns], csHypDa[Ns], csHypDb[Ns], csHypDast[Ns], csHypDDbar[Ns], csHypDDbarFF[Ns], csHypD_Dast[Ns], csHypD[Ns], csJPsiN[Ns];
	for (int i = 0; i < Ns; i ++){
		double s = s_min + i*s_delta;
		double tcsHypDa = 0., tcsHypDb = 0., tcsHypDast = 0., tcsHypDDbar = 0., tcsHypDDbarFF = 0.;
		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			tcsHypDa = tcsHypDa 	+ VM_Diff_CrossSection_NJPsi__HypDa(s, cosTheta)*cos_delta;
			tcsHypDb = tcsHypDb 	+ VM_Diff_CrossSection_NJPsi__HypDb(s, cosTheta)*cos_delta;
			if (s > 18.5) tcsHypDast = tcsHypDast + VM_Diff_CrossSection_NJPsi__HypDast(s, cosTheta)*cos_delta;
		}

		Double_t s1_max = TMath::Power( TMath::Sqrt(s) - mD, 2);
		Double_t s1_min = TMath::Power(mN + mD,2);
		if (s1_max > s1_min){
			Double_t s1_delta = (s1_max - s1_min)/Ns1;
			for (int j = 0; j < Ns1; j ++){
				double s1 = s1_min + j*s1_delta;
				double tempIntT = 0, tempIntTFF = 0;
				double t_max = 0.5*(sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,s1) )/s - (mj*mj - mN*mN)*(mD*mD - s1)/s + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s );
				double t_min = 0.5*(-1*sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,s1) )/s - (mj*mj - mN*mN)*(mD*mD - s1)/s + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s );
				if (t_max <= t_min) cout << " t something wrong\n";
				Double_t t_delta = (t_max - t_min)/Nt;
				for (int k = 0; k < Nt; k ++){
					double t = t_min + k*t_delta;
					Double_t temp = VM_Diff_CrossSection_NJPsi__HypDDbar(s, t, s1)*t_delta;
					tempIntT = tempIntT + temp;
					Double_t tempFF = VM_Diff_CrossSection_NJPsi__HypDDbarFF(s, t, s1)*t_delta;
					tempIntTFF = tempIntTFF + tempFF;
				}
				tcsHypDDbar = tcsHypDDbar + tempIntT*s1_delta;
				tcsHypDDbarFF = tcsHypDDbarFF + tempIntTFF*s1_delta;
			}
		}
		sPoint[i] = TMath::Sqrt(s);
		csHypDa[i] = tcsHypDa*_GeV2_mbar;
		csHypDb[i] = tcsHypDb*_GeV2_mbar;
		csHypDast[i] = tcsHypDast;
		csHypDDbar[i] = tcsHypDDbar/_GeV2_mbar;
		csHypDDbarFF[i] = tcsHypDDbarFF/_GeV2_mbar;
		csHypD[i] = tcsHypDa*_GeV2_mbar + tcsHypDb*_GeV2_mbar;
		csHypD_Dast[i] = tcsHypDa*_GeV2_mbar + tcsHypDb*_GeV2_mbar + tcsHypDast;
		csJPsiN[i] = tcsHypDa*_GeV2_mbar + tcsHypDb*_GeV2_mbar + tcsHypDast + tcsHypDDbarFF/_GeV2_mbar;
	}
	TGraph *gHypDa = new TGraph(Ns,sPoint,csHypDa);
	TGraph *gHypDb = new TGraph(Ns,sPoint,csHypDb);
	TGraph *gHypDast = new TGraph(Ns,sPoint,csHypDast);
	TGraph *gHypDDbar = new TGraph(Ns,sPoint,csHypDDbar);
	TGraph *gHypDDbarFF = new TGraph(Ns,sPoint,csHypDDbarFF);
	TGraph *gHypD = new TGraph(Ns,sPoint,csHypD);
	TGraph *gHypD_Dast = new TGraph(Ns,sPoint,csHypD_Dast);
	TGraph *gJPsiN = new TGraph(Ns,sPoint,csJPsiN);

	gHypDa->SetLineStyle(4);
	gHypDa->SetLineColor(3);
	gHypDa->SetLineWidth(1);

	gHypDb->SetLineStyle(4);
	gHypDb->SetLineColor(3);
	gHypDb->SetLineWidth(1);

	gHypD->SetLineStyle(4);
	gHypD->SetLineColor(3);
	gHypD->SetLineWidth(2);

	gHypD_Dast->SetLineStyle(1);
	gHypD_Dast->SetLineColor(3);
	gHypD_Dast->SetLineWidth(2);


	gHypDast->SetLineStyle(3);
	gHypDast->SetLineColor(2);
	gHypDast->SetLineWidth(2);

	gHypDDbar->SetLineStyle(2);
	gHypDDbar->SetLineColor(4);
	gHypDDbar->SetLineWidth(2);

	gHypDDbarFF->SetLineStyle(2);
	gHypDDbarFF->SetLineColor(5);
	gHypDDbarFF->SetLineWidth(2);

	gJPsiN->SetLineStyle(1);
	gJPsiN->SetLineColor(1);
	gJPsiN->SetLineWidth(2);

	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("");
	mg->Add(gHypDa,"l");
	mg->Add(gHypDb,"l");
	mg->Add(gHypDast,"l");
	mg->Add(gHypDDbar,"l");
	mg->Add(gHypDDbarFF,"l");


	TCanvas *c1 = new TCanvas("c1","J/#Psi N",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);

	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("A");				
	mg->GetYaxis()->SetTitle("T_{A}");				
	mg->GetYaxis()->CenterTitle();				
	mg->GetYaxis()->SetRangeUser(0,5);
	mg->GetXaxis()->SetRangeUser(4,15);

	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	leg->SetFillColor(10);
	leg->AddEntry(gHypDa,"gHypDa","lep");
	leg->AddEntry(gHypDb,"gHypDb","lep");
	leg->AddEntry(gHypDast,"gHypDast","lep");
	leg->AddEntry(gHypDDbar,"gHypDDbar","lep");
	leg->AddEntry(gHypDDbarFF,"gHypDDbarFF","lep");
	leg->Draw("same");

	TCanvas *c2 = new TCanvas("c2","HypDb",600,800);
	c2->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c2->SetFrameFillColor(10);
	c2->SetFillColor(10);

	TMultiGraph *mg1 = new TMultiGraph();
	mg1->SetTitle("");
	mg1->Add(gHypD,"l");
	mg1->Add(gHypD_Dast,"l");
	mg1->Add(gHypDast,"l");
	mg1->Add(gHypDDbarFF,"l");
	mg1->Add(gJPsiN,"l");

	mg1->Draw("AP");
	mg1->GetXaxis()->SetTitle("A");				
	mg1->GetYaxis()->SetTitle("T_{A}");				
	mg1->GetYaxis()->CenterTitle();				
	mg1->GetYaxis()->SetRangeUser(0,5);
	mg1->GetXaxis()->SetRangeUser(4,15);

	TLegend* leg1 = new TLegend(.75,.80,.95,.95);
	leg1->SetHeader("Legend");
	leg1->SetFillColor(10);
	leg1->AddEntry(gHypD,"J/#Psi + N -> #Lambda #bar{D}","l");
	leg1->AddEntry(gHypD_Dast,"J/#Psi + N -> #Lambda #bar{D} + #Lambda #bar{D}^{#ast}","l");
	leg1->AddEntry(gHypDast,"J/#Psi + N -> #Lambda #bar{D}^{#ast}","l");
	leg1->AddEntry(gHypDDbarFF,"J/#Psi + N -> N D #bar{D}","l");
	leg1->AddEntry(gJPsiN,"J/#Psi + N","l");
	leg1->Draw("same");
}
