{
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	TGraph *GRho = CPT::Instance()->GetT_rho();
	TGraph *GOmg = CPT::Instance()->GetT_omg();
	TGraph *GPhi = CPT::Instance()->GetT_phi();
	TGraph *GJPsi = CPT::Instance()->GetT_J_Psi();

	const Double_t b_v[4] 		= {11, 		10, 	7, 	4};
	const Double_t X[4]   		= {5, 		0.55, 	0.34, 	0.0015};
	const Double_t epsilon[4]   	= {0.22, 	0.22, 	0.22, 	0.80};
	const Double_t Y[4]  		= {26, 		18, 	0, 	0};
	const Double_t eta[4] 		= {1.23, 	1.92, 	0, 	0};
	const Double_t f_v[4] 		= {2.02, 	23.1, 	13.7, 	10.4};
	TF1 *f = new TF1("f","[0] * ( [1]*pow(x,[2] ) + [3]*pow(x,-1*[4] ) )/1000 ",40,150);
	TF1 *f1 = new TF1("f1","[0] * ( [1]*pow(x,[2] ) )/1000 ",40,150);


	for(int i = 0; i < 4; i++){
		f->SetParameters(b_v[i], X[i], epsilon[i], Y[i], eta[i]);
		f1->SetParameters(b_v[i], X[i], epsilon[i]);
		if (i == 0) GRho->Fit("f", "WR");
		if (i == 1) GOmg->Fit("f", "WR");
		if (i == 2) GPhi->Fit("f1", "WR");
		if (i == 3) GJPsi->Fit("f1", "WR");
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
//	gPad->SetLogx();
//	gPad->SetLogy();

	GRho->SetMarkerStyle(21);
	GRho->SetMarkerSize(0.5);
	GOmg->SetMarkerStyle(22);
	GOmg->SetMarkerSize(0.5);
	GPhi->SetMarkerStyle(23);
	GPhi->SetMarkerSize(0.5);
	GJPsi->SetMarkerStyle(24);
	GJPsi->SetMarkerSize(0.5);

	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("Omega meson's transparency at 1.1 GeV. Ressonance model.");
	mg->Add(GRho);
	mg->Add(GOmg);
	mg->Add(GPhi);
	mg->Add(GJPsi);

	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("W(GeV)");				
	mg->GetYaxis()->SetTitle("#sigma (mbar)");				
	mg->GetYaxis()->SetRangeUser(1e-2,0.0014);
	mg->GetXaxis()->SetRangeUser(0,150);

	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	leg->AddEntry(GRho,"#rho","p");
	leg->AddEntry(GOmg,"#omega","p");
	leg->AddEntry(GPhi,"#phi","p");
	leg->AddEntry(GJPsi,"J/#Psi","p");
	leg->Draw("same");

}
