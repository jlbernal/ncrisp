/* ================================================================================
 * 
 * 	Copyright 2015 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include "TF1.h"
#include "TCanvas.h"

const double mRho = 775.49/1000.;
const double mProton = 938.2723/1000.; //proton

double Trg_Fun(double x, double y, double z){
	return x*x + y*y + z*z - 2*x*y -2*y*z -2*x*z;
}

double Momentum(double *x, double *p){
	
	double thrld = mRho+mProton;
	
	double s = x[0];
	//double s = pow(x[0] + thrld,2.);
	double y = p[0];
	double z = p[1];
	
	return (s*s + y*y + z*z - 2*s*y -2*y*z -2*s*z)/(4.*s);
}

double RhoN_Total_CS_ResM(double *x, double *p){
	
	const int N = 9;
	
	///F15, D13, P11, P13, F17, D33, S31, F35, P31
	double I_R[N] = {5./2., 3./2., 1./2., 3./2., 7./2., 3./2., 1./2., 5./2., 1./2.};
	double M_R[N] = {1.680, 1.700, 1.710, 1.720, 1.990, 1.700, 1.900, 1.905, 1.910}; //GeV
	double Width_R[N] = {.125, .100, .110, .150, .260, .280, .150, .350, .250}; //GeV
	double Width_RhoN[N] = {0.05*Width_R[0], 0.1*Width_R[1], 0.2*Width_R[2], 0.7*Width_R[3], 0.05*Width_R[4], 0.3*Width_R[5], 0.45*Width_R[6], 0.72*Width_R[7], 0.37*Width_R[8]}; //GeV
	
	double mRho2 = mRho*mRho;
	double mN2 = mProton*mProton;
	double thrld = mRho+mProton;
	
	double s = x[0];
	//double s = pow(x[0] + thrld,2.);
	
	double sum = 0;
	for(int i=0; i<N; i++){
	
		sum += (2.*I_R[i] + 1) * ( Width_RhoN[i] * Width_R[i] ) 
			/ ( pow(sqrt(s) - M_R[i],2.) + pow(Width_R[i],2.)/4. ); 
	}
	
	return ( TMath::Pi()/(6.*Trg_Fun(s,mRho2,mN2)/(4.*s)) ) * sum * 0.388 * (TMath::Pi()*sqrt(Trg_Fun(s,mRho2,mN2)/(4.*s))*pow(s,-1./2.)); //GeV^-2 --> milibarn
}

double RhoN_partials_CS_ResM(double *x, double *p){
	
	const int N = 9;
	
	///F15, D13, P11, P13, F17, D33, S31, F35, P31
	double I_R[N] = {5./2., 3./2., 1./2., 3./2., 7./2., 3./2., 1./2., 5./2., 1./2.};
	double M_R[N] = {1.680, 1.700, 1.710, 1.720, 1.990, 1.700, 1.900, 1.905, 1.910}; //GeV
	double Width_R[N] = {.125, .100, .110, .150, .260, .280, .150, .350, .250}; //GeV
	double Width_RhoN[N] = {0.05*Width_R[0], 0.1*Width_R[1], 0.2*Width_R[2], 0.7*Width_R[3], 0.05*Width_R[4], 0.3*Width_R[5], 0.45*Width_R[6], 0.72*Width_R[7], 0.37*Width_R[8]}; //GeV
	
	double mRho2 = mRho*mRho;
	double mN2 = mProton*mProton;
	double thrld = mRho+mProton;
	
	double s = x[0];
	//double s = pow(x[0] + thrld,2.);
	
	double sum = 0;
	for(int i=p[0]; i<p[0]+1; i++){
	
		sum += (2.*I_R[i] + 1) * ( Width_RhoN[i] * Width_R[i] ) 
			/ ( pow(sqrt(s) - M_R[i],2.) + pow(Width_R[i],2.)/4. ); 
	}
	
	return ( TMath::Pi()/6.) * sum * 0.388; //GeV^-2 --> milibarn
}


void RhoTotalFSI_ResModel(){
	
	double thrld = mRho+mProton;
	double mRho2 = mRho*mRho;
	double mN2 = mProton*mProton;
	
	cout << "threshold: " << thrld << endl;
	
///Full cross section	
	TF1* RhoN_total_Res = new TF1("RhoN_CS_ResM",RhoN_Total_CS_ResM,2.937,4,1);
	RhoN_total_Res->SetNpx(1000);
	
	TCanvas *c1 = new TCanvas("c1","RhoN Total Cross Section",10,20,700,500);
	c1->SetFillColor(10);
	c1->SetBorderMode(0);
	
	RhoN_total_Res->Draw();
	RhoN_total_Res->GetXaxis()->SetTitle("s [GeV^{2}]");
	RhoN_total_Res->GetYaxis()->SetTitle("#sigma [mb]");
	//RhoN_total_Res->GetXaxis()->SetLimits(2.936,4);
	
	gPad->SetLogx();
	
	c1->Print("results/Figures/RhoN_TotalCS_ResM.eps");
	c1->Print("results/Figures/RhoN_TotalCS_ResM.pdf");
	
///Looking just at the resonances	
	TGraph *gT = new TGraph(RhoN_total_Res);
	gT->SetLineStyle(1);
	gT->SetLineColor(1);

	TF1* RhoN_partial_Res = new TF1("RhoN_CS_ResM",RhoN_partials_CS_ResM,0.,4,1);
	RhoN_partial_Res->SetNpx(1000);
	
	TGraph **g = new TGraph*[9];
	TMultiGraph *mg = new TMultiGraph();
	
	for(int i=0; i<9; i++){
		
		RhoN_partial_Res->SetParameter(0,i);
		g[i] = new TGraph(RhoN_partial_Res);
		g[i]->SetLineStyle(i+1);
		g[i]->SetLineColor(i+1);
		mg->Add(g[i], "L");
		
	}
	//mg->Add(gT, "L");
	
	TCanvas *c2 = new TCanvas("c2","RhoN Cross Section - Resonances",10,20,700,500);
	c2->SetFillColor(10);
	c2->SetBorderMode(0);
	
	mg->Draw("A");
	mg->GetXaxis()->SetTitle("s [GeV^{2}]");
	mg->GetYaxis()->SetTitle("#sigma [mb]");
	mg->GetXaxis()->SetLimits(2.937,4);
	
	gPad->SetLogx();
	
	c2->Print("results/Figures/RhoN_CS_ResModel_resonances.eps");
	c2->Print("results/Figures/RhoN_CS_ResModel_resonances.pdf");
	
	
///Função Triangular - momento da partícula rho	
	TF1* MomentumFunc = new TF1("MomentumFunc",Momentum,2.937,4,2);
	MomentumFunc->SetParameters(mRho2,mN2);
	MomentumFunc->SetNpx(1000);
	
	cout << "k^2(2.937) = " << MomentumFunc->Eval(2.937) << endl; 
	
	TCanvas *c3 = new TCanvas("c3","Momentum Function",10,20,700,500);
	c3->SetFillColor(10);
	c3->SetBorderMode(0);
	
	MomentumFunc->Draw();
	
	gPad->SetLogx();
	
	MomentumFunc->GetXaxis()->SetTitle("s [GeV^{2}]");
	MomentumFunc->GetYaxis()->SetTitle("k^{2} [GeV^{2}]");
 	//MomentumFunc->GetXaxis()->SetLimits(2.936,4);
	
	c3->Print("results/Figures/MomentumFunc.eps");
	c3->Print("results/Figures/MomentumFunc.pdf");
	

}

















