/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado y nunca puede ser menor que -4*m_pion^2 o produce una singularidad en el termino Reggeon;
************************************/
{

	double MV, MP, MV_2, MP_2; // masa en Gev (VERY IMPORTANT)

	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	double Q_2  = 0.0;
	double W_i = 0;
	double W_f = 400.;
	const int NW = 500;
	const int Nt = 10000;

	double W_pass = (W_f - W_i)/NW;
	double W[NW];

// Phi Meson
	double TotalCS_Phi  [NW];
	MV = CPT::phi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	MP = CPT::p_mass/1000.;
	MV_2 = TMath::Power(MV,2);
	MP_2 = TMath::Power(MP,2);
	for (int i = 0; i < NW; i++){
		W[i] = W_i + i*W_pass;
		double W_2 = TMath::Power(W[i],2);
		double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
		double z_lim_p = VM_Mand_z(W_2,Q_2,t_lim_p,MP,MV);
		double z_lim_m = VM_Mand_z(W_2,Q_2,t_lim_m,MP,MV);
//		cout << "t_lim_p = " << t_lim_p << ", t_lim_m = " << t_lim_m << ", z_lim_p = " << z_lim_p << ", z_lim_m = " << z_lim_m << endl;
		if (t_lim_m < -1.6) t_lim_m = -1.6;
		double t_pass = (t_lim_p -t_lim_m)/Nt;
		TotalCS_Phi[i] = 0;
		double DiffCS;			
		double _t;			
		for (int j = 0; j < Nt; j++){
			_t = t_lim_m + j*t_pass;
//			double z = -1*Mand_cos_theta_t(W_2,_t[j],MP,0,MP,MV);
			double z = VM_Mand_z(W_2,Q_2,_t,MP,MV);
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtPhi(z,_t,Q_2);
			TotalCS_Phi[i] += DiffCS*t_pass; 
//			if ((W[i] > 90) && (W[i]< 95))
//				cout << "t = " << _t << ", DiffCS = " << DiffCS << endl;
			}
//		TGraph TG_DiffCS(Nt, _t, DiffCS); 
//		TotalCS_Phi[i] = TG_DiffCS.Integral(); 
	}
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gPad->SetLogy();
	gPad->SetLogx();
//	TGraph *gr = new TGraph(NW,W,TotalCS_J_Psi);
	TGraph *gr = new TGraph(NW,W,TotalCS_Phi);
	gr->GetYaxis()->SetNdivisions(5);
	gr->GetYaxis()->SetRangeUser(1e-8,1);
	gr->GetXaxis()->SetRangeUser(1,1000.);
	gr->GetYaxis()->SetTitle("Total Cros Section (mb)");
	gr->GetXaxis()->SetTitle("W (GeV)");
	gr->Draw("AC");	
	c1->Print("TotalCS_Phi.pdf");
}


