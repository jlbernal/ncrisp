double Trg_Fun(double x, double y, double z){
	return x*x + y*y + z*z - 2*x*y -2*y*z -2*x*z;
}
double Mand_Sigma(double m1, double m2, double m3, double m4){
	return m1*m1 + m2*m2 + m3*m3 + m4*m4;
}
double VM_Diff_CrossSection_NJPsi__HypDDbar_s1FF(Double_t s, Double_t t, Double_t s1){
	Double_t gJDD = 7.64;
	Double_t mj = 3.096916; // all mass in GeV
	Double_t mN = 0.9395656;	
	Double_t mD = 1.8694;
	Double_t mlambda = 2.28646;
	Double_t mPion = 0.1349766;
	Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s ) * ( TMath::Power( mN - mj,2) - s )/4/s;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
//	Double_t Md_2_1 = TMath::Power( 1/(t - mD*mD) ,2);
	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t)/(t - mD*mD) ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t Md_2 = Md_2_0* Md_2_1*Md_2_2;
	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
//	cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Md_2_0 = " << Md_2_0  << ", Md_2_1 = " << Md_2_1  << ", Md_2_2 = " << Md_2_2 << ", sigma = " << SigmaDN << ", Diff CS = " << Md_2 << endl;
	return Md_2*SigmaDN;
}
double VM_Diff_CrossSection_NJPsi__HypDDbar_s1(Double_t s, Double_t t, Double_t s1){
	Double_t gJDD = 7.64;
	Double_t mj = 3.096916; // all mass in GeV
	Double_t mN = 0.9395656;	
	Double_t mD = 1.8694;
	Double_t mlambda = 2.28646;
	Double_t mPion = 0.1349766;
	Double_t Lambda = 3.1;
	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s ) * ( TMath::Power( mN - mj,2) - s )/4/s;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
	Double_t Md_2_1 = TMath::Power( 1/(t - mD*mD) ,2);
//	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t)/(t - mD*mD) ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t Md_2 = Md_2_0* Md_2_1*Md_2_2;
	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
//	cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Md_2_0 = " << Md_2_0  << ", Md_2_1 = " << Md_2_1  << ", Md_2_2 = " << Md_2_2 << ", sigma = " << SigmaDN << ", Diff CS = " << Md_2 << endl;
	return Md_2*SigmaDN;
}
void JPsiN_TotalCSd_s1(){
	Double_t gJDD = 7.64;
	Double_t mj = 3.096916; // all mass in GeV
	Double_t mN = 0.9395656;	
	Double_t mD = 1.8694;
	Double_t mlambda = 2.28646;
	Double_t mPion = 0.1349766;
	Double_t Lambda = 3.1;
	Double_t s_max = 10000;
	Double_t s_min = 25;
	const Int_t Ns = 50;
	Double_t s_delta = (s_max - s_min)/Ns;

	const Int_t Nt = 50;
	const Int_t Ns1 = 50;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3

	double sPoint[Ns], csHypDDbar[Ns], csHypDDbarFF[Ns];
	for (int i = 0; i < Ns; i ++){
		double s = s_min + i*s_delta;
		double tempIntS1 = 0, tempIntS1FF = 0;
		Double_t s1_max = TMath::Power( TMath::Sqrt(s) - mD, 2);
		Double_t s1_min = TMath::Power(mN + mD,2);
		if (s1_max <= s1_min) cout << "something wrong\n";
		Double_t s1_delta = (s1_max - s1_min)/Ns1;
		for (int j = 0; j < Ns1; j ++){
			double s1 = s1_min + j*s1_delta;
			double tempIntT = 0, tempIntTFF = 0;
			double t_max = 0.5*(sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,s1) )/s - (mj*mj - mN*mN)*(mD*mD - s1)/s + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s );
			double t_min = 0.5*(-1*sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,s1) )/s - (mj*mj - mN*mN)*(mD*mD - s1)/s + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s );
			if (t_max <= t_min) cout << " t something wrong\n";

			Double_t t_delta = (t_max - t_min)/Nt;
			for (int k = 0; k <= Nt; k ++){
				double t = t_min + k*t_delta;
				Double_t temp = VM_Diff_CrossSection_NJPsi__HypDDbar_s1(s, t, s1)*t_delta;
				tempIntT = tempIntT + temp;
				Double_t tempFF = VM_Diff_CrossSection_NJPsi__HypDDbar_s1FF(s, t, s1)*t_delta;
				tempIntTFF = tempIntTFF + tempFF;
//				cout << " s = " << s << ", t = " << t  << ", s1 = " << s1 << ", Diff CS = " << temp << endl;
			}
			tempIntS1 = tempIntS1 + tempIntT*s1_delta;
			tempIntS1FF = tempIntS1FF + tempIntTFF*s1_delta;
		}
		sPoint[i] = TMath::Sqrt(s);
		csHypDDbar[i] = tempIntS1/_GeV2_mbar;
		csHypDDbarFF[i] = tempIntS1FF/_GeV2_mbar;
	}
	TGraph *gHypDDbar = new TGraph(Ns,sPoint,csHypDDbar);
	TGraph *gHypDDbarFF = new TGraph(Ns,sPoint,csHypDDbarFF);
	gHypDDbar->SetMarkerStyle(20);
	gHypDDbarFF->SetMarkerStyle(21);
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gPad->SetLogy();
//	gPad->SetLogx();
	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("");
	mg->Add(gHypDDbar);
	mg->Add(gHypDDbarFF);

	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("A");				
	mg->GetYaxis()->SetTitle("T_{A}");				
	mg->GetYaxis()->CenterTitle();				
//	mg->GetYaxis()->SetRangeUser(0.1,125);
	mg->GetXaxis()->SetRangeUser(4,100);

	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetFillColor(10);
	leg->SetHeader("Legend");
	leg->AddEntry(gHypDDbar,"gHypDDbar","lep");
	leg->AddEntry(gHypDDbarFF,"gHypDDbarFF","lep");
	leg->Draw("same");

}




