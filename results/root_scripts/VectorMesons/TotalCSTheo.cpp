//Double_t onePionCrossSectionP(Double_t *xx, Double_t *par){
Double_t onePionCrossSectionE(Double_t E){
//	Double_t W = xx[0];
	// in microbarn (GEV)^1/2  
	const Double_t bp1 = 91., bp2 = 71.4; 
	// the energy in GeV
	Double_t x = -2. * ( E - 0.139 );
	return ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );  
}
Double_t onePionCrossSectionW(Double_t W){
//	Double_t W = xx[0];
	// in microbarn (GEV)^1/2  
	const Double_t bp1 = 91., bp2 = 71.4; 
	// the energy in GeV
	Double_t E = W*W/2/.9382723 - .9382723/2;
	Double_t x = -2. * ( E - 0.139 );
	return ( bp1 + bp2 / TMath::Sqrt(E) ) * ( 1. - exp(x) );  
}

void TotalCSTheo(){
//	TF1 *f1 = new TF1("myFunc",onePionCrossSectionP,0.1,70,0);
//	f1->Draw();
	Int_t N = 1000;
	TGraph *TE = new TGraph();
	TGraph *TW = new TGraph();
	TGraph *TWE = new TGraph();
	Double_t Emax = 7;
	Double_t Emin = 0.15;
	Double_t Epass = (Emax - Emin)/N;

	Double_t Wmax = 7;
	Double_t Wmin = 1.03;
	Double_t Wpass = (Wmax - Wmin)/N;
	TLorentzVector nucleon(0, 0, 0, .9382723); 

	for(int i = 0; i <= N; i++){
		Double_t W0 = Wmin + i*Wpass;
		Double_t CS = onePionCrossSectionW(W0);
		cout << "W0 = " << W0 << ", CS = " << CS << endl;
		TW->SetPoint(i, W0, CS);
	}
	for(int i = 0; i <= N; i++){
		Double_t E0 = Emin + i*Wpass;
		TLorentzVector gamma(E0, 0, 0, E0); 
		Double_t CS = onePionCrossSectionE(E0);
		Double_t WE = (gamma + nucleon).M();
		Double_t CSWE = onePionCrossSectionW(WE);
	
		cout << "E0 = " << E0 << ", CS = " << CS << endl;
		cout << "WE0 = " << E0 << ", CSWE = " << CSWE << endl;
		TE->SetPoint(i, E0, CS);
		TWE->SetPoint(i, E0, CSWE);
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TE->SetMarkerStyle(21);
	TE->SetMarkerSize(1);
	TE->SetLineWidth(3);
	TE->SetLineColor(1);
	TE->SetLineStyle(1);

	TWE->SetMarkerStyle(22);
	TWE->SetMarkerSize(1);
	TWE->SetLineWidth(3);
	TWE->SetLineColor(2);
	TWE->SetLineStyle(1);
	TE->Draw("Ap");
	TWE->Draw("SAME");

	TCanvas *c1 = new TCanvas("c1","c1",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TW->SetMarkerStyle(21);
	TW->SetMarkerSize(1);
	TW->SetLineWidth(3);
	TW->SetLineColor(1);
	TW->SetLineStyle(1);
	TW->Draw("Ap");
}
