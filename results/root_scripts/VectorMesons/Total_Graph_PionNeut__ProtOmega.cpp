/************************************
OBSERVACIONES
1. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	double MVP = CPT::pion_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MP = CPT::p_mass/1000.;
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MP_2 = TMath::Power(MP,2);
	double MN_2 = MN*MN;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 10000;
	const int Ns = 300;
	double Ppion[Ns], s[Ns], TCS[Ns], TCS1[Ns];
	double Ppion_0 = 0.8; Ppion_1 = 3;
	double Ppion_pass = (Ppion_1 - Ppion_0)/Ns; 
	for (int i = 0; i < Ns; i++){
		Ppion[i] = Ppion_0 + i* Ppion_pass;
		s[i] = TMath::Power(MN + sqrt(Ppion[i]*Ppion[i] + MVP_2) ,2) - Ppion[i]*Ppion[i];
//		cout << "Ppion[i] = " << Ppion[i] << ", s = " << s << endl;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],MVP_2,MN_2)*Trg_Fun(s[i],MVO_2,MP_2) )/s[i] - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s[i] + Mand_Sigma(MN,MVP,MP,MVO) - s[i] ); //Eq (13)
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],MVP_2,MN_2)*Trg_Fun(s[i],MVO_2,MP_2) )/s[i] - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s[i] + Mand_Sigma(MN,MVP,MP,MVO) - s[i] ); //Eq (13)
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/N;
		TCS[i] = 0;
		TCS1[i] = VM_Total_CrossSection_Npion_Nomega(s[i]);
		for (int j = 0; j < N; j++){
			t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s[i],t,MN,MVP,MP,MVO);
//			cout <<  "t = " << t[j] << ", cos theta = " << z << ", t_lim_p = " << t_lim_p << ", t_lim_m = " << t_lim_m << endl;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PionNeut__ProtOmega(s[i],t,z);
			TCS[i] =  TCS[i] + DiffCS * t_pass;
		}
		cout << "s = " << s[i] << ", t_lim_p = " << t_lim_p << ", t_lim_m = "<< t_lim_m << ", Ppion[" << i << "] = " << Ppion[i] << ", TCS[" << i << "] = " << TCS[i] << endl;
	}
	// create graph
	TGraph *gr = new TGraph(Ns,s,TCS);
	TGraph *gr1 = new TGraph(Ns,s,TCS1);
	gr1->GetYaxis()->SetNdivisions(5);
	gr1->GetYaxis()->SetRangeUser(1e-2,5.);
	gr1->GetXaxis()->SetRangeUser(0.5,30);
	gr1->GetYaxis()->SetTitle("Total Cros Section (#mu bar)");
	gr1->GetXaxis()->SetTitle("P_{#pi} (GeV/c)");
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr1->Draw("AC*");
	gr->Draw("Csame");
	gPad->SetLogy();
	c1->Print("Amplitude/Results/DiffCS/Total_PionNeut_OmegaProt.pdf");
}

