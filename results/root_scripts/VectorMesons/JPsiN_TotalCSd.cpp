double VM_Diff_CrossSection_NJPsi__HypDDbar_s1(double* x, double* par){
	double s1 = x[0];
	double s = par[0];
	double t = par[1];
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;

	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	// ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s1 ) * ( TMath::Power( mN - mj,2) - s1 )/4/s1;

	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);

	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t)/(t - mD*mD) ,2);

	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;

	Double_t SigmaDN;
	if ( ( ( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
	return Md_2_2*SigmaDN;
}
double VM_Diff_CrossSection_NJPsi__HypDDbar(double* x, double* par){
	double t = x[0];
	double s = par[0];

	TF1 *f2 = new TF1("f2", VM_Diff_CrossSection_NJPsi__HypDDbar_s1,1,10,2);
	f2->SetParameter(0,s);
	f2->SetParameter(1,t);
	return f2->Integral(1,10);;
}
void JPsiN_TotalCSd(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Double_t s_max = 100;
	const Double_t s_min = 25;
	const Int_t Ns = 100;
	const Double_t s_delta = (s_max - s_min)/Ns;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	double sPoint[Ns], csHypDDbar[Ns];
	for (int i = 0; i < Ns; i ++){
		double s = s_min + i*s_delta;
		TF1 *f1 = new TF1("f1", VM_Diff_CrossSection_NJPsi__HypDDbar,0,10,1);
		f1->SetParameter(0,s);
		sPoint[i] = TMath::Sqrt(s);
		csHypDDbar[i] = f1->Integral(-1,1);
	}
}




