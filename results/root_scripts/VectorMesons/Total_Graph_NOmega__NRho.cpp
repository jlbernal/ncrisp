/************************************
OBSERVACIONES
1. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	double MVR = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVR_2 = TMath::Power(MVR,2);
	double MN_2 = TMath::Power(MN,2);
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 10000;
	const int Ns = 300;
	double Pomega[Ns], TCS[Ns];
	double Pomega_0 = 0.05; 
	double Pomega_1 = 4;
	double Pomega_pass = (Pomega_1 - Pomega_0)/Ns; 
	for (int i = 0; i < Ns; i++){
		Pomega[i] = Pomega_0 + i* Pomega_pass;
		double s = TMath::Power(MN + sqrt(Pomega[i]*Pomega[i] + MVO_2) ,2) - Pomega[i]*Pomega[i];
		cout << "Pomega[i] = " << Pomega[i] << ", s = " << s << endl;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s,MVR_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVR_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVR,MN,MVO) - s ); //Eq (13)
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s,MVR_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVR_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVR,MN,MVO) - s ); //Eq (13)
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/N;
		TCS[i] = 0;
		for (int j = 0; j < N; j++){
			t = t_lim_m + j* t_pass;
//			cout <<  "t = " << t[j] << ", cos theta = " << z << ", t_lim_p = " << t_lim_p << ", t_lim_m = " << t_lim_m << endl;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NOmega__NRho(s,t);
			TCS[i] =  TCS[i] + DiffCS * t_pass;
		}
		TCS[i] = TMath::Abs(TCS[i]);
		cout << "s = " << s << ", t_lim_p = " << t_lim_p << ", t_lim_m = "<< t_lim_m << ", Pomega[" << i << "] = " << Pomega[i] << ", TCS[" << i << "] = " << TCS[i] << endl;
	}
	// create graph
	TGraph *gr = new TGraph(Ns,Pomega,TCS);
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->GetYaxis()->SetNdivisions(5);
	gr->GetYaxis()->SetRangeUser(1,13);
	gr->GetXaxis()->SetRangeUser(0.1,4);
	gr->GetYaxis()->SetTitle("Total Cros Section (m bar)");
	gr->GetXaxis()->SetTitle("P_{#omega} (GeV/c)");
	gr->Draw("AC");
	c1->Update();
	gPad->SetLogy();
	gPad->SetLogx();
	c1->Print("Amplitude/Results/DiffCS/Total_NOmega__NRho.pdf");
}

