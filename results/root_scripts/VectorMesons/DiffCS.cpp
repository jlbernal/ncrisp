{
	double MV = CPT::rho_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MP = CPT::p_mass/1000.;
	double MV_2 = TMath::Power(MV,2);
	double MP_2 = TMath::Power(MP,2);
	double Q_2  = 0.0;
	double W = 94.;
	double W_2 = W*W;
	double cos = -1;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 10;
	double DiffCS;
	double t;
	double t_lim_p = 0.5*(    sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
	double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)

//	cout << "PROBANDO. 1ero:  "<< (sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) ) + (Q_2 + MP_2)*(MV_2 - MP_2))/W_2 << ", 2do: "<<  (W_2 + Q_2 - MV_2- 2*MP_2 ) << endl;	
	double cos_p = -1*Mand_cos_theta_s(W*W,t_lim_p,MP,0,MP,MV);   
	double cos_m = -1*Mand_cos_theta_s(W*W,t_lim_m,MP,0,MP,MV);   
	cout << "t_lim_p: " << t_lim_p << ", cos_p: " << cos_p << ", t_lim_m: " << t_lim_m << ", cos_m: " << cos_m  <<  endl;
	t_lim_m = -2.5;
	double t_pass = (t_lim_p - t_lim_m)/N;
	for (int j = 0; j < N; j++){
		t = t_lim_m + j* t_pass;
		double z = -1*Mand_cos_theta_t(W*W,t,MP,0,MP,MV);   
		cos = -1*Mand_cos_theta_s(W*W,t,MP,0,MP,MV);   
		double z1 = -1*VM_Mand_z(W*W,Q_2,t,MP,MV);
		DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtRho(z,t,Q_2);
//		cout << " z1: " << z1 << " z: " << z << ", cos theta: " << cos <<  ",  t: " << t <<  ", la seccion diff es: " << DiffCS << endl;
	}
}

