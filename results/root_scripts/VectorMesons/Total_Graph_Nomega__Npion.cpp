{
	double MVR = CPT::rho_0_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVR_2 = TMath::Power(MVR,2);
	double MN_2 = TMath::Power(MN,2);
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 10000;
	const int Ns = 300;
	double Prho[Ns], TCS[Ns];
	double Prho_0 = 0; 
	double Prho_1 = 4;
	double Prho_pass = (Prho_1 - Prho_0)/Ns; 
	for (int i = 0; i < Ns; i++){
		Prho[i] = Prho_0 + i* Prho_pass;
		double s = TMath::Power(MN + sqrt(Prho[i]*Prho[i] + MVR_2) ,2) - Prho[i]*Prho[i];
		TCS[i] = VM_Total_CrossSection_Nrho_Npion(s);;
		cout << "s = " << s  << ", Prho[" << i << "] = " << Prho[i] << ", TCS[" << i << "] = " << TCS[i] << endl;
	}
	// create graph
	TGraph *gr = new TGraph(Ns,Prho,TCS);
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
//	gr->GetYaxis()->SetNdivisions(5);
	gr->GetYaxis()->SetRangeUser(4e-1,100);
	gr->GetXaxis()->SetRangeUser(0.5,4);
	c1->Update();
	gr->GetYaxis()->SetTitle("Total Cros Section (#mu bar)");
	gr->GetXaxis()->SetTitle("P_{#pi} (GeV/c)");
	gr->Draw("AC");
	gPad->SetLogy();
	gPad->SetLogx();
	c1->Print("Amplitude/Results/DiffCS/Total_NRho__NOmega.pdf");
}

