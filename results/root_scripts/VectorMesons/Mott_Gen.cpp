{
	const double _GeV2_mbar = 0.3894;	// Collins pag 3 1GeV⁻² = 0.3894 mb
	const double _Mev_Fermi = 197;		// 1 MeV⁻¹ = 197 fm
	const int Z = 1;
	double p_i = 0.1;
	double p_f = 2000.;
	const int Cases  = 50;
	const int Np = 500;
	const int Ntheta = 500;
	double p_pass = (p_f - p_i)/Np;
	double theta_pass = TMath::Pi()/Ntheta;
	double p[Np];
	double W[Np];
	double TotalCS[Np];
	const double MP = CPT::p_mass/1000.;
	double	Q_2 = 1./TMath::Power(CPT::r0/197*TMath::Power(240,1./3.) , 2)*1e-6;
	cout << "Virtualiy maxima es = " << Q_2 << endl;
	for (int i = 0; i < Np; i++){
		p[i] = p_i + i*p_pass;
		TLorentzVector P;
		P.SetXYZM(0.,0.,p[i],MP);
		TotalCS[i] = 0;
		for (int j = 1; j <= Ntheta; j++){
			double theta = theta_pass*j;
			double Diff = Mott_Diff_CrossSection_Sig(Z,P,theta);
			TotalCS[i] = TotalCS[i] + _GeV2_mbar*TMath::TwoPi()*Diff*TMath::Sin(theta)*theta_pass;
		}
		TLorentzVector PN, PT;
		PN.SetXYZM(0.,0.,0.,MP);
		PT = P + PN;
		W[i] = (PT).Mag2();
//		cout << "W[" << i << "] = " << W[i] << ", TotalCS[" << i << "] = " << TotalCS[i] << endl;;
	}

	TH1F *th1 = new TH1F("th1","Transfered momentum",50, 0, 2*p[Np-1]);
	Double_t phi ;
	Double_t prob;

	for (int k = 0; k < Cases; k++){
		phi = gRandom->Uniform() * TMath::Pi();
		prob = gRandom->Uniform();
		for (int i = 0; i < Np; i++){
			double pp = p_i + i*p_pass;
			TLorentzVector P;
			P.SetXYZM(0.,0.,pp,MP);
			double TotalCS_phi = 0;
			double theta = 0;
			int j = 0;
			while(theta <= phi){
				j++;
				theta = theta_pass*j;
				double Diff = Mott_Diff_CrossSection_Sig(Z,P,theta);
				TotalCS_phi = TotalCS_phi + _GeV2_mbar*TMath::TwoPi()*Diff*TMath::Sin(theta)*theta_pass;
			}
//			cout << "TotalCS_phi = " << TotalCS_phi << ", TotalCS[" << i << "] = " << TotalCS[i] << ", prob = " << prob << endl;
			if (TotalCS_phi/TotalCS[i]>prob){
				double Q = 2*p[i]*TMath::Sin(theta/2);
				th1->Fill(Q*Q);
			}else cout << "TotalCS_phi = " << TotalCS_phi << ", TotalCS[" << i << "] = " << TotalCS[i] << ", prob = " << prob << endl;
		}
	}
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,900,600);
	gPad->SetLogx();
	gPad->SetLogy();

	TGraph *gr = new TGraph(Np,W,TotalCS);
	gr->GetYaxis()->SetNdivisions(5);
//	gr->GetXaxis()->SetRangeUser(0,10);
	gr->GetYaxis()->SetTitle("Total Cros Section (mb)");
	gr->GetXaxis()->SetTitle("W^{2} (GeV^{2})");
	gr->Draw("AC");
	c1->Print("Amplitude/Results/Mott/TotalCS.pdf");
	TCanvas *c2 = new TCanvas("c2","Transfered Momentum Histogram",200,10,900,600);
	th1->GetYaxis()->SetNdivisions(5);
	th1->GetXaxis()->SetRangeUser(1,p_f);
	th1->GetYaxis()->SetTitle("Dist");
	th1->GetXaxis()->SetTitle("Transfered Momentum Q^{2} = |#vec{p} - #vec{p'}|^{2} (GeV^{2})");
	th1->Draw();
	c2->Print("Amplitude/Results/Mott/TransMomentum.pdf");
}
