
/*****************************************************
These values were obtained by fittin the CPT's TGraph in good agreement with experimental data
using the  following formula:
rho and omega vector meson cross section:
	TF1 *f = new TF1("f","[0] * ( [1]*pow(x,[2] ) + [3]*pow(x,-1*[4] ) )/1000 ",0,3000);
phi and J_Psi vector meson cross section:
	TF1 *f1 = new TF1("f","[0] * ( [1]*pow(x,[2] ) )/1000 ",0,3000);

****************************************
Minimizer is Minuit / Migrad
Chi2                      =  4.86587e-07
NDf                       =         3686
Edm                       =  3.81402e-07
NCalls                    =          432
p0                        =      2.41928   +/-   0.000456204 
p1                        =      1.36667   +/-   0.000259137 
p2                        =     0.284959   +/-   4.04028e-05 
p3                        =      71.6749   +/-   0.368106    
p4                        =       1.6616   +/-   0.00148023  

****************************************
Minimizer is Minuit / Migrad
Chi2                      =  4.20713e-06
NDf                       =         3686
Edm                       =  7.49937e-06
NCalls                    =          221
p0                        =      3.38212   +/-   0.00393919  
p1                        =     0.199673   +/-   0.000235108 
p2                        =      0.15405   +/-   0.000249596 
p3                        =     -1744.95   +/-   138.161     
p4                        =      2.70346   +/-   0.0180488   

****************************************
Minimizer is Minuit / Migrad
Chi2                      =  1.79025e-06
NDf                       =         3688
Edm                       =  2.64471e-06
NCalls                    =          143
p0                        =      2.36765   +/-   0.0101407   
p1                        =     0.113964   +/-   0.000488917 
p2                        =     0.328556   +/-   0.000765154 

****************************************
Minimizer is Minuit / Migrad
Chi2                      =   4.0658e-08
NDf                       =         3688
Edm                       =  5.84809e-08
NCalls                    =           63
p0                        =      2.55277   +/-   0.0100419   
p1                        =  0.000957289   +/-   8.4694e-06  
p2                        =     0.753558   +/-   0.00211188  

*****************************************************/

{
	Double_t p0[4] = {2.53228, -2.46407, 2.0793, 2.55352};
	Double_t p1[4] = {1.58588, -0.152171, 0.108622, 0.000957571};
	Double_t p2[4] = {0.24486, 0.275743, 0.365984, 0.753327};
	Double_t p3[2] = {-342.224, 76.1339};
	Double_t p4[2] = {5.54622, 2.48786};

	TF1 *f = new TF1("f","[0] * ( [1]*pow(x,[2] ) + [3]*pow(x,-1*[4] ) )/1000 ",20,7000);
	TF1 *f1 = new TF1("f1","[0] * ( [1]*pow(x,[2] ) )/1000 ",20,7000);

	TF1 *fv[4];
	fv[0] = new TF1("fv0","2.41928 * ( 1.36667*pow(x,0.284959 ) + 71.6749*pow(x,-1*1.6616 ) )/1000 ",20,7000);
	fv[1] = new TF1("fv1","3.38212 * ( 0.199673*pow(x,0.15405 ) + -1744.95*pow(x,-1*2.70346 ) )/1000 ",20,7000);
	fv[2] = new TF1("fv2","2.36765 * ( 0.113964*pow(x,0.328556 ) )/1000 ",20,7000);
	fv[3] = new TF1("fv3","2.55277 * ( 0.000957289*pow(x,0.753558 ) )/1000 ",20,7000);

	TString name[4] = {"#rho","#omega","#phi","J/#Psi"};
	TGraph *T[4];

/*	for(int i = 0; i < 2; i++){
		f->SetParameters(p0[i], p1[i], p2[i], p3[i], p4[i]);
		T[i] = new TGraph(f);
	}
	for(int i = 2; i < 4; i++){
		f1->SetParameters(p0[i], p1[i], p2[i]);
		T[i] = new TGraph(f1);
	}
*/
	for(int i = 0; i < 4; i++){
		T[i] = new TGraph(fv[i]);
	}
	TCanvas *c = new TCanvas("c","c",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
//	gPad->SetLogx();
	gPad->SetLogy();

	TMultiGraph *mg = new TMultiGraph();
	mg->SetTitle("Vector Meson's Total Cross Section.");
	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetHeader("Legend");
	for(int i = 0; i < 4; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		mg->Add(T[i]);
		leg->AddEntry(T[i],name[i].Data(),"p");
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("x(GeV)");				
	mg->GetYaxis()->SetTitle("#sigma (mbar)");				
	mg->GetYaxis()->SetRangeUser(1e-6,1);
	mg->GetXaxis()->SetRangeUser(-50,7000);
	leg->Draw("same");
}
