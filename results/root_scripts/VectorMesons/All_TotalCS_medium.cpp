/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado y nunca puede ser menor que -4*m_pion^2 o produce una singularidad en el termino Reggeon;
************************************/
void All_TotalCS_medium(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".include base");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const double _GeV2_mbar = 0.3894;	// Collins pag 3 1GeV⁻² = 0.3894 mb
	const double MN = CPT::n_mass/1000;
	const double Mrho = CPT::rho_0_mass/1000;
	const double Momega = CPT::omega_mass/1000;
	const double Mpion_n = CPT::pion_p_mass/1000;
	const double Mphi = CPT::phi_mass/1000;
	const double Msigma = CPT::sigma_mass/1000;
	const double MN_2 = MN*MN;
	const double Mrho_2 = Mrho*Mrho;
	const double Momega_2 = Momega*Momega;
	const double Mpion_n_2 = Mpion_n*Mpion_n;
	const double Mphi_2 = Mphi*Mphi;
	const double Msigma_2 = Msigma*Msigma;

	double s_i = 0;
	double s_f = 100.;
	const int Ns = 300;
	const int Nt = 1000;
	double s_pass = (s_f - s_i)/Ns;
	double s[Ns];

// Npion_Nomega
	double TotalCS_Npion_Nomega [Ns], TotalCS_Nomega_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t, DiffCSinv;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Npion_Nomega[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s[i],t,MN,Mpion_n,MN,Momega);
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_PionNeut__ProtOmega(s[i],t,z);
			TotalCS_Npion_Nomega[i] =  TotalCS_Npion_Nomega[i] + DiffCS * t_pass;
		}
	}
// Nomega_Npion
	double TotalCS_Nomega_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mpion_n_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mpion_n_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mpion_n,MN,Momega) - s[i] );
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_Npion[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s[i],t,MN,Momega,MN,Mpion_n);
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_ProtOmega__PionNeut(s[i],t,z);
			TotalCS_Nomega_Npion[i] =  TotalCS_Nomega_Npion[i] + DiffCS * t_pass;
		}
	}

// Nomega_Nrho
	double TotalCS_Nomega_Nrho [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_Nrho[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NOmega__NRho(s[i],t);
			TotalCS_Nomega_Nrho[i] =  TotalCS_Nomega_Nrho[i] + DiffCS * t_pass;
		}
	}

// Nrho_Nomega
	double TotalCS_Nrho_Nomega [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Mrho_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Mrho_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Mrho,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nrho_Nomega[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NRho__NOmega(s[i],t);
			TotalCS_Nrho_Nomega[i] =  TotalCS_Nrho_Nomega[i] + DiffCS * t_pass;
		}
	}
// Nomega_elastic
	double TotalCS_Nomega_elastic [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s[i],Momega_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Momega_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Momega,MN,Momega) - s[i] );
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s[i],Momega_2,MN_2)*Trg_Fun(s[i],Momega_2,MN_2) )/s[i] - (MN_2 - Momega_2)*(MN_2 - Momega_2)/s[i] + Mand_Sigma(MN,Momega,MN,Momega) - s[i] );
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/Nt;
		TotalCS_Nomega_elastic[i] = 0;
		for (int j = 0; j < Nt; j++){
			double t = t_lim_m + j* t_pass;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_NOmega_elastic(s[i],t);
			TotalCS_Nomega_elastic[i] =  TotalCS_Nomega_elastic[i] + DiffCS * t_pass;
		}
	}

// Nomega_N2pion
	double TotalCS_Nomega_N2pion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nomega_N2pion[i] = VM_Total_CrossSection_NOmega_2pionN(s[i],Ns);
	}

// Npion_Nomega
	double TotalCS_Npion_Nomega1 [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nomega1[i] = VM_Total_CrossSection_Npion_Nomega(s[i]);
	}

// Npion_phi
	double TotalCS_Npion_Nphi [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nphi[i] = VM_Total_CrossSection_Npion_Nphi(s[i]);
	}

// Npion_rho
	double TotalCS_Npion_Nrho [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Npion_Nrho[i] = VM_Total_CrossSection_Npion_Nrho(s[i]);
	}

// Nomega_Npion
	double TotalCS_Nomega_Npion1 [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nomega_Npion1[i] = VM_Total_CrossSection_Nomega_Npion(s[i]);
	}

// Nphi_Npion
	double TotalCS_Nphi_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nphi_Npion[i] = VM_Total_CrossSection_Nphi_Npion(s[i]);
	}

// Nrho_Npion
	double TotalCS_Nrho_Npion [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		TotalCS_Nrho_Npion[i] = VM_Total_CrossSection_Nrho_Npion(s[i]);
	}


	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ncos = 1000;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;

// NJPsi_ND
	double TotalCS_NJPsi_ND[Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypD = 0.;
		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			if (s[i] > 17.3) tcsHypD = tcsHypD + VM_Diff_CrossSection_NJPsi__HypD(s[i], cosTheta)*cos_delta;
		}
		TotalCS_NJPsi_ND[i] = tcsHypD*_GeV2_mbar;
	}

// NJPsi_NDast
	double TotalCS_NJPsi_NDast [Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypDast = 0.;
		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			if (s[i] > 18.5) tcsHypDast = tcsHypDast + VM_Diff_CrossSection_NJPsi__HypDast(s[i], cosTheta)*cos_delta;
		}
		TotalCS_NJPsi_NDast[i] = tcsHypDast;
	}

	const Int_t Nt = 100;
	const Int_t Ns1 = 100;
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
// NJPsi_NDDbar
	double TotalCS_NJPsi_NDDbar[Ns];
	for (int i = 0; i < Ns; i++){
		s[i] = s_i + i*s_pass;
		double tcsHypDDbar = 0.;
		Double_t s1_max = TMath::Power( TMath::Sqrt(s[i]) - mD, 2);
		Double_t s1_min = TMath::Power(mN + mD,2);
		if (s1_max > s1_min){
			Double_t s1_delta = (s1_max - s1_min)/Ns1;
			for (int j = 0; j < Ns1; j ++){
				double s1 = s1_min + j*s1_delta;
				double tempIntT = 0;
				double t_max = 0.5*(sqrt( Trg_Fun(s[i],mj*mj,mN*mN)*Trg_Fun(s[i],mD*mD,s1) )/s[i] - (mj*mj - mN*mN)*(mD*mD - s1)/s[i] + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s[i] );
				double t_min = 0.5*(-1*sqrt( Trg_Fun(s[i],mj*mj,mN*mN)*Trg_Fun(s[i],mD*mD,s1) )/s[i] - (mj*mj - mN*mN)*(mD*mD - s1)/s[i] + Mand_Sigma(mj,mN,mD,sqrt(s1)) - s[i] );
				if (t_max <= t_min) cout << " t something wrong\n";
				Double_t t_delta = (t_max - t_min)/Nt;
				for (int k = 0; k < Nt; k ++){
					double t = t_min + k*t_delta;
					Double_t temp = VM_Diff_CrossSection_NJPsi__HypDDbar(s[i], t, s1)*t_delta;
					tempIntT = tempIntT + temp;
				}
				tcsHypDDbar = tcsHypDDbar + tempIntT*s1_delta;
			}
		}		
		TotalCS_NJPsi_NDDbar[i] = tcsHypDDbar/_GeV2_mbar;
	}

	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	TGraph *gr  = new TGraph(Ns,s,TotalCS_Npion_Nomega);
	gr->GetYaxis()->SetNdivisions(10);
	gr->GetYaxis()->SetRangeUser(1e-2,500.);
	gr->GetXaxis()->SetRangeUser(2,100.);
	gr->GetYaxis()->SetTitle("Total Cros Section (mb)");
	gr->GetXaxis()->SetTitle("s (GeV^{2})");
	gr->SetLineWidth(1);
	gr->Draw("AC");
	TGraph *gr1 = new TGraph(Ns,s,TotalCS_Nomega_Npion);
	gr1->SetLineColor(2);
	gr1->SetLineWidth(2);
	gr1->Draw("Csame");
	TGraph *gr2 = new TGraph(Ns,s,TotalCS_Nomega_Nrho);
	gr2->SetLineStyle(2);
	gr2->SetLineWidth(1);
	gr2->Draw("Csame");
	TGraph *gr3 = new TGraph(Ns,s,TotalCS_Nrho_Nomega);
	gr3->SetLineStyle(2);
	gr3->SetLineWidth(2);
	gr3->SetLineColor(2);
	gr3->Draw("Csame");
	TGraph *gr4 = new TGraph(Ns,s,TotalCS_Nomega_elastic);
	gr4->SetLineStyle(3);
	gr4->SetLineWidth(1);
	gr4->Draw("Csame");
	TGraph *gr5 = new TGraph(Ns,s,TotalCS_Nomega_N2pion);
	gr5->SetLineStyle(4);
	gr5->SetLineColor(3);
	gr5->SetLineWidth(1);
	gr5->Draw("Csame");
	gPad->SetLogy();
	gPad->SetLogx();

	c1->Update();
	TLatex n;
	Double_t c1ymax  = c1->GetUymax();
	Double_t c1ypass = c1ymax/15.;
	n.DrawLatex(2.5,100,"Meson Exchange model");
	n.DrawLatex(2.5,50,"(Eur. Phys. J. A 6, 71(1999) )");
	TLine m[6];
	m[1].SetLineColor(2); m[2].SetLineStyle(2); m[3].SetLineColor(2); m[3].SetLineStyle(2); m[4].SetLineStyle(3); m[4].SetLineStyle(3); m[5].SetLineColor(3); m[5].SetLineStyle(4);
	TString sch[12] =  {"N#pi #rightarrow N#omega", "N#omega #rightarrow N#pi", "N#omega #rightarrow N#rho", "N#rho #rightarrow N#omega", "N#omega elastic", "N#omega #rightarrow N 2#pi ","N#pi #rightarrow N#omega" ,  "N#omega #rightarrow N#pi" ,"N#pi #rightarrow N#rho" ,  "N#rho #rightarrow N#pi", "N#pi #rightarrow N#phi" ,  "N#phi #rightarrow N#pi" };
	for (int i = 0; i < 6; i++){
		double yy = TMath::Power(10,c1ymax - (i+1)*2*c1ypass);
		double xx = 20; double xx1 = 30; double xx2 = 40;
		m[i].SetX1(20. );
		m[i].SetX2(30. );
		m[i].SetY1(yy);
		m[i].SetY2(yy);
		m[i].SetLineWidth(3);
		n.DrawLatex(xx2,yy,sch[i].Data());
		m[i].Draw("same");
		}
	c1->Print("Amplitude/Results/TotalCS/All_TotalCS_medium.pdf");

	TCanvas *c2 = new TCanvas("c2","Graph Draw Options",200,10,600,400);
	gPad->SetLogy();
	gPad->SetLogx();
	TGraph *grp  = new TGraph(Ns,s,TotalCS_Npion_Nomega1);
	grp->GetYaxis()->SetNdivisions(10);
	grp->GetYaxis()->SetRangeUser(1e-2,500.);
	grp->GetXaxis()->SetRangeUser(2,100);
	grp->GetYaxis()->SetTitle("Total Cros Section (mb)");
	grp->GetXaxis()->SetTitle("s (GeV^{2})");
	grp->SetLineWidth(2);
	grp->Draw("AC");
	TGraph *grp1 = new TGraph(Ns,s,TotalCS_Nomega_Npion1);
	grp1->SetLineColor(2);
	grp1->SetLineWidth(2);
	grp1->Draw("Csame");
	TGraph *grp2 = new TGraph(Ns,s,TotalCS_Npion_Nrho);
	grp2->SetLineStyle(2);
	grp2->SetLineWidth(2);
	grp2->Draw("Csame");
	TGraph *grp3 = new TGraph(Ns,s,TotalCS_Nrho_Npion);
	grp3->SetLineStyle(2);
	grp3->SetLineWidth(2);
	grp3->SetLineColor(2);
	grp3->Draw("Csame");
	TGraph *grp5 = new TGraph(Ns,s,TotalCS_Npion_Nphi);
	grp5->SetLineStyle(3);
	grp5->SetLineWidth(2);
	grp5->Draw("Csame");
	TGraph *grp6 = new TGraph(Ns,s,TotalCS_Nphi_Npion);
	grp6->SetLineStyle(3);
	grp6->SetLineWidth(2);
	grp6->SetLineColor(2);
	grp6->Draw("Csame");


	c2->Update();
	Double_t c2ymax  = c2->GetUymax();
	Double_t c2ypass = c2ymax/15.;
	TLatex n1;
	n1.DrawLatex(2.5,100,"Ressonance model");
	n1.DrawLatex(2.5,50,"(Z. Phys. A 358, 357(1997) )");
	TLine m1[6];
	m1[1].SetLineColor(2); m1[2].SetLineStyle(2); m1[3].SetLineColor(2); m1[3].SetLineStyle(2); m1[4].SetLineStyle(3); m1[4].SetLineStyle(3); m1[5].SetLineColor(2); m1[5].SetLineStyle(3);
	for (int i = 0; i < 6; i++){
		double yy = TMath::Power(10,c2ymax - (i+1)*2*c2ypass);
		double xx = 20; double xx1 = 30; double xx2 = 40;
		m1[i].SetX1(20. );
		m1[i].SetX2(30. );
		m1[i].SetY1(yy);
		m1[i].SetY2(yy);
		m1[i].SetLineWidth(3);
		n1.DrawLatex(xx2,yy,sch[i+6].Data());
		m1[i].Draw("same");
		}
	c2->Print("Amplitude/Results/TotalCS/All_TotalCS_medium_1.pdf");

	TCanvas *c3 = new TCanvas("c3","JPsi N",200,10,600,400);
	c3->SetFrameFillColor(10);
	c3->SetFillColor(10);
	TGraph *grNJPsi_ND  = new TGraph(Ns,s,TotalCS_NJPsi_ND);
	TGraph *grNJPsi_NDast  = new TGraph(Ns,s,TotalCS_NJPsi_NDast);
	TGraph *grNJPsi_NDDbar  = new TGraph(Ns,s,TotalCS_NJPsi_NDDbar);

	TMultiGraph *mg3 = new TMultiGraph();
	mg3->SetTitle("");
	mg3->Add(grNJPsi_ND,"l");
	mg3->Add(grNJPsi_NDast,"l");
	mg3->Add(grNJPsi_NDDbar,"l");
	mg3->Draw("AP");

	mg3->GetYaxis()->SetTitle("#sigma(mbar)");				
	mg3->GetXaxis()->SetTitle("s(GeV^{2})");				
	mg3->GetYaxis()->CenterTitle();				
	mg3->GetYaxis()->SetRangeUser(0,15);
	mg3->GetXaxis()->SetRangeUser(0,100);

	TLegend* leg3 = new TLegend(.75,.80,.95,.95);
	leg3->SetHeader("Legend");
	leg3->SetFillColor(10);
	leg3->AddEntry(grNJPsi_ND,"J/#Psi + N -> #Lambda #bar{D}","l");
	leg3->AddEntry(grNJPsi_NDast,"J/#Psi + N ->  #Lambda #bar{D}^{#ast}","l");
	leg3->AddEntry(grNJPsi_NDDbar,"J/#Psi + N -> N D #bar{D}","l");
	leg3->Draw("same");

}


