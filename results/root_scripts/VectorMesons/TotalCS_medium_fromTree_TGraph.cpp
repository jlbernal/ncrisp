{
	TGraph *TG[12]; 

	TG[0] = CPT::Instance()->GetT_Npion_Nomega();
	TG[1] = CPT::Instance()->GetT_Nomega_Npion();
	TG[2] = CPT::Instance()->GetT_Nomega_Nrho();
	TG[3] = CPT::Instance()->GetT_Nrho_Nomega();
	TG[4] = CPT::Instance()->GetT_Nomega_elast();
	TG[5] = CPT::Instance()->GetT_Nomega_N2pion();
	TG[6] = CPT::Instance()->GetT_Npion_Nomega1();
	TG[7] = CPT::Instance()->GetT_Npion_Nphi();
	TG[8] = CPT::Instance()->GetT_Npion_Nrho();
	TG[9] = CPT::Instance()->GetT_Nomega_Npion1();
	TG[10] = CPT::Instance()->GetT_Nphi_Npion();
	TG[11] = CPT::Instance()->GetT_Nrho_Npion();

	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	TG[0]->GetYaxis()->SetNdivisions(10);
	TG[0]->GetYaxis()->SetRangeUser(1e-2,500.);
	TG[0]->GetXaxis()->SetRangeUser(2,100.);
	TG[0]->GetYaxis()->SetTitle("Total Cros Section (mb)");
	TG[0]->GetXaxis()->SetTitle("s (GeV^{2})");
	TG[0]->SetLineWidth(1);
	TG[0]->Draw("AC");
	TG[1]->SetLineColor(2);
	TG[1]->SetLineWidth(2);
	TG[1]->Draw("Csame");
	TG[2]->SetLineStyle(2);
	TG[2]->SetLineWidth(1);
	TG[2]->Draw("Csame");
	TG[3]->SetLineStyle(2);
	TG[3]->SetLineWidth(2);
	TG[3]->SetLineColor(2);
	TG[3]->Draw("Csame");
	TG[4]->SetLineStyle(3);
	TG[4]->SetLineWidth(1);
	TG[4]->Draw("Csame");
	TG[5]->SetLineStyle(4);
	TG[5]->SetLineColor(3);
	TG[5]->SetLineWidth(1);
	TG[5]->Draw("Csame");
	gPad->SetLogy();
	gPad->SetLogx();

	c1->Update();
	TLatex n;
	Double_t c1ymax  = c1->GetUymax();
	Double_t c1ypass = c1ymax/15.;
	n.DrawLatex(2.5,100,"Meson Exchange model");
	n.DrawLatex(2.5,50,"(Eur. Phys. J. A 6, 71(1999) )");
	TLine m[6];
	m[1].SetLineColor(2); m[2].SetLineStyle(2); m[3].SetLineColor(2); m[3].SetLineStyle(2); m[4].SetLineStyle(3); m[4].SetLineStyle(3); m[5].SetLineColor(3); m[5].SetLineStyle(4);
	TString sch[12] =  {"N#pi #rightarrow N#omega", "N#omega #rightarrow N#pi", "N#omega #rightarrow N#rho", "N#rho #rightarrow N#omega", "N#omega elastic", "N#omega #rightarrow N 2#pi ","N#pi #rightarrow N#omega" ,  "N#omega #rightarrow N#pi" ,"N#pi #rightarrow N#rho" ,  "N#rho #rightarrow N#pi", "N#pi #rightarrow N#phi" ,  "N#phi #rightarrow N#pi" };
	for (int i = 0; i < 6; i++){
		double yy = TMath::Power(10,c1ymax - (i+1)*2*c1ypass);
		double xx = 20; double xx1 = 30; double xx2 = 40;
		m[i].SetX1(20. );
		m[i].SetX2(30. );
		m[i].SetY1(yy);
		m[i].SetY2(yy);
		m[i].SetLineWidth(3);
		n.DrawLatex(xx2,yy,sch[i].Data());
		m[i].Draw("same");
		}
	c1->Print("Amplitude/Results/TotalCS/All_TotalCS_Tree_medium.pdf");

	TCanvas *c2 = new TCanvas("c2","Graph Draw Options",200,10,600,400);
	gPad->SetLogy();
	gPad->SetLogx();
	TG[6]->GetYaxis()->SetNdivisions(10);
	TG[6]->GetYaxis()->SetRangeUser(1e-2,500.);
	TG[6]->GetXaxis()->SetRangeUser(2,100);
	TG[6]->GetYaxis()->SetTitle("Total Cros Section (mb)");
	TG[6]->GetXaxis()->SetTitle("s (GeV^{2})");
	TG[6]->SetLineWidth(2);
	TG[6]->Draw("AC");
	TG[9]->SetLineColor(2);
	TG[9]->SetLineWidth(2);
	TG[9]->Draw("Csame");
	TG[8]->SetLineStyle(2);
	TG[8]->SetLineWidth(2);
	TG[8]->Draw("Csame");
	TG[11]->SetLineStyle(2);
	TG[11]->SetLineWidth(2);
	TG[11]->SetLineColor(2);
	TG[11]->Draw("Csame");
	TG[10]->SetLineStyle(3);
	TG[10]->SetLineWidth(2);
	TG[10]->Draw("Csame");
	TG[7]->SetLineStyle(3);
	TG[7]->SetLineWidth(2);
	TG[7]->SetLineColor(2);
	TG[7]->Draw("Csame");


	c2->Update();
	Double_t c2ymax  = c2->GetUymax();
	Double_t c2ypass = c2ymax/15.;
	TLatex n1;
	n1.DrawLatex(2.5,100,"Ressonance model");
	n1.DrawLatex(2.5,50,"(Z. Phys. A 358, 357(1997) )");
	TLine m1[6];
	m1[1].SetLineColor(2); m1[2].SetLineStyle(2); m1[3].SetLineColor(2); m1[3].SetLineStyle(2); m1[4].SetLineStyle(3); m1[4].SetLineStyle(3); m1[5].SetLineColor(2); m1[5].SetLineStyle(3);
	for (int i = 0; i < 6; i++){
		double yy = TMath::Power(10,c2ymax - (i+1)*2*c2ypass);
		double xx = 20; double xx1 = 30; double xx2 = 40;
		m1[i].SetX1(20. );
		m1[i].SetX2(30. );
		m1[i].SetY1(yy);
		m1[i].SetY2(yy);
		m1[i].SetLineWidth(3);
		n1.DrawLatex(xx2,yy,sch[i+6].Data());
		m1[i].Draw("same");
		}
	c2->Print("Amplitude/Results/TotalCS/All_TotalCS_Tree_medium_1.pdf");

}
