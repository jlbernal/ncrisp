{
	
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	
	TGraph *gr =   CPT::Instance()->GetT_rho();
	TGraph *gr1 =  CPT::Instance()->GetT_omg();
	TGraph *gr2 =  CPT::Instance()->GetT_phi();
	TGraph *gr3 =  CPT::Instance()->GetT_J_Psi();

	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->GetYaxis()->SetNdivisions(5);
//	gr->GetYaxis()->SetRangeUser(1,13);
//	gr->GetXaxis()->SetRangeUser(0.1,4);
	gr->GetYaxis()->SetTitle("Photo Cros Section (m bar)");
	gr->GetXaxis()->SetTitle("W (GeV/c)");
	gr->Draw("AC");
	gr1->Draw("same");
	gr2->Draw("same");
	gr3->Draw("same");
	c1->Update();
	gPad->SetLogy();
	gPad->SetLogx();
}
