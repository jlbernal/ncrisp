
//double VM_Diff_CrossSection_NJPsi__HypDa(double s, double cos){
double VM_Diff_CrossSection_NJPsi__HypDa(double* x, double* par){
	double cos = x[0];
	double s = par[0];
	const Double_t gJDD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_p_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	double t = 0.5*(   cos* sqrt( Trg_Fun(s,mj*mj,mN*mN)*Trg_Fun(s,mD*mD,mlambda*mlambda) )/s - (mj*mj - mN*mN)*(mD*mD - mlambda*mlambda)/s + Mand_Sigma(mj,mN,mD,mlambda) - s );

	const Double_t Lambda_JDD = 3.1;
	const Double_t Lambda_DNL = 2;
	Double_t F_JDD = Lambda_JDD*Lambda_JDD/(t - Lambda_JDD*Lambda_JDD);
	Double_t F_DNL = Lambda_DNL*Lambda_DNL/(t - Lambda_DNL*Lambda_DNL);

	Double_t Ma_2_0 = 8*gJDD*gJDD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Ma_2_1 = 1./(t - mD*mD) + 1./(mD*mD + mj*mj - t); //intercambio
	Double_t Ma_2_2 = (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Ma_2_3 = TMath::Power( (mj*mj + mD*mD - t)/2.,2 )- mj*mj*mD*mD;
	Double_t Ma_2 = Ma_2_0*TMath::Power(Ma_2_1, 2)*Ma_2_2*Ma_2_3;
	Double_t fluxo_2 = ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s );
	if ( fluxo_2 > 0)
		Ma_2 = Ma_2 * TMath::Sqrt( fluxo_2 );
	else Ma_2 = 0;
//	cout << "Ma_2_0 = " << Ma_2_0 << ", Ma_2_1 = " << Ma_2_1 << ", Ma_2_2 = " << Ma_2_2 << ", Ma_2_3 = " << Ma_2_3 << ", fluxo " << TMath::Sqrt( fluxo_2) << endl;
	Double_t result =  2*Ma_2/64/TMath::Pi()/s*TMath::Power(F_JDD*F_DNL,2);
//	if (result!=result) result = 0;
	return result;
}
void JPsiN_TotalCSa(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Double_t s_max = 10000;
	const Double_t s_min = 17.28;
	const Int_t Ns = 1000;
	const Double_t s_delta = (s_max - s_min)/Ns;
	const Double_t cos_max = 1;
	const Double_t cos_min = -1;
	const Int_t Ncos = 1000;
	const Double_t cos_delta = (cos_max - cos_min)/Ncos;
	const double _GeV2_mbar = 0.3894;	// Collins pag 3

	double sPoint[Ns], csHypDa[Ns], csHypDb[Ns], csHypDast[Ns], csHypDDbar[Ns];
	for (int i = 0; i < Ns; i ++){
		double s = s_min + i*s_delta;
		double tcsHypDa = 0., tcsHypDb = 0., tcsHypDast = 0., tcsHypDDbar = 0.;
		TF1 *f1 = new TF1("f1", VM_Diff_CrossSection_NJPsi__HypDa,-1,1,1);
		f1->SetParameter(0,s);
/*		for (int j = 0; j < Ncos; j ++){
			Double_t cosTheta = cos_min + j*cos_delta;
			Double_t csTemp = VM_Diff_CrossSection_NJPsi__HypDa(s, cosTheta);
			tcsHypDa = tcsHypDa + csTemp*cos_delta;
		}
*/		sPoint[i] = TMath::Sqrt(s);
		csHypDa[i] = f1->Integral(-1,1)*_GeV2_mbar;
//		cout << "s = " << s << ", CS = " << f1->Integral(-1,1)*_GeV2_mbar << endl;
//		cout << "s = " << s << ", CS = " << tcsHypDa*_GeV2_mbar << endl;
	}
	TGraph *gHypDa = new TGraph(Ns,sPoint,csHypDa);

	TCanvas *c1 = new TCanvas("c1","HypDa",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gHypDa->Draw("ALP");
}
