//Double_t JPsiN_AngDist(Double_t *x, Double_t *par){
Double_t JPsiN_AngDist(Double_t cos, Double_t s){
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
//	Double_t cos = x[0];
//	Double_t s = par[0];
	double MVO = CPT::J_Psi_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MN_2 = TMath::Power(MN,2);

	double t = 0.5*( cos*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s - (MN_2 - MVO_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVO,MN,MVO) - s ); //Eq (13)
	double z = VM_Mand_z(s,0,t,MN,MVO);
	Double_t f = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtJ_Psi(z,t,0)*sqrt( Trg_Fun(s,MVO_2,MN_2)*Trg_Fun(s,MVO_2,MN_2) )/s/2;
	return f* 1000;
}
void testJPsi(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	gROOT->ProcessLine(".include base");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc+");
	gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();

	const Double_t smin = 7.885;
	const Double_t smax = 17.885;
	const Int_t N = 100;
	const Double_t ds = (smax - smin)/N;
//	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			double s = smin + j*ds;
			double temp = 	JPsiN_AngDist( 1,  s);
			cout << "s = " << s << ", diffCS = " << temp << endl;
		}
//	}
/*	const Double_t s = 16.7885;
	TF1 *f4 = new TF1("JPsiN_AngDist",JPsiN_AngDist,0.999999,1,1);
	f4->SetParameter(0,s/1000/1000); //MeV^2 -> GeV^2
	f4->SetNpx(1000);
	Double_t cosTheta = f4->GetRandom();
	cout << "s = " << s << ", cos Theta = " << cosTheta << endl;
	f4->Draw();
*/}
