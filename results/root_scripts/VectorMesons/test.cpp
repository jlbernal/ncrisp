void test() {
	TFile f("AFile.root","RECREATE");
	const Int_t n = 20;
	Double_t x[n], y[n];
	for (Int_t i=0; i<n; i++) {
		x[i] = i*0.1;
		y[i] = 10*sin(x[i]+0.2);
	}
	TGraph *gr1 = new TGraph (n, x, y);
	gr1->Write();
	f.Write();
}

