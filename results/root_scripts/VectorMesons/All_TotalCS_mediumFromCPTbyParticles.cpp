Double_t rho_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N#rightarrowVM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if ( W > 1.745 ){
		if (W < 100 ){
			Double_t CS = CPT::Instance()->GetT_rho()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t omega_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N#rightarrowVM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3./sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if  (W > 1.745){
		if (W < 100 ){
			Double_t CS = CPT::Instance()->GetT_omg()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t phi_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N#rightarrowVM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3.; 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if (W > 2.0132){
		if (W < 100 ){
			Double_t CS = CPT::Instance()->GetT_phi()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t JPsi_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N#rightarrowVM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 2./3.; 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if (W > 4.0396){
		if (W < 100 ){
			Double_t CS = CPT::Instance()->GetT_J_Psi()->Eval(W);
			return CS/C*100; //milibar
		} else {
			Double_t CS = 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
			return CS/C*100; //milibar
		}
	} else return 0;
}
Double_t PionN_OmegaN_cross_section(Double_t s){
	if (s > 2.97345){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_PionN_cross_section(Double_t s){
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_RhoN_cross_section(Double_t s){
	if (s > 2.96902){
		Double_t CS = CPT::Instance()->GetT_Nomega_Nrho()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t RhoN_OmegaN_cross_section(Double_t s){
	if (s > 2.96843){
		Double_t CS = CPT::Instance()->GetT_Nrho_Nomega()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t OmegaN_elastic_cross_section(Double_t s){
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Nomega_elast()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_2PionN_cross_section(Double_t s){
	if (s > 2.96615){
		Double_t CS = CPT::Instance()->GetT_Nomega_N2pion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_OmegaN1_cross_section(Double_t s){ 
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega1()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_PhiN_cross_section(Double_t s){
	if (s > 3.8434){
		Double_t CS = CPT::Instance()->GetT_Npion_Nphi()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_RhoN_cross_section(Double_t s){
	if (s > 2.94142){
		Double_t CS = CPT::Instance()->GetT_Npion_Nrho()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_PionN1_cross_section(Double_t s){
	if (s > 2.96624){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion1()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PhiN_PionN_cross_section(Double_t s){
	if (s > 3.8372){
		Double_t CS = CPT::Instance()->GetT_Nphi_Npion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t RhoN_PionN_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_Nrho_Npion()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_ND_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_ND()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_NDast_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDast()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}
Double_t NJPsi_NDDbar_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDDbar()->Eval(s);
		return CS > 0 ? CS : 0; //milibar
	} else return 0;
}

Double_t RhoN_Total_CS_HighEn(Double_t s){ //using quark model

	double W = sqrt(s); //GeV
	if(W >= 2.){
		double CS_pipP = 42.6816*pow(W,-1.29223) + 14.0899*pow(W,0.166105); //milibarn 
		double CS_pimP = 34.5152*pow(W,-0.540496) + 7.02817*pow(W,0.306528); //milibarn
		return (1./2.)*(CS_pipP+CS_pimP);
	}
	else return 0.;
}

Double_t RhoN_mPion_CS_HighEn(Double_t s){
	
	double W = sqrt(s); //GeV
	if(W >= 2.){
		double RhoN_PionN_cs = RhoN_PionN_cross_section(s); 
		double RhoN_OmegaN_cs = RhoN_OmegaN_cross_section(s);
		double rho_elastic_cs = rho_elastic_cross_section(s);
		double CS = RhoN_Total_CS_HighEn_ReggeFit(s) - RhoN_PionN_cs - RhoN_OmegaN_cs - rho_elastic_cs;	
		return CS;
	}
	else return 0.;
}

Double_t RhoN_Total_CS_HighEn_ReggeFit(Double_t s){ //Regge Fit to VDM data

	double W = sqrt(s); //GeV
	if(W >= 2.){
		double CS = 80.5807*pow(W,-0.597137) + 9.49625*pow(W,0.326840); //milibarn
		return CS;
	}
	else return 0.;
}

Double_t rho_0_PhotonCrossSectionP(Double_t s){
	
	double W = sqrt(s); //GeV
	Double_t CS;
	if (W < 100 ){
		CS = CPT::Instance()->GetT_rho()->Eval(W);
		return CS;
	} else { 
		CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) );
		return CS; //milibarn
	}
	return 0.;
}

Double_t rho0_PhotoCS_Forward(Double_t s){ //Klein1999
	
	double W = sqrt(s); //GeV
	Double_t CS;
	CS = 11. * ( 5.*pow(W,0.22) + 26.*pow(W,-1.23) );
	return CS; //milibarn
}

Double_t RhoN_Total_CS_HighEn_VDM(Double_t s){ //using theoretical rho photoproduction
	
	double couplingConst = 0.61; //gamma_rho^2/4pi
	double couplingConst2 = 2.02; //gamma_rho^2/4pi - Klein1999
	double alpha = 1./137.; //fine structure constant
	
	double gN_rhoN_Forward = 10.9 * rho_0_PhotonCrossSectionP(s); // slope parameter for rho elastic b = 10.9  GeV^-2 H1Collaboration1996
	double CS = sqrt( couplingConst * (64.*TMath::Pi()/alpha) * gN_rhoN_Forward );
// 	double CS = sqrt( couplingConst2 * (16.*TMath::Pi()/alpha) * rho0_PhotoCS_Forward(s) ); //Klein1999
	return CS;
}

void All_TotalCS_mediumFromCPTbyParticles(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	const Int_t nGraph = 22;
	TGraph *T[nGraph];
	Double_t Emax = 50000; Int_t N = 1000000;
	Double_t pass = (Emax - 1)/N;
	Int_t lineStyleIndex[6] = {1, 8, 7, 5, 3, 10};
	for(int i = 0; i < nGraph; i++)
		T[i] = new TGraph();
	for(int i = 0; i <= N; i++){
		Double_t s = 1 + i*pass;
		T[0]->SetPoint(i, s,  PionN_OmegaN1_cross_section(s));
		T[1]->SetPoint(i, s,  PionN_OmegaN_cross_section(s)); 
		T[2]->SetPoint(i, s,  PionN_RhoN_cross_section(s));
		T[3]->SetPoint(i, s,  PionN_PhiN_cross_section(s));

		T[4]->SetPoint(i, s,  rho_elastic_cross_section(s));
		T[5]->SetPoint(i, s,  RhoN_OmegaN_cross_section(s));
		T[6]->SetPoint(i, s,  RhoN_PionN_cross_section(s));

		T[7]->SetPoint(i, s,  OmegaN_elastic_cross_section(s));
		T[8]->SetPoint(i, s,  omega_elastic_cross_section(s));
		T[9]->SetPoint(i, s,  OmegaN_PionN_cross_section(s));
		T[10]->SetPoint(i, s,  OmegaN_PionN1_cross_section(s));
		T[11]->SetPoint(i, s,  OmegaN_RhoN_cross_section(s));
		Double_t temp = OmegaN_2PionN_cross_section(s);
		T[12]->SetPoint(i, s,  temp*3);
//		T[12]->SetPoint(i, s,  OmegaN_2PionN_cross_section(s));
//		cout << "i = " << i << ", s = " << s << ", OmegaN_2PionN_cross_section(s) = " << temp*3 << endl; 
		T[13]->SetPoint(i, s,  phi_elastic_cross_section(s));
		T[14]->SetPoint(i, s,  PhiN_PionN_cross_section(s));

		T[15]->SetPoint(i, s,  JPsi_elastic_cross_section(s));
		Double_t NJPsi_ND, NJPsi_NDast, NJPsi_NDDbar;
		NJPsi_ND 	= NJPsi_ND_cross_section(s);
		NJPsi_NDast 	= NJPsi_NDast_cross_section(s);
		NJPsi_NDDbar 	= NJPsi_NDDbar_cross_section(s);
		T[16]->SetPoint(i, s,  NJPsi_ND);
		T[17]->SetPoint(i, s,  NJPsi_NDast);
		T[18]->SetPoint(i, s,  NJPsi_NDDbar);
//		if (s > 500)
			T[19]->SetPoint(i, s,  NJPsi_ND + NJPsi_NDast + NJPsi_NDDbar);
//		else 
//			T[19]->SetPoint(i, s,  NJPsi_NDDbar);
		if( s >= 4 ) T[20]->SetPoint(i-((4-1)/pass), s,  RhoN_Total_CS_HighEn_ReggeFit(s));
		
		T[21]->SetPoint(i, s,  RhoN_mPion_CS_HighEn(s));
	}
	
	///RhoN Total CS HighEn VDM using theory
	N = 100;
	double Emin = 4.;// GeV^2
	Emax = 50000.; //GeV^2
	pass = (log(Emax)-log(Emin))/N;
	TGraph *RhoNTotalVDM = new TGraph();
	for(int i=0; i<=N; i++){
		
		s = exp(log(Emin) + i*pass);
		RhoNTotalVDM->SetPoint(i, s, RhoN_Total_CS_HighEn_VDM(s));
	}
	RhoNTotalVDM->SetMarkerSize(0.8);
	RhoNTotalVDM->SetMarkerStyle(20);

	///RhoN Total CS HighEn VDM with Experimental ds/dt|t=0
	double couplingConst = 0.61; //gamma_rho^2/4pi
	double alpha = 1./137.; //fine structure constant
	double ProtonMass = CPT::p_mass/1000.;
	//Cross sections and photon energies from Ballam1973 and Anderson1971
	double x[6] = {2.*ProtonMass*(2.8) + ProtonMass*ProtonMass, 2.*ProtonMass*(4.7) + ProtonMass*ProtonMass, 2.*ProtonMass*(9.3) + ProtonMass*ProtonMass, 2.*ProtonMass*(6) + ProtonMass*ProtonMass, 2.*ProtonMass*(12) + ProtonMass*ProtonMass, 2.*ProtonMass*(18) + ProtonMass*ProtonMass}; //s GeV^2
	double y[3] = {0.104, 0.094, 0.086}; //Soding model milibarn
	//double y[3] = {0.148, 0.109, 0.088}; //Phenomenological Soding model milibarn
	//double y[3] = {0.138, 0.114, 0.095}; //Parametrization milibarn
	//double y[3] = {0.144, 0.109, 0.084}; //s-channel helicity conserving dipion milibarn
	double ExpCS[6] = {sqrt(couplingConst * (64.*TMath::Pi()/alpha) * y[0]), sqrt(couplingConst * (64.*TMath::Pi()/alpha) * y[1]), sqrt(couplingConst * (64.*TMath::Pi()/alpha) * y[2]), 28.6, 28.5, 27.6};
	
	TGraph *RhoNTotalVDM_Exp = new TGraph(6,x,ExpCS);
	RhoNTotalVDM_Exp->SetMarkerSize(0.8);
	RhoNTotalVDM_Exp->SetMarkerStyle(21);
	
	///RhoN Total CS HighEn VDM with Experimental ds/dt
	TFile F("data/PhotoProduction_VectorMeson_Exp.root");
	TGraphErrors *RhoPhotoCS = (TGraphErrors*)F.Get("TGE_Rho");
	
	TGraphErrors *RhoNTotalVDM_Exp2 = new TGraphErrors();
	RhoNTotalVDM_Exp2->SetMarkerSize(0.8);
	RhoNTotalVDM_Exp2->SetMarkerStyle(20);
		
	double couplingConst = 0.61; //gamma_rho^2/4pi
	double alpha = 1./137.; //fine structure constant
	int index = 0.;
	for(int i=0; i<RhoPhotoCS->GetN(); i++){
		
		if(RhoPhotoCS->GetX()[i] >= 2.){
			
			double gN_rhoN_Forward = 10.9 * RhoPhotoCS->GetY()[i]; // slope parameter for rho elastic b = 10.9  GeV^-2 H1Collaboration1996
			double CS = sqrt( couplingConst * (64.*TMath::Pi()/alpha) * gN_rhoN_Forward );
					
			double ErrX = RhoPhotoCS->GetEX()[i];
			double ErrY = (1./2.)*sqrt(couplingConst*(64.*TMath::Pi()/alpha)*10.9)
						*pow(RhoPhotoCS->GetY()[i],-1./2.)*RhoPhotoCS->GetEY()[i];
				
			RhoNTotalVDM_Exp2->SetPoint(index,pow(RhoPhotoCS->GetX()[i],2.),CS);
			RhoNTotalVDM_Exp2->SetPointError(index,ErrX,ErrY);
			index++;
		}
	}
		
	//ELASTIC
	TCanvas *c = new TCanvas("c","Pion meson Cross Section",1000,1000);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TString name[4] = {"#pi N #rightarrow #omega N (RM)", "#pi N #rightarrow #omega N (EM)", "#pi N #rightarrow #rho N (RM)", "#pi N #rightarrow #phi N (RM)"};
	TMultiGraph *mg = new TMultiGraph();
	TLegend* leg = new TLegend(.55,.65,.9,.9);
	leg->SetFillColor(10);
//	leg->SetHeader("Legend");
	for(int i = 0; i < 4; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i]);
		T[i]->SetLineColor(1 + i);
		mg->Add(T[i],"L");
		leg->AddEntry(T[i], name[i].Data(),"L");
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg->GetYaxis()->SetTitle("#sigma [mb]");				
	mg->GetYaxis()->SetTitleOffset(1.2);				
	mg->GetYaxis()->SetRangeUser(1e-3,10);
	mg->GetXaxis()->SetRangeUser(2.5,200);
	leg->Draw("same");
	c->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_pion.pdf");
	c->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_pion.eps");
	//ONE MESON EXCHANGE MODEL RHO, OMEGA, PHI
	TCanvas *c1 = new TCanvas("c1","#rho meson Cross Section",1000,1000);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();

	TString name1[3] = {"#rho N elast. (VDM)", "#rho N #rightarrow #omega N (EM)","#rho N #rightarrow #pi N (RM)"};
	TMultiGraph *mg1 = new TMultiGraph();
	mg1->SetTitle("");
	TLegend* leg1 = new TLegend(.55,.65,.9,.9);
	leg1->SetFillColor(10);
//	leg1->SetNColumns(2);
//	leg1->SetHeader("Legend");
	for(int i = 4; i < 7; i++){
		T[i]->SetMarkerStyle(21 + i-4);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i - 4]);
		T[i]->SetLineColor(1 + i - 4);
		mg1->Add(T[i],"L");
		leg1->AddEntry(T[i], name1[i - 4].Data(),"L");
	}
	mg1->Draw("AP");
	mg1->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg1->GetYaxis()->SetTitle("#sigma [mb]");				
	mg1->GetYaxis()->SetTitleOffset(1.2);				
	mg1->GetYaxis()->SetRangeUser(1e-4,20);
	mg1->GetXaxis()->SetRangeUser(2.5,200);
	leg1->Draw("same");
	c1->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_rho.pdf");
	c1->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_rho.eps");
	//RESSONANCE MODEL RHO, OMEGA, PHI
	TCanvas *c2 = new TCanvas("c2","#omega meson Cross Section",1000,1000);
	c2->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c2->SetFrameFillColor(10);
	c2->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg2 = new TMultiGraph();
	mg2->SetTitle("");
	TLegend* leg2 = new TLegend(.35,.65,.9,.9);
	leg2->SetFillColor(10);
//	leg2->SetHeader("Legend");
	leg2	->SetNColumns(2);
	TString name2[6] = {"#omega N elast. (EM)", "#omega N elast. (VDM)","#omega N #rightarrow #pi N (EM)", "#omega N #rightarrow #pi N (RM)","#omega N #rightarrow #rho N (EM)" ,"#omega N #rightarrow #pi #pi N (EM)"};
	for(int i = 7; i < 13; i++){
		T[i]->SetMarkerStyle(21 + i-7);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i - 7]);
		if (1 + i - 7 < 5) T[i]->SetLineColor(1 + i - 7);
		else T[i]->SetLineColor(1 + i - 6);
		mg2->Add(T[i],"L");
		leg2->AddEntry(T[i], name2[i - 7].Data(),"L");
	}
	mg2->Draw("AP");
	mg2->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg2->GetYaxis()->SetTitle("#sigma [mb]");				
	mg2->GetYaxis()->SetTitleOffset(1.2);				
	mg2->GetYaxis()->SetRangeUser(1e-3,100);
	mg2->GetXaxis()->SetRangeUser(2.5,200);
	leg2->Draw("same");
	c2->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_omega.pdf");
	c2->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_omega.eps");
	//ONE MESON EXCHANGE MODEL J/Psi
	TCanvas *c3 = new TCanvas("c3","#phi Cross Section",1000,1000);
	c3->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c3->SetFrameFillColor(10);
	c3->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg3 = new TMultiGraph();
	mg3->SetTitle("");
	TLegend* leg3 = new TLegend(.55,.65,.9,.9);
	leg3->SetFillColor(10);
//	leg3->SetHeader("Legend");
	TString name3[2] = {"#phi N elast. (VDM)", "#phi N #rightarrow #pi N (RM)"};
	for(int i = 13; i < 15; i++){
		T[i]->SetMarkerStyle(21 + i-13);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i - 13]);
		T[i]->SetLineColor(1 + i - 13);
		mg3->Add(T[i],"L");
		leg3->AddEntry(T[i], name3[i - 13].Data(),"L");
	}
	mg3->Draw("AP");
	mg3->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg3->GetYaxis()->SetTitle("#sigma [mb]");				
	mg3->GetYaxis()->SetTitleOffset(1.2);				
	mg3->GetYaxis()->SetRangeUser(1e-6,1);
	mg3->GetXaxis()->SetRangeUser(1,7000);
	leg3->Draw("same");
	c3->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_phi.pdf");
	c3->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_phi.eps");


	TCanvas *c4 = new TCanvas("c4","J/Psi meson Cross Section",1000,1000);
	c4->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c4->SetFrameFillColor(10);
	c4->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg4 = new TMultiGraph();
	mg4->SetTitle("");
	TLegend* leg4 = new TLegend(.55,.55,.9,.73);
	leg4->SetFillColor(10);
//	leg4->SetHeader("Legend");
	TString name4[5] = {"J/#Psi N elast. (VDM)", "NJ/#Psi #rightarrow #Lambda_{c} D (EM)" ,  "NJ/#Psi #rightarrow #Lambda_{c} D^{*} (EM)" ,  "NJ/#Psi #rightarrow #Lambda_{c} D D (EM)", "Total NJ/#Psi (EM)"};
	for(int i = 15; i <= 19; i++){
		T[i]->SetMarkerStyle(21 + i-15);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i - 15]);
		T[i]->SetLineColor(1 + i - 15);
		mg4->Add(T[i],"L");
		leg4->AddEntry(T[i], name4[i - 15].Data(),"L");
	}
	mg4->Draw("AP");
	mg4->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg4->GetYaxis()->SetTitle("#sigma [mb]");				
	mg4->GetYaxis()->SetTitleOffset(1.2);				
	mg4->GetYaxis()->SetRangeUser(1e-4,1e6);
	mg4->GetXaxis()->SetRangeUser(10,7000);
	leg4->Draw("same");
	c4->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_JPsi.pdf");
	c4->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_JPsi.eps");
	
	TCanvas *c5 = new TCanvas("c5","#rho meson Cross Section",1000,1000);
	c5->SetFillColor(42);
	c5->SetFrameFillColor(10);
	c5->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	//c5->SetGridx();
	//c5->SetGridy();

	TString name5[5] = {"#rhoN elast. (VDM)", "#rhoN #rightarrow #omegaN (EM)","#rhoN #rightarrow #piN (RM)","#rhoN total (Regge Fit)","#rhoN #rightarrow m#piN (Regge Fit)"};
	TMultiGraph *mg5 = new TMultiGraph();
	mg5->SetTitle("");
	TLegend* leg5 = new TLegend(.55,.7,.9,.9);
	leg5->SetFillColor(10);
//	leg1->SetNColumns(2);
//	leg1->SetHeader("Legend");
	for(int i = 4; i < 7; i++){
		T[i]->SetMarkerStyle(21 + i-4);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(lineStyleIndex[i - 4]);
		T[i]->SetLineColor(1 + i - 4);
		mg5->Add(T[i],"L");
		leg5->AddEntry(T[i], name5[i - 4].Data(),"L");
	}
	T[20]->SetLineWidth(2);
	T[20]->SetLineColor(4);
	T[20]->SetLineStyle(4);
	mg5->Add(T[20],"L");
	leg5->AddEntry(T[20], name5[3].Data(),"L");
	T[21]->SetLineWidth(2);
	T[21]->SetLineColor(6);
	T[21]->SetLineStyle(6);
	mg5->Add(T[21],"L");
	leg5->AddEntry(T[21], name5[4].Data(),"L");
	
	mg5->Add(RhoNTotalVDM_Exp2,"P");
	leg5->AddEntry(RhoNTotalVDM_Exp2,"#rhoN total (VDM)","P");
	
	mg5->Draw("AP");
	mg5->GetXaxis()->SetTitle("s[GeV^{2}]");				
	mg5->GetYaxis()->SetTitle("#sigma [mb]");				
	mg5->GetYaxis()->SetTitleOffset(1.2);				
	mg5->GetYaxis()->SetRangeUser(1e-3,4000); //1e-4,4000
	mg5->GetXaxis()->SetRangeUser(2.5,5e4);
	leg5->Draw("same");
	///zooming the graph
	TPad *inset_pad = new TPad("inset","zooming", 0.36, 0.2, 0.95, 0.65);
	inset_pad->Draw();
	inset_pad->cd();
	gPad->SetLogy();
	gPad->SetLogx();
	gPad->SetFillStyle(4000);
	TMultiGraph *mg6 = new TMultiGraph();
	mg6->Add(T[20],"L");
	mg6->Add(T[21],"L");
	mg6->Add(RhoNTotalVDM_Exp2,"P");
	mg6->Draw("AP");
	mg6->GetYaxis()->SetRangeUser(30,100);
	mg6->GetXaxis()->SetRangeUser(2.5,30);
	
	c5->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_rho_2.pdf");
	c5->SaveAs("results/Figures/TotalHighEnergyMediumFromCPT_rho_2.eps");
	
}