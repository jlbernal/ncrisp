/************************************
OBSERVACIONES
1. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
	double MVP = CPT::pion_p_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MVO = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	double MP = CPT::p_mass/1000.;
	double MN = CPT::n_mass/1000.;
	double MVO_2 = TMath::Power(MVO,2);
	double MVP_2 = TMath::Power(MVP,2);
	double MP_2 = TMath::Power(MP,2);
	double MN_2 = TMath::Power(MN,2);
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int N = 10000;
	const int Ns = 300;
	double Ppion[Ns], Pomega[Ns], TCS[Ns];
	double Pomega_0 = 0; 
	double Pomega_1 = 4;
	double Pomega_pass = (Pomega_1 - Pomega_0)/Ns; 
	for (int i = 0; i < Ns; i++){
		Pomega[i] = Pomega_0 + i* Pomega_pass;
		double s = TMath::Power(MN + sqrt(Pomega[i]*Pomega[i] + MVO_2) ,2) - Pomega[i]*Pomega[i];
//		cout << "Ppion[i] = " << Ppion[i] << ", s = " << s << endl;
		double DiffCS, t;
		double t_lim_p = 0.5*(    sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MP_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MP,MVO) - s ); //Eq (13)
		double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(s,MVP_2,MN_2)*Trg_Fun(s,MVO_2,MP_2) )/s - (MN_2 - MVP_2)*(MN_2 - MVO_2)/s + Mand_Sigma(MN,MVP,MP,MVO) - s ); //Eq (13)
//		t_lim_m = -0.5;
		double t_pass = (t_lim_p - t_lim_m)/N;
		TCS[i] = 0;
		for (int j = 0; j < N; j++){
			t = t_lim_m + j* t_pass;
			double z = Mand_cos_theta_s(s,t,MN,MVP,MP,MVO);
//			cout <<  "t = " << t[j] << ", cos theta = " << z << ", t_lim_p = " << t_lim_p << ", t_lim_m = " << t_lim_m << endl;
			DiffCS = _GeV2_mbar*VM_Diff_CrossSection_ProtOmega__PionNeut(s,t,z);
			TCS[i] =  TCS[i] + DiffCS * t_pass;
		}
		cout << "s = " << s <<  ", Pomega = " << Pomega[i] << ", TCS = " << TCS[i] <<  endl;
	}
	// create graph
	TGraph *gr = new TGraph(Ns,Pomega,TCS);
	gr->GetYaxis()->SetNdivisions(5);
	gr->GetYaxis()->SetRangeUser(1e-1,1e6);
	gr->GetXaxis()->SetRangeUser(1e-2,5);
	gr->GetYaxis()->SetTitle("Total Cros Section (#mu bar)");
	gr->GetXaxis()->SetTitle("P_{#pi} (GeV/c)");
	TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);
	gr->Draw("AC");
	gPad->SetLogy();
	gPad->SetLogx();
	c1->Print("Amplitude/Results/DiffCS/Total_OmegaProt_PionNeut.pdf");
}

