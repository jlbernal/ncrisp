
double VM_Diff_CrossSection_NJPsi__HypDa(double s, double t){
	const Double_t gJDD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	
	Double_t Ma_2_0 = 8*gJDD*gJDD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Ma_2_1 = 1./(t - mD*mD) + 1./(mD*mD + mj*mj - t); //intercambio
	Double_t Ma_2_2 = (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Ma_2_3 = TMath::Power( (mj*mj + mD*mD - t)/2.,2 )- mj*mj*mD*mD;

	Double_t Ma_2 = Ma_2_0*TMath::Power(Ma_2_1, 2)*Ma_2_2*Ma_2_3;

	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Ma_2 = Ma_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Ma_2 = 0;
	return Ma_2/64/TMath::Pi()/TMath::Pi()/s;
}
double VM_Diff_CrossSection_NJPsi__HypDb(double s, double t){
	const Double_t gJDastD = 7.64;
	const Double_t gDastNLam =-19.;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	Double_t pj_p = (2*s + t - mj*mj - 2*mN*mN - mD*mD)/2.;
	Double_t pj_q = (mj*mj - mD*mD + t)/2.;
	Double_t p_2 = 2*(mN*mN + mlambda*mlambda )- t;

	Double_t Mb_2_0 = gJDastD*gJDastD*gDastNLam*gDastNLam/(3*mj*mj); 
	Double_t Mb_2_1 = 1./(t - mDast*mDast)/(t - mDast*mDast); //intercambio
	Double_t Mb_2_2 = mj*mj*(p_2*t - TMath::Power( mlambda*mlambda - mN*mN,2) );
	Double_t Mb_2_3 = 2*pj_p*pj_q*( mlambda*mlambda - mN*mN);
	Double_t Mb_2_4 = p_2*pj_q*pj_q + t*pj_p*pj_p;
	Double_t Mb_2_5 = 4*( (mN*mN + mlambda*mlambda - t)/2. - mN*mlambda)* ( mj*mj*t - pj_q*pj_q) ;
	Double_t Mb_2 = Mb_2_0*Mb_2_1 * (Mb_2_2 + Mb_2_3 - Mb_2_4 - Mb_2_5 );
	if ( (( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s )) > 0)
		Mb_2 = Mb_2 * TMath::Sqrt( ( TMath::Power(mlambda + mD ,2) - s )*( TMath::Power(mlambda - mD ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mb_2 = 0;
	return Mb_2/64/TMath::Pi()/TMath::Pi()/s;
}
double VM_Diff_CrossSection_NJPsi__HypDast(double s, double t){
	const Double_t gJDastD = 7.64;
	const Double_t gDNLam = 14.8;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mDast = CPT::Dast_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;

	Double_t Mc_2_0 = 4*gJDastD*gJDastD*gDNLam*gDNLam/(3*mj*mj); 
	Double_t Mc_2_1 = 1./(t - mD*mD)/(t - mD*mD); //intercambio
	Double_t Mc_2_2 =(mN*mN + mlambda*mlambda - t)/2. - mN*mlambda;
	Double_t Mc_2_3 = TMath::Power( (mj*mj + mDast*mDast - t)/2. ,2) - mj*mj*mDast*mDast;
	Double_t Mc_2 = Mc_2_0 * Mc_2_1 * Mc_2_2 * Mc_2_3;
	if ((( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ))  >0 )
		Mc_2 = Mc_2 * TMath::Sqrt( ( TMath::Power(mlambda + mDast ,2) - s )*( TMath::Power(mlambda - mDast ,2) - s )/( TMath::Power(mN + mj ,2) - s )/( TMath::Power(mN - mj ,2) - s ) );
	else Mc_2 = 0;
	return Mc_2/64/TMath::Pi()/TMath::Pi()/s;
}


Double_t VM_Diff_CrossSection_NJPsi__HypDast_s1(Double_t *x, Double_t *par){
	const Double_t gJDD = 7.64;
	const Double_t mj = CPT::J_Psi_mass*1e-3; // all mass in GeV
	const Double_t mN = CPT::n_mass*1e-3;	
	const Double_t mD = CPT::D_0_mass*1e-3;
	const Double_t mlambda = CPT::lambdac_p_mass*1e-3;
	const Double_t mPion = CPT::pion_0_mass*1e-3;
	const Double_t Lambda = 3.1;
	Float_t s1 = x[0];
	Float_t s = par[0];
	Float_t t = par[1];

	Double_t qD_2 = ( TMath::Power( mN + mD,2) - s1 ) * ( TMath::Power( mN - mD,2) - s1 )/4/s1;
	Double_t qj_2 = t; // ver esto... en el articulo no encontré que es qj_2 en la ecuacion 16
//	Double_t qj_2 = ( TMath::Power( mN + mj,2) - s1 ) * ( TMath::Power( mN - mj,2) - s1 )/4/s1;
	Double_t Md_2_0 = gJDD*gJDD/96/TMath::Pi()/TMath::Pi()/s/qj_2 * TMath::Sqrt(qD_2 * s1);
//	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t) ,2)/TMath::Power( t - mD ,2);
	Double_t Md_2_1 = TMath::Power( Lambda*Lambda/(Lambda*Lambda - t) ,2)/TMath::Power( t - mD*mD ,2);
	Double_t Md_2_2 = ( TMath::Power(mj + mD ,2) - t )*( TMath::Power(mj - mD ,2) - t )/mj/mj;
	Double_t SigmaDN;
	if ( (( TMath::Power(mlambda + mPion ,2) - s1)*( TMath::Power(mlambda - mPion ,2) - s1)/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1)) >0 )
		SigmaDN = TMath::Sqrt( ( TMath::Power(mlambda + mPion ,2) - s1 )*( TMath::Power(mlambda - mPion ,2) - s1 )/( TMath::Power(mD + mN ,2) - s1 )/( TMath::Power(mD - mN ,2) - s1 ) )*27/s1 + 20.;
	else SigmaDN = 0;
	return Md_2_2*SigmaDN;
}

Double_t DiffCS_t_Graph_JPsi_F1(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	double s = 30; double t = 0.01;
	const Double_t s1_max = 10;
	const Double_t s1_min = 0;
	TF1 *fCs1 = new TF1("fCs1", VM_Diff_CrossSection_NJPsi__HypDast_s1, s1_min, s1_max, 2);
	fCs1->SetParameters(s,t);
//	TF1 *fCs1 = new TF1("myfunc",myfunction,0,10,2);
//	fCs1->SetParameters(2,1);
	fCs1->Print();
	fCs1->Draw();
	return fCs1->Integral(s1_min,s1_max);
}

