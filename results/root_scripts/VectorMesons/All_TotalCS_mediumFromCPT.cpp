Double_t rho_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if ( W > 1.745 ){
		if (W < 40 ){
			Double_t CS = CPT::Instance()->GetT_rho()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.41928 * ( 1.36667*pow(W,0.284959 ) + 71.6749*pow(W,-1*1.6616 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t omega_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3./sqrt(2); 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if  (W > 1.745){
		if (W < 40 ){
			Double_t CS = CPT::Instance()->GetT_omg()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 3.38212 * ( 0.199673*pow(W,0.15405 ) + -1744.95*pow(W,-1*2.70346 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t phi_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 1./3.; 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if (W > 2.0132){
		if (W < 40 ){
			Double_t CS = CPT::Instance()->GetT_phi()->Eval(W);
			return CS/C; //milibar
		} else {
			Double_t CS = 2.36765 * ( 0.113964*pow(W,0.328556 ) )/1000;
			return CS/C; //milibar
		}
	} else return 0;
}
Double_t JPsi_elastic_cross_section(Double_t s){
	//ussing relation Amp_{ganma N-> VM N} = NC NV Amp_{VM N elast}
	const double NC = 3; 
	const double NV = 2./3.; 
	double C = NC*NV*NC*NV;
	Double_t W = sqrt(s);
	if (W > 4.0396){
		if (W < 40 ){
			Double_t CS = CPT::Instance()->GetT_J_Psi()->Eval(W);
			return CS/C*100; //milibar
		} else {
			Double_t CS = 2.55277 * ( 0.000957289*pow(W,0.753558 ) )/1000;
			return CS/C*100; //milibar
		}
	} else return 0;
}
Double_t PionN_OmegaN_cross_section(Double_t s){
	if (s > 2.97345){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_PionN_cross_section(Double_t s){
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_RhoN_cross_section(Double_t s){
	if (s > 2.96902){
		Double_t CS = CPT::Instance()->GetT_Nomega_Nrho()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t RhoN_OmegaN_cross_section(Double_t s){
	if (s > 2.96843){
		Double_t CS = CPT::Instance()->GetT_Nrho_Nomega()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_elastic_cross_section(Double_t s){
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Nomega_elast()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_2PionN_cross_section(Double_t s){
	if (s > 2.96615){
		Double_t CS = CPT::Instance()->GetT_Nomega_N2pion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_OmegaN1_cross_section(Double_t s){ 
	if (s > 2.96603){
		Double_t CS = CPT::Instance()->GetT_Npion_Nomega1()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_PhiN_cross_section(Double_t s){
	if (s > 3.8434){
		Double_t CS = CPT::Instance()->GetT_Npion_Nphi()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PionN_RhoN_cross_section(Double_t s){
	if (s > 2.94142){
		Double_t CS = CPT::Instance()->GetT_Npion_Nrho()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t OmegaN_PionN1_cross_section(Double_t s){
	if (s > 2.96624){
		Double_t CS = CPT::Instance()->GetT_Nomega_Npion1()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t PhiN_PionN_cross_section(Double_t s){
	if (s > 3.8372){
		Double_t CS = CPT::Instance()->GetT_Nphi_Npion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t RhoN_PionN_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_Nrho_Npion()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t NJPsi_ND_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_ND()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t NJPsi_NDast_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDast()->Eval(s);
		return CS; //milibar
	} else return 0;
}
Double_t NJPsi_NDDbar_cross_section(Double_t s){
	if (s > 2.94163){
		Double_t CS = CPT::Instance()->GetT_NJPsi_NDDbar()->Eval(s);
		return CS; //milibar
	} else return 0;
}
void All_TotalCS_mediumFromCPT(){
	gROOT->ProcessLine(".L base/CrispParticleTable.cc+");
	CrispParticleTable *cpt = CrispParticleTable::Instance();
	TGraph *T[19];
	Double_t Emax = 7000; Int_t N = 100000;
	Double_t pass = (Emax - 1)/N;
	for(int i = 0; i < 19; i++)
		T[i] = new TGraph();
	for(int i = 0; i <= N; i++){
		Double_t s = 1 + i*pass;
		T[0]->SetPoint(i, s,  rho_elastic_cross_section(s));
		T[1]->SetPoint(i, s,  omega_elastic_cross_section(s));
		T[2]->SetPoint(i, s,  phi_elastic_cross_section(s));
		T[3]->SetPoint(i, s,  JPsi_elastic_cross_section(s));
		T[4]->SetPoint(i, s,  PionN_OmegaN_cross_section(s));
		T[5]->SetPoint(i, s,  OmegaN_PionN_cross_section(s));
		T[6]->SetPoint(i, s,  OmegaN_RhoN_cross_section(s));
		T[7]->SetPoint(i, s,  RhoN_OmegaN_cross_section(s));
		T[8]->SetPoint(i, s,  OmegaN_elastic_cross_section(s));
		T[9]->SetPoint(i, s,  OmegaN_2PionN_cross_section(s));
		T[10]->SetPoint(i, s,  PionN_OmegaN1_cross_section(s)); 
		T[11]->SetPoint(i, s,  OmegaN_PionN1_cross_section(s));
		T[12]->SetPoint(i, s,  PionN_RhoN_cross_section(s));
		T[13]->SetPoint(i, s,  RhoN_PionN_cross_section(s));
		T[14]->SetPoint(i, s,  PionN_PhiN_cross_section(s));
		T[15]->SetPoint(i, s,  PhiN_PionN_cross_section(s));
		T[16]->SetPoint(i, s,  NJPsi_ND_cross_section(s));
		T[17]->SetPoint(i, s,  NJPsi_NDast_cross_section(s));
		T[18]->SetPoint(i, s,  NJPsi_NDDbar_cross_section(s));
	}
	/*ELASTIC*/
	TCanvas *c = new TCanvas("c","elastic Cross Section",600,800);
	c->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c->SetFrameFillColor(10);
	c->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TString name[4] = {"#rho","#omega","#phi","J/#Psi"};
	TMultiGraph *mg = new TMultiGraph();
	TLegend* leg = new TLegend(.75,.80,.95,.95);
	leg->SetFillColor(10);
	leg->SetHeader("Legend");
	for(int i = 0; i < 4; i++){
		T[i]->SetMarkerStyle(21 + i);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(1 + i);
		T[i]->SetLineColor(1 + i);
		mg->Add(T[i],"L");
		TString nameTemp = name[i] + " N elastic";
		leg->AddEntry(T[i], nameTemp.Data(),"L");
	}
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("s(GeV^{2})");				
	mg->GetYaxis()->SetTitle("#sigma (m bar)");				
	mg->GetYaxis()->SetRangeUser(1e-6,1e-1);
	mg->GetXaxis()->SetRangeUser(2.5,7000);
	leg->Draw("same");
	c->SaveAs("TotalHighEnergyMediumFromCPT_Elastic.pdf");
	/*ONE MESON EXCHANGE MODEL RHO, OMEGA, PHI*/
	TCanvas *c1 = new TCanvas("c1","meson exchange model rho, omega, phi meson Cross Section",600,800);
	c1->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TString sch[15] =  {"N#pi #rightarrow N#omega", "N#omega #rightarrow N#pi", "N#omega #rightarrow N#rho", "N#rho #rightarrow N#omega", "N#omega elastic", "N#omega #rightarrow N #pi #pi","N#pi #rightarrow N#omega" ,  "N#omega #rightarrow N#pi" ,"N#pi #rightarrow N#rho" ,  "N#rho #rightarrow N#pi", "N#pi #rightarrow N#phi" ,  "N#phi #rightarrow N#pi" ,  "NJ/#Psi #rightarrow #Lambda_{c} D" ,  "NJ/#Psi #rightarrow #Lambda_{c} D^{*}" ,  "NJ/#Psi #rightarrow #Lambda_{c} D D" };
	TMultiGraph *mg1 = new TMultiGraph();
	mg1->SetTitle("");
	TLegend* leg1 = new TLegend(.75,.80,.95,.95);
	leg1->SetFillColor(10);
	leg1->SetNColumns(2);
	leg1->SetHeader("Legend");
	for(int i = 4; i < 10; i++){
		T[i]->SetMarkerStyle(21 + i-4);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(1 + i - 4);
		T[i]->SetLineColor(1 + i - 4);
		mg1->Add(T[i],"L");
		leg1->AddEntry(T[i], sch[i - 4].Data(),"L");
	}
	mg1->Draw("AP");
	mg1->GetXaxis()->SetTitle("s(GeV^{2})");				
	mg1->GetYaxis()->SetTitle("#sigma (m bar)");				
	mg1->GetYaxis()->SetRangeUser(1e-3,1e2);
	mg1->GetXaxis()->SetRangeUser(2.5,200);
	leg1->Draw("same");
	c1->SaveAs("TotalHighEnergyMediumFromCPT_MesonExch.pdf");
	/*RESSONANCE MODEL RHO, OMEGA, PHI*/
	TCanvas *c2 = new TCanvas("c2","Ressonance model rho, omega, phi meson Cross Section",600,800);
	c2->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c2->SetFrameFillColor(10);
	c2->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg2 = new TMultiGraph();
	mg2->SetTitle("");
	TLegend* leg2 = new TLegend(.75,.80,.95,.95);
	leg2->SetFillColor(10);
	leg2->SetHeader("Legend");
	leg2	->SetNColumns(2);
	for(int i = 10; i < 16; i++){
		T[i]->SetMarkerStyle(21 + i-10);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(1 + i - 10);
		T[i]->SetLineColor(1 + i - 10);
		mg2->Add(T[i],"L");
		leg2->AddEntry(T[i], sch[i - 4].Data(),"L");
	}
	mg2->Draw("AP");
	mg2->GetXaxis()->SetTitle("s(GeV^{2})");				
	mg2->GetYaxis()->SetTitle("#sigma (m bar)");				
	mg2->GetYaxis()->SetRangeUser(1e-3,1e2);
	mg2->GetXaxis()->SetRangeUser(2.5,200);
	leg2->Draw("same");
	c2->SaveAs("TotalHighEnergyMediumFromCPT_Ressonace.pdf");
	/*ONE MESON EXCHANGE MODEL J/Psi*/
	TCanvas *c3 = new TCanvas("c3","One meson model J/Psi meson Cross Section",600,800);
	c3->SetFillColor(42);
//	gStyle->SetOptStat(kFALSE);
	c3->SetFrameFillColor(10);
	c3->SetFillColor(10);
	gPad->SetLogx();
	gPad->SetLogy();
	TMultiGraph *mg3 = new TMultiGraph();
	mg3->SetTitle("");
	TLegend* leg3 = new TLegend(.75,.80,.95,.95);
	leg3->SetFillColor(10);
	leg3->SetHeader("Legend");
	for(int i = 16; i < 19; i++){
		T[i]->SetMarkerStyle(21 + i-16);
		T[i]->SetMarkerSize(1);
		T[i]->SetLineWidth(2);
		T[i]->SetLineStyle(1 + i - 16);
		T[i]->SetLineColor(1 + i - 16);
		mg3->Add(T[i],"L");
		leg3->AddEntry(T[i], sch[i - 4].Data(),"L");
	}
	mg3->Draw("AP");
	mg3->GetXaxis()->SetTitle("s(GeV^{2})");				
	mg3->GetYaxis()->SetTitle("#sigma (m bar)");				
	mg3->GetYaxis()->SetRangeUser(1e-3,1e3);
	mg3->GetXaxis()->SetRangeUser(10,7000);
	leg3->Draw("same");
	c3->SaveAs("TotalHighEnergyMediumFromCPT_JPsi.pdf");
}
