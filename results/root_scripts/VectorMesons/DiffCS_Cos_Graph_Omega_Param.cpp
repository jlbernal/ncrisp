/************************************
OBSERVACIONES
1. tuve que multiplicar por -1  en el calculo de z 
2. la equacion 2 del texto está equivocada o al menos no funciona el resultado da el doble de lo que da por la formula 
directa del libro. El problema es que la del articulo contiene la virtualidad del foton.
3. El t_min tiene que empezar desde -1.6 ya que es muy pequeño el valor para que sea detectado;
************************************/
{
 	ofstream* DiffCSfile = new ofstream ("Amplitude/Results/Data/VM_DiffCS_Param_Omega.out");

/*	*DiffCSfile << "##################################################################" << endl;
	*DiffCSfile << "#  Vector Meson Diff Cross Section Fitt parama Database  Omega   #" << endl;
	*DiffCSfile << "#########                                                #########" << endl;
	*DiffCSfile << "#               CRISP (Colaboração Río-São Paulo)                #" << endl;
	*DiffCSfile << "#########                                                #########" << endl;
	*DiffCSfile << "#                 ROOT CINT (C/C++ interpreter)                  #" << endl;
	*DiffCSfile << "#########                                                #########" << endl;
	*DiffCSfile << "#      Implementation file: DiffCS_Cos_Graph_Omega_Param.cpp     #" << endl;
	*DiffCSfile << "#########                                                #########" << endl;
	*DiffCSfile << "#                      Descripction:                             #" << endl;
	*DiffCSfile << "# Parameter fitt result Database of Omega vector-meson  Differen-#" << endl;
	*DiffCSfile << "# cial Cross Section until energy cut and virtuality Q² range    #" << endl;
	*DiffCSfile << "# between 0 and 35 GeV.                                          #" << endl;
	*DiffCSfile << "# Database store on file VM_DiffCS_Param_Omega.out               #" << endl;
	*DiffCSfile << "##################################################################" << endl;
*/
	const double _GeV2_mbar = 0.3894;	// Collins pag 3
	const int Nt    = 500;
	const int NW    = 500;
	const int NComp = 5;
	const int NQ    = 1;
	const double theta_0 = TMath::TwoPi() * 1e-2;
	const bool CnvPrint = true;
	const TString EqStr = "[0]+ [1]*exp([2] + [3]*x + [4]*x*x)";
	const TString EqStrLtx = "a_{0}+ a_{1}exp(a_{2} + a_{3}x + a_{4}x^{2})"; //for figures
	const int PAR = 5;

	const double MV = CPT::omega_mass/1000.; // masa en Gev (VERY IMPORTANT)
	const double MP = CPT::p_mass/1000.;
	const double MV_2 = TMath::Power(MV,2);
	const double MP_2 = TMath::Power(MP,2);

	*DiffCSfile << PAR << "  " << NQ +1 << "  " << NW << endl;
	*DiffCSfile << "# Q_2       W         ";   

	for (int ii = 0 ; ii < PAR-1 ; ii ++)
		*DiffCSfile << "a[" << ii << "]      ";
	*DiffCSfile << "a[" << PAR -1 << "]" << endl;
	
	double Q_2_i = 0.0;
	double Q_2_f = 35.;
	double Q_2_pass = (Q_2_f - Q_2_i)/NQ;
	for (int ii = 0 ; ii <= NQ ; ii ++){
		double Q_2  = Q_2_i + ii*Q_2_pass;
		double W_i = 5;
		double W_f = 100.;
		double W_pass = (W_f - W_i)/NW;
		double par[PAR + 1][NW];
		double parErr[PAR][NW];
		double _W[NW];
		for (int i = 0 ; i < NW ; i ++){
			double W = W_i + i*W_pass ;
			double W_2 = W*W;
			double t_max = VM_Mand_t (W_2, Q_2,1,MP,MV);
			double z_max = VM_Mand_z (W_2,Q_2,t_max,MP,MV);
			double DiffCScos_max = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z_max,t_max,Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
			double t = VM_Mand_t (W_2, Q_2,TMath::Cos(theta_0),MP,MV);
			double z = VM_Mand_z (W_2,Q_2,t,MP,MV);
			double DiffCScos = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z,t,Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
//			cout << " W["<< i << "] =  " << W << ", t = " << t <<  ", DiffCS_max(cos = 1) = " <<  DiffCScos_max <<  ", DiffCS(cos) = " <<  DiffCScos <<  endl;
			if (DiffCScos_max > 1e3*DiffCScos) {
				cout << "la energia de corte es W = " << W << endl << endl;
				W_f = W; 
				break;
			}
		}
		W_pass = (W_f - W_i)/NW;
		TGraph *gr;
		TF1 *f1 = new TF1("f1",EqStr,0.5,1);
		TCanvas *cComp[NQ+1][NComp];
 		ofstream *data[NQ+1][NComp];
		bool PrintComp = false;
		int nComp = -1;
		for (int i = 0 ; i < NW ; i ++){
			_W[i] = W_i + i*W_pass ;
			TString WComp = "%f";
			TString Q_2Comp = "%f";
			if (CnvPrint){
				for (int n1 = 0; n1 <= NComp; n1++){
					if (i == n1*(int)((double)NW/NComp)) {
						PrintComp = true;
						nComp++;
						WComp = Form(WComp.Data(),_W[i]);
						Q_2Comp = Form(Q_2Comp.Data(),Q_2);
						cComp[ii][nComp] = new TCanvas("cComp" + WComp,"",200,10,600,400);
						data[ii][nComp] =  new ofstream ("Amplitude/Results/DataFitt/DiffCS_Cos_Graph_Omega_Parm_" + Q_2Comp + "_" + WComp + ".out");
					}
				}
			}
			double W_2 = _W[i]*_W[i];
			double DiffCS[Nt];
			double t[Nt];
			double Cos[Nt];
			double t_lim_p = 0.5*( sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			double t_lim_m = 0.5*( -1*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2 - (W_2 + Q_2 - MV_2- 2*MP_2 ) + (Q_2 + MP_2)*(MV_2 - MP_2)/W_2); //Eq (13)
			if (t_lim_m < -2.5) t_lim_m = -2.5;
			double t_pass = (t_lim_p - t_lim_m)/Nt;
			for (int j = 0; j < Nt; j++){
				t[j] = t_lim_m + j* t_pass;
				double z = VM_Mand_z(W_2,Q_2,t[j],MP,MV);
				DiffCS[j] = _GeV2_mbar*VM_Diff_CrossSection_PhoProt__ProtOmega(z,t[j],Q_2)*sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) )/W_2/2;
				Cos[j] = (W_2*W_2 + W_2*(2*t[j] - (MP_2 + MP_2 + MV_2 - Q_2)) + (-Q_2 - MP_2)*(MV_2 - MP_2))/sqrt( Trg_Fun(W_2,-Q_2,MP_2)*Trg_Fun(W_2,MV_2,MP_2) ) ;
				if (PrintComp){
					*data[ii][nComp] << Cos[j] << "       "  << DiffCS[j] << endl;
				} 
			}
			// fitting parameters
			gr = new TGraph(Nt,Cos,DiffCS);// create graph
			gr->Fit("f1","QR");
			if ((PrintComp)&&(CnvPrint)){
	//			gPad->SetLogy();
				gr->GetYaxis()->SetNdivisions(5);
				gr->GetYaxis()->SetTitle("d#sigma/cos #theta (mb/rad)");
				gr->GetXaxis()->SetTitle("cos #theta (rad)");
				gr->Draw("AC");
				cComp[ii][nComp]->Update();
				TLatex LtxComp;
				double X = (cComp[ii][nComp]->GetUxmax() - cComp[ii][nComp]->GetUxmin())/10;
				double Y = (cComp[ii][nComp]->GetUymax() - cComp[ii][nComp]->GetUymin())/10;
				TString Chi = "%f";
				double CvPass = 0.8;
				Chi = Form(Chi.Data(),f1->GetChisquare()/(Nt - PAR));
				LtxComp.DrawLatex(cComp[ii][nComp]->GetUxmin() + X, cComp[ii][nComp]->GetUymax() - CvPass*Y, EqStrLtx);
				LtxComp.DrawLatex(cComp[ii][nComp]->GetUxmin() + X, cComp[ii][nComp]->GetUymax() - 2*CvPass*Y, "Q^{2} = " + Q_2Comp);
				LtxComp.DrawLatex(cComp[ii][nComp]->GetUxmin() + X, cComp[ii][nComp]->GetUymax() - 3*CvPass*Y, "W = " + WComp);
				for (int n2 = 0; n2 < PAR; n2++){
					TString PARstr = "%f";
					PARstr = Form(PARstr.Data(),f1->GetParameter(n2));
					TString NPar = "%d";
					NPar = Form(NPar.Data(),n2);
					LtxComp.DrawLatex(cComp[ii][nComp]->GetUxmin() + X, cComp[ii][nComp]->GetUymax() - (4*CvPass + CvPass*n2)*Y, "Par[" + NPar + "] = " + PARstr);
				}
				LtxComp.DrawLatex(cComp[ii][nComp]->GetUxmin() + X, cComp[ii][nComp]->GetUymax() - (4*CvPass + CvPass*PAR)*Y, "Chi-Square = " + Chi);
				cComp[ii][nComp]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Omega_FittComp_"  + Q_2Comp + "_" + WComp + ".pdf");
			}
			double _par[PAR];
			f1->GetParameters(&_par[0]);
			par[PAR][i] = f1->GetChisquare()/(Nt - PAR);
			*DiffCSfile << Q_2 << "     " << _W[i] << "    ";
			for (int k = 0; k < PAR; k++){
				if (k < PAR -1 ) *DiffCSfile << _par[k] << "    ";
					else *DiffCSfile << _par[k] << endl;
				par[k][i] = _par[k];
				parErr[k][i] = f1->GetParError(k)/_par[k];
				parErr[k][i] = sqrt(parErr[k][i]*parErr[k][i]);
//				cout << " Q² = " << Q_2  << " W = " << _W[i] << ", par[" << k << "][" << i << "] = " << par[k][i] << ", ChiSqr = " << par[PAR][i] << endl;
			}
			PrintComp = false;
	//		cout << " W = " << _W[i] << ", ChiSqr = " << par[PAR][i] << endl;
		}
			// drawing
		if ((CnvPrint)&&(ii==0)){
			TString EfStr = "%d";
			EfStr = Form(EfStr.Data(),(int)W_f);
			TLatex n;
			Double_t cpass, cmax, cmin, y1, y2;
			TCanvas *c[PAR + 1];
			TGraph *GR[PAR + 1]; //beside Chi square
			for (int i = 0; i <= PAR; i++){
				GR[i] = new TGraph(NW,_W,par[i]);
				TString NCv = "%d";
				NCv = Form(NCv.Data(),i);
				c[i] = new TCanvas("c" + NCv,"",200,10,600,400);
				GR[i]->GetYaxis()->SetNdivisions(5);
				if (i == PAR) GR[i]->GetYaxis()->SetTitle("Chi-Square");
					else GR[i]->GetYaxis()->SetTitle("Parameter " + NCv);
				GR[i]->GetXaxis()->SetTitle("W (GeV)");
				GR[i]->Draw("AC");
				c[i]->Update();
				cmax  = c[i]->GetUymax();
				cmin  = c[i]->GetUymin();
				cpass = TMath::Abs(cmax - cmin)/10.;
					y1 = cmax -     cpass;
				y2 = cmax - 2 * cpass;
				n.SetTextSizePixels(20);
				double X = (W_f - W_i)/10;
				if (i == PAR) n.DrawLatex(X, y1 , "Chi-Square");
					else  n.DrawLatex(X, y1 , "Parameter " + NCv);
				n.DrawLatex(X, y2, EqStrLtx);
				if (i == PAR) c[i]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Omega_Par_ChiSq.pdf");
					else c[i]->Print("Amplitude/Results/DiffCS/DiffCS_Cos_Omega_Par_" + NCv + "_" + EfStr + ".pdf");
			}
		}
	}
}	
