{
	gROOT->Reset();
	gROOT->ProcessLine(".L helpers/DataHelperH.cc+");
	const char *FileName[4];
	const char *FileCode[4];
	FileName[0] = "data/hyperon/C12/C12_p2_theta_0.out";
	FileCode[0] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[1] = "data/hyperon/C12/C12_p2_theta_1.out";
	FileCode[1] = "P2:Cos:n1:n2:nn:p1:p2:np";

	FileName[2] = "data/hyperon/C12/C12_p2_theta_con_recoil.out";
	FileCode[2] = "P2:Cos:p1:p2:np:n1:n2:nn";

	FileName[3] = "data/hyperon/C12/C12_p2_theta_sin_recoil.out";
	FileCode[3] = "P2:Cos:p1:p2:np:n1:n2:nn";

	cout << "*********************Hyperon analisis Begin**************************" << endl;
	TFile *fileExp 		= new TFile("exp_data/hyperon/HyperonHistExpData.root");
	TFile *fileRec 		= new TFile("results/mcmc/hyperon/C12R/C12R_176_200000.root");
	TFile *fileNoRec 	= new TFile("results/mcmc/hyperon/C12NR/C12NR_176_200000.root");

	TH1D *HypPNN_n1BRec, *HypPNN_n2BRec, *HypPNP_n1BRec, *HypPNP_p2BRec, *HypPNN_n1BLRec, *HypPNN_n2BLRec, *HypPNP_n1BLRec, *HypPNP_p2BLRec, *HypCosThetaNNBRec, *HypCosThetaNPBRec, *HypCosThetaNNBLRec,*HypCosThetaNPBLRec, *HypPairNNBfRec, *HypPairNPBfRec, *HypKNRec, *HypKPRec, *HypKNNRec, *HypKNPRec, *HypCosThetaNNRec, *HypCosThetaNPRec, *HypPairNNRec, *HypPairNPRec;

	TH1D *HypPNN_n1BNoRec, *HypPNN_n2BNoRec, *HypPNP_n1BNoRec, *HypPNP_p2BNoRec, *HypPNN_n1BLNoRec, *HypPNN_n2BLNoRec, *HypPNP_n1BLNoRec, *HypPNP_p2BLNoRec, *HypCosThetaNNBNoRec, *HypCosThetaNPBNoRec, *HypCosThetaNNBLNoRec,*HypCosThetaNPBLNoRec, *HypPairNNBfNoRec, *HypPairNPBfNoRec, *HypKNNoRec, *HypKPNoRec, *HypKNNNoRec, *HypKNPNoRec, *HypCosThetaNNNoRec, *HypCosThetaNPNoRec, *HypPairNNNoRec, *HypPairNPNoRec;
/******************************CRISP RESULTS FILE ACCESS**************************************/
/***************************WITH RECOIL MODEL DATA FILE ACCESS************************************/
	fileRec->GetObject("HypPNN_n1B;1", HypPNN_n1BRec);
	fileRec->GetObject("HypPNN_n2B;1", HypPNN_n2BRec);
	fileRec->GetObject("HypPNP_n1B;1", HypPNP_n1BRec);
	fileRec->GetObject("HypPNP_p2B;1", HypPNP_p2BRec);
	fileRec->GetObject("HypPNN_n1BL;1", HypPNN_n1BLRec);
	fileRec->GetObject("HypPNN_n2BL;1", HypPNN_n2BLRec);
	fileRec->GetObject("HypPNP_n1BL;1", HypPNP_n1BLRec);
	fileRec->GetObject("HypPNP_p2BL;1", HypPNP_p2BLRec);
	fileRec->GetObject("HypCosThetaNNB;1", HypCosThetaNNBRec);
	fileRec->GetObject("HypCosThetaNPB;1", HypCosThetaNPBRec);
	fileRec->GetObject("HypCosThetaNNBL;1", HypCosThetaNNBLRec);
	fileRec->GetObject("HypCosThetaNPBL;1", HypCosThetaNPBLRec);
	fileRec->GetObject("HypPairNNBf;1", HypPairNNBfRec);
	fileRec->GetObject("HypPairNPBf;1", HypPairNPBfRec);
	fileRec->GetObject("HypKN;1", HypKNRec);
	fileRec->GetObject("HypKP;1", HypKPRec);
	fileRec->GetObject("HypKNN;1", HypKNNRec);
	fileRec->GetObject("HypKNP;1", HypKNPRec);
	fileRec->GetObject("HypCosThetaNN;1", HypCosThetaNNRec);
	fileRec->GetObject("HypCosThetaNP;1", HypCosThetaNPRec);
	fileRec->GetObject("HypPairNN;1", HypPairNNRec);
	fileRec->GetObject("HypPairNP;1", HypPairNPRec);

/***************************WITH NO RECOIL MODEL DATA FILE ACCESS************************************/
	fileNoRec->GetObject("HypPNN_n1B;1", HypPNN_n1BNoRec);
	fileNoRec->GetObject("HypPNN_n2B;1", HypPNN_n2BNoRec);
	fileNoRec->GetObject("HypPNP_n1B;1", HypPNP_n1BNoRec);
	fileNoRec->GetObject("HypPNP_p2B;1", HypPNP_p2BNoRec);
	fileNoRec->GetObject("HypPNN_n1BL;1", HypPNN_n1BLNoRec);
	fileNoRec->GetObject("HypPNN_n2BL;1", HypPNN_n2BLNoRec);
	fileNoRec->GetObject("HypPNP_n1BL;1", HypPNP_n1BLNoRec);
	fileNoRec->GetObject("HypPNP_p2BL;1", HypPNP_p2BLNoRec);
	fileNoRec->GetObject("HypCosThetaNNB;1", HypCosThetaNNBNoRec);
	fileNoRec->GetObject("HypCosThetaNPB;1", HypCosThetaNPBNoRec);
	fileNoRec->GetObject("HypCosThetaNNBL;1", HypCosThetaNNBLNoRec);
	fileNoRec->GetObject("HypCosThetaNPBL;1", HypCosThetaNPBLNoRec);
	fileNoRec->GetObject("HypPairNNBf;1", HypPairNNBfNoRec);
	fileNoRec->GetObject("HypPairNPBf;1", HypPairNPBfNoRec);
	fileNoRec->GetObject("HypKN;1", HypKNNoRec);
	fileNoRec->GetObject("HypKP;1", HypKPNoRec);
	fileNoRec->GetObject("HypKNN;1", HypKNNNoRec);
	fileNoRec->GetObject("HypKNP;1", HypKNPNoRec);
	fileNoRec->GetObject("HypCosThetaNN;1", HypCosThetaNNNoRec);
	fileNoRec->GetObject("HypCosThetaNP;1", HypCosThetaNPNoRec);
	fileNoRec->GetObject("HypPairNN;1", HypPairNNNoRec);
	fileNoRec->GetObject("HypPairNP;1", HypPairNPNoRec);
/******************************CRISP END **************************************/

/******************************EXPERIMENTAL DATA FILE ACCESS**************************************/
	TH1F *TnnK, *TnpK, *th1_nnBCos, *th1_npBCos, *th1s_nB, *th1s_pB, *KPN;
	fileExp->GetObject("th1s_nnK;1",TnnK);
	fileExp->GetObject("th1s_npK;1",TnpK);
	fileExp->GetObject("th1_npBCos;1",th1_npBCos);
	fileExp->GetObject("th1_nnBCos;1",th1_nnBCos);
	fileExp->GetObject("th1s_nB;1",th1s_nB);
	fileExp->GetObject("th1s_pB;1",th1s_pB);
	fileExp->GetObject("KPN;1",KPN);
/******************************END**************************************/

Double_t scale;
TLegend *legend;
/****************************Pair kinetic Energy spectra. NN Channel**********************************/
	TCanvas *c1 = new TCanvas("c1","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c1->SetFrameFillColor(10);
	c1->SetFillColor(10);

	TnnK->SetLineWidth(2);
	TnnK->SetLineStyle(1);
	scale = TnnK->Integral()/HypKNNRec->Integral();
	HypKNNRec->SetLineWidth(2);
	HypKNNRec->SetLineStyle(2);
	HypKNNRec->Scale(scale);
	scale = TnnK->Integral()/HypKNNNoRec->Integral();
	HypKNNNoRec->SetLineWidth(2);
	HypKNNNoRec->SetLineStyle(3);
	HypKNNNoRec->Scale(scale);

	HypKNNNoRec->SetTitle("");
	HypKNNNoRec->Draw();
	HypKNNRec->Draw("same");
	TnnK->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(TnnK,"Exp Data");
	legend->AddEntry(HypKNNRec,"Recoil model");
	legend->AddEntry(HypKNNNoRec,"No Recoil model");
	legend->Draw("same");
	c1->SaveAs("PairkineticEnergyNN.eps");
/******************************END**************************************/

/****************************Pair kinetic Energy spectra. NP Channel**********************************/
	TCanvas *c2 = new TCanvas("c2","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c2->SetFrameFillColor(10);
	c2->SetFillColor(10);

	TnpK->SetLineWidth(2);
	TnpK->SetLineStyle(1);
	scale = TnpK->Integral()/HypKNPRec->Integral();
	HypKNPRec->SetLineWidth(2);
	HypKNPRec->SetLineStyle(2);
	HypKNPRec->Scale(scale);
	HypKNPRec->SetTitle("");
	scale = TnpK->Integral()/HypKNPNoRec->Integral();
	HypKNPNoRec->SetLineWidth(2);
	HypKNPNoRec->SetLineStyle(3);
	HypKNPNoRec->Scale(scale);

	HypKNPRec->Draw();
	HypKNPNoRec->Draw("same");
	TnpK->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(TnpK,"Exp Data");
	legend->AddEntry(HypKNPRec,"Recoil model");
	legend->AddEntry(HypKNPNoRec,"No Recoil model");
	legend->Draw("same");
	c2->SaveAs("PairkineticEnergyNP.eps");
/******************************END**************************************/

/****************************Pair CosTheta spectra. NN Channel**********************************/
	TCanvas *c3 = new TCanvas("c3","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c3->SetFrameFillColor(10);
	c3->SetFillColor(10);

	th1_nnBCos->SetLineWidth(2);
	th1_nnBCos->SetLineStyle(1);
	th1_nnBCos->SetMarkerStyle(21);
	th1_nnBCos->SetMarkerSize(1);
	scale = th1_nnBCos->Integral()/HypCosThetaNNRec->Integral();
	HypCosThetaNNRec->SetLineWidth(2);
	HypCosThetaNNRec->SetLineStyle(2);
	HypCosThetaNNRec->Scale(scale);
	scale = th1_nnBCos->Integral()/HypCosThetaNNNoRec->Integral();
	HypCosThetaNNNoRec->SetLineStyle(3);
	HypCosThetaNNNoRec->SetLineWidth(2);
	HypCosThetaNNNoRec->Scale(scale);

	th1_nnBCos->SetTitle("");
	th1_nnBCos->Draw();
	HypCosThetaNNRec->Draw("same");
	HypCosThetaNNNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(th1_nnBCos,"Exp Data");
	legend->AddEntry(HypCosThetaNNRec,"Recoil model");
	legend->AddEntry(HypCosThetaNNNoRec,"No Recoil model");
	legend->Draw("same");
	c3->SaveAs("PairCosThetaNN.eps");
/******************************END**************************************/

/****************************Pair CosTheta spectra. NP Channel**********************************/
	TCanvas *c4 = new TCanvas("c4","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c4->SetFrameFillColor(10);
	c4->SetFillColor(10);

	th1_npBCos->SetLineWidth(2);
	th1_npBCos->SetLineStyle(1);
	th1_npBCos->SetMarkerStyle(21);
	th1_npBCos->SetMarkerSize(1);
	scale = th1_npBCos->Integral()/HypCosThetaNPRec->Integral();
	HypCosThetaNPRec->SetLineWidth(2);
	HypCosThetaNPRec->SetLineStyle(2);
	HypCosThetaNPRec->Scale(scale);
	scale = th1_npBCos->Integral()/HypCosThetaNPNoRec->Integral();
	HypCosThetaNPNoRec->SetLineStyle(3);
	HypCosThetaNPNoRec->SetLineWidth(2);
	HypCosThetaNPNoRec->Scale(scale);

	th1_npBCos->SetTitle("");
	th1_npBCos->Draw();
	HypCosThetaNPRec->Draw("same");
	HypCosThetaNPNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(th1_npBCos,"Exp Data");
	legend->AddEntry(HypCosThetaNPRec,"Recoil model");
	legend->AddEntry(HypCosThetaNPNoRec,"No Recoil model");
	legend->Draw("same");
	c4->SaveAs("PairCosThetaNP.eps");
/******************************END**************************************/

/****************************Neutron single particle kinetic energy spectra**********************************/
	TCanvas *c5 = new TCanvas("c5","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c5->SetFrameFillColor(10);
	c5->SetFillColor(10);

	th1s_nB->SetLineWidth(2);
	th1s_nB->SetLineStyle(1);
	th1s_nB->SetMarkerStyle(20);
	th1s_nB->SetMarkerSize(1);
	
	scale = th1s_nB->Integral()/HypKNRec->Integral();
	HypKNRec->SetLineWidth(2);
	HypKNRec->SetLineStyle(2);
	HypKNRec->Scale(scale);
	scale = th1s_nB->Integral()/HypKNNoRec->Integral();
	HypKNNoRec->SetLineStyle(3);
	HypKNNoRec->SetLineWidth(2);
	HypKNNoRec->Scale(scale);

	th1s_nB->SetTitle("");
	th1s_nB->Draw();
	HypKNRec->Draw("same");
	HypKNNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(th1s_nB,"Exp Data. KEK.");
	legend->AddEntry(HypKNRec,"Recoil model");
	legend->AddEntry(HypKNNoRec,"No Recoil model");
	legend->Draw("same");
	c5->SaveAs("SinglePartNeutronK.eps");
/******************************END**************************************/

/****************************Proton single particle kinetic energy spectra**********************************/
	TCanvas *c6 = new TCanvas("c6","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c6->SetFrameFillColor(10);
	c6->SetFillColor(10);
//	KEK experiments report starting from 30-40 MeV bin, so normalization is set on this energy range.
	Double_t Sum_th1s_pB = 0, Sum_KPN = 0, Sum_HypKPRec = 0, Sum_HypKPNoRec = 0;
	for (Int_t i = 4; i < th1s_pB->GetNbinsX(); i++)
		Sum_th1s_pB += th1s_pB->GetBinContent(i);
	for (Int_t i = 4; i < KPN->GetNbinsX(); i++)
		Sum_KPN += KPN->GetBinContent(i);
	for (Int_t i = 4; i < HypKPRec->GetNbinsX(); i++)
		Sum_HypKPRec += HypKPRec->GetBinContent(i);
	for (Int_t i = 4; i < HypKPNoRec->GetNbinsX(); i++)
		Sum_HypKPNoRec += HypKPNoRec->GetBinContent(i);

	th1s_pB->SetLineWidth(2);
	th1s_pB->SetLineStyle(1);
	th1s_pB->SetMarkerStyle(20);
	th1s_pB->SetMarkerSize(1);

	scale = Sum_th1s_pB/Sum_KPN;
	KPN->SetLineWidth(2);
	KPN->SetLineStyle(1);
	KPN->SetMarkerStyle(24);
	KPN->SetMarkerSize(1);
	
	scale = Sum_th1s_pB/Sum_HypKPRec;
	HypKPRec->SetLineWidth(2);
	HypKPRec->SetLineStyle(2);
	HypKPRec->Scale(scale);
	scale = Sum_th1s_pB/Sum_HypKPNoRec;
	HypKPNoRec->SetLineWidth(2);
	HypKPNoRec->SetLineStyle(3);
	HypKPNoRec->Scale(scale);

	KPN->SetTitle("");
	KPN->Draw();
	th1s_pB->Draw("same");
	HypKPRec->Draw("same");
	HypKPNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(th1s_pB,"Exp Data. KEK.");
	legend->AddEntry(KPN,"Exp Data. FINUDA.");
	legend->AddEntry(HypKPRec,"Recoil model");
	legend->AddEntry(HypKPNoRec,"No Recoil model");
	legend->Draw("same");
	c6->SaveAs("SinglePartProtonK.eps");
/******************************END**************************************/

/****************************Neutron 2 single particle Momenta spectra before cascade**********************************/
	TCanvas *c7 = new TCanvas("c7","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c7->SetFrameFillColor(10);
	c7->SetFillColor(10);

	HypPNN_n2BRec->SetLineWidth(2);
	HypPNN_n2BRec->SetLineStyle(1);
	HypPNN_n2BNoRec->SetLineStyle(2);
	HypPNN_n2BNoRec->SetLineWidth(2);

	HypPNN_n2BLRec->SetLineWidth(2);
	HypPNN_n2BLRec->SetLineStyle(3);
	HypPNN_n2BLNoRec->SetLineStyle(4);
	HypPNN_n2BLNoRec->SetLineWidth(2);

	HypPNN_n2BRec->Draw();
	HypPNN_n2BNoRec->Draw("same");
	HypPNN_n2BLRec->Draw("same");
	HypPNN_n2BLNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(HypPNN_n2BRec,"Recoil model. CM.");
	legend->AddEntry(HypPNN_n2BNoRec,"No Recoil model. CM.");
	legend->AddEntry(HypPNN_n2BLRec,"Recoil model. Lab.");
	legend->AddEntry(HypPNN_n2BLNoRec,"No Recoil model. Lab.");
	legend->Draw("same");
	c7->SaveAs("SinglePartProtonMBCM.eps");
/******************************END**************************************/

/****************************Proton 2 single particle Momenta spectra before cascade**********************************/
	TCanvas *c8 = new TCanvas("c8","different scales hists",900,600);
	//create, fill and draw h1
	gStyle->SetOptStat(kFALSE);
	c8->SetFrameFillColor(10);
	c8->SetFillColor(10);

	HypPNP_p2BRec->SetLineWidth(2);
	HypPNP_p2BRec->SetLineStyle(1);
	HypPNP_p2BNoRec->SetLineStyle(2);
	HypPNP_p2BNoRec->SetLineWidth(2);

	HypPNP_p2BLRec->SetLineWidth(2);
	HypPNP_p2BLRec->SetLineStyle(3);
	HypPNP_p2BLNoRec->SetLineStyle(4);
	HypPNP_p2BLNoRec->SetLineWidth(2);

	HypPNP_p2BRec->Draw();
	HypPNP_p2BNoRec->Draw("same");
	HypPNP_p2BLRec->Draw("same");
	HypPNP_p2BLNoRec->Draw("same");

	legend = new TLegend(.75,.80,.95,.95);
	legend->AddEntry(HypPNP_p2BRec,"Recoil model. CM.");
	legend->AddEntry(HypPNP_p2BNoRec,"No Recoil model. CM.");
	legend->AddEntry(HypPNP_p2BLRec,"Recoil model. Lab.");
	legend->AddEntry(HypPNP_p2BLNoRec,"No Recoil model. Lab.");
	legend->Draw("same");
	c8->SaveAs("SinglePartProtonMBCM.eps");
/******************************END**************************************/

}
