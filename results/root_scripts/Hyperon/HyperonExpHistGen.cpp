/*
Experimental Histogram Sato et al. Generator
*/
{
cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!" << endl;	
//	Sato's Histogram
	TH1F *th1s_nnS = new TH1F("th1s_nnS","T_{nn} 's Histogram  of ^{12}_{#Lambda}C Sato et al.",25, 0, 250);
	TH1F *th1s_npS = new TH1F("th1s_npS","T_{np} 's Histogram  of ^{12}_{#Lambda}C Sato et al.",25, 0, 250);

	Int_t CnnS[12] = {950,700,1400,1400,1200,1600,420,400,1000,250,255,500};
	Int_t CnpS[12] = {360,710,1210,1410,1410,1460,1460,1060,350,250,150,150};

	Int_t InnS = 65;
	Int_t InpS = 85;
	for (int i = 0; i < 12; i++){
		for (int j = 0; j < CnnS[i]; j++)
			th1s_nnS->Fill(InnS);
		InnS = InnS + 10;
	}
	for (int i = 0; i < 12; i++){
		for (int j = 0; j < CnpS[i]; j++)
			th1s_npS->Fill(InpS);
		InpS = InpS + 10;
	}
//	Kim's back back region Histogram

	TH1F *th1s_nnK = new TH1F("th1s_nnK","T_{nn}'s Histogram on ^{12}_{#Lambda}C Kim et al. at back back region",25, 0, 250);
	TH1F *th1s_npK = new TH1F("th1s_npK","T_{np}'s Histogram on ^{12}_{#Lambda}C Kim et al. at back back region",25, 0, 250);

	Int_t CnnK[12] = {400,300,600,600,500,700,200,200,400,100,100,200};
	Int_t CnpK[12] = {400,800,1400,1600,1600,1700,1700,1200,400,300,200,200};

	Int_t InnK = 75;
	Int_t InpK = 85;
	for (int i = 0; i < 12; i++){
		for (int j = 0; j < CnnK[i]; j++)
			th1s_nnK->Fill(InnK);
		InnK = InnK + 10;
	}
	for (int i = 0; i < 12; i++){
		for (int j = 0; j < CnpK[i]; j++)
			th1s_npK->Fill(InpK);
		InpK = InpK + 10;
	}

//	Kim's NON back back region Histogram
	TH1F *th1s_nnK1 = new TH1F("th1s_nnK1","T_{nn}'s Histogram on ^{12}_{#Lambda}C Kim et al. at Non back back region",25, 0, 250);
	TH1F *th1s_npK1 = new TH1F("th1s_npK1","T_{np}'s Histogram on ^{12}_{#Lambda}C Kim et al. at Non back back region",25, 0, 250);

	Int_t CnnK1[15] = {100,500,200,300,300,400,0,0,0,0,200,0, 100, 0, 200};
	Int_t CnpK1[10] = {200,100,400,300,100,0,0,100,0,100};

	Int_t InnK1 = 65;
	Int_t InpK1 = 85;

	for (int i = 0; i < 10; i++){
		for (int j = 0; j < CnpK1[i]; j++)
			th1s_npK1->Fill(InpK1);
		InpK1 = InpK1 + 10;
	}
	for (int i = 0; i < 15; i++){
		for (int j = 0; j < CnnK1[i]; j++)
			th1s_nnK1->Fill(InnK1);
		InnK1 = InnK1 + 10;
	}

//	Garbarino's Histograms

	Double_t CGP[14] = {400,400,350,240,175,150,130,110,95,75,50,25,10,5};
	Double_t CGN_N[14] = {0,80,50,34,34,36,38,40,36,30,24,15,6,5};
	Double_t CGN_P[14] = {0,325,175,115,100,90,80,73,60,49,29,20,15,5};

	Double_t CGCosNP_N[20] = {9,8,7,6,5,4,3,3,3,3,3,2,1,1,0.5,0.5,0,0,0,0};
	Double_t CGCosNP_P[20] = {130,95,62,45,38,34,30,27,28,26,25,23,21,19,19,18,19,21,22,23};

	TH1F *th1G_P = new TH1F("th1G_P","Single Proton Spectra",25, 0, 250);
	TH1F *th1G_N = new TH1F("th1G_N","Single Neutron Spectra",25, 0, 250);
	TH1F *th1G_N_N = new TH1F("th1G_N_N","Single Neutron Spectra neutron-induced",25, 0, 250);
	TH1F *th1G_N_P = new TH1F("th1G_N_P","Single Neutron Spectra proton-induced",25, 0, 250);

	TH1F *th1G_Cos_np = new TH1F("th1G_Cos_np","Neutron-proton channel's Angular distribution",40, -1, 1);
	TH1F *th1G_Cos_np_N = new TH1F("th1G_Cos_np_N","Neutron-proton channel neutron-induced's Angular distribution",40, -1, 1);
	TH1F *th1G_Cos_np_P = new TH1F("th1G_Cos_np_P","Neutron-proton channel proton-induced's Angular distribution",40, -1, 1);
	
	for(Int_t i=0; i < 14; i++){
		th1G_P->SetBinContent(i+1,CGP[i]);
		th1G_N_N->SetBinContent(i+1,CGN_N[i]);
		th1G_N_P->SetBinContent(i+1,CGN_P[i]);
	} 

	for(Int_t i=1; i <= 20; i++){
		th1G_Cos_np_N->SetBinContent(2*i,CGCosNP_N[i-1]);;
		th1G_Cos_np_N->SetBinContent(2*i-1,CGCosNP_N[i-1]);;

		th1G_Cos_np_P->SetBinContent(2*i,CGCosNP_P[i-1]);;
		th1G_Cos_np_P->SetBinContent(2*i-1,CGCosNP_P[i-1]);;
	
	} 

	th1G_N->Add(th1G_N_N);
	th1G_N->Add(th1G_N_P);
	th1G_Cos_np->Add(th1G_Cos_np_P);
	th1G_Cos_np->Add(th1G_Cos_np_N);

//	Kim's Cos Theta Histogram
	TH1F *th1_nnCos = new TH1F("th1_nnCos","Y_{NN}(Cos #theta)'s Histogram  ^{12}_{#Lambda}C Kim et al.",40, -1, 1);
	TH1F *th1_npCos = new TH1F("th1_npCos","Y_{NP}(Cos #theta)'s Histogram  ^{12}_{#Lambda}C Kim et al.",40, -1, 1);

	th1_nnCos->SetBinContent(1,15.);
	th1_nnCos->SetBinContent(2,9.);
	th1_nnCos->SetBinContent(3,3.);
	th1_nnCos->SetBinContent(4,4.);
	th1_nnCos->SetBinContent(5,6.);
	th1_nnCos->SetBinContent(6,4.);
	th1_nnCos->SetBinContent(8,1.);
	th1_nnCos->SetBinContent(9,1.);
	th1_nnCos->SetBinContent(13,2.);
	th1_nnCos->SetBinContent(16,1.);
	th1_nnCos->SetBinContent(17,1.);
	th1_nnCos->SetBinContent(19,1.);
	th1_nnCos->SetBinContent(22,2.);
	th1_nnCos->SetBinContent(23,1.);
	th1_nnCos->SetBinContent(24,2.);
	th1_nnCos->SetBinContent(29,1.);
	th1_nnCos->SetBinContent(30,2.);
	th1_nnCos->SetBinContent(31,1.);
	th1_nnCos->SetBinContent(34,1.);
	th1_nnCos->SetBinContent(35,1.);
	th1_nnCos->SetBinContent(36,1.);
	th1_nnCos->SetBinContent(37,3.);
	th1_nnCos->SetBinContent(38,1.);

	th1_npCos->SetBinContent(1,45.);
	th1_npCos->SetBinContent(2,33.);
	th1_npCos->SetBinContent(3,11.);
	th1_npCos->SetBinContent(4,4.);
	th1_npCos->SetBinContent(5,3.);
	th1_npCos->SetBinContent(6,2.);
	th1_npCos->SetBinContent(7,2.);
	th1_npCos->SetBinContent(8,1.);
	th1_npCos->SetBinContent(15,1.);
	th1_npCos->SetBinContent(17,1.);
	th1_npCos->SetBinContent(18,1.);
	th1_npCos->SetBinContent(19,1.);
	th1_npCos->SetBinContent(21,1.);
	th1_npCos->SetBinContent(25,1.);
	th1_npCos->SetBinContent(35,1.);

	TH1F *th1_Kp = new TH1F("KP","Experimental Proton's Spectro",18, 0, 180);

	th1_Kp->SetBinContent(0,0.);
	th1_Kp->SetBinError(0,0.);
	th1_Kp->SetBinContent(1,80.58);
	th1_Kp->SetBinError(1,40.);
	th1_Kp->SetBinContent(2,52.82);
	th1_Kp->SetBinError(2,13.);
	th1_Kp->SetBinContent(3,45.25);
	th1_Kp->SetBinError(3,10.);
	th1_Kp->SetBinContent(4,37.69);
	th1_Kp->SetBinError(4,3.43);
	th1_Kp->SetBinContent(5,40.93);
	th1_Kp->SetBinError(5,6.75);
	th1_Kp->SetBinContent(6,40.56);
	th1_Kp->SetBinError(6,6.33);
	th1_Kp->SetBinContent(7,44.89);
	th1_Kp->SetBinError(7,7.12);
	th1_Kp->SetBinContent(8,47.42);
	th1_Kp->SetBinError(8,7.26);
	th1_Kp->SetBinContent(9,33.);
	th1_Kp->SetBinError(9,6.70);
	th1_Kp->SetBinContent(10,19.66);
	th1_Kp->SetBinError(10,6.27);
	th1_Kp->SetBinContent(11,2.72);
	th1_Kp->SetBinError(12,5.48);
	th1_Kp->SetBinContent(12,1.64);
	th1_Kp->SetBinError(12,1.22);
	th1_Kp->SetBinContent(13,2.);
	th1_Kp->SetBinError(13,0.6);
	th1_Kp->SetBinContent(14,0.92);
	th1_Kp->SetBinError(14,0.49);
	th1_Kp->SetBinContent(15,0.91);
	th1_Kp->SetBinError(15,0.44);
	th1_Kp->SetBinContent(16,0.);
	th1_Kp->SetBinError(16,0.);
	th1_Kp->SetBinContent(17,0.);
	th1_Kp->SetBinError(17,0.);
//	th1_Kp->SetBinContent(18,0.);
//	th1_Kp->SetBinError(18,0.);
//	th1_Kp->SetBinContent(19,0.);
//	th1_Kp->SetBinError(19,0.);

	TH1F *th1_KpN = new TH1F("KPN","Experimental Proton's Spectro",18, 0, 180);

	th1_KpN->SetBinContent(1,0.);
	th1_KpN->SetBinError(1,0.);
	th1_KpN->SetBinContent(2,7.6411031E-02);
	th1_KpN->SetBinError(2,3.1468198E-02);
	th1_KpN->SetBinContent(3,6.9016412E-02);
	th1_KpN->SetBinError(3,1.4574345E-02);
	th1_KpN->SetBinContent(4,5.1364139E-02);
	th1_KpN->SetBinError(4,6.3707461E-03);
	th1_KpN->SetBinContent(5,4.1959703E-02);
	th1_KpN->SetBinError(5,5.4100780E-03);
	th1_KpN->SetBinContent(6,5.2053038E-02);
	th1_KpN->SetBinError(6,6.1558597E-03);
	th1_KpN->SetBinContent(7,4.1182324E-02);
	th1_KpN->SetBinError(7,5.4479991E-03);
	th1_KpN->SetBinContent(8,4.9714573E-02);
	th1_KpN->SetBinError(8,6.1305789E-03);
	th1_KpN->SetBinContent(9,4.9980018E-02);
	th1_KpN->SetBinError(9,6.1116186E-03);
	th1_KpN->SetBinContent(10,4.2105071E-02);
	th1_KpN->SetBinError(10,6.1621796E-03);
	th1_KpN->SetBinContent(11,2.7157834E-02);
	th1_KpN->SetBinError(11,5.6755254E-03);
	th1_KpN->SetBinContent(12,7.4514975E-03);
	th1_KpN->SetBinError(12,5.4669594E-03);
	th1_KpN->SetBinContent(13,-1.2513965E-03);
	th1_KpN->SetBinError(13,4.6895770E-03);
	th1_KpN->SetBinContent(14,-1.2197956E-03);
	th1_KpN->SetBinError(14,4.0006768E-03);
	th1_KpN->SetBinContent(15,-1.8202132E-03);
	th1_KpN->SetBinError(15,2.3511087E-03);
	th1_KpN->SetBinContent(16,6.3201843E-04);
	th1_KpN->SetBinError(16,1.4726029E-03);
	th1_KpN->SetBinContent(17,0.);
	th1_KpN->SetBinError(17,0.);
	th1_KpN->SetBinContent(18,0.);
	th1_KpN->SetBinError(18,0.);
//	th1_KpN->SetBinContent(18,0.);
//	th1_KpN->SetBinError(18,0.);
//	th1_KpN->SetBinContent(19,0.);
//	th1_KpN->SetBinError(19,0.);

/*
	th1_KpN->SetBinContent(1,0.);
	th1_KpN->SetBinError(1,0.);
	th1_KpN->SetBinContent(2,5.5749487E-02);
	th1_KpN->SetBinError(2,2.2959197E-02);
	th1_KpN->SetBinContent(3,5.0354373E-02);
	th1_KpN->SetBinError(3,1.0633442E-02);
	th1_KpN->SetBinContent(4,3.7475273E-02);
	th1_KpN->SetBinError(4,4.6480959E-03);
	th1_KpN->SetBinContent(5,3.0613799E-02);
	th1_KpN->SetBinError(5,3.9471928E-03);
	th1_KpN->SetBinContent(6,3.7977897E-02);
	th1_KpN->SetBinError(6,4.4913148E-03);
	th1_KpN->SetBinContent(7,3.0046623E-02);
	th1_KpN->SetBinError(7,3.9748601E-03);
	th1_KpN->SetBinContent(8,3.6271751E-02);
	th1_KpN->SetBinError(8,4.4728699E-03);
	th1_KpN->SetBinContent(9,3.6465421E-02);
	th1_KpN->SetBinError(9,4.4590365E-03);
	th1_KpN->SetBinContent(10,3.0719858E-02);
	th1_KpN->SetBinError(10,4.4959262E-03);
	th1_KpN->SetBinContent(11,1.9814355E-02);
	th1_KpN->SetBinError(11,4.1408632E-03);
	th1_KpN->SetBinContent(12,5.4366123E-03);
	th1_KpN->SetBinError(12,3.9886935E-03);
	th1_KpN->SetBinContent(13,-9.1301888E-04);
	th1_KpN->SetBinError(13,3.4215152E-03);
	th1_KpN->SetBinContent(14,-8.8996283E-04);
	th1_KpN->SetBinError(14,2.9188937E-03);
	th1_KpN->SetBinContent(15,-1.3280275E-03);
	th1_KpN->SetBinError(15,1.7153688E-03);
	th1_KpN->SetBinContent(16,4.6112065E-04);
	th1_KpN->SetBinError(16,1.0744111E-03);
	th1_KpN->SetBinContent(17,0.);
	th1_KpN->SetBinError(17,0.);
	th1_KpN->SetBinContent(18,0.);
	th1_KpN->SetBinError(18,0.);
//	th1_KpN->SetBinContent(18,0.);
//	th1_KpN->SetBinError(18,0.);
//	th1_KpN->SetBinContent(19,0.);
//	th1_KpN->SetBinError(19,0.);
*/
/* Experimento Bhang*/

	TH1F *th1s_nB = new TH1F("th1s_nB","T_{nn}'s Histogram on ^{12}_{#Lambda}C Bhang et al. at back back region",18, 0, 180);
	TH1F *th1s_pB = new TH1F("th1s_pB","T_{np}'s Histogram on ^{12}_{#Lambda}C Bhang et al. at back back region",18, 0, 180);

	th1s_pB->SetBinContent(4,0.0647623);
	th1s_pB->SetBinError(4,0.0086266);
	th1s_pB->SetBinContent(5,0.0605261);
	th1s_pB->SetBinError(5,0.0042328);
	th1s_pB->SetBinContent(6,0.0671832);
	th1s_pB->SetBinError(6,0.0039880);
	th1s_pB->SetBinContent(7,0.0515438);
	th1s_pB->SetBinError(7,0.0035263);
	th1s_pB->SetBinContent(8,0.0491899);
	th1s_pB->SetBinError(8,0.0034035);
	th1s_pB->SetBinContent(9,0.0354022);
	th1s_pB->SetBinError(9,0.0028729);
	th1s_pB->SetBinContent(10,0.0322680);
	th1s_pB->SetBinError(10,0.0027792);
	th1s_pB->SetBinContent(11,0.0206405);
	th1s_pB->SetBinError(11,0.0022115);
	th1s_pB->SetBinContent(12,0.0180262);
	th1s_pB->SetBinError(12,0.0020391);
	th1s_pB->SetBinContent(13,0.0089154);
	th1s_pB->SetBinError(13,0.0014407);
	th1s_pB->SetBinContent(14,0.0058463);
	th1s_pB->SetBinError(14,0.0011607);
	th1s_pB->SetBinContent(15,0.0047258);
	th1s_pB->SetBinError(15,0.0010429);
/*
35  0.0647623   0.0086266
45  0.0605261   0.0042328
55  0.0671832   0.0039880
65  0.0515438   0.0035263
75  0.0491899   0.0034035
85  0.0354022   0.0028729
95  0.0322680   0.0027792
105 0.0206405   0.0022115
115 0.0180262   0.0020391
125 0.0089154   0.0014407
135 0.0058463   0.0011607
145 0.0047258   0.0010429
*/

	th1s_nB->SetBinContent(2,0.2421850);
	th1s_nB->SetBinError(2,0.0092929);
	th1s_nB->SetBinContent(3,0.2006950);
	th1s_nB->SetBinError(3,0.0082310);
	th1s_nB->SetBinContent(4,0.1356110);
	th1s_nB->SetBinError(4,0.0061171);
	th1s_nB->SetBinContent(5,0.1232110);
	th1s_nB->SetBinError(5,0.0058376);
	th1s_nB->SetBinContent(6,0.1103010);
	th1s_nB->SetBinError(6,0.0056808);
	th1s_nB->SetBinContent(7,0.1040870);
	th1s_nB->SetBinError(7,0.0056608);
	th1s_nB->SetBinContent(8,0.0878503);
	th1s_nB->SetBinError(8,0.0053741);
	th1s_nB->SetBinContent(9,0.0763217);
	th1s_nB->SetBinError(9,0.0050824);
	th1s_nB->SetBinContent(10,0.0618569);
	th1s_nB->SetBinError(10,0.0047050);
	th1s_nB->SetBinContent(11,0.0443907);
	th1s_nB->SetBinError(11,0.0040448);
	th1s_nB->SetBinContent(12,0.0297059);
	th1s_nB->SetBinError(12,0.0034999);
	th1s_nB->SetBinContent(13,0.0238476);
	th1s_nB->SetBinError(13,0.0029730);
	th1s_nB->SetBinContent(14,0.0166273);
	th1s_nB->SetBinError(14,0.0025817);
	th1s_nB->SetBinContent(15,0.0090572);
	th1s_nB->SetBinError(15,0.0021395);


/*
15  0.2421850   0.0092929  
25  0.2006950   0.0082310
35  0.1356110   0.0061171
45  0.1232110   0.0058376
55  0.1103010   0.0056808
65  0.1040870   0.0056608
75  0.0878503   0.0053741
85  0.0763217   0.0050824
95  0.0618569   0.0047050
105 0.0443907   0.0040448
115 0.0297059   0.0034999
125 0.0238476   0.0029730
135 0.0166273   0.0025817
145 0.0090572   0.0021395
*/
	TH1F *th1_nnBCos = new TH1F("th1_nnBCos","Y_{NN}(Cos #theta)'s Histogram  ^{12}_{#Lambda}C Bhang et al.",32, -1, 0.6);
	TH1F *th1_npBCos = new TH1F("th1_npBCos","Y_{NP}(Cos #theta)'s Histogram  ^{12}_{#Lambda}C Bhang et al.",32, -1, 0.6);

	th1_npBCos->SetBinContent(1,0.03892);
	th1_npBCos->SetBinError(1,0.00592);
	th1_npBCos->SetBinContent(2,0.03356);
	th1_npBCos->SetBinError(2,0.00594);
	th1_npBCos->SetBinContent(3,0.02984);
	th1_npBCos->SetBinError(3,0.00691);
	th1_npBCos->SetBinContent(4,0.02025);
	th1_npBCos->SetBinError(4,0.00588);
	th1_npBCos->SetBinContent(5,0.00834);
	th1_npBCos->SetBinError(5,0.00424);
	th1_npBCos->SetBinContent(6,0.00751);
	th1_npBCos->SetBinError(6,0.00455);
	th1_npBCos->SetBinContent(7,0.00739);
	th1_npBCos->SetBinError(7,0.00522);
	th1_npBCos->SetBinContent(8,0.00760);
	th1_npBCos->SetBinError(8,0.00538);
	th1_npBCos->SetBinContent(9,0.00736);
	th1_npBCos->SetBinError(9,0.00736);
	th1_npBCos->SetBinContent(15,0.00494);
	th1_npBCos->SetBinError(15,0.00494);
	th1_npBCos->SetBinContent(17,0.00471);
	th1_npBCos->SetBinError(17,0.00507);
	th1_npBCos->SetBinContent(18,0.00579);
	th1_npBCos->SetBinError(18,0.00579);
	th1_npBCos->SetBinContent(19,0.00474);
	th1_npBCos->SetBinError(19,0.00474);
	th1_npBCos->SetBinContent(21,0.00473);
	th1_npBCos->SetBinError(21,0.00526);
	th1_npBCos->SetBinContent(25,0.01163);
	th1_npBCos->SetBinError(25,0.01163);

/*
-0.975  0.03892    0.00592
-0.925  0.03356     0.00594 
-0.875  0.02984     0.00691 
-0.825  0.02025     0.00588 
-0.775  0.00834     0.00424 
-0.725  0.00751     0.00455 
-0.675  0.00739     0.00522 
-0.625  0.00760     0.00538 
-0.575  0.00736     0.00736 
-0.275  0.00494     0.00494 
-0.175  0.00471     0.00507 
-0.125  0.00579     0.00579 
-0.075  0.00474     0.00474 
0.025   0.00473     0.00526 
0.225   0.01163     0.01163 
*/
	th1_nnBCos->SetBinContent(1,0.01957);
	th1_nnBCos->SetBinError(1,0.00493);
	th1_nnBCos->SetBinContent(2,0.01291);
	th1_nnBCos->SetBinError(2,0.00462);
	th1_nnBCos->SetBinContent(3,0.00639);
	th1_nnBCos->SetBinError(3,0.00395);
	th1_nnBCos->SetBinContent(4,0.01009);
	th1_nnBCos->SetBinError(4,0.00530);
	th1_nnBCos->SetBinContent(5,0.01860);
	th1_nnBCos->SetBinError(5,0.00767);
	th1_nnBCos->SetBinContent(6,0.01506);
	th1_nnBCos->SetBinError(6,0.00768);
	th1_nnBCos->SetBinContent(8,0.00437);
	th1_nnBCos->SetBinError(8,0.00437);
	th1_nnBCos->SetBinContent(9,0.00293);
	th1_nnBCos->SetBinError(9,0.00376);
	th1_nnBCos->SetBinContent(13,0.00577);
	th1_nnBCos->SetBinError(13,0.00483);
	th1_nnBCos->SetBinContent(16,0.00408);
	th1_nnBCos->SetBinError(16,0.00408);
	th1_nnBCos->SetBinContent(17,0.00307);
	th1_nnBCos->SetBinError(17,0.00342);
	th1_nnBCos->SetBinContent(19,0.00547);
	th1_nnBCos->SetBinError(19,0.00547);
	th1_nnBCos->SetBinContent(22,0.00920);
	th1_nnBCos->SetBinError(22,0.00692);
	th1_nnBCos->SetBinContent(23,0.00386);
	th1_nnBCos->SetBinError(23,0.00386);
	th1_nnBCos->SetBinContent(24,0.00649);
	th1_nnBCos->SetBinError(24,0.00459);
	th1_nnBCos->SetBinContent(29,0.00439);
	th1_nnBCos->SetBinError(29,0.00475);
	th1_nnBCos->SetBinContent(30,0.01038);
	th1_nnBCos->SetBinError(30,0.00734);
	th1_nnBCos->SetBinContent(31,0.00456);
	th1_nnBCos->SetBinError(31,0.00456);

/*
-0.975  0.01957     0.00493
-0.925  0.01291     0.00462
-0.875  0.00639     0.00395
-0.825  0.01009     0.00530
-0.775  0.01860     0.00767
-0.725  0.01506     0.00768
-0.625  0.00437     0.00437
-0.575  0.00293     0.00376
-0.375  0.00577     0.00483
-0.225  0.00408     0.00408
-0.175  0.00307     0.00342
-0.075  0.00547     0.00547
0.075   0.00920     0.00692
0.125   0.00386     0.00386
0.175   0.00649     0.00459
0.425   0.00439     0.00475
0.475   0.01038     0.00734
0.525   0.00456     0.00456
*/
Int_t n = 14;
Float_t x[n] = {0.15,0.2,0.26107, 0.3, 0.35,0.40,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8};
Float_t y[n] = {0.409533,0.423001,0.432938,0.437742,0.44866,0.451834,0.46493,0.471391,0.472799,0.494111,0.490304,0.497833,0.500249,0.499313};
// create the error arrays
Float_t ex[n] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
Float_t ey[n] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
// create the TGraphErrors and draw it
TGraph *gr = new TGraphErrors(n,x,y,ex,ey);
gr->SetName("NP");
gr->SetTitle("TGraphErrors Example");
gr->SetMarkerColor(4);
gr->SetMarkerStyle(21);

Int_t m = 6;
Float_t x1[m] = {0.,2.,4.,6.,8.,10.};
Float_t y1[m] = {0.317157,0.384917,0.46395,0.555192,0.661782,0.771373};
// create the error arrays
Float_t ex1[m] = {0.0,0.0,0.0,0.0,0.0,0.0};
Float_t ey1[m] = {0.0,0.0,0.0,0.0,0.0,0.0};
// create the TGraphErrors and draw it
TGraph *gr1 = new TGraphErrors(m,x1,y1,ex1,ey1);
gr1->SetName("V");
gr1->SetTitle("TGraphErrors Example");
gr1->SetMarkerColor(4);
gr1->SetMarkerStyle(21);


/*
0.15	0.409533
0.20	0.423001
0.26107	0.432938
0.3	0.437742
0.35	0.44866
0.40	0.451834
0.45	0.46493
0.5	0.471391
0.55	0.472799
0.6	0.494111
0.65	0.490304
0.7	0.497833
0.75	0.500249
0.8	0.499313
*/

	TObjArray Hlist (0);  // Creating an array of histograms

	Double_t scale = 1.0/th1s_nnS->Integral(); //normalize histograms
	th1s_nnS->Scale(scale);

	scale = 1.0/th1s_npS->Integral();
	th1s_npS->Scale(scale);

	scale = 1.0/th1s_nnK1->Integral();
	th1s_nnK1->Scale(scale);

	scale = 1.0/th1s_npK1->Integral();
	th1s_npK1->Scale(scale);

	scale = 1.0/th1_nnCos->Integral();
	th1_nnCos->Scale(scale);

	scale = 1.0/th1_npCos->Integral();
	th1_npCos->Scale(scale);

/*	scale = 1.0/th1_Kp->Integral();
	th1_Kp->Scale(scale);

	scale = 1.0/th1s_nB->Integral();
	th1s_nB->Scale(scale);

	scale = 1.0/th1s_pB->Integral();
	th1s_pB->Scale(scale);

	scale = 1.0/th1_nnBCos->Integral();
	th1_nnBCos->Scale(scale);

	scale = 1.0/th1_npBCos->Integral();
	th1_npBCos->Scale(scale);
*/
	scale = 1.0/th1G_P->Integral();
	th1G_P->Scale(scale);
	scale = 1.0/th1G_N->Integral();
	th1G_N->Scale(scale);
	scale = 1.0/th1G_Cos_np->Integral();
	th1G_Cos_np->Scale(scale);


	Hlist.Add(th1s_nnS);   // Adding histograms to the list 
	Hlist.Add(th1s_npS); 
  
	Hlist.Add(th1s_nnK);  
	Hlist.Add(th1s_npK);   

	Hlist.Add(th1s_nnK1);   
	Hlist.Add(th1s_npK1);   

	Hlist.Add(th1_nnCos);
	Hlist.Add(th1_npCos);

	Hlist.Add(th1s_nB);
	Hlist.Add(th1s_pB); 

	Hlist.Add(th1_nnBCos);
	Hlist.Add(th1_npBCos);

	Hlist.Add(th1_Kp);
	Hlist.Add(th1_KpN);
	Hlist.Add(gr);
	Hlist.Add(gr1);

	Hlist.Add(th1G_P);
	Hlist.Add(th1G_N);
	Hlist.Add(th1G_Cos_np);


	TFile f( "results/root_scripts/HyperonHistExpData.root","recreate");
	Hlist->Write();

	th1s_nnS->Delete();    
	th1s_npS->Delete(); 
  
	th1s_nnK->Delete();  
	th1s_npK->Delete();   

	th1s_nnK1->Delete();   
	th1s_npK1->Delete();   

	th1_nnCos->Delete();
	th1_npCos->Delete();

	th1s_nB->Delete();
	th1s_pB->Delete(); 

	th1_nnBCos->Delete();
	th1_npBCos->Delete();

	th1G_P->Delete();
	th1G_N_N->Delete();
	th1G_N_P->Delete();
	th1G_N->Delete();

	th1G_Cos_np_N->Delete();
	th1G_Cos_np->Delete();
	th1G_Cos_np_P->Delete();

	th1_Kp->Delete();
	gr->Delete();
	f.Close();
}
