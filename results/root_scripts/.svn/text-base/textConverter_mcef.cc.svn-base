#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TVectorD.h"

#include <iomanip>

using namespace std;

void textConverter_mcef(){
 
  gROOT->Reset();
  gROOT->ProcessLine(".L helpers/NameTable.hh");
  
  TString nucleusName = "";
  TString fname_in = "";
  TString generator = "";
  TString energy = "";
  
  Int_t Z = 0;
  Int_t N = 0;
  
  TString option = "";
  
  cout << "Which results do you want to convert? fission or spallation?" << endl;
  cin >> option;
  
  if(option.CompareTo("fission") == 0){
	 cout << "Enter (space separated) the nucleus name (Pb208, Am241, etc), generator (proton, photon, bremss, etc) and energy." << endl;
	 cin >> nucleusName >> generator >> energy;
	 fname_in = "results/mcef/" + generator + "/fission/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.root";
	 TString fname_out_1 = "results/mcef/" + generator + "/fission/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.txt";
	 TString fname_out_2 = "results/mcef/" + generator + "/fission/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef_CS.txt";
	 
	 Int_t A_fissao = 0, Z_fissao = 0, neutron = 0, proton = 0, alpha = 0;
	 Double_t fissility = 0., E_fissao = 0.;
	 
	 TFile *F = new TFile(fname_in.Data());
	 TTree *t = (TTree*)F->Get("history");
	 TBranch *Fission = (TBranch*)t->GetBranch("Fission");
	 int n = Fission->GetEntries();
	 TLeaf *l1 = (TLeaf*)Fission->GetLeaf("fiss_A");
	 l1->SetAddress(&A_fissao);
	 TLeaf *l2 = (TLeaf*)Fission->GetLeaf("fiss_Z");
	 l2->SetAddress(&Z_fissao);
	 TLeaf *l3 = (TLeaf*)Fission->GetLeaf("fiss_E");
	 l3->SetAddress(&E_fissao);
	 TLeaf *l4 = (TLeaf*)Fission->GetLeaf("fissility");
	 l4->SetAddress(&fissility);
	 TLeaf *l5 = (TLeaf*)Fission->GetLeaf("fiss_neutron");
	 l5->SetAddress(&neutron);
	 TLeaf *l6 = (TLeaf*)Fission->GetLeaf("fiss_proton");
	 l6->SetAddress(&proton);
	 TLeaf *l7 = (TLeaf*)Fission->GetLeaf("fiss_alpha");
	 l7->SetAddress(&alpha);
	 
	 ofstream outFile(fname_out_1.Data());
	 outFile << "# Fissioning nucleus information" << endl;
	 outFile << "#" << setw(70) << "=========== emissions ==========" << endl;
	 outFile << "#A" << setw(6) << "Z" << setw(12) << "E" << setw(15) << "fissility" << setw(12) << "neutrons" << setw(12) << "protons" << setw(12) << "alphas" << endl;
	 
	 for(int i=0; i<n; i++){
		Fission->GetEntry(i);
		outFile << A_fissao << setw(6) << Z_fissao << setw(12) << E_fissao << setw(15) << fissility << setw(12) << neutron << setw(12) << proton << setw(12) << alpha << endl;
	 }
	 
	 TVectorD *CS = (TVectorD*)F->Get("FissionCrossSection");
	 ofstream outFile(fname_out_2.Data());
	 outFile << "# Fission Cross Section" << endl;
	 outFile << CS(0) << endl;
  }
  if(option.CompareTo("spallation") == 0){
	 cout << "Enter information, space separated: nucleus (Au197, Pb208, etc), atomic number, event generator, energy (MeV) and number of products" << endl;
	 cin >> nucleusName >> Z >> generator >> energy >> N;
	 fname_in = "results/mcef/" + generator + "/spallation/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.root";
	 TString fname_out_1 = "results/mcef/" + generator + "/spallation/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.txt";
	 TString fname_out_2 = "results/mcef/" + generator + "/spallation/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef_Z%d.txt";
  
	 Int_t A_final = 0, Z_final = 0, neutron = 0, proton = 0, alpha = 0;
	 
	 TFile *F = new TFile(fname_in.Data());
	 TTree *t = (TTree*)F->Get("history");
	 TBranch *Spallation = (TBranch*)t->GetBranch("Spallation");
	 int n = Spallation->GetEntries();
	 TLeaf *l1 = (TLeaf*)Spallation->GetLeaf("spall_A");
	 l1->SetAddress(&A_final);
	 TLeaf *l2 = (TLeaf*)Spallation->GetLeaf("spall_Z");
	 l2->SetAddress(&Z_final);
	 TLeaf *l3 = (TLeaf*)Spallation->GetLeaf("spall_neutron");
	 l3->SetAddress(&neutron);
	 TLeaf *l4 = (TLeaf*)Spallation->GetLeaf("spall_proton");
	 l4->SetAddress(&proton);
	 TLeaf *l5 = (TLeaf*)Spallation->GetLeaf("spall_alpha");
	 l5->SetAddress(&alpha);
	 
	 ofstream outFile(fname_out_1.Data());
	 outFile << "# Residual nucleus information" << endl;
	 outFile << "#" << setw(43) << "=========== emissions ==========" << endl;
	 outFile << "#A" << setw(6) << "Z" << setw(12) << "neutrons" << setw(12) << "protons" << setw(12) << "alphas" << endl;
	 
	 for(int i=0; i<n; i++){
		Spallation->GetEntry(i);
		outFile << A_final << setw(6) << Z_final << setw(12) << neutron << setw(12) << proton << setw(12) << alpha << endl;
	 }
	 
	 NameTable nameTable;
	 Int_t z = 0;
	 string title;
	 TString file_products;
	 
	 for(int i=0; i<N; i++){
		z = Z-i;
		title = Form(("%d" + nameTable.get(z)).data(), z);
		file_products = Form(fname_out_2.Data(), z);
		ofstream fProd(file_products.Data());
		fProd << "# A" << setw(20) << "cross section" << endl;
		TGraph *calc = (TGraph*)F->Get(title.data());
		for(int j=0; j<calc->GetN(); j++){
		  fProd << calc->GetX()[j] << setw(20) << calc->GetY()[j] << endl;
		}
	 }
  } 
  
}