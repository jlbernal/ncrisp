#include "TString.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"

double GetF(double xo){

	const double fission_parm[48] = {0.00000, 0.00001,  0.00005,  0.00016,
												0.00037, 0.00073,  0.00126,  0.00201,
												0.00303, 0.00436,  0.00606,  0.00819,
												0.01084, 0.01409,  0.01808,  0.02293, 
												0.02876, 0.03542,  0.04258,  0.04997, 
												0.05747, 0.06503,  0.07260,  0.08017, 
												0.08773, 0.09526,  0.10276,  0.11023, 
												0.11765, 0.12502,  0.13235,  0.13962, 
												0.14684, 0.15400,  0.16110,  0.16814, 
												0.17510, 0.18199,  0.18880,  0.19553,
												0.20217, 0.20871,  0.21514,  0.22144, 
												0.22761, 0.23363,  0.23945,  0.24505 };
	double X = 1.;
	int i = 0; 
	const double d = .02;   

	for ( i = 0; i < 48; i++ ) {
		if ( xo > X ) 
			break;     
		X -= d;
	}  
  
	double X_1 = X - d, fo = 0.;
			
	if ( i < 47 ) 
		fo = ( (xo - X_1) * fission_parm[i] + (X - xo) * fission_parm[i+1]) / d;
	else 
		fo = fission_parm[47];  
	
	return fo;
}

double FissionBarrierCorrection(int A){

	double x = A;

	const double 	a1 = 18.03, 
					a2 = 101.8, 
					a3 = 934.6, 
					a4 = 822.3, 
					a5 = 30.34, 
					a6 = 341.4, 
					a7 = 736.5,
					//
					b1 = 10.92, 
					b2 = 19.77, 
					b3 = 47.3, 
					b4 = 94.15, 
					b5 = 122.8,
					b6 = 137.2, 
					b7 = 167.5, 
					//
					c1 = 7.319, 
					c2 = 17.01, 
					c3 = 40.18, 
					c4 = 37.25, 
					c5 = 22.5,
					c6 = 35.46, 
					c7 = 67.54,
					//
					p1 = -0.0003578, 
					p2 = 0.2026, 
					p3 = -28.03, 
					p4 = -230.9; 
  
	return 	p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
			a1 * exp(-pow(((x - b1) / c1), 2)) +
			a2 * exp(-pow(((x - b2) / c2), 2)) +
			a3 * exp(-pow(((x - b3) / c3), 2)) +
			a4 * exp(-pow(((x - b4) / c4), 2)) +
			a5 * exp(-pow(((x - b5) / c5), 2)) +
			a6 * exp(-pow(((x - b6) / c6), 2)) +
			a7 * exp(-pow(((x - b7) / c7), 2));
}

double CalculateBf_Antigo(int A, int Z){
	
	double Bf = 0.;
	int N = A - Z;
	const double 	as = 17.9439,  //MeV
			k  = 1.7826;

	double Es = 1. - k *  pow( (double)(N - Z)/(double)A, 2);
	
	Es = as * Es * pow(A, (2./3.) );

	double 	p1 = 50.88,
		p2 = 1.7826,
		xo = ( (double)(Z * Z)/(double)A ) / ( p1 * (1. - p2 * pow( (double)(N-Z)/(double)A, 2) ) );

	if( xo <= 0.06 ) 
		xo = 0.06;

	double fo = GetF(xo);  
	
	Bf = fo * Es;
	Bf = Bf + FissionBarrierCorrection(A);

	return Bf;
}

double CalculateBn(int A, int Z){
	return -.16 * (A - Z) + .25 * Z + 5.6;
}

void FissionBarrier_Analisys(){

	 Int_t A_fissao = 0.;
	 Int_t Z_fissao = 0.;
	 Double_t E_fissao = 0.;
	 
	 TString Generator = "bremss";
	 TString Nucleus = "Ta181";
	 TString Energy = "3500";

	 TString fname_in = "results/mcef/" + Generator + "/fission/" + Nucleus + "/" + Nucleus + "_" + Energy + "_mcef.root";

	 TFile *F = new TFile(fname_in.Data());
	 TTree *t = (TTree*)F->Get("history");
	 TBranch *Fission = (TBranch*)t->GetBranch("Fission");
	 int n = Fission->GetEntries();
	 TLeaf *l1 = (TLeaf*)Fission->GetLeaf("fiss_A");
	 l1->SetAddress(&A_fissao);
	 TLeaf *l2 = (TLeaf*)Fission->GetLeaf("fiss_Z");
	 l2->SetAddress(&Z_fissao);
	 TLeaf *l3 = (TLeaf*)Fission->GetLeaf("fiss_E");
	 l3->SetAddress(&E_fissao);
	 
	 TGraph *En = new TGraph(n);
	 En->SetMarkerStyle(20);
	 En->SetMarkerColor(kBlack);
	 En->SetMarkerSize(1.0);
	 
	 TGraph *Bf = new TGraph(n);
	 Bf->SetMarkerStyle(20);
	 Bf->SetMarkerColor(kRed);
	 Bf->SetMarkerSize(1.0);
	 
	 TGraph *Bn = new TGraph(n);
	 Bn->SetMarkerStyle(20);
	 Bn->SetMarkerColor(kBlue);
	 Bn->SetMarkerSize(1.0);
	 
	 double fissPar = 0., Barr = 0., Sn = 0.;
	 
	 for(int i=0; i<n; i++){
	   
		  Fission->GetEntry(i);
		  
		  fissPar = (Z_fissao*Z_fissao)/double(A_fissao);
		  Barr = CalculateBf_Antigo(A_fissao, Z_fissao);
		  Sn = CalculateBn(A_fissao, Z_fissao);
		  
		  En->SetPoint(i,fissPar,E_fissao);
		  Bf->SetPoint(i,fissPar,Barr);
		  Bn->SetPoint(i,fissPar,Sn);
	 }
	 
	 TCanvas *c = new TCanvas("c","Energy vs fissility parameter", 20, 20, 800, 500);
	 c->SetFillColor(10);
	 
	 TMultiGraph *mg = new TMultiGraph();
	 mg->Add(En, "P");
	 mg->Add(Bf, "P");
	 mg->Add(Bn, "P");
	 mg->Draw("AP");
	 mg->GetXaxis()->SetTitle("Z^{2}/A");
	 mg->GetYaxis()->SetTitle("Energy");
	 gPad->SetGridy();
	 
	 TLegend *leg =  new TLegend(0.8,0.9,0.99,0.99);
	 leg->AddEntry(En, "Excitation Energy", "P");
	 leg->AddEntry(Bf, "Fission Barrier", "P");
	 leg->AddEntry(Bn, "Neutron Sep Energy", "P");
	 leg->Draw();
	 leg->SetFillColor(10);

}