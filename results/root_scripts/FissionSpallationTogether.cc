#include "TTree.h"
#include "TString.h"
#include "TFile.h"

using namespace std;

void FissionSpallationTogether(){

  TString nucleusName = "";
  TString fname_in1 = "";
  TString fname_in2 = "";
  TString generator = "";
  TString energy = "";
  TString fname_out = "";
  
  cout << "Enter (space separated) the nucleus name (Pb208, Am241, etc), generator (proton, photon, bremss, etc) and energy." << endl;
  cin >> nucleusName >> generator >> energy;
  fname_in1 = "results/MultimodalFission/" + generator + "/" + nucleusName + "/" + nucleusName + "_" + energy + "_fragments.root";
  fname_in2 = "results/mcef/" + generator + "/spallation/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.root";
  fname_out = "results/" + nucleusName + "_" + generator + "_"  + energy + "_spalfiss.root";
  
  Int_t A = 0, Z = 0, A_final = 0, Z_final = 0, fragA = 0, fragZ = 0;
  
  TFile file(fname_out.Data(), "recreate");
  TTree *Hist = new TTree("history", "Fission + Spallation Tree");
  Hist->Branch("A",&A,"A/I");
  Hist->Branch("Z",&Z,"Z/I");
  
  TFile *F;
  F = new TFile(fname_in1.Data());
  TTree *t1 = (TTree*)F->Get("history");
  int n1 = t1->GetEntries();
  TBranch *b1 = (TBranch*)t1->GetBranch("FragA");
  b1->SetAddress(&fragA);
  TBranch *b2 = (TBranch*)t1->GetBranch("FragZ");
  b2->SetAddress(&fragZ);
  
  for(int i=0; i<n1; i++){
	 t1->GetEntry(i);
	 A = fragA;
	 Z = fragZ;
	 Hist->Fill();
  }
  F->Close();
  
  F = new TFile(fname_in2.Data());
  TTree *t2 = (TTree*)F->Get("history");
  TBranch *Spallation = (TBranch*)t2->GetBranch("Spallation");
  int n2 = Spallation->GetEntries();
  TLeaf *l1 = (TLeaf*)Spallation->GetLeaf("spall_A");
  l1->SetAddress(&A_final);
  TLeaf *l2 = (TLeaf*)Spallation->GetLeaf("spall_Z");
  l2->SetAddress(&Z_final);
  
  for(int i=0; i<n2; i++){
	 Spallation->GetEntry(i);
	 A = A_final;
	 Z = Z_final;
	 Hist->Fill();
  }
  F->Close();
  
    
  file.Write();
  file.Close();
}



