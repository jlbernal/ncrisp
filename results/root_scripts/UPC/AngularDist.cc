
void AngularDist(){
	
	//gROOT->ProcessLine(".L base/CrispParticleTable.cc");
	//gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Helper.cc");
	//gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Ampl_Terms.cc");
	//gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Amplitudes.cc");
	//gROOT->ProcessLine(".L Amplitude/CrossSection/VectorMeson_Diff_Cross_Sections.cc");
 	//CrispParticleTable *cpt = CrispParticleTable::Instance();
 	//cpt->DatabasePDG()->ReadPDGTable("data/crisp_table.txt");
	
	
	Int_t NPart(0), initialAttempts(0), TotalAtt(0); 
	Int_t Part_PID[500]; 
	Double_t Part_PX[500]; 
	Double_t Part_PY[500]; 
	Double_t Part_PZ[500];
	Double_t Part_E[500]; 
	
	double R = 1.18*pow(197,1./3.);
	double geoCS = TMath::Pi()*R*R*10.; //mb
	TString TargetName = "Au197";
	TString VMName = "Rho0";
	TString folder = "NoSecondaryProduc/RHIC";
	int VMid = 113;
	const int N = 3;
	//int E[] = {4.112e+03, 6.780e+03, 1.118e+04, 1.843e+04, 3.039e+04, 5.010e+04, 8.260e+04, 1.362e+05, 2.245e+05, 3.702e+05, 6.103e+05, 1.006e+06, 1.659e+06}; //MeV
	int E[] = {4.112e+03, 8.260e+04, 1.659e+06}; //MeV
	int Nevents = 40000;

	TString file = "results/mcmc/photon/" + TargetName + "/" + VMName + "/" + folder + "/" + TargetName + "_%dMeV_%d.root";
	TString file_out_name = "results/root_scripts/UPC/AngularDist_Rho0_RHIC.root";

	TFile fileOut(file_out_name.Data(),"recreate");

	TH1D **Histograms = new TH1D*[N];

	for(int i=0; i<N; i++){
		
		///obtaining from CRISP
		TLorentzVector photon(E[i], 0., 0., E[i]);
		TString histName = Form("hist_t_%d",i);
		TString histTitle = Form("|t| Distribution - E_{#gamma} = %.2f TeV;|t| [GeV^{2}];d#sigma/dt [mb/GeV^{2}]", E[i]/1.e6);
		hist_t[i] = new TH1D(histName.Data(),histTitle.Data(),100,0,2.5);
	
		TString File = Form(file.Data(), E[i], Nevents);
				
		TFile *f = new TFile(File.Data()); 
		TTree *tNuc = (TTree*)f->Get("History");
		tNuc->SetBranchAddress("initialAttempts",&initialAttempts);

		//***********UnBind Particle analysis*******************
		tNuc->SetBranchAddress("UnBindPart_Number",&NPart);
		tNuc->SetBranchAddress("UnBindPart_Pid",&Part_PID[0]);
		tNuc->SetBranchAddress("UnBindPart_PX",&Part_PX[0]);
		tNuc->SetBranchAddress("UnBindPart_PY",&Part_PY[0]);
		tNuc->SetBranchAddress("UnBindPart_PZ",&Part_PZ[0]);
		tNuc->SetBranchAddress("UnBindPart_E",&Part_E[0]);
		int n = tNuc->GetEntries();
				
		for(int k=0; k<n; k++){
			tNuc->GetEntry(k);
					
			TotalAtt += initialAttempts;
					
			for(int l=0; l<NPart; l++){
					
				if(Part_PID[l] == VMid) {
					//double pT = TMath::Sqrt( Part_PY[l]*Part_PY[l] + Part_PZ[l]*Part_PZ[l] );
					TLorentzVector meson(Part_PX[l], Part_PY[l], Part_PZ[l], Part_E[l]);
					double t = TMath::Abs( (photon-meson).M2() ) * 1e-6; //(p1 - p3)^2 //GeV^2
					//cout << "t: " << t << endl;
					hist_t[i]->Fill(t);
				}
			}
					
		} //loop nos eventos de cascata de um processo MPI
		delete f;
		
		//TCanvas *Cv = new TCanvas("Cv","|t| Distribution",10,20,600,450);
		//Cv->SetFillColor(10);
		
		double scale = (geoCS/TotalAtt);
		hist_t[i]->Scale(scale);
		//gStyle->SetOptStat(kFALSE);
		cout << "Writing histogram " << histName.Data() << endl;		
		gDirectory->Cd(fileOut.GetPath());
		//gDirectory->pwd();
		//fileOut.cd();
		hist_t[i]->Write();
		//hist_t->Draw();
		//hist_t->GetYaxis()->SetTitle("d#sigma/dt [mb/GeV^{2}]");
		//hist_t->GetXaxis()->SetTitle("|t|[GeV^{2}]");
		
		//Cv->Print(Form("results/root_scripts/UPC/AuAu_Rho0_MomentumTrans_%.3fTeV.pdf",E[i]/1.e6));
		
		//delete Cv;
		
	}
	fileOut.Close();
}
