

void AngularDist_fromCascadeSteps(){
	
	Int_t _Mes_PID[500];
	Double_t _Mes_Px[500];
	Double_t _Mes_Py[500];
	Double_t _Mes_Pz[500];
	Double_t _Mes_E[500];
	Double_t _Mes_K[500];
	
	TTree *tNuc, *t2;
	TBranch *Casc_Mes_PID, *Casc_Mes_Px, *Casc_Mes_Py, *Casc_Mes_Pz, *Casc_Mes_K, *Casc_Mes_E;
	
	double R = 1.18*pow(197,1./3.);
	double geoCS = TMath::Pi()*R*R*10.; //mb
	TString TargetName = "Au197";
	TString VMName = "Rho0";
	TString folder = "NoSecondaryProduc/RHIC/All_Cascade_History";
	int VMid = 113;
	const int N = 5;
	//int E[] = {4.112e+03, 6.780e+03, 1.118e+04, 1.843e+04, 3.039e+04, 5.010e+04, 8.260e+04, 1.362e+05, 2.245e+05, 3.702e+05, 6.103e+05, 1.006e+06, 1.659e+06}; //MeV
	int E[] = {4.112e+03, 1.843e+04, 8.260e+04, 3.702e+05, 1.659e+06}; //MeV
	//int E[] = {1.843e+04, 3.702e+05}; //MeV
	int Nevents = 40000;
	int mpiProcN = 64;
	
	int N_perhost = Nevents / mpiProcN;

	TString file = "results/mcmc/photon/" + TargetName + "/" + VMName + "/" + folder + "/" + TargetName + "_%dMeV_%d_%d.root";
	
	TCanvas *Cv = new TCanvas("Cv","|t| Distribution",10,20,600,450);
	Cv->SetFillColor(10);
	gStyle->SetOptStat(kFALSE);

	for(int i=0; i<N; i++){
		
		TLorentzVector photon(E[i], 0., 0., E[i]);
		TString histName = Form("|t| Distribution - E_{#gamma} = %.4f TeV;|t|[GeV^{2}];d#sigma/dt [mb/GeV^{2}]", E[i]/1.e6);
		TH1D *hist_t = new TH1D("|t|",histName.Data(),20,0,3.5);
	
		for(int j=0; j<mpiProcN; j++){
				TString File = Form(file.Data(), E[i], Nevents, j);

				cout << "Reading file: " << File.Data() << endl;
				
				TFile *f = new TFile(File.Data()); 
				tNuc = (TTree*)f->Get("History");
				int n = tNuc->GetEntries();
				
				int start = N_perhost * j;
				int end = N_perhost * (j + 1);
				//if(j=mpiProcN-1){
				//	end = Nevents-1;
				//}
				
				cout << "start: " << start << "  end: " << end << endl;
				
				for(int m=start; m<end; m++){
					TString Tname = Form ("Tree_%d",m);
					t2  = (TTree*)f->Get(Tname);
					Casc_Mes_PID = t2->GetBranch("Mes_Pid");
					Casc_Mes_PID->SetAddress(&_Mes_PID[0]);
					Casc_Mes_Px = t2->GetBranch("Mes_PX");
					Casc_Mes_Px->SetAddress(&_Mes_Px[0]);
					Casc_Mes_Py = t2->GetBranch("Mes_PY");
					Casc_Mes_Py->SetAddress(&_Mes_Py[0]);
					Casc_Mes_Pz = t2->GetBranch("Mes_PZ");
					Casc_Mes_Pz->SetAddress(&_Mes_Pz[0]);
					Casc_Mes_E = t2->GetBranch("Mes_E");
					Casc_Mes_E->SetAddress(&_Mes_E[0]);	
					
					int l = 0;
					Casc_Mes_PID->GetEntry(l);
					Casc_Mes_Px->GetEntry(l);
					Casc_Mes_Py->GetEntry(l);
					Casc_Mes_Pz->GetEntry(l);
					Casc_Mes_E->GetEntry(l);
					
					if(_Mes_PID[l] == VMid ) {
						//double pT = TMath::Sqrt( Part_PY[l]*Part_PY[l] + Part_PZ[l]*Part_PZ[l] );
						TLorentzVector meson(_Mes_Px[l], _Mes_Py[l], _Mes_Pz[l], _Mes_E[l]);
		
						double t = TMath::Abs( (photon-meson).M2() ) * 1e-6; //(p1 - p3)^2 //GeV^2
			
						//cout << "t: " << t << endl;
						hist_t->Fill(t);
					}
				}

				delete f;
		} //loop nos processos MPI

		hist_t->Draw();
		Cv->Print(Form("results/root_scripts/UPC/Rho0_MomentumTrans_%.4fTeV_fromCascadeSteps.jpg",E[i]/1.e6));
		//Cv->Update();
	}
	//delete f;
	
	//TCanvas *Cv = new TCanvas("Cv","|t| Distribution",10,20,600,450);
	//Cv->SetFillColor(10);
	
	//double scale = (geoCS/TotalAtt)*(1./1000.);
	//hist_t->Scale(scale);
		
	//gStyle->SetOptStat(kFALSE);
	//hist_t->Draw();
	//hist_t->GetYaxis()->SetTitle("d#sigma/dt [mb/GeV^{2}]");
	//hist_t->GetXaxis()->SetTitle("|t|[GeV^{2}]");
		
	//Cv->Print(Form("results/root_scripts/UPC/Rho0_MomentumTrans_%.4fTeV_fromCascadeSteps.jpg",E[i]/1.e6));
	
	//delete Cv;
}
