

void VM_CreationPosition(){
	
	int VM_ID = 113;

	TString nucleus = "Au197";
	Int_t NucA = 197;
	Int_t mpiProcN = 64, Nevents = 40000;
	Int_t N = Nevents / mpiProcN;
	
	const int energies = 5;
	Int_t E[energies] = {4.112e+03, 1.843e+04, 8.260e+04, 3.702e+05, 1.659e+06}; //MeV

	TH1D *hUnbound = new TH1D("hUnbound","Unbound Vector Meson",10,0,8);
	TH1D *hDestroyed = new TH1D("hDestroyed","Destroyed Vector Meson",10,0,8);
	
	TString file = "results/mcmc/photon/" + nucleus + "/Rho0/NoSecondaryProduc/RHIC/All_Cascade_History/" + nucleus + "_%dMeV_%d_%d.root";
	
	for (int m = 0; m < energies; m++){ // Energy cases

		for(int j=0; j<mpiProcN; j++){
		
			TString File = Form(file.Data(), E[m], Nevents, j);
			
			cout << "File: " << File.Data() << endl;
			TFile *f = new TFile(File.Data());

			//***********cascade history analysis*******************
			Int_t _NM;
			Int_t _Mes_PID[500];
			Bool_t  _Mes_IsB[500];
			Double_t Mes_X[500];
			Double_t Mes_Y[500];
			Double_t Mes_Z[500];
			
			TTree *t2;
			for (int n= j*N ; n < (j+1)*N; n++){
		
				TString Tname = Form("Tree_%d",n);
				t2  = (TTree*)f->Get(Tname);
				t2->SetBranchAddress("Mes_NM",&_NM);
				t2->SetBranchAddress("Mes_Pid",&_Mes_PID[0]);
				t2->SetBranchAddress("Mes_IsBind",&_Mes_IsB[0]);
				t2->SetBranchAddress("Mes_X",&Mes_X[0]);
				t2->SetBranchAddress("Mes_Y",&Mes_Y[0]);
				t2->SetBranchAddress("Mes_Z",&Mes_Z[0]);
				
				//cout << "pegou os branches" << endl;
			
				//read the branch for all entries
				Int_t nentries = t2->GetEntries(); //cascade steps
				Bool_t wasVMFirstCreated = false;
				Bool_t wasVMDestroyed = false;
				Bool_t wasVMUnBound = false;
				Double_t creationPos = 0., destructionPos = 0., unbindingPos = 0.;
				Int_t i=0;
				
				while( !(wasVMUnBound || wasVMDestroyed) && (i < nentries) ){

					Bool_t wasVMFoundOnEntry = false;
						
					t2->GetEntry(i);
					
					//cout << "dentro do while, depois de pegar entrada " << i << endl;
						
					for (int l=0; l<_NM; l++) {
						
						if (_Mes_PID[l] == VM_ID) {
							
							//cout << "Vector meson found" << endl;
							
							wasVMFoundOnEntry = true;
							
							if ((wasVMFoundOnEntry)&&(!wasVMFirstCreated)){
								creationPos = TMath::Sqrt( Mes_X[l]*Mes_X[l] + Mes_Y[l]*Mes_Y[l] + Mes_Z[l]*Mes_Z[l] );
								wasVMFirstCreated = true;
								//creationEntry = i;
							}
							if (!_Mes_IsB[l]) { // means it was unbound

								//cout << "desligado" << endl;
								unbindingPos = TMath::Sqrt( Mes_X[l]*Mes_X[l] + Mes_Y[l]*Mes_Y[l] + Mes_Z[l]*Mes_Z[l] );
								wasVMUnBound = true;		
								//unBindEntry = i;
							}
						} 
					}
					if ((wasVMFirstCreated)&&(!wasVMFoundOnEntry)){ //means it was destroyed

						//cout << "destruído" << endl;
						destructionPos = TMath::Sqrt( Mes_X[l]*Mes_X[l] + Mes_Y[l]*Mes_Y[l] + Mes_Z[l]*Mes_Z[l] );
						wasVMDestroyed = true;
						//destroyEntry = i;
					}
					i++;
				} //loop on cascade steps
				
				if(unbindingPos > 0.) hUnbound->Fill(creationPos);
				if(destructionPos > 0.) hDestroyed->Fill(creationPos);
				
				//cout << "histogramas feitos" << endl;
				
			} //loop on each cascade
		} //loop nos processos paralelos
	} //loop on each energy
	
// 	TH1D *hRatio = new TH1D("hRatio","Emitted/Reabsorbed JPsi",10,0,8);
// 	
// 	hRatio->Divide(hUnbound,hDestroyed);

	std::cout << "Unbound N = " << hUnbound->Integral() << std::endl;
	std::cout << "Destroyed N = " << hDestroyed->Integral() << std::endl;
	
	TCanvas *cv = new TCanvas("cv","Position of Unbound and Destroyed Vector Meson",10,20,700,500);
	cv->SetBorderMode(0);
	cv->SetFillColor(10);
	
	//cv->SetLogy();
	
	gStyle->SetOptStat(kFALSE);
	gStyle->SetOptTitle(kFALSE);
	
	//hRatio->SetLineColor(1);
	//hRatio->Draw();
	
// 	hRatio->GetYaxis()->SetTitle("Emitted / Reabsorbed");
// 	hRatio->GetXaxis()->SetTitle("|#vec{r}| (fm)");
// 	hRatio->GetXaxis()->SetTitleSize(0.05);
// 	hRatio->GetYaxis()->SetTitleSize(0.05);
// 	hRatio->GetYaxis()->SetLabelSize(0.05);
// 	hRatio->GetXaxis()->SetLabelSize(0.05);
// 	hRatio->GetYaxis()->SetTitleOffset(1.);
	
	hUnbound->SetLineColor(2);
	hUnbound->SetLineStyle(1);
	hUnbound->SetLineWidth(2);
	hDestroyed->SetLineColor(1);
	hDestroyed->SetLineStyle(2);
	hDestroyed->SetLineWidth(2);
	
	//NOTE: Use this only in case of variance reduction 
	//hUnbound->Scale(1./1000.);
	//hDestroyed->Scale(1./1000.);
	
	hDestroyed->Draw();
	hUnbound->Draw("same");

	hDestroyed->GetYaxis()->SetTitle("Count");
	hDestroyed->GetXaxis()->SetTitle("|#vec{r}| (fm)");
	hDestroyed->GetXaxis()->SetTitleSize(0.05);
	hDestroyed->GetYaxis()->SetTitleSize(0.05);
	hDestroyed->GetYaxis()->SetLabelSize(0.05);
	hDestroyed->GetXaxis()->SetLabelSize(0.05);
	hDestroyed->GetYaxis()->SetTitleOffset(0.9);
	
	TLegend leg(0.15, 0.72, 0.43, 0.85);
	leg.AddEntry(hUnbound, "Emitted", "L");
	leg.AddEntry(hDestroyed, "Reabsorbed", "L");
	leg.Draw();
	leg.SetFillColor(10);
	
	//cv->Print("results/root_scripts/UPC/VMCreationPosition.pdf");
	cv->Print("results/root_scripts/UPC/VMCreationPosition.eps");
	cv->Print("results/root_scripts/UPC/VMCreationPosition.jpg");

}
