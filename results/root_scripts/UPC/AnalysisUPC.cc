/* ================================================================================
 * 
 * 	Copyright 2015 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.
 * 
 *  ================================================================================
 * 
 * 	This script was designed to analyse the output of the intranuclear
 * 	cascade calculation and give transverse momentum and rapidity 
 * 	distribution of vector mesons using the method of virtual photons 
 * 	in order to account for results of ultra-peripheral collisions.
 * 
 * 	Although totally functional, this code is still under development.
 * 	It's first use was related to exclusive J/Psi photoproduction.
 * 	Any application to other particles or reactions
 * 	may require adaptations
 * 
 * 	Author: Evandro A. Segundo
 * ================================================================================
 */ 



#include "TGraphErrors.h"
#include "TGraph.h"
#include "TF1.h"
#include "TString.h"
#include "TTree.h"

#include <vector>

#define hbar 1.; 
#define c 1.; 
#define e 0.0854358;
#define alpha 1/137.034;
#define mp 938.2723;
#define r0 1.18;

class AnalysisUPC {
	
public:
	AnalysisUPC();
	virtual ~AnalysisUPC();
	
	void SetZ(int);
	void SetA(int);
	void SetZTarget(int);
	void SetATarget(int);
	void SetTargetName(TString);
	void SetZProj(int);
	void SetAProj(int);
	void SetGammaProj(double);
	void SetProjName(TString);
	void SetNucleusName(TString);
	
	void SetVMid(int); //vector meson ID
	void SetVMmass(double); //MeV
	void SetCMEnergy(double); //MeV
	void SetCSUnit(TString); //milibarn or microbarn
	void SetProcess(TString);
	
	void SetmpiProcN(int);
	void SetNevents(int);
	void SetNEnergies(int);
	void SetVarRedFactor(double);
	
	double GetTargetRadius(); //fm
	double GetProjRadius(); //fm
	double GetGeomCS(); //milibarn or microbarn
	
	TH1D** GetpTHistVector();
	TH1D** GetBackRapidityHistVector();
	TH1D** GetForwRapidityHistVector();
	TGraphErrors* GetCSForw();
	TGraphErrors* GetCSBack();
	TGraphErrors* GetCSTotal();
	TGraphErrors* Get_dsigmadE();
	
	TGraphErrors* PhotoNuclearCS(int [], TString);
	TF1* ConstructPhotonFlux();
	void ComputeCrossSection(TF1*, int []);
	void InitHists(int []);

	
private:
	int Z_Target, A_Target, Z_Proj, A_Proj, Z, A;
	TString TargetName, ProjName, NucleusName, VMName, unit;
	int VMid, mpiProcN, Nevents, Nenergies;
	double VMmass, CMEnergy, GammaBeam, GammaProj, VarRedFactor;
	TH1D **pTHistEn, **RapidityEnBack, **RapidityEnForw, **Rapidity;
	TGraphErrors *CS, *CSYBack, *CSYForw, *CSYTotal;
	
};

AnalysisUPC::AnalysisUPC(){
	
	SetZ(82);
	SetA(208);
	SetNucleusName("Pb208");
	
	SetVMName("JPsi");
	SetVMid(443); //vector meson ID
	SetVMmass(3096.916); //MeV
	SetCMEnergy(2.76); //TeV sqrt{s_NN}
	
	SetmpiProcN(4);
	SetNevents(250000);
	SetNEnergies(11);
	
	SetVarRedFactor(1000.);
	SetCSUnit("milibarn");
}

AnalysisUPC::~AnalysisUPC(){}

void AnalysisUPC::InitHists(int E[]){
	
	pTHistEn = new TH1D*[Nenergies];
	RapidityEnBack = new TH1D*[Nenergies];
	RapidityEnForw = new TH1D*[Nenergies];
	Rapidity = new TH1D*[Nenergies];
	
	for(int i=0; i<Nenergies; i++){
		TString name = Form("pTHistEn%d",i);
		TString title = Form("Vector Meson Transverse Momentum - E_{#gamma} = %3f;p_{T} [GeV/c];yield", E[i]/(double)1e6);
		pTHistEn[i] = new TH1D(name.Data(),title.Data(),100,0,2);
		
		TString name2 = Form("BackRapidityEn%d",i);
		TString title2 = Form("Backward Rapidity - E_{#gamma} = %3f; y; d#sigma/dy", E[i]/(double)1e6);
		RapidityEnBack[i] = new TH1D(name2.Data(),title2.Data(),50,-4,4);
		
		TString name3 = Form("ForwRapidityEn%d",i);
		TString title3 = Form("Forward Rapidity - E_{#gamma} = %3f; y; d#sigma/dy", E[i]/(double)1e6);
		RapidityEnForw[i] = new TH1D(name3.Data(),title3.Data(),50,-4,4);
		
		TString name4 = Form("RapidityTargRF_En%d",i);
		TString title4 = Form("Rapidity Target Ref. - E_{#gamma} = %3f; y; d#sigma/dy", E[i]/(double)1e6);
		Rapidity[i] = new TH1D(name4.Data(),title4.Data(),20,0,12);
	}
}

void AnalysisUPC::SetZ(int z){
	Z = z;
	Z_Target = z;
	Z_Proj = z;
}

void AnalysisUPC::SetA(int a){
	A = a;
	A_Target = a;
	A_Proj = a;
}

void AnalysisUPC::SetZTarget(int z){ Z_Target = z; }	
void AnalysisUPC::SetATarget(int a){ A_Target = a; }
void AnalysisUPC::SetTargetName(TString name){ TargetName = name; }
void AnalysisUPC::SetZProj(int z){ Z_Proj = z; }	
void AnalysisUPC::SetAProj(int a){ A_Proj = a; }

void AnalysisUPC::SetGammas(double gamma_b, double gamma_L){ 
	
	GammaProj = gamma_b; 
	GammaBeam = gamma_L;
}

void AnalysisUPC::SetProjName(TString name){ ProjName = name; }

void AnalysisUPC::SetNucleusName(TString name){ 
	
	NucleusName = name; 
	TargetName = name;
	ProjName = name;
}

void AnalysisUPC::SetVMid(int ID){ VMid = ID; }
void AnalysisUPC::SetVMmass(double mass){ VMmass = mass; }
void AnalysisUPC::SetVMName(TString name){ VMName = name; }
void AnalysisUPC::SetCMEnergy(double CME){ //CME is the energy of a pair of nucleons
	
	CMEnergy = CME*(double)A*1e6; //TeV -> MeV 
}

void AnalysisUPC::SetmpiProcN(int N){ mpiProcN = N;	}
void AnalysisUPC::SetNevents(int N){ Nevents = N; }
void AnalysisUPC::SetNEnergies(int N){ Nenergies = N; }
void AnalysisUPC::SetVarRedFactor(double f){ VarRedFactor = f; }

double AnalysisUPC::GetTargetRadius(){ 
	
	double R = r0*pow(A_Target,1./3.);
	return R;	
}

double AnalysisUPC::GetProjRadius(){ 
	
	double R = r0*pow(A_Proj,1./3.);
	return R;	
}

void AnalysisUPC::SetCSUnit(TString unidade){ //milibarn or microbarn
	
	unit = unidade;
}

double AnalysisUPC::GetGeomCS(){ 
	
	double R = GetTargetRadius();
	double geoCS = TMath::Pi()*R*R*10.; //mb
	if(unit.CompareTo("microbarn") == 0) geoCS = geoCS * 1000.;
	return geoCS;	
}

TH1D** AnalysisUPC::GetpTHistVector(){ return pTHistEn; }
TH1D** AnalysisUPC::GetBackRapidityHistVector(){ return RapidityEnBack; }
TH1D** AnalysisUPC::GetForwRapidityHistVector(){ return RapidityEnForw; }
TH1D** AnalysisUPC::GetRapidityHistVector(){ return Rapidity; } //Target Reference Frame
TGraphErrors* AnalysisUPC::GetCSForw(){ return CSYForw; }
TGraphErrors* AnalysisUPC::GetCSBack(){ return CSYBack; }
TGraphErrors* AnalysisUPC::GetCSTotal(){ return CSYTotal; }
TGraphErrors* AnalysisUPC::Get_dsigmadE(){ return CS; }

TGraphErrors* AnalysisUPC::PhotoNuclearCS(int E[], TString folder){
	
	Int_t Count(0), CountPC(0), initialAttempts(0), TotalAtt(0), NPart(0), neutron(0), proton(0);
	Int_t Part_PID[500]; 
	Double_t Part_PX[500]; 
	Double_t Part_PY[500]; 
	Double_t Part_PZ[500]; 
	Double_t Part_E[500]; 
	Double_t Part_K[500];
	
	double beta = TMath::Sqrt( 1. - 1./pow(GammaBeam, 2) ); //beta of the moving target
	double y_CMRef = (1./2.)*log( (1. + beta)/(1. - beta) ); //rapidity of the target in the center-of-mass reference frame
	
	CS = new TGraphErrors(Nenergies);
	CS->SetMarkerStyle(20);
	CS->SetMarkerColor(1);
	CS->SetTitle("Photonuclear Cross Section");
	CS->SetName("dsigma_dE");
	
	TString file = "results/mcmc/photon/" + TargetName + "/" + VMName + "/" + folder + "/" + TargetName + "_%dMeV_%d.root";
	
	for(int i=0; i<Nenergies; i++){
		
		vector<double> nP;
		vector<double> nPCoh;
		vector<double> nPInco;
		
		//for(int j=0; j<mpiProcN; j++){
			TString File = Form(file.Data(), E[i], Nevents);
			
			TFile *f = new TFile(File.Data()); 
			TTree *tNuc = (TTree*)f->Get("History");
			tNuc->SetBranchAddress("initialAttempts",&initialAttempts);

			//***********UnBind Particle analysis*******************
			tNuc->SetBranchAddress("UnBindPart_Number",&NPart);
			tNuc->SetBranchAddress("UnBindPart_Pid",&Part_PID[0]);
			tNuc->SetBranchAddress("UnBindPart_PX",&Part_PX[0]);
			tNuc->SetBranchAddress("UnBindPart_PY",&Part_PY[0]);
			tNuc->SetBranchAddress("UnBindPart_PZ",&Part_PZ[0]);
			tNuc->SetBranchAddress("UnBindPart_K",&Part_K[0]);
			tNuc->SetBranchAddress("UnBindPart_E",&Part_E[0]);
			int n = tNuc->GetEntries();
			
			for(int k=0; k<n; k++){
				tNuc->GetEntry(k);
				neutron = 0;
				proton = 0;
				bool pion = false;
				bool resonance = false;
				TotalAtt += initialAttempts;
				
				for(int l=0; l<NPart; l++){
				
					if(Part_PID[l] == VMid) {
						
						for(int v=0; v<NPart; v++){
							if(Part_PID[v] == -211 || Part_PID[v] == 211 || Part_PID[v] == 111){
								pion = true;
							}
							else if(Part_PID[v] == 1114 || Part_PID[v] == 2114 || Part_PID[v] == 2214 || Part_PID[v] == 2224){
								resonance = true;
							}
						}
						
						if(resonance || pion) break;  ///beyond this point production is exclusive
						
						for(int v=0; v<NPart; v++){
							if(Part_PID[v] == 2112){
								neutron++;
							}
							else if(Part_PID[v] == 2212){
								proton++;
							}
						}
						
						//if(neutron < 2) break;  ///selecting events according to number of neutrons emitted
						//if(proton < 2) break;  ///selecting events according to number of protons emitted
						
						double pT = TMath::Sqrt( Part_PY[l]*Part_PY[l] + Part_PZ[l]*Part_PZ[l] );

						Count++;
						CountPC++;
						pTHistEn[i]->Fill(pT/1000.); //GeV
						
						double y_TargRef = (1./2.)*log( (Part_E[l] + Part_PX[l]) / (Part_E[l] - Part_PX[l]) );
						RapidityEnBack[i]->Fill(y_TargRef - y_CMRef); //taking particle rapidity to the CM reference frame
						RapidityEnForw[i]->Fill(-(y_TargRef - y_CMRef)); //taking particle rapidity to the CM reference frame
						Rapidity[i]->Fill(y_TargRef);
					}
				}
				nP.push_back(CountPC/VarRedFactor); //Variance reduction
				CountPC = 0;
				
			} //loop on cascade events
		//} //loop on MPI processes
		delete f;

		double CountRenorm = 0., mean = 0., var = 0., sigma = 0., erroEst = 0., geoCS = 0.;

		CountRenorm = Count/VarRedFactor; //Variance reduction
			
		mean = CountRenorm/(double)TotalAtt;

		for(int m=0; m<n; m++){
			var = var + pow( (nP[m] - mean), 2 );
		}
		for(int m=0; m<TotalAtt-n; m++ ){
			var = var + pow( (0. - mean), 2 );
		}
		sigma = sqrt( var/(TotalAtt-1.) ); //standard deviation for vector meson counts
		erroEst = sigma/sqrt(TotalAtt); // mean standard deviation
			
		geoCS = GetGeomCS();

		//cout << "Count: " << Count << "  CountRenorm: " << CountRenorm << "  TotalAtt: " << TotalAtt << "  mean: " << mean << "  geoCS: " << geoCS <<  endl;		
			
		cout << "E: " << E[i]/1e6 << " TeV  CS: " << mean * geoCS << " +/- " << erroEst * geoCS << endl;
				
		CS->SetPoint(i,E[i], mean * geoCS ); //energy in MeV
		CS->SetPointError(i,0, erroEst * geoCS );
		
		pTHistEn[i]->Scale( (2.*(1./(double)TotalAtt)) / VarRedFactor ); //Variance Reduction - Factor 2 accounts for two identical ions colliding
		RapidityEnBack[i]->Scale( (geoCS/(double)TotalAtt) * (1./VarRedFactor) ); //Variance Reduction
		RapidityEnForw[i]->Scale( (geoCS/(double)TotalAtt) * (1./VarRedFactor) ); //Variance Reduction
		Rapidity[i]->Scale( (geoCS/(double)TotalAtt) * (1./VarRedFactor) ); //Variance Reduction
		
		TotalAtt = 0;
		Count = 0;
		nP.clear();	
	} //loop in the energies
	
	return CS;
}

Double_t PhotonFluxFunction(Double_t *x, Double_t *p){

	Int_t Z_proj = p[0];
	Int_t Z_targ = p[1];
	Double_t beta = p[2];
	Double_t gamma = p[3];
	Double_t E = p[4]; //energy enters here as a parameter
	
	Double_t v = ( E*x[0]*(1./197.) ) / ( beta*c*gamma*hbar );
	
	Double_t K1 = TMath::BesselK(1, v),
		 K0 = TMath::BesselK(0, v);
	
	Double_t flux = pow(Z_proj/TMath::Pi(), 2)*alpha*(1./(beta*beta))*(1./pow(x[0]*(1./197.),2))*v*v*( K1*K1 + (1./(gamma*gamma))*K0*K0 );
	
	return 2.*TMath::Pi()*x[0]*(1./197.)*flux*gT->Eval(x[0]);
}


Double_t N(Double_t *x, Double_t *p){
	
	Double_t E = x[0];
	
	Int_t Z_proj = p[0];
	Int_t Z_targ = p[1];
	Double_t beta = p[2];
	Double_t gamma = p[3];
	
	double bmin = 0., bmax = 1000.; //limits for impact parameter in fm

	TF1 *f = new TF1("f1",PhotonFluxFunction,bmin,bmax,5); 
	f->SetParameters(Z_proj, Z_targ, beta, gamma, E);

	Double_t INT = f->Integral(bmin,bmax);
	
	return INT;
}


Double_t analyticalN(Double_t *x, Double_t *p){
	
	Int_t Z_proj = p[0];
	Int_t Z_targ = p[1];
	Double_t beta = p[2];
	Double_t gamma = p[3];
	Double_t bmin = p[4]*(1./197.); //MeV^-1
	
	Double_t v = ( x[0]*bmin ) / ( beta*c*gamma*hbar );
	
	Double_t K1 = TMath::BesselK(1, v),
		 K0 = TMath::BesselK(0, v);
		 
	Double_t flux = (2./TMath::Pi()) * Z_proj*Z_proj * alpha * (1./(beta*beta)) * ( v*K0*K1 - (pow(beta*v, 2)/2.) * (K1*K1 - K0*K0) );
	
	return flux;
}

TF1* AnalysisUPC::ConstructPhotonFlux(){
	
	Double_t R_proj = GetProjRadius(); //projectile radius (fm)
	Double_t R_targ = GetTargetRadius(); //target radius (fm)

 	Double_t s = CMEnergy * CMEnergy;
 	Double_t En = ( ( s - 2.*A_Target*A_Proj*mp*mp )*c*c ) / ( 2.*A_Target*mp ); //projectile energy on target reference
 	Double_t p = pow( (En*En)/(c*c) - (A_Proj*A_Proj*mp*mp)*c*c, 1./2. ); //projectile momentum on target reference
 	Double_t beta = p/En;
 	double gamma_b = 1./sqrt(1. - beta*beta);
	double gamma_L = sqrt((gamma_b + 1.)/2.);
	//Double_t gamma_L = sqrt_sNN / (2.*mp);
	SetGammas(gamma_b, gamma_L);
	
	double b_min = R_targ + R_proj;
	Double_t Emin = 0.;
	Double_t Emax = CMEnergy - A_Target*mp - A_Proj*mp;/*upper energy limit in the photon flux (MeV) 
								in the rest frame of the target*/

	cout << "Photon Max Energy: " << Emax/1.e6 << " TeV" << endl;
								
	//numerical integration using the probability of survival
 	TF1 *N = new TF1("N", N, Emin, Emax, 4);
 	N->SetParameters(Z_Proj, Z_Target, beta, gamma_b);

	//analytical expression for the photon flux using bmin = 2R_targ
	TF1 *aN = new TF1("aN", analyticalN, Emin, Emax, 5);
	aN->SetParameters(Z_Proj, Z_Target, beta, gamma_b, b_min);
	
	return N;
}


void AnalysisUPC::ComputeCrossSection(TF1* N, int E[]){
	
	int nEn = Nenergies;
	
	CSYBack = new TGraphErrors(nEn);
	CSYBack->SetMarkerStyle(20);
	CSYBack->SetMarkerColor(2);
	CSYBack->SetTitle("Backward Rapidity Distribution");
	CSYBack->SetName("RapDistBack");
	
	CSYForw = new TGraphErrors(nEn);
	CSYForw->SetMarkerStyle(20);
	CSYForw->SetMarkerColor(1);
	CSYForw->SetTitle("Forward Rapidity Distribution");
	CSYForw->SetName("RapDistForw");
	
	CSYTotal = new TGraphErrors(nEn);
	CSYTotal->SetMarkerStyle(20);
	CSYTotal->SetMarkerColor(4);
	CSYTotal->SetTitle("Total Rapidity Distribution");
	CSYTotal->SetName("RapDistTotal");
	
	Double_t CSperY1 = 0., CSperY2 = 0., y = 0., forwErr = 0., backErr = 0.;
		
	///photonuclear cross section calculated by CRISP

	for(int i=0; i<nEn; i++){
				
		cout << "E: " << E[i] << "  N: " << N->Eval(E[i]) << endl;
			
		CSperY1 = N->Eval(E[nEn-1-i]) * CS->Eval(E[nEn-1-i]);
		forwErr = N->Eval(E[nEn-1-i]) * CS->GetEY()[nEn-1-i];
		CSperY2 = N->Eval(E[i]) * CS->Eval(E[i]);
		backErr = N->Eval(E[i]) * CS->GetEY()[i];
			
		y = log((E[i])/(GammaBeam*VMmass));
				
		CSYForw->SetPoint(i, y, CSperY1);
		CSYForw->SetPointError(i, 0, forwErr);
		CSYBack->SetPoint(i, y, CSperY2);
		CSYBack->SetPointError(i, 0, backErr);
	}	

	for(int i=0; i<CSYForw->GetN(); i++){
		double ErrorTotal = TMath::Sqrt( pow(CSYForw->GetEY()[i], 2.) + pow(CSYBack->GetEY()[i], 2.) );	
		CSYTotal->SetPoint(i, CSYForw->GetX()[i], CSYForw->GetY()[i]+CSYBack->GetY()[i]);
		CSYTotal->SetPointError(i, 0, ErrorTotal);
	}
	
	///Normalizing pT and Rapidity histograms for each energy 
	for(int i=0; i<nEn; i++){
		pTHistEn[i]->Scale( N->Eval(E[i])/E[i] );
		RapidityEnBack[i]->Scale( N->Eval(E[i]) );
		RapidityEnForw[i]->Scale( N->Eval(E[i]) );
		Rapidity[i]->Scale( N->Eval(E[i]) );
	}
}


///================================= AuAu
TString collision = "AuAu";
///================================= PbPb
//TString collision = "PbPb";
///======================================
TString survFile = "results/root_scripts/UPC/" + collision + "/survival_probability_" + collision + ".data";
TGraph *gT = new TGraph(survFile.Data(),"%lg %lg");

void AnalysisUPC(){
	
	AnalysisUPC VecM;
	
///============================================= MÉSON RHO0 ===============================================================================================================
	
/*	TString nucleus = "Au197";
	TString vecMeson = "Rho0";
	TString folder = "NoSecondaryProduc/RHIC";
	TString unit = "milibarn";
	int NEn = 13;
	double sqrt_sNN = 0.2; //TeV
	
	VecM.SetZ(79);
	VecM.SetA(197);
	VecM.SetNucleusName(nucleus);
	
	VecM.SetVMName(vecMeson);
	VecM.SetVMid(113); //vector meson ID
	VecM.SetVMmass(775.49); //MeV

	VecM.SetCMEnergy(sqrt_sNN); //TeV this is sqrt{s_NN}, center of mass energy of a pair of nucleons
	VecM.SetCSUnit(unit);
	
	VecM.SetNevents(40000);
	VecM.SetNEnergies(NEn);
	VecM.SetVarRedFactor(1.);
	
	//photon energies for sqrt_sNN = 0.2 TeV
	int E[] = {4.112e+03, 6.780e+03, 1.118e+04, 1.843e+04, 3.039e+04, 5.010e+04, 8.260e+04, 1.362e+05, 2.245e+05, 3.702e+05, 6.103e+05, 1.006e+06, 1.659e+06}; //MeV
	//int E[] = {4.112e+03, 1.843e+04, 8.260e+04, 3.702e+05, 1.659e+06}; //MeV
	*/
	
///=============================================  MÉSON JPSI ==============================================================================================================
/*
	TString nucleus = "Pb208";
	TString vecMeson = "JPsi";
	TString folder = "All_channels_On/LHC";
	TString unit = "milibarn";
	int NEn = 11;
	double sqrt_sNN = 2.76; //TeV
	
	VecM.SetZ(82);
	VecM.SetA(208);
	VecM.SetNucleusName(nucleus);
	
	VecM.SetVMName(vecMeson);
	VecM.SetVMid(443); //vector meson ID
	VecM.SetVMmass(3096.916); //MeV

	VecM.SetCMEnergy(sqrt_sNN); //TeV this is sqrt{s_NN}, center of mass energy of a pair of nucleons
	VecM.SetCSUnit(unit);
	
	VecM.SetNevents(250000);
	VecM.SetNEnergies(NEn);
	VecM.SetVarRedFactor(1000.);
	
	//int E[] = {80.827e3, 0.138e6, 0.219e6, 0.374e6, 0.597e6, 1.016e6, 1.623e6, 2.763e6, 4.413e6, 7.511e6, 11.996e6, 20.417e6, 32.608e6, 55.498e6, 88.637e6, 150.859e6, 240.942e6}; //MeV
	int E[] = {0.219e6, 0.597e6, 1.016e6, 1.623e6, 2.763e6, 4.413e6, 7.511e6, 11.996e6, 20.417e6, 32.608e6, 88.637e6}; //MeV
*/	
	///---------------------------------------------------------------------------------------------------------------------------------------------------------------
	TString nucleus = "Au197";
	TString vecMeson = "JPsi";
	TString folder = "All_channels_On/RHIC";
	TString unit = "microbarn";
	int NEn = 13;
	double sqrt_sNN = 0.2; //TeV
	
	VecM.SetZ(79);
	VecM.SetA(197);
	VecM.SetNucleusName(nucleus);
	
	VecM.SetVMName(vecMeson);
	VecM.SetVMid(443); //vector meson ID
	VecM.SetVMmass(3096.916); //MeV

	VecM.SetCMEnergy(sqrt_sNN); //MeV
	VecM.SetCSUnit(unit);
	
	VecM.SetNevents(40000);
	VecM.SetNEnergies(NEn);
	VecM.SetVarRedFactor(1000.);
	
	//int E[] = {4.467e4, 5.736e4, 7.365e4, 9.457e4, 1.214e5, 1.559e5, 2.002e5, 2.571e5, 3.301e5, 4.238e5, 5.442e5, 6.988e5, 8.972e5, 1.152e6, 1.479e6, 1.899e6, 2.439e6}; //MeV
	int E[] = {1.643e4, 2.708e4, 4.467e4, 7.365e4, 1.214e5, 2.002e5, 3.301e5, 5.442e5, 8.972e5, 1.479e6, 2.439e6, 4.019e6, 6.628e6}; //MeV

///=====================================================================================================================================================================
	
	VecM.InitHists(E);
	
	cout << endl;
	cout << "Virtual photon flux being calculated..." << endl;
	TF1* N = VecM.ConstructPhotonFlux();

	cout << endl;
	cout << "Calculating photonuclear cross section..." << endl;
	VecM.PhotoNuclearCS(E, folder);
		
	cout << endl;
	cout << "UPC cross section being calculated..." << endl;
	VecM.ComputeCrossSection(N, E);
		
	cout << endl;
	cout << "Recovering pT distribution and reconstructed rapidity..." << endl;
	
	TH1D **pT = NULL;
	TH1D **BackRap = NULL;
	TH1D **ForwRap = NULL;
	TH1D **Rap = NULL; //Target Reference Frame
		
	pT = VecM.GetpTHistVector();
	BackRap = VecM.GetBackRapidityHistVector();
	ForwRap = VecM.GetForwRapidityHistVector();
	Rap = VecM.GetRapidityHistVector();

	TH1D *pTHistTotal = new TH1D("pTHistTotal","Total Transverse Momentum;p_{T} [GeV]; Yield",100,0,2); //GeV
	if(unit.CompareTo("milibarn")==0){
		TH1D *RapHistTotal = new TH1D("RapHistTotal","Total Rapidity; y; d#sigma/dy [mb]",50,-4,4);
	}
	else if(unit.CompareTo("microbarn")==0){
		TH1D *RapHistTotal = new TH1D("RapHistTotal","Total Rapidity; y; d#sigma/dy [#mub]",50,-4,4);
	}
	TString file = "results/root_scripts/UPC/IncoherentUPC_" + vecMeson + "_" + nucleus + ".root"; 
	TFile F(file.Data(),"recreate");

	for(int i=0; i<NEn; i++){
		pT[i]->Write();
		BackRap[i]->Write();
		Rap[i]->Write();
		pTHistTotal->Add(pT[i]);
		RapHistTotal->Add(BackRap[i]);
		RapHistTotal->Add(ForwRap[i]);
	}
	pTHistTotal->Write();
	RapHistTotal->Write();
		
	TGraphErrors* CSforw = VecM.GetCSForw();
	TGraphErrors* CSback = VecM.GetCSBack();
	TGraphErrors* CStotal = VecM.GetCSTotal();
	TGraphErrors* CS = VecM.Get_dsigmadE();
	if(unit.CompareTo("milibarn")==0){
		CSforw->GetYaxis()->SetTitle("d#sigma/dy [mb]");
		CSforw->GetXaxis()->SetTitle("y");
		CSback->GetYaxis()->SetTitle("d#sigma/dy [mb]");
		CSback->GetXaxis()->SetTitle("y");
		CStotal->GetYaxis()->SetTitle("d#sigma/dy [mb]");
		CStotal->GetXaxis()->SetTitle("y");
		CS->GetYaxis()->SetTitle("d#sigma/dE [mb]");
	}
	else if(unit.CompareTo("microbarn")==0){
		CSforw->GetYaxis()->SetTitle("d#sigma/dy [#mub]");
		CSforw->GetXaxis()->SetTitle("y");
		CSback->GetYaxis()->SetTitle("d#sigma/dy [#mub]");
		CSback->GetXaxis()->SetTitle("y");
		CStotal->GetYaxis()->SetTitle("d#sigma/dy [#mub]");
		CStotal->GetXaxis()->SetTitle("y");
		CS->GetYaxis()->SetTitle("d#sigma/dE [#mub]");	
	}
	CS->GetXaxis()->SetTitle("E_{#gamma} [MeV]");
		
	CSforw->Write();
	CSback->Write();
	CStotal->Write();
	CS->Write();
	
	F.Close();
		
	cout << endl;
	cout << "File .root with all information generated at folder results/root_scripts/UPC!" << endl;
	
}
