/* ================================================================================
 * 
 * 	Copyright 2014 Evandro Andrade Segundo
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.
 * 
 * 	===========================================================================
 * 
 * 	This script was designed to integrate the analysis of J/Psi photoproduction
 * 	in PbPb collisions at sqrt_sNN = 2.76 TeV and AuAu collisions at 
 * 	sqrt_sNN = 0.2 TeV
 * 
 * 	Any application to other particles or reactions will require adaptations
 * 
 * 	Author: Evandro A. Segundo
 * ================================================================================
 */




#include "TF1.h"
#include "Math/SpecFunc.h"

Double_t transmission_probability( Double_t *x, Double_t *par ){
	
	double b = x[0];
	double Z_targ = par[0];
	double Z_proj = par[1];

	//double sigNN = 3.9; //nucleon-nucleon total cross section (fm^2) proton-neutron
	
	//parameters for Pb
	double rho0Pb = 0.16; //(fm^-3)
	double RPb = 6.62; // (fm)
	double aPb = 0.546; // (fm)
	
	//parameters for Au
	double rho0Au = 0.17; //(fm^-3)
	double RAu = 6.43; // (fm)
	double aAu = 0.541; // (fm)
	
	//double ftNucDensPb = 0.; //Fourier transform of nuclear density for Pb
	//double ftNucDensp = 0.; //Fourier transform of nuclear density for proton
	
	TF1 *J0_bessel = new TF1("J0","ROOT::Math::cyl_bessel_j(0, x*[0])",0.,6000.);
	J0_bessel->SetParameter(0,b);
	
	double min = 0., max = 10.;
	double Chi = 0.;
	
	if(Z_targ == 82.){
		TF1 *ftTarg = new TF1("ftTarg","((4.*TMath::Pi()*[0])/(x*x*x)) * ( sin(x*[1]) - x*[1]*cos(x*[1]) ) * ( 1./(1.+x*x*[2]*[2]) )",min,max);
		ftTarg->SetParameter(0, rho0Pb);
		ftTarg->SetParameter(1, RPb);
		ftTarg->SetParameter(2, aPb);
		
		if(Z_proj == Z_targ){
			
			double sigNNPbPb = 8.0; //nucleon-nucleon total cross section (fm^2)
			
			TF1 *integrand = new TF1("integrand","x*ftTarg*ftTarg*J0",min,max);
			double INT = integrand->Integral(min,max);
			Chi = (sigNNPbPb/(4.*TMath::Pi())) * INT;
			delete integrand;
			delete ftTarg;
			//cout << "Chi: " <<  Chi << "  T: " << TMath::Exp(-2. * Chi) << endl;
			return TMath::Exp(-2. * Chi);
		}
		else if(Z_proj == 1.){
			
			double a2Prot = 0.71; //(fm^-2)
			double sigNNpPb = 9.0; //nucleon-nucleon total cross section (fm^2)
			
			TF1 *ftProt = new TF1("ftProt","1./(1.+(x*x)/([0]))",min,max);
			ftProt->SetParameter(0, a2Prot);
			
			TF1 *integrand = new TF1("integrand","x*ftTarg*ftProt*J0",min,max);
			double INT = integrand->Integral(min,max);
			Chi = (sigNNpPb/(4.*TMath::Pi())) * INT;
			delete integrand;
			delete ftTarg;
			delete ftProt;
			return TMath::Exp(-2. * Chi);
		}
	}
	else if(Z_targ == 79.){
		TF1 *ftTarg = new TF1("ftTarg","((4.*TMath::Pi()*[0])/(x*x*x)) * ( sin(x*[1]) - x*[1]*cos(x*[1]) ) * ( 1./(1.+x*x*[2]*[2]) )",min,max);
		ftTarg->SetParameter(0, rho0Au);
		ftTarg->SetParameter(1, RAu);
		ftTarg->SetParameter(2, aAu);
		
		if(Z_proj == Z_targ){
			
			double sigNNAuAu = 5.3; //nucleon-nucleon total cross section (fm^2)
			
			TF1 *integrand = new TF1("integrand","x*ftTarg*ftTarg*J0",min,max);
			double INT = integrand->Integral(min,max);
			Chi = (sigNNAuAu/(4.*TMath::Pi())) * INT;
			delete integrand;
			delete ftTarg;
			//cout << "Chi: " <<  Chi << "  T: " << TMath::Exp(-2. * Chi) << endl;
			return TMath::Exp(-2. * Chi);
		}
		
	}
	else {
		cout << "Parameters for transmission probability not ye defined for this target. Assumed sharp cut-off on b_min." << endl;
		return 1.;
	}
	
	return 1.;
	
}

void SurvivalProbability(){
	
	double Z_targ = 82.;
	double Z_proj = 82.;
	
	TF1 *T = new TF1("T",transmission_probability,8.,20.,2);
	T->SetParameters(Z_targ, Z_proj);
	T->SetLineColor(1);
	
	//ofstream ofs("survival_probability_PbPb.data");
	
	double step = 0.;
	int n = 100;
	double b = 0.;
	step = 60./n;
	
// 	for(int i=0; i<=n; i++){
// 		
// 		b = i*step;
// 		
// 		ofs << b << "\t" << T->Eval(b) << endl;
// 	}
	
	TCanvas *Cv = new TCanvas("Cv","Transmission Probability",200,10,600,400);
	
	T->Draw();
	//T->DrawIntegral("same");
	
	gStyle->SetOptTitle(kFALSE);
	
	T->GetXaxis()->SetTitle("b (fm)");
	T->GetYaxis()->SetTitle("T(b)");
	T->GetXaxis()->SetLabelSize(0.05);
	T->GetYaxis()->SetLabelSize(0.05);
	T->GetXaxis()->SetTitleSize(0.05);
	T->GetYaxis()->SetTitleSize(0.05);
	T->GetYaxis()->SetTitleOffset(0.9);
	
	//Cv->Print("TransProb_PbPb.pdf");
	Cv->Print("TransProb_PbPb.eps");
	

	
}