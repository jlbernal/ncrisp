#include "TGraph.h"
#include "TH1.h"
#include "TString.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "TVectorD.h"

void multimodeFission(){
  
  TString nucleusName = "";
  TString fname_in = "";
  TString exp_file = "";
  TString energy = "";
  TString generator = "";
  TString normOption = "";
  Double_t mass = 0.;
  TString freq = "";
  
  cout << "Enter information, space separated: nucleus (Au197, Pb208, etc), mass number, event generator, energy, normalization option (crisp or data) and option for frequencies (yes or no)" << endl;
  cin >> nucleusName >> mass >> generator >> energy >> normOption >> freq;

  fname_in = "results/MultimodalFission/" + generator + "/" + nucleusName + "/" + nucleusName + "_" + energy + "_fragments.root";
  exp_file = "exp_data/data_Multimode/" + generator + "/" + nucleusName + "_" + energy + ".data";

  std::cout << "Ploting..." << std::endl << std::endl;

  //Experimental plot
  TGraphErrors *exp = new TGraphErrors(exp_file.Data(), "%lg %lg %lg");
  exp->SetMarkerStyle(20);
  exp->SetMarkerSize(0.8);

  //Simulation plot
  Int_t A = 0;
  TFile F(fname_in.Data());
  TTree *t = (TTree*)F.Get("history");
  t->SetBranchAddress("FragA",&A);
  int n = t->GetEntries();
  
  Double_t Amin = mass,
			  Amax = 0;
  Double_t Atemp = 0.;

  for(int i=0; i<exp->GetN(); i++){
	 Atemp = exp->GetX()[i];
	 if(Atemp < Amin) {
		Amin = Atemp;
	 }
	 if(Atemp > Amax) {
		Amax = Atemp;
	 }		
  }
  
  Amin = (int)Amin;
  Amax = (int)Amax;
  TH1D *h = new TH1D("hCalc",nucleusName.Data(),Amax-Amin+1,Amin,Amax+1); //+1 is to include both limits in the range
  
  for(int i=0; i<n; i++){
	 t->GetEntry(i);
	 h->Fill(A);
  }

  Double_t scale = exp->Integral()/h->Integral();
  
  if(normOption.CompareTo("crisp") == 0){
		
	 TString fname_mcef = "results/mcef/" + generator + "/fission/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.root";
	 TFile F2(fname_mcef.Data());
	 TVectorD *CS = (TVectorD*)F2.Get("FissionCrossSection");
	 scale = (2.*CS(0))/h->Integral();
  }
 
  if(freq.CompareTo("yes") == 0){
		
		TGraph *gr2 = new TGraph(h);
  		TString out_freq = "FragmentDist_Counting_" + nucleusName + "_" + energy + "_" + generator + ".txt";
  		std::ofstream file_freq(out_freq.Data());

  		file_freq << Form("A \t Counting \n");

  		for(int n=0 ; n < gr2->GetN() ; n++){

				int A = gr2->GetX()[n];
				double Y = gr2->GetY()[n];

				file_freq << Form("%d \t %f \n", A, Y);
		}
		delete gr2;
  }
  cout << "scale factor by ratio of the areas (A_exp./A_calc.): " << exp->Integral()/h->Integral() << endl;
  //cout << "scale by cs: " << 0.9/h->Integral() << endl; 
  h->Scale(scale);
  
  TGraph *gr1 = new TGraph(h);
  
  gr1->SetLineStyle(1); 

  TCanvas *c1 = new TCanvas("c","Mass Distribution", 200, 10, 600, 400);
  c1->SetFillColor(10);
  c1->SetBorderMode(0);
  
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(gr1, "L");
  mg->Add(exp, "P");
  
  gPad->SetLogy();

  mg->Draw("A");
  mg->GetXaxis()->SetTitle("A");
  mg->GetXaxis()->SetTitleSize(0.05);
  mg->GetXaxis()->SetLabelSize(0.05);
  if(generator.CompareTo("bremss") == 0){
	 mg->GetYaxis()->SetTitle("#Y_{A}, mb/eq.q.");
  } else if(generator.CompareTo("photon") == 0){
	 mg->GetYaxis()->SetTitle("#sigma(A) (#mub)");
  } else if(generator.CompareTo("proton") == 0 || generator.CompareTo("deuteron") == 0 || generator.CompareTo("ultra") == 0){
	 mg->GetYaxis()->SetTitle("#sigma(A) (mb)");
  }
  mg->GetYaxis()->SetTitleSize(0.05);
  mg->GetYaxis()->SetLabelSize(0.05);
  //mg->SetMinimum(0.1);
  gStyle->SetFrameFillColor(10);
  gStyle->SetOptTitle(kFALSE);

  leg = new TLegend(0.73,0.85,0.93,0.99);
  leg->AddEntry(gr1, "CRISP", "L");
  leg->AddEntry(exp,"Experimental", "P");
  leg->SetFillColor(10);
  leg->Draw();
  
  TString graph = "FragmentDist_" + nucleusName + "_" + energy + "_" + generator + ".eps";
  c1->Print(graph.Data());

}
