#include "TCanvas.h"
#include "TStyle.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TNtuple.h"
#include "TVectorD.h"
#include "TLatex.h"

#include <fstream>
#include <string>

using namespace std;

TGraphErrors* fillTGraphYError(const char* filename, Double_t energy_min, Double_t energy_max ){ 
  
	ifstream ifs;
	ifs.open(filename);
  
	Float_t x, y, dy;  
	//TFile f("fillTGraphYError.root","RECREATE");
	TNtuple *tuple = new TNtuple("aTuple","","x:y:dy");

	Int_t size = 0;
	while (true) {
    
		if (!ifs.good()) 
			break;   
    
		string pdline;
		getline(ifs, pdline);
    
		char c = pdline.c_str()[0];
    
		if ( c != '#' ) {      
			istringstream istr(pdline);      
			istr >> x >> y >> dy;
			
			if ( x > energy_min && x <= energy_max ) {	
				tuple->Fill(x, y, dy);   
				size++;      
			}
		}	 
	}
  
	tuple->SetBranchAddress("x",&x);
	tuple->SetBranchAddress("y",&y);
	tuple->SetBranchAddress("dy",&dy);
  
	Double_t *X = new Double_t[size];
	Double_t *Y = new Double_t[size];
	Double_t *DY = new Double_t[size];  
  
	for ( Int_t i = 0; i < size; i++ ) {    
		tuple->GetEntry(i);
		X[i] = x; 
		Y[i] = y; 
		DY[i] = dy;   
	}
	//f.Write();
	TGraphErrors* tge = new TGraphErrors(size, X, Y, 0, DY);

	return tge;
}

void photofission(){

  TString nucleusName = "";
  TString fname_in = "";
  TString exp_file = "";
  TString generator = "photon";

  cout << "Enter information: nucleus (Au197, Pb208, etc)" << endl;
  cin >> nucleusName;

  fname_in = "results/mcef/" + generator + "/" + nucleusName + "/" + nucleusName + "_%d_mcef.root";
  exp_file = "exp_data/mcef/photofission/" + nucleusName + ".dat";

  const int number_of_energies = 16;
  Int_t energies[number_of_energies] = { 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1100, 1200 };

  std::cout << "Running Photofission Plot... " << std::endl;

  TString title = "PhotoFission " + nucleusName;
  TCanvas *c1 = new TCanvas("c1", title.Data() ,200, 10, 700, 500);
  c1->SetBorderMode(0);
  c1->SetFillColor(10);

  TGraphErrors *grexp1 = fillTGraphYError(exp_file.Data(),energies[0],energies[number_of_energies-1]);
  grexp1->SetMarkerStyle(20);
  grexp1->SetMarkerSize(1.2);

  TGraphErrors *gr1 = new TGraphErrors(number_of_energies);
  gr1->SetMarkerStyle(4);
  gr1->SetMarkerSize(1.2);

  TMultiGraph *mg = new TMultiGraph();
  mg->SetTitle(title.Data());

  mg->Add(grexp1);
  mg->Add(gr1);
  
  for( Int_t i = 0; i < number_of_energies; i++ ){

	 TString inputFileName = Form(fname_in.Data(), energies[i]);
	 TFile F(inputFileName.Data());
	 std::cout << "Reading MCEF file: " << inputFileName.Data() << std::endl;

	 TVectorD *CS = (TVectorD*)F.Get("FissionCrossSection");
 	 gr1->GetX()[i] = energies[i];
 	 gr1->GetY()[i] = CS(0);
	 
	 delete CS;
  }

  mg->Draw("AP");
  mg->GetXaxis()->SetTitle("#omega [MeV]");
  mg->GetYaxis()->SetTitle("#sigma_{f, A}[#mu b]");
  mg->GetXaxis()->SetLabelSize(0.05);
  mg->GetYaxis()->SetLabelSize(0.05);
  mg->GetXaxis()->SetTitleSize(0.05);
  mg->GetYaxis()->SetTitleSize(0.05);

  TLegend *leg = new TLegend(0.65,0.80,0.95,0.99);
  leg->AddEntry(gr1, "CRISP", "P");
  leg->AddEntry(grexp1, "Experimental", "P");
  leg->Draw();
  leg->SetFillColor(10);


  gStyle->SetOptTitle(kFALSE);
	 
  TString graph = nucleusName + "photo.eps";
  c1->Print(graph.Data());
}
