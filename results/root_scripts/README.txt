
Explanation about the content of the folder results/root_scripts:

The scripts in this folder are for general use by users. They are not part
of the core of the CRISP code and do not require compilation. 

In fact, they don't use the classes of CRISP to work. These codes are pure ROOT
scripts that perform analysis and plots of the results and must be
run in the ROOT session (the interpreted mode). 

Some useful scripts for plots and analysis are included as examples. 
