#include "TStyle.h"
#include "TString.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPaveText.h"

#include <fstream>

using namespace std;

void spallation(){
  
  gROOT->Reset();
  gROOT->ProcessLine(".L helpers/NameTable.hh");
  
  TString nucleusName = "";
  TString fname_in = "";
  TString exp_file = "";
  TString nevents = "";
  TString generator = "";
  TString energy = "";
  
  Int_t Z = 0;
  Int_t N = 0;

  cout << "Enter information, space separated: nucleus (Au197, Pb208, etc), atomic number, event generator, energy (MeV) and number of products" << endl;
  cin >> nucleusName >> Z >> generator >> energy >> N;

  fname_in = "results/mcef/" + generator + "/" + nucleusName + "/" + nucleusName + "_" + energy + "_mcef.root";
  exp_file = "exp_data/mcef/spallation/" + nucleusName + "/e" + nucleusName + "_" + energy + "_Z%d.dat";
  
  std::cout << "Running Spallation Plot... " << std::endl;
  
  TString Ctitle = "Spallation " + nucleusName;
  TCanvas *c1 = new TCanvas( "c1", Ctitle.Data(), 20, 10, 500, 700 );
  c1->SetBorderMode(0);
  c1->SetFillColor(10);
  
  TPaveText pt1(0.42,0.005,0.62,0.04);
  pt1.SetBorderSize(0);
  TText *t1=pt1.AddText("mass number (A)");
  t1->SetTextSize(0.033);
  pt1.SetFillColor(10);

  TPaveText pt2(0.03,0.35,0.09,0.65);
  pt2.SetBorderSize(0);
  TString axis;
  if(generator.CompareTo("photon") == 0 || generator.CompareTo("bremss") == 0 || generator.CompareTo("ultra") == 0 || generator.CompareTo("hyperon") == 0) axis = "#sigma(A) [#mub]";
  if(generator.CompareTo("proton") == 0) axis = "#sigma(A) [mb]";
  TText *t2=pt2.AddText(axis.Data());
  t2->SetTextAngle(90);
  t2->SetTextSize(0.033);
  pt2.SetFillColor(10);

  TPaveText pt3(0.71,0.92,0.97,0.99);
  pt3.SetTextAlign(12);
  TText *t3=pt3.AddText("#circ CRISP");
  TText *t3=pt3.AddText("#bullet Experimental");
  pt3.SetFillColor(10);

  pt1.Draw();
  pt2.Draw();
  pt3.Draw();
  
  subpad = new TPad ("subpad","Titulo",0.1,0.05,0.97,0.9);
  subpad->SetBorderMode(0);
  subpad->SetFillColor(10);
  subpad->Draw();
  subpad->Divide(2,int(ceil(N/2.)));
  
  TMultiGraph *mg;
  TGraph *exp;
  
  TFile F(fname_in.Data());
  NameTable nameTable;
  Int_t z = 0;
  string title;
  for(int i=0; i<N; i++){
	 
	 z = Z-i;
	 title = Form(("%d" + nameTable.get(z)).data(), z);
	 TGraph *calc = (TGraph*)F.Get(title.data());
	 calc->SetMarkerStyle(4);
	 calc->SetMarkerSize(0.5);
	 
	 exp = new TGraph(Form(exp_file.Data(),z),"%lg %lg");
	 exp->SetMarkerStyle(20);
	 exp->SetMarkerSize(0.5);
	 
	 mg = new TMultiGraph();
	 mg->Add(calc);
	 mg->Add(exp);
	 mg->SetTitle(title.data());
	 
	 //global settings
	 gStyle->SetTitleFillColor(kWhite);
	 gStyle->SetTitleStyle(1001);
	 gStyle->SetTitleH(0.13);
	 gStyle->SetTitleW(0.16);
	 gStyle->SetTitleX(0.2);
	 gStyle->SetLabelSize(0.13,"X");
	 gStyle->SetLabelSize(0.13,"Y");
	 gStyle->SetNdivisions(403, "X");
	 
	 subpad->cd(i+1);
	 //local settings concerning the current Pad
	 gPad->SetLogy();
	 gPad->SetBorderMode(0);
	 
	 mg->Draw("AP");
  }
    
  TString figName = nucleusName + "spall.eps";
  c1->Print(figName.Data());
  
}

