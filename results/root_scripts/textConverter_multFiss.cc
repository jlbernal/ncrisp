#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TVectorD.h"

#include <iomanip>

using namespace std;

void textConverter_multFiss(){
 
  TString nucleusName = "";
  TString fname_in = "";
  TString generator = "";
  TString energy = "";
  
  Int_t Z = 0;
  Int_t N = 0;
  

  cout << "Enter (space separated) the nucleus name (Pb208, Am241, etc), generator (proton, photon, bremss, etc) and energy." << endl;
  cin >> nucleusName >> generator >> energy;
  fname_in = "results/MultimodalFission/" + generator + "/" + nucleusName + "/" + nucleusName + "_" + energy + "_fragments.root";
  TString fname_out = "results/MultimodalFission/" + generator + "/" + nucleusName + "/" + nucleusName + "_" + energy + "_fragments.txt";
  
  Int_t fragA = 0, fragZ = 0, neutron = 0, proton = 0, alpha = 0;
  
  TFile *F = new TFile(fname_in.Data());
  TTree *t = (TTree*)F->Get("history");
  int n = t->GetEntries();
  TBranch *b1 = (TBranch*)t->GetBranch("FragA");
  b1->SetAddress(&fragA);
  TBranch *b2 = (TBranch*)t->GetBranch("FragZ");
  b2->SetAddress(&fragZ);
  TBranch *b3 = (TBranch*)t->GetBranch("neutron");
  b3->SetAddress(&neutron);
  TBranch *b4 = (TBranch*)t->GetBranch("proton");
  b4->SetAddress(&proton);
  TBranch *b5 = (TBranch*)t->GetBranch("alpha");
  b5->SetAddress(&alpha);

  ofstream outFile(fname_out.Data());
  outFile << "# Fragments information" << endl;
  outFile << "#" << setw(43) << "=========== emissions ==========" << endl;
  outFile << "#A" << setw(6) << "Z" << setw(12) << "neutrons" << setw(12) << "protons" << setw(12) << "alphas" << endl;
  
  for(int i=0; i<n; i++){
	 t->GetEntry(i);
	 outFile << fragA << setw(6) << fragZ << setw(12) << neutron << setw(12) << proton << setw(12) << alpha << endl;
  }
  
}