/* ================================================================================
 * 
 * 	Copyright 2009
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include <fstream>
#include "BsfgModel.hh"

Tabela_a table_a("exp_data/mcef/shell.dat");
Tabela_a table_BSFG("exp_data/nuclear_masses/mass-hfb02.dat");

// ajuste dia 02/10/2009 chisqr = 151360.82
//double  prf1 = 1.01, prf2 = 1.1, prf3 = 1.0055964,  prf4 =1.02, prf5 = 1.02, prf6 =1.0348328, prf7 = 1.1,
//	  pp1  = 19.6,  pp2  =  1.7638206, pa1  = 20.000003,  pa2  = 4.532145, pn1  = 19.800002,  pn2  = 1.3000232,  pbf1 = -0.5;


#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(BsfgModel);
#endif // CRISP_SKIP_ROOTDICT

typedef CrispParticleTable CPT;


double BsfgModel::CalculateMass(int A, int Z)
{
	return McefModel::CalculateMass(A, Z);
}


/*
//usa-se para gerar o grafico de rf
double BsfgModel::CalculateRf(double y)
{
	double rf = 1.;

	// RF linear
	rf = prf1*y + prf2;	
	if(rf < 1.0) rf = 1.0;
	if(rf > 1.2) rf = 1.2;
	
	return rf;
}
*/



double BsfgModel::Mexp(int A, int Z)
{
	if( !table_BSFG.Exist(Z, A) )
		std::cout << "![Z:" << Z << ",A:" << A << "] ";

	double delta = table_BSFG.Get( Z, A );

	const double u = 931.2874;
	
	return delta + (A * u);// /(double)A;
}


double BsfgModel::MassCorrection( int A, int Z )
{
	double r = ( Mexp(A,Z) - CalculateMass(A, Z) );	
	return r;
}

double BsfgModel::CorrectionDerived( int A, int Z )
{
	double r = (1. / 4.) * (MassCorrection(A+1, Z+1) - MassCorrection(A-1, Z-1));
	return  r;
}


double BsfgModel::CalculateSd(int A, int Z)
{
	const double md = 1875.613;    // the deuteron mass in MeV. 
	return CalculateMass(A - 2, Z - 1) - CalculateMass(A, Z) + md;
}

double BsfgModel::CalculatePd(int A, int Z)
{
	return (1. / 4.) * ( pow (-1., Z+1 ) ) * ( CalculateSd (A+2, Z+1) - 2. * CalculateSd (A, Z) + CalculateSd (A-2, Z-1) );
}


double BsfgModel::a_modelo_BSFG(int A, int Z)
{
	double a;     
	double  p1 = 0.127,
		p2 = (4.98) * pow(10., -3.),
		p3 = (-8.95) * pow(10., -5.);
	
	int N = A - Z;
	int par1 = N % 2;
	int par2 = Z % 2;
	
	if( par1 == 0 && par2 == 0 ) 		// both N and Z even
	{
		a = A * ( p1 + p2 * ( MassCorrection(A, Z) - 0.5 * CalculatePd(A, Z) ) + p3 * A);
	}
	else if( par1 == 1 && par2 == 1 )	// both odd
	{
		a = A * ( p1 + p2 * (MassCorrection(A, Z) + 0.5 * CalculatePd(A, Z)) + p3 * A);
	}
	else 					// different parity
	{	
		a = A * ( p1 + p2 * MassCorrection(A, Z) + p3 * A);
	}
	
	return a;
}

double BsfgModel::CalculateAn(int A, int Z)
{
	if( !table_BSFG.Exist(Z, A-1) ){
		return McefModel::CalculateAn(A, Z);
	}
	return a_modelo_BSFG(A-1, Z);
}


double BsfgModel::CalculateAp(int A, int Z)
{
	if( !table_BSFG.Exist(Z-1, A-1) ){
		return McefModel::CalculateAp(A, Z);
	}
	return a_modelo_BSFG(A-1, Z-1);

}

double BsfgModel::CalculateAa(int A, int Z)
{
	if( !table_BSFG.Exist(Z-2, A-4) ){
		return McefModel::CalculateAa(A, Z);
	}
	return a_modelo_BSFG(A-4, Z-2);
}

