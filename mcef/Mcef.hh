/* ================================================================================
 * 
 * 	Copyright 2002, 2009, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef MCEF_pHH
#define MCEF_pHH

#include <iostream>
#include <vector>

#include "TObject.h"
#include "TRandom.h"
#include "TTree.h"
#include "TString.h"

#include "ParticleDynamics.hh"
#include "base_defs.hh"
//#include "McefModel.hh"
//#include "BsfgModel.hh"
#include "Models.hh"



class Particle : public TObject
{
public:
	int particleID;		//!< 0 == neutron, 1 == proton, 2 == alfa.            // CPT::proton_ID, CPT::neutron_ID
	double mass;
	double energy;
	
	Particle( int ID = 0, double energy = 0.0, double mass = 0.0 ) : TObject()
	{
		this->particleID = ID;
		this->mass = mass;
		this->energy = energy;
	}
	
	virtual ~Particle() {}
	
	#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(Particle,1);
	#endif // CRISP_SKIP_ROOTDICT

};									

class Mcef: public TObject{

private:
	Double_t An, Bn;
	Double_t Af, Bf;
	Double_t Ap, Bp, Vp;
	Double_t Aa, Ba, Va;	
	Int_t    n_fission;
	Int_t    n_proton, n_neutron, n_alpha;
	Double_t _fissility;
	Double_t _final_E;
	int i;

	std::vector<Double_t> Neutron_E;
	std::vector<Double_t> Proton_E;
	std::vector<Double_t> Alpha_E;
	
	TH1D *proWeissE;	
	TH1D *neuWeissE;
	TH1D *alpWeissE;
	
	TGraph *gammaP;	
	TGraph *gammaA;
	TGraph *gammaF;
	
	TH1D *fissParam;
	
	McefModel *model;

protected:									

	Double_t Gamma_p(Int_t A, Int_t Z, Double_t E);						
	Double_t Gamma_a(Int_t A, Int_t Z, Double_t E);
	Double_t Gamma_f(Int_t A, Int_t Z, Double_t E);
	void InitMcefParams(Int_t A, Int_t Z, Double_t E);

public: 									


	Mcef ();
	Mcef (TString modelLD, bool WeissWidth = false, bool WeissDist = false, TString modelBarrier = "nix");
	Mcef ( const char* model_name );
	//Mcef ( McefModel *modelo );	
	virtual ~Mcef();								
	Double_t Generate(Int_t A, Int_t Z, Double_t En, bool WeissWidth = false, bool WeissDist = false);
	inline Int_t NumberFissions() { return n_fission; }								
	inline Int_t ProtonMultiplicity() { return n_proton; }								
	inline Int_t NeutronMultiplicity() { return n_neutron; }										
	inline Int_t AlphaMultiplicity() { return n_alpha; } 
	inline Double_t Fissility() { return _fissility; }  
	inline Double_t final_E() { return _final_E; }										
	void FinalConfiguration ( const Int_t A, const Int_t Z, Int_t &A_final, Int_t &Z_final);
	inline TH1D ProtonKDist(){ return *proWeissE; }
	inline TH1D NeutronKDist(){ return *neuWeissE; }
	inline TH1D AlphaKDist(){ return *alpWeissE; }
	inline TGraph GammaPDist(){ return *gammaP; }
	inline TGraph GammaADist(){ return *gammaA; }
	inline TGraph GammaFDist(){ return *gammaF; }
	inline TH1D fissParamDist(){ return *fissParam; }
	inline std::vector<Double_t> Nvect() {return Neutron_E;}
	inline std::vector<Double_t> Pvect() {return Proton_E;}
	inline std::vector<Double_t> Avect() {return Alpha_E;}
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(Mcef,1);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
