/* ================================================================================
 * 
 * 	Copyright 2002, 2009, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __McefModel_pHH
#define __McefModel_pHH

#include "TNamed.h"
#include "TGraph2D.h"
#include "base_defs.hh"
#include "CrispParticleTable.hh"
#include "BindingEnergyTable.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <map>


enum ETypeMass
{
	EChung,
	EPearsonOriginal,
	EPearsonAdj,
	ETable
};										

class McefModel: public TNamed{
	
private:
	TString modelBar;

protected:										

	double GetF(double xo);
	double FissionBarrierCorrection(int A);
	double FissionBarrierCorrection2(int A);
	double FissionBarrierCorrection3(int A, int Z);
	double FissionBarrierCorrection4(int A, int Z);

public:

	McefModel():TNamed() {}
	McefModel(const char* name, const char* title):TNamed(name, title) {}
	void SetMass(ETypeMass tipo);
	ETypeMass GetMass();									
	double CalculateMass(int A, int Z);
	double CalculateMass_Chung(int A, int Z);
	double CalculateMass_Pearson_Adj(int A, int Z);
	double CalculateMass_Pearson_Original(int A, int Z);
	double CalculateMass_Table(int A, int Z);
	double CalculateBf(int A, int Z, double E);
	double CalculateBf_NIX(int A, int Z, double E);
	double CalculateBf_ETFSI(int A, int Z, double E);
	double CalculateRf(double y);
	double CalculateAf(int A, int Z);
	virtual double CalculateAn(int A, int Z);  
	virtual double CalculateAp(int A, int Z);
	virtual double CalculateAa(int A, int Z);
	double CalculateBn(int A, int Z);
	double CalculateBp(int A, int Z);
	double CalculateBa(int A, int Z);
	double CalculateVp(int A, int Z, double E);
	double CalculateVa(int A, int Z, double E);
	void Set_prf(unsigned int n, double par);
	void Set_pp1(double par);
	void Set_pp2(double par);
	void Set_pa1(double par);
	void Set_pa2(double par);
	void Set_pn1(double par);
	void Set_pn2(double par);
	double Get_prf(unsigned int n);
	double Get_pp1();
	double Get_pp2();
	double Get_pa1();
	double Get_pa2();
	double Get_pn1();
	double Get_pn2();
	void SetBarrierModel(TString model);
  
#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(McefModel,0);
#endif // CRISP_SKIP_ROOTDICT

};

#endif
