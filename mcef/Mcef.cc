/* ================================================================================
 * 
 * 	Copyright 2002, 2009, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "Mcef.hh"
#include <fstream>
#include <exception>

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(Particle);
#endif // CRISP_SKIP_ROOTDICT

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(Mcef);
#endif // CRISP_SKIP_ROOTDICT

const double w_step = .01;

Mcef::Mcef() : TObject(){
	model = new McefModel();
	_final_E = 0.0;
	An = Bn = 0.;
	Af = Bf = 0.;
	Ap = Bp = Vp = 0.;
	Aa = Ba = Va = 0.;
	fissParam = new TH1D("fissParam","Fissility Parameter Distribution",120,10,40);
}

Mcef::Mcef (TString modelLD, bool WeissWidth, bool WeissDist, TString modelBarrier) : TObject(){
	
	if(modelLD.CompareTo("dostrovsky") == 0){
		model = new McefModel();
	}
	else if(modelLD.CompareTo("BSFG") == 0){
		model = new BsfgModel();
		ETypeMass tipoMass(EPearsonOriginal);
		model->SetMass(tipoMass);
	}
	else{
		model = new McefModel();
	}
	model->SetBarrierModel(modelBarrier);
	_final_E = 0.0;
	An = Bn = 0.;
	Af = Bf = 0.;
	Ap = Bp = Vp = 0.;
	Aa = Ba = Va = 0.;
	if(WeissWidth){
		gammaP = new TGraph();
		gammaP->SetMarkerStyle(20);
		gammaP->SetTitle("#Gamma_{p}/#Gamma_{n}");
		gammaA = new TGraph();
		gammaA->SetMarkerStyle(20);
		gammaA->SetTitle("#Gamma_{a}/#Gamma_{n}");
		gammaF = new TGraph();
		gammaF->SetMarkerStyle(20);
		gammaF->SetTitle("#Gamma_{f}/#Gamma_{n}");

		i=0;
	}
	if(WeissDist){
		proWeissE = new TH1D("protWE","Kinetic Energy Distribution - proton",40,0,40);
		neuWeissE = new TH1D("neutWE","Kinetic Energy Distribution - neutron",40,0,40);
		alpWeissE = new TH1D("alphWE","Kinetic Energy Distribution - alpha",40,0,40);
	}
	
	fissParam = new TH1D("fissParam","Fissility Parameter Distribution",120,10,40);
}

Mcef::Mcef ( const char* model_name ) : TObject(){
	model = new McefModel(model_name, model_name);
	_final_E = 0.0;
	An = Bn = 0.;
	Af = Bf = 0.;
	Ap = Bp = Vp = 0.;
	Aa = Ba = Va = 0.;
}

// Mcef::Mcef ( McefModel *modelo ) : TObject(){
// 	model = modelo;
// 	_final_E = 0.0;
// 	An = Bn = 0.;
// 	Af = Bf = 0.;
// 	Ap = Bp = Vp = 0.;
// 	Aa = Ba = Va = 0.;
// }

Mcef::~Mcef(){
	if(model) delete model; model = 0;
}

inline double weisskopf_probability(double eps, double T, double v){

	double invT2 = 1.0/( T * T );
	if ( eps >= v ) 
		return ( invT2 * (eps - v) * exp( -(eps - v) / T ) * w_step);
	else 
		return 0.;
}

inline double weisskopfEnergy(double E, double A, double V = 0.){

	double eps = 0.;  
	double T   = sqrt(E / A);  
	
	if( (float) (( E - T )/ A)  > .05 ) {
    
		T = sqrt( ( E - T ) / A );    
		double p_sum = 0.; 
		double modulo = 1.;
		double x = gRandom->Uniform();
    
		while ( ( p_sum < x ) && ( modulo > 0.01) ) {      

			p_sum += weisskopf_probability(eps, T, V);
			modulo = fabs (p_sum - x);
			eps = eps + w_step;
		}    
	}
	else 
		eps = ( V >= 0. ) ? V: 2.;
  
	return eps;
}


double Mcef::Gamma_p(int A, int Z, double E){  
	double Ep = E - this->Bp - this->Vp;  
	double En = E - this->Bn;

	if ( (Ep > 0.) && (En > 0.) ) {

		double Kp = Ep / En;    
		double x = 2. * (sqrt(this->Ap) * sqrt(Ep) - sqrt(this->An) * sqrt(En));
		return Kp * exp(x);
	}
	return 0.;
}

double Mcef::Gamma_a(int A, int Z, double E){
	double Ea = E - this->Ba - this->Va;
	double En = E - this->Bn;  
	if ( Ea > 0. && En > 0. ) { 
		double Ka = 2.0 * Ea / En;
		double x = 2. * (sqrt(this->Aa) * sqrt(Ea) - sqrt(this->An) * sqrt(En));
		return Ka * exp(x);
	}
	return 0.;
}

double Mcef::Gamma_f(int A, int Z, double E){
	double Ef = E - this->Bf;
	double En = E - this->Bn;
  
	if (Ef > 0. && En > 0. ) {    
		if( ( this->Af * Ef ) <= 0 ){
			std::cout << "\n" << "Af = " << Af << ", Ef = " << Ef << ", An = " << An << "\n";
			throw std::exception();
		}
		double Kf = 14.39  * this->An * (2. * sqrt( this->Af * Ef) - 1.) / (4.  * this->Af * pow( (double)A, 2./3.) * En );      
		double x  =  2. * (sqrt(this->Af) * sqrt(Ef) - sqrt(this->An) * sqrt(En));
		return Kf * exp(x);
	}
	return 0.;  
}

void Mcef::InitMcefParams(int A, int Z, double En){

	this->An = model->CalculateAn(A, Z);
	this->Bn = model->CalculateBn(A, Z);
	this->Af = model->CalculateAf(A, Z);
	this->Bf = model->CalculateBf(A, Z, En);
	this->Ap = model->CalculateAp(A, Z);
	this->Bp = model->CalculateBp(A, Z);
	this->Vp = model->CalculateVp(A, Z, En);
	this->Aa = model->CalculateAa(A, Z);
	this->Ba = model->CalculateBa(A, Z);
	this->Va = model->CalculateVa(A, Z, En);
}

double Mcef::Generate(int A, int Z, double En, bool WeissWidth, bool WeissDist){

	// Debug-------------------------------------------------------------------------
	long debug1(0);	// variable to control the maximum number of iterations of Generate
	// End Debug --------------------------------------------------------------------

	_final_E = En;
	n_fission = 0;
	n_proton = n_neutron = n_alpha = 0;
	int a = A, z = Z; 
	double NF = 1, W = 0.;
	
	while ( _final_E > 0 && a > 0 && z > 0){
	  
		InitMcefParams(a, z, _final_E);
		
		if(_final_E < this->Bn) break;
		
		double eps = 2.;
		double Gf(0.0), Gp(0.0), Ga(0.0), Gn(1.0);
		try {
			Gf = this->Gamma_f(a, z, _final_E);
			Gp = this->Gamma_p(a, z, _final_E);
			Ga = this->Gamma_a(a, z, _final_E);
		} catch(std::exception &e)
		{ std::cerr << "Error: (this->Af * Ef ) <= 0\n"; throw e; }

		if(WeissWidth){
		  gammaP->SetPoint(i,a,Gp);
		  gammaA->SetPoint(i,a,Ga);
		  gammaF->SetPoint(i,a,Gf);
		  i++;
		}
		double fisP = (z*double(z))/a;
		fissParam->Fill(fisP);
		
		if(Ba != Ba){
			std::cout << "error Ba" << std::endl;
			Ga = 0.0;
		}
		if(Bp != Bp){
			std::cout << "error Bp" << std::endl;
			Gp = 0.0;
		}
		if(Bn != Bn){
			std::cout << "error Gn" << std::endl;
			Gn = 0.0;
		}
		if(Bf != Bf){
			std::cout << "error Bf" << std::endl;
			Gf = 0.0;
		}	
		if(Gn != Gn){
			std::cout << "error Gn" << std::endl;
			Gn = 0.0;
		}
		if(Ga != Ga){
			std::cout << "error Ga" << std::endl;
			Ga = 0.0;
		}
		if(Gp != Gp){
			std::cout << "error Gp" << std::endl;
			Gp = 0.0;
		}
		if(Gf != Gf){
			std::cout << "error Gf" << std::endl;
			Gf = 0.0;
		}
		
		// If nucleus doens't have enough protons and neutrons to emit alpha
		if( (a-z)<2 || z<2 ){
			Ga = 0.0;
			std::cout << "not sufficient proton and neutron - alpha emission impossible" << std::endl;
		}
		// If nucleus doens't have enough neutron to emit
		if( (a-z)<1 ){
			std::cout << "not sufficient neutron - emission impossible" << std::endl;
			Gn = 0.0;
		}
		// If nucleus doens't have enough proton to emit
		if( z<1 ){
			std::cout << "not sufficient proton - emission impossible" << std::endl;
			Gp = 0.0;
		}

		double G_sum = Gn + Gf + Gp + Ga;
		// if there is no chance of anything to happen, Generate must end
		if(G_sum == 0.0)
			break;
		
		// Fission probability
		double p_fissao = Gf / ( G_sum );
		W += NF * p_fissao;
		NF = NF * (1. - p_fissao);      
		
		double x = gRandom->Uniform(1);
		
		// Debug-------------------------------------------------------------------------
		debug1++;
		if(debug1 > 100000)
		{
			std::cerr << "MCEF got stuck \"double Mcef::Generate(int A, int Z, double En)\".\nIt probably entered in infinit loop inside while";
			std::cerr << "Generate( " << A << ", " << Z <<", " << En << ")\n";				
			std::cerr << "double eps = " << eps << "\n";
			std::cerr << "double Gf = " << Gf << "\n";
			std::cerr << "double Gp = " << Gp << "\n";
			std::cerr << "double Ga = " << Ga << "\n";
			std::cerr << "double Gn = " << Gn << "\n";
			std::cerr << "double G_sum = " << G_sum << "\n";
			std::cerr << "double p_fissao = " << p_fissao << "\n";
			std::cerr << "double W = " << W << "\n";
			std::cerr << "double NF = " << NF << "\n";
			std::cerr << "double x = " << x << "\n";
			std::cerr << "Forcing exit...";
			//throw std::exception();
			exit(1);
		}
		// End Debug --------------------------------------------------------------------

		if ( x <= Gp / G_sum ) {       
			/*
			* One proton go out.
			*/
			/*
			 * NOTE: If Weisskopf energy distribution is not used
			 * the charged particle leaves with kinetic energy equal to the coulomb barrier
			 * (no tunneling is considered in the statistical evaporation model since
			 * this is a collective process concentrating energy in a specific particle)
			 */
			if(this->Vp > 0) eps = this->Vp;

			if(WeissDist){
				eps = weisskopfEnergy( _final_E, this->Ap, this->Vp);
			}
			if( (this->Bp + eps) > _final_E ){	//just enough energy to release the particle
				eps = _final_E - this->Bp;
				_final_E = 0.0;
			} else {
				_final_E -= (this->Bp + eps);
			}
			if(WeissDist){
				proWeissE->Fill(eps);
				Proton_E.push_back(eps);	
			}
			a--; z--;  
			n_proton++;
		} 
		else {

			if ( x <= (Gp + Ga) / G_sum ) {       
				/*
				* One alpha ... 
				*/
				/*
				* NOTE: If Weisskopf energy distribution is not used
				* the charged particle leaves with kinetic energy equal to the coulomb barrier
				* (no tunneling is considered in the statistical evaporation model since
				* this is a collective process concentrating energy in a specific particle)
				*/
				if(this->Va > 0) eps = this->Va;

				if(WeissDist){
					eps = weisskopfEnergy(_final_E, this->Aa, this->Va);
				}
				if( (fabs(this->Ba) + eps) > _final_E ){ //just enough energy to release the particle
					eps = _final_E - fabs(this->Ba);
					_final_E = 0.0;
				} else {
					_final_E -= (fabs(this->Ba) + eps);
				} 
				if(WeissDist){
					alpWeissE->Fill(eps);
					Alpha_E.push_back(eps);
				}
				a -= 4; z -= 2;    
				n_alpha++;
			} 
			else {

				if ( x <= ( Gp + Ga + Gf) / G_sum ) {       
					/*	   
					* fission !
					*/
					n_fission++;      
					_fissility = W;
					return _fissility;
				}
				else {

					if ( x <= ( Gp + Ga + Gf + Gn) / G_sum ) {
						/*
						* ... and a neutron go out!
						*/    
						if(WeissDist){
							eps = weisskopfEnergy(_final_E, this->An);
						} 
						if( (this->Bn + eps) > _final_E ){ //just enough energy to release the particle
							eps = _final_E - this->Bn;
							_final_E = 0.0;
						} else {
							_final_E -= (this->Bn + eps);
						}
						if(WeissDist){
							neuWeissE->Fill(eps);
							Neutron_E.push_back(eps); 
						}
						a--;
						n_neutron++; 
					}
				}
			}      
		}
	} // the 'while' closing brace ...
	_fissility = W;
	return _fissility;
}

void Mcef::FinalConfiguration( const Int_t A, const Int_t Z, Int_t &A_final, Int_t &Z_final){
	
	Z_final = Z - ProtonMultiplicity() - AlphaMultiplicity() * 2;
	A_final = A - ProtonMultiplicity() - NeutronMultiplicity() - AlphaMultiplicity() * 4;
}									

