/* ================================================================================
 * 
 * 	Copyright 2002, 2009, 2015
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#include <fstream>
#include "McefModel.hh"

//These options for mass calculation are recommended this way
//Any change here requires a whole new set of density levels parameters and fission ratio
ETypeMass tipoMass(/*EPearsonAdj */ EPearsonOriginal /*EChung*/ /*ETable*/);

BindingEnergyTable BTable;


/*
 * NOTE: Original parameters
 */
double prf[] = { 1.30,		// y > 37.50
      /* prf1*/  1.26,		// y > 37.00 && y <= 37.50
      /* prf2*/  1.22, 		// y > 36.50 && y <= 37.00
      /* prf3*/  1.20, 		// y > 36.00 && y <= 36.50
      /* prf4*/  1.15, 		// y > 35.50 && y <= 36.00
      /* prf5*/  1.10,		// y > 35.00 && y <= 35.50
      /* prf6*/  1.00,		// y > 34.50 && y <= 35.00
      /* prf7*/  1.00,		// y > 34.00 && y <= 34.50
      /* prf8*/  1.00,		// y > 33.50 && y <= 34.00
      /* prf9*/  1.20,		// y > 33.25 && y <= 33.50
      /* prf10*/  1.15,		// y > 33.00 && y <= 33.25
      /* prf11*/  1.15,		// y > 32.50 && y <= 33.00
      /* prf12*/  1.05,		// y > 32.00 && y <= 32.50
      /* prf13*/  1.00,		// y > 31.50 && y <= 32.00
      /* prf14*/  1.00,		// y > 31.00 && y <= 31.50
      /* prf15*/  1.00,		// y > 30.50 && y <= 31.00
      /* prf16*/  1.00,		// y > 30.00 && y <= 30.50
      /* prf17*/  1.00,		// y > 29.50 && y <= 30.00
      /* prf18*/  1.00,		// y > 29.00 && y <= 29.50
      /* prf19*/  1.00,		// y > 28.50 && y <= 29.00
      /* prf20*/  1.00};	// y <= 28.50

double pp1 = 20.07,
       pp2 = 3.84,
       pa1 = 18.68,
       pa2 = 2.02,
       pn1 = 18.81,
       pn2 = 1.30;
	

       
/*
 * NOTE: These Dostrovsky parameters will be adjusted later to fit in with the models BSFG and the original Pearson mass fomula,
 * considering that the BSFG model doesn't account for all possible nuclei an requires Pearson's original mass formula so 
 * that the Dostrovsky model is needed anyway
 */
#if 0
double prf[] = { 1.29875 ,			// y > 37.50
      /* prf1*/  1.25807556963 ,		// y > 37.00 && y <= 37.50
      /* prf2*/  1.21625 , 			// y > 36.50 && y <= 37.00
      /* prf3*/  1.093768988208, 		// y > 36.00 && y <= 36.50
      /* prf4*/  1.05125, 			// y > 35.50 && y <= 36.00
      /* prf5*/  1.043844389675,		// y > 35.00 && y <= 35.50
      /* prf6*/  1.006061553298,		// y > 34.50 && y <= 35.00
      /* prf7*/  1.004451681981,		// y > 34.00 && y <= 34.50
      /* prf8*/  1.00465678667,		// y > 33.50 && y <= 34.00
      /* prf9*/  1.06438793896,		// y > 33.00 && y <= 33.50
      /* prf10*/  1.087298739708,		// y > 32.50 && y <= 33.00
      /* prf11*/  1.0775,			// y > 32.00 && y <= 32.50
      /* prf12*/  1.06875,			// y > 31.50 && y <= 32.00
      /* prf13*/  1.014091758915, 		// y > 31.00 && y <= 31.50
      /* prf14*/  1.0975,			// y > 30.50 && y <= 31.00
      /* prf15*/  1.035,			// y > 30.00 && y <= 30.50
      /* prf16*/  1.071659386563,		// y > 29.50 && y <= 30.00
      /* prf17*/  1.084135262171,		// y > 29.00 && y <= 29.50
      /* prf18*/  1.084734928521,		// y > 28.50 && y <= 29.00
      /* prf19*/  1.065758659645};		// y <= 28.50

double pp1 = 18.670295218,
       pp2 = 3.83501,
       pa1 = 18.67906239024,
       pa2 = 25.2201,
       pn1 = 18.81302,
       pn2 = 1.30001;
#endif
       
void McefModel::Set_prf(unsigned int n, double par){ prf[n] = par;}
double McefModel::Get_prf(unsigned int n){ return prf[n];}

void McefModel::Set_pp1 (double par){ pp1 = par;}
void McefModel::Set_pp2 (double par){ pp2 = par;}
void McefModel::Set_pa1 (double par){ pa1 = par;}
void McefModel::Set_pa2 (double par){ pa2 = par;}
void McefModel::Set_pn1 (double par){ pn1 = par;}
void McefModel::Set_pn2 (double par){ pn2 = par;}
double McefModel::Get_pp1() { return ::pp1;}
double McefModel::Get_pp2() { return ::pp2;}
double McefModel::Get_pa1() { return ::pa1;}
double McefModel::Get_pa2() { return ::pa2;}
double McefModel::Get_pn1() { return ::pn1;}
double McefModel::Get_pn2() { return ::pn2;}		

void McefModel::SetBarrierModel(TString model){ modelBar = model; }

#if !defined(CRISP_SKIP_ROOTDICT)
ClassImp(McefModel);
#endif // CRISP_SKIP_ROOTDICT										

typedef CrispParticleTable CPT;

void McefModel::SetMass(ETypeMass tipo){
	::tipoMass = tipo;
}

ETypeMass McefModel::GetMass(){
	return ::tipoMass;
}									

double McefModel::CalculateMass(int A, int Z){
	
  	if( ::tipoMass == ETable ){
		return CalculateMass_Table(A, Z);
	}
	else if( ::tipoMass == EPearsonAdj ){
		return CalculateMass_Pearson_Adj(A, Z);
	}
	else if( ::tipoMass == EPearsonOriginal ){
		return CalculateMass_Pearson_Original(A, Z);
	}
	else if( ::tipoMass == EChung ){
		return CalculateMass_Chung(A, Z);
	}
	else{
		return CalculateMass_Pearson_Adj(A, Z);
	}
	return 0.;
}

double McefModel::CalculateMass_Table(int A, int Z){
	
	double mN = 939.566, mH = 938.272;
	double Mass = Z*mH + (A-Z)*mN - BTable.Get(Z, A);
	return Mass;
}

double McefModel::CalculateMass_Chung(int A, int Z){

	const double pn =   1.008665;
	const double pz =   1.007825;
	const double a1 =  16.710;
	const double a2 =  18.500;
	const double a3 = 100.00;
	const double a4 =   0.750;
	const double f  = 931.478;
	double delta = 0.;
	int	z2 = 2 * (int)(ceil( (double)Z / 2.)),
		n2 = 2 * (int)(ceil( (double)(Z - A) / 2.));    

	if( z2 == Z ) {    
		if( n2 == (Z - A) ) {
			delta= -36. * pow( (double)A, -3./4. );
		}
	}
	else {     
		if ( n2  != Z - A ) 
			delta = 36. * pow(A, -3.0/4.0);
	}  

	double m = 0.; 
	m = pn * (A - Z)*f;
	m = m + pz * Z * f;
	m = m + 0.511 * Z;
	m = m - a1 * A * f/1000.;
	m = m + a2 * pow( (double)A, 2.0/3.0) * f/1000.;
	m = m + a3 * (f/1000.) * pow( (double)A/2. - (double)Z, 2) / (double)A;
	m = m + a4 * pow( (double)Z, 2.) / pow( (double)A, 1./3.);
	m = m + delta * f/1000.;
	
	return m;
}

double McefModel::CalculateMass_Pearson_Adj(int A, int Z)
{
/*
MIGRAD MINIMIZATION HAS CONVERGED.
 FCN=2.36527e+10 FROM MIGRAD    STATUS=CONVERGED     121 CALLS         122 TOTAL
                     EDM=1.46138e-06    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  Av          -1.50175e+01   1.27821e-05   0.00000e+00  -4.52885e+02
   2  Asf          1.55981e+01   3.22665e-05  -0.00000e+00  -1.15925e+02
   3  Asym        -7.09740e+00   6.77166e-04  -0.00000e+00   1.06468e+00
   4  Ass          1.44764e+02   2.18625e-03   0.00000e+00   1.43433e-01
   5  c1_corr      0.00000e+00     fixed    
   6  c2_corr      0.00000e+00     fixed    
 EXTERNAL ERROR MATRIX.    NDIM=  25    NPAR=  4    ERR DEF=1
  1.634e-10 -4.121e-10 -6.060e-09  1.394e-08 
 -4.121e-10  1.041e-09  1.536e-08 -3.560e-08 
 -6.060e-09  1.536e-08  4.586e-07 -1.394e-06 
  1.394e-08 -3.560e-08 -1.394e-06  4.780e-06 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4
        1  0.99930   1.000 -0.999 -0.700  0.499
        2  0.99929  -0.999  1.000  0.703 -0.505
        3  0.97890  -0.700  0.703  1.000 -0.941
        4  0.96931   0.499 -0.505 -0.941  1.000
*/
	//TMinuit adjusted parameters (above)
	double Av = -1.50175e+01, Asf = 1.55981e+01, Asym = -7.09740e+00, Ass = 1.44764e+02;
	double mN = 939.566, mH = 938.272;
	int N = A - Z;
	double r0 = 1.233;	//fm
	double e2 = 1.44;	//carga elementar em unidades gaussianas
	double  Ac = (3. * e2)/(5. * r0);
		
	double J = double(N-Z)/(double)A;
	double Mpearson =  A* ( Av
				+ Asf * (pow((double)A,-1./3.))
				+ Ac * (Z * Z)*(pow((double)A, -4./3.))
				+ ( Asym + Ass * pow((double)A,-1./3.) )* J*J );
	double Mbase = Z * mH + N * mN;
	double r = Mpearson + Mbase;
	
	return r;
}										


double McefModel::CalculateMass_Pearson_Original(int A, int Z)
{
//	Pearson 2001
	double Av = -15.65, Asf = 17.63, Asym = 27.72, Ass = -25.60;
	double mN = 939.566, mH = 938.272;
	int N = A - Z;
	double r0 = 1.233;
	double e2 = 1.44;	//carga elementar em unidades gaussianas
	double  Ac = (3. * e2)/(5. * r0);
		
	double J = double(N-Z)/(double)A;
	double Mpearson =  A* ( Av
				+ Asf * (pow((double)A,-1./3.))
				+ Ac * (Z * Z)*(pow((double)A, -4./3.))
				+ ( Asym + Ass * pow((double)A,-1./3.) )* J*J );
	double Mbase = Z * mH + N * mN;
	double r = Mpearson + Mbase;
	
	return r;
}	

double McefModel::GetF(double xo){

	const double fission_parm[48] = {0.00000, 0.00001,  0.00005,  0.00016,
					0.00037, 0.00073,  0.00126,  0.00201,
					0.00303, 0.00436,  0.00606,  0.00819,
					0.01084, 0.01409,  0.01808,  0.02293, 
					0.02876, 0.03542,  0.04258,  0.04997, 
					0.05747, 0.06503,  0.07260,  0.08017, 
					0.08773, 0.09526,  0.10276,  0.11023, 
					0.11765, 0.12502,  0.13235,  0.13962, 
					0.14684, 0.15400,  0.16110,  0.16814, 
					0.17510, 0.18199,  0.18880,  0.19553,
					0.20217, 0.20871,  0.21514,  0.22144, 
					0.22761, 0.23363,  0.23945,  0.24505 };
	double X = 1.;
	int i = 0; 
	const double d = .02;   

	for ( i = 0; i < 48; i++ ) {
		if ( xo > X ) 
			break;     
		X -= d;
	}  
  
	double X_1 = X - d, fo = 0.;
			
	if ( i < 47 ) 
		fo = ( (xo - X_1) * fission_parm[i] + (X - xo) * fission_parm[i+1]) / d;
	else 
		fo = fission_parm[47];  
	
	return fo;
}

double McefModel::FissionBarrierCorrection(int A){

	double x = A;

	const double 	a1 = 18.03, 
			a2 = 101.8, 
			a3 = 934.6, 
			a4 = 822.3, 
			a5 = 30.34, 
			a6 = 341.4, 
			a7 = 736.5,
			//
			b1 = 10.92, 
			b2 = 19.77, 
			b3 = 47.3, 
			b4 = 94.15, 
			b5 = 122.8,
			b6 = 137.2, 
			b7 = 167.5, 
			//
			c1 = 7.319, 
			c2 = 17.01, 
			c3 = 40.18, 
			c4 = 37.25, 
			c5 = 22.5,
			c6 = 35.46, 
			c7 = 67.54,
			//
			p1 = -0.0003578, 
			p2 = 0.2026, 
			p3 = -28.03, 
			p4 = -230.9; 
  
	return 	p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
			a1 * exp(-pow(((x - b1) / c1), 2)) +
			a2 * exp(-pow(((x - b2) / c2), 2)) +
			a3 * exp(-pow(((x - b3) / c3), 2)) +
			a4 * exp(-pow(((x - b4) / c4), 2)) +
			a5 * exp(-pow(((x - b5) / c5), 2)) +
			a6 * exp(-pow(((x - b6) / c6), 2)) +
			a7 * exp(-pow(((x - b7) / c7), 2));
}										

double McefModel::FissionBarrierCorrection2(int A){

	double x = A;
	
	const double 	a1 = -24.03, 
			a2 = 46.3, 
			a3 = 24.08, 
			a4 = -125.1,
			a5 = 47.58, 
			a6 = 138, 
			a7 = 23.96, 
			a8 = 11.29,
			//
			b1 = 7.927, 
			b2 = 18.22, 
			b3 = 53.73, 
			b4 = -169.2, 
			b5 = 84.8,
			b6 = -106.1,  
			b7 = -54.39, 
			b8 = 140.5,
			//
			c1 = 16.51, 
			c2 = 26.2, 
			c3 = 16.67, 
			c4 = 16.58, 
			c5 = 40.25,
			c6 = 16, 
			c7 = 70.22, 
			c8 = 22.64,
			//
			p1 = -6.022e-005, 
			p2 = 0.02085, 
			p3 = -1.794, 
			p4 = -11.39,
			a9 = -3.396e+014, 
			b9 = -2979, 
			c9 = 556.1, 
			//
			a10 = 10.25,
			b10 = 209.5, 
			c10 = 7.761; 
  
	if (x < 163) {
		return 	p1 * pow(x, 3) + p2 * pow(x, 2) + p3 * x + p4 +
				a1 * exp(-pow(((x - b1) / c1), 2)) +
				a2 * exp(-pow(((x - b2) / c2), 2)) +
				a3 * exp(-pow(((x - b3) / c3), 2)) +
				a4 * exp(-pow(((x - b4) / c4), 2)) +
				a5 * exp(-pow(((x - b5) / c5), 2)) +
				a6 * exp(-pow(((x - b6) / c6), 2)) +
				a7 * exp(-pow(((x - b7) / c7), 2)) +
				a8 * exp(-pow(((x - b8) / c8), 2));
	} 
	else {
		return 	 a9 * exp(-pow(((x - b9) / c9), 2)) +
				a10 * exp(-pow(((x - b10) / c10), 2));
	}
}

double McefModel::FissionBarrierCorrection3(int A, int Z){

//This correction was adjusted for ETFSI fission barrier in range (Z>= 75 && Z<=95) && (A>=190 && A<=240)
/*
 FCN=1.57054

  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   7  R            7.11579e+05   1.01189e+07   3.39285e+00  -3.22735e-11
   8  Ao           2.12321e+02   4.49826e+00   2.74524e-03  -2.24429e-05
   9  Zo           4.99742e+01   2.36598e+01   1.42637e-03  -1.07644e-05
  10  sigA         2.90639e+00   2.66601e+00   1.56872e-03   4.19303e-05
  11  sigZ        -6.92446e+00   3.61442e+00   2.94752e-04   5.87951e-05
*/

   const double R = 7.11579e+05,
   		Ao = 2.12321e+02,
   		Zo = 4.99742e+01,
  		sigA = 2.90639e+00,
  		sigZ = -6.92446e+00;

    return R * exp( - ( ((A-Ao)*(A-Ao))/(2.*sigA*sigA) + ((Z-Zo)*(Z-Zo))/(2.*sigZ*sigZ) ) );
}

double McefModel::FissionBarrierCorrection4(int A, int Z){

//	Both corrections below were adjusted for Nix fission barrier 

/*
 Fit number 1

 EIGENVALUES OF SECOND-DERIVATIVE MATRIX:
         6.7643e-01  1.0000e+00  1.0000e+00  1.0000e+00  1.3236e+00
 COVARIANCE MATRIX CALCULATED SUCCESSFULLY
 FCN=1875.69 FROM MIGRAD    STATUS=CONVERGED    1174 CALLS        1175 TOTAL
                     EDM=5.80082e-07    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  R           -3.50117e+09   1.41421e+00   1.66949e+03  -1.31413e-11
   2  Ao          -4.97603e+02   1.40096e+00   8.04732e-02  -1.21334e-04
   3  Zo           6.78389e+01   1.41421e+00   3.72084e+01   6.70563e-04
   4  sigA         1.15426e+02   5.92487e-01   1.29159e-02  -8.90229e-04
   5  sigZ         4.63522e+02   1.41421e+00   4.00408e+01   7.65148e-05
 EXTERNAL ERROR MATRIX.    NDIM=  25    NPAR=  5    ERR DEF=1
  2.000e+00  2.165e-10 -4.148e-14  1.411e-09  9.464e-15 
  2.165e-10  1.963e+00  1.360e-07 -2.686e-01 -2.572e-06 
 -4.148e-14  1.360e-07  2.000e+00 -5.660e-05  5.096e-06 
  1.411e-09 -2.686e-01 -5.660e-05  3.510e-01 -2.282e-05 
  9.464e-15 -2.572e-06  5.096e-06 -2.282e-05  2.000e+00 
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5
        1  0.00000   1.000  0.000 -0.000  0.000  0.000
        2  0.32357   0.000  1.000  0.000 -0.324 -0.000
        3  0.00007  -0.000  0.000  1.000 -0.000  0.000
        4  0.32357   0.000 -0.324 -0.000  1.000 -0.000
        5  0.00003   0.000 -0.000  0.000 -0.000  1.000
*/

   const double R = -3.50117e+09,
   		Ao = -4.97603e+02,
   		Zo = 6.78389e+01,
  		sigA = 1.15426e+02,
  		sigZ = 4.63522e+02;

/*
 Fit number 2 (Z>= 75 && Z<=95) && (A>=190 && A<=240)

 EIGENVALUES OF SECOND-DERIVATIVE MATRIX:
        -1.4456e-02 -6.1431e-03 -1.8054e-06  3.4826e-04  5.0203e+00
 MINUIT WARNING IN HESSE   
 ============== MATRIX FORCED POS-DEF BY ADDING 0.019476 TO DIAGONAL.
 MIGRAD TERMINATED WITHOUT CONVERGENCE.
 FCN=34.7148 FROM MIGRAD    STATUS=FAILED        658 CALLS         659 TOTAL
                     EDM=0.0886617    STRATEGY= 1      ERR MATRIX NOT POS-DEF
  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  R           -1.96109e+06   1.92257e+06   1.68911e+01  -2.78021e-06
   2  Ao          -2.93509e+07   1.77004e+06   1.65622e+01   1.70109e-06
   3  Zo           2.81287e+07   1.03515e+06   1.59911e+01  -2.85595e-06
   4  sigA         9.69921e+06   6.47880e+05   1.43916e+01   5.14774e-06
   5  sigZ         7.32800e+06   2.86919e+05   1.67143e+01   1.09626e-05
 EXTERNAL ERROR MATRIX.    NDIM=  25    NPAR=  5    ERR DEF=1
  3.696e+12  1.505e+12 -7.541e+11  6.009e+11  2.220e+11 
  1.505e+12  3.133e+12  2.669e+11  9.554e+10 -7.857e+10 
 -7.541e+11  2.669e+11  1.072e+12  1.065e+11 -4.082e+09 
  6.009e+11  9.554e+10  1.065e+11  4.197e+11 -3.136e+10 
  2.220e+11 -7.857e+10 -4.082e+09 -3.136e+10  8.232e+10 
ERR MATRIX NOT POS-DEF
 PARAMETER  CORRELATION COEFFICIENTS  
       NO.  GLOBAL      1      2      3      4      5
        1  0.99690   1.000  0.442 -0.379  0.482  0.403
        2  0.99033   0.442  1.000  0.146  0.083 -0.155
        3  0.98997  -0.379  0.146  1.000  0.159 -0.014
        4  0.99210   0.482  0.083  0.159  1.000 -0.169
        5  0.99113   0.403 -0.155 -0.014 -0.169  1.000
	 

   const double R = -1.96109e+06,
   		Ao = -2.93509e+07,
   		Zo = 2.81287e+07,
  		sigA = 9.69921e+06,
  		sigZ = 7.32800e+06;
*/
   return R * exp( - ( ((A-Ao)*(A-Ao))/(2.*sigA*sigA) + ((Z-Zo)*(Z-Zo))/(2.*sigZ*sigZ) ) );
}

double McefModel::CalculateAf(int A, int Z){

	double y = (double)Z*(double)Z/(double)A;
	double rf = CalculateRf(y);
	double an = CalculateAn(A, Z);  
	
	return rf * an;
}

double McefModel::CalculateBf_NIX(int A, int Z, double){
	
	double Bf = 0.;
	int N = A - Z;
	const double 	as = 17.9439,  //MeV
			k  = 1.7826;

	double Es = 1. - k *  pow( (double)(N - Z)/(double)A, 2);
	
	Es = as * Es * pow(A, (2./3.) );

	double 	p1 = 50.88,
		p2 = 1.7826,
		xo = ( (double)(Z * Z)/(double)A ) / ( p1 * (1. - p2 * pow( (double)(N-Z)/(double)A, 2) ) );

	if( xo <= 0.06 ) 
		xo = 0.06;

	double fo = GetF(xo);  
	
	Bf = fo * Es;
	Bf = Bf + FissionBarrierCorrection(A)
		  /* + FissionBarrierCorrection4(A, Z)*/ ;
	
//-----------------------------------------------------------	
/* idef inp
if (A>=180 || A<=150){

		Bf=0.22*(A-Z)-1.40*Z+110.5;
		//Bf=0.22*(A-Z)-1.40*Z+101.5;
		//Bf=0.22*(A-Z)-1.40*Z+102.5; //corre��o para o Am
		//Bf=Bf*(1-E/getB());
		Bf=Bf*(1.-E/(A*CPT::p_mass - CalculateMass(A, Z)));

		if (Bf < 0){
			Bf =0;
			A=0;
		}
}
//-----------------------------------------------------------	
*/	
	return Bf;
}

double McefModel::CalculateBf_ETFSI(int A, int Z, double E){

//	Model adjusted in the range (Z>= 75 && Z<=95) && (A>=190 && A<=240)
//	The correction has its own fit
/*
 FCN=1.57054

  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p1           1.15209e+03   1.02278e+01   8.64034e-04   1.70321e-05
   2  p2          -1.56042e+01   1.35076e-01   1.00648e-05  -6.72767e-05
   3  p3          -4.69923e+00   4.80197e-02   3.78224e-06   2.30278e-03
   4  p4           8.60585e+00   3.04347e-01   2.66253e-05  -3.62591e-04
   5  p5           4.21749e-02   1.36457e-03   1.16654e-07  -1.38203e-01
   6  p6           1.31525e-02   1.81809e-04   1.00000e-07   1.75828e-01
*/

	double Bf = 0.;

	const double p1 = 1.15209e+03,
		     p2 = -1.56042e+01, 
		     p3 = -4.69923e+00, 
		     p4 = 8.60585e+00,
		     p5 = 4.21749e-02,
		     p6 = 1.31525e-02;

	return Bf = p1 + p2*Z + p3*A + p4*pow(Z,2.0)/A + p5*pow(Z,2.0) + p6*pow(A,2.0);
				 /*  + FissionBarrierCorrection3(A, Z);*/
}

double McefModel::CalculateBf(int A, int Z, double E ){
	if(modelBar.CompareTo("nix") == 0){
		return CalculateBf_NIX(A, Z, E);
	}
	else if(modelBar.CompareTo("ETFSI") == 0){
		if( A >= 200 && Z>= 78 ){
			return CalculateBf_ETFSI(A, Z, E);
		}
		else {
			return CalculateBf_NIX(A, Z, E);
		}
	}
	else{
		return CalculateBf_NIX(A, Z, E);
	}
}

double McefModel::CalculateAn(int A, int Z){
	const double a_dostrovsk = (double)A/pn1;
	int 	N = A - Z;
	double 	theta = (double)(N - Z) / (double)A;
	return a_dostrovsk * sqr( 1. - ((pn2 * theta)/(double)A) );
}

double McefModel::CalculateBn(int A, int Z){
	//return -.16 * (A - Z) + .25 * Z + 5.6;
	return CPT::n_mass + CalculateMass(A - 1, Z) - CalculateMass(A, Z);
}

double McefModel::CalculateAp(int A, int Z){
	const double a_dostrovsk = (double)A/pp1;  
	int 	N = A - Z;
	double 	theta = (double)(N - Z) / (double)A;
	return a_dostrovsk * sqr( 1. + ((pp2 * theta)/(double)A) );
}

double McefModel::CalculateBp(int A, int Z) {
	return CPT::p_mass + CalculateMass(A - 1, Z - 1) - CalculateMass(A, Z);  
}

double McefModel::CalculateVp(int A, int Z, double E){

	const double r0 = 1.18;
	double	eletron2 = 1.44,
		Vp = (.70 * (Z - 1) * eletron2) / (r0 * pow((double)(A - 1),1./3.) + 1.14 ),
		F = Vp * ( 1 - E / ( A * CPT::p_mass - CalculateMass(A, Z) ) );
  
	if (F <= 0){
		A = 0;
		F = 0;
	}
  
	return F;
}

double McefModel::CalculateAa(int A, int Z){
	const double a_dostrovsk = (double)A/pa1;
	return a_dostrovsk * sqr( 1. - ( pa2/double(A) ) );	
}

double McefModel::CalculateBa(int A, int Z){
	const double mcef_ma = 3727.4;    // the alpha mass in MeV. 
	return mcef_ma + CalculateMass(A - 4, Z - 2) - CalculateMass(A, Z);
}

double McefModel::CalculateVa(int A, int Z, double E){

	const double 	r0 = 1.18,
			eletron2 = 1.44;

	double Va = ( 2. * .83 * (Z - 2) * eletron2 ) / ( r0 * pow( (double)(A - 4), 1./3.) + 2.16 );  
  
	return Va * ( 1 - E/ ( A * CPT::p_mass - CalculateMass(A, Z) ) );  
}

double McefModel::CalculateRf(double y){

	double rf = 1.;

	if(y > 37.50)
	rf = prf[0];

	if(y > 37.00 && y <= 37.50)
	rf = prf[1];

	if(y > 36.50 && y <= 37.00)
	rf = prf[2];

	if(y > 36.00 && y <= 36.50)
	rf = prf[3];

	if(y > 35.50 && y <= 36.00)
	rf = prf[4];

	if(y > 35.00 && y <= 35.50)
	rf = prf[5];

	if(y > 34.50 && y <= 35.00)
	rf = prf[6];

	if(y > 34.00 && y <= 34.50)
	rf = prf[7];

	if(y > 33.50 && y <= 34.00)
	rf = prf[8];

	if(y > 33.25 && y <= 33.50)
	rf = prf[9];  

	if(y > 33.00 && y <= 33.25)
	rf = prf[10]; 	
	
	if(y > 32.50 && y <= 33.00)
	rf = prf[11];

	if(y > 32.00 && y <= 32.50)
	rf = prf[12];

	if(y > 31.50 && y <= 32.00)
	rf = prf[13];

	if(y > 31.00 && y <= 31.50)
	rf = prf[14];

	if (y > 30.50 && y <= 31.00)
	rf = prf[15];

	if (y > 30.00 && y <= 30.50)
	rf = prf[16];  

	if(y > 29.50 && y <= 30.00)
	rf = prf[17];

	if(y > 29.00 && y <= 29.50)
	rf = prf[18];
	
	if(y > 28.50 && y <= 29.00)
	rf = prf[19];

	if(y <= 28.50)
	rf = prf[20];

	return rf;
}
