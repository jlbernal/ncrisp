/* ================================================================================
 * 
 * 	Copyright 2009
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

#ifndef __BsfgModel_pHH
#define __BsfgModel_pHH

#include "TNamed.h"
#include "TGraph2D.h"

#include "base_defs.hh"
#include "CrispParticleTable.hh"
#include "McefModel.hh"


#include <iostream>
#include <sstream>
#include <iomanip>
#include <map>


class Tabela_a {
public:
	std::map< std::pair< int, int >, double > _table;	

	Tabela_a( std::string _file )
	{
		ReadATable(_file);
	}

	void Push( int Z, int A, double _a )
	{
		_table[ std::pair<int,int>(Z, A) ] = _a;
	}
	
	bool Exist( int Z, int A )
	{
		std::map< std::pair< int, int >, double >::iterator it;
		it = _table.find( std::pair<int,int>(Z, A) );
		if( it == _table.end() )
			return false;
		else
			return true;
	}
	
	double Get( int Z, int A )
	{
		
		std::map< std::pair< int, int >, double >::iterator it;
		it = _table.find( std::pair<int,int>(Z, A) );
		if( it == _table.end() )
			return 0.0;
		else
			return it->second;
	}
	
	void Dump()
	{
		std::cout << "Z\tA\ta\n";
		std::map< std::pair< int, int >, double >::iterator it;
		for ( it = _table.begin() ; it != _table.end(); it++ )
			std::cout << (*it).first.first << "\t" <<  (*it).first.second << "\t" << (*it).second << std::endl;
	}
	
	void ReadATable( std::string _file )
	{
		TGraph2D data( _file.c_str(),"%lg %lg %lg");
		Int_t N = data.GetN();
		Double_t* X = data.GetX();
		Double_t* Y = data.GetY();
		Double_t* Z = data.GetZ();
		for(int i(0); i<N; i++)
		{
			Push( X[i], Y[i], Z[i] );
		}
	}
	
	//! ler sem usar TGraph
	void ReadATable2( std::string _file )
	{
		std::ifstream ifs( _file.c_str() );
		std::string buf;
		
		while(ifs.good())
		{
			int _Z(0), _A(0);
			double a(0);
			ifs >> _Z >> _A >> a;
			Push(_Z,_A,a);
		}
		ifs.close();
	}
};


class BsfgModel: public McefModel
{
protected:
	

public:
	BsfgModel():McefModel() {}
	BsfgModel(const char* name, const char* title):McefModel(name, title) {}

	virtual double CalculateMass(int A, int Z);
	virtual double Mexp(int A, int Z);
	virtual double MassCorrection( int A, int Z );
	virtual double CorrectionDerived( int A, int Z );
	virtual double CalculateSd(int A, int Z);
	virtual double CalculatePd(int A, int Z);
	virtual double a_modelo_BSFG(int A, int Z);
	virtual double CalculateAn(int A, int Z);
	virtual double CalculateAp(int A, int Z);
	virtual double CalculateAa(int A, int Z);
	

	#if !defined(CRISP_SKIP_ROOTDICT)
	ClassDef(BsfgModel,0);
	#endif // CRISP_SKIP_ROOTDICT

};

#endif // __BsfgModel_pHH

