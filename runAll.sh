#!/bin/bash

echo "Running mpi? yes or no."
read MPIOPT

if [ "$MPIOPT" = "yes" ] ; then
	echo "Enter the number of processors and input files for cascade and mcef."
	read N input1 input2
	mpirun -np $N ./mcmc-mpi.exec input/$input1
	./mcef.exec input/$input2
elif [ "$MPIOPT" = "no" ] ; then
	echo "Enter the input files for cascade and mcef."
	read input1 input2
	./mcmc.exec input/$input1
	./mcef.exec input/$input2
fi
