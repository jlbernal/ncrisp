/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TCANONICAL_HH
#define __TCANONICAL_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include "TFragment.hh" 
#include "definedvalues.hh"

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

/// Defini��o do vetor de ponteiros de TFragment usado para construir a parti��o, ou seja, 
/// cada ponteiro TFragment* armazena o endere�o de um fragmento na mem�ria. 
///
//_________________________________________________________________________________________________________

typedef vector<TFragment*> vPartition;

/// A classe TCanonical cont�m a defini��o do modelo e grandesas fisicas para a simula��o 
/// do processo de Multifragmenta��o 
///
//_________________________________________________________________________________________________________

class TCanonical {

public:

/// Construtor Pad�o da Classe.
///
//_________________________________________________________________________________________________________

	TCanonical():Omega_0(16.00) ,   // [MeV] 
				 Betha_0(18.00)  ,   // [MeV]
				 C_c(0.737)      ,   // [C/fm]
				 K_asym(30.00)   ,   // [MeV]
				 Q_asym(35.00)   ,   // [MeV]
				 T_c(16.00)      ,   // [MeV]
				 eps_0(16.00)    ,   // [MeV]
				 D_(2.3)         {}; // [fm]

/// Destrutor Pad�o da Classe.
///
//_________________________________________________________________________________________________________

	~TCanonical(){};

/// Defini��o de betha (C.f. eq. T-3.3).
///
///   @param  T	: Temperatura da Parti��o.                                   
///   @return	: betha(T).                                   
///
//_________________________________________________________________________________________________________

	double betha(double T);

/// Defini��o da primeira derivada de betha em rela��o a temperatura.
///
///   @param  T	: Temperatura da Parti��o.                                   
///   @return	: Primeira derivada de betha(T).                                   
///
//_________________________________________________________________________________________________________

	double dbethadT(double T);

/// Defini��o da segunda derivada de betha em rela��o a temperatura.
///
///   @param  T	: Temperatura da Parti��o.                                   
///   @return	: Primeira derivada de betha(T).                                   
///
//_________________________________________________________________________________________________________

	double d2bethadT2(double T);

/// Defini��o do volume do sistema n�o fragmentado.
///
///   @param  A	: N�mero de Baryons da Parti��o.                                   
///   @return	: Volume do sistema n�o fragmentado.                                   
///
//_________________________________________________________________________________________________________

	double V_0(int A);

/// Defini��o da fra��o incrementada ao volume pelo processo de fragmenta��o.
///
///   @param  A0	: N�mero de Baryons da Parti��o.                                   
///   @param  Mf	: Multiplicidade da Parti��o.                                   
///   @return		: Fra��o incrementada ao volume pelo processo de fragmenta��o.                                   
///
//_________________________________________________________________________________________________________

	double x_f(int A0, int Mf);

/// Defini��o do volume do sistema fragmentado.
///
///   @param  A0	: N�mero de Baryons da Parti��o.                                   
///   @param  Mf	: Multiplicidade da Parti��o.                                   
///   @return		: Volume do sistema fragmentado.                                   
///
//_________________________________________________________________________________________________________

	double V(int A0, int Mf);

/// Defini��o do volume acessivel para o movimento dos fragmentos.
///
///   @param  A0	: N�mero de Baryons da Parti��o.                                   
///   @param  Mf	: Multiplicidade da Parti��o.                                   
///   @return		: Volume acessivel para o movimento dos fragmentos.                                   
///
//_________________________________________________________________________________________________________

	double V_free(int A0, int Mf);

/// Defini��o da ras�o entre o volume do sistema fragmentado pelo volume do n�o fragmentado.
///
///   @param  A0	: N�mero de Baryons da Parti��o.                                   
///   @param  Mf	: Multiplicidade da Parti��o.                                   
///   @return		: Ras�o entre volume (V/V0).                                   
///
//_________________________________________________________________________________________________________

	double V_frac(const int A0, const int Mf);

/// Defini��o da Energia de liga��o (T=0).
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @return		: Energia de liga��o B(A,Z).                                   
//_________________________________________________________________________________________________________

	double B(int A, int Z);

/// Defini��o do Termo de Energia T�rmica ( cin�tica ) da Parti��o.
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @return		: Energia T�rmica da Parti��o Ek(T).                                   
//_________________________________________________________________________________________________________

	double E_k(double T);

/// Defini��o do Termo de Energia de Excita��o do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @return		: Energia de Excita��o do Fragmento Eex(A,Z,T).                                   
//_________________________________________________________________________________________________________

	double E_ex(int A,int Z, double T);

/// Defini��o do Termo de Energia de Coulomb do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  Vfrac	: Ras�o entre Volume do Sistema nao Fragmentado pelo Fragmentado (V0/V).                                   
///   @return		: Energia de Coulomb do Fragmento Ec(A,Z,Vfrac).                                   
//_________________________________________________________________________________________________________

	double E_C(int A,int Z, double Vfrac);

/// Defini��o do Termo de Energia do Fragmento .
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  V		: Volume do Sistema Fragmentado.                                   
///   @return		: Energia do Fragmento E(A,Z,T).                                   
//_________________________________________________________________________________________________________

	double Efrag(int A,int Z, double T, double Vfrac);

/// Defini��o da diferen�a entre a Energia inicial (sistema n�o fragmentado) e final (sistema fragmentado)
/// usado para obter a temperatura da Parti��o.
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  E_exc	: Energia de Excita��o do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Diferen�a da Energia do Sistema deltaE(T,Mf,A0,Z0,E_exc,vPart).                                   
//_________________________________________________________________________________________________________

	double deltaE(double T, int Mf, int A0, int Z0, double E_exc ,vPartition & vec);

/// Defini��o da Energia do Sistema .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  E_exc	: Energia de Excita��o do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Energia do Sistema E(T,Mf,A0,Z0,E_exc,vPart).                                   
//_________________________________________________________________________________________________________

	double Epart(double T, int Mf, int A0, int Z0, double E_exc ,vPartition & vec);

/// Defini��o do fator de degeneresc�ncia.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @return		: Fator de Degeneresc�ncia g(A,Z).                                   
//_________________________________________________________________________________________________________

	double g(const int A,const int Z);

/// Defini��o do Comprimento de Onda Termico do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @return		: Comprimento de Onda Termico lambdaT(A,Z).                                   
//_________________________________________________________________________________________________________

	double lambdaT(const int A, const double T);

/// Defini��o do Termo de Energia Livre de Coulomb do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  Vfrac	: Ras�o entre Volume do Sistema nao Fragmentado pelo Fragmentado (V0/V).                                   
///   @return		: Energia Livre de Coulomb do Fragmento Fc(A,Z,Vfrac).                                   
//_________________________________________________________________________________________________________


	double F_c(int A, int Z, double Vfrac);

/// Defini��o do Termo de Energia Livre de Excita��o do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @return		: Energia Livre de Excita��o do Fragmento Fex(A,Z,T).                                   
//_________________________________________________________________________________________________________

	double F_ex(int A,int Z, double T);

/// Defini��o do Termo de Energia Lvre T�rmica ( cin�tica ) da Parti��o.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Vfree	: Volume disponivel no Sistema para movimento do Fragmentado .                                   
///   @param  Naz	: Multiplicidade do Fragmento.                                   
///   @return		: Energia Livre T�rmica da Parti��o Ftrans(T).                                   
//_________________________________________________________________________________________________________

	double F_trans(int A, int Z, double T, double Vfree, int Naz);

/// Defini��o do Termo de Energia Livre do Fragmento .
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  M		: Multiplicidade do Fragmento.                                   
///   @param  Vfrac	: Ras�o entre Volume do Sistema nao Fragmentado pelo Fragmentado (V0/V).                                   
///   @param  Vfree	: Volume disponivel no Sistema para movimento do Fragmentado .                                   
///   @return		: Energia do Fragmento F(A,Z,T,M,Vfrac,Vfree).                                   
//_________________________________________________________________________________________________________

	double Fpart (int A, int Z, double T, int M, double Vfrac, double Vfree);

/// Defini��o da Energia Livre do Sistema .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Energia Livre do Sistema F(T,Mf,A0,Z0,vPart).                                   
//_________________________________________________________________________________________________________

	double F (int A0, int Z0, double T, int Mf, vPartition & vec);

/// Defini��o da Fun��o Parti�a� em termos da Energia LivreEnergia .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Fun��o Parti�a� Z(A0,Z0,T,Mf,vPart).                                   
//_________________________________________________________________________________________________________

	double Z (int A0, int Z0, double T, int Mf, vPartition & vec);

/// Defini��o do Termo de Entropia de Excita��o do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @return		: Contribui��o de Excita��o da Entropia do Fragmento Sex(A,Z,T).                                   
//_________________________________________________________________________________________________________

	double S_ex(int A,int Z, double T);

/// Defini��o do Termo de Entropia de T�rmica ( cin�tica ) da Parti��o.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Vfree	: Volume disponivel no Sistema para movimento do Fragmentado .                                   
///   @param  Naz	: Multiplicidade do Fragmento.                                   
///   @return		: Contribui��o T�rmica da Entropia da Parti��o Strans.                                   
//_________________________________________________________________________________________________________

	double S_trans(int A, int Z, double T, double Vfree, int Naz);

/// Defini��o da Entropia do Sistema .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Entropia do Sistema S(T,Mf,A0,Z0,vPart).                                   
//_________________________________________________________________________________________________________

	double S (int A0, int Z0, double T, int Mf, vPartition & vec);

/// Defini��o do Termo de Capacidade termica de Excita��o do Fragmento.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @return		: Contribui��o de Excita��o Calor Especifico do Fragmento Cvex(A,Z,T).                                   
//_________________________________________________________________________________________________________

	double Cv_ex(int A,int Z, double T);

/// Defini��o do Termo de Capacidade termica T�rmico ( cin�tica ) da Parti��o.
///
///   @param  A		: N�mero de Massa.                                   
///   @param  Z		: N�mero At�mico.                                   
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Vfree	: Volume disponivel no Sistema para movimento do Fragmentado .                                   
///   @param  Naz	: Multiplicidade do Fragmento.                                   
///   @return		: Contribui��o T�rmica do Calor Especifico Cvtrans.                                   
//_________________________________________________________________________________________________________

	double Cv_trans();

/// Defini��o da Capacidade termica do Sistema .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Calor Especifico do Sistema Cv(T,Mf,A0,Z0,vPart).                                   
//_________________________________________________________________________________________________________

	double Cv(int A0, int Z0, double T, int Mf, vPartition & vec);

/// Defini��o do Termo da Press�o (T�rmica) ( cin�tica ) da Parti��o.
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Vfree	: Volume disponivel no Sistema para movimento do Fragmentado .                                   
///   @return		: Contribui��o T�rmica da Press�o Ptrans.                                   
//_________________________________________________________________________________________________________

	double P_trans(double Vfree, double T);

/// Defini��o da Press�o do Sistema .
///
///   @param  T		: Temperatura da Parti��o.                                   
///   @param  Mf	: Multiplicidade do Sistema.                                   
///   @param  A0	: N�mero de Massa do Sistema.                                   
///   @param  Z0	: N�mero At�mico do Sistema.                                   
///   @param  vec	: Vetor Parti��o do Sistema.                                   
///   @return		: Press�o do Sistema P(T,Mf,A0,Z0,vPart).                                   
//_________________________________________________________________________________________________________

	double P (int A0, int Z0, double T, int Mf, vPartition & vec);

/// Defini��o da Fun��o fatorial de um n�mero inteiro.
///
///   @param  N		: Numero.                                   
///   @return		: N!.                                   
//_________________________________________________________________________________________________________

	double fatorial(int N);

/// Calcula a Temperatura de uma dada parti��o em um intervalo definido [0,Tc] usando o m�todo de varredura.
///
///   @param  Multiplicity		: Multiplicidade do Sistema.                                   
///   @param  A					: N�mero de Massa do Sistema.                                   
///   @param  Z					: N�mero At�mico do Sistema.                                   
///   @param  ExcitationEnergy	: Energia de Excita��o do Sistema.                                   
///   @param  Partition			: Vetor Parti��o do Sistema.                                   
///   @return					: Temperatura da Parti��o(T>0) se T<0 temperatura n�o encontrada no intervalo.                                   
//_________________________________________________________________________________________________________

	double GetTemperature( int Multiplicity, int A, int Z, double ExcitationEnergy ,vPartition & Partition );

/// Muda o valor do parametro Omega_0.
///
/// @param Omega_0_ : novo valor de Omega_0.
///
//_________________________________________________________________________________________________________

	inline void SetOmega_0( const int Omega_0_ ){ Omega_0 = Omega_0_; }

/// Muda o valor do parametro Betha_0.
///
/// @param Betha_0_ : novo valor de Betha_0.
///
//_________________________________________________________________________________________________________

	inline void SetBetha_0( const int Betha_0_ ){ Betha_0 = Betha_0_; }

/// Muda o valor do parametro C_c.
///
/// @param C_c_ : novo valor de C_c.
///
//_________________________________________________________________________________________________________

	inline void SetC_c( const int C_c_ ){ C_c = C_c_; }

/// Muda o valor do parametro K_asym.
///
/// @param K_asym_ : novo valor de K_asym.
///
//_________________________________________________________________________________________________________

	inline void SetK_asym( const int K_asym_ ){ K_asym = K_asym_; }

/// Muda o valor do parametro Q_asym.
///
/// @param Q_asym_ : novo valor de Q_asym.
///
//_________________________________________________________________________________________________________

	inline void SetQ_asym( const int Q_asym_ ){ Q_asym = Q_asym_; }

/// Muda o valor do parametro eps_0.
///
/// @param eps_0_ : novo valor de eps_0.
///
//_________________________________________________________________________________________________________

	inline void Seteps_0( const int eps_0_ ){ eps_0 = eps_0_; }

/// Muda o valor do parametro D_.
///
/// @param D__ : novo valor de D_.
///
//_________________________________________________________________________________________________________

	inline void SetD_( const int D__ ){ D_ = D__; }

/// Muda o valor do parametro T_c.
///
/// @param T_c_ : novo valor de T_c.
///
//_________________________________________________________________________________________________________

	inline void SetT_c( const int T_c_ ){ T_c = T_c_; }

private:	

	double		Omega_0 , // [MeV] 
				Betha_0 , // [MeV]
				C_c		, // [C/fm]
				K_asym	, // [MeV]
				Q_asym	, // [MeV]
				eps_0	, // [MeV]
				D_		, // [fm]
				T_c		; // [MeV]

};

#endif


