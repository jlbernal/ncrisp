/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TRANDOMI_HH
#define __TRANDOMI_HH


#include "TRandom.h"

#include <iostream>
#include <ctime>

///
/// A Classe TRandom foi construida com o objetivo de armazenar 
/// funções que geram números aleatórios para o uso no método de
/// Monte Carlo para a fragmentação e por padronização - facilitando 
/// a alterações dos geradores caso nescessários.
///
//_________________________________________________________________________________________________________

class TRandomi{

public:

///
/// Construtor
///
//_________________________________________________________________________________________________________

	TRandomi(){
	}

///
/// Destrutor
///
//_________________________________________________________________________________________________________

	~TRandomi(){}
	
/// 
/// Retorna um número inteiro aleatório na faixa [xi,xf]
/// 
/// @param xi : inicio do intervalo.
/// @param xf : fim do intervalo.
/// @return número inteiro aleatório.
/// 
//_________________________________________________________________________________________________________

	int intRand(int xi, int xf){

		return xi+gRandom->Integer((xf-xi)+1);
	}

/// 
/// Retorna um número aleatório real no intervalo [0,1]
/// 
/// @return número aleatório entre 0 e 1.
/// 
//_________________________________________________________________________________________________________

	double doubleRand(){

		return gRandom->Uniform();

	}

private:
	
};

#endif
