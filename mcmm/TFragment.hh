/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TFRAGMENT_HH
#define __TFRAGMENT_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <vector>

///                                                             
/// A Classe TFragment define as grandesas e métodos associados ao Fragmento.                                                             
///                                                             

class TFragment  
{
public:

///                                                             
///   Construtor                                                
///                                                             
///   @param  A_   : Número de Baryons.                       
///   @param  Z_   : Carga.                                   
///   @param  E_Ex : Energia de Excitação.                
///

	TFragment(const int A_, const int Z_, const double E_Ex):
		A(A_),
		Z(Z_),
		ExcitationEnergy(E_Ex),
		Probability(0)
		{}

///  Construtor Padrão

	TFragment():
		A(0),
		Z(0),
		ExcitationEnergy(0),
		Probability(0)
	{}

///    Construtor de Cópia

	TFragment(const	 TFragment & frag)
	{

		A				 = frag.A;
		Z				 = frag.Z;
		ExcitationEnergy = frag.ExcitationEnergy;
		Probability 	 = frag.Probability;
		
	}

public:

///    Destrutor Padrão 

	~TFragment()
	{}

public:

///                                                             
///   Retorna a Energia de Excitação do Fragmento.                                                 
///                                                             
	double GetExcitationEnergy();

///                                                             
///   Retorna o número de Baryons do Fragmento.                                                 
///                                                             

	inline int GetA(){ return A; }

///                                                             
///   Retorna a carga do Fragmento.                                                 
///                                                             

	inline int GetZ(){ return Z; }

///                                                             
///   Retorna a multiplicidade do Fragmento.                                                 
///                                                             

	inline int GetM(){ return M; }

///                                                             
///   Retorna a probabilidade associada ao Fragmento.                                                 
///                                                             

	inline double GetProb(){ return Probability; }

///                                                             
///   Altera o número de Baryons do Fragmento.                   
///                                                             
///   @param  A_   : número de baryons                       
///

	inline void SetA( const int A_ ){ A = A_; }

///                                                             
///   Altera a Carga do Fragmento.                   
///                                                             
///   @param  Z_   : Carga                       
///

	inline void SetZ( const int Z_ ){ Z = Z_; }

///                                                             
///   Altera a Multiplicidade do Fragmento.                   
///                                                             
///   @param  M_   : Multiplicidade                       
///

	inline void SetM( const int M_ ){ M = M_; }

///                                                             
///   Altera a Probabilidade associada ao Fragmento.                   
///                                                             
///   @param  Probability_   : Probabilidade                       
///

	inline void SetProb( const double Probability_ )
	{ 
		Probability = Probability_; 
	}

///                                                             
///   Altera a Energia de Excitação associada ao Fragmento.                   
///                                                             
///   @param  E_   : Energia de Excitação                      
///

	inline void SetExcitationEnergy(const double E_ )
	{ 
		ExcitationEnergy = E_; 
	}

///                                                             
///   Definição do operador Atribuição.                   
///                                                             

	const TFragment & operator=(const TFragment &var);

///                                                             
///   Definição do operador Igualdade.                   
///                                                             

	bool operator==(const TFragment &var) const;

///                                                             
///   Definição do operador Diferença.                   
///                                                             

	bool operator!=(const TFragment &var) const;

private:

	/// Energia de Excitação.   
	double ExcitationEnergy,
	/// Probabilidade Associada. 
		   Probability;
	/// Número de Baryons do Fragmento. 
	int A,
	/// Carga do Fragmento. 
		Z,
	/// Multiplicidade do Fragmento. 
		M;
	
	// momentum
	// position
};	


#endif

