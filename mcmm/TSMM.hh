/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TSMM_HH
#define __TSMM_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <string>
#include <fstream>  
#include <sstream>
#include <vector>

#include "TPartition.hh" 
#include "TStoragef.hh"
#include "TRandomi.hh"

#define RelaxationParameter 1.0e-5

using namespace std;

/// Classe que executa o calculo de Monte Carlo para o processo de multifragmrnta��o.
/// 
/// 
//_________________________________________________________________________________________________________

class TSMM{

public:

/// Construtor 
/// 
/// @param A_		: Numero de Baryons do Nucleo.
/// @param Z_		: Numero de Baryons do Nucleo.
/// @param E_		: Energia de Excita��o do Nucleo.
/// @param Method_	: Methodo usado para o calculo (analogic|findmax|stepsavemax).
/// 
//_________________________________________________________________________________________________________

	TSMM(int A_,int Z_,double E_, string Method_);

/// Construtor 
/// 
/// @param A_		: Numero de Baryons do Nucleo.
/// @param Z_		: Numero de Baryons do Nucleo.
/// @param E_		: Energia de Excita��o do Nucleo.
/// 
//_________________________________________________________________________________________________________

	TSMM(int A_,int Z_,double E_);

/// Construtor Padr�o
/// 
//_________________________________________________________________________________________________________

	TSMM();

/// Destrutor
/// 
//_________________________________________________________________________________________________________

	~TSMM();

/// Seleciona o metodo de calculo.
/// 
/// @param meth	: Methodo usado para o calculo (analogic|findmax|stepsavemax).
/// 
//_________________________________________________________________________________________________________

	void setMethod(string meth);

/// Executa o calculo.
/// 
/// @param hist			: Numero de historias executadas.
/// @param relaxation	: Parametro de relaxa��o do calculo (usado em findmax|stepsavemax).
/// @param arq			: nome do arquivo de entrada de dados.
/// 
//_________________________________________________________________________________________________________

	void execute(int hist, string arq){

		execute(hist, RelaxationParameter, arq);

	}



	void execute(int hist, double relaxation, string arq){
	
		ifstream fin(arq.c_str(),ios::in);
		
		if (fin.is_open()){

			int		A_,
					Z_;
			double	E_;

			while (fin >> Z_ >> A_ >> E_){
				execute(hist,relaxation);
			}
		}
		else{
			cerr << "Error : Arquivo: "<< arq << " n�o encontrado ou inexixtente. \n";
			exit(1);
		}
		bank.MakeStatistics();
		fin.close();
	}

/// Executa o calculo.
/// 
/// @param hist			: Numero de historias executadas.
/// @param relaxation	: Parametro de relaxa��o do calculo (usado em findmax|stepsavemax).
/// 
//_________________________________________________________________________________________________________

	void execute(int hist, double relaxation);

/// Executa o calculo.
/// 
/// @param hist			: Numero de historias executadas.
/// 
//_________________________________________________________________________________________________________

	void execute(int hist );

/// Metodo findmaxMethod.
/// 
/// @param hist			: Numero de historias executadas.
/// 
//_________________________________________________________________________________________________________

	void findmaxMethod(int hist);

/// Metodo findmaxMethod.
/// 
/// @param hist			: Numero de historias executadas.
/// @param relaxation	: Parametro de relaxa��o do calculo (usado em findmax|stepsavemax).
/// 
//_________________________________________________________________________________________________________

	void findmaxMethod(int hist, double relaxation);

/// Metodo analogicMethod.
/// 
/// @param hist			: Numero de historias executadas.
/// 
//_________________________________________________________________________________________________________

	void analogicMethod(int hist);

/// Metodo stepsavemaxMethod.
/// 
/// @param hist			: Numero de historias executadas.
/// 
//_________________________________________________________________________________________________________

	void stepsavemaxMethod (int hist);

/// Metodo stepsavemaxMethod.
/// 
/// @param hist			: Numero de historias executadas.
/// @param relaxation	: Parametro de relaxa��o do calculo (usado em findmax|stepsavemax).
/// 
//_________________________________________________________________________________________________________

	void stepsavemaxMethod (int hist, double relaxation);

/// Executa o calculo Monte Carlo em cascata em um intervalo de energia.
/// 
/// @param hist			: Numero de historias executadas.
/// @param Ei			: Limite inferior de energia.
/// @param Ef			: Limite superior de energia.
/// @param step			: numero de intervalos para calculo.
/// 
//_________________________________________________________________________________________________________

	void scanningProcess(int hist, double Ei, double Ef, int step);

/// Executa o calculo Monte Carlo em cascata em um intervalo de energia.
/// 
/// @param hist			: Numero de historias executadas.
/// @param relaxation	: Parametro de relaxa��o do calculo (usado em findmax|stepsavemax).
/// @param Ei			: Limite inferior de energia.
/// @param Ef			: Limite superior de energia.
/// @param step			: numero de intervalos para calculo.
/// 
//_________________________________________________________________________________________________________

	void scanningProcess(int hist, double Ei, double Ef, int step, double relax);

/// Altera os valores de A, Z e E.
/// 
/// @param A_		: Numero de Baryons do Nucleo.
/// @param Z_		: Numero de Baryons do Nucleo.
/// @param E_		: Energia de Excita��o do Nucleo.
/// 
//_________________________________________________________________________________________________________

	void setAZE(int A_, int Z_, double E_);

/// Grava saida em arquivo.
/// 
/// 
//_________________________________________________________________________________________________________

	void writefile();

/// Converte inteiro em string.
/// 
/// @param I : numero inteiro.
/// 
//_________________________________________________________________________________________________________

	string convert(int I);

/// Converte float em string.
/// 
/// @param I : numero float.
/// 
//_________________________________________________________________________________________________________

	string convert(double I);

private:

/// metodo de calculo.
	string Method;
/// Guardador de dados.
	TStoragef bank;
/// numero de Barions.
	int		A,
/// Carga.
			Z;
/// Energia de excita��o.
	double	E;


};
#endif
