/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TCanonical.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

double TCanonical::betha(double T)
{
	
	return Betha_0 * pow((T_c * T_c - T * T) / (T_c * T_c + T * T), 0.5e1 / 0.4e1);
	
}

//_________________________________________________________________________________________________________

double TCanonical::dbethadT(double T)
{
	
	return  0.5e1 / 0.4e1 * Betha_0 * pow((T_c * T_c - T * T) / (T_c * T_c + T * T), 
		    0.1e1 / 0.4e1) * (-0.2e1 * T / (T_c * T_c + T * T) - 0.2e1 * (T_c * T_c 
			- T * T) * pow(T_c * T_c + T * T, -0.2e1) * T);
}

//_________________________________________________________________________________________________________

double TCanonical::d2bethadT2(double T){
	
	return  0.5e1 / 0.16e2 * Betha_0 * pow((T_c * T_c - T * T) / (T_c * T_c + T * T
		    ),-0.3e1 / 0.4e1) * pow(-0.2e1 * T / (T_c * T_c + T * T) - 0.2e1 * (T_c
			*T_c - T * T) * pow(T_c * T_c + T * T, -0.2e1)*T,0.2e1) + 0.5e1 / 0.4e1
			* Betha_0 * pow((T_c * T_c - T * T)/(T_c * T_c + T * T), 0.1e1 / 0.4e1)
			* (-0.2e1 / (T_c * T_c + T * T) + 0.8e1 * T * T * pow(T_c * T_c + T * T
			, -0.2e1) + 0.8e1 * (T_c * T_c - T * T) * pow(T_c * T_c + T * T, -0.3e1
			) * T * T - 0.2e1 * (T_c * T_c - T * T) * pow(T_c * T_c + T * T, -0.2e1
			));	
}

//_________________________________________________________________________________________________________

double TCanonical::V_0(int A)
{

	return  0.1333333333e1 * 0.3141592654e1 * int(A) * pow(r_0, 0.3e1);

}

//_________________________________________________________________________________________________________

double TCanonical::x_f(int A0, int Mf)
{
	
	return  pow( 0.1e1 + 0.500000e0 * D_ * (pow(int(Mf), 0.3333333333e0) - 0.1e1) * 
		    pow(int(A0),-0.3333333333e0), 0.3e1) - 0.1e1;
}

//_________________________________________________________________________________________________________

double TCanonical::V_free(int A0, int Mf)
{
	
	return  x_f(A0,Mf)*V_0(A0);

}

//_________________________________________________________________________________________________________

double TCanonical::V(int A0, int Mf)
{

	return  (1.0 + x_f(A0,Mf))*V_0(A0);

}

//_________________________________________________________________________________________________________

double TCanonical::B(int A,int Z)
{

	if ((A == 1)&&(Z == 0)){
		return 0.;
	} 
	else if((A == 1)&&(Z == 1)){
		return C_c;
	}
	else if((A == 2)&&(Z == 0)){
		return 0.12;
	}
	else if((A == 2)&&(Z == 1)){
		return -2.226;
	}
	else if((A == 2)&&(Z == 2)){
		return 0.8;
	}
	else if((A == 3)&&(Z == 0)){
		return 0.0;
	}
	else if((A == 3)&&(Z == 1)){
		return -8.48;
	}
	else if((A == 3)&&(Z == 2)){
		return -7.72;
	}
	else if((A == 4)&&(Z == 1)){
		return -5.66;
	}
	else if((A == 4)&&(Z == 2)){
		return -28.29;
	}
	else if((A == 4)&&(Z == 3)){
		return -4.82;
	}
	else {
		return  Omega_0 * int(A) -Betha_0 * pow(int(A), 0.2e1/0.3e1) - C_c * int(Z)
			    * int(Z) * pow(int(A),-0.1e1 / 0.3e1) - K_asym * pow(int(A) - 0.2e1
				* int(Z), 0.2e1) / int(A) / (0.1e1 + 0.22500000e1 * K_asym / Q_asym
				* pow(int(A), -0.1e1 / 0.3e1));
	}
}

//_________________________________________________________________________________________________________

double TCanonical::g(const int A,const int Z)
{
	if ((A == 1)&&(Z == 0)){
		return 2.0;
	} 
	else if((A == 1)&&(Z == 1)){
		return 2.0;
	}
	else if((A == 2)&&(Z == 0)){
		return 1.0;
	}
	else if((A == 2)&&(Z == 1)){
		return 3.0;
	}
	else if((A == 2)&&(Z == 2)){
		return 1.0;
	}
	else if((A == 3)&&(Z == 0)){
		return 2.0;
	}
	else if((A == 3)&&(Z == 1)){
		return 2.0;
	}
	else if((A == 3)&&(Z == 2)){
		return 2.0;
	}
	else if((A == 4)&&(Z == 1)){
		return 5.0;
	}
	else if((A == 4)&&(Z == 2)){
		return 1.0;
	}
	else if((A == 4)&&(Z == 3)){
		return 5.0;
	}
	else {
		return  1.0;
	}
}

//_________________________________________________________________________________________________________

double TCanonical::GetTemperature( int Multiplicity, int A, int Z, double ExcitationEnergy ,vPartition & Partition )
{
	
	double n = 100.0; // define o numero de intervalos da varredura
	double h = T_c/n;
	n = 0;

	double e_ante=0;
	double e_prox=0;

	do{

		e_ante = deltaE(n, Multiplicity, A, Z, ExcitationEnergy ,Partition);
		n +=h; 
		if (n >= T_c) {
			return -1.0;
		}
		e_prox = deltaE(n, Multiplicity, A, Z, ExcitationEnergy ,Partition);

		if(e_prox*e_ante < 0 ) {
			h = - h/3;
		}

	}while(abs(deltaE(n, Multiplicity, A, Z, ExcitationEnergy ,Partition)) > 1.0e-10 );

	return n;
}

//_________________________________________________________________________________________________________

double TCanonical::V_frac(const int A0, const int Mf)
{
	return 1.0/( x_f(A0, Mf) + 1.0 );
}

//_________________________________________________________________________________________________________

double TCanonical::E_k(double T)
{
	return ( 3.0/2.0 ) * T ;
}

//_________________________________________________________________________________________________________

double TCanonical::E_ex(int A,int Z, double T)
{
	
	if ((A <= 3)||((A == 4)&&(Z != 2))) {
		return 0.0;
	}
	else if ((A == 4)&&(Z == 2)) {
		return (-T*T/eps_0)*double(A);
	}
	else {
		return T * T * double(A) / eps_0 + (Betha_0 * pow((T_c * T_c - T * T) /
			   ( T_c * T_c + T * T ),0.5e1 / 0.4e1) - T * dbethadT(T) - Betha_0
			   ) * pow(double(A), 0.6666666667e0);
	}
}

//_________________________________________________________________________________________________________

double TCanonical::E_C(int A,int Z, double Vfrac)
{
	return  -C_c * Z * Z * pow(Vfrac / A, 0.3333333333e0);
}

//_________________________________________________________________________________________________________

double TCanonical::Efrag(int A,int Z, double T, double Vfrac)
{
	return E_ex(A,Z,T) + E_C(A,Z,Vfrac) + E_k(T) - B(A,Z) ;
}

//_________________________________________________________________________________________________________

double TCanonical::deltaE(double T, int Mf, int A0, int Z0, double E_exc ,vPartition & vec)
{
	double Vfrac = V_frac(A0,Mf);
	double Etot =	- E_exc 
					+ B(A0,Z0) 
					- E_C(A0,Z0,Vfrac) // Termo de Coulomb da Esfera.
					- E_k(T); // retira contribuição termica relativa ao referencial.

	for (vPartition::iterator SS = vec.begin(); SS != vec.end(); SS++)
		Etot += (*SS)->GetM()*Efrag((*SS)->GetA(),(*SS)->GetZ(),T,Vfrac); 

	return Etot;
}

//_________________________________________________________________________________________________________

double TCanonical::Epart(double T, int Mf, int A0, int Z0, double E_exc ,vPartition & vec)
{
	double Vfrac = V_frac(A0,Mf);
	double Etot =	  double(Z0)*mpc2
					+ double(A0-Z0)*mnc2
					- E_C(A0,Z0,Vfrac)// Termo de Coulomb da Esfera.
					- E_k(T); // retira contribuição termica relativa ao referencial.

	for (vPartition::iterator SS = vec.begin(); SS != vec.end(); SS++)
		Etot += (*SS)->GetM()*Efrag((*SS)->GetA(),(*SS)->GetZ(),T,Vfrac); 

	return Etot;
}

//_________________________________________________________________________________________________________

double TCanonical::lambdaT(const int A, const double T)
{
	return hc*sqrt(2*PI/(mnc2*double(A)*T));
}

//_________________________________________________________________________________________________________

double TCanonical::F_c(int A, int Z, double Vfrac)
{
	return -C_c * pow(Vfrac, 0.1e1 / 0.3e1) * Z * Z * pow(A, -0.1e1 / 0.3e1);
}

//_________________________________________________________________________________________________________

double TCanonical::F_ex(int A,int Z, double T)
{
	if ((A <= 3)||((A == 4)&&(Z != 2))) {
		return 0;
	}
	else if ((A == 4)&&(Z == 2)) {
		return -T*T*double(A)/eps_0;
	}
	else {
		return	-T * T * A / eps_0 + (betha(T) - Betha_0) * pow(A, 0.6666666667e0);
	}
}

//_________________________________________________________________________________________________________

double TCanonical::F_trans(int A, int Z, double T, double Vfree, int Naz)
{
	return -T*(
			   log( g(A,Z) * Vfree  / pow( lambdaT(A,T),3.0 ) )
			   - log( fatorial(Naz) ) / double(Naz)
		   );
}

//_________________________________________________________________________________________________________

double TCanonical::Fpart(int A, int Z, double T, int M, double Vfrac, double Vfree)
{
	return F_ex(A,Z,T) + F_c(A,Z,Vfrac) + F_trans(A, Z, T, Vfree, M) - B(A,Z);
}

//_________________________________________________________________________________________________________

double TCanonical::F(int A0, int Z0, double T, int Mf, vPartition & vec)
{

	vPartition::iterator SS;
	double sum=0;
	double Vfrac = V_frac(A0,Mf);
	double Vfree = V_free(A0,Mf);

	sum = F_c(A0, Z0, Vfrac); 
	
	for (SS=vec.begin(); SS!=vec.end();SS++){
		sum += double((*SS)->GetM())*(
			Fpart ((*SS)->GetA(),(*SS)->GetZ(),T,(*SS)->GetM(),Vfrac,Vfree)
			);
	}
	return sum;
}

//_________________________________________________________________________________________________________

double TCanonical::Z(int A0, int Z0, double T, int Mf, vPartition & vec)
{
	return exp(-F(A0,Z0,T,Mf,vec)/T);
}

//_________________________________________________________________________________________________________

double TCanonical::S_ex(int A,int Z, double T)
{
	if ((A <= 3)||((A == 4)&&(Z != 2))) {
		return 0.0;
	}
	else if ((A == 4)&&(Z == 2)) {
		return 2.0*T*double(A)/eps_0;
	}
	else {
		return	(2.0*T/eps_0)*double(A) - dbethadT(T)*pow(double(A),2./3.); 
	}
}

//_________________________________________________________________________________________________________

double TCanonical::S_trans(int A, int Z, double T, double Vfree, int Naz)
{
	return (log( g(A,Z) * Vfree / pow( lambdaT(A,T),3.0 ) )
			   - log( fatorial(Naz) ) / double(Naz) + 3.0/2.0);
}

//_________________________________________________________________________________________________________

double TCanonical::S(int A0, int Z0, double T, int Mf, vPartition & vec)
{
	vPartition::iterator SS;
	double sum=-3/2;
	double Vfree = V_free(A0,Mf);
		
	for (SS=vec.begin(); SS!=vec.end();SS++){
		sum += double((*SS)->GetM())*(
			  S_ex( (*SS)->GetA(), (*SS)->GetZ(), T)
			+ S_trans((*SS)->GetA(), (*SS)->GetZ(), T, Vfree,(*SS)->GetM())
		);
	}
	return sum;
}

//_________________________________________________________________________________________________________

double TCanonical::Cv_ex(int A,int Z, double T)
{
	if ((A <= 3)||((A == 4)&&(Z != 2))) {
		return 0.0;
	}
	else if ((A == 4)&&(Z == 2)) {
		return 2.0*T*double(A)/eps_0;
	}
	else {
		return	(2.0*T/eps_0)*double(A) + -T*d2bethadT2(T)*pow(double(A),2./3.); 
	}
}

//_________________________________________________________________________________________________________

double TCanonical::Cv_trans()
{
	return 3.0/2.0;
}

//_________________________________________________________________________________________________________

double TCanonical::Cv(int A0, int Z0, double T, int Mf, vPartition & vec)
{
	vPartition::iterator SS;
	double sum= - Cv_trans();
		
	for (SS=vec.begin(); SS!=vec.end();SS++){
		sum += double((*SS)->GetM())*(
			  Cv_ex( (*SS)->GetA(), (*SS)->GetZ(), T)
			+ Cv_trans()
			);
	}
	return sum;
}

//_________________________________________________________________________________________________________

double TCanonical::P_trans(double Vfree, double T)
{
	return T/Vfree;
}

//_________________________________________________________________________________________________________

double TCanonical::P(int A0, int Z0, double T, int Mf, vPartition & vec)
{
	vPartition::iterator SS;
	double sum=0;
	double V_ = V(A0,Mf);
	double Vfrac = V_frac(A0,Mf);
	double Vfree = V_free(A0,Mf);

	sum = - P_trans(Vfree, T) - (1/(3.0*V_))*F_c(A0, Z0, Vfrac); 
	
	for (SS=vec.begin(); SS!=vec.end();SS++){
		sum += double((*SS)->GetM())*(
			P_trans(Vfree, T) +
			(1/(3.0*V_))*F_c((*SS)->GetA(), (*SS)->GetZ(), Vfrac)
			);
	}
	return sum;
}

//_________________________________________________________________________________________________________

double TCanonical::fatorial(int N)
{
	if (N == 0){
		return 1.0;
	}
	else if(N == 1){
		return 1.0;
	}
	else{
		double mount=1;
		for (int i = 1; i != N+1; i++)
			mount = mount * double(i);
		return mount;
	}
}



