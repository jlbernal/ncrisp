/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TPartitionHelper.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

void TPartitionHelper::Makefirstpartition(int A, int M, vector<int> & v){

	if (v.empty() != true ){
		v.clear();
	}

	for (int j=0; j<(M-1); j++) {
			v.push_back(1);
	}

	v.push_back(A+1-M);
}

//_________________________________________________________________________________________________________

bool TPartitionHelper::NextPartitionIs(vector<int> & vec){

	int i, m;

	i = vec.size()-1;

	for(m=vec[i] ; m-vec[i]<2 ; ){
		if(--i < 0)
			return false;
	}
	return true;
}

//_________________________________________________________________________________________________________

bool TPartitionHelper::nextpartition(vector<int> & vec) // pag 232 do Andrews 
{
	int		i, 
			j, 
			m, 
			res, 
			length;

	length = vec.size();

	i = length -1;

	for(m=vec[i] ; m-vec[i]<2 ; ){

		if(--i < 0)
			return false;
	}

	// Lambda_j = i (veja Andrews)
	// se existe a proxima parti��o Lambda_j >= 0, caso contrario quarda o indice.

	vec[i]++;
	j=vec[i];

	res= -1;  // res - Valor Residual

	do {
		res += vec[i] - j;
		vec[i] = j;
	} while (++i < length-1);

	vec[length-1] += res;

	return true;
}

