/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TSMM.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

TSMM::TSMM(int A_,int Z_,double E_, string Method_):Method(Method_),A(A_),Z(Z_),E(E_){}

//_________________________________________________________________________________________________________

TSMM::TSMM(int A_,int Z_,double E_):Method("analogic"),A(A_),Z(Z_),E(E_){}

//_________________________________________________________________________________________________________

TSMM::TSMM():Method("analogic"),A(0),Z(0),E(0){}

//_________________________________________________________________________________________________________

TSMM::~TSMM()
{
	bank.reset();
}

//_________________________________________________________________________________________________________

void TSMM::setMethod(string meth)
{
	if ((meth =="analogic")||(meth =="findmax")||(meth =="stepsavemax"))
		Method = meth;
	else
		cout << "Warning : Method " << meth << " not exist. \n"
			 << "Using "<<Method<<" Method. \n";
}

//_________________________________________________________________________________________________________

void TSMM::execute(int hist, double relaxation)
{
	if ((A==0)||(Z==0)||(E==0)){
			
		cerr << " Error : TSMM : Invalid value for (A,Z,E) \n";
		exit(1);
	}
	if(Method =="analogic"){
		
		cerr << " Error : TSMM : Invalid value for Method analogic : relaxation . \n";
		exit(1);
	} 
	else if(Method =="findmax"){

		cerr<<"Warning : Method No Tested. \n";
		findmaxMethod(hist, relaxation);
		bank.MakeStatistics();
	}
	else if(Method =="stepsavemax"){
			
		stepsavemaxMethod(hist,relaxation);		
		bank.MakeStatistics();
	}
}

//_________________________________________________________________________________________________________

void TSMM::execute(int hist )
{
	if ((A==0)||(Z==0)||(E==0)){
		
		cerr << " Error : TSMM : Invalid value for (A,Z,E) \n";
		exit(1);
	}
	if(Method =="analogic"){
		
		analogicMethod(hist);
		bank.MakeStatistics();
	} 
	else if(Method =="findmax"){
		
		cerr<<"Warning : Method No Tested. \n";
		findmaxMethod(hist);
		bank.MakeStatistics();
	}
	else if(Method =="stepsavemax"){
		
		stepsavemaxMethod(hist);		
		bank.MakeStatistics();

	}
}

//_________________________________________________________________________________________________________

void TSMM::findmaxMethod(int hist)
{
	findmaxMethod(hist, RelaxationParameter);
}

//_________________________________________________________________________________________________________

void TSMM::findmaxMethod(int hist, double relaxation)
{
	vector<double> probmaxpart;

	TPartition *a = new TPartition(A,Z,E);

	for (int Multip = 1; Multip <= A; Multip++){
		probmaxpart.push_back(a->findMustProbable(Multip));
	}

	double tempweight	= 0;
	int count			= 0;
	double mount		= 0;
		
	while ( count < hist ){
		

		a->MakeProcess();

		do{
			tempweight = a->GetWeight();
		}while( probmaxpart[(int(a->GetMultiplicity())-1)]*relaxation <= (tempweight) );

		mount = tempweight;
		cout << "Peso : " << mount << "\n";
		bank.AddWeight(mount);
		bank.AddMultiplicity(a->GetMultiplicity(),mount);
		bank.AddTemperature(a->GetTemperature(),mount);
		bank.AddEntropy(a->GetEntropy(),mount);
		bank.AddCv(a->GetCv(),mount);
		bank.AddPartition(a->GetPartition(),mount); 
		count++;
		cout << " hist : " << count << "\n";
		
	}
	delete a;
}

//_________________________________________________________________________________________________________

void TSMM::analogicMethod(int hist){
	
	int count = 0;
		
	TPartition *a = new TPartition(A,Z,E);

	double mount = 0;

	while ( count < hist ){
	
		mount = a->GetWeight();
	
		a->MakeProcess();

		bank.AddWeight(mount);
		bank.AddMultiplicity(a->GetMultiplicity(),mount);
		bank.AddTemperature(a->GetTemperature(),mount);
		bank.AddEntropy(a->GetEntropy(),mount);
		bank.AddCv(a->GetCv(),mount);
		bank.AddPartition(a->GetPartition(),mount); 

		count++;
	}
	delete a;
}

//_________________________________________________________________________________________________________

void TSMM::stepsavemaxMethod (int hist)
{
	stepsavemaxMethod (hist, RelaxationParameter);
}

//_________________________________________________________________________________________________________

void TSMM::stepsavemaxMethod (int hist, double relaxation)
{
	double maxweight	= 0;
	double tempweight	= 0;
	int count			= 0;
	bool flag			= false;
	double mount		= 0;
		
	TPartition *a = new TPartition(A,Z,E);

	while ( count < hist ){
		
		flag=false;

		a->MakeProcess();
		tempweight = a->GetWeight();

		if (tempweight > maxweight ){
			maxweight = tempweight;
			flag = true;
		}
		else if(maxweight*relaxation <= (tempweight)){
			flag = true;
		}
		if (flag == true){

			mount = tempweight;
			cout << "Peso : " << mount << "\n";
			bank.AddWeight(mount);
			bank.AddMultiplicity(a->GetMultiplicity(),mount);
			bank.AddTemperature(a->GetTemperature(),mount);
			bank.AddEntropy(a->GetEntropy(),mount);
			bank.AddCv(a->GetCv(),mount);
			bank.AddPartition(a->GetPartition(),mount); 

			count++;
			cout << " hist : " << count << "\n";
		}
	}
	delete a;
}

//_________________________________________________________________________________________________________

void TSMM::scanningProcess(int hist, double Ei, double Ef, int step)
{

	scanningProcess(hist,Ei,Ef,step,RelaxationParameter);
}

//_________________________________________________________________________________________________________

void TSMM::scanningProcess(int hist, double Ei, double Ef, int step, double relax)
{
	E = Ei;
	double dE = (Ef-Ei)/double(step);

	string name;

	name = "scanZ" + convert(Z)+"A"+ convert(A);
	ofstream fout (name.c_str());

	fout.setf(ios::scientific, ios::floatfield);
	fout.adjustfield;

	fout	<<  "	Energy       "
			<<  "	Multiplicity 	Variance     "
			<<  "	Temperature  	Variance     "
			<<  "	Heat Capacity	Variance     "
			<<  "	Entropy      	Variance     "
			<<  "\n\n";

	for( int j = 0; j<=step; j++ ){
			
		E = Ei+double(j)*dE;

		execute(hist,relax);

		writefile();
			
		fout	<<  "	" << E
				<<  "	" << bank.GetMultiplicity() << "	" << bank.GetsigMultiplicity()
				<<  "	" << bank.GetTemperature()	<< "	" << bank.GetsigTemperature()
				<<  "	" << bank.GetCv()			<< "	" << bank.GetsigCv()
				<<  "	" << bank.GetEntropy()		<< "	" << bank.GetsigEntropy()		<< "\n";

		bank.reset();
	}
	fout.close();
}

//_________________________________________________________________________________________________________

void TSMM::setAZE(int A_, int Z_, double E_)
{
	A	=  A_;
	Z	=  Z_;
	E	=  E_;
}

//_________________________________________________________________________________________________________

void TSMM::writefile()
{
	string name;

	name = "outZ" + convert(Z)+"A"+ convert(A)+"E"+convert(E);
	ofstream fout (name.c_str());

	fout << bank;
	fout.close();
}

//_________________________________________________________________________________________________________

string TSMM::convert(int I)
{
	string name;
	stringstream Convert;

	Convert << I;
	Convert >> name;

	return name;
}

//_________________________________________________________________________________________________________

string TSMM::convert(double I)
{
	string name;
	stringstream Convert;

	Convert << I;
	Convert >> name;

	return name;
}



