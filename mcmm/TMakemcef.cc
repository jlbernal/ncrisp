/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


#include "TMakemcef.hh"

void EvaporePartition(vPartition & Part){

	Mcef *mcef = new Mcef();

	for (vPartition::iterator i = Part.begin(); i != Part.end(); i++ ) {

		mcef->Generate( (*i)->GetA(), (*i)->GetZ(), (*i)->GetExcitationEnergy());

		int a_final,z_final;
		mcef->FinalConfiguration((*i)->GetA(), (*i)->GetZ(), a_final, z_final);

		(*i)->SetA(a_final);
		(*i)->SetZ(a_final);

				//NumberFissions()     -> ????
				//ProtonMultiplicity() ->  + 10001	
				//NeutronMultiplicity()->  + 00001
				//AlphaMultiplicity()  ->  + 20004
							
	}
}


