/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TSTORAGEF_HH
#define __TSTORAGEF_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <map>
#include <cmath>

#include <string>
#include <fstream>

#include "TPartition.hh"

typedef vector<TFragment*> vPartition;

using namespace std;

/// TStoragef � uma classe, n�o muito especializada, criada para guardar valores 
/// para o usoa da simla��o Monte Carlo.
///
//_________________________________________________________________________________________________________

class TStoragef {

public:

/// Construtor Padr�o.
/// 
/// 
//_________________________________________________________________________________________________________

	TStoragef():	Mount(0),
				Temperature(0),
				Cv(0),
				Entropy(0),
				Multiplicity(0){}

/// Destrutor Padr�o.
/// 
/// 
//_________________________________________________________________________________________________________

	~TStoragef();

/// Limpa Conteiners e zera variaveis.
/// 
/// 
//_________________________________________________________________________________________________________

	void reset();

/// Guarda contagens dos fragmentos e seu respectivo peso e seu valor quadratico.
/// 
/// @param A_		: Numero de Barions.
/// @param Z_		: Carga.
/// @param M		: Multiplicidade.
/// @param Weight	: Peso Associado.
/// 
//_________________________________________________________________________________________________________
	
	void AddYield(int A_, int Z_, double M, double Weight);

/// Guarda todos os elementos de uma parti��o.
/// 
/// @param Part		: Vetor Parti��o.
/// @param Weight	: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddPartition(vPartition & Part, double Weight);

/// Incrementa peso total de Weight.
/// 
/// @param Weight	: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddWeight( double Weight);

/// Incrementa a Temperatura e seu valor quadratico.
/// 
/// @param Temperature_	: Temperatura .
/// @param Weight		: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddTemperature(double Temperature_, double Weight_);

/// Incrementa a Entropia e seu valor quadratico.
/// 
/// @param Entropy		: Entropia.
/// @param Weight		: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddEntropy(double Entropy_,  double Weight_);

/// Incrementa a Capacidade termica e seu valor quadratico.
/// 
/// @param Cv_			: Capacidade termica.
/// @param Weight		: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddCv(double Cv_, double Weight_);

/// Incrementa a Multiplicidade e seu valor quadratico.
/// 
/// @param Multiplicity	: Multiplicidade.
/// @param Weight		: Peso Associado.
/// 
//_________________________________________________________________________________________________________

	void AddMultiplicity(double Multiplicity_, double Weight_);

/// Controi as estatisticas (faz a media).
/// 
/// 
//_________________________________________________________________________________________________________

	void MakeStatistics();

/// Sobrecarga de operador << para cout.
/// 
/// 
//_________________________________________________________________________________________________________

	friend ostream& operator<<(ostream& out, TStoragef& stor){
		
		out.setf(ios::scientific, ios::floatfield);
		
		out << "\n     Physics Properties [Breakup System] \n\n";

		out <<"		<Temperature>  : "	<< stor.Temperature	<<"	"
										<< stor.GetsigTemperature()<< "\n";
		out <<"		<Entropy>      : "	<< stor.Entropy		<<"	"
										<< stor.GetsigEntropy()<< "\n";
		out <<"		<Multiplicity> : "	<< stor.Multiplicity<<"	"
										<< stor.GetsigMultiplicity()<< "\n";
		out <<"		<Cv>           : "	<< stor.Cv			<<"	"
										<< stor.GetsigCv()<< "\n";

		out << "\n\n";

		out << "     Prodction [Nuclide(Nucleon)]    \n\n";
		out << "		ZAID		Yield			RERR     \n\n";

		out.adjustfield;

		map<int, double>::iterator imap;
		
		for (imap = stor.Yield.begin(); imap != stor.Yield.end(); imap++ ){
			out << "		"<< imap->first 
				<< "		"<< imap->second 
				<< "		"<< sqrt(- imap->second * imap->second + stor.Yield2[imap->first])
				<< "\n";
		}

		return out;
	}

/// Sobrecarga de operador << para saida de disco.
/// 
/// 
//_________________________________________________________________________________________________________

	friend ofstream& operator<<(ofstream& out , TStoragef& stor){

		out.setf(ios::scientific, ios::floatfield);
		
		out << "\n     Physics Properties [Breakup System] \n\n";

		out <<"		<Temperature>  : "	<< stor.Temperature	<<"	"
										<< stor.GetsigTemperature()<< "\n";
		out <<"		<Entropy>      : "	<< stor.Entropy		<<"	"
										<< stor.GetsigEntropy()<< "\n";
		out <<"		<Multiplicity> : "	<< stor.Multiplicity<<"	"
										<< stor.GetsigMultiplicity()<< "\n";
		out <<"		<Cv>           : "	<< stor.Cv			<<"	"
										<< stor.GetsigCv()<< "\n";

		out << "\n\n";

		out << "     Prodction [Nuclide(Nucleon)]    \n\n";
		out << "		ZAID		Yield			RERR     \n\n";

		out.adjustfield;

		map<int, double>::iterator imap;
		
		for (imap = stor.Yield.begin(); imap != stor.Yield.end(); imap++ ){
			out << "		"<< imap->first 
				<< "		"<< imap->second 
				<< "		"<< sqrt(- imap->second * imap->second + stor.Yield2[imap->first])
				<< "\n";
		}

		return out;
	}

/// Retorna a Temperatura.
/// 
/// @return : Temperatura
/// 
//_________________________________________________________________________________________________________

	double GetTemperature();

/// Retorna a variancia da Temperatura.
/// 
/// @return : Variancia da Temperatura
/// 
//_________________________________________________________________________________________________________

	double GetsigTemperature();

/// Retorna a Entropia.
/// 
/// @return : Entropia
/// 
//_________________________________________________________________________________________________________

	double GetEntropy();

/// Retorna a variancia da Entropia.
/// 
/// @return : Variancia da Entropia
/// 
//_________________________________________________________________________________________________________

	double GetsigEntropy();

/// Retorna a Multiplicidade.
/// 
/// @return : Multiplicidade
/// 
//_________________________________________________________________________________________________________

	double GetMultiplicity();

/// Retorna a variancia da Multiplicidade.
/// 
/// @return : Variancia da Multiplicidade.
/// 
//_________________________________________________________________________________________________________

	double GetsigMultiplicity();

/// Retorna a Capacidade Termica.
/// 
/// @return : Capacidade Termica
/// 
//_________________________________________________________________________________________________________

	double GetCv();

/// Retorna a variancia da Capacidade Termica.
/// 
/// @return : Variancia da Capacidade Termica
/// 
//_________________________________________________________________________________________________________

	double GetsigCv();

private:

	/// Mapa para armazenar Yield (ZZAAA)
	map<int, double> Yield;
	/// Mapa para armazenar valor quadratico do Yield (ZZAAA)
	map<int, double> Yield2;

	/// cumulativa do Peso (sum(Qi)).
	 double	Mount,
	/// Temperatura <T>.
				Temperature,
	/// Entropia <S>.
				Entropy,
	/// Multiplicidade <M>.
				Multiplicity,
	/// Numero de Barions do nucleo <A>.
				A,
	/// Carga do nucleo <Z>.
				Z,
	/// Capacidade Termica <Cv>.
				Cv,	
	/// Press�o <P>.
				P,	
	/// <T^2>
				sigTemperature,
	/// <S^2>
				sigEntropy,
	/// <M^2>
				sigMultiplicity,
	/// <A^2>
				sigA,
	/// <Z^2>
				sigZ,
	/// <Cv^2>
				sigCv,	
	/// <P^2>
				sigP;	
};
#endif

