/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TPARTITIONHELPER_HH
#define __TPARTITIONHELPER_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <vector>

using namespace std;

/// Classe contruida para gerar interativamente os vetores inteiros das parti��es.
///
//_________________________________________________________________________________________________________

class TPartitionHelper{

public :

/// Constroi o vetor parti�a� inteiro.
///
/// @param A : Numero de elementos da parti��o.
/// @param M : Multiplicidade da parti��o.
/// @param v : Vetor parti��o.
//_________________________________________________________________________________________________________

	static void Makefirstpartition(int A, int M, vector<int> & v);

/// verifica se a pr�xima parti��o existe.
///
/// @param v : Vetor parti��o.
/// @return : true se existe, false se n�o existe a pr�xima parti��o. 
//_________________________________________________________________________________________________________

	static bool NextPartitionIs(vector<int> & vec);

/// Constroi apartir do vetor parti��o v a proxima parti��o (altera v).
///
/// @param v : Vetor parti��o.
/// @return : true se existe, false se n�o existe a pr�xima parti��o. 
//_________________________________________________________________________________________________________

	static bool nextpartition(vector<int> & vec) ;

};
#endif