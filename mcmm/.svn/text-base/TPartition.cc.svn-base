/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TPartition.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

TPartition::TPartition(int A_, int Z_,double E_Ex_):Z(Z_),A(A_),ExcitationEnergy(E_Ex_),VarRegularization(250)
{
	probMultiplicity.Generate(A); // Gera a fun��o parti��o para o sorteio (fun��o PAM).
}

//_________________________________________________________________________________________________________

TPartition::TPartition(int A_, int Z_):Z(Z_),A(A_),ExcitationEnergy(0)
{
	probMultiplicity.Generate(A); // Gera a fun��o parti��o para o sorteio (fun��o PAM).
}

//_________________________________________________________________________________________________________

TPartition::~TPartition(){
	Reset(); // Limpa os elementos da Parti��o.
}

//_________________________________________________________________________________________________________

void TPartition::Reset(){

	if (!Partition.empty()){
		for_each(Partition.begin(),Partition.end(),DeleteTFragment()); // Elimina cada elemento de Partition.
	}

	Partition.clear();  // Limpa o vetor de Ponteiros

	Weight			= 0.0;
	Temperature		= 0.0;
	HelmoltzEnergy	= 0.0;
}

//_________________________________________________________________________________________________________

void TPartition::MakeProcess(){
	Reset();				// Limpa os elementos da Parti��o.
	while(!(Generate()));	// Executa Generate() at� obter uma parti��o v�lida.
}
//_________________________________________________________________________________________________________

void TPartition::ContinueProcess(){
	expand(Partition);



	///...
}

//_________________________________________________________________________________________________________

int TPartition::chooseMultiplicity()
{
	double bias = germ.doubleRand();	// sorteio .
	double mount=0;
	int M =0;

	do {										// Obtem da Multiplicidade de 
		M++;									// acordo com a cumulativa.
		mount += probMultiplicity.getProb(M);	//
	}while( bias > mount);

	Weight	=  double(mount);	// Armazena probabilidade. 
	return M; 
}

//_________________________________________________________________________________________________________

void TPartition::choosePartition(){

	vector<int> vSorteio;
	vector<int> :: iterator _vSorteio; 
	vPartition  :: iterator _Partition;

	int aux_var     = 0; 
	bool duplicates = false;

	vSorteio.push_back(0);	// Limites para o sorteio  
	vSorteio.push_back(A);	//   dos M fragmentos.


	for (int count=0; count != Multiplicity-1; count++ ){	// Sorteia M-1 inteiros diferente 
															//   e igualmente distribuidos.
		aux_var = germ.intRand(1,A-1);
		duplicates = false;

		for(_vSorteio = vSorteio.begin();_vSorteio != vSorteio.end(); ++_vSorteio ){
			if (*_vSorteio == aux_var) 
				duplicates = true;	
		}
			
		if (duplicates == false)
			vSorteio.push_back(aux_var); // Se inteiro n�o existe add no vetor.

		else if(duplicates == true)
			count--;
	}

	sort(vSorteio.begin(),vSorteio.end());	// Ordena vetor de inteiros

	{ //	Adiciona mas Massas (A) e Cargas para cada fragmento.

		int A_temp,Z_temp;
		int Z_mount = 0;
		
		for (int i = 0; i != Multiplicity; i++ ){ 
			A_temp = vSorteio[i+1]-vSorteio[i];
			Z_temp = int(double(A_temp)*(double(Z)/double(A)));
			Z_mount += Z_temp;

			// melhorar a escolha de z pelo minimo.

			TFragment *frag = new TFragment(A_temp,Z_temp,0);			
			Partition.push_back(frag);

		}
		Z_mount = Z-Z_mount;

		int sort = 0;

		// Completa as cargas (uniformemente sorteadas).

		while ((Z_mount) > 0){

			sort = germ.intRand(0,Multiplicity-1);

			A_temp = (Partition[sort])->GetA();
			Z_temp = (Partition[sort])->GetZ();

			if ( Z_temp < MaxZ(A_temp) ){

				(Partition[sort])->SetZ(Z_temp+1);
				Z_mount--;

			}
		}
	}

	vSorteio.clear();	// Limpa o vetor de inteiros (fragmentos j� contruidos).

	//	Fim do sorteio da Parti��o

}

//_________________________________________________________________________________________________________

void TPartition::mountPartition(vector<int>& vec ){

	{ //	Adiciona mas Massas (A) e Cargas para cada fragmento.

		int A_temp,Z_temp;
		int Z_mount = 0;
		
		for (vector<int>::iterator i = vec.begin(); i != vec.end(); i++ ){ 
			
			A_temp = (*i);
			Z_temp = int(double(A_temp)*(double(Z)/double(A)));
			Z_mount += Z_temp;

			// melhorar a escolha de z pelo minimo.

			TFragment *frag = new TFragment(A_temp,Z_temp,0);			
			Partition.push_back(frag);

		}

		Z_mount = Z-Z_mount;

		int sort = 0;

		// Completa as cargas (uniformemente sorteadas).

		while ((Z_mount) > 0){

			sort = germ.intRand(0,Multiplicity-1);

			A_temp = (Partition[sort])->GetA();
			Z_temp = (Partition[sort])->GetZ();

			if ( Z_temp < MaxZ(A_temp) ){

				(Partition[sort])->SetZ(Z_temp+1);
				Z_mount--;

			}
		}
	}
	//	Fim da montagem da Parti��o
}

//_________________________________________________________________________________________________________

bool TPartition::Generate()
{
		
	vPartition  :: iterator _Partition;
	
	Multiplicity = chooseMultiplicity();	// Obtem a Multiplicidade.

	choosePartition();  // Sorteia a parti��o igualmente distribuida

	colapse(Partition);	// Colapsa Parti��o para calcular as grandesas fisicas do sistema.

	Temperature = model.GetTemperature(Multiplicity, A, Z, ExcitationEnergy ,Partition);

	if (Temperature < 0.0) {
		Reset();
		//std::cout << "T<0 \n";
		return false;
	};


	HelmoltzEnergy   = model.F (A,Z,Temperature,Multiplicity,Partition);
	Entropy			 = model.S (A,Z,Temperature,Multiplicity,Partition);
	Cv				 = model.Cv(A,Z,Temperature,Multiplicity,Partition);
	Weight			*= exp( double(Entropy - VarRegularization)); 

//	cout << "Weight : " << Weight << "\n"; 

	if (numeric_limits< double>::infinity() == Weight) {
		Reset(); 
		cerr << "Weight extrapolated numerical limit : regularize Weight in TPartition. \n";
		exit(1);
	}

	if (HelmoltzEnergy > 0 ) {
		cerr << " Warning : (TPartiton::Generate()) : Positive Value of Free Energy (deltaF) \n";
		Reset();
		return false;
	}

	return true;
}

//_________________________________________________________________________________________________________

bool TPartition::Generate(vector<int>& vec )
{
		
	vPartition  :: iterator _Partition;
	
	mountPartition(vec); // monta a parti��o de acordo com vec

	colapse(Partition);	// Colapsa Parti��o para calcular as grandesas fisicas do sistema.

	TCanonical model;

	Temperature = model.GetTemperature(Multiplicity, A, Z, ExcitationEnergy ,Partition);

	if (Temperature < 0.) {
		Reset();
		return false;
	};

	HelmoltzEnergy  = model.F(A,Z,Temperature,Multiplicity,Partition);
	Weight			= exp(-(HelmoltzEnergy+model.B(A,Z)+ExcitationEnergy)/Temperature);

 	if (HelmoltzEnergy > 0 ) {
		cerr << " Warning : (TPartiton::Generate()) : Positive Value of Free Energy (deltaF) ";
		Reset();
		return false;
	}

	return true;
}

//_________________________________________________________________________________________________________

int TPartition::MIN(int A_, int B_)
{

	if (A_ == B_) 
		return A_;
	else 
		return  A_ < B_ ? A_:B_;
		
}

//_________________________________________________________________________________________________________

int TPartition::MAX(int A_, int B_)
{

	if (A_ == B_)
		return A_;
	else 
		return  A_ < B_ ? A_:B_;
}

//_________________________________________________________________________________________________________

int TPartition::MaxZ(int A_)
{

	if (A_ == 1) {
		return 1;
	}
	else if (A_ == 2) {
        return 2;
	}
	else if (A_ == 3) {
	    return 2;
	}
	else if (A_ == 4) {
	   return 3;
	}
	else if (A_ == 5) {
		return 3;
	}
	else if (A_ == 6) {
		return 4;
	}
	else if (A_ == 7) {
		return 5;
	}
	else if (A_ == 8) {
		return 6;
	}
	else if (A_ == 9) {
		return 6;
	}
	else if (A_ == 10) {
		return 7;
	}
	else if (A_ == 11) {
		return 7;
	}
	else if (A_ == 12) {
		return 8;
	}
	else {
		int _z_ = MIN(A_ - 2,(3 * A_ )/4);
		_z_		= MIN(A_/2+4,_z_);
		_z_     = MIN(Z,_z_);
		_z_		= MIN(_z_,A);
		return    MIN(Z,_z_);
	}
}

//_________________________________________________________________________________________________________

int TPartition::MinZ(int A_)
{
		if (A_ == 1) {
		return 0;
	}
	else if (A_ == 2) {
        return 0;
	}
	else if (A_ == 3) {
	    return 0;
	}
	else if (A_ == 4) {
		return 1;
	}
	else if (A_ == 5) {
		return 2;
	}
	else if (A_ == 6) {
		return 2;
	}
	else if (A_ == 7) {
		return 3;
	}
	else if (A_ == 8) {
		return 3;
	}
	else if (A_ == 9) {
		return 3;
	}
	else if (A_ == 10) {
		return 3;
	}
	else if (A_ == 11) {
		return 4;
	}
	else if (A_ == 12) {
		return 4;
	}
	else {
		int _z_ = A_/3;
		_z_		= MAX(A_/2-6,_z_);
		_z_		= MIN((Z-5)*A_/A,_z_);
		_z_		= MAX(_z_,Z + A_ - A);
		return    MAX(_z_,0);
	}
}

//_________________________________________________________________________________________________________

int TPartition::removefirst(vPartition &vec)
{
	vPartition::iterator SS;
	bool flag = false;
	int count = 0;
	int AA,ZZ;

	SS = vec.begin();
	do {
		if (SS == vec.begin()){
			AA = (*SS)->GetA();
			ZZ = (*SS)->GetZ();
		}

		if (flag == true){
			SS = vec.begin();
			flag = false;
		}
	
		if (((AA == (*SS)->GetA())&&(ZZ == (*SS)->GetZ()))&&(vec.size()>1)){
			count ++;
			delete (*SS);
			SS = vec.erase(SS);

			if (SS != vec.begin())
				SS--;
			else
				flag = true;
		}
		if (((AA == (*SS)->GetA())&&(ZZ == (*SS)->GetZ()))&&(vec.size()==1)){
			count ++;
			delete (*SS);
			SS= vec.erase(SS);
		}
		if (vec.size() != 0)
			SS++;
	}while (SS != vec.end());

	return count;		
}

//_________________________________________________________________________________________________________

void TPartition::colapse(vPartition & vec)
{
	vPartition part;
	vPartition::iterator SS;
	
	while (!(vec.empty())){
		TFragment *frag = new TFragment(vec[0]->GetA(),vec[0]->GetZ(),0);
		frag -> SetM(removefirst(vec));
		part.push_back(frag);
	}
	for (SS = part.begin(); SS != part.end(); SS++){
		vec.push_back(*SS);
	}
	part.clear();
}

//_________________________________________________________________________________________________________

void TPartition::expand(vPartition & vec)
{
	vPartition::iterator SS;

	double energyex = 0;
	for (SS = vec.begin(); SS != vec.end(); SS++){
	
		energyex = model.E_ex((*SS)->GetA(),(*SS)->GetZ(),Temperature);
		
		if ( (*SS)->GetM() >1 ) {
			for (int i = 1; i <= ((*SS)->GetM()-1); i++){
		
				TFragment *frag = new TFragment((*SS)->GetA(),(*SS)->GetZ(),0);
				frag -> SetM(1);
				frag -> SetExcitationEnergy(energyex);
				vec.push_back(frag);
			}
			(*SS)->SetM(1);
			(*SS)->SetExcitationEnergy(energyex);
		}

	}
	
}

//_________________________________________________________________________________________________________

double TPartition::findMustProbable( int Mult )
{

	vector<int> vect;

	TPartitionHelper::Makefirstpartition(A, Multiplicity, vect);

	Multiplicity		= Mult;
	double MaxWeight	= 0;

	do{
		Reset();
		Generate(vect);

		if (Weight > MaxWeight){
			MaxWeight = Weight; 
		}
	}while (TPartitionHelper::nextpartition(vect) == true);
	
	vect.clear();
	return MaxWeight;
}

//_________________________________________________________________________________________________________

vPartition& TPartition::GetPartition()
{
	return Partition;
}

//_________________________________________________________________________________________________________

double TPartition::GetMultiplicity()
{
	return Multiplicity ;
}

//_________________________________________________________________________________________________________

double  TPartition::GetWeight()
{
	return Weight ;
}

//_________________________________________________________________________________________________________

double TPartition::GetTemperature()
{
	return Temperature;
}

//_________________________________________________________________________________________________________

double TPartition::GetEntropy()
{
	return Entropy;
}

//_________________________________________________________________________________________________________

double TPartition::GetCv()
{
	return Cv;
}



