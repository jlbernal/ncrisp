/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __TPARTITION_HH
#define __TPARTITION_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include "PamClass.hh"
#include "TFragment.hh" 
#include "TRandomi.hh"
#include "TCanonical.hh"
#include "TPartitionHelper.hh"

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>

using namespace std;

///
/// Definição do vetor de ponteiros de TFragment usado para construir a partição, ou seja, 
/// cada ponteiro TFragment* armazena o endereço de um fragmento na memória. 
///
//_________________________________________________________________________________________________________

typedef vector<TFragment*> vPartition;

/// A Classe TPartition é responsavel por gerar cada partição do processo de multifragmentação,
/// armazenar os fragmento, ou seja, faz o processo ce fragmentação em si.
///
//_________________________________________________________________________________________________________

class TPartition{

public:

/// Construtor Padão da Classe.
///
///   @param  A_   : Número de Baryons do Núcleo.                       
///   @param  Z_   : Carga do Núcleo.                                   
///   @param  E_Ex : Energia de Excitação do Núcleo.                
///
//_________________________________________________________________________________________________________


	TPartition(int A_, int Z_,double E_Ex_);

/// Construtor Padão da Classe Sobrecarregado. Seta a Energia de Excitação do Núcleo como 0.
///
///   @param  A_   : Número de Baryons do Núcleo.                       
///   @param  Z_   : Carga do Núcleo.                                   
///
//_________________________________________________________________________________________________________


	TPartition(int A_, int Z_);

/// Construtor de Cópia
///
///   @param  part   : Objeto TPartition.                                   
///
//_________________________________________________________________________________________________________

	TPartition(const TPartition & part){}

/// Destrutor Padão da Classe.
///
//_________________________________________________________________________________________________________

	~TPartition();

/// Limpa os elementos da partição.
///
//_________________________________________________________________________________________________________

	void Reset();

/// Gera a partição, sorteando os elementos e calcula as grandesas fisicas do sistema.
///
///   @return  : "true" de a Partição é valida ou "false" se não é.                                   
///
//_________________________________________________________________________________________________________

	bool Generate();

/// Gera a partição, sorteando os elementos e calcula as grandesas fisicas do sistema.
///
///   @param	: Vetor inteiro (partiçao P).
///   @return	: "true" de a Partição é valida ou "false" se não é.                                   
///
//_________________________________________________________________________________________________________

	bool Generate(vector<int>& vec);


/// Executa o processo de construção da partição.
///
//_________________________________________________________________________________________________________

	void MakeProcess();

/// Continua o processo - Evaporação/fissão dos fragmentos.
///
//_________________________________________________________________________________________________________

	void ContinueProcess();


/// Monta a Partição de acordo com o vetor inteiro (partiçao P).
///
///   @param	: Vetor inteiro (partiçao P).
///   @return	: "true" de a Partição é valida ou "false" se não é.                                   
///
//_________________________________________________________________________________________________________

	void mountPartition(vector<int>& vec );

/// Sorteia a partição igualmente distribuida.
///
//_________________________________________________________________________________________________________

	void choosePartition();

/// Sorteia a multiplicidade de fragmentação do sistema e armazena sua probabilidade em "Weight".
///
///   @return  : Multiplicidade do sistema.                                   
///
//_________________________________________________________________________________________________________

	int chooseMultiplicity();

/// Remove todos os elementos iguais ao Primeiro elemento do vetor.
///
///   @param  vPartition	: Vetor contendo ponteiros para os fragmentos.                                   
///   @return				: Número de elemento iguais ao Primeiro elemento.                                   
///
//_________________________________________________________________________________________________________

	int removefirst(vPartition &vec);

/// Colapsa a o vetor Partição retirando os elementos reptidos e adicionando o seu numero 
/// à Multilicidade daquele fragmento.
///
///   @param  vPartition	: Vetor contendo ponteiros para os fragmentos.                                   
///
//_________________________________________________________________________________________________________

	void colapse(vPartition & vec);

/// Espande o vetor Partição colocando os elementos reptidos e redefinindo a
/// Multilicidade dos fragmentos.
///
///   @param  vPartition	: Vetor contendo ponteiros para os fragmentos.                                   
///
//_________________________________________________________________________________________________________

	void expand(vPartition & vec);

/// Retorna o menor de dois inteiros.
/// 
/// @param  A_ : Primeiro Inteiro.                                   
/// @param  B_ : Segundo Inteiro.                                   
/// @return    : Retorna o menor dos dois elementos.                                   
/// 
//_________________________________________________________________________________________________________

	int MIN(int A_, int B_);

/// Retorna o maior de dois inteiros.
/// 
/// @param  A_ : Primeiro Inteiro.                                   
/// @param  B_ : Segundo Inteiro.                                   
/// @return    : Retorna o maior dos dois elementos.                                   
/// 
//_________________________________________________________________________________________________________

	int MAX(int A_, int B_);


/// Função que calcula o valor máximo da carga Z.
/// 
/// @param  A_ : Número de Bárions do fragmento.                                   
/// @return    : O valor máximo para a carga.                                   
/// 
//_________________________________________________________________________________________________________

	int MaxZ(int A_);

/// Função que calcula o valor minimo da carga Z.
/// 
/// @param  A_ : Número de Bárions do fragmento.                                   
/// @return    : O valor minimo para a carga.                                   
/// 
//_________________________________________________________________________________________________________

	int MinZ(int A_);

/// Rotina que procura a partição mais provavel (busca uma a uma).
/// 
/// @param  Mult	: Multiplicidade da Partição.                                   
/// @return			: Peso máximo.                                   
/// 
//_________________________________________________________________________________________________________

	double findMustProbable( int Mult );

///	Retorna o vetor Partição.
/// 
/// @return			: Partição.                                   
/// 
//_________________________________________________________________________________________________________

	vPartition& GetPartition();

///	Retorna a Multiplicidade da Partição.
/// 
/// @return			: Multiplicidade.                                   
/// 
//_________________________________________________________________________________________________________
	
	double GetMultiplicity();

///	Retorna o peso da Partição.
/// 
/// @return			: Peso da Partição.                                   
/// 
//_________________________________________________________________________________________________________

	double GetWeight();

///	Retorna a Temperatura da Partição.
/// 
/// @return			: Temperatura.                                   
/// 
//_________________________________________________________________________________________________________

	double GetTemperature();

///	Retorna a Entropia da Partição.
/// 
/// @return			: Entropia.                                   
/// 
//_________________________________________________________________________________________________________

	double GetEntropy();

///	Retorna o Calor especifico a volume constante da Partição.
/// 
/// @return			: Calor especifico.                                   
/// 
//_________________________________________________________________________________________________________

	double GetCv();

private:

	/// Energia de Excitação Inicial do Sistema.
	double ExcitationEnergy;
	/// Baryon Number.
	int	A,
	/// Charge Nunber. 
		Z; 
	/// Temperatura do Sistema.
	double Temperature;
	// Energia Livre de Helmoltz Partição.
	double HelmoltzEnergy;
	/// Entropy .
	double Entropy;
	/// Cv.
	double Cv;
	/// Peso (Probabilidade Não Normalizada da Partição).
	 double Weight;
	/// Multiplicidade da Partição.
	int Multiplicity;
	double VarRegularization;

	/// Gerador da Função Partição.
	PamClass probMultiplicity;
	/// Vetor Partição - Armazena os Fragmentos( tipo TFragment ).
	vPartition Partition;
	/// Clase que contem o gerador de números aleatórios.
	TRandomi germ;
	/// Rotinas de modelo
	TCanonical model;

public:

/// Estrurura usada para deletar fragmentos do vertor vPartition.
//_________________________________________________________________________________________________________

	struct DeleteTFragment
	{
		void operator()(const TFragment *ptr) const
		{
			delete ptr;
		}
	};
};

#endif 

