/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __DEFINED_VALUES_HH
#define __DEFINED_VALUES_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#define PI       3.14159265358979323846

/// Define Constantes usadas em todo codigo.
///

const double	/// Raio Nuclear
				r_0		=   1.2     , // [fm]
				/// Constande Planck x Velocidade da Luz
				hc		= 197.3     , // []
				/// Massa do netron
				mnc2	= 939.5656  , // [MeV]
				/// Massa do proton.
				mpc2	= 938.2723  ; // [MeV]
#endif