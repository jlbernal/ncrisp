/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */


//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TStoragef.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

TStoragef::~TStoragef()
{
	if (!(Yield.empty()) ){
		Yield.clear();
		Yield2.clear();
	}
}

//_________________________________________________________________________________________________________

void TStoragef::AddYield(int A_, int Z_, double M, double Weight)
{
	int Zaid = 1000*Z_ + A_;
	
	if (Yield.find(Zaid) != Yield.end()) {
		(Yield[Zaid]) = (Yield[Zaid]) +  double(M)*Weight;
		(Yield2[Zaid]) = (Yield2[Zaid]) +  double(M*M)*Weight;
	}
	else{
		Yield.insert(map<int,double>::value_type(Zaid, double(M)*Weight));
		Yield2.insert(map<int,double>::value_type(Zaid, double(M*M)*Weight));
	}
}

//_________________________________________________________________________________________________________

void TStoragef::AddPartition(vPartition & Part,  double Weight)
{
	for (vPartition  :: iterator _Partition = Part.begin(); _Partition != Part.end(); _Partition++ ){
		AddYield((*_Partition)->GetA(), (*_Partition)->GetZ(),(*_Partition)->GetM(), Weight);
	} 
}

//_________________________________________________________________________________________________________

void TStoragef::reset(){

	if (!(Yield.empty()) ){
		Yield.clear();
		Yield2.clear();
	}
	Mount=Temperature=Cv=Entropy=Multiplicity=0;
	sigTemperature=sigCv=sigEntropy=sigMultiplicity=0;
}

//_________________________________________________________________________________________________________

void TStoragef::AddWeight( double Weight){
	Mount = Mount+ Weight;
}

//_________________________________________________________________________________________________________

void TStoragef::AddTemperature(double Temperature_, double Weight_){
	Temperature += Temperature_*Weight_;
	sigTemperature += Temperature_*Temperature_*Weight_;
}

//_________________________________________________________________________________________________________

void TStoragef::AddEntropy(double Entropy_,  double Weight_){
	Entropy +=  double(Entropy_)*Weight_;
	sigEntropy +=  double(Entropy_*Entropy_)*Weight_;
}

//_________________________________________________________________________________________________________

void TStoragef::AddCv(double Cv_, double Weight_){
	Cv +=  double(Cv_)*Weight_;
	sigCv +=  double(Cv_*Cv_)*Weight_;
}

//_________________________________________________________________________________________________________

void TStoragef::AddMultiplicity(double Multiplicity_, double Weight_){
	Multiplicity +=  double(Multiplicity_)*Weight_;
	sigMultiplicity +=  double(Multiplicity_*Multiplicity_)*Weight_;
}

//_________________________________________________________________________________________________________

void TStoragef::MakeStatistics(){

	Temperature  = Temperature/Mount;
	Entropy		 = Entropy/Mount;
	Multiplicity = Multiplicity/Mount;
	Cv			 = Cv/Mount;
	sigTemperature  = sigTemperature/Mount;
	sigEntropy		= sigEntropy/Mount;
	sigMultiplicity = sigMultiplicity/Mount;
	sigCv			= sigCv/Mount;

	map<int,  double>::iterator imap;

	for (imap = Yield.begin(); imap != Yield.end(); imap++ ){
		(imap->second)=(imap->second)/Mount ;
	}

	for (imap = Yield2.begin(); imap != Yield2.end(); imap++ ){
		(imap->second)=(imap->second)/Mount ;
	}
}

//_________________________________________________________________________________________________________

double TStoragef::GetTemperature(){
	return Temperature;
}

//_________________________________________________________________________________________________________

double TStoragef::GetsigTemperature(){
	return sqrt(sigTemperature - Temperature*Temperature);
}

//_________________________________________________________________________________________________________

double TStoragef::GetEntropy(){
	return Entropy;
}

//_________________________________________________________________________________________________________

double TStoragef::GetsigEntropy(){
	return sqrt(sigEntropy - Entropy*Entropy);
}

//_________________________________________________________________________________________________________

double TStoragef::GetMultiplicity(){
	return Multiplicity;
}

//_________________________________________________________________________________________________________

double TStoragef::GetsigMultiplicity(){
	return sqrt(sigMultiplicity - Multiplicity*Multiplicity);
}

//_________________________________________________________________________________________________________

double TStoragef::GetCv(){
	return Cv;
}

//_________________________________________________________________________________________________________

double TStoragef::GetsigCv(){
	return sqrt(sigCv - Cv*Cv);
}




