/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "PamClass.hh"

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

//_________________________________________________________________________________________________________

double PamClass::PAM( int A0 , int M)
{

	double count=0;

	if (M == 1) 
		count +=1;

	for (int i=0;i<=((A0/M)-1);i++)
		if (M > 1) count+=PAM_Mod3(A0-i*M-1 , M-1);

	return count;

}

//_________________________________________________________________________________________________________

double PamClass::PAM_Mod1( const int A0, const int M)
{

	double count=0;

	if (M == 4) 
		count +=	( 3*(double(A0)+1)*( 2*double(A0)*(double(A0)+2)-13+9*pow(-1,double(A0)))
					-96*cos(2*PI*double(A0)/3.0)+108*pow(-1,double(A0/2))*double(A0/2)+32*pow(3,.5)
					*sin(2*PI*double(A0)/3.0))/864;
	else if (M == 3) 
			count += (6*double(A0*A0)-7-9*pow(-1,double(A0))+16*cos(2*PI*double(A0)/3.0))/72;
	else if (M == 2) 
		count += (2*A0-1+pow(-1,double(A0)))/4.0;
	else if (M == 1) 
		count +=1;

	for (int i=0;i != M ;i++)
		if ((M > 4) && ((A0-M)>0) ) count+=PAM(A0-M , M-i);

	return count;

}	

//_________________________________________________________________________________________________________

double PamClass::PAM_Mod2( const int A0, const int M)
{

	double count=0;

	if (M == 4) 
		count += (
			3*(double(A0)+1)*(
					2*double(A0)*(double(A0)+2)-13+9*pow(-1,double(A0))
					)
			-96*cos(2*PI*double(A0)/3.0)
			+108*pow(-1,double(A0/2))*double(A0/2)
			+32*pow(3,.5)*sin(2*PI*double(A0)/3.0)
		)/864;
	else if (M == 3) 
		count +=(6*double(A0*A0)-7-9*pow(-1,double(A0))+16*cos(2*PI*double(A0)/3.0))/72;
		else if (M == 2) 
		count +=(2*A0-1+pow(-1,double(A0)))/4.0;
	else if (M == 1) 
		count +=1;

	for (int i=0;i<=((A0/M)-1);i++)
		if (M > 4) count+=PAM(A0-i*M-1 , M-1);

	return count;

}	

//_________________________________________________________________________________________________________

double PamClass::PAM_Mod3( int A0 , int M)
{

	double count=0;

	if (M == 1) 
		count +=1;

	for (int i=0;i<=((A0/M)-1);i++)
		if (M > 1) {
		
			int Zaid = (A0-i*M-1)*1000000+(M-1);

			if (Table.find(Zaid) != Table.end()) {
				count+=Table[Zaid];
			}
			else{
				double Weight = PAM_Mod3(A0-i*M-1 , M-1);
				Table.insert(map<int,double>::value_type(Zaid,Weight));
				count+=Weight;
			}

		}

	return count;

}

//_________________________________________________________________________________________________________

double PamClass::PAM_Mod4( int A0 , int M)
{

	double count=0;

	if (M == 1) 
		count +=1;

	for (int i=0;i != M ;i++)
		if ((M > 1) && ((A0-M)>0)) {
		
			int Zaid = (A0-i*M-1)*1000000+(M-1);

			if (Table.find(Zaid) != Table.end()) {
				count+=Table[Zaid];
			}
			else{
				double Weight = PAM(A0-M , M-i);
				Table.insert(map<int,double>::value_type(Zaid,Weight));
				count+=Weight;
			}

		}

	return count;

}

//_________________________________________________________________________________________________________

double PamClass::PAM( int A0 )
{
	
	double norm = 0;	

	for (int M=1;M<=A0;M++)
		norm += PAM(A0 , M);

	return norm;
}	

//_________________________________________________________________________________________________________

void PamClass::Generate( int A_)
{
		
	Prob.clear();
	double Norm = 0;

	for (int i=0; i!=A_; i++){

		Prob.push_back(PAM_Mod3(A_,i+1));
		Norm += Prob[i];
//		cout << "Pam("<<A_<<","<< i+1 <<") : "<< Prob[i] <<"\n";
	}
	for (int i=0; i!=A_; i++)
		Prob[i]=Prob[i]/Norm;
	
}

//_________________________________________________________________________________________________________

PamClass::PamClass( int A_ , string path)
{
	string name;
	stringstream Convert;
	{
		Convert << int(A_);
		Convert >> name;
	}
	name = path+name;

	ifstream fin(name.c_str(),ios::in);
		
	if (fin.is_open()){
		double temp=0;
		while (fin >> temp){
			Prob.push_back(temp);
		}
		

	}else {
		makeProbArq(name,A_);
	}
	fin.close();
}

//_________________________________________________________________________________________________________

PamClass::PamClass( int A_ )
{
	makeProb(A_);
}

//_________________________________________________________________________________________________________

PamClass::~PamClass(){
	Prob.clear();
	Table.clear();
}

//_________________________________________________________________________________________________________

double PamClass::getProb(const int M)
{
	return Prob[M-1]; 
}

//_________________________________________________________________________________________________________

void PamClass::makeProb(int A_)
{
	double Norm = 0;
	for (int i=0; i!=A_; i++){
		Prob.push_back(PAM_Mod3(A_,i+1));
		//cout << "Pam("<<A_<<","<< i+1 <<") : "<< Prob[i] <<"\n";
		Norm += Prob[i]; 
	}
	for (int i=0; i!=A_; i++){
		Prob[i]=Prob[i]/Norm;
	}
	
}

//_________________________________________________________________________________________________________

void PamClass::addProb(double temp)
{
	Prob.push_back(temp);
}

//_________________________________________________________________________________________________________

void PamClass::makeProbArq(string & namearq, int A_)
{
	ofstream fout(namearq.c_str());
	makeProb(A_);

	for (vector<double> ::iterator i=Prob.begin() ; i != Prob.end() ; i++){
		fout << " " << (*i) << "\n";
	}

	fout.close();
}


// PamClass::
