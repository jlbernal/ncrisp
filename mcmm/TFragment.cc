/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#include "TFragment.hh" 

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

double TFragment::GetExcitationEnergy()
{

	return ExcitationEnergy;

}

const TFragment & TFragment::operator=(const TFragment &var)
{

	if (this != &var){
		ExcitationEnergy = var.ExcitationEnergy;
		A = var.A;
		Z = var.Z;
	}

	return *this;

}

bool TFragment::operator==(const TFragment &var) const
{

	return (A == var.A)&&(Z == var.Z);

}

bool TFragment::operator!=(const TFragment &var) const
{

	return (A != var.A)||(Z != var.Z);

}
