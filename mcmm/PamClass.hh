/* ================================================================================
 * 
 * 	Copyright 2010 Pedro C. R. Rossi, Airton Deppman
 * 
 * 	This file is part of CRISP.
 * 
 * 	CRISP is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 * 	CRISP is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with CRISP. If not, see <http://www.gnu.org/licenses/>.

 * ================================================================================
 */

//------------------------------------------------------------------------//
//					
//						This document is part of:
//			Statistical Multifragmentation Module of CRISP code	
//
//		Authors :	Pedro C. R. Rossi (pedro.russorossi@gmail.com)
//					Airton Deppman (adeppman@gmail.com)
//					
//------------------------------------------------------------------------//
#ifndef __PAMCLASS_HH
#define __PAMCLASS_HH

#ifdef _WIN32_
#endif
#ifdef _LINUX_
#endif

#include <iostream>
#include <vector>
#include <cmath>
#include <map>

#include <string>
#include <sstream>
#include <fstream>

#include "definedvalues.hh"


using namespace std;

/// A Classe PamClass Calcula as funções de estatisticas (PAM Formula) usada 
/// no modelo de multifragmentação. A distribuição P(A,M) é gerada no construtor 
/// para uma dado valor de A e seus valores normalizados (probabilidades) 
/// armazenados em um vetor para uso posterior atraves da função getProb(M),
/// onde M é a multiplicidade (M <= A).
/// 
//_________________________________________________________________________________________________________

class PamClass  
{
public:

/// Construtor
///
/// @param A_	: Numero de Baryons da partição.
/// @param path : Caminho para gravar/ler o arquivo PAM.
///
//_________________________________________________________________________________________________________

	PamClass( int A_ , string path);

/// Construtor
///
/// @param A_	: Numero de Baryons da partição.
///
//_________________________________________________________________________________________________________

	PamClass( int A_ );

/// Construtor Padrão.
//_________________________________________________________________________________________________________

	PamClass(){}

/// Construtor de Cópia.
//_________________________________________________________________________________________________________

	PamClass(const PamClass & copy ){}

/// Destrutor.
//_________________________________________________________________________________________________________

	~PamClass();

/// Retorna o numero parcial de combinações (P(A0,M)) associado a multiplicidade M.
///
/// @param A0 : Numero de Baryons da partição.
/// @param  M : Multiplicidade da partição.
///
/// @return Retorna o numero parcial de combinações.
///
//_________________________________________________________________________________________________________

	double PAM( const int A0 , const int M);

/// Retorna o numero parcial de combinações (P(A0,M)) associado a multiplicidade M.
/// Metodo 1 : Usa soluções analiticas para M<=4 e usa a eq. de recorrencia (#) ver manual.
///
/// @param A0 : Numero de Baryons da partição.
/// @param  M : Multiplicidade da partição.
///
/// @return Retorna o numero parcial de combinações.
///
//_________________________________________________________________________________________________________

	double PAM_Mod1( const int A0 , const int M);

/// Retorna o numero parcial de combinações (P(A0,M)) associado a multiplicidade M.
/// Metodo 2 : Usa soluções analiticas para M<=4 e usa a eq. de recorrencia usual.
///
/// @param A0 : Numero de Baryons da partição.
/// @param  M : Multiplicidade da partição.
///
/// @return Retorna o numero parcial de combinações.
///
//_________________________________________________________________________________________________________

	double PAM_Mod2( const int A0 , const int M);

/// Retorna o numero parcial de combinações (P(A0,M)) associado a multiplicidade M.
/// Metodo 3 : Mapeia a recorrencia e usa a eq. de recorrencia usual.
///
/// @param A0 : Numero de Baryons da partição.
/// @param  M : Multiplicidade da partição.
///
/// @return Retorna o numero parcial de combinações.
///
//_________________________________________________________________________________________________________

	double PAM_Mod3( int A0 , int M);

/// Retorna o numero parcial de combinações (P(A0,M)) associado a multiplicidade M.
/// Metodo 4 : Mapeia a recorrencia e usa a eq. de recorrencia (#) ver manual.
/// Este é o método adotado (Default).
/// @param A0 : Numero de Baryons da partição.
/// @param  M : Multiplicidade da partição.
///
/// @return Retorna o numero parcial de combinações.
///
//_________________________________________________________________________________________________________

	double PAM_Mod4( int A0 , int M);

/// Retorna o numero total de combinações (P(A0)).
///	
//_________________________________________________________________________________________________________

	double PAM( const int A0 );

/// Retorna a probabilidade associada à mutiplicidade M.
/// @param M valor de
/// @return valor
///
//_________________________________________________________________________________________________________

	double getProb(const int M);

/// Constroi o vetor Partição .
/// @param A :  Número de elementos.
///
//_________________________________________________________________________________________________________

	void makeProb(int A_);

	void addProb(double temp);

/// Constroi o vetor Partição  grava no arquivo.
///
/// @param A :  Número de elementos.
/// @param namearq :  Nome do arquivo.
///
//_________________________________________________________________________________________________________

	void makeProbArq(string & namearq, int A_);

/// Gera a fumção Partição.
///
/// @param A :  Número de elementos.
///
//_________________________________________________________________________________________________________

	void Generate( int A_);

private:

/// Vetor usado para armazenar as probabilidades associadas.
/// - Prob = [P(A,1)/P(A),P(A,2)/P(A),...,P(A,A)/P(A)]
///
//_________________________________________________________________________________________________________

	vector<double> Prob;

/// Mapa usado para tabelar a função.
//_________________________________________________________________________________________________________

	map<int,double> Table;

};	
#endif
