
#include "THtml.h"
#include "TSystem.h"

// cria documentacao para o CRISP
// somente funciona no linux
// testado com root 5.26.00
void html()
{
	THtml html;
	
//	html.LoadAllLibs();									// Carrega as bibliotecas do ROOT
	gSystem->Load("libCrisp.so");						// Carrega a biblioteca do CRISP
	html.SetCharset("UTF-8");							// Codificacao utilizada no HTML gerado
	html.SetAuthorTag("USP, CBPF and UESC");			// Autor(es)
	html.SetProductName("CRISP");						// Nome do programa
	html.SetRootURL("http://root.cern.ch/root/html");	// Link para pagina do ROOT
	html.SetHomepage("http://www.crisp.com.br/doc");	// Link para a pagina online do CRISP (quando ela existir)
	
	html.SetIncludePath("$(ROOTSYS):base:mcef:helpers:cascade:generators");	// includes
	html.SetInputDir("base:mcef:helpers:cascade:generators");
//	html.SetSourceDir("base:mcef:helpers:cascade:generators");				// pasta onde encontrar a codigo fonte do CRISP
	html.SetOutputDir("doc");		// pasta onde a documentacao serah colocada

	
//	html.MakeIndex("BaryonDecay");
//	html.MakeClass("BaryonDecay");
	html.MakeAll(kTRUE);					// gera toda a documentacao...
}
